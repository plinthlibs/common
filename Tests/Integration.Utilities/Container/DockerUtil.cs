using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integration.Utilities.Container;
public static class DockerUtil
{
    // when running tests in docker, uses host.docker.internal, otherwise hit ::1 where the docker is running
    /// <summary>
    /// The host to use when connecting to resources inside docker
    /// </summary>
#if RUNNING_IN_CI
    public static readonly string ContainerHost = Environment.GetEnvironmentVariable("BITBUCKET_DOCKER_HOST_INTERNAL") ?? "127.0.0.1";
#else
    public static readonly string ContainerHost = File.Exists("/.dockerenv") ? "host.docker.internal" : "::1";
#endif
}
