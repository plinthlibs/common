using System.Diagnostics;
using System.Net.Sockets;
using Microsoft.Extensions.Logging;
using NUnit.Framework;

namespace Integration.Utilities.Container;

public static class DockerCompose
{
    private static readonly ILogger logger = StaticLogManager.GetLogger();

    private static bool _weStarted = false;

    /// <summary>
    /// Start containers via docker compose (a docker-compose.yml should be in the project root)
    /// </summary>
    /// <param name="hostPort">port that the container is listening on</param>
    public static async Task Up(int hostPort)
    {
        // check if container running via docker-compose
        if (await IsContainerRunning(hostPort))
            return;

        var file = OperatingSystem.IsLinux() ? "docker" : "wsl";
        var firstArg = OperatingSystem.IsLinux() ? string.Empty : "docker";

        TestContext.Progress.WriteLine($"running {file} {firstArg} compose up");

        using var _ = logger.LogExecTiming(LogLevel.Information, "docker compose up");
        var p = Process.Start(new ProcessStartInfo
        {
            FileName = file,
            Arguments = $"{firstArg} compose up -d -V --wait",
            RedirectStandardOutput = true,
            RedirectStandardError = true
        });
        await p!.WaitForExitAsync();
        var stdout = await p.StandardOutput.ReadToEndAsync();
        TestContext.Progress.WriteLine($"docker compose (out):{stdout}");
        var stderr = await p.StandardOutput.ReadToEndAsync();
        TestContext.Progress.WriteLine($"docker compose (err):{stderr}");
        _weStarted = true;
    }

    private static async Task<bool> IsContainerRunning(int port)
    {
        using var tcpClient = new TcpClient();

        TestContext.Progress.WriteLine($"Checking connection to {DockerUtil.ContainerHost}:{port}");
        try
        {
            await tcpClient.ConnectAsync(DockerUtil.ContainerHost, port);
            TestContext.Progress.WriteLine("Container is running");
            tcpClient.Close();
            return true;
        }
        catch (Exception)
        {
            TestContext.Progress.WriteLine("Container is not running");
            return false;
        }
    }

    /// <summary>
    /// Stop containers via docker compose, if we started them
    /// </summary>
    public static async Task Down()
    {
        if (!_weStarted)
            return;

        var file = OperatingSystem.IsLinux() ? "docker" : "wsl";
        var firstArg = OperatingSystem.IsLinux() ? string.Empty : "docker";

        TestContext.Progress.WriteLine($"running {firstArg} compose down");

        using var _ = logger.LogExecTiming(LogLevel.Information, "docker compose down");
        var p = Process.Start(new ProcessStartInfo
        {
            FileName = file,
            Arguments = $"{firstArg} compose down",
            RedirectStandardOutput = true
        });

        await p!.WaitForExitAsync();
        var stdout = await p.StandardOutput.ReadToEndAsync();
        TestContext.Progress.WriteLine($"docker compose (out):{stdout}");
        var stderr = await p.StandardOutput.ReadToEndAsync();
        TestContext.Progress.WriteLine($"docker compose (err):{stderr}");
    }
}
