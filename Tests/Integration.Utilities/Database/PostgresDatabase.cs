using Dapper;
using Microsoft.Extensions.Logging;
using Npgsql;
using System.Text.RegularExpressions;

namespace Integration.Utilities.Database;

public static class PostgresDatabase
{
    private static readonly ILogger logger = StaticLogManager.GetLogger();

    private static readonly object _lock = new object();
    private static readonly List<Task> _tasks = [];

    public static void CreateDB(string connectionString, string dbName, string user, string pass)
    {
        using (logger.LogExecTiming("Creating Database"))
        {
            using var conn = new NpgsqlConnection(connectionString);

            lock (_lock)
            {
                conn.Open();

                var text = DatabaseDDL;
                text = text.Replace("$$DBNAME$$", dbName);
                text = text.Replace("$$TESTUSER$$", user);
                text = text.Replace("$$TESTPASS$$", pass);

                foreach (var s in Regex.Split(text, @"^\$\$SEP\$\$\s*$", RegexOptions.Multiline))
                {
                    if (string.IsNullOrWhiteSpace(s))
                        continue;
                    logger.Trace($"Executing {s}");
                    conn.Execute(s);
                }

                conn.Close();
            }
        }
    }

    public static void DeleteDatabase(string connectionString, string dbName)
    {
        _tasks.Add(Task.Run(() =>
        {
            using var conn = new NpgsqlConnection(connectionString);

            lock (_lock)
            {
                conn.Open();

                conn.Execute($"DROP DATABASE {dbName} WITH (FORCE);");

                conn.Close();
            }
        }));
    }

    public static async Task WaitForDeletes()
    {
        using (logger.LogExecTiming("Waiting for deletes"))
        {
            await Task.WhenAll(_tasks.ToArray());
        }
    }

    private const string DatabaseDDL = $$"""
DO
$do$
BEGIN
    IF (EXISTS (SELECT FROM pg_database WHERE datname = '$$DBNAME$$')) THEN
        DROP DATABASE $$DBNAME$$ WITH (FORCE);
    END IF;
END
$do$

$$SEP$$

CREATE DATABASE $$DBNAME$$;

$$SEP$$

DO
$do$
BEGIN
    IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles
      WHERE rolname = '$$TESTUSER$$') THEN

      CREATE ROLE $$TESTUSER$$ LOGIN PASSWORD '$$TESTPASS$$';
    END IF;
END
$do$

$$SEP$$

GRANT ALL PRIVILEGES ON DATABASE $$DBNAME$$ TO $$TESTUSER$$;
""";

}
