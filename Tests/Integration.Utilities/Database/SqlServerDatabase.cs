using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using System.Text.RegularExpressions;

namespace Integration.Utilities.Database;

public static class SqlServerDatabase
{
    private static readonly ILogger logger = StaticLogManager.GetLogger();

    private static readonly object _lock = new object();
    private static readonly List<Task> _tasks = [];

    public static void CreateDB(string connectionString, string dbName, string user, string pass)
    {
        using (logger.LogExecTiming("Creating Database"))
        {
            using var conn = new SqlConnection(connectionString);

            lock (_lock)
            {
                conn.Open();

                var text = DatabaseDDL;
                text = text.Replace("$$DBNAME$$", dbName);
                text = text.Replace("$$TESTUSER$$", user);
                text = text.Replace("$$TESTPASS$$", pass);

                foreach (var s in Regex.Split(text, @"^GO\s*$", RegexOptions.Multiline))
                {
                    if (string.IsNullOrWhiteSpace(s))
                        continue;
                    logger.Trace($"Executing {s}");
                    conn.Execute(s);
                }

                conn.Close();
            }
        }
    }

    public static void DeleteDatabase(string connectionString, string dbName)
    {
        _tasks.Add(Task.Run(() =>
        {
            using var conn = new SqlConnection(connectionString);

            lock (_lock)
            {
                conn.Open();

                conn.Execute($"ALTER DATABASE [{dbName}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;");
                conn.Execute($"DROP DATABASE [{dbName}]");

                conn.Close();
            }
        }));
    }

    public static async Task WaitForDeletes()
    {
        using (logger.LogExecTiming("Waiting for deletes"))
        {
            await Task.WhenAll(_tasks.ToArray());
        }
    }

    private const string DatabaseDDL = $$"""
IF (EXISTS (SELECT name FROM sys.databases WHERE name = '$$DBNAME$$'))
BEGIN
    ALTER DATABASE [$$DBNAME$$] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
    DROP DATABASE [$$DBNAME$$];
END
GO

CREATE DATABASE [$$DBNAME$$];
GO

IF NOT EXISTS (SELECT 1 FROM master.sys.server_principals WHERE [name] = N'$$TESTUSER$$')
BEGIN
    CREATE LOGIN [$$TESTUSER$$] WITH PASSWORD = '$$TESTPASS$$'
END

USE [$$DBNAME$$]
GO

IF NOT EXISTS (SELECT 1 FROM sys.database_principals WHERE [name] = N'$$TESTUSER$$')
BEGIN
    CREATE USER [$$TESTUSER$$] FOR LOGIN [$$TESTUSER$$];
END

GO
EXEC sp_addrolemember N'db_owner', N'$$TESTUSER$$';

IF NOT EXISTS ( SELECT  *
            FROM    sys.schemas
            WHERE   name = N'$$SCHEMA$$' )
EXEC('CREATE SCHEMA [$$SCHEMA$$]');
GO
""";

}
