using Microsoft.AspNetCore;
using Plinth.WindowsService;
using Plinth.Logging.NLog;

[assembly: NUnit.Framework.NonTestAssembly]

namespace Tests.Plinth.WindowsService.AspNetCore;

public class Program
{
    private static readonly ILogger log = StaticLogManagerSetup.BasicNLogSetup();

    public static void Main(string[] args)
    {
        log.Debug("startup! {0}", Environment.Version);

        WindowsServiceRunner.RunWebHost(
            BuildWebHost,
            args,
            callbacks: new Dictionary<ServiceState, Action>()
            {
                [ServiceState.Starting] = () => log.Debug("Starting..."),
                [ServiceState.Started] = () => log.Debug("Started"),
                [ServiceState.Stopping] = () => log.Debug("Stopping..."),
                [ServiceState.Stopped] = () => log.Debug("Stopped"),
            });

        log.Debug("exit!");
    }

    public static IWebHost BuildWebHost(string[] args) =>
        WebHost.CreateDefaultBuilder(args)
            .UseStartup<Startup>()
            .UseContentRoot(WindowsServiceRunner.FindContentRoot())
            .ConfigureLogging(builder => builder.AddNLog())
            .CaptureStartupErrors(false)
            .Build();
}
