﻿namespace Tests.Plinth.WindowsService.AspNetCore;

public class Startup
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    // This method gets called by the runtime. Use this method to add services to the container.
    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
    public void ConfigureServices(/*IServiceCollection services*/)
    {
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }

        app.Run(async (context) =>
        {
            log.Debug($"request from {context.Connection.RemoteIpAddress} on port {context.Connection.LocalPort}");
            await context.Response.WriteAsync("Hello World!");
        });
    }
}
