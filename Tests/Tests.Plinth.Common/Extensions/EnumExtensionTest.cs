﻿using Plinth.Common.Extensions;
using NUnit.Framework;

namespace Tests.Plinth.Common.Extensions;

public enum TestEnum
{
    NoDescValue,

    [System.ComponentModel.Description("This is the description")]
    DescValue
}

[TestFixture]
public class EnumExtensionTest
{
    public enum SimpleEnum1
    {
        EnumVal1 = 0,
        EnumVal2 = 1,
        EnumVal3 = 2,
        EnumVal4 = 3,
        EnumValDefault = 4,
        EnumOnlyIn1 = 5
    }

    public enum SimpleEnum2
    {
        EnumVal1 = 5,
        EnumVal2 = 4,
        EnumVal3 = 3,
        EnumVal4 = 2,
        EnumValDefault = 1,
        EnumOnlyIn2 = 0
    }

    public enum SimpleEnum3
    {
        EnumVal,
        Enumval,
        Enum_val,
        enumVal,
        enumval,
        EnumVAL,
        ENUMVal,
        EnumVaL,
        EnUmVal,
        EnUmVaL
    }

    [Test]
    [TestCase(SimpleEnum3.EnumVal, "Enum Val")]
    [TestCase(SimpleEnum3.Enumval, "Enumval")]
    [TestCase(SimpleEnum3.Enum_val, "Enum_val")]
    [TestCase(SimpleEnum3.enumVal, "enum Val")]
    [TestCase(SimpleEnum3.EnumVAL, "Enum VAL")]
    [TestCase(SimpleEnum3.enumval, "enumval")]
    [TestCase(SimpleEnum3.ENUMVal, "ENUM Val")]
    [TestCase(SimpleEnum3.EnumVaL, "Enum Va L")]
    [TestCase(SimpleEnum3.EnUmVal, "En Um Val")]
    [TestCase(SimpleEnum3.EnUmVaL, "En Um Va L")]
    public void Test_SplitUppercaseWords(SimpleEnum3 enumVal, string expectedResult)
    {
        var enumAsSpaceSeparatedStr = enumVal.SplitUppercaseWords();
        Assert.That(enumAsSpaceSeparatedStr, Is.EqualTo(expectedResult));
    }

    [Test]
    public void Test_GetDesc()
    {
        Assert.That(TestEnum.NoDescValue.GetDescription(), Is.EqualTo("NoDescValue"));
        Assert.That(TestEnum.DescValue.GetDescription(), Is.EqualTo("This is the description"));
    }

    [Test]
    public void Test_ToEnum()
    {
        SimpleEnum1 e1 = SimpleEnum1.EnumVal1;
        Assert.That(e1.ToEnum<SimpleEnum2>(), Is.EqualTo(SimpleEnum2.EnumVal1));

        e1 = SimpleEnum1.EnumVal4;
        Assert.That(e1.ToEnum<SimpleEnum2>(), Is.EqualTo(SimpleEnum2.EnumVal4));

        SimpleEnum2 e2 = SimpleEnum2.EnumVal2;
        Assert.That(e2.ToEnum<SimpleEnum1>(), Is.EqualTo(SimpleEnum1.EnumVal2));

        e2 = SimpleEnum2.EnumVal3;
        Assert.That(e2.ToEnum<SimpleEnum1>(), Is.EqualTo(SimpleEnum1.EnumVal3));

        Assert.Throws<ArgumentException>(() => SimpleEnum1.EnumOnlyIn1.ToEnum<SimpleEnum2>());
        Assert.Throws<ArgumentException>(() => SimpleEnum2.EnumOnlyIn2.ToEnum<SimpleEnum1>());
    }

    [Test]
    public void Test_ToEnumOrNull()
    {
        SimpleEnum1? e1 = null;
        Assert.That(e1!.ToEnumOrNull<SimpleEnum2>(), Is.Null);

        e1 = SimpleEnum1.EnumVal4;
        Assert.That(e1.ToEnumOrNull<SimpleEnum2>(), Is.EqualTo(SimpleEnum2.EnumVal4));

        SimpleEnum2? e2 = null;
        Assert.That(e2!.ToEnumOrNull<SimpleEnum1>(), Is.Null);

        e2 = SimpleEnum2.EnumVal3;
        Assert.That(e2.ToEnumOrNull<SimpleEnum1>(), Is.EqualTo(SimpleEnum1.EnumVal3));

        Assert.That(SimpleEnum1.EnumOnlyIn1.ToEnumOrNull<SimpleEnum2>(), Is.Null);
        Assert.That(SimpleEnum2.EnumOnlyIn2.ToEnumOrNull<SimpleEnum1>(), Is.Null);
    }

    [Test]
    public void Test_ToEnumOrDefault()
    {
        SimpleEnum1 e1 = SimpleEnum1.EnumVal1;
        Assert.That(e1.ToEnumOrDefault<SimpleEnum2>(SimpleEnum2.EnumVal4), Is.EqualTo(SimpleEnum2.EnumVal1));

        e1 = SimpleEnum1.EnumVal4;
        Assert.That(e1.ToEnumOrDefault<SimpleEnum2>(SimpleEnum2.EnumVal3), Is.EqualTo(SimpleEnum2.EnumVal4));

        SimpleEnum2 e2 = SimpleEnum2.EnumVal2;
        Assert.That(e2.ToEnumOrDefault<SimpleEnum1>(SimpleEnum1.EnumVal1), Is.EqualTo(SimpleEnum1.EnumVal2));

        e2 = SimpleEnum2.EnumVal3;
        Assert.That(e2.ToEnumOrDefault<SimpleEnum1>(SimpleEnum1.EnumVal4), Is.EqualTo(SimpleEnum1.EnumVal3));

        Assert.That(SimpleEnum1.EnumOnlyIn1.ToEnumOrDefault<SimpleEnum2>(SimpleEnum2.EnumOnlyIn2), Is.EqualTo(SimpleEnum2.EnumOnlyIn2));
        Assert.That(SimpleEnum2.EnumOnlyIn2.ToEnumOrDefault<SimpleEnum1>(SimpleEnum1.EnumOnlyIn1), Is.EqualTo(SimpleEnum1.EnumOnlyIn1));

        SimpleEnum1? e1q = SimpleEnum1.EnumVal1;
        Assert.That(e1q.ToEnumOrDefault<SimpleEnum2>(SimpleEnum2.EnumVal4), Is.EqualTo(SimpleEnum2.EnumVal1));

        e1q = null;
        Assert.That(e1q!.ToEnumOrDefault<SimpleEnum2>(SimpleEnum2.EnumVal4), Is.EqualTo(SimpleEnum2.EnumVal4));

        e1q = SimpleEnum1.EnumVal4;
        Assert.That(e1q.ToEnumOrDefault<SimpleEnum2>(SimpleEnum2.EnumVal3), Is.EqualTo(SimpleEnum2.EnumVal4));

        SimpleEnum2? e2q = SimpleEnum2.EnumVal2;
        Assert.That(e2q.ToEnumOrDefault<SimpleEnum1>(SimpleEnum1.EnumVal1), Is.EqualTo(SimpleEnum1.EnumVal2));

        e2q = null;
        Assert.That(e2q!.ToEnumOrDefault<SimpleEnum1>(SimpleEnum1.EnumVal1), Is.EqualTo(SimpleEnum1.EnumVal1));

        e2q = SimpleEnum2.EnumVal3;
        Assert.That(e2q.ToEnumOrDefault<SimpleEnum1>(SimpleEnum1.EnumVal4), Is.EqualTo(SimpleEnum1.EnumVal3));
    }
}
