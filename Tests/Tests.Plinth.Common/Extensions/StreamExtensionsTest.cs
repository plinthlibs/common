using Plinth.Common.Extensions;
using NUnit.Framework;
using System.Text;
using System.Reflection;

namespace Tests.Plinth.Common.Extensions;

[TestFixture]
public class StreamExtensionsTest
{
    [Test]
    public async Task TestNull()
    {
        Stream? s = null;
        Assert.That(s.ToBytes(), Is.Null);
        Assert.That(s.ToBytes(25), Is.Null);

        Assert.That(await s.ToBytesAsync(), Is.Null);
        Assert.That(await s.ToBytesAsync(25), Is.Null);
    }

    [Test]
    public async Task TestMemoryStreamToBytes()
    {
        const string text = "This is a test";
        var bytes = Encoding.ASCII.GetBytes(text);
        using (var stream = new MemoryStream(bytes))
        {
            var toBytes = stream.ToBytes();
            Assert.That(toBytes, Is.EqualTo(bytes));
            Assert.That(Encoding.ASCII.GetString(toBytes!), Is.EqualTo(text));
        }

        using (var stream = new MemoryStream(bytes))
        {
            var toBytes = await stream.ToBytesAsync();
            Assert.That(toBytes, Is.EqualTo(bytes));
            Assert.That(Encoding.ASCII.GetString(toBytes!), Is.EqualTo(text));
        }
    }

    [Test]
    [TestCase(256)]
    [TestCase(1)]
    [TestCase(0)]
    public async Task TestMemoryStreamToBytes_Length(int length)
    {
        const string text = "This is a test";
        var bytes = Encoding.ASCII.GetBytes(text);
        using (var stream = new MemoryStream(bytes))
        {
            var toBytes = stream.ToBytes(length);
            Assert.That(toBytes, Is.EqualTo(bytes).AsCollection);
            Assert.That(Encoding.ASCII.GetString(toBytes!), Is.EqualTo(text));
        }

        using (var stream = new MemoryStream(bytes))
        {
            var toBytes = await stream.ToBytesAsync(length);
            Assert.That(toBytes, Is.EqualTo(bytes).AsCollection);
            Assert.That(Encoding.ASCII.GetString(toBytes!), Is.EqualTo(text));
        }
    }

    [Test]
    [TestCase(256)]
    [TestCase(1)]
    [TestCase(0)]
    [TestCase(-1)]
    public void TestFileStreamToBytes(int length)
    {
        string filename = Path.Combine(new FileInfo(Assembly.GetAssembly(typeof(StreamExtensionsTest))!.Location).Directory!.FullName, "TestFileStreamToBytes.txt");
        const string text = "This is a test";
        try
        {
            File.WriteAllText(filename, text);
            var bytes = Encoding.ASCII.GetBytes(text);
            using var stream = new FileStream(filename, FileMode.Open);
            var toBytes = length >= 0 ? stream.ToBytes(length) : stream.ToBytes();
            Assert.That(toBytes, Is.EqualTo(bytes).AsCollection);
            Assert.That(Encoding.ASCII.GetString(toBytes!), Is.EqualTo(text));
        }
        finally
        {
            if (File.Exists(filename))
                File.Delete(filename);
        }
    }

    [Test]
    [TestCase(256)]
    [TestCase(1)]
    [TestCase(0)]
    [TestCase(-1)]
    public async Task TestFileStreamToBytesASync(int length)
    {
        string filename = Path.Combine(new FileInfo(Assembly.GetAssembly(typeof(StreamExtensionsTest))!.Location).Directory!.FullName, "TestFileStreamToBytesAsync.txt");
        const string text = "This is a test";
        try
        {
            File.WriteAllText(filename, text);
            var bytes = Encoding.ASCII.GetBytes(text);
            using var stream = new FileStream(filename, FileMode.Open);
            var toBytes = length >= 0 ? await stream.ToBytesAsync(length) : await stream.ToBytesAsync();
            Assert.That(toBytes, Is.EqualTo(bytes).AsCollection);
            Assert.That(Encoding.ASCII.GetString(toBytes!), Is.EqualTo(text));
        }
        finally
        {
            if (File.Exists(filename))
                File.Delete(filename);
        }
    }

    [Test]
    public void TestCloseStream()
    {
        const string text = "This is a test";
        var bytes = Encoding.ASCII.GetBytes(text);
        using var stream = new MemoryStream(bytes);
        var toBytes = stream.ToBytes();
        Assert.That(toBytes, Is.EqualTo(bytes));
        Assert.That(Encoding.ASCII.GetString(toBytes!), Is.EqualTo(text));

        Assert.That(stream.CanRead, Is.False);
    }

    [Test]
    public void TestLeaveStreamOpen()
    {
        const string text = "This is a test";
        var bytes = Encoding.ASCII.GetBytes(text);
        using var stream = new MemoryStream(bytes);
        var toBytes = stream.ToBytes(leaveStreamOpen: true);
        Assert.That(toBytes, Is.EqualTo(bytes));
        Assert.That(Encoding.ASCII.GetString(toBytes!), Is.EqualTo(text));

        Assert.That(stream.CanRead);
    }
}
