using NUnit.Framework;

namespace Tests.Plinth.Common.Collection;

[TestFixture]
public class EnumerableExtensionsTest
{
    [Test]
    public void IsNullOrEmpty_EmptyAndNullList_ReturnsTrue()
    {
        List<string>? NullList = null;
        Assert.That(NullList.IsNullOrEmpty());

        var EmptyList = new List<string>();
        Assert.That(EmptyList.IsNullOrEmpty());
    }

    [Test]
    public void IsNullOrEmpty_NonEmptyList_ReturnsFalse()
    {
        var NonEmptyList = new List<int>() { 1, 2 };
        Assert.That(NonEmptyList.IsNullOrEmpty(), Is.False);
    }

    [Test]
    public void Many_NullPredicate_Fails()
    {
        Assert.Throws<ArgumentNullException>(() => new List<int>().Many(null!));
    }

    [Test]
    public void Many_ReturnsFalse()
    {
        List<string>? NullList = null;
        Assert.That(NullList.Many(), Is.False);

        var EmptyList = new List<string>();
        Assert.That(EmptyList.Many(), Is.False);

        var OneElementList = new List<int>() { 1 };
        Assert.That(OneElementList.Many(), Is.False);

        var TwoElementList = new List<int>() { 1, 2 };
        Assert.That(TwoElementList.Many(v => v == 1), Is.False);

        Assert.That(NullList!.Many(v => v == "1"), Is.False);
    }

    [Test]
    public void Many_ReturnsTrue()
    {
        var TwoElementList = new List<int>() { 1, 2 };
        Assert.That(TwoElementList.Many());

        var ThreeElementList = new List<int>() { 1, 2, 3 };
        Assert.That(ThreeElementList.Many());
        Assert.That(ThreeElementList.Many(v => v > 1));
    }

    private readonly AsyncLocal<bool> _isMaterialized = new();

    [Test]
    public void IsNullOrEmpty_Materialized()
    {
        // we take 10, so there is something to materialize
        _isMaterialized.Value = false;
        Assert.That(ValGenerator(5).Take(10).IsNullOrEmpty(), Is.False);
        Assert.That(_isMaterialized.Value);

        // we take 0, so there is nothing to materialize
        _isMaterialized.Value = false;
        Assert.That(ValGenerator(5).Take(0).IsNullOrEmpty());
        Assert.That(_isMaterialized.Value, Is.False);
    }

    private IEnumerable<int> ValGenerator(int start)
    {
        _isMaterialized.Value = true;
        int v = start;
        while (true)
        {
            v += 2;
            yield return v;
        }
    }
}
