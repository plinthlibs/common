﻿using Plinth.Common.Extensions;
using NUnit.Framework;
using System.Data;

namespace Tests.Plinth.Common.Extensions;

[TestFixture]
public class DataRowExtensionsTest
{
    public DataTable DataTable
    {
        get
        {
            var dt = new DataTable();
            dt.Columns.Add(new DataColumn("int", typeof(int)));
            dt.Columns.Add(new DataColumn("string", typeof(string)));
            dt.Columns.Add(new DataColumn("bool", typeof(bool)));
            dt.Columns.Add(new DataColumn("decimal", typeof(decimal)));
            dt.Columns.Add(new DataColumn("null", typeof(string)));

            var row = dt.NewRow();
            row["int"] = 1;
            row["string"] = "str";
            row["bool"] = true;
            row["decimal"] = 5m;
            row["null"] = null;
            dt.Rows.Add(row);

            return dt;
        }
    }

    [Test]
    public void Test_DataRowExtensions_AsString()
    {
        var dt = DataTable;
        var row = dt.Rows[0];

        // Get row values
        Assert.That(row.AsString("int"), Is.EqualTo("1"));
        Assert.That(row.AsString("string"), Is.EqualTo("str"));
        Assert.That(row.AsString("bool"), Is.EqualTo("True"));
        Assert.That(row.AsString("decimal"), Is.EqualTo("5"));

        // Nonexistent column
        Assert.That(row.AsString("asdf"), Is.EqualTo(string.Empty));
        Assert.That(row.AsString("asdf", "default"), Is.EqualTo("default"));

        // Null value
        Assert.That(row.AsString("null"), Is.EqualTo(string.Empty));
        Assert.That(row.AsString("null", "default"), Is.EqualTo("default"));
    }

    [Test]
    public void Test_DataRowExtensions_AsBool()
    {
        var dt = DataTable;
        var row = dt.Rows[0];

        // Get row values
        Assert.That(row.AsBool("int"), Is.EqualTo(false));
        Assert.That(row.AsBool("string"), Is.EqualTo(false));
        Assert.That(row.AsBool("bool"), Is.EqualTo(true));
        Assert.That(row.AsBool("decimal"), Is.EqualTo(false));

        Assert.That(row.AsBool("decimal", true), Is.EqualTo(true));

        // Nonexistent column
        Assert.That(row.AsBool("asdf"), Is.EqualTo(false));
        Assert.That(row.AsBool("asdf", true), Is.EqualTo(true));

        // Null value
        Assert.That(row.AsBool("null"), Is.EqualTo(false));
        Assert.That(row.AsBool("null", true), Is.EqualTo(true));

        // Get row values
        Assert.That(row.AsBoolNull("int"), Is.EqualTo(null));
        Assert.That(row.AsBoolNull("string"), Is.EqualTo(null));
        Assert.That(row.AsBoolNull("bool"), Is.EqualTo(true));
        Assert.That(row.AsBoolNull("decimal"), Is.EqualTo(null));

        // Nonexistent column
        Assert.That(row.AsBoolNull("asdf"), Is.EqualTo(null));

        // Null value
        Assert.That(row.AsBoolNull("null"), Is.EqualTo(null));
    }

    [Test]
    public void Test_DataRowExtensions_AsDecimal()
    {
        var dt = DataTable;
        var row = dt.Rows[0];

        // Get row values
        Assert.That(row.AsDecimal("int"), Is.EqualTo(1m));
        Assert.That(row.AsDecimal("string"), Is.EqualTo(0m));
        Assert.That(row.AsDecimal("bool"), Is.EqualTo(0m));
        Assert.That(row.AsDecimal("decimal"), Is.EqualTo(5m));

        Assert.That(row.AsDecimal("decimal", 2m), Is.EqualTo(5m));
        Assert.That(row.AsDecimal("bool", 2m), Is.EqualTo(2m));

        // Nonexistent column
        Assert.That(row.AsDecimal("asdf"), Is.EqualTo(0m));
        Assert.That(row.AsDecimal("asdf", 2m), Is.EqualTo(2m));

        // Null value
        Assert.That(row.AsDecimal("null"), Is.EqualTo(0m));
        Assert.That(row.AsDecimal("null", 1m), Is.EqualTo(1m));

        // Get row values
        Assert.That(row.AsDecimalNull("int"), Is.EqualTo(1m));
        Assert.That(row.AsDecimalNull("string"), Is.EqualTo(null));
        Assert.That(row.AsDecimalNull("bool"), Is.EqualTo(null));
        Assert.That(row.AsDecimalNull("decimal"), Is.EqualTo(5m));

        // Nonexistent column
        Assert.That(row.AsDecimalNull("asdf"), Is.EqualTo(null));

        // Null value
        Assert.That(row.AsDecimalNull("null"), Is.EqualTo(null));
    }

    [Test]
    public void Test_DataRowExtensions_AsInt()
    {
        var dt = DataTable;
        var row = dt.Rows[0];

        // Get row values
        Assert.That(row.AsInt("int"), Is.EqualTo(1));
        Assert.That(row.AsInt("string"), Is.EqualTo(0));
        Assert.That(row.AsInt("bool"), Is.EqualTo(0));
        Assert.That(row.AsInt("decimal"), Is.EqualTo(5));

        Assert.That(row.AsInt("int", 2), Is.EqualTo(1));
        Assert.That(row.AsInt("bool", 2), Is.EqualTo(2));

        // Nonexistent column
        Assert.That(row.AsInt("asdf"), Is.EqualTo(0));
        Assert.That(row.AsInt("asdf", 2), Is.EqualTo(2));

        // Null value
        Assert.That(row.AsInt("null"), Is.EqualTo(0));
        Assert.That(row.AsInt("null", 1), Is.EqualTo(1));

        // Get row values
        Assert.That(row.AsIntNull("int"), Is.EqualTo(1));
        Assert.That(row.AsIntNull("string"), Is.EqualTo(null));
        Assert.That(row.AsIntNull("bool"), Is.EqualTo(null));
        Assert.That(row.AsIntNull("decimal"), Is.EqualTo(5));

        // Nonexistent column
        Assert.That(row.AsIntNull("asdf"), Is.EqualTo(null));

        // Null value
        Assert.That(row.AsIntNull("null"), Is.EqualTo(null));

        // Get row values
        Assert.That(row.AsIntNullZero("int"), Is.EqualTo(1));
        Assert.That(row.AsIntNullZero("string"), Is.EqualTo(null));
        Assert.That(row.AsIntNullZero("bool"), Is.EqualTo(null));
        Assert.That(row.AsIntNullZero("decimal"), Is.EqualTo(5));

        // Nonexistent column
        Assert.That(row.AsIntNullZero("asdf"), Is.EqualTo(null));

        // Null value
        Assert.That(row.AsIntNullZero("null"), Is.EqualTo(null));

        row["int"] = 0;
        Assert.That(row.AsIntNullZero("int"), Is.EqualTo(null));
    }
}
