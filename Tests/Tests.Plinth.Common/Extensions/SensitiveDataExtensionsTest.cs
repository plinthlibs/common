using Plinth.Common.Extensions;
using NUnit.Framework;

namespace Tests.Plinth.Common.Utils;

[TestFixture]
public class SensitiveDataExtensionsTest
{
    #region mask all
    [Test]
    public void Test_MaskAllJsonValues()
    {
        string s = "{ \"borrower\": \"buddy\", \"cosigner\": \"buddy's buddy\", \"primary_phone\": \"123-435-6678\", \"work_phone\": \"1234356678\" }";

        Assert.That(s.MaskJsonValues(), Is.EqualTo("{ \"borrower\": \"_REDACTED_\", \"cosigner\": \"_REDACTED_\", \"primary_phone\": \"_REDACTED_\", \"work_phone\": \"_REDACTED_\" }"));
    }

    [Test]
    public void Test_MaskJsonValuesWithKeyFormat()
    {
        string s = "{ \"borrower\": \"buddy\", \"cosigner\": \"buddy's buddy\", \"primary_phone\": \"123-435-6678\", \"work_phone\": \"1234356678\" }";

        Assert.That(s.MaskJsonValues("primary_phone"), Is.EqualTo("{ \"borrower\": \"buddy\", \"cosigner\": \"buddy's buddy\", \"primary_phone\": \"_REDACTED_\", \"work_phone\": \"1234356678\" }"));
        Assert.That(s.MaskJsonValues("borrower"), Is.EqualTo("{ \"borrower\": \"_REDACTED_\", \"cosigner\": \"buddy's buddy\", \"primary_phone\": \"123-435-6678\", \"work_phone\": \"1234356678\" }"));
        Assert.That(s.MaskJsonValues("[a-zA-Z]*er"), Is.EqualTo("{ \"borrower\": \"_REDACTED_\", \"cosigner\": \"_REDACTED_\", \"primary_phone\": \"123-435-6678\", \"work_phone\": \"1234356678\" }"));
    }

    [Test]
    public void Test_MaskJsonValuesWithValueFormat()
    {
        string s = "{ \"borrower\": \"buddy\", \"phone\": \"123-435-6678\", \"phone\": \"1234356678\" }";

        Assert.That(s.MaskJsonValues("phone", "\\d{3}[\\-]\\d{3}[\\-]\\d{4}"), Is.EqualTo("{ \"borrower\": \"buddy\", \"phone\": \"_REDACTED_\", \"phone\": \"1234356678\" }"));
    }

    [Test]
    public void Test_MaskAllXmlValues()
    {
        string s =
            "<info>" +
            "   <borrower>buddy</borrower>" +
            "   <cosigner>buddy's buddy</cosigner>" +
            "   <primary_phone>123-435-6678</primary_phone>" +
            "   <work_phone>1234356678</work_phone>" +
            "</info>";

        Assert.That(
            s.MaskXmlValues(), Is.EqualTo("<info>" +
            "   <borrower>_REDACTED_</borrower>" +
            "   <cosigner>_REDACTED_</cosigner>" +
            "   <primary_phone>_REDACTED_</primary_phone>" +
            "   <work_phone>_REDACTED_</work_phone>" +
            "</info>"));
    }

    [Test]
    public void Test_MaskXmlValuesWithKeyFormat()
    {
        string s =
            "<info>" +
            "   <borrower>buddy</borrower>" +
            "   <cosigner>buddy's buddy</cosigner>" +
            "   <primary_phone>123-435-6678</primary_phone>" +
            "   <work_phone>1234356678</work_phone>" +
            "</info>";

        Assert.That(
            s.MaskXmlValues("primary_phone"), Is.EqualTo("<info>" +
            "   <borrower>buddy</borrower>" +
            "   <cosigner>buddy's buddy</cosigner>" +
            "   <primary_phone>_REDACTED_</primary_phone>" +
            "   <work_phone>1234356678</work_phone>" +
            "</info>"));

        Assert.That(
            s.MaskXmlValues("borrower"), Is.EqualTo("<info>" +
            "   <borrower>_REDACTED_</borrower>" +
            "   <cosigner>buddy's buddy</cosigner>" +
            "   <primary_phone>123-435-6678</primary_phone>" +
            "   <work_phone>1234356678</work_phone>" +
            "</info>"));

        Assert.That(
            s.MaskXmlValues("[a-zA-Z]*er"), Is.EqualTo("<info>" +
            "   <borrower>_REDACTED_</borrower>" +
            "   <cosigner>_REDACTED_</cosigner>" +
            "   <primary_phone>123-435-6678</primary_phone>" +
            "   <work_phone>1234356678</work_phone>" +
            "</info>"));
    }

    [Test]
    public void Test_MaskXmlValuesWithValueFormat()
    {
        string s =
            "<info>" +
            "   <borrower>buddy</borrower>" +
            "   <phone>123-435-6678</phone>" +
            "   <phone>1234356678</phone>" +
            "</info>";

        Assert.That(
            s.MaskXmlValues("phone", "\\d{3}[\\-]\\d{3}[\\-]\\d{4}"), Is.EqualTo("<info>" +
            "   <borrower>buddy</borrower>" +
            "   <phone>_REDACTED_</phone>" +
            "   <phone>1234356678</phone>" +
            "</info>"));
    }

    [Test]
    public void Test_MaskAllQueryStringValues()
    {
        string s = "www.Plinth.com?borrower=buddy&cosigner=buddy%27s%20buddy&primary_phone=123-435-6678&work_phone=1234356678";

        Assert.That(s.MaskQueryStringValues(), Is.EqualTo("www.Plinth.com?borrower=_REDACTED_&cosigner=_REDACTED_&primary_phone=_REDACTED_&work_phone=_REDACTED_"));
    }

    [Test]
    public void Test_MaskQueryStringValuesWithKeyFormat()
    {
        string s = "www.Plinth.com?borrower=buddy&cosigner=buddy%27s%20buddy&primary_phone=123-435-6678&work_phone=1234356678";

        Assert.That(s.MaskQueryStringValues("primary_phone"), Is.EqualTo("www.Plinth.com?borrower=buddy&cosigner=buddy%27s%20buddy&primary_phone=_REDACTED_&work_phone=1234356678"));
        Assert.That(s.MaskQueryStringValues("borrower"), Is.EqualTo("www.Plinth.com?borrower=_REDACTED_&cosigner=buddy%27s%20buddy&primary_phone=123-435-6678&work_phone=1234356678"));
        Assert.That(s.MaskQueryStringValues("[a-zA-Z]*er"), Is.EqualTo("www.Plinth.com?borrower=_REDACTED_&cosigner=_REDACTED_&primary_phone=123-435-6678&work_phone=1234356678"));
    }

    [Test]
    public void Test_MaskQueryStringValuesWithValueFormat()
    {
        string s = "www.Plinth.com?borrower=buddy&phone=123-435-6678&phone=1234356678";

        Assert.That(s.MaskQueryStringValues("phone", "\\d{3}[\\-]\\d{3}[\\-]\\d{4}"), Is.EqualTo("www.Plinth.com?borrower=buddy&phone=_REDACTED_&phone=1234356678"));
    }
    #endregion

    #region ssn
    [Test]
    public void Test_MaskJsonSsn()
    {
        string s = "{ \"borrower\": \"buddy\", \"ssn\": \"123-45-6678\" }";
        Assert.That(s.MaskSsn(), Is.EqualTo("{ \"borrower\": \"buddy\", \"ssn\": \"_REDACTED_\" }"));

        s = "{ \"ssnab\": \"buddy\", \"Ssn\":\"123456789\" }";
        Assert.That(s.MaskSsn(), Is.EqualTo("{ \"ssnab\": \"buddy\", \"Ssn\":\"_REDACTED_\" }"));

        s = "{ \"ssn\": \"123-45-6789\", \"Ssn\":\"123456789\" }";
        Assert.That(s.MaskSsn(), Is.EqualTo("{ \"ssn\": \"_REDACTED_\", \"Ssn\":\"_REDACTED_\" }"));

        s = "<noField>11</noField>";
        Assert.That(s.MaskSsn(), Is.EqualTo("<noField>11</noField>"));
    }

    [Test]
    public void Test_MaskXmlSsn()
    {
        string s =
            "<info>" +
            "   <ssn>111-22-3344</ssn>" +
            "   <phone>1234356678</phone>" +
            "   <ssn>111-22-3344</ssn>" +
            "</info>";

        Assert.That(
            s.MaskSsn(), Is.EqualTo("<info>" +
            "   <ssn>_REDACTED_</ssn>" +
            "   <phone>1234356678</phone>" +
            "   <ssn>_REDACTED_</ssn>" +
            "</info>"));

        s =
            "<info>" +
            "   <ssnRule>111-22-3344</ssn>" +
            "   <phone>1234356678</phone>" +
            "   <ssn>111-22-3344</ssn>" +
            "</info>";

        Assert.That(
            s.MaskSsn(), Is.EqualTo("<info>" +
            "   <ssnRule>111-22-3344</ssn>" +
            "   <phone>1234356678</phone>" +
            "   <ssn>_REDACTED_</ssn>" +
            "</info>"));
    }

    [Test]
    public void Test_MaskQueryStringSsn()
    {
        string s = "www.Plinth.com?borrower=buddy&ssn=123-45-6678";
        Assert.That(s.MaskSsn(), Is.EqualTo("www.Plinth.com?borrower=buddy&ssn=_REDACTED_"));

        s = "www.Plinth.com?ssnRule=buddy&ssn=123-45-6678";
        Assert.That(s.MaskSsn(), Is.EqualTo("www.Plinth.com?ssnRule=buddy&ssn=_REDACTED_"));

        s = "www.Plinth.com?ssnRule=buddy&ssn=123-45-6678&a=b&ssn=098-12-5678";
        Assert.That(s.MaskSsn(), Is.EqualTo("www.Plinth.com?ssnRule=buddy&ssn=_REDACTED_&a=b&ssn=_REDACTED_"));
    }

    [Test]
    public void Test_MaskSsnCase()
    {
        string s = "{ \"borrower\": \"buddy\", \"Ssn\":\"123456789\" }";
        Assert.That(s.MaskSsn(), Is.EqualTo("{ \"borrower\": \"buddy\", \"Ssn\":\"_REDACTED_\" }"));
    }

    [Test]
    public void Test_MaskSsn_EndOfString()
    {
        Assert.That("\"ssn\"".MaskSsn(), Is.EqualTo("\"ssn\""));
        Assert.That("<ssn>".MaskSsn(), Is.EqualTo("<ssn>"));
        Assert.That("ssn=".MaskSsn(), Is.EqualTo("ssn="));

        Assert.That("\"ssn".MaskSsn(), Is.EqualTo("\"ssn"));
        Assert.That("<ssn".MaskSsn(), Is.EqualTo("<ssn"));
        Assert.That("ssn".MaskSsn(), Is.EqualTo("ssn"));
    }
    #endregion

    #region password
    [Test]
    public void Test_MaskJsonPassword()
    {
        string s = "{ \"borrower\": \"buddy\", \"password\": \"abc123\", \"password\": \"123\" }";
        Assert.That(s.MaskPassword(), Is.EqualTo("{ \"borrower\": \"buddy\", \"password\": \"_REDACTED_\", \"password\": \"_REDACTED_\" }"));

        s = "{ \"PasswordRule\": \"buddy\", \"password\": \"abc123\" }";
        Assert.That(s.MaskPassword(), Is.EqualTo("{ \"PasswordRule\": \"buddy\", \"password\": \"_REDACTED_\" }"));

        s = "<noField>11</noField>";
        Assert.That(s.MaskPassword(), Is.EqualTo("<noField>11</noField>"));
    }

    [Test]
    public void Test_MaskXmlPassword()
    {
        string s =
            "<info>" +
            "   <password>Plinth123</password>" +
            "   <phone>1234356678</phone>" +
            "   <password>Plinth123</password>" +
            "   <userPassword>Plinth123</userPassword>" +
            "</info>";

        Assert.That(
            s.MaskPassword(), Is.EqualTo("<info>" +
            "   <password>_REDACTED_</password>" +
            "   <phone>1234356678</phone>" +
            "   <password>_REDACTED_</password>" +
            "   <userPassword>_REDACTED_</userPassword>" +
            "</info>"));

        s =
            "<info>" +
            "   <passwordRule>Plinth123</passwordRule>" +
            "   <phone>1234356678</phone>" +
            "   <password>Plinth123</password>" +
            "</info>";

        Assert.That(
            s.MaskPassword(), Is.EqualTo("<info>" +
            "   <passwordRule>Plinth123</passwordRule>" +
            "   <phone>1234356678</phone>" +
            "   <password>_REDACTED_</password>" +
            "</info>"));
    }

    [Test]
    public void Test_MaskQueryStringPassword()
    {
        string s = "www.Plinth.com?borrower=buddy&password=Plinth123";
        Assert.That(s.MaskPassword(), Is.EqualTo("www.Plinth.com?borrower=buddy&password=_REDACTED_"));

        s = "www.Plinth.com?passwordRule=buddy&password=Plinth123";
        Assert.That(s.MaskPassword(), Is.EqualTo("www.Plinth.com?passwordRule=buddy&password=_REDACTED_"));

        s = "www.Plinth.com?passwordRule=buddy&password=Plinth123&a=b&password=123";
        Assert.That(s.MaskPassword(), Is.EqualTo("www.Plinth.com?passwordRule=buddy&password=_REDACTED_&a=b&password=_REDACTED_"));
    }

    [Test]
    public void Test_MaskPrefixedPassword()
    {
        string s = "{ \"borrower\": \"buddy\", \"newPassword\": \"abc123\", \"oldPassword\": \"123\" }";
        Assert.That(s.MaskPassword(), Is.EqualTo("{ \"borrower\": \"buddy\", \"newPassword\": \"_REDACTED_\", \"oldPassword\": \"_REDACTED_\" }"));

        s = "{ \"newPasswordRule\": \"buddy\", \"newPassword\": \"abc123\" }";
        Assert.That(s.MaskPassword(), Is.EqualTo("{ \"newPasswordRule\": \"buddy\", \"newPassword\": \"_REDACTED_\" }"));
    }

    [Test]
    public void Test_MaskPasswordCase()
    {
        string s = "{ \"borrower\": \"buddy\", \"PaSsWoRd\": \"abc123\" }";
        Assert.That(s.MaskPassword(), Is.EqualTo("{ \"borrower\": \"buddy\", \"PaSsWoRd\": \"_REDACTED_\" }"));

        s = "{ \"PaSsWoRdRule\": \"buddy\", \"PaSsWoRd\": \"abc123\" }";
        Assert.That(s.MaskPassword(), Is.EqualTo("{ \"PaSsWoRdRule\": \"buddy\", \"PaSsWoRd\": \"_REDACTED_\" }"));
    }

    [Test]
    public void Test_MaskPassword_EndOfString()
    {
        Assert.That("\"password\"".MaskPassword(), Is.EqualTo("\"password\""));
        Assert.That("<password>".MaskPassword(), Is.EqualTo("<password>"));
        Assert.That("password=".MaskPassword(), Is.EqualTo("password="));

        Assert.That("\"password".MaskPassword(), Is.EqualTo("\"password"));
        Assert.That("<password".MaskPassword(), Is.EqualTo("<password"));
        Assert.That("password".MaskPassword(), Is.EqualTo("password"));

        Assert.That("\"newPassword\"".MaskPassword(), Is.EqualTo("\"newPassword\""));
        Assert.That("<newPassword>".MaskPassword(), Is.EqualTo("<newPassword>"));
        Assert.That("newPassword=".MaskPassword(), Is.EqualTo("newPassword="));

        Assert.That("\"newPassword".MaskPassword(), Is.EqualTo("\"newPassword"));
        Assert.That("<newPassword".MaskPassword(), Is.EqualTo("<newPassword"));
        Assert.That("newPassword".MaskPassword(), Is.EqualTo("newPassword"));
    }
    #endregion

    #region card number
    [Test]
    public void Test_MaskJsonCreditCardNumber()
    {
        string s = "{ \"firstName\": \"buddy\", \"cardNumber\": \"4111111111111111\", \"cardNumber\": \"2\" }";
        Assert.That(s.MaskCreditCardNumbers(), Is.EqualTo("{ \"firstName\": \"buddy\", \"cardNumber\": \"_REDACTED_\", \"cardNumber\": \"_REDACTED_\" }"));

        s = "{ \"cardNumberRule\": \"buddy\", \"cardNumber\": \"4111111111111111\" }";
        Assert.That(s.MaskCreditCardNumbers(), Is.EqualTo("{ \"cardNumberRule\": \"buddy\", \"cardNumber\": \"_REDACTED_\" }"));

        s = "<noField>11</noField>";
        Assert.That(s.MaskCreditCardNumbers(), Is.EqualTo("<noField>11</noField>"));
    }

    [Test]
    public void Test_MaskXmlCreditCardNumber()
    {
        string s =
            "<info>" +
            "   <cardNumber>Plinth123</cardNumber>" +
            "   <firstName>1234356678</firstName>" +
            "   <cardNumber>Plinth123</cardNumber>" +
            "   <creditCardNumber>Plinth123</creditCardNumber>" +
            "</info>";

        Assert.That(
            s.MaskCreditCardNumbers(), Is.EqualTo("<info>" +
            "   <cardNumber>_REDACTED_</cardNumber>" +
            "   <firstName>1234356678</firstName>" +
            "   <cardNumber>_REDACTED_</cardNumber>" +
            "   <creditCardNumber>_REDACTED_</creditCardNumber>" +
            "</info>"));

        s =
            "<info>" +
            "   <cardNumberRule>Plinth123</cardNumberRule>" +
            "   <firstName>1234356678</firstName>" +
            "   <cardNumber>Plinth123</cardNumber>" +
            "</info>";

        Assert.That(
            s.MaskCreditCardNumbers(), Is.EqualTo("<info>" +
            "   <cardNumberRule>Plinth123</cardNumberRule>" +
            "   <firstName>1234356678</firstName>" +
            "   <cardNumber>_REDACTED_</cardNumber>" +
            "</info>"));
    }

    [Test]
    public void Test_MaskQueryStringCreditCardNumber()
    {
        string s = "www.Plinth.com?firstName=buddy&cardNumber=Plinth123";
        Assert.That(s.MaskCreditCardNumbers(), Is.EqualTo("www.Plinth.com?firstName=buddy&cardNumber=_REDACTED_"));

        s = "www.Plinth.com?cardNumberRule=buddy&cardNumber=Plinth123";
        Assert.That(s.MaskCreditCardNumbers(), Is.EqualTo("www.Plinth.com?cardNumberRule=buddy&cardNumber=_REDACTED_"));

        s = "www.Plinth.com?cardNumberRule=buddy&cardNumber=Plinth123&a=b&cardNumber=Plinth123";
        Assert.That(s.MaskCreditCardNumbers(), Is.EqualTo("www.Plinth.com?cardNumberRule=buddy&cardNumber=_REDACTED_&a=b&cardNumber=_REDACTED_"));
    }

    [Test]
    public void Test_MaskPrefixedCreditCardNumber()
    {
        string s = "{ \"firstName\": \"buddy\", \"creditCardNumber\": \"8756756785674\", \"oldCardNumber\": \"875674\" }";
        Assert.That(s.MaskCreditCardNumbers(), Is.EqualTo("{ \"firstName\": \"buddy\", \"creditCardNumber\": \"_REDACTED_\", \"oldCardNumber\": \"_REDACTED_\" }"));

        s = "{ \"creditCardNumberRule\": \"buddy\", \"creditCardNumber\": \"8756756785674\" }";
        Assert.That(s.MaskCreditCardNumbers(), Is.EqualTo("{ \"creditCardNumberRule\": \"buddy\", \"creditCardNumber\": \"_REDACTED_\" }"));
    }

    [Test]
    public void Test_MaskJsonCreditCardNumberCase()
    {
        string s = "{ \"firstName\": \"buddy\", \"cArDNuMbeR\": \"4111111111111111\" }";
        Assert.That(s.MaskCreditCardNumbers(), Is.EqualTo("{ \"firstName\": \"buddy\", \"cArDNuMbeR\": \"_REDACTED_\" }"));

        s = "{ \"cardNumberRule\": \"buddy\", \"cArDNuMbeR\": \"4111111111111111\" }";
        Assert.That(s.MaskCreditCardNumbers(), Is.EqualTo("{ \"cardNumberRule\": \"buddy\", \"cArDNuMbeR\": \"_REDACTED_\" }"));
    }

    [Test]
    public void Test_MaskCardnumber_EndOfString()
    {
        Assert.That("\"cardnumber\"".MaskCreditCardNumbers(), Is.EqualTo("\"cardnumber\""));
        Assert.That("<cardnumber>".MaskCreditCardNumbers(), Is.EqualTo("<cardnumber>"));
        Assert.That("cardnumber=".MaskCreditCardNumbers(), Is.EqualTo("cardnumber="));

        Assert.That("\"newCardnumber\"".MaskCreditCardNumbers(), Is.EqualTo("\"newCardnumber\""));
        Assert.That("<newCardnumber>".MaskCreditCardNumbers(), Is.EqualTo("<newCardnumber>"));
        Assert.That("newCardnumber=".MaskCreditCardNumbers(), Is.EqualTo("newCardnumber="));
    }
    #endregion

    #region sensitive fields
    [Test]
    public void Test_MaskJsonSensitiveFields()
    {
        string s = "{ \"borrower\": \"buddy\", \"ssn\": \"123-45-6678\", \"password\": \"Plinth123\", \"cardNumber\": \"4111111111111111\" }";
        Assert.That(s.MaskSensitiveFields(), Is.EqualTo("{ \"borrower\": \"buddy\", \"ssn\": \"_REDACTED_\", \"password\": \"_REDACTED_\", \"cardNumber\": \"_REDACTED_\" }"));

        s = "<noField>11</noField>";
        Assert.That(s.MaskSensitiveFields(), Is.EqualTo("<noField>11</noField>"));
    }

    [Test]
    public void Test_MaskXmlSensitiveFields()
    {
        string s =
            "<info>" +
            "   <ssn>111-22-3344</ssn>" +
            "   <phone>1234356678</phone>" +
            "   <password>Plinth123</password>" +
            "   <cardNumber>4111111111111111</cardNumber>" +
            "</info>";

        Assert.That(
            s.MaskSensitiveFields(), Is.EqualTo("<info>" +
            "   <ssn>_REDACTED_</ssn>" +
            "   <phone>1234356678</phone>" +
            "   <password>_REDACTED_</password>" +
            "   <cardNumber>_REDACTED_</cardNumber>" +
            "</info>"));
    }

    [Test]
    public void Test_MaskQueryStringSensitiveFields()
    {
        string s = "www.Plinth.com?borrower=buddy&ssn=123-45-6678&password=Plinth123&cardNumber=4111111111111111";
        Assert.That(s.MaskSensitiveFields(), Is.EqualTo("www.Plinth.com?borrower=buddy&ssn=_REDACTED_&password=_REDACTED_&cardNumber=_REDACTED_"));
    }
    #endregion
}
