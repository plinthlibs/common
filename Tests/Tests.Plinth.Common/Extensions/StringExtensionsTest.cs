using Plinth.Common.Extensions;
using NUnit.Framework;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Dynamic;

namespace Tests.Plinth.Common.Extensions;

[TestFixture]
public class StringExtensionsTest
{
    private enum SimpleEnum
    {
        EnumVal1,
        EnumVal2,
        EnumVal3,
        EnumVal4,
        EnumValDefault
    }

    [Flags]
    private enum FlagsEnum
    {
        EnumValNone = 0,
        EnumVal1 = 1,
        EnumVal2 = 2,
        EnumVal3 = 4,
        EnumVal4 = 8,
        EnumVal5 = 16
    }

    private enum AnnotatedEnum
    {
        Form1040 = 1040,
        [EnumMember(Value = "W2")]
        FormW2 = 1,
        [EnumMember(Value = "4506T")]
        Form4506T,
        [EnumMember(Value = "VENDOR-COMPLETE")]
        Complete
    }

    [Flags]
    private enum AnnotatedFlagsEnum
    {
        EnumValNone = 0,
        EnumVal1 = 1,
        [EnumMember(Value = "e2")]
        EnumVal2 = 2,
        EnumVal3 = 4,
        [EnumMember(Value = "E4")]
        EnumVal4 = 8,
        EnumVal5 = 16
    }

    private struct TestStruct
    {
        public int X = 0;

        public TestStruct()
        {
        }
    }

    private readonly string? snull = null;
    private readonly int int1 = 23321;
    private readonly float fl1 = 23321.54f;

    #region Enum Tests

    [Test]
    public void Test_ToEnum()
    {
        Assert.That("EnumVal1".ToEnum<SimpleEnum>(), Is.EqualTo(SimpleEnum.EnumVal1));
        Assert.That("EnumVal2".ToEnum<SimpleEnum>(), Is.EqualTo(SimpleEnum.EnumVal2));
        Assert.That("EnumVal3".ToEnum<SimpleEnum>(), Is.EqualTo(SimpleEnum.EnumVal3));
        Assert.That("EnumVal4".ToEnum<SimpleEnum>(), Is.EqualTo(SimpleEnum.EnumVal4));

        Assert.That("EnumVal2,EnumVal4".ToEnum<FlagsEnum>(), Is.EqualTo(FlagsEnum.EnumVal2 | FlagsEnum.EnumVal4));
        Assert.That("EnumVal1, EnumVal3".ToEnum<FlagsEnum>(), Is.EqualTo(FlagsEnum.EnumVal1 | FlagsEnum.EnumVal3));
    }

    [Test]
    public void Test_ToEnum_Invalid()
    {
        Assert.Throws<ArgumentException>(() => "NotAValidValue".ToEnum<SimpleEnum>());
        Assert.Throws<ArgumentNullException>(() => ((string?)null!).ToEnum<SimpleEnum>());
        Assert.Throws<ArgumentException>(() => "".ToEnum<SimpleEnum>());

        // this doesn't compile: "".ToEnum<int>()
        // this doesn't compile: "".ToEnum<TestStruct>()
    }

    [Test]
    public void Test_ToEnumOrDefault()
    {
        Assert.That("EnumVal1".ToEnumOrDefault<SimpleEnum>(SimpleEnum.EnumValDefault), Is.EqualTo(SimpleEnum.EnumVal1));
        Assert.That("EnumVal2".ToEnumOrDefault<SimpleEnum>(SimpleEnum.EnumValDefault), Is.EqualTo(SimpleEnum.EnumVal2));
        Assert.That("EnumVal3".ToEnumOrDefault<SimpleEnum>(SimpleEnum.EnumValDefault), Is.EqualTo(SimpleEnum.EnumVal3));
        Assert.That("EnumVal4".ToEnumOrDefault<SimpleEnum>(SimpleEnum.EnumValDefault), Is.EqualTo(SimpleEnum.EnumVal4));

        Assert.That("".ToEnumOrDefault<SimpleEnum>(SimpleEnum.EnumValDefault), Is.EqualTo(SimpleEnum.EnumValDefault));
        Assert.That(((string?)null!).ToEnumOrDefault<SimpleEnum>(SimpleEnum.EnumValDefault), Is.EqualTo(SimpleEnum.EnumValDefault));
        Assert.That("not valid".ToEnumOrDefault<SimpleEnum>(SimpleEnum.EnumValDefault), Is.EqualTo(SimpleEnum.EnumValDefault));

        Assert.That("EnumVal2,EnumVal4".ToEnumOrDefault<FlagsEnum>(FlagsEnum.EnumValNone), Is.EqualTo(FlagsEnum.EnumVal2 | FlagsEnum.EnumVal4));
        Assert.That("EnumVal1, EnumVal3".ToEnumOrDefault<FlagsEnum>(FlagsEnum.EnumValNone), Is.EqualTo(FlagsEnum.EnumVal1 | FlagsEnum.EnumVal3));

        Assert.That("".ToEnumOrDefault<FlagsEnum>(FlagsEnum.EnumValNone), Is.EqualTo(FlagsEnum.EnumValNone));
        Assert.That(((string?)null!).ToEnumOrDefault<FlagsEnum>(FlagsEnum.EnumValNone), Is.EqualTo(FlagsEnum.EnumValNone));
        Assert.That("not valid".ToEnumOrDefault<FlagsEnum>(FlagsEnum.EnumValNone), Is.EqualTo(FlagsEnum.EnumValNone));
    }

    [Test]
    public void Test_ToEnumOrNull()
    {
        Assert.That("EnumVal1".ToEnumOrNull<SimpleEnum>(), Is.EqualTo(SimpleEnum.EnumVal1));
        Assert.That("EnumVal2".ToEnumOrNull<SimpleEnum>(), Is.EqualTo(SimpleEnum.EnumVal2));
        Assert.That("EnumVal3".ToEnumOrNull<SimpleEnum>(), Is.EqualTo(SimpleEnum.EnumVal3));
        Assert.That("EnumVal4".ToEnumOrNull<SimpleEnum>(), Is.EqualTo(SimpleEnum.EnumVal4));

        Assert.That("".ToEnumOrNull<SimpleEnum>(), Is.Null);
        Assert.That(((string?)null!).ToEnumOrNull<SimpleEnum>(), Is.Null);
        Assert.That("not valid".ToEnumOrNull<SimpleEnum>(), Is.Null);

        Assert.That("EnumVal2,EnumVal4".ToEnumOrNull<FlagsEnum>(), Is.EqualTo(FlagsEnum.EnumVal2 | FlagsEnum.EnumVal4));
        Assert.That("EnumVal1, EnumVal3".ToEnumOrNull<FlagsEnum>(), Is.EqualTo(FlagsEnum.EnumVal1 | FlagsEnum.EnumVal3));

        Assert.That("".ToEnumOrNull<FlagsEnum>(), Is.Null);
        Assert.That(((string?)null!).ToEnumOrNull<FlagsEnum>(), Is.Null);
        Assert.That("not valid".ToEnumOrNull<FlagsEnum>(), Is.Null);
    }

    [Test]
    public void Test_ToAnnotatedEnum()
    {
        Assert.That("Form1040".ToEnum<AnnotatedEnum>(), Is.EqualTo(AnnotatedEnum.Form1040));
        Assert.That("1040".ToEnum<AnnotatedEnum>(), Is.EqualTo(AnnotatedEnum.Form1040));
        Assert.That("FORMw2".ToEnum<AnnotatedEnum>(), Is.EqualTo(AnnotatedEnum.FormW2));
        Assert.That("w2".ToEnum<AnnotatedEnum>(), Is.EqualTo(AnnotatedEnum.FormW2));
        Assert.That("1".ToEnum<AnnotatedEnum>(), Is.EqualTo(AnnotatedEnum.FormW2));
        Assert.That("4506t".ToEnum<AnnotatedEnum>(), Is.EqualTo(AnnotatedEnum.Form4506T));
        Assert.That("vendor-complete".ToEnum<AnnotatedEnum>(), Is.EqualTo(AnnotatedEnum.Complete));
        Assert.That("UnknownForm".ToEnumOrNull<AnnotatedEnum>(), Is.Null);
        Assert.That(((string?)null!).ToEnumOrNull<AnnotatedEnum>(), Is.Null);
        Assert.Throws<ArgumentException>(() => "invalidEnum".ToEnum<AnnotatedEnum>());

        // test non-annotated enum
        Assert.That("EnumVal1".ToEnumOrDefault(SimpleEnum.EnumValDefault), Is.EqualTo(SimpleEnum.EnumVal1));
        Assert.That("EnumVal2".ToEnumOrDefault(SimpleEnum.EnumValDefault), Is.EqualTo(SimpleEnum.EnumVal2));
        Assert.That("EnumVal3".ToEnumOrDefault(SimpleEnum.EnumValDefault), Is.EqualTo(SimpleEnum.EnumVal3));
        Assert.That("EnumVal4".ToEnumOrDefault(SimpleEnum.EnumValDefault), Is.EqualTo(SimpleEnum.EnumVal4));

        // test with flagged enum
        Assert.That("EnumVal2,EnumVal4".ToEnum<FlagsEnum>(), Is.EqualTo(FlagsEnum.EnumVal2 | FlagsEnum.EnumVal4));
        Assert.That("EnumVal1, EnumVal3".ToEnum<FlagsEnum>(), Is.EqualTo(FlagsEnum.EnumVal1 | FlagsEnum.EnumVal3));

        // test with annotated flagged enum
        Assert.That("e2,e4".ToEnum<AnnotatedFlagsEnum>(), Is.EqualTo(AnnotatedFlagsEnum.EnumVal2 | AnnotatedFlagsEnum.EnumVal4));
        Assert.That("enumval1, e2".ToEnum<AnnotatedFlagsEnum>(), Is.EqualTo(AnnotatedFlagsEnum.EnumVal1 | AnnotatedFlagsEnum.EnumVal2));
        Assert.That("enumval1, notfound, e2".ToEnumOrNull<AnnotatedFlagsEnum>(), Is.Null);
    }

    #endregion Enum Tests

    [Test]
    public void Test_ToDateTimeOrNull()
    {
        var dt1 = new DateTime(2013, 10, 11);
        Assert.That("20131011000000".ToDateTimeOrNull("yyyyMMddHHmmss"), Is.EqualTo(dt1));
        Assert.That("2013-10-11".ToDateTimeOrNull("yyyy-MM-dd"), Is.EqualTo(dt1));

        Assert.That("2013-10-11".ToDateTimeOrNull(""), Is.Null);
        Assert.That("2013-10-11".ToDateTimeOrNull(null!), Is.Null);
        Assert.That("2013-10-11".ToDateTimeOrNull("yyyyMMdd"), Is.Null);
        Assert.That("".ToDateTimeOrNull("yyyyMMdd"), Is.Null);
        Assert.That(snull!.ToDateTimeOrNull("yyyyMMdd"), Is.Null);
    }

    [Test]
    public void Test_ToDecimalOrNull()
    {
        var dec = 23321.54;
        Assert.That("23321.54".ToDecimalOrNull(), Is.EqualTo(dec));
        Assert.That("-23321.54".ToDecimalOrNull(), Is.EqualTo(-dec));
        Assert.That("23321".ToDecimalOrNull(), Is.EqualTo(dec - 0.54));

        Assert.That("23321.54d".ToDecimalOrNull(), Is.Null);
        Assert.That("x".ToDecimalOrNull(), Is.Null);
        Assert.That(snull!.ToDecimalOrNull(), Is.Null);
    }

    [Test]
    public void Test_ToIntOrNull()
    {
        Assert.That("23321".ToIntOrNull(), Is.EqualTo(int1));
        Assert.That("-23321".ToIntOrNull(), Is.EqualTo(-int1));

        Assert.That("23321.54".ToIntOrNull(), Is.Null);
        Assert.That("x".ToIntOrNull(), Is.Null);
        Assert.That(snull!.ToIntOrNull(), Is.Null);
    }

    [Test]
    public void Test_ToLongOrNull()
    {
        var long1 = 5000000000L;
        var long2 = 500L;
        Assert.That("5000000000".ToLongOrNull(), Is.EqualTo(long1));
        Assert.That("500".ToLongOrNull(), Is.EqualTo(long2));
        Assert.That("23321".ToLongOrNull(), Is.EqualTo(int1));

        Assert.That("23321.54".ToLongOrNull(), Is.Null);
        Assert.That("x".ToLongOrNull(), Is.Null);
        Assert.That(snull!.ToLongOrNull(), Is.Null);
    }

    [Test]
    public void Test_ToFloatOrNull()
    {
        float bigFl = 1.643e6f;
        Assert.That("23321.54".ToFloatOrNull(), Is.EqualTo(fl1));
        Assert.That("-23321.54".ToFloatOrNull(), Is.EqualTo(-fl1));
        Assert.That("1.643e6".ToFloatOrNull(), Is.EqualTo(bigFl));

        Assert.That("23321.54f".ToFloatOrNull(), Is.Null);
        Assert.That("x".ToFloatOrNull(), Is.Null);
        Assert.That(snull!.ToFloatOrNull(), Is.Null);
    }

    [Test]
    public void Test_ToDoubleOrNull()
    {
        double db1 = 23321d; // 23321.54f produces a rounding error in the least digit
        double bigDbl = 1.643e6d;
        Assert.That("23321".ToDoubleOrNull(), Is.EqualTo(db1));
        Assert.That("1.643e6".ToDoubleOrNull(), Is.EqualTo(bigDbl));

        Assert.That("23321.54d".ToDoubleOrNull(), Is.Null);
        Assert.That("x".ToDoubleOrNull(), Is.Null);
        Assert.That(snull!.ToDoubleOrNull(), Is.Null);
    }

    [Test]
    public void Test_ToBool()
    {
        Assert.That("y".ToBool(), Is.EqualTo(true));
        Assert.That("Y".ToBool(), Is.EqualTo(true));
        Assert.That("yes".ToBool(), Is.EqualTo(true));
        Assert.That("Yes".ToBool(), Is.EqualTo(true));
        Assert.That("YES".ToBool(), Is.EqualTo(true));
        Assert.That("true".ToBool(), Is.EqualTo(true));
        Assert.That("TRUE".ToBool(), Is.EqualTo(true));
        Assert.That("N".ToBool(), Is.EqualTo(false));
        Assert.That("anything".ToBool(), Is.EqualTo(false));
        Assert.That(snull!.ToBool(), Is.EqualTo(false));

        Assert.That("Y".ToBool("y"), Is.EqualTo(true));
        Assert.That("y".ToBool("Y"), Is.EqualTo(true));
        Assert.That(snull!.ToBool("Y"), Is.EqualTo(false));
    }

    [Test]
    public void Test_ToBoolOrNull()
    {
        Assert.That("y".ToBoolOrNull(), Is.EqualTo(true));
        Assert.That("Y".ToBoolOrNull(), Is.EqualTo(true));
        Assert.That("yes".ToBoolOrNull(), Is.EqualTo(true));
        Assert.That("Yes".ToBoolOrNull(), Is.EqualTo(true));
        Assert.That("YES".ToBoolOrNull(), Is.EqualTo(true));
        Assert.That("true".ToBoolOrNull(), Is.EqualTo(true));
        Assert.That("TRUE".ToBoolOrNull(), Is.EqualTo(true));
        Assert.That("N".ToBoolOrNull(), Is.EqualTo(false));
        Assert.That("anything".ToBoolOrNull(), Is.EqualTo(false));

        Assert.That("Y".ToBoolOrNull("y"), Is.EqualTo(true));
        Assert.That("y".ToBoolOrNull("Y"), Is.EqualTo(true));

        Assert.That(snull!.ToBoolOrNull(), Is.Null);
    }

    [Test]
    public void Test_ChangeType()
    {
        // string
        Assert.That("text".ChangeType<string>(), Is.EqualTo("text"));
        Assert.That("   text   ".ChangeType<string>(), Is.EqualTo("   text   "));
        Assert.That("   ".ChangeType<string>(), Is.EqualTo("   "));
        Assert.That(((string?)null!).ChangeType<string>(), Is.EqualTo(null));

        // boolean
        Assert.That("true".ChangeType<bool>(), Is.EqualTo(true));
        Assert.That("   TRUE   ".ChangeType<bool>(), Is.EqualTo(true));
        Assert.That("False".ChangeType<bool>(), Is.EqualTo(false));
        Assert.Throws<FormatException>(() => "yes".ChangeType<bool>());
        Assert.Throws<InvalidOperationException>(() => ((string?)null!).ChangeType<bool>());
        Assert.Throws<InvalidOperationException>(() => "   ".ChangeType<bool>());

        Assert.That("true".ChangeType<bool?>(), Is.EqualTo(true));
        Assert.That("   TRUE   ".ChangeType<bool?>(), Is.EqualTo(true));
        Assert.That("False".ChangeType<bool?>(), Is.EqualTo(false));
        Assert.Throws<FormatException>(() => "yes".ChangeType<bool?>());
        Assert.That(((string?)null!).ChangeType<bool?>(), Is.EqualTo(null));
        Assert.That("   ".ChangeType<bool?>(), Is.EqualTo(null));

        // .net core 2.1 changed the exception due to overflowing the target type
        var invalidExcType = typeof(ArgumentException);

        // byte
        Assert.That("25".ChangeType<byte>(), Is.EqualTo(25));
        Assert.That("   25   ".ChangeType<byte>(), Is.EqualTo(25));
        Assert.Throws(invalidExcType, () => "1000".ChangeType<byte>());
        Assert.Throws<InvalidOperationException>(() => ((string?)null!).ChangeType<byte>());
        Assert.Throws<InvalidOperationException>(() => "   ".ChangeType<byte>());

        Assert.That("25".ChangeType<byte?>(), Is.EqualTo(25));
        Assert.That("   25   ".ChangeType<byte?>(), Is.EqualTo(25));
        Assert.Throws(invalidExcType, () => "1000".ChangeType<byte?>());
        Assert.That(((string?)null!).ChangeType<byte?>(), Is.EqualTo(null));
        Assert.That("   ".ChangeType<byte?>(), Is.EqualTo(null));

        // short
        Assert.That("25".ChangeType<short>(), Is.EqualTo(25));
        Assert.That("   25   ".ChangeType<short>(), Is.EqualTo(25));
        Assert.Throws(invalidExcType, () => "100000".ChangeType<short>());
        Assert.Throws<InvalidOperationException>(() => ((string?)null!).ChangeType<short>());
        Assert.Throws<InvalidOperationException>(() => "   ".ChangeType<short>());

        Assert.That("25".ChangeType<short?>(), Is.EqualTo(25));
        Assert.That("   25   ".ChangeType<short?>(), Is.EqualTo(25));
        Assert.Throws(invalidExcType, () => "100000".ChangeType<short?>());
        Assert.That(((string?)null!).ChangeType<short?>(), Is.EqualTo(null));
        Assert.That("   ".ChangeType<short?>(), Is.EqualTo(null));

        // int
        Assert.That("25".ChangeType<int>(), Is.EqualTo(25));
        Assert.That("   25   ".ChangeType<int>(), Is.EqualTo(25));
        Assert.Throws(invalidExcType, () => "10000000000".ChangeType<int>());
        Assert.Throws<InvalidOperationException>(() => ((string?)null!).ChangeType<int>());
        Assert.Throws<InvalidOperationException>(() => "   ".ChangeType<int>());

        Assert.That("25".ChangeType<int?>(), Is.EqualTo(25));
        Assert.That("   25   ".ChangeType<int?>(), Is.EqualTo(25));
        Assert.Throws(invalidExcType, () => "10000000000".ChangeType<int?>());
        Assert.That(((string?)null!).ChangeType<int?>(), Is.EqualTo(null));
        Assert.That("   ".ChangeType<int?>(), Is.EqualTo(null));

        // long
        Assert.That("25".ChangeType<long>(), Is.EqualTo(25));
        Assert.That("   25   ".ChangeType<long>(), Is.EqualTo(25));
        Assert.Throws(invalidExcType, () => "10000000000000000000".ChangeType<long>());
        Assert.Throws<InvalidOperationException>(() => ((string?)null!).ChangeType<long>());
        Assert.Throws<InvalidOperationException>(() => "   ".ChangeType<long>());

        Assert.That("25".ChangeType<long?>(), Is.EqualTo(25));
        Assert.That("   25   ".ChangeType<long?>(), Is.EqualTo(25));
        Assert.Throws(invalidExcType, () => "10000000000000000000".ChangeType<long?>());
        Assert.That(((string?)null!).ChangeType<long?>(), Is.EqualTo(null));
        Assert.That("   ".ChangeType<long?>(), Is.EqualTo(null));

        // float
        Assert.That("2.5".ChangeType<float>(), Is.EqualTo(2.5));
        Assert.That("   2.5   ".ChangeType<float>(), Is.EqualTo(2.5));
        Assert.That("1e39".ChangeType<float>(), Is.EqualTo(float.PositiveInfinity));
        Assert.Throws<InvalidOperationException>(() => ((string?)null!).ChangeType<float>());
        Assert.Throws<InvalidOperationException>(() => "   ".ChangeType<float>());

        Assert.That("2.5".ChangeType<float?>(), Is.EqualTo(2.5));
        Assert.That("   2.5   ".ChangeType<float?>(), Is.EqualTo(2.5));
        Assert.That("1e39".ChangeType<float?>(), Is.EqualTo(float.PositiveInfinity));
        Assert.That(((string?)null!).ChangeType<float?>(), Is.EqualTo(null));
        Assert.That("   ".ChangeType<float?>(), Is.EqualTo(null));

        // double
        Assert.That("2.5".ChangeType<double>(), Is.EqualTo(2.5));
        Assert.That("   2.5   ".ChangeType<double>(), Is.EqualTo(2.5));
        Assert.That("1e309".ChangeType<double>(), Is.EqualTo(float.PositiveInfinity));
        Assert.Throws<InvalidOperationException>(() => ((string?)null!).ChangeType<double>());
        Assert.Throws<InvalidOperationException>(() => "   ".ChangeType<double>());

        Assert.That("2.5".ChangeType<double?>(), Is.EqualTo(2.5));
        Assert.That("   2.5   ".ChangeType<double?>(), Is.EqualTo(2.5));
        Assert.That("1e309".ChangeType<double?>(), Is.EqualTo(float.PositiveInfinity));
        Assert.That(((string?)null!).ChangeType<double?>(), Is.EqualTo(null));
        Assert.That("   ".ChangeType<double?>(), Is.EqualTo(null));

        // decimal
        Assert.That("2.5".ChangeType<decimal>(), Is.EqualTo(2.5));
        Assert.That("   2.5   ".ChangeType<decimal>(), Is.EqualTo(2.5));
        Assert.Throws(invalidExcType, () => "100000000000000000000000000000".ChangeType<decimal>());
        Assert.Throws<InvalidOperationException>(() => ((string?)null!).ChangeType<decimal>());
        Assert.Throws<InvalidOperationException>(() => "   ".ChangeType<decimal>());

        Assert.That("2.5".ChangeType<decimal?>(), Is.EqualTo(2.5));
        Assert.That("   2.5   ".ChangeType<decimal?>(), Is.EqualTo(2.5));
        Assert.Throws(invalidExcType, () => "100000000000000000000000000000".ChangeType<decimal?>());
        Assert.That(((string?)null!).ChangeType<decimal?>(), Is.EqualTo(null));
        Assert.That("   ".ChangeType<decimal?>(), Is.EqualTo(null));

        // time span
        Assert.That("15:20:57".ChangeType<TimeSpan>(), Is.EqualTo(TimeSpan.Parse("15:20:57")));
        Assert.That("   15:20:57   ".ChangeType<TimeSpan>(), Is.EqualTo(TimeSpan.Parse("15:20:57")));
        Assert.Throws<FormatException>(() => "unknown".ChangeType<TimeSpan>());
        Assert.Throws<InvalidOperationException>(() => ((string?)null!).ChangeType<TimeSpan>());
        Assert.Throws<InvalidOperationException>(() => "   ".ChangeType<TimeSpan>());

        Assert.That("15:20:57".ChangeType<TimeSpan?>(), Is.EqualTo(TimeSpan.Parse("15:20:57")));
        Assert.That("   15:20:57   ".ChangeType<TimeSpan?>(), Is.EqualTo(TimeSpan.Parse("15:20:57")));
        Assert.Throws<FormatException>(() => "unknown".ChangeType<TimeSpan?>());
        Assert.That(((string?)null!).ChangeType<TimeSpan?>(), Is.EqualTo(null));
        Assert.That("   ".ChangeType<TimeSpan?>(), Is.EqualTo(null));

        // date/time
        Assert.That("1983-05-25".ChangeType<DateTime>(), Is.EqualTo(new DateTime(1983, 05, 25)));
        Assert.That("   1983-05-25   ".ChangeType<DateTime>(), Is.EqualTo(new DateTime(1983, 05, 25)));
        Assert.Throws<FormatException>(() => "unknown".ChangeType<DateTime>());
        Assert.Throws<InvalidOperationException>(() => ((string?)null!).ChangeType<DateTime>());
        Assert.Throws<InvalidOperationException>(() => "   ".ChangeType<DateTime>());

        Assert.That("1983-05-25".ChangeType<DateTime?>(), Is.EqualTo(new DateTime(1983, 05, 25)));
        Assert.That("   1983-05-25   ".ChangeType<DateTime?>(), Is.EqualTo(new DateTime(1983, 05, 25)));
        Assert.Throws<FormatException>(() => "unknown".ChangeType<DateTime?>());
        Assert.That(((string?)null!).ChangeType<DateTime?>(), Is.EqualTo(null));
        Assert.That("   ".ChangeType<DateTime?>(), Is.EqualTo(null));

        // GUID
        Assert.That("{2DC0B86E-3E16-445A-88B8-C39EFF611331}".ChangeType<Guid>(), Is.EqualTo(new Guid("2DC0B86E-3E16-445A-88B8-C39EFF611331")));
        Assert.That("2dc0b86e3e16445a88b8c39eff611331".ChangeType<Guid>(), Is.EqualTo(new Guid("2DC0B86E-3E16-445A-88B8-C39EFF611331")));
        Assert.That("   2dc0b86e3e16445a88b8c39eff611331   ".ChangeType<Guid>(), Is.EqualTo(new Guid("2DC0B86E-3E16-445A-88B8-C39EFF611331")));
        Assert.Throws<FormatException>(() => "unknown".ChangeType<Guid>());
        Assert.Throws<InvalidOperationException>(() => ((string?)null!).ChangeType<Guid>());
        Assert.Throws<InvalidOperationException>(() => "   ".ChangeType<Guid>());

        Assert.That("{2DC0B86E-3E16-445A-88B8-C39EFF611331}".ChangeType<Guid?>(), Is.EqualTo(new Guid("2DC0B86E-3E16-445A-88B8-C39EFF611331")));
        Assert.That("2dc0b86e3e16445a88b8c39eff611331".ChangeType<Guid?>(), Is.EqualTo(new Guid("2DC0B86E-3E16-445A-88B8-C39EFF611331")));
        Assert.That("   2dc0b86e3e16445a88b8c39eff611331   ".ChangeType<Guid?>(), Is.EqualTo(new Guid("2DC0B86E-3E16-445A-88B8-C39EFF611331")));
        Assert.Throws<FormatException>(() => "unknown".ChangeType<Guid?>());
        Assert.That(((string?)null!).ChangeType<Guid?>(), Is.EqualTo(null));
        Assert.That("   ".ChangeType<Guid?>(), Is.EqualTo(null));
    }

    [Test]
    public void TestTruncate()
    {
        Assert.That("abcd".Truncate(4000), Is.EqualTo("abcd"));
        Assert.That("abcd".Truncate(5), Is.EqualTo("abcd"));
        Assert.That("abcd".Truncate(4), Is.EqualTo("abcd"));
        Assert.That("abcd".Truncate(3), Is.EqualTo("abc..."));
        Assert.That("abcd".Truncate(2), Is.EqualTo("ab..."));
        Assert.That("abcd".Truncate(1), Is.EqualTo("a..."));
        Assert.That("abcd".Truncate(0), Is.EqualTo(""));
        Assert.That("abcd".Truncate(-1), Is.EqualTo(""));

        Assert.That("abcd".Truncate(3, string.Empty), Is.EqualTo("abc"));
        Assert.That("abcd".Truncate(3, null), Is.EqualTo("abc"));
        Assert.That("abcd".Truncate(3, "-We're done here"), Is.EqualTo("abc-We're done here"));
        Assert.That("abcd".Truncate(4000, "-We're done here"), Is.EqualTo("abcd"));

        Assert.That(((string)null!).Truncate(10), Is.Null);
    }

#region Enum Performance Testing

    [Flags]
#pragma warning disable IDE0079 // remove unnecessary suppression
#pragma warning disable CA1069 // Enums values should not be duplicated
    private enum FlagPerfEnum : ulong
    {
        E0 = 0, E1 = 1, E2 = E1 * 2, E3 = E2 * 2, E4 = E3 * 2, E5 = E4 * 2, E6 = E5 * 2, E7 = E6 * 2, E8 = E7 * 2, E9 = E8 * 2,
        E10 = E9 * 2, E11 = E10 * 2, E12 = E11 * 2, E13 = E12 * 2, E14 = E13 * 2, E15 = E14 * 2, E16 = E15 * 2, E17 = E16 * 2, E18 = E17 * 2, E19 = E18 * 2,
        E20 = E19 * 2, E21 = E20 * 2, E22 = E21 * 2, E23 = E22 * 2, E24 = E23 * 2, E25 = E24 * 2, E26 = E25 * 2, E27 = E26 * 2, E28 = E27 * 2, E29 = E28 * 2,
        E30 = E29 * 2, E31 = E30 * 2, E32 = E31 * 2, E33 = E32 * 2, E34 = E33 * 2, E35 = E34 * 2, E36 = E35 * 2, E37 = E36 * 2, E38 = E37 * 2, E39 = E38 * 2,
        E40 = E39 * 2, E41 = E40 * 2, E42 = E41 * 2, E43 = E42 * 2, E44 = E43 * 2, E45 = E44 * 2, E46 = E45 * 2, E47 = E46 * 2, E48 = E47 * 2, E49 = E48 * 2,
        E50 = E49 * 2, E51 = E50 * 2, E52 = E51 * 2, E53 = E52 * 2, E54 = E53 * 2, E55 = E54 * 2, E56 = E55 * 2, E57 = E56 * 2, E58 = E57 * 2, E59 = E58 * 2,
        E60 = E59 * 2, E61 = E60 * 2, E62 = E61 * 2, E63 = E62 * 2, E64 = E63 * 2,
        E65 = 1, E66 = E65 * 2, E67 = E66 * 2, E68 = E67 * 2, E69 = E68 * 2,
        E70 = E69 * 2, E71 = E70 * 2, E72 = E71 * 2, E73 = E72 * 2, E74 = E73 * 2, E75 = E74 * 2, E76 = E75 * 2, E77 = E76 * 2, E78 = E77 * 2, E79 = E78 * 2,
        E80 = E79 * 2, E81 = E80 * 2, E82 = E81 * 2, E83 = E82 * 2, E84 = E83 * 2, E85 = E84 * 2, E86 = E85 * 2, E87 = E86 * 2, E88 = E87 * 2, E89 = E88 * 2,
        E90 = E89 * 2, E91 = E90 * 2, E92 = E91 * 2, E93 = E92 * 2, E94 = E93 * 2, E95 = E94 * 2, E96 = E95 * 2, E97 = E96 * 2, E98 = E97 * 2, E99 = E98 * 2,
        E100 = 1, E101 = E100 * 2, E102 = E101 * 2, E103 = E102 * 2, E104 = E103 * 2, E105 = E104 * 2, E106 = E105 * 2, E107 = E106 * 2, E108 = E107 * 2, E109 = E108 * 2,
        [EnumMember(Value = "Enum110")]
        E110 = E109 * 2,
        E111 = E110 * 2, E112 = E111 * 2, E113 = E112 * 2, E114 = E113 * 2, E115 = E114 * 2, E116 = E115 * 2, E117 = E116 * 2, E118 = E117 * 2, E119 = E118 * 2,
        E120 = E119 * 2, E121 = E120 * 2, E122 = E121 * 2, E123 = E122 * 2, E124 = E123 * 2, E125 = E124 * 2, E126 = E125 * 2, E127 = E126 * 2, E128 = E127 * 2, E129 = E128 * 2,
        E130 = E129 * 2, E131 = E130 * 2, E132 = E131 * 2, E133 = E132 * 2, E134 = E133 * 2, E135 = E134 * 2, E136 = E135 * 2, E137 = E136 * 2, E138 = E137 * 2, E139 = E138 * 2,
        E140 = E139 * 2, E141 = E140 * 2, E142 = E141 * 2, E143 = E142 * 2, E144 = E143 * 2, E145 = E144 * 2, E146 = E145 * 2, E147 = E146 * 2, E148 = E147 * 2, E149 = E148 * 2,
        E150 = E149 * 2, E151 = E150 * 2, E152 = E151 * 2, E153 = E152 * 2, E154 = E153 * 2, E155 = E154 * 2, E156 = E155 * 2, E157 = E156 * 2, E158 = E157 * 2, E159 = E158 * 2,
        E160 = E159 * 2, E161 = E160 * 2, E162 = E161 * 2, E163 = E162 * 2,
        E164 = 1, E165 = E164 * 2, E166 = E165 * 2, E167 = E166 * 2, E168 = E167 * 2, E169 = E168 * 2,
        E170 = E169 * 2, E171 = E170 * 2, E172 = E171 * 2, E173 = E172 * 2, E174 = E173 * 2, E175 = E174 * 2, E176 = E175 * 2, E177 = E176 * 2, E178 = E177 * 2, E179 = E178 * 2,
        E180 = E179 * 2, E181 = E180 * 2, E182 = E181 * 2, E183 = E182 * 2, E184 = E183 * 2, E185 = E184 * 2, E186 = E185 * 2, E187 = E186 * 2, E188 = E187 * 2, E189 = E188 * 2,
        E190 = E189 * 2, E191 = E190 * 2, E192 = E191 * 2, E193 = E192 * 2, E194 = E193 * 2, E195 = E194 * 2, E196 = E195 * 2, E197 = E196 * 2, E198 = E197 * 2, E199 = E198 * 2,
        E200 = 1, E201 = E200 * 2, E202 = E201 * 2, E203 = E202 * 2, E204 = E203 * 2, E205 = E204 * 2, E206 = E205 * 2, E207 = E206 * 2, E208 = E207 * 2, E209 = E208 * 2,
        E210 = E209 * 2, E211 = E210 * 2, [EnumMember(Value = "Enum212")]
        E212 = E211 * 2,
        E213 = E212 * 2, E214 = E213 * 2, E215 = E214 * 2, E216 = E215 * 2, E217 = E216 * 2, E218 = E217 * 2, E219 = E218 * 2,
        E220 = E219 * 2, E221 = E220 * 2, E222 = E221 * 2, E223 = E222 * 2, E224 = E223 * 2, E225 = E224 * 2, E226 = E225 * 2, E227 = E226 * 2, E228 = E227 * 2, E229 = E228 * 2,
        E230 = E229 * 2, E231 = E230 * 2, E232 = E231 * 2, E233 = E232 * 2, E234 = E233 * 2, E235 = E234 * 2, E236 = E235 * 2, E237 = E236 * 2, E238 = E237 * 2, E239 = E238 * 2,
        E240 = E239 * 2, E241 = E240 * 2, E242 = E241 * 2, E243 = E242 * 2, E244 = E243 * 2, E245 = E244 * 2, E246 = E245 * 2, E247 = E246 * 2, E248 = E247 * 2, E249 = E248 * 2,
        E250 = E249 * 2, E251 = E250 * 2, E252 = E251 * 2, E253 = E252 * 2, E254 = E253 * 2, E255 = E254 * 2, E256 = E255 * 2, E257 = E256 * 2, E258 = E257 * 2, E259 = E258 * 2,
        E260 = E259 * 2, E261 = E260 * 2, E262 = E261 * 2, E263 = E262 * 2,
        E264 = 1, E265 = E264 * 2, E266 = E265 * 2, E267 = E266 * 2, E268 = E267 * 2, E269 = E268 * 2,
        E270 = E269 * 2, E271 = E270 * 2, E272 = E271 * 2, E273 = E272 * 2, E274 = E273 * 2, E275 = E274 * 2, E276 = E275 * 2, E277 = E276 * 2, E278 = E277 * 2, E279 = E278 * 2,
        E280 = E279 * 2, E281 = E280 * 2, E282 = E281 * 2, E283 = E282 * 2, E284 = E283 * 2, E285 = E284 * 2, E286 = E285 * 2, E287 = E286 * 2, E288 = E287 * 2, E289 = E288 * 2,
        E290 = E289 * 2, E291 = E290 * 2, E292 = E291 * 2, E293 = E292 * 2, E294 = E293 * 2, E295 = E294 * 2, E296 = E295 * 2, E297 = E296 * 2, E298 = E297 * 2, E299 = E298 * 2,
        E300 = 1, E301 = E300 * 2, E302 = E301 * 2, E303 = E302 * 2, E304 = E303 * 2, E305 = E304 * 2, E306 = E305 * 2, E307 = E306 * 2, E308 = E307 * 2, E309 = E308 * 2,
        E310 = E309 * 2, E311 = E310 * 2, E312 = E311 * 2, E313 = E312 * 2, E314 = E313 * 2, E315 = E314 * 2, E316 = E315 * 2, E317 = E316 * 2, E318 = E317 * 2, E319 = E318 * 2,
        E320 = E319 * 2, E321 = E320 * 2, E322 = E321 * 2, E323 = E322 * 2, E324 = E323 * 2, E325 = E324 * 2, E326 = E325 * 2, E327 = E326 * 2, E328 = E327 * 2, E329 = E328 * 2,
        E330 = E329 * 2, E331 = E330 * 2, E332 = E331 * 2, E333 = E332 * 2, E334 = E333 * 2, E335 = E334 * 2, E336 = E335 * 2, E337 = E336 * 2, E338 = E337 * 2, E339 = E338 * 2,
        E340 = E339 * 2, E341 = E340 * 2, E342 = E341 * 2, E343 = E342 * 2, E344 = E343 * 2, E345 = E344 * 2, E346 = E345 * 2, E347 = E346 * 2, E348 = E347 * 2, E349 = E348 * 2,
        E350 = E349 * 2, E351 = E350 * 2, E352 = E351 * 2, E353 = E352 * 2, E354 = E353 * 2, E355 = E354 * 2, E356 = E355 * 2, E357 = E356 * 2, E358 = E357 * 2, E359 = E358 * 2,
        E360 = E359 * 2, [EnumMember(Value = "Enum361")]
        E361 = E360 * 2, E362 = E361 * 2, E363 = E362 * 2,
        E364 = 1, E365 = E364 * 2, E366 = E365 * 2, E367 = E366 * 2, E368 = E367 * 2, E369 = E368 * 2,
        E370 = E369 * 2, E371 = E370 * 2, E372 = E371 * 2, E373 = E372 * 2, E374 = E373 * 2, E375 = E374 * 2, E376 = E375 * 2, E377 = E376 * 2, E378 = E377 * 2, E379 = E378 * 2,
        E380 = E379 * 2, E381 = E380 * 2, E382 = E381 * 2, E383 = E382 * 2, E384 = E383 * 2, E385 = E384 * 2, E386 = E385 * 2, E387 = E386 * 2, E388 = E387 * 2, E389 = E388 * 2,
        E390 = E389 * 2, E391 = E390 * 2, E392 = E391 * 2, E393 = E392 * 2, E394 = E393 * 2, E395 = E394 * 2, E396 = E395 * 2, E397 = E396 * 2, E398 = E397 * 2, E399 = E398 * 2,
        E400 = 1, E401 = E400 * 2, E402 = E401 * 2, E403 = E402 * 2, E404 = E403 * 2, E405 = E404 * 2, E406 = E405 * 2, E407 = E406 * 2, E408 = E407 * 2, E409 = E408 * 2,
        E410 = E409 * 2, E411 = E410 * 2, E412 = E411 * 2, E413 = E412 * 2, E414 = E413 * 2, E415 = E414 * 2, E416 = E415 * 2, E417 = E416 * 2, E418 = E417 * 2, E419 = E418 * 2,
        E420 = E419 * 2, E421 = E420 * 2, E422 = E421 * 2, E423 = E422 * 2, E424 = E423 * 2, E425 = E424 * 2, E426 = E425 * 2, E427 = E426 * 2, E428 = E427 * 2, E429 = E428 * 2,
        E430 = E429 * 2, E431 = E430 * 2, E432 = E431 * 2, E433 = E432 * 2, E434 = E433 * 2, E435 = E434 * 2, E436 = E435 * 2, E437 = E436 * 2, E438 = E437 * 2, E439 = E438 * 2,
        E440 = E439 * 2, E441 = E440 * 2, E442 = E441 * 2, [EnumMember(Value = "Enum443")]
        E443 = E442 * 2, E444 = E443 * 2, E445 = E444 * 2, E446 = E445 * 2, E447 = E446 * 2, E448 = E447 * 2, E449 = E448 * 2,
        E450 = E449 * 2, E451 = E450 * 2, E452 = E451 * 2, E453 = E452 * 2, E454 = E453 * 2, E455 = E454 * 2, E456 = E455 * 2, E457 = E456 * 2, E458 = E457 * 2, E459 = E458 * 2,
        E460 = E459 * 2, E461 = E460 * 2, E462 = E461 * 2, E463 = E462 * 2,
        E464 = 1, E465 = E464 * 2, E466 = E465 * 2, E467 = E466 * 2, E468 = E467 * 2, E469 = E468 * 2,
        E470 = E469 * 2, E471 = E470 * 2, E472 = E471 * 2, E473 = E472 * 2, E474 = E473 * 2, E475 = E474 * 2, E476 = E475 * 2, E477 = E476 * 2, E478 = E477 * 2, E479 = E478 * 2,
        E480 = E479 * 2, E481 = E480 * 2, E482 = E481 * 2, E483 = E482 * 2, E484 = E483 * 2, E485 = E484 * 2, E486 = E485 * 2, E487 = E486 * 2, E488 = E487 * 2, E489 = E488 * 2,
        E490 = E489 * 2, E491 = E490 * 2, E492 = E491 * 2, E493 = E492 * 2, E494 = E493 * 2, E495 = E494 * 2, E496 = E495 * 2, E497 = E496 * 2, E498 = E497 * 2, E499 = E498 * 2,
        E500 = 1, E501 = E500 * 2, E502 = E501 * 2, E503 = E502 * 2, E504 = E503 * 2, E505 = E504 * 2, E506 = E505 * 2, E507 = E506 * 2, E508 = E507 * 2, E509 = E508 * 2,
        E510 = E509 * 2, E511 = E510 * 2, E512 = E511 * 2, E513 = E512 * 2, E514 = E513 * 2, E515 = E514 * 2, E516 = E515 * 2, E517 = E516 * 2, E518 = E517 * 2, E519 = E518 * 2,
        E520 = E519 * 2, E521 = E520 * 2, E522 = E521 * 2, E523 = E522 * 2, E524 = E523 * 2, E525 = E524 * 2, E526 = E525 * 2, [EnumMember(Value = "Enum527")]
        E527 = E526 * 2, E528 = E527 * 2, E529 = E528 * 2,
        E530 = E529 * 2, E531 = E530 * 2, E532 = E531 * 2, E533 = E532 * 2, E534 = E533 * 2, E535 = E534 * 2, E536 = E535 * 2, E537 = E536 * 2, E538 = E537 * 2, E539 = E538 * 2,
        E540 = E539 * 2, E541 = E540 * 2, E542 = E541 * 2, E543 = E542 * 2, E544 = E543 * 2, E545 = E544 * 2, E546 = E545 * 2, E547 = E546 * 2, E548 = E547 * 2, E549 = E548 * 2,
        E550 = E549 * 2, E551 = E550 * 2, E552 = E551 * 2, E553 = E552 * 2, E554 = E553 * 2, E555 = E554 * 2, E556 = E555 * 2, E557 = E556 * 2, E558 = E557 * 2, E559 = E558 * 2,
        E560 = E559 * 2, E561 = E560 * 2, E562 = E561 * 2, E563 = E562 * 2,
        E564 = 1, E565 = E564 * 2, E566 = E565 * 2, E567 = E566 * 2, E568 = E567 * 2, E569 = E568 * 2,
        E570 = E569 * 2, E571 = E570 * 2, E572 = E571 * 2, E573 = E572 * 2, E574 = E573 * 2, E575 = E574 * 2, E576 = E575 * 2, E577 = E576 * 2, E578 = E577 * 2, E579 = E578 * 2,
        E580 = E579 * 2, E581 = E580 * 2, E582 = E581 * 2, E583 = E582 * 2, E584 = E583 * 2, E585 = E584 * 2, E586 = E585 * 2, E587 = E586 * 2, E588 = E587 * 2, E589 = E588 * 2,
        E590 = E589 * 2, E591 = E590 * 2, E592 = E591 * 2, E593 = E592 * 2, E594 = E593 * 2, E595 = E594 * 2, E596 = E595 * 2, E597 = E596 * 2, E598 = E597 * 2, E599 = E598 * 2,
        E600 = 1, [EnumMember(Value = "Enum601")]
        E601 = E600 * 2, E602 = E601 * 2, E603 = E602 * 2, E604 = E603 * 2, E605 = E604 * 2, E606 = E605 * 2, E607 = E606 * 2, E608 = E607 * 2, E609 = E608 * 2,
        E610 = E609 * 2, E611 = E610 * 2, E612 = E611 * 2, E613 = E612 * 2, E614 = E613 * 2, E615 = E614 * 2, E616 = E615 * 2, E617 = E616 * 2, E618 = E617 * 2, E619 = E618 * 2,
        E620 = E619 * 2, E621 = E620 * 2, E622 = E621 * 2, E623 = E622 * 2, E624 = E623 * 2, E625 = E624 * 2, E626 = E625 * 2, E627 = E626 * 2, E628 = E627 * 2, E629 = E628 * 2,
        E630 = E629 * 2, E631 = E630 * 2, E632 = E631 * 2, E633 = E632 * 2, E634 = E633 * 2, E635 = E634 * 2, E636 = E635 * 2, E637 = E636 * 2, E638 = E637 * 2, E639 = E638 * 2,
        E640 = E639 * 2, E641 = E640 * 2, E642 = E641 * 2, E643 = E642 * 2, E644 = E643 * 2, E645 = E644 * 2, E646 = E645 * 2, E647 = E646 * 2, E648 = E647 * 2, E649 = E648 * 2,
        E650 = E649 * 2, E651 = E650 * 2, E652 = E651 * 2, E653 = E652 * 2, E654 = E653 * 2, E655 = E654 * 2, E656 = E655 * 2, E657 = E656 * 2, E658 = E657 * 2, E659 = E658 * 2,
        E660 = E659 * 2, E661 = E660 * 2, E662 = E661 * 2, E663 = E662 * 2,
        E664 = 1, E665 = E664 * 2, E666 = E665 * 2, E667 = E666 * 2, E668 = E667 * 2, E669 = E668 * 2,
        E670 = E669 * 2, E671 = E670 * 2, E672 = E671 * 2, E673 = E672 * 2, E674 = E673 * 2, E675 = E674 * 2, E676 = E675 * 2, E677 = E676 * 2, E678 = E677 * 2, E679 = E678 * 2,
        E680 = E679 * 2, E681 = E680 * 2, E682 = E681 * 2, E683 = E682 * 2, E684 = E683 * 2, E685 = E684 * 2, E686 = E685 * 2, E687 = E686 * 2, E688 = E687 * 2, E689 = E688 * 2,
        E690 = E689 * 2, E691 = E690 * 2, E692 = E691 * 2, E693 = E692 * 2, E694 = E693 * 2, E695 = E694 * 2, E696 = E695 * 2, E697 = E696 * 2, E698 = E697 * 2, E699 = E698 * 2,
        E700 = 1, E701 = E700 * 2, E702 = E701 * 2, E703 = E702 * 2, E704 = E703 * 2, E705 = E704 * 2, E706 = E705 * 2, E707 = E706 * 2, E708 = E707 * 2, E709 = E708 * 2,
        E710 = E709 * 2, E711 = E710 * 2, E712 = E711 * 2, E713 = E712 * 2, E714 = E713 * 2, E715 = E714 * 2, [EnumMember(Value = "Enum716")]
        E716 = E715 * 2, E717 = E716 * 2, E718 = E717 * 2, E719 = E718 * 2,
        E720 = E719 * 2, E721 = E720 * 2, E722 = E721 * 2, E723 = E722 * 2, E724 = E723 * 2, E725 = E724 * 2, E726 = E725 * 2, E727 = E726 * 2, E728 = E727 * 2, E729 = E728 * 2,
        E730 = E729 * 2, E731 = E730 * 2, E732 = E731 * 2, E733 = E732 * 2, E734 = E733 * 2, E735 = E734 * 2, E736 = E735 * 2, E737 = E736 * 2, E738 = E737 * 2, E739 = E738 * 2,
        E740 = E739 * 2, E741 = E740 * 2, E742 = E741 * 2, E743 = E742 * 2, E744 = E743 * 2, E745 = E744 * 2, E746 = E745 * 2, E747 = E746 * 2, E748 = E747 * 2, E749 = E748 * 2,
        E750 = E749 * 2, E751 = E750 * 2, E752 = E751 * 2, E753 = E752 * 2, E754 = E753 * 2, E755 = E754 * 2, E756 = E755 * 2, E757 = E756 * 2, E758 = E757 * 2, E759 = E758 * 2,
        E760 = E759 * 2, E761 = E760 * 2, E762 = E761 * 2, E763 = E762 * 2,
        E764 = 1, E765 = E764 * 2, E766 = E765 * 2, E767 = E766 * 2, E768 = E767 * 2, E769 = E768 * 2,
        E770 = E769 * 2, E771 = E770 * 2, E772 = E771 * 2, E773 = E772 * 2, E774 = E773 * 2, E775 = E774 * 2, E776 = E775 * 2, E777 = E776 * 2, E778 = E777 * 2, E779 = E778 * 2,
        E780 = E779 * 2, E781 = E780 * 2, E782 = E781 * 2, E783 = E782 * 2, E784 = E783 * 2, E785 = E784 * 2, E786 = E785 * 2, E787 = E786 * 2, E788 = E787 * 2, E789 = E788 * 2,
        E790 = E789 * 2, E791 = E790 * 2, E792 = E791 * 2, E793 = E792 * 2, E794 = E793 * 2, E795 = E794 * 2, E796 = E795 * 2, E797 = E796 * 2, E798 = E797 * 2, E799 = E798 * 2,
        E800 = 1, E801 = E800 * 2, E802 = E801 * 2, E803 = E802 * 2, E804 = E803 * 2, E805 = E804 * 2, E806 = E805 * 2, E807 = E806 * 2, E808 = E807 * 2, E809 = E808 * 2,
        E810 = E809 * 2, E811 = E810 * 2, E812 = E811 * 2, E813 = E812 * 2, E814 = E813 * 2, E815 = E814 * 2, E816 = E815 * 2, E817 = E816 * 2, E818 = E817 * 2, E819 = E818 * 2,
        E820 = E819 * 2, E821 = E820 * 2, E822 = E821 * 2, E823 = E822 * 2, E824 = E823 * 2, E825 = E824 * 2, E826 = E825 * 2, E827 = E826 * 2, E828 = E827 * 2, E829 = E828 * 2,
        E830 = E829 * 2, E831 = E830 * 2, [EnumMember(Value = "Enum832")]
        E832 = E831 * 2, E833 = E832 * 2, E834 = E833 * 2, E835 = E834 * 2, E836 = E835 * 2, E837 = E836 * 2, E838 = E837 * 2, E839 = E838 * 2,
        E840 = E839 * 2, E841 = E840 * 2, E842 = E841 * 2, E843 = E842 * 2, E844 = E843 * 2, E845 = E844 * 2, E846 = E845 * 2, E847 = E846 * 2, E848 = E847 * 2, E849 = E848 * 2,
        E850 = E849 * 2, E851 = E850 * 2, E852 = E851 * 2, E853 = E852 * 2, E854 = E853 * 2, E855 = E854 * 2, E856 = E855 * 2, E857 = E856 * 2, E858 = E857 * 2, E859 = E858 * 2,
        E860 = E859 * 2, E861 = E860 * 2, E862 = E861 * 2, E863 = E862 * 2,
        E864 = 1, E865 = E864 * 2, E866 = E865 * 2, E867 = E866 * 2, E868 = E867 * 2, E869 = E868 * 2,
        E870 = E869 * 2, E871 = E870 * 2, E872 = E871 * 2, E873 = E872 * 2, E874 = E873 * 2, E875 = E874 * 2, E876 = E875 * 2, E877 = E876 * 2, E878 = E877 * 2, E879 = E878 * 2,
        E880 = E879 * 2, E881 = E880 * 2, E882 = E881 * 2, E883 = E882 * 2, E884 = E883 * 2, E885 = E884 * 2, E886 = E885 * 2, E887 = E886 * 2, E888 = E887 * 2, E889 = E888 * 2,
        E890 = E889 * 2, E891 = E890 * 2, E892 = E891 * 2, E893 = E892 * 2, E894 = E893 * 2, E895 = E894 * 2, E896 = E895 * 2, E897 = E896 * 2, E898 = E897 * 2, E899 = E898 * 2,
        E900 = 1, E901 = E900 * 2, E902 = E901 * 2, E903 = E902 * 2, E904 = E903 * 2, E905 = E904 * 2, E906 = E905 * 2, E907 = E906 * 2, E908 = E907 * 2, E909 = E908 * 2,
        E910 = E909 * 2, E911 = E910 * 2, E912 = E911 * 2, E913 = E912 * 2, E914 = E913 * 2, E915 = E914 * 2, E916 = E915 * 2, E917 = E916 * 2, E918 = E917 * 2, E919 = E918 * 2,
        E920 = E919 * 2, E921 = E920 * 2, E922 = E921 * 2, E923 = E922 * 2, E924 = E923 * 2, E925 = E924 * 2, E926 = E925 * 2, E927 = E926 * 2, E928 = E927 * 2, E929 = E928 * 2,
        E930 = E929 * 2, E931 = E930 * 2, E932 = E931 * 2, E933 = E932 * 2, E934 = E933 * 2, E935 = E934 * 2, E936 = E935 * 2, E937 = E936 * 2, E938 = E937 * 2, E939 = E938 * 2,
        E940 = E939 * 2, E941 = E940 * 2, E942 = E941 * 2, E943 = E942 * 2, E944 = E943 * 2, E945 = E944 * 2, E946 = E945 * 2, E947 = E946 * 2, E948 = E947 * 2, E949 = E948 * 2,
        E950 = E949 * 2, E951 = E950 * 2, E952 = E951 * 2, E953 = E952 * 2, E954 = E953 * 2, E955 = E954 * 2, E956 = E955 * 2, E957 = E956 * 2, E958 = E957 * 2, E959 = E958 * 2,
        E960 = E959 * 2, E961 = E960 * 2, E962 = E961 * 2, E963 = E962 * 2,
        E964 = 1, E965 = E964 * 2, E966 = E965 * 2, E967 = E966 * 2, E968 = E967 * 2, E969 = E968 * 2,
        E970 = E969 * 2, E971 = E970 * 2, E972 = E971 * 2, E973 = E972 * 2, [EnumMember(Value = "Enum974")]
        E974 = E973 * 2, E975 = E974 * 2, E976 = E975 * 2, E977 = E976 * 2, E978 = E977 * 2, E979 = E978 * 2,
        E980 = E979 * 2, E981 = E980 * 2, E982 = E981 * 2, E983 = E982 * 2, E984 = E983 * 2, E985 = E984 * 2, E986 = E985 * 2, E987 = E986 * 2, E988 = E987 * 2, E989 = E988 * 2,
        E990 = E989 * 2, E991 = E990 * 2, E992 = E991 * 2, E993 = E992 * 2, E994 = E993 * 2, E995 = E994 * 2, E996 = E995 * 2, E997 = E996 * 2, E998 = E997 * 2, E999 = E998 * 2
    }
#pragma warning restore CA1069 // Enums values should not be duplicated
#pragma warning restore IDE0079 // remove unnecessary suppression

    //[Test]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Structure", "NUnit1028:The non-test method is public", Justification = "<Pending>")]
    public void Test_FlagPerfEnumPerformance()
    {
        ExecuteEnumPerformance<FlagPerfEnum>("Best case", 1000, "E1");
        ExecuteEnumPerformance<FlagPerfEnum>("Worst case", 1000, "E999");
        ExecuteEnumPerformance<FlagPerfEnum>("Multiple values", 1000, "E4,E120,E456,E999");
        ExecuteEnumPerformance<FlagPerfEnum>("First Annotation", 1000, "Enum110");
        ExecuteEnumPerformance<FlagPerfEnum>("Last Annotation", 1000, "Enum974");
        ExecuteEnumPerformance<FlagPerfEnum>("Multiple annotations", 1000, "Enum527, Enum212, Enum832");
        ExecuteEnumPerformance<FlagPerfEnum>("Mixed annotations", 1000, "E776,E200, Enum361, E512, Enum974, E940");
        ExecuteEnumPerformance<FlagPerfEnum>("Not Found", 1000, "E10000");
        ExecuteEnumPerformance<FlagPerfEnum>("Mixed Found and Not Found", 1000, "E265, E10000");
    }

    private static void ExecuteEnumPerformance<T>(string prefix, int numIterations, string value) where T : struct, Enum
    {
        var timer = Stopwatch.StartNew();
        var e1 = default(T);
        for (var i = 0; i < numIterations; i++)
        {
            _ = Enum.TryParse(value, out e1);
        }
        timer.Stop();
        var timer1 = timer.ElapsedMilliseconds;

        timer = Stopwatch.StartNew();
        T? e2 = null;
        for (var i = 0; i < numIterations; i++)
        {
            e2 = value.ToEnumOrNull<T>();
        }
        timer.Stop();
        var timer2 = timer.ElapsedMilliseconds;

        Console.WriteLine($"{prefix}: TryParse result = {e1} with time = {timer1} ms. : str.ToEnum result = {e2} with time = {timer2} ms. for value '{value}");
    }

#endregion Enum Performance Testing
}
