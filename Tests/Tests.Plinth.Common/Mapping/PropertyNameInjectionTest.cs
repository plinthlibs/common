using Plinth.Common.Mapping;
using NUnit.Framework;
using Omu.ValueInjecter;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace Tests.Plinth.Common.Mapping;

[TestFixture]
public class PropertyNameInjectionTest
{
    public class Foo
    {
        public enum FooEnum
        {
            Unknown,
            Value1,
            Value2,
            Value3,
            Value4
        }

        [Flags]
        public enum FooFlagsEnum
        {
            Default = 0,
            Value1 = 1 << 0,
            Value2 = 1 << 1,
            Value3 = 1 << 2,
            Value4 = 1 << 3
        }

        public string? String { get; set; }
        public object? Object { get; set; }

        public FooEnum Enum1 { get; set; }
        public Bar.BarEnum Enum2 { get; set; }
        public FooEnum Enum3 { get; set; }
        public FooEnum Enum4 { get; set; }
        public FooEnum? Enum5 { get; set; }
        public FooEnum? Enum6 { get; set; }

        public FooEnum EnumE2S { get; set; }
        public FooEnum? EnumEq2S { get; set; }
        public FooEnum? EnumEqnull2S { get; set; }
        public string? EnumS2E { get; set; }
        public string? EnumSq2E { get; set; }
        public string? EnumSq2Enull { get; set; }

        public FooFlagsEnum? FlagsEnum1 { get; set; }
        public FooFlagsEnum? FlagsEnum2 { get; set; }
        public FooFlagsEnum? FlagsEnum3 { get; set; }
        public FooEnum? FlagsEnum4 { get; set; }

        public FooFlagsEnum FlagsEnumE2S { get; set; }
        public FooFlagsEnum? FlagsEnumEq2S { get; set; }
        public FooFlagsEnum? FlagsEnumEqnull2S { get; set; }
        public string? FlagsEnumS2E { get; set; }
        public string? FlagsEnumSq2E { get; set; }
        public string? FlagsEnumSq2Enull { get; set; }

        public int Int1 { get; set; }
        public int Int2 { get; set; }
        public int? Int3 { get; set; }
        public int? Int4 { get; set; }

        public Foo? Child { get; set; }

        public Foo[]? Array { get; set; }
        public List<Foo>? List { get; set; }
        public Foo[]? ArrayToList { get; set; }
        public List<Foo>? ListToArray { get; set; }

        public IList<int>? IListToIList { get; set; }
        public List<int>? ListToIList { get; set; }
        public IList<int>? IListToList { get; set; }

        public FooEnum[]? EnumArrayToList { get; set; }
        public List<FooEnum>? EnumListToArray { get; set; }
        public List<FooEnum>? EnumListToString { get; set; }
        public List<string>? StringListToEnum { get; set; }
        public Dictionary<string, string?>? ScalarDictionary { get; set; }
        public Dictionary<Foo, string?>? ObjectStringDictionary { get; set; }
        public Dictionary<string, Foo?>? StringObjectDictionary { get; set; }

        public IDictionary<string, int>? IDictionaryToIDictionary { get; set; }
        public IDictionary<string, int>? IDictionaryToDictionary { get; set; }
        public Dictionary<string, int>? DictionaryToIDictionary { get; set; }
        public Dictionary<string, int>? DictionaryCaseInsensitive { get; set; }
        public CaseInsensitiveDictionary<string, int>? CaseInsensitiveDictionary { get; set; }

        public HashSet<string>? ScalarSet { get; set; }
        public HashSet<Foo>? ObjectSet { get; set; }

        public ISet<string>? ISetToISet { get; set; }
        public ISet<string>? ISetToHashSet { get; set; }
        public HashSet<string>? HashSetToISet { get; set; }
        public HashSet<string>? HashSetCaseInsensitive { get; set; }
        public CaseInsensitiveHashSet<string>? CaseInsensitiveHashSet { get; set; }

        public string[]? StringArray { get; set; }
        public List<string>? StringList { get; set; }
        public List<int>? IntList { get; set; }
        public int[]? IntArray { get; set; }
        public List<int?>? NullableIntList { get; set; }
        public int?[]? NullableIntArray { get; set; }

        public object? ObjectString { get; set; }
        public object? ObjectEnum { get; set; }
        public object? ObjectInt { get; set; }
        public object? ObjectChild { get; set; }
        public object? ObjectArray { get; set; }
    }

    public class Bar
    {
        public enum BarEnum
        {
            Unknown,
            Value1,
            Value2,
            Value3
        }

        [Flags]
        public enum BarFlagsEnum
        {
            Default = 0,
            Value1 = 1 << 0,
            Value2 = 1 << 1,
            Value3 = 1 << 2,
        }

        public string? String { get; set; }
        public object? Object { get; set; }

        public BarEnum Enum1 { get; set; }
        public BarEnum Enum2 { get; set; }
        public BarEnum Enum3 { get; set; }
        public BarEnum? Enum4 { get; set; }
        public BarEnum Enum5 { get; set; }
        public BarEnum Enum6 { get; set; }

        public string? EnumE2S { get; set; }
        public string? EnumEq2S { get; set; }
        public string? EnumEqnull2S { get; set; }
        public BarEnum EnumS2E { get; set; }
        public BarEnum? EnumSq2E { get; set; }
        public BarEnum? EnumSq2Enull { get; set; }

        public BarFlagsEnum? FlagsEnum1 { get; set; }
        public BarFlagsEnum? FlagsEnum2 { get; set; }
        public BarEnum? FlagsEnum3 { get; set; }
        public BarFlagsEnum? FlagsEnum4 { get; set; }

        public string? FlagsEnumE2S { get; set; }
        public string? FlagsEnumEq2S { get; set; }
        public string? FlagsEnumEqnull2S { get; set; }
        public BarFlagsEnum FlagsEnumS2E { get; set; }
        public BarFlagsEnum? FlagsEnumSq2E { get; set; }
        public BarFlagsEnum? FlagsEnumSq2Enull { get; set; }

        public int Int1 { get; set; }
        public int? Int2 { get; set; }
        public int Int3 { get; set; }
        public int? Int4 { get; set; }

        public Bar? Child { get; set; }

        public Bar[]? Array { get; set; }
        public List<Bar>? List { get; set; }
        public List<Bar>? ArrayToList { get; set; }

        public IList<int>? IListToIList { get; set; }
        public IList<int>? ListToIList { get; set; }
        public List<int>? IListToList { get; set; }

        public Bar[]? ListToArray { get; set; }
        public List<BarEnum>? EnumArrayToList { get; set; }
        public BarEnum[]? EnumListToArray { get; set; }
        public List<string>? EnumListToString { get; set; }
        public List<BarEnum>? StringListToEnum { get; set; }
        public Dictionary<string, string?>? ScalarDictionary { get; set; }
        public Dictionary<Bar, string?>? ObjectStringDictionary { get; set; }
        public Dictionary<string, Foo?>? StringObjectDictionary { get; set; }

        public IDictionary<string, int>? IDictionaryToIDictionary { get; set; }
        public Dictionary<string, int>? IDictionaryToDictionary { get; set; }
        public IDictionary<string, int>? DictionaryToIDictionary { get; set; }
        public Dictionary<string, int>? DictionaryCaseInsensitive { get; set; }
        public CaseInsensitiveDictionary<string, int>? CaseInsensitiveDictionary { get; set; }

        public HashSet<string>? ScalarSet { get; set; }
        public HashSet<Bar>? ObjectSet { get; set; }

        public ISet<string>? ISetToISet { get; set; }
        public HashSet<string>? ISetToHashSet { get; set; }
        public ISet<string>? HashSetToISet { get; set; }
        public HashSet<string>? HashSetCaseInsensitive { get; set; }
        public CaseInsensitiveHashSet<string>? CaseInsensitiveHashSet { get; set; }

        public string[]? StringArray { get; set; }
        public List<string>? StringList { get; set; }
        public List<int>? IntList { get; set; }
        public int[]? IntArray { get; set; }
        public List<int?>? NullableIntList { get; set; }
        public int?[]? NullableIntArray { get; set; }

        public object? ObjectString { get; set; }
        public object? ObjectEnum { get; set; }
        public object? ObjectInt { get; set; }
        public object? ObjectChild { get; set; }
        public object? ObjectArray { get; set; }
    }

    private Foo MakeFoo()
    {
        return new Foo()
        {
            String = "test",
            Object = "obj",
            Enum1 = Foo.FooEnum.Value1,
            Enum2 = Bar.BarEnum.Value2,
            Enum3 = Foo.FooEnum.Value4,
            Enum4 = Foo.FooEnum.Value4,
            Enum5 = null,
            Enum6 = Foo.FooEnum.Value1,

            EnumE2S = Foo.FooEnum.Value1,
            EnumEq2S = Foo.FooEnum.Value2,
            EnumEqnull2S = null,
            EnumS2E = "Value3",
            EnumSq2E = "Value2",
            EnumSq2Enull = null,

            FlagsEnum1 = Foo.FooFlagsEnum.Value1 | Foo.FooFlagsEnum.Value3,
            FlagsEnum2 = Foo.FooFlagsEnum.Value1 | Foo.FooFlagsEnum.Value4,
            FlagsEnum3 = Foo.FooFlagsEnum.Value2,
            FlagsEnum4 = Foo.FooEnum.Value2,

            FlagsEnumE2S = Foo.FooFlagsEnum.Value1 | Foo.FooFlagsEnum.Value2,
            FlagsEnumEq2S = Foo.FooFlagsEnum.Value2 | Foo.FooFlagsEnum.Value3,
            FlagsEnumEqnull2S = null,
            FlagsEnumS2E = "Value2, Value3",
            FlagsEnumSq2E = "Value2",
            FlagsEnumSq2Enull = null,

            Int1 = 1,
            Int2 = 2,
            Int3 = null,
            Int4 = null,
            Child = new Foo()
            {
                String = "test",
                Enum1 = Foo.FooEnum.Value1,
                Enum2 = Bar.BarEnum.Value2,
                Enum3 = Foo.FooEnum.Value4,
                Enum4 = Foo.FooEnum.Value4,
                Enum5 = null,
                Enum6 = Foo.FooEnum.Value1,
                Int1 = 1,
                Int2 = 2,
                Int3 = null,
                Int4 = null,
                IntList = [5, 6]
            },
            Array = [new Foo { Int1 = 5 }, new Foo { Int1 = 8, Enum1 = Foo.FooEnum.Value1 }],
            List = [new Foo { Int1 = 99 }, new Foo { Int1 = 999, Enum3 = Foo.FooEnum.Value3 }],
            ArrayToList = [new Foo { Int1 = 6 }, new Foo { Int1 = 9, Enum1 = Foo.FooEnum.Value2 }],
            ListToArray = [new Foo { Int1 = 199 }, new Foo { Int1 = 1999, Enum3 = Foo.FooEnum.Value3 }],

#pragma warning disable IDE0028 // Simplify collection initialization
            IListToIList = new List<int>() { 1, 3, 5 },
            ListToIList = [2, 4, 6],
            IListToList = new List<int>() { 8, 9, 10 },
#pragma warning restore IDE0028 // Simplify collection initialization

            EnumArrayToList = [Foo.FooEnum.Value2, Foo.FooEnum.Value3],
            EnumListToArray = [Foo.FooEnum.Value1, Foo.FooEnum.Value3, Foo.FooEnum.Value4],
            EnumListToString = [Foo.FooEnum.Value1, Foo.FooEnum.Value3],
            StringListToEnum = ["Value2", "Value3"],
            ScalarDictionary = new Dictionary<string, string?>() { { "1", "12" }, { "2", "22" } },
            ObjectStringDictionary = new Dictionary<Foo, string?>() { { new Foo { Int1 = 5 }, "a" } },
            StringObjectDictionary = new Dictionary<string, Foo?>() { { "a", null }, { "b", new Foo { Int1 = 5 } }, { "c", null } },

            IDictionaryToIDictionary = new Dictionary<string, int>() { { "a", 5 }, { "b", 7 } },
            IDictionaryToDictionary = new Dictionary<string, int>() { { "c", 6 }, { "d", 8 } },
            DictionaryToIDictionary = new Dictionary<string, int>() { { "e", 9 }, { "f", 10 } },
            DictionaryCaseInsensitive = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase) { { "UpPeR", 55 }, { "camelCase", 77 } },
            CaseInsensitiveDictionary = new CaseInsensitiveDictionary<string, int>() { { "UpPeR", 55 }, { "camelCase", 77 } },

            ScalarSet = ["a", "b", "c"],
            ObjectSet = new HashSet<Foo>() { { new Foo { Int1 = 7 } } },

            ISetToISet = new HashSet<string>() { "a1", "b1", "c1" },
            HashSetToISet = ["a2", "b2", "c2"],
            ISetToHashSet = new HashSet<string>() { "a3", "b3", "c3" },
            HashSetCaseInsensitive = ["UpPeR", "camelCase"],
            CaseInsensitiveHashSet = ["UpPeR", "camelCase"],

            StringList = ["A", "B", "C"],
            StringArray = ["D", "E", "F"],
            IntList = [1, 2, 3],
            IntArray = [4, 5, 6],
            NullableIntList = [1, null, 3],
            NullableIntArray = [4, null, 6],

            ObjectString = "test",
            ObjectEnum = Foo.FooEnum.Value1,
            ObjectInt = 1,
            ObjectChild = new Foo()
            {
                String = "test",
                Enum1 = Foo.FooEnum.Value1,
                Enum2 = Bar.BarEnum.Value2,
                Enum3 = Foo.FooEnum.Value4,
                Enum4 = Foo.FooEnum.Value4,
                Enum5 = null,
                Enum6 = Foo.FooEnum.Value1,
                Int1 = 1,
                Int2 = 2,
                Int3 = null,
                Int4 = null,
                IntList = [5, 6]
            },
            ObjectArray = new Foo[] { new Foo { Int1 = 5 }, new Foo { Int1 = 8, Enum1 = Foo.FooEnum.Value1 } },
        };
    }

    [Test]
    public void Test_MapList()
    {
        var foos = new List<Foo?>() { MakeFoo(), MakeFoo(), null };

        var bars = MapperExt.MapList<Foo, Bar>(foos);

        Assert.That(bars?.Count, Is.EqualTo(3));
        AssertFooToBar(foos[0], bars?[0]);
        AssertFooToBar(foos[1], bars?[1]);
        Assert.That(bars?[2], Is.Null);

        Assert.That(MapperExt.MapList<Foo, Bar>(null), Is.Null);

        // test IEnumerable
        var bars2 = MapperExt.MapList<Foo, Bar>(foos.Skip(1).Take(2));

        Assert.That(bars2?.Count, Is.EqualTo(2));
        AssertFooToBar(foos[0], bars2?[0]);
        Assert.That(bars2?[1], Is.Null);
    }

    [Test]
    public void Test_MapListInParallel()
    {
        var foos = new List<Foo?>();
        for (int i = 0; i < 10; i++)
            foos.Add(MakeFoo());
        foos.Add(null);

        var bars = MapperExt.MapListInParallel<Foo, Bar>(foos);
        Assert.That(bars?.Count, Is.EqualTo(11));
        for (int i = 0; i < 10; i++)
            AssertFooToBar(foos[i], bars?[i]);
        Assert.That(bars?[10], Is.Null);

        Assert.That(MapperExt.MapListInParallel<Foo, Bar>(null), Is.Null);

        // test IEnumerable
        var bars2 = MapperExt.MapListInParallel<Foo, Bar>(foos.Skip(8).Take(3));
        Assert.That(bars2?.Count, Is.EqualTo(3));
        for (int i = 0; i < 2; i++)
            AssertFooToBar(foos[i + 8], bars2?[i]);
        Assert.That(bars2?[2], Is.Null);
    }

    [Test]
    [TestCase(8, 8, 8)]
    [TestCase(8, 8, 1)]
    [TestCase(8, 1, 8)]
    [TestCase(8, 1, 1)]
    [TestCase(1, 8, 8)]
    [TestCase(1, 8, 1)]
    [TestCase(1, 1, 8)]
    [TestCase(1, 1, 1)]
    public void Test_MapListInParallel_DiffMinimums(int list, int dict, int props)
    {
        var foos = new List<Foo?>();
        for (int i = 0; i < 10; i++)
            foos.Add(MakeFoo());
        foos.Add(null);

        var bars = MapperExt.MapListInParallel<Foo, Bar>(foos, list, dict, props);

        Assert.That(bars?.Count, Is.EqualTo(11));
        for (int i = 0; i < 10; i++)
            AssertFooToBar(foos[i], bars?[i]);
        Assert.That(bars?[10], Is.Null);
    }

    [Test]
    public void Test_Inspect_ValueTypes()
    {
        Assert.That(PropertyNameInjection.IsTypeValueType(typeof(string)));
        Assert.That(PropertyNameInjection.IsTypeValueType(typeof(int)));
        Assert.That(PropertyNameInjection.IsTypeValueType(typeof(int?)));
        Assert.That(PropertyNameInjection.IsTypeValueType(typeof(long)));
        Assert.That(PropertyNameInjection.IsTypeValueType(typeof(long?)));
        Assert.That(PropertyNameInjection.IsTypeValueType(typeof(decimal)));
        Assert.That(PropertyNameInjection.IsTypeValueType(typeof(decimal?)));
        Assert.That(PropertyNameInjection.IsTypeValueType(typeof(Foo.FooEnum)));
        Assert.That(PropertyNameInjection.IsTypeValueType(typeof(object)), Is.False);
    }

    [Test]
    public void Test_Inspect_IList()
    {
        Assert.That(PropertyNameInjection.IsTypeIList(typeof(IList<int>)));
        Assert.That(PropertyNameInjection.IsTypeIList(typeof(List<int>)));
        Assert.That(PropertyNameInjection.IsTypeIList(typeof(Collection<int>)));
        Assert.That(PropertyNameInjection.IsTypeIList(typeof(ReadOnlyCollection<int>)));
        Assert.That(PropertyNameInjection.IsTypeIList(typeof(HashSet<int>)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeIList(typeof(ISet<int>)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeIList(typeof(object)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeIList(typeof(Dictionary<int, string>)), Is.False);
    }

    [Test]
    public void Test_Inspect_ISet()
    {
        Assert.That(PropertyNameInjection.IsTypeISet(typeof(ISet<int>)));
        Assert.That(PropertyNameInjection.IsTypeISet(typeof(HashSet<int>)));
        Assert.That(PropertyNameInjection.IsTypeISet(typeof(List<int>)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeISet(typeof(IList<int>)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeISet(typeof(object)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeISet(typeof(Dictionary<int, string>)), Is.False);
    }

    [Test]
    public void Test_Inspect_IListOrISet()
    {
        Assert.That(PropertyNameInjection.IsTypeIListOrISet(typeof(IList<int>)));
        Assert.That(PropertyNameInjection.IsTypeIListOrISet(typeof(List<int>)));
        Assert.That(PropertyNameInjection.IsTypeIListOrISet(typeof(Collection<int>)));
        Assert.That(PropertyNameInjection.IsTypeIListOrISet(typeof(ReadOnlyCollection<int>)));
        Assert.That(PropertyNameInjection.IsTypeIListOrISet(typeof(ISet<int>)));
        Assert.That(PropertyNameInjection.IsTypeIListOrISet(typeof(HashSet<int>)));
        Assert.That(PropertyNameInjection.IsTypeIListOrISet(typeof(object)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeIListOrISet(typeof(Dictionary<int, string>)), Is.False);
    }

    [Test]
    public void Test_Inspect_IDictionary()
    {
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(Dictionary<,>)));
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(Dictionary<int, string>)));
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(Dictionary<Foo, Bar>)));
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(Dictionary<Foo.FooEnum, object>)));
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(Dictionary<object, object>)));
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(ReadOnlyDictionary<,>)));
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(ReadOnlyDictionary<int, string>)));
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(ReadOnlyDictionary<Foo, Bar>)));
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(ReadOnlyDictionary<Foo.FooEnum, object>)));
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(ReadOnlyDictionary<object, object>)));
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(ReadOnlyDictionary<object, object>)));
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(ReadOnlyDictionary<,>)));
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(IDictionary<object, object>)));
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(IDictionary<,>)));

        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(List<int>)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(Collection<int>)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(ReadOnlyCollection<int>)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(HashSet<int>)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(object)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(Foo)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeIDictionary(typeof(Bar)), Is.False);
    }

    [Test]
    public void Test_Inspect_IsTypeDeepInjectable()
    {
        Assert.That(PropertyNameInjection.IsTypeDeepInjectable(typeof(object)));
        Assert.That(PropertyNameInjection.IsTypeDeepInjectable(typeof(Foo)));
        Assert.That(PropertyNameInjection.IsTypeDeepInjectable(typeof(Bar)));

        Assert.That(PropertyNameInjection.IsTypeDeepInjectable(typeof(string)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeDeepInjectable(typeof(int)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeDeepInjectable(typeof(long)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeDeepInjectable(typeof(decimal)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeDeepInjectable(typeof(Foo.FooEnum)), Is.False);

        Assert.That(PropertyNameInjection.IsTypeDeepInjectable(typeof(List<int>)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeDeepInjectable(typeof(Collection<int>)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeDeepInjectable(typeof(ReadOnlyCollection<int>)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeDeepInjectable(typeof(HashSet<int>)), Is.False);
        Assert.That(PropertyNameInjection.IsTypeDeepInjectable(typeof(Dictionary<int, string>)), Is.False);
    }

    [Test]
    public void Test_Match_Enum()
    {
        Assert.That(PropertyNameInjection.CanEnumInject(typeof(Foo.FooEnum), typeof(Bar.BarEnum)));
        Assert.That(PropertyNameInjection.CanEnumInject(typeof(Foo.FooEnum), typeof(string)));
        Assert.That(PropertyNameInjection.CanEnumInject(typeof(string), typeof(Bar.BarEnum)));

        Assert.That(PropertyNameInjection.CanEnumInject(typeof(Foo.FooEnum), typeof(int)), Is.False);
    }

    [Test]
    public void Test_Match_Collection()
    {
        var pni = new PropertyNameInjection();

        Assert.That(pni.CanCollectionInject(typeof(List<int>), typeof(List<int>)));
        Assert.That(pni.CanCollectionInject(typeof(List<Foo.FooEnum>), typeof(List<string>)));
        Assert.That(pni.CanCollectionInject(typeof(List<List<Foo.FooEnum>>), typeof(List<List<string>>)));
        Assert.That(pni.CanCollectionInject(typeof(List<int>), typeof(int[])));
        Assert.That(pni.CanCollectionInject(typeof(int[]), typeof(List<int>)));
        Assert.That(pni.CanCollectionInject(typeof(int[]), typeof(int[])));

        Assert.That(pni.CanCollectionInject(typeof(int[]), typeof(object)), Is.False);
        Assert.That(pni.CanCollectionInject(typeof(List<int>), typeof(object)), Is.False);

        Assert.That(pni.CanCollectionInject(typeof(HashSet<int>), typeof(HashSet<int>)));
        Assert.That(pni.CanCollectionInject(typeof(HashSet<int>), typeof(ISet<int>)));
        Assert.That(pni.CanCollectionInject(typeof(ISet<int>), typeof(ISet<int>)));
        Assert.That(pni.CanCollectionInject(typeof(ISet<int>), typeof(HashSet<int>)));
    }

    [Test]
    public void Test_Match_Dictionary()
    {
        var pni = new PropertyNameInjection();

        Assert.That(pni.CanDictionaryInject(typeof(Dictionary<int, object>), typeof(Dictionary<int, object>)));
        Assert.That(pni.CanDictionaryInject(typeof(Dictionary<string, object>), typeof(Dictionary<Foo.FooEnum, object>)));
        Assert.That(pni.CanDictionaryInject(typeof(Dictionary<Foo, int>), typeof(Dictionary<Bar, int?>)));

        Assert.That(pni.CanDictionaryInject(typeof(Dictionary<Foo, int>), typeof(IDictionary<Bar, int?>)));
        Assert.That(pni.CanDictionaryInject(typeof(IDictionary<Foo, int>), typeof(IDictionary<Bar, int?>)));
        Assert.That(pni.CanDictionaryInject(typeof(IDictionary<Foo, int>), typeof(Dictionary<Bar, int?>)));

        Assert.That(pni.CanDictionaryInject(typeof(Dictionary<int, object>), typeof(List<int>)), Is.False);
    }

    [Test]
    [TestCase(false, false, false)]
    [TestCase(false, false, true)]
    [TestCase(false, true, false)]
    [TestCase(false, true, true)]
    [TestCase(true, false, false)]
    [TestCase(true, false, true)]
    [TestCase(true, true, false)]
    [TestCase(true, true, true)]
#pragma warning disable CS8604 // Possible null reference argument.
    public void Test_Inject(bool parallelList, bool parallelDictionary, bool parallelProps)
    {
        var foo = MakeFoo();

        var bar = new Bar();
        PropertyNameInjection pni;
        if (!parallelList && !parallelProps)
            pni = new PropertyNameInjection();
        else
            pni = new PropertyNameInjection(
                parallelList ? 0 : int.MaxValue,
                parallelDictionary ? 0 : int.MaxValue,
                parallelProps ? 0 : int.MaxValue);

        bar.InjectFrom(pni, foo);

        AssertFooToBar(foo, bar);
    }

    [Test]
    [TestCase(8, 8)]
    [TestCase(0, 0)]
    [TestCase(0, 8)]
    [TestCase(8, 0)]
    [TestCase(int.MaxValue, 8)]
    [TestCase(8, int.MaxValue)]
    public void Test_MapperExtension(int listMin, int propMin)
    {
        var foo = MakeFoo();

        Bar bar = MapperExt.MapInParallel<Bar>(foo, listMin, propMin);

        AssertFooToBar(foo, bar);
    }

    [Test]
    public void Test_MapperExt_NullSafe()
    {
        Foo? foo = null;
        Bar? bar = MapperExt.MapNullSafe<Bar>(foo);
        Assert.That(bar, Is.Null);

        foo = MakeFoo();
        bar = MapperExt.MapNullSafe<Bar>(foo);
        Assert.That(bar, Is.Not.Null);
    }

    private void AssertFooToBar(Foo? foo, Bar? bar)
    {
        if (bar == null)
        {
            Assert.Fail("bar is null");
            return;
        }
        if (foo == null)
        {
            Assert.Fail("foo is null");
            return;
        }
        Assert.That(bar.String, Is.EqualTo("test"));
        Assert.That(bar.Object, Is.EqualTo("obj"));
        Assert.That(bar.Enum1, Is.EqualTo(Bar.BarEnum.Value1));
        Assert.That(bar.Enum2, Is.EqualTo(Bar.BarEnum.Value2));
        Assert.That(bar.Enum3, Is.EqualTo(Bar.BarEnum.Unknown));
        Assert.That(bar.Enum4, Is.Null);
        Assert.That(bar.Enum5, Is.EqualTo(Bar.BarEnum.Unknown));
        Assert.That(bar.Enum6, Is.EqualTo(Bar.BarEnum.Value1));

        Assert.That(bar.EnumE2S, Is.EqualTo("Value1"));
        Assert.That(bar.EnumEq2S, Is.EqualTo("Value2"));
        Assert.That(bar.EnumEqnull2S, Is.Null);
        Assert.That(bar.EnumS2E, Is.EqualTo(Bar.BarEnum.Value3));
        Assert.That(bar.EnumSq2E, Is.EqualTo(Bar.BarEnum.Value2));
        Assert.That(bar.EnumSq2Enull, Is.Null);

        Assert.That(bar.FlagsEnum1, Is.EqualTo(Bar.BarFlagsEnum.Value1 | Bar.BarFlagsEnum.Value3));
        Assert.That(bar.FlagsEnum2, Is.Null);
        Assert.That(bar.FlagsEnum3, Is.EqualTo(Bar.BarEnum.Value2));
        Assert.That(bar.FlagsEnum4, Is.EqualTo(Bar.BarFlagsEnum.Value2));

        Assert.That(bar.FlagsEnumE2S, Is.EqualTo("Value1, Value2"));
        Assert.That(bar.FlagsEnumEq2S, Is.EqualTo("Value2, Value3"));
        Assert.That(bar.FlagsEnumEqnull2S, Is.Null);
        Assert.That(bar.FlagsEnumS2E, Is.EqualTo(Bar.BarFlagsEnum.Value2 | Bar.BarFlagsEnum.Value3));
        Assert.That(bar.FlagsEnumSq2E, Is.EqualTo(Bar.BarFlagsEnum.Value2));
        Assert.That(bar.FlagsEnumSq2Enull, Is.Null);

        Assert.That(bar.Int1, Is.EqualTo(1));
        Assert.That(bar.Int2, Is.EqualTo(2));
        Assert.That(bar.Int3, Is.EqualTo(0));
        Assert.That(bar.Int4, Is.EqualTo(null));

        Assert.That(bar.Child?.String, Is.EqualTo("test"));

        Assert.That(bar.Child?.Enum1, Is.EqualTo(Bar.BarEnum.Value1));
        Assert.That(bar.Child?.Enum2, Is.EqualTo(Bar.BarEnum.Value2));
        Assert.That(bar.Child?.Enum3, Is.EqualTo(Bar.BarEnum.Unknown));
        Assert.That(bar.Enum4, Is.Null);
        Assert.That(bar.Child?.Enum5, Is.EqualTo(Bar.BarEnum.Unknown));
        Assert.That(bar.Child?.Enum6, Is.EqualTo(Bar.BarEnum.Value1));

        Assert.That(bar.Child?.Int1, Is.EqualTo(1));
        Assert.That(bar.Child?.Int2, Is.EqualTo(2));
        Assert.That(bar.Child?.Int3, Is.EqualTo(0));
        Assert.That(bar.Child?.Int4, Is.EqualTo(null));

        Assert.That(bar.Array?.Length, Is.EqualTo(2));
        Assert.That(bar.Array?[0].Int1, Is.EqualTo(5));
        Assert.That(bar.Array?[1].Int1, Is.EqualTo(8));
        Assert.That(bar.Array?[1].Enum1, Is.EqualTo(Bar.BarEnum.Value1));

        Assert.That(bar.List?.Count, Is.EqualTo(2));
        Assert.That(bar.List?[0].Int1, Is.EqualTo(99));
        Assert.That(bar.List?[1].Int1, Is.EqualTo(999));
        Assert.That(bar.List?[1].Enum3, Is.EqualTo(Bar.BarEnum.Value3));

        Assert.That(bar.ArrayToList?.Count, Is.EqualTo(2));
        Assert.That(bar.ArrayToList?[0].Int1, Is.EqualTo(6));
        Assert.That(bar.ArrayToList?[1].Int1, Is.EqualTo(9));
        Assert.That(bar.ArrayToList?[1].Enum1, Is.EqualTo(Bar.BarEnum.Value2));

        Assert.That(bar.ListToArray?.Length, Is.EqualTo(2));
        Assert.That(bar.ListToArray?[0].Int1, Is.EqualTo(199));
        Assert.That(bar.ListToArray?[1].Int1, Is.EqualTo(1999));
        Assert.That(bar.ListToArray?[1].Enum3, Is.EqualTo(Bar.BarEnum.Value3));

        Assert.That(bar.IListToIList, Is.Not.SameAs(foo.IListToIList));
        Assert.That(bar.IListToIList, Is.EqualTo(foo.IListToIList).AsCollection);
        Assert.That(bar.ListToIList, Is.Not.SameAs(foo.ListToIList));
        Assert.That(bar.ListToIList, Is.EqualTo(foo.ListToIList).AsCollection);
        Assert.That(bar.IListToList, Is.Not.SameAs(foo.IListToList));
        Assert.That(bar.IListToList, Is.EqualTo(foo.IListToList).AsCollection);

        Assert.That(bar.EnumArrayToList?.Count, Is.EqualTo(2));
        Assert.That(bar.EnumArrayToList?[0], Is.EqualTo(Bar.BarEnum.Value2));
        Assert.That(bar.EnumArrayToList?[1], Is.EqualTo(Bar.BarEnum.Value3));

        Assert.That(bar.EnumListToArray?.Length, Is.EqualTo(3));
        Assert.That(bar.EnumListToArray?[0], Is.EqualTo(Bar.BarEnum.Value1));
        Assert.That(bar.EnumListToArray?[1], Is.EqualTo(Bar.BarEnum.Value3));
        Assert.That(bar.EnumListToArray?[2], Is.EqualTo(Bar.BarEnum.Unknown));

        Assert.That(bar.EnumListToString?.Count, Is.EqualTo(2));
        Assert.That(bar.EnumListToString?[0], Is.EqualTo("Value1"));
        Assert.That(bar.EnumListToString?[1], Is.EqualTo("Value3"));

        Assert.That(bar.StringListToEnum?.Count, Is.EqualTo(2));
        Assert.That(bar.StringListToEnum?[0], Is.EqualTo(Bar.BarEnum.Value2));
        Assert.That(bar.StringListToEnum?[1], Is.EqualTo(Bar.BarEnum.Value3));

        Assert.That(bar.ScalarDictionary!, Is.Not.Null);
        Assert.That(bar.ScalarDictionary, Is.Not.SameAs(foo.ScalarDictionary));
        Assert.That(bar.ScalarDictionary.Keys, Has.Count.EqualTo(2));
        Assert.That(bar.ScalarDictionary.ContainsKey("2"));
        Assert.That(bar.ScalarDictionary["2"], Is.EqualTo("22"));

        // Assert.AreNotSame(foo.ObjectStringDictionary, bar.ObjectStringDictionary); // always fails
        Assert.That(bar.ObjectStringDictionary, Is.Not.Null);
        Assert.That(bar.ObjectStringDictionary.Keys, Has.Count.EqualTo(1));
        Assert.That(bar.ObjectStringDictionary.Keys.First().Int1, Is.EqualTo(5));
        Assert.That(bar.ObjectStringDictionary.ContainsValue("a"));

        Assert.That(bar.StringObjectDictionary, Is.Not.SameAs(foo.StringObjectDictionary));
        Assert.That(bar.StringObjectDictionary.Keys, Has.Count.EqualTo(3));
        Assert.That(bar.StringObjectDictionary.ContainsKey("c"));
        Assert.That(bar.StringObjectDictionary["b"]?.Int1, Is.EqualTo(5));

        Assert.That(bar.IDictionaryToIDictionary, Is.Not.SameAs(foo.IDictionaryToIDictionary));
        Assert.That(bar.IDictionaryToIDictionary?.Keys.Count, Is.EqualTo(2));
        Assert.That(bar.IDictionaryToIDictionary?["a"], Is.EqualTo(5));
        Assert.That(bar.IDictionaryToIDictionary?["b"], Is.EqualTo(7));

        Assert.That(bar.IDictionaryToDictionary, Is.Not.SameAs(foo.IDictionaryToDictionary));
        Assert.That(bar.IDictionaryToDictionary?.Keys.Count, Is.EqualTo(2));
        Assert.That(bar.IDictionaryToDictionary?["c"], Is.EqualTo(6));
        Assert.That(bar.IDictionaryToDictionary?["d"], Is.EqualTo(8));

        Assert.That(bar.DictionaryToIDictionary, Is.Not.SameAs(foo.DictionaryToIDictionary));
        Assert.That(bar.DictionaryToIDictionary?.Keys.Count, Is.EqualTo(2));
        Assert.That(bar.DictionaryToIDictionary?["e"], Is.EqualTo(9));
        Assert.That(bar.DictionaryToIDictionary?["f"], Is.EqualTo(10));

        // mapping the comparator is not supported
        Assert.That(bar.DictionaryCaseInsensitive, Is.Not.Null);
        Assert.That(bar.DictionaryCaseInsensitive, Is.Not.SameAs(foo.DictionaryCaseInsensitive));
        Assert.That(bar.DictionaryCaseInsensitive.Keys, Has.Count.EqualTo(2));
        Assert.That(bar.DictionaryCaseInsensitive["UpPeR"], Is.EqualTo(55));
        //Assert.AreEqual(55, bar.DictionaryCaseInsensitive["UPPER"]);
        Assert.That(bar.DictionaryCaseInsensitive.ContainsKey("upper"), Is.False);
        Assert.That(bar.DictionaryCaseInsensitive["camelCase"], Is.EqualTo(77));
        //Assert.AreEqual(77, bar.DictionaryCaseInsensitive["CAMELcase"]);
        Assert.That(bar.DictionaryCaseInsensitive.ContainsKey("cAmElCaSe"), Is.False);
        Assert.That(bar.DictionaryCaseInsensitive.ContainsKey("notThere"), Is.False);

        Assert.That(bar.CaseInsensitiveDictionary, Is.Not.Null);
        Assert.That(bar.CaseInsensitiveDictionary, Is.Not.SameAs(foo.CaseInsensitiveDictionary));
        Assert.That(bar.CaseInsensitiveDictionary.Keys, Has.Count.EqualTo(2));
        Assert.That(bar.CaseInsensitiveDictionary["UpPeR"], Is.EqualTo(55));
        Assert.That(bar.CaseInsensitiveDictionary["UPPER"], Is.EqualTo(55));
        Assert.That(bar.CaseInsensitiveDictionary.ContainsKey("upper"));
        Assert.That(bar.CaseInsensitiveDictionary["camelCase"], Is.EqualTo(77));
        Assert.That(bar.CaseInsensitiveDictionary["CAMELcase"], Is.EqualTo(77));
        Assert.That(bar.CaseInsensitiveDictionary.ContainsKey("cAmElCaSe"));
        Assert.That(bar.CaseInsensitiveDictionary.ContainsKey("notThere"), Is.False);

        Assert.That(bar.ScalarSet, Is.Not.Null);
        Assert.That(bar.ScalarSet, Is.EqualTo(foo.ScalarSet).AsCollection);
        Assert.That(bar.ScalarSet, Is.Not.SameAs(foo.ScalarSet));
        Assert.That(bar.ScalarSet, Does.Contain("b"));
        Assert.That(bar.ScalarSet, Does.Not.Contain("d"));
        Assert.That(bar.ObjectSet, Is.Not.Null);
        Assert.That(bar.ObjectSet, Has.Count.EqualTo(1));
        //Assert.AreNotSame(foo.ObjectSet, bar.ObjectSet); // always passes
        Assert.That(bar.ObjectSet.First().Int1, Is.EqualTo(7));

        Assert.That(bar.ISetToISet, Is.EqualTo(foo.ISetToISet).AsCollection);
        Assert.That(bar.ISetToISet, Is.Not.SameAs(foo.ISetToISet));
        Assert.That(bar.HashSetToISet, Is.EqualTo(foo.HashSetToISet).AsCollection);
        Assert.That(bar.HashSetToISet, Is.Not.SameAs(foo.HashSetToISet));
        Assert.That(bar.ISetToHashSet, Is.EqualTo(foo.ISetToHashSet).AsCollection);
        Assert.That(bar.ISetToHashSet, Is.Not.SameAs(foo.ISetToHashSet));

        // mapping the comparator is not supported
        Assert.That(bar.HashSetCaseInsensitive, Is.Not.Null);
        Assert.That(bar.HashSetCaseInsensitive, Is.Not.SameAs(foo.HashSetCaseInsensitive));
        Assert.That(bar.HashSetCaseInsensitive, Has.Count.EqualTo(2));
        Assert.That(bar.HashSetCaseInsensitive, Does.Contain("UpPeR"));
        //Assert.IsTrue(bar.HashSetCaseInsensitive.Contains("UPPER"));
        Assert.That(bar.HashSetCaseInsensitive, Does.Not.Contain("notThere"));

        Assert.That(bar.CaseInsensitiveHashSet, Is.Not.Null);
        Assert.That(bar.CaseInsensitiveHashSet, Is.Not.SameAs(foo.CaseInsensitiveHashSet));
        Assert.That(bar.CaseInsensitiveHashSet, Has.Count.EqualTo(2));
        Assert.That(bar.CaseInsensitiveHashSet, Does.Contain("UpPeR").IgnoreCase);
        Assert.That(bar.CaseInsensitiveHashSet, Does.Contain("UPPER").IgnoreCase);
        Assert.That(bar.CaseInsensitiveHashSet, Does.Not.Contain("notThere").IgnoreCase);

        Assert.That(bar.StringList, Is.EqualTo(foo.StringList).AsCollection);
        Assert.That(bar.StringList, Is.Not.SameAs(foo.StringList));
        Assert.That(bar.IntList, Is.EqualTo(foo.IntList).AsCollection);
        Assert.That(bar.IntList, Is.Not.SameAs(foo.IntList));
        Assert.That(bar.NullableIntList, Is.EqualTo(foo.NullableIntList).AsCollection);
        Assert.That(bar.NullableIntList, Is.Not.SameAs(foo.NullableIntList));

        Assert.That(bar.Child?.IntList, Is.EqualTo(foo.Child?.IntList).AsCollection);
        // Assert.AreNotSame(foo.StringList, bar.IntList); // always passes

        Assert.That(bar.StringArray, Is.EqualTo(foo.StringArray).AsCollection);
        Assert.That(bar.StringArray, Is.Not.SameAs(foo.StringArray));
        Assert.That(bar.IntArray, Is.EqualTo(foo.IntArray).AsCollection);
        Assert.That(bar.IntArray, Is.Not.SameAs(foo.IntArray));
        Assert.That(bar.NullableIntArray, Is.EqualTo(foo.NullableIntArray).AsCollection);
        Assert.That(bar.NullableIntArray, Is.Not.SameAs(foo.NullableIntArray));

        Assert.That(bar.ObjectString, Is.EqualTo("test"));
        Assert.That(bar.ObjectEnum, Is.EqualTo(Foo.FooEnum.Value1));
        Assert.That(bar.ObjectInt, Is.EqualTo(1));
        Assert.That(((Foo?)bar.ObjectChild)?.String, Is.EqualTo("test"));
        Assert.That(((Foo[]?)bar.ObjectArray)?.Length, Is.EqualTo(2));
    }
#pragma warning restore CS8604 // Possible null reference argument.

    //[Test]
    // timing test
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Structure", "NUnit1028:The non-test method is public", Justification = "<Pending>")]
    public void Test_BigList()
    {
        var f = new Foo
        {
            IntList = Enumerable.Repeat(10, 1000000).ToList(),
            List = Enumerable.Repeat(
            new Foo()
            {
                String = "test",
                Enum1 = Foo.FooEnum.Value1,
                Enum2 = Bar.BarEnum.Value2,
                Enum3 = Foo.FooEnum.Value4,
                Enum4 = Foo.FooEnum.Value4,
                Enum5 = null,
                Enum6 = Foo.FooEnum.Value1,

                EnumE2S = Foo.FooEnum.Value1,
                EnumEq2S = Foo.FooEnum.Value2,
                EnumEqnull2S = null,
                EnumS2E = "Value3",
                EnumSq2E = "Value2",
                EnumSq2Enull = null,

                Int1 = 1,
                Int2 = 2,
                Int3 = null,
                Int4 = null,
                Child = new Foo()
                {
                    String = "test",
                    Enum1 = Foo.FooEnum.Value1,
                    Enum2 = Bar.BarEnum.Value2,
                    Enum3 = Foo.FooEnum.Value4,
                    Enum4 = Foo.FooEnum.Value4,
                    Enum5 = null,
                    Enum6 = Foo.FooEnum.Value1,
                    Int1 = 1,
                    Int2 = 2,
                    Int3 = null,
                    Int4 = null,
                    IntList = [5, 6]
                },
                Array = [new Foo { Int1 = 5 }, new Foo { Int1 = 8, Enum1 = Foo.FooEnum.Value1 }],
                List = [new Foo { Int1 = 99 }, new Foo { Int1 = 999, Enum3 = Foo.FooEnum.Value3 }],
                ScalarDictionary = new Dictionary<string, string?>() { { "1", "12" }, { "2", "22" } },
                ObjectStringDictionary = new Dictionary<Foo, string?>() { { new Foo { Int1 = 5 }, "a" } },
                StringObjectDictionary = new Dictionary<string, Foo?>() { { "a", null }, { "b", new Foo { Int1 = 5 } }, { "c", null } },

                StringList = ["A", "B", "C"],
                StringArray = ["D", "E", "F"],
                IntList = Enumerable.Repeat(10, 1000000).ToList(),
                IntArray = [4, 5, 6],
                NullableIntList = [1, null, 3],
                NullableIntArray = [4, null, 6],

                ObjectString = "test",
                ObjectEnum = Foo.FooEnum.Value1,
                ObjectInt = 1,
                ObjectChild = new Foo()
                {
                    String = "test",
                    Enum1 = Foo.FooEnum.Value1,
                    Enum2 = Bar.BarEnum.Value2,
                    Enum3 = Foo.FooEnum.Value4,
                    Enum4 = Foo.FooEnum.Value4,
                    Enum5 = null,
                    Enum6 = Foo.FooEnum.Value1,
                    Int1 = 1,
                    Int2 = 2,
                    Int3 = null,
                    Int4 = null,
                    IntList = [5, 6]
                },
                ObjectArray = new Foo[] { new Foo { Int1 = 5 }, new Foo { Int1 = 8, Enum1 = Foo.FooEnum.Value1 } },
            }, 10).ToList()
        };

        // mapping is heavily benefitted by the JIT, so run a few times
        for (int i = 0; i < 3; i++)
        {
            {
                // slowest
                Stopwatch sw = Stopwatch.StartNew();
                var bar = new Bar();
                bar.InjectFrom(new PropertyNameInjection(), f);
                sw.Stop();
                Console.WriteLine($"Serial: {sw.Elapsed}");
            }

            {
                // faster
                Stopwatch sw = Stopwatch.StartNew();
                var bar = new Bar();
                bar.InjectFrom(new PropertyNameInjection(8, int.MaxValue, int.MaxValue), f);
                sw.Stop();
                Console.WriteLine($"Parallel List Only: {sw.Elapsed}");
            }

            {
                // slightly faster
                Stopwatch sw = Stopwatch.StartNew();
                var bar = new Bar();
                bar.InjectFrom(new PropertyNameInjection(8, int.MaxValue, 8), f);
                sw.Stop();
                Console.WriteLine($"Parallel Lists/Props: {sw.Elapsed}");
            }
        }
    }

    //[Test]
    // timing test
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Structure", "NUnit1028:The non-test method is public", Justification = "<Pending>")]
    public void Test_BigDictionary()
    {
#pragma warning disable IDE0017 // Simplify object initialization
        var f = new Foo();
#pragma warning restore IDE0017 // Simplify object initialization

        f.StringObjectDictionary = Enumerable.Repeat(
            new Foo()
            {
                String = "test",
                Enum1 = Foo.FooEnum.Value1,
                Enum2 = Bar.BarEnum.Value2,
                Enum3 = Foo.FooEnum.Value4,
                Enum4 = Foo.FooEnum.Value4,
                Enum5 = null,
                Enum6 = Foo.FooEnum.Value1,

                EnumE2S = Foo.FooEnum.Value1,
                EnumEq2S = Foo.FooEnum.Value2,
                EnumEqnull2S = null,
                EnumS2E = "Value3",
                EnumSq2E = "Value2",
                EnumSq2Enull = null,

                Int1 = 1,
                Int2 = 2,
                Int3 = null,
                Int4 = null,
                Child = new Foo()
                {
                    String = "test",
                    Enum1 = Foo.FooEnum.Value1,
                    Enum2 = Bar.BarEnum.Value2,
                    Enum3 = Foo.FooEnum.Value4,
                    Enum4 = Foo.FooEnum.Value4,
                    Enum5 = null,
                    Enum6 = Foo.FooEnum.Value1,
                    Int1 = 1,
                    Int2 = 2,
                    Int3 = null,
                    Int4 = null,
                    IntList = [5, 6]
                },
                Array = [new Foo { Int1 = 5 }, new Foo { Int1 = 8, Enum1 = Foo.FooEnum.Value1 }],
                List = [new Foo { Int1 = 99 }, new Foo { Int1 = 999, Enum3 = Foo.FooEnum.Value3 }],
                ScalarDictionary = new Dictionary<string, string?>() { { "1", "12" }, { "2", "22" } },
                ObjectStringDictionary = new Dictionary<Foo, string?>() { { new Foo { Int1 = 5 }, "a" } },
                StringObjectDictionary = new Dictionary<string, Foo?>() { { "a", null }, { "b", new Foo { Int1 = 5 } }, { "c", null } },

                StringList = ["A", "B", "C"],
                StringArray = ["D", "E", "F"],
                IntList = Enumerable.Repeat(10, 1000000).ToList(),
                IntArray = [4, 5, 6],
                NullableIntList = [1, null, 3],
                NullableIntArray = [4, null, 6],

                ObjectString = "test",
                ObjectEnum = Foo.FooEnum.Value1,
                ObjectInt = 1,
                ObjectChild = new Foo()
                {
                    String = "test",
                    Enum1 = Foo.FooEnum.Value1,
                    Enum2 = Bar.BarEnum.Value2,
                    Enum3 = Foo.FooEnum.Value4,
                    Enum4 = Foo.FooEnum.Value4,
                    Enum5 = null,
                    Enum6 = Foo.FooEnum.Value1,
                    Int1 = 1,
                    Int2 = 2,
                    Int3 = null,
                    Int4 = null,
                    IntList = [5, 6]
                },
                ObjectArray = new Foo[] { new Foo { Int1 = 5 }, new Foo { Int1 = 8, Enum1 = Foo.FooEnum.Value1 } },
            }, 10)
            .Select((foo, i) => new { foo, i })
            .ToDictionary(o => o.i.ToString(), o => o.foo)!;

        // mapping is heavily benefitted by the JIT, so run a few times
        for (int i = 0; i < 3; i++)
        {
            {
                // slowest
                Stopwatch sw = Stopwatch.StartNew();
                var bar = new Bar();
                bar.InjectFrom(new PropertyNameInjection(), f);
                sw.Stop();
                Console.WriteLine($"Serial: {sw.Elapsed}");
            }

            {
                // faster
                Stopwatch sw = Stopwatch.StartNew();
                var bar = new Bar();
                bar.InjectFrom(new PropertyNameInjection(int.MaxValue, 8, int.MaxValue), f);
                sw.Stop();
                Console.WriteLine($"Parallel Dictionary Only: {sw.Elapsed}");
            }

            {
                // slightly faster
                Stopwatch sw = Stopwatch.StartNew();
                var bar = new Bar();
                bar.InjectFrom(new PropertyNameInjection(int.MaxValue, 8, 8), f);
                sw.Stop();
                Console.WriteLine($"Parallel Dictionaries/Props: {sw.Elapsed}");
            }
        }
    }

    public class AllPrimitives
    {
        public int Int0 { get; set; }
        public int Int1 { get; set; }
        public int Int2 { get; set; }
        public int Int3 { get; set; }
        public int Int4 { get; set; }
        public int Int5 { get; set; }
        public int Int6 { get; set; }
        public int Int7 { get; set; }
        public int Int8 { get; set; }
        public int Int9 { get; set; }
        public string? String0 { get; set; }
        public string? String1 { get; set; }
        public string? String2 { get; set; }
        public string? String3 { get; set; }
        public string? String4 { get; set; }
        public string? String5 { get; set; }
        public string? String6 { get; set; }
        public string? String7 { get; set; }
        public string? String8 { get; set; }
        public string? String9 { get; set; }
        public decimal Decimal0 { get; set; }
        public decimal Decimal1 { get; set; }
        public decimal Decimal2 { get; set; }
        public decimal Decimal3 { get; set; }
        public decimal Decimal4 { get; set; }
        public decimal Decimal5 { get; set; }
        public decimal Decimal6 { get; set; }
        public decimal Decimal7 { get; set; }
        public decimal Decimal8 { get; set; }
        public decimal Decimal9 { get; set; }
        public int Int10 { get; set; }
        public int Int11 { get; set; }
        public int Int12 { get; set; }
        public int Int13 { get; set; }
        public int Int14 { get; set; }
        public int Int15 { get; set; }
        public int Int16 { get; set; }
        public int Int17 { get; set; }
        public int Int18 { get; set; }
        public int Int19 { get; set; }
        public string? String10 { get; set; }
        public string? String11 { get; set; }
        public string? String12 { get; set; }
        public string? String13 { get; set; }
        public string? String14 { get; set; }
        public string? String15 { get; set; }
        public string? String16 { get; set; }
        public string? String17 { get; set; }
        public string? String18 { get; set; }
        public string? String19 { get; set; }
        public decimal Decimal10 { get; set; }
        public decimal Decimal11 { get; set; }
        public decimal Decimal12 { get; set; }
        public decimal Decimal13 { get; set; }
        public decimal Decimal14 { get; set; }
        public decimal Decimal15 { get; set; }
        public decimal Decimal16 { get; set; }
        public decimal Decimal17 { get; set; }
        public decimal Decimal18 { get; set; }
        public decimal Decimal19 { get; set; }
    }

    //[Test]
    // timing test
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Structure", "NUnit1028:The non-test method is public", Justification = "<Pending>")]
    public void Test_AllPrimitives()
    {
        var f = new AllPrimitives();

        var r = new Random();
        foreach (var p in typeof(AllPrimitives).GetProperties(System.Reflection.BindingFlags.Public))
        {
            if (p.PropertyType == typeof(string))
                p.SetValue(f, "string" + r.Next().ToString());
            else if (p.PropertyType == typeof(int))
                p.SetValue(f, r.Next());
            else if (p.PropertyType == typeof(decimal))
                p.SetValue(f, (decimal)r.Next());
        }

        // mapping is heavily benefitted by the JIT, so run a few times
        for (int i = 0; i < 3; i++)
        {
            {
                // slowest
                Stopwatch sw = Stopwatch.StartNew();
                var bar = new AllPrimitives();
                bar.InjectFrom(new PropertyNameInjection(), f);
                sw.Stop();
                Console.WriteLine($"Serial: {sw.Elapsed}");
            }

            {
                // faster
                Stopwatch sw = Stopwatch.StartNew();
                var bar = new AllPrimitives();
                bar.InjectFrom(new PropertyNameInjection(8, int.MaxValue, int.MaxValue), f);
                sw.Stop();
                Console.WriteLine($"Parallel List Only: {sw.Elapsed}");
            }

            {
                // faster
                Stopwatch sw = Stopwatch.StartNew();
                var bar = new AllPrimitives();
                bar.InjectFrom(new PropertyNameInjection(int.MaxValue, 8, int.MaxValue), f);
                sw.Stop();
                Console.WriteLine($"Parallel Dictionaries Only: {sw.Elapsed}");
            }

            {
                // slightly faster
                Stopwatch sw = Stopwatch.StartNew();
                var bar = new AllPrimitives();
                bar.InjectFrom(new PropertyNameInjection(8, 8, 8), f);
                sw.Stop();
                Console.WriteLine($"Parallel Lists/Dictionaries/Props: {sw.Elapsed}");
            }
        }
    }
}
