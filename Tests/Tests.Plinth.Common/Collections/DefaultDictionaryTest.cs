using NUnit.Framework;
using System.Collections;

namespace Tests.Plinth.Common.Collections;

[TestFixture]
public class DefaultDictionaryTest
{
    [TestCase(0, 0)]
    [TestCase(0, 2)]
    [TestCase(0, -2)]
    [TestCase(1, 0)]
    [TestCase(1, 2)]
    [TestCase(1, -2)]
    [TestCase(2, 0)]
    [TestCase(2, 2)]
    [TestCase(2, -2)]
    [TestCase(3, 0)]
    [TestCase(3, 2)]
    [TestCase(3, -2)]
    public void Test_Counting(int testCase, int defaultVal)
    {
        Dictionary<string, int>? orig = testCase >= 3 ? new() : null;
        var dict = testCase switch
        {
            0 => new DefaultDictionary<string, int>(defaultValue: defaultVal),
            1 => new DefaultDictionary<string, int>( () => defaultVal ),
            2 => new DefaultDictionary<string, int>(orig!, defaultValue: defaultVal),
            3 => new DefaultDictionary<string, int>(orig!, () => defaultVal ),
            _ => throw new NotSupportedException()
        };

        dict["one"]++;
        dict["two"] = 2;
        dict["three"] += 3;
        dict["minus2"] -= 2;
        dict["fixed"] = 10;

        Assert.That(dict["_zero_"], Is.EqualTo(defaultVal));
        Assert.That(dict["one"], Is.EqualTo(1 + defaultVal));
        Assert.That(dict["two"], Is.EqualTo(2));
        Assert.That(dict["three"], Is.EqualTo(3 + defaultVal));
        Assert.That(dict["minus2"], Is.EqualTo(-2 + defaultVal));
        Assert.That(dict["fixed"], Is.EqualTo(10));

        Assert.That(dict.TryGetValue("try", out var t));
        Assert.That(defaultVal, Is.EqualTo(t));

        if (orig != null)
        {
            Assert.That(orig["_zero_"], Is.EqualTo(defaultVal));
            Assert.That(orig["one"], Is.EqualTo(1 + defaultVal));
            Assert.That(orig["two"], Is.EqualTo(2));
            Assert.That(orig["three"], Is.EqualTo(3 + defaultVal));
            Assert.That(orig["minus2"], Is.EqualTo(-2 + defaultVal));
            Assert.That(orig["fixed"], Is.EqualTo(10));
            Assert.That(orig["try"], Is.EqualTo(defaultVal));
        }

        Assert.That(dict.Values, Is.EquivalentTo(new int[] { defaultVal, 1 + defaultVal, 2, 3 + defaultVal, -2 + defaultVal, 10, defaultVal }));
        Assert.That(dict.Select(kv => kv.Value).ToList(), Is.EquivalentTo(new int[] { defaultVal, 1 + defaultVal, 2, 3 + defaultVal, -2 + defaultVal, 10, defaultVal }));
        var enumerableVals = new List<int>();
        foreach (var o in (IEnumerable)dict)
            enumerableVals.Add(((KeyValuePair<string, int>)o!).Value);
        Assert.That(enumerableVals, Is.EquivalentTo(new int[] { defaultVal, 1 + defaultVal, 2, 3 + defaultVal, -2 + defaultVal, 10, defaultVal }));

        var arrayVals = new KeyValuePair<string,int>[dict.Count];
        dict.CopyTo(arrayVals, 0);
        Assert.That(arrayVals, Is.EquivalentTo(dict));

        Assert.That(dict.Keys, Is.EquivalentTo(new string[] { "_zero_", "one", "two", "three", "minus2", "fixed", "try" }));
        Assert.That(dict.Select(kv => kv.Key).ToList(), Is.EquivalentTo(new string[] { "_zero_", "one", "two", "three", "minus2", "fixed", "try" }));
        var enumerableKeys = new List<string>();
        foreach (var o in (IEnumerable)dict)
            enumerableKeys.Add(((KeyValuePair<string, int>)o!).Key);
        Assert.That(enumerableKeys, Is.EquivalentTo(new string[] { "_zero_", "one", "two", "three", "minus2", "fixed", "try" }));

        dict.Add("added1", 101);
        dict.Add(new KeyValuePair<string, int>("added2", 102));
        Assert.That(dict["added1"], Is.EqualTo(101));
        Assert.That(dict["added2"], Is.EqualTo(102));
        Assert.That(dict, Does.Contain(new KeyValuePair<string, int>("added1", 101)));
        Assert.That(dict, Does.Not.Contain(new KeyValuePair<string, int>("added1", 5101)));

        dict.Remove("added1");
        Assert.That(dict.ContainsKey("added1"), Is.False);
        Assert.That(dict["added1"], Is.EqualTo(defaultVal));
        Assert.That(dict.ContainsKey("added1"));

        dict.Remove(new KeyValuePair<string, int>("added2", 5102));
        Assert.That(dict.ContainsKey("added2"));
        dict.Remove(new KeyValuePair<string, int>("added2", 102));
        Assert.That(dict.ContainsKey("added2"), Is.False);

        Assert.That(dict.IsReadOnly, Is.False);

        dict.Clear();
        Assert.That(dict, Is.Empty);
        Assert.That(dict.Keys, Is.Empty);
        Assert.That(dict.Values, Is.Empty);
        if (orig != null)
            Assert.That(orig, Is.Empty);
    }

    [TestCase(0)]
    [TestCase(1)]
    public void Test_MultiMap(int testCase)
    {
        var dict = testCase switch
        {
            0 => new DefaultDictionary<string, ISet<int>>(defaultValue: new HashSet<int>()),
            1 => new DefaultDictionary<string, ISet<int>>(() => new HashSet<int>()),
            _ => throw new NotSupportedException()
        };

        Assert.That( dict["a"].Add(10) );
        Assert.That( dict["a"].Add(20) );
        Assert.That(dict["a"], Has.Count.EqualTo(2));
        Assert.That( dict["a"].Contains(10) );
        Assert.That( dict["a"].Contains(20) );
        Assert.That( dict["a"].Contains(30), Is.False);

        if (testCase == 0)
        {
            Assert.That(dict["b"], Has.Count.EqualTo(2));
            Assert.That(dict["c"].Contains(20));
        }
        else
        {
            Assert.That(dict["b"], Is.Empty);
            Assert.That(dict["c"].Contains(20), Is.False);
        }

        Assert.That(dict, Has.Count.EqualTo(3));
    }

    [Test]
    public void Test_Nulls()
    {
        var dict1 = new DefaultDictionary<string, string>((string)null!);
        var dict2 = new DefaultDictionary<string, string>( () => (string)null!);

        Assert.That(dict1.ContainsKey("a"), Is.False);
        Assert.That(dict1["a"], Is.Null);
        Assert.That(dict1.ContainsKey("a"));

        Assert.That(dict1.ContainsKey("b"), Is.False);
        Assert.That(dict1.TryGetValue("b", out var b1));
        Assert.That(dict1.ContainsKey("b"));
        Assert.That(b1, Is.Null);
        Assert.That(dict1["b"], Is.Null);

        Assert.That(dict2.ContainsKey("a"), Is.False);
        Assert.That(dict2["a"], Is.Null);
        Assert.That(dict2.ContainsKey("a"));

        Assert.That(dict2.ContainsKey("b"), Is.False);
        Assert.That(dict2.TryGetValue("b", out var b2));
        Assert.That(dict2.ContainsKey("b"));
        Assert.That(b2, Is.Null);
        Assert.That(dict2["b"], Is.Null);
    }

    [Test]
    public void Test_NonStringKey()
    {
        var dict1 = new DefaultDictionary<int, int>(10);
        int max = 0;
        var dict2 = new DefaultDictionary<int, int>(() => max++);

        Assert.That(dict1[93], Is.EqualTo(10));
        Assert.That(dict1[9368], Is.EqualTo(10));
        Assert.That(dict1[9368], Is.EqualTo(10));
        dict1[1] *= 10;
        Assert.That(dict1[1], Is.EqualTo(100));
        Assert.That(dict1, Has.Count.EqualTo(3));

        Assert.That(dict2[93], Is.EqualTo(0));
        Assert.That(dict2[9367], Is.EqualTo(1));

        Assert.That(dict2[9368], Is.EqualTo(2));
        Assert.That(dict2[9368], Is.EqualTo(2));

        dict2[1] *= 10;
        Assert.That(dict2[1], Is.EqualTo(30));
        Assert.That(dict2, Has.Count.EqualTo(4));
    }

    [Test]
    public void Test_JsonObjectStyle()
    {
        var dict = new DefaultDictionary<string, DefaultDictionary<string, Dictionary<string, object>>>
            (() => new DefaultDictionary<string, Dictionary<string, object>>(
                () => []));

        dict["level1"]["level2a"]["level3"] = 55;
        dict["level1"]["level2b"]["level3"] = "thing";
        dict["level1"]["level2c"]["level3"] = new List<decimal> { 0.25M, 1.99M };

        var level1 = dict["level1"];
        Assert.That(level1["level2a"]["level3"], Is.EqualTo(55));
        Assert.That(level1["level2b"]["level3"], Is.EqualTo("thing"));
        Assert.That(level1["level2c"]["level3"] as IEnumerable, Is.EqualTo(new List<decimal> { 0.25M, 1.99M }).AsCollection);
    }
}
