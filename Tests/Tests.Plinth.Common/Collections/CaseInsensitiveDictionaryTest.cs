using NUnit.Framework;

namespace Tests.Plinth.Common.Collections;

[TestFixture]
public class CaseInsensitiveDictionaryTest
{
    [TestCase(1)]
    [TestCase(2)]
    [TestCase(3)]
    public void Test_CaseInsensitiveDictionary(int tcase)
    {
        IDictionary<string, string> dict;
        switch (tcase)
        {
#pragma warning disable IDE0028 // Simplify collection initialization
            case 1: dict = new CaseInsensitiveDictionary<string, string>(); dict["kEy"] = "value"; dict["KEY2"] = "value2"; break;
#pragma warning restore IDE0028 // Simplify collection initialization
            case 2:
                dict = new CaseInsensitiveDictionary<string, string>(55)
                {
                    { "kEy", "value" },
                    { "KEY2", "value2" }
                };
                break;

            case 3: dict = new CaseInsensitiveDictionary<string, string>(new Dictionary<string, string>() { ["kEy"] = "value", ["KEY2"] = "value2" }); break;

            default:
                throw new NotSupportedException();
        }

        Assert.That(dict.ContainsKey("key"));
        Assert.That(dict.ContainsKey("kEy"));
        Assert.That(dict.ContainsKey("KEY"));
        Assert.That(dict["key2"], Is.EqualTo("value2"));

        Assert.That(dict.ContainsKey("keys"), Is.False);
    }

    [Test]
    public void Test_CaseInsensitiveDictionary_Ext()
    {
        var dict = new List<string>() { "kEy", "KEY2" }.ToCaseInsensitiveDictionary(k => k);

        Assert.That(dict.ContainsKey("key"));
        Assert.That(dict.ContainsKey("kEy"));
        Assert.That(dict.ContainsKey("KEY"));
        Assert.That(dict["key2"], Is.EqualTo("KEY2"));

        Assert.That(dict.ContainsKey("keys"), Is.False);

        var dict2 = new List<string>() { "kEy", "KEY2" }.ToCaseInsensitiveDictionary(k => k, k => k.Length);

        Assert.That(dict2.ContainsKey("key"));
        Assert.That(dict2.ContainsKey("kEy"));
        Assert.That(dict2.ContainsKey("KEY"));
        Assert.That(dict2["key2"], Is.EqualTo(4));
        Assert.That(dict2.All(kv => kv.Value == kv.Key.Length));

        Assert.That(dict2.ContainsKey("keys"), Is.False);
    }

    [Test]
    public void Test_CaseInsensitiveDictionary_Throws()
    {
        List<string>? x = null;
        Assert.Throws<ArgumentNullException>(() => x!.ToCaseInsensitiveDictionary(null!));
        Assert.Throws<ArgumentNullException>(() => x!.ToCaseInsensitiveDictionary(s => s));
        x = ["a"];
        Assert.Throws<ArgumentNullException>(() => x!.ToCaseInsensitiveDictionary<string,string>(s => s, null!));
        Assert.Throws<ArgumentNullException>(() => x!.ToCaseInsensitiveDictionary<string,string>(null!, s => s));
        Assert.Throws<ArgumentNullException>(() => x!.ToCaseInsensitiveDictionary<string,string>(null!, null!));
    }

    [Test]
    public void Test_BadKeyType()
    {
        Assert.Throws<NotSupportedException>(() => new CaseInsensitiveDictionary<int, string>());
        Assert.Throws<NotSupportedException>(() => new CaseInsensitiveDictionary<List<string>, string>());
        Assert.Throws<NotSupportedException>(() => new CaseInsensitiveDictionary<int, string>(55));
        Assert.Throws<NotSupportedException>(() => new CaseInsensitiveDictionary<int, string>(new Dictionary<string, string>() { { "a", "b" } }));
    }
}
