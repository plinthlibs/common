using NUnit.Framework;

namespace Tests.Plinth.Common.Collections;

[TestFixture]
public class CaseInsensitiveHashSetTest
{
    [Test]
    public void Test_CaseInsensitiveHashSet()
    {
        var set = new CaseInsensitiveHashSet<string>
        {
            "kEy",
            "KEY2"
        };

        Assert.That(set, Does.Contain("key").IgnoreCase);
        Assert.That(set, Does.Contain("kEy").IgnoreCase);
        Assert.That(set, Does.Contain("KEY").IgnoreCase);
        Assert.That(set, Does.Contain("key2").IgnoreCase);

        Assert.That(set, Does.Not.Contain("keys").IgnoreCase);
    }

    [Test]
    public void Test_CaseInsensitiveHashSet_Ext()
    {
        var set = new List<string> { "kEy", "KEY", "kEy2" }.ToCaseInsensitiveHashSet(s => s);

        Assert.That(set, Does.Contain("key").IgnoreCase);
        Assert.That(set, Does.Contain("kEy").IgnoreCase);
        Assert.That(set, Does.Contain("KEY").IgnoreCase);
        Assert.That(set, Does.Contain("key2").IgnoreCase);

        Assert.That(set, Does.Not.Contain("keys").IgnoreCase);

        set = new List<Tuple<int, string>> { Tuple.Create(1, "kEy"), Tuple.Create(2, "kEy2") }.ToCaseInsensitiveHashSet(s => s.Item2);

        Assert.That(set, Does.Contain("key").IgnoreCase);
        Assert.That(set, Does.Contain("kEy").IgnoreCase);
        Assert.That(set, Does.Contain("KEY").IgnoreCase);
        Assert.That(set, Does.Contain("key2").IgnoreCase);

        Assert.That(set, Does.Not.Contain("keys").IgnoreCase);
    }

    [Test]
    public void Test_CaseInsensitiveHashSet_Throws()
    {
        List<string>? x = null;
        Assert.Throws<ArgumentNullException>(() => x!.ToCaseInsensitiveHashSet());
        Assert.Throws<ArgumentNullException>(() => x!.ToCaseInsensitiveHashSet(null!));
        Assert.Throws<ArgumentNullException>(() => x!.ToCaseInsensitiveHashSet(s => s));
        x = ["a"];
        Assert.Throws<ArgumentNullException>(() => x!.ToCaseInsensitiveHashSet(null!));
    }

    [Test]
    public void Test_CaseInsensitiveHashSet_IEnumerable()
    {
        var set = new CaseInsensitiveHashSet<string>(["kEy", "KEY2"]);

        Assert.That(set, Does.Contain("key").IgnoreCase);
        Assert.That(set, Does.Contain("kEy").IgnoreCase);
        Assert.That(set, Does.Contain("KEY").IgnoreCase);

        Assert.That(set, Does.Not.Contain("keys").IgnoreCase);
    }

    [Test]
    public void Test_BadKeyType()
    {
        Assert.Throws<NotSupportedException>(() => new CaseInsensitiveHashSet<int>());
        Assert.Throws<NotSupportedException>(() => new CaseInsensitiveHashSet<List<string>>());
        Assert.Throws<NotSupportedException>(() => new CaseInsensitiveHashSet<int>(["a", "b"]));
    }
}
