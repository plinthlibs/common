using Plinth.Common.Utils;
using NUnit.Framework;

namespace Tests.Plinth.Common.Utils;

[TestFixture]
public class HexUtilTest
{
#if NET9_0_OR_GREATER
#pragma warning disable CS0618
#endif
    [Test]
    public void Test_ToHex()
    {
        Assert.That(HexUtil.ToHex([0x12, 0xab, 0x1c, 0x4f]), Is.EqualTo("12ab1c4f"));

        Assert.That(HexUtil.ToHex([0x12, 0xab, 0x1c]), Is.EqualTo("12ab1c"));
    }

    [Test]
    public void Test_ToHex_Offsets()
    {
        var b = new byte[] { 0x12, 0xab, 0x1c, 0x4f };
        Assert.That(HexUtil.ToHex(b, 0, 4), Is.EqualTo("12ab1c4f"));
        Assert.That(HexUtil.ToHex(b, 0, 3), Is.EqualTo("12ab1c"));
        Assert.That(HexUtil.ToHex(b, 0, 1), Is.EqualTo("12"));
        Assert.That(HexUtil.ToHex(b, 0, 0), Is.EqualTo(""));

        Assert.That(HexUtil.ToHex(b, 1, 3), Is.EqualTo("ab1c4f"));
        Assert.That(HexUtil.ToHex(b, 2, 2), Is.EqualTo("1c4f"));
        Assert.That(HexUtil.ToHex(b, 3, 1), Is.EqualTo("4f"));
        Assert.That(HexUtil.ToHex(b, 4, 0), Is.EqualTo(""));

        Assert.Throws<ArgumentOutOfRangeException>(() => HexUtil.ToHex(b, 0, 5));
        Assert.Throws<ArgumentOutOfRangeException>(() => HexUtil.ToHex(b, 5, 0));
        Assert.Throws<ArgumentOutOfRangeException>(() => HexUtil.ToHex(b, 2, 3));
        Assert.Throws<ArgumentOutOfRangeException>(() => HexUtil.ToHex(b, 3, 2));
        Assert.Throws<ArgumentOutOfRangeException>(() => HexUtil.ToHex(b, 3, -1));
        Assert.Throws<ArgumentOutOfRangeException>(() => HexUtil.ToHex(b, -1, 2));
    }

    [Test]
    public void Test_ToHex_ZeroBytes()
    {
        Assert.That(HexUtil.ToHex([]), Is.EqualTo(""));
    }

    [Test]
    public void Test_ToHex_Null()
    {
        Assert.Throws<ArgumentNullException>(() => HexUtil.ToHex(null!));
        Assert.Throws<ArgumentNullException>(() => HexUtil.ToHex(null!, 0, 4));
    }

    [Test]
    public void Test_HexToBytes()
    {
        Assert.That(HexUtil.ToBytes("12ab1c4f"), Is.EqualTo(new byte[] { 0x12, 0xab, 0x1c, 0x4f }).AsCollection);
        Assert.That(HexUtil.ToBytes("12AB1C4f"), Is.EqualTo(new byte[] { 0x12, 0xab, 0x1c, 0x4f }).AsCollection);

        Assert.That(HexUtil.ToBytes("12AB1C"), Is.EqualTo(new byte[] { 0x12, 0xab, 0x1c }).AsCollection);
    }

    [Test]
    public void Test_HexToBytes_OddLength()
    {
        Assert.Throws<FormatException>(() => HexUtil.ToBytes("12abc"));
    }

    [Test]
    public void Test_HexToBytes_BadChar()
    {
        Assert.Throws<FormatException>(() => HexUtil.ToBytes("12abcP"));
    }

    [Test]
    public void Test_HexToBytes_Null()
    {
        Assert.Throws<ArgumentNullException>(() => HexUtil.ToBytes(null!));
    }
#if NET9_0_OR_GREATER
#pragma warning restore CS0618
#endif
}
