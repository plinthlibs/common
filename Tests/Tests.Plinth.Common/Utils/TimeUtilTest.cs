using Microsoft.Extensions.Logging;
using Plinth.Common.Logging;
using Plinth.Common.Utils;
using NUnit.Framework;

namespace Tests.Plinth.Common.Utils;

[TestFixture]
public class TimeUtilTest
{
    private class MockLogger : ILogger
    {
        public List<LogEvent> Logs { get; set; } = [];
        public AsyncLocal<Dictionary<string, object>> Context { get; set; } = new AsyncLocal<Dictionary<string, object>>();

        public class LogEvent
        {
            public LogLevel logLevel; public EventId eventId; public object? state; public Exception? exception; public string? message;
            public Dictionary<string, object>? Context { get; set; }
        }

        private class MockIDisposable(IEnumerable<KeyValuePair<string, object>>? items, MockLogger? log) : IDisposable
        {
            public void Dispose()
            {
                if (items != null)
                    foreach (var kv in items)
                        log?.Context.Value?.Remove(kv.Key);
            }
        }

        public IDisposable? BeginScope<TState>(TState state) where TState : notnull
        {
            if (state is IEnumerable<KeyValuePair<string, object>> i)
            {
                Context.Value ??= [];
                foreach (var kv in i)
                    Context.Value.Add(kv.Key, kv.Value);
                return new MockIDisposable(i, this);
            }

            return new MockIDisposable(null, this);
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception? exception, Func<TState, Exception?, string> formatter)
        {
            Logs.Add(new LogEvent { logLevel = logLevel, eventId = eventId, state = state, exception = exception, message = formatter(state, exception),
            Context = this.Context.Value?.ToDictionary(kv => kv.Key, kv => kv.Value) });
        }
    }

    [Test]
    public void Test_LogExec()
    {
        var logger = new MockLogger();
        TimeUtil.LogExecTiming(() => Thread.Sleep(5), "Running", logger);
        Assert.That(logger.Logs, Has.Count.EqualTo(2));
        logger.Logs.Clear();

        logger = new MockLogger();
        var t = new TimeUtil.TimeLogger("test", logger);
        Thread.Sleep(5);
        t.Dispose();
        Assert.That(logger.Logs, Has.Count.EqualTo(2));
        Assert.That(t.Duration.TotalMilliseconds, Is.GreaterThan(0));

        // uses static log manager, can't confirm
        t = new TimeUtil.TimeLogger("test");
        t.Dispose();
    }

    [Test]
    public void Test_LogExecProps()
    {
        var logger = new MockLogger();

        using (logger.BeginScope(new KeyValuePair<string, object>[] { new KeyValuePair<string, object>("Test", 10) }))
        {
            var t = new TimeUtil.TimeLogger("test", logger);

            Assert.That(logger.Logs, Has.Count.EqualTo(1));
            Assert.That(logger.Logs[0].Context, Is.Not.Null);
            Assert.That(logger.Logs[0].Context?["Test"], Is.EqualTo(10));

            t.Dispose();
            Assert.That(logger.Logs, Has.Count.EqualTo(2));
            Assert.That(logger.Logs[1].Context, Is.Not.Null);
            Assert.That(logger.Logs[1].Context?["Test"], Is.EqualTo(10));
        }
    }   
    
    [Test]
    public void Test_LogExec_Ext()
    {
        var logger = new MockLogger();
        var t = logger.LogExecTiming("test");
        Thread.Sleep(5);
        t.Dispose();
        Assert.That(logger.Logs, Has.Count.EqualTo(2));
        logger.Logs.Clear();

        logger = new MockLogger();
        t = logger.LogExecTiming(LogLevel.Information, "test");
        Thread.Sleep(5);
        t.Dispose();
        Assert.That(logger.Logs, Has.Count.EqualTo(2));
        Assert.That(t.Duration.TotalMilliseconds, Is.GreaterThan(0));
    }

    [Test]
    public void Test_LogExecProps_Level_Ext()
    {
        var logger = new MockLogger();

        using (logger.BeginScope(new KeyValuePair<string, object>[] { new KeyValuePair<string, object>("Test", 10) }))
        {
            var t = logger.LogExecTiming(LogLevel.Warning, "test");

            Assert.That(logger.Logs, Has.Count.EqualTo(1));
            Assert.That(logger.Logs[0].Context, Is.Not.Null);
            Assert.That(logger.Logs[0].Context?["Test"], Is.EqualTo(10));

            t.Dispose();
            Assert.That(logger.Logs, Has.Count.EqualTo(2));
            Assert.That(logger.Logs[1].Context, Is.Not.Null);
            Assert.That(logger.Logs[1].Context?["Test"], Is.EqualTo(10));
        }
    }
}
