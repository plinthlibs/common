using NUnit.Framework;
using Plinth.Common.Utils;

namespace Tests.Plinth.Common.Utils;

[TestFixture]
public class DateTimeUtilTest
{
    [Test]
    public void Test_YearDiff()
    {
        Assert.That((double)DateTimeUtil.YearDiff(new DateTime(2014, 05, 05), new DateTime(2013, 05, 05)), Is.EqualTo(1.0d).Within(0.01d));

        Assert.That((double)DateTimeUtil.YearDiff(new DateTime(2014, 08, 20), new DateTime(2011, 02, 22)), Is.EqualTo(3.5d).Within(0.01d));
    }

    [Test]
    public void Test_MostRecent()
    {
        var dt1 = new DateTime(2014, 11, 20);
        var dt2 = new DateTime(2014, 11, 21);

        Assert.That(DateTimeUtil.MostRecent(dt1, dt2), Is.EqualTo(dt2));
        Assert.That(DateTimeUtil.MostRecent(dt2, dt1), Is.EqualTo(dt2));
        Assert.That(DateTimeUtil.MostRecent(dt1, dt1), Is.EqualTo(dt1));
    }

    [Test]
    public void Test_BeginningOfDay()
    {
        var dt1 = DateTime.UtcNow;
        var beg = DateTimeUtil.BeginningOfDay(dt1);

        Assert.That(beg.Day, Is.EqualTo(dt1.Day));
        Assert.That(beg.Month, Is.EqualTo(dt1.Month));
        Assert.That(beg.Year, Is.EqualTo(dt1.Year));
        Assert.That(beg.Hour, Is.EqualTo(0));
        Assert.That(beg.Minute, Is.EqualTo(0));
        Assert.That(beg.Second, Is.EqualTo(0));
        Assert.That(beg.Millisecond, Is.EqualTo(0));
    }

    [Test]
    public void Test_EndOfDay()
    {
        var dt1 = DateTime.UtcNow;
        var end = DateTimeUtil.EndOfDay(dt1);

        Assert.That(end.Day, Is.EqualTo(dt1.Day));
        Assert.That(end.Month, Is.EqualTo(dt1.Month));
        Assert.That(end.Year, Is.EqualTo(dt1.Year));
        Assert.That(end.Hour, Is.EqualTo(23));
        Assert.That(end.Minute, Is.EqualTo(59));
        Assert.That(end.Second, Is.EqualTo(59));
        Assert.That(end.Millisecond, Is.EqualTo(999));
    }

    [Test]
    public void Test_BeginningOfDay_AtBeginningOfDay()
    {
        var dt1 = DateTime.UtcNow;
        var dt2 = new DateTime(dt1.Year, dt1.Month, dt1.Day, 0, 0, 0, 0, DateTimeKind.Utc);
        var beg = DateTimeUtil.BeginningOfDay(dt2);

        Assert.That(beg.Day, Is.EqualTo(dt2.Day));
        Assert.That(beg.Month, Is.EqualTo(dt2.Month));
        Assert.That(beg.Year, Is.EqualTo(dt2.Year));
        Assert.That(beg.Hour, Is.EqualTo(0));
        Assert.That(beg.Minute, Is.EqualTo(0));
        Assert.That(beg.Second, Is.EqualTo(0));
        Assert.That(beg.Millisecond, Is.EqualTo(0));
    }

    [Test]
    public void Test_EndOfDay_AtEndOfDay()
    {
        var dt1 = DateTime.UtcNow;
        var dt2 = new DateTime(dt1.Year, dt1.Month, dt1.Day, 23, 59, 59, 999, DateTimeKind.Utc);
        var end = DateTimeUtil.EndOfDay(dt2);

        Assert.That(end.Day, Is.EqualTo(dt2.Day));
        Assert.That(end.Month, Is.EqualTo(dt2.Month));
        Assert.That(end.Year, Is.EqualTo(dt2.Year));
        Assert.That(end.Hour, Is.EqualTo(23));
        Assert.That(end.Minute, Is.EqualTo(59));
        Assert.That(end.Second, Is.EqualTo(59));
        Assert.That(end.Millisecond, Is.EqualTo(999));
    }

    [Test]
    public void Test_DaysOfYear()
    {
        Assert.That(DateTimeUtil.DaysInYear(2015), Is.EqualTo(365));
        Assert.That(DateTimeUtil.DaysInYear(2016), Is.EqualTo(366));
        Assert.That(DateTimeUtil.DaysInYear(2000), Is.EqualTo(366));
        Assert.That(DateTimeUtil.DaysInYear(1900), Is.EqualTo(365));
    }

    [Test]
    [TestCase("Pacific Standard Time")]
    [TestCase("America/Los_Angeles")]
    public void Test_ConvertToUtcFromPacific_Net6(string tz)
    {
        var t = new DateTime(2015, 6, 8, 10, 34, 12, 0, DateTimeKind.Unspecified);
        var c = DateTimeUtil.ToUtc(t, tz);

        Assert.That(c, Is.EqualTo(new DateTime(2015, 6, 8, 17, 34, 12, 0, DateTimeKind.Utc)));

        // DST
        t = new DateTime(2015, 3, 8, 3, 0, 0, 0, DateTimeKind.Unspecified);
        c = DateTimeUtil.ToUtc(t, tz);

        Assert.That(c, Is.EqualTo(new DateTime(2015, 3, 8, 10, 0, 0, 0, DateTimeKind.Utc)));

        DateTime? n = null;
        Assert.That(DateTimeUtil.ToUtc(n, tz), Is.Null);
        n = new DateTime(2015, 6, 8, 10, 34, 12, 0, DateTimeKind.Unspecified);
        c = DateTimeUtil.ToUtc(n, tz);
        Assert.That(c, Is.EqualTo(new DateTime(2015, 6, 8, 17, 34, 12, 0, DateTimeKind.Utc)));
    }

    [Test]
    [TestCase("Central Standard Time")]
    [TestCase("America/Chicago")]
    public void Test_ConvertToUtcFromCentral_Net6(string tz)
    {
        var t = new DateTime(2015, 6, 8, 10, 34, 12, 0, DateTimeKind.Unspecified);
        var c = DateTimeUtil.ToUtc(t, tz);

        Assert.That(c, Is.EqualTo(new DateTime(2015, 6, 8, 15, 34, 12, 0, DateTimeKind.Utc)));

        // DST
        t = new DateTime(2015, 3, 8, 3, 0, 0, 0, DateTimeKind.Unspecified);
        c = DateTimeUtil.ToUtc(t, tz);

        Assert.That(c, Is.EqualTo(new DateTime(2015, 3, 8, 8, 0, 0, 0, DateTimeKind.Utc)));

        DateTime? n = null;
        Assert.That(DateTimeUtil.ToUtc(n, tz), Is.Null);
        n = new DateTime(2015, 6, 8, 10, 34, 12, 0, DateTimeKind.Unspecified);
        c = DateTimeUtil.ToUtc(n, tz);
        Assert.That(c, Is.EqualTo(new DateTime(2015, 6, 8, 15, 34, 12, 0, DateTimeKind.Utc)));
    }

    [Test]
    [TestCase("Eastern Standard Time")]
    [TestCase("America/New_York")]
    public void Test_ConvertToUtcFromEastern_Net6(string tz)
    {
        var t = new DateTime(2015, 6, 8, 10, 34, 12, 0, DateTimeKind.Unspecified);
        var c = DateTimeUtil.ToUtc(t, tz);

        Assert.That(c, Is.EqualTo(new DateTime(2015, 6, 8, 14, 34, 12, 0, DateTimeKind.Utc)));

        // DST
        t = new DateTime(2015, 3, 8, 3, 0, 0, 0, DateTimeKind.Unspecified);
        c = DateTimeUtil.ToUtc(t, tz);

        Assert.That(c, Is.EqualTo(new DateTime(2015, 3, 8, 7, 0, 0, 0, DateTimeKind.Utc)));

        DateTime? n = null;
        Assert.That(DateTimeUtil.ToUtc(n, tz), Is.Null);
        n = new DateTime(2015, 6, 8, 10, 34, 12, 0, DateTimeKind.Unspecified);
        c = DateTimeUtil.ToUtc(n, tz);
        Assert.That(c, Is.EqualTo(new DateTime(2015, 6, 8, 14, 34, 12, 0, DateTimeKind.Utc)));
    }

    [Test]
    [TestCase("Pacific Standard Time")]
    [TestCase("America/Los_Angeles")]
    public void Test_ConvertPacific_Net6(string tz)
    {
        var t = new DateTime(2015, 6, 8, 17, 34, 12, 0, DateTimeKind.Utc);
        var c = DateTimeUtil.FromUtc(t, tz);

        Assert.That(c, Is.EqualTo(new DateTime(2015, 6, 8, 10, 34, 12, 0, DateTimeKind.Unspecified)));

        // DST
        t = new DateTime(2015, 3, 8, 10, 0, 0, 0, DateTimeKind.Utc);
        c = DateTimeUtil.FromUtc(t, tz);

        Assert.That(c, Is.EqualTo(new DateTime(2015, 3, 8, 3, 0, 0, 0, DateTimeKind.Unspecified)));

        DateTime? n = null;
        Assert.That(DateTimeUtil.FromUtc(n, tz), Is.Null);
        n = new DateTime(2015, 6, 8, 17, 34, 12, 0, DateTimeKind.Utc);
        Assert.That(DateTimeUtil.FromUtc(n, tz), Is.EqualTo(new DateTime(2015, 6, 8, 10, 34, 12, 0, DateTimeKind.Unspecified)));
    }

    [Test]
    [TestCase("Central Standard Time")]
    [TestCase("America/Chicago")]
    public void Test_ConvertCentral_Net6(string tz)
    {
        var t = new DateTime(2015, 6, 8, 15, 34, 12, 0, DateTimeKind.Utc);
        var c = DateTimeUtil.FromUtc(t, tz);

        Assert.That(c, Is.EqualTo(new DateTime(2015, 6, 8, 10, 34, 12, 0, DateTimeKind.Unspecified)));

        // DST
        t = new DateTime(2015, 3, 8, 8, 0, 0, 0, DateTimeKind.Utc);
        c = DateTimeUtil.FromUtc(t, tz);

        Assert.That(c, Is.EqualTo(new DateTime(2015, 3, 8, 3, 0, 0, 0, DateTimeKind.Unspecified)));

        DateTime? n = null;
        Assert.That(DateTimeUtil.FromUtc(n, tz), Is.Null);
        n = new DateTime(2015, 6, 8, 15, 34, 12, 0, DateTimeKind.Utc);
        Assert.That(DateTimeUtil.FromUtc(n, tz), Is.EqualTo(new DateTime(2015, 6, 8, 10, 34, 12, 0, DateTimeKind.Unspecified)));
    }

    [Test]
    [TestCase("Eastern Standard Time")]
    [TestCase("America/New_York")]
    public void Test_ConvertToEasternFromUtc_Net6(string tz)
    {
        var t = new DateTime(2015, 6, 8, 14, 34, 12, 0, DateTimeKind.Utc);
        var c = DateTimeUtil.FromUtc(t, tz);

        Assert.That(c, Is.EqualTo(new DateTime(2015, 6, 8, 10, 34, 12, 0, DateTimeKind.Unspecified)));

        // DST
        t = new DateTime(2015, 3, 8, 7, 0, 0, 0, DateTimeKind.Utc);
        c = DateTimeUtil.FromUtc(t, tz);

        Assert.That(c, Is.EqualTo(new DateTime(2015, 3, 8, 3, 0, 0, 0, DateTimeKind.Unspecified)));

        DateTime? n = null;
        Assert.That(DateTimeUtil.FromUtc(n, tz), Is.Null);
        n = new DateTime(2015, 6, 8, 14, 34, 12, 0, DateTimeKind.Utc);
        Assert.That(DateTimeUtil.FromUtc(n, tz), Is.EqualTo(new DateTime(2015, 6, 8, 10, 34, 12, 0, DateTimeKind.Unspecified)));
    }

    [Test]
    [TestCase(463)]
    [TestCase(999)]
    [TestCase(0)]
    public void Test_TruncateToSecond(int millis)
    {
        var dt = new DateTime(2015, 6, 8, 10, 34, 12, millis, DateTimeKind.Unspecified);
        var truncated = DateTimeUtil.TruncateToSecond(dt);

        Assert.That(truncated, Is.EqualTo(new DateTime(2015, 6, 8, 10, 34, 12, 0, DateTimeKind.Unspecified)));
    }

    [Test]
    [TestCase(46, 463)]
    [TestCase(9, 999)]
    [TestCase(0, 0)]
    public void Test_TruncateToMinute(int seconds, int millis)
    {
        var dt = new DateTime(2015, 6, 8, 10, 34, seconds, millis, DateTimeKind.Local);
        var truncated = DateTimeUtil.TruncateToMinute(dt);

        Assert.That(truncated, Is.EqualTo(new DateTime(2015, 6, 8, 10, 34, 0, 0, DateTimeKind.Local)));
    }

    [Test]
    [TestCase(4, 46, 463)]
    [TestCase(9, 9, 999)]
    [TestCase(0, 0, 0)]
    public void Test_TruncateToHour(int minutes, int seconds, int millis)
    {
        var dt = new DateTime(2015, 6, 8, 10, minutes, seconds, millis, DateTimeKind.Utc);
        var truncated = DateTimeUtil.TruncateToHour(dt);

        Assert.That(truncated, Is.EqualTo(new DateTime(2015, 6, 8, 10, 0, 0, 0, DateTimeKind.Utc)));
    }
}
