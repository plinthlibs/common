using System.Collections.Concurrent;
using System.Diagnostics;
using Plinth.Common.Utils;
using NUnit.Framework;

namespace Tests.Plinth.Common.Utils;

[TestFixture]
class AsyncUtilTest
{
    [Test]
    public void TestRunSync()
    {
        int count = 0;
        List<Task> tasks = [];
        for (int i = 0; i < 10000; i++)
        {
            var t = Task.Run(() => AsyncUtil.RunSync(async () => await work()));
            tasks.Add(t);
        }

        Task.WaitAll(tasks.ToArray());
        Assert.That(count, Is.EqualTo(10000));

        async Task work()
        {
            await Task.Yield();
            Interlocked.Increment(ref count);
        }
    }

    [Test]
    public void TestRunSync_T()
    {
        int count = 0;
        List<Task> tasks = [];
        for (int i = 0; i < 10000; i++)
        {
            var t = Task.Run(() => AsyncUtil.RunSync(async () => await work()));
            tasks.Add(t);
        }

        Task.WaitAll(tasks.ToArray());
        Assert.That(count, Is.EqualTo(10000));

        async Task<int> work()
        {
            await Task.Yield();
            return Interlocked.Increment(ref count);
        }
    }
}
