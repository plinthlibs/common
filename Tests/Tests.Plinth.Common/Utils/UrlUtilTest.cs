﻿using Plinth.Common.Utils;
using NUnit.Framework;

namespace Tests.Plinth.Common.Utils;

[TestFixture]
class UrlUtilTest
{
    [TestCase("http://domain.com", "path", "http://domain.com/path")]
    [TestCase("http://domain.com/", "path", "http://domain.com/path")]
    [TestCase("http://domain.com/", "/path", "http://domain.com/path")]
    [TestCase("http://domain.com/", "/path/to/something?a=5", "http://domain.com/path/to/something?a=5")]
    [TestCase("http///", "////a", "http/a")]
    public void TestSimpleJoin(string a, string b, string result)
    {
        Assert.That(UrlUtil.Join(a, b), Is.EqualTo(result));
    }

    [Test]
    public void TestMultiJoin()
    {
        Assert.That(UrlUtil.Join("http://domain.com", "path", "to", "something?a=5"), Is.EqualTo("http://domain.com/path/to/something?a=5"));
        Assert.That(UrlUtil.Join("http://domain.com/", "path/", "to/", "something?a=5"), Is.EqualTo("http://domain.com/path/to/something?a=5"));
        Assert.That(UrlUtil.Join("http://domain.com/", "/path/", "/to/", "/something?a=5"), Is.EqualTo("http://domain.com/path/to/something?a=5"));
        Assert.That(UrlUtil.Join("https://domain.com/", "/path/", "/to/", "/something?a=5"), Is.EqualTo("https://domain.com/path/to/something?a=5"));
        Assert.That(UrlUtil.Join("../path", "/to/", "/something?a=5"), Is.EqualTo("../path/to/something?a=5"));
        Assert.That(UrlUtil.Join("http://///", "///a///", "////", "////something?a=5"), Is.EqualTo("http:/a/something?a=5"));
    }
}
