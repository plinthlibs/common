using Plinth.Common.Cache;
using NUnit.Framework;
using System.Collections.Concurrent;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;

namespace Tests.Plinth.Common.Cache;

[TestFixture]
public class SyncCacheExtTest
{
    private class Obj
    {
        public string? Value { get; set; }
    }

    private static MemoryCache MakeCache() => new MemoryCache(Options.Create(new MemoryCacheOptions()));

    private Func<IMemoryCache, object, TimeSpan, Func<T>, T> MakeGetOrCreate<T>(string mode)
    {
        return mode switch
        {
            "MaxDuration" => MemoryCacheExtensions.GetOrCreateAtomicMaxDuration,
            "RecentlyUsed" => MemoryCacheExtensions.GetOrCreateAtomicRecentlyUsed,
            "Manual-MaxDuration" => (c, o, ts, f) => MemoryCacheExtensions.GetOrCreateAtomic(c, o, new MemoryCacheEntryOptions { AbsoluteExpirationRelativeToNow = ts }, f),
            "Manual-RecentlyUsed" => (c, o, ts, f) => MemoryCacheExtensions.GetOrCreateAtomic(c, o, new MemoryCacheEntryOptions { SlidingExpiration = ts }, f),
            _ => throw new NotSupportedException(mode)
        };
    }

    [Test]
    [TestCase("MaxDuration")]
    [TestCase("RecentlyUsed")]
    [TestCase("Manual-MaxDuration")]
    [TestCase("Manual-RecentlyUsed")]
    public void GetOrCreate(string mode)
    {
        using var c = MakeCache();
        var getOrCreate = MakeGetOrCreate<Obj>(mode);

        var o1 = new Obj { Value = "value1" };
        var r1 = getOrCreate(c, "key1", TimeSpan.FromMilliseconds(50), () => o1);
        Assert.That(o1, Is.SameAs(r1));
        Assert.That(r1.Value, Is.EqualTo("value1"));

        var o2 = new Obj { Value = "value2" };
        var r2 = getOrCreate(c, "key1", TimeSpan.FromMilliseconds(50), () => o2);
        Assert.That(o1, Is.SameAs(r2));
        Assert.That(o2, Is.Not.SameAs(r2));
        Assert.That(r2.Value, Is.EqualTo("value1"));
    }

    [Test]
    [TestCase("MaxDuration")]
    [TestCase("RecentlyUsed")]
    [TestCase("Manual-MaxDuration")]
    [TestCase("Manual-RecentlyUsed")]
    // proper way to get infinite expiration is to pass an empty MemoryCacheOptions
    public void GetOrCreateInfinite_Fails(string mode)
    {
        using var c = MemoryCacheUtil.Create();
        var getOrCreate = MakeGetOrCreate<Obj>(mode);

        var o1 = new Obj { Value = "value1" };

        if (mode.Contains("MaxDuration"))
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => getOrCreate(c, "key1", TimeSpan.MaxValue, () => o1));
        }
        else
        {
            var r1 = getOrCreate(c, "key1", TimeSpan.MaxValue, () => o1);
            Assert.That(o1, Is.SameAs(r1));
            Assert.That(r1.Value, Is.EqualTo("value1"));

            var o2 = new Obj { Value = "value2" };
            var r2 = getOrCreate(c, "key1", TimeSpan.MaxValue, () => o2);
            Assert.That(o1, Is.SameAs(r2));
            Assert.That(o2, Is.Not.SameAs(r2));
            Assert.That(r2.Value, Is.EqualTo("value1"));
        }
    }

    [Test]
    [TestCase("MaxDuration")]
    [TestCase("RecentlyUsed")]
    [TestCase("Manual-MaxDuration")]
    [TestCase("Manual-RecentlyUsed")]
    public void Expiration(string mode)
    {
        using var c = MakeCache();
        var getOrCreate = MakeGetOrCreate<Obj>(mode);

        var o1 = new Obj { Value = "value1" };
        var r1 = getOrCreate(c, "key1", TimeSpan.FromMilliseconds(50), () => o1);
        Assert.That(o1, Is.SameAs(r1));
        Assert.That(r1.Value, Is.EqualTo("value1"));

        Thread.Sleep(75);

        var o2 = new Obj { Value = "value2" };
        var r2 = getOrCreate(c, "key1", TimeSpan.FromMilliseconds(50), () => o2);
        Assert.That(o2, Is.SameAs(r2));
        Assert.That(o2, Is.Not.SameAs(r1));
        Assert.That(r2.Value, Is.EqualTo("value2"));
    }

    [Test]
    [TestCase("MaxDuration")]
    [TestCase("RecentlyUsed")]
    [TestCase("Manual-MaxDuration")]
    [TestCase("Manual-RecentlyUsed")]
    public void ValueFactoryFails(string mode)
    {
        using var c = MakeCache();
        var getOrCreate = MakeGetOrCreate<string>(mode);

        Assert.Throws<Exception>(() => getOrCreate(c, "key1", TimeSpan.FromHours(1), () => func()));

        Assert.That(c.Get("key1"), Is.Null);

        static string func()
        {
            throw new Exception();
        }
    }

    [Test]
    [TestCase("MaxDuration")]
    [TestCase("RecentlyUsed")]
    [TestCase("Manual-MaxDuration")]
    [TestCase("Manual-RecentlyUsed")]
    public void MultiThread_SingleInit(string mode)
    {
        // confirms that only a single thread gets to initialize the cached entry

        using var c = MakeCache();
        var getOrCreate = MakeGetOrCreate<string>(mode);
        var cd = new ConcurrentDictionary<int, bool>();
        var cde = new CountdownEvent(1);
        var cde_start = new CountdownEvent(64);
        var cde2 = new CountdownEvent(64);

        for (int i = 0; i < 64; i++)
        {
            var t = new Thread(() =>
            {
                int tid = Environment.CurrentManagedThreadId;
                cde_start.Signal();
                cde.Wait();
                //Console.WriteLine("{0}: get or add: {1}", tid, DateTime.UtcNow.ToString("O"));
                try
                {
                    getOrCreate(c, "k1", TimeSpan.FromHours(1), () => gen_cache(tid));
                }
                catch (Exception /*e*/)
                {
                    //Console.WriteLine("{0}: exception: {1} {2}", tid, e.Message, DateTime.UtcNow.ToString("O"));
                }
                //Console.WriteLine("{0}: got: {1}", tid, DateTime.UtcNow.ToString("O"));
                cde2.Signal();
            });
            t.Start();
        }

        cde_start.Wait();
        cde.Signal();
        cde2.Wait();

        //Console.WriteLine("{0}", string.Join(",", cd.Keys));
        Assert.That(cd, Has.Count.EqualTo(1));

        string gen_cache(int tid)
        {
            //Console.WriteLine("{0}: got IN: {1}", Environment.CurrentManagedThreadId, DateTime.UtcNow.ToString("O"));
            cd.TryAdd(tid, true);
            //Console.WriteLine("{0}: ret cache: {1}", Environment.CurrentManagedThreadId, DateTime.UtcNow.ToString("O"));
            return "abc";
        }
    }

    [Test]
    [TestCase("MaxDuration")]
    [TestCase("RecentlyUsed")]
    [TestCase("Manual-MaxDuration")]
    [TestCase("Manual-RecentlyUsed")]
    public void MultiThread_Exception(string mode)
    {
        // confirms that everybody gets to initialize the cached entry if it fails

        using var c = MakeCache();
        var getOrCreate = MakeGetOrCreate<string>(mode);
        var cd = new ConcurrentDictionary<int, bool>();
        var cde = new CountdownEvent(1);
        var cde2 = new CountdownEvent(32);
        int concurrentTest = 0;

        //Console.WriteLine($"entering: {mode}");

        for (int i = 0; i < 32; i++)
        {
            var t = new Thread(() =>
            {
                var tid = Environment.CurrentManagedThreadId;
                cde.Wait();
                //Console.WriteLine("{0}: get or add: {1}", tid, DateTime.UtcNow.ToString("O"));
                try
                {
                    getOrCreate(c, "k1", TimeSpan.FromHours(1), () => gen_cache(tid));
                }
                catch (Exception /*e*/)
                {
                    //Console.WriteLine("{0}: exception: {1} {2}", tid, e.Message, DateTime.UtcNow.ToString("O"));
                }
                //Console.WriteLine("{0}: got: {1}", tid, DateTime.UtcNow.ToString("O"));
                cde2.Signal();
            });
            t.Start();
        }

        cde.Signal();
        cde2.Wait();

        //Console.WriteLine("{0}", string.Join(",", cd.Keys));
        Assert.That(cd, Has.Count.EqualTo(32));

        string gen_cache(int tid)
        {
            if (Interlocked.Increment(ref concurrentTest) != 1)
            {
                throw new Exception($"concurrent access detected(a) : {concurrentTest}");
            }
            //Console.WriteLine("{0}: got IN: {1}", tid, DateTime.UtcNow.ToString("O"));
            cd.TryAdd(tid, true);
            //Console.WriteLine("{0}: throw: {1}", tid, DateTime.UtcNow.ToString("O"));
            if (Interlocked.Decrement(ref concurrentTest) != 0)
                throw new Exception($"concurrent access detected(b) : {concurrentTest}");
            throw new Exception("bad");
        }
    }
}
