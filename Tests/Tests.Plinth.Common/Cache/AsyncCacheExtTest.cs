using Plinth.Common.Cache;
using NUnit.Framework;
using System.Collections.Concurrent;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Win32;

namespace Tests.Plinth.Common.Cache;

[TestFixture]
public class AsyncCacheExtTest
{
    private class Obj
    {
        public string? Value { get; set; }
    }

    private static async Task<T> VA<T>(T v)
    {
        await Task.Yield();
        return v;
    }

    private Func<IMemoryCache, object, TimeSpan, Func<Task<T>>, Task<T>> MakeGetOrCreate<T>(string mode)
    {
        return mode switch
        {
            "MaxDuration" => MemoryCacheExtensions.GetOrCreateAtomicMaxDurationAsync,
            "RecentlyUsed" => MemoryCacheExtensions.GetOrCreateAtomicRecentlyUsedAsync,
            "Manual-MaxDuration" => (c, o, ts, f) => MemoryCacheExtensions.GetOrCreateAtomicAsync(c, o, new MemoryCacheEntryOptions { AbsoluteExpirationRelativeToNow = ts }, f),
            "Manual-RecentlyUsed" => (c, o, ts, f) => MemoryCacheExtensions.GetOrCreateAtomicAsync(c, o, new MemoryCacheEntryOptions { SlidingExpiration = ts }, f),
            _ => throw new NotSupportedException(mode)
        };
    }

    [Test]
    [TestCase("MaxDuration")]
    [TestCase("RecentlyUsed")]
    [TestCase("Manual-MaxDuration")]
    [TestCase("Manual-RecentlyUsed")]
    public async Task GetOrCreate(string mode)
    {
        using var c = MemoryCacheUtil.Create(o => o.TrackStatistics = true);
        var getOrCreate = MakeGetOrCreate<Obj>(mode);

        var o1 = new Obj { Value = "value1" };
        var r1 = await getOrCreate(c, "key1", TimeSpan.FromMilliseconds(50), () => VA(o1));
        Assert.That(o1, Is.SameAs(r1));
        Assert.That(r1.Value, Is.EqualTo("value1"));

        var o2 = new Obj { Value = "value2" };
        var r2 = await getOrCreate(c, "key1", TimeSpan.FromMilliseconds(50), () => VA(o2));
        Assert.That(o1, Is.SameAs(r2));
        Assert.That(o2, Is.Not.SameAs(r2));
        Assert.That(r2.Value, Is.EqualTo("value1"));

#if NET8_0_OR_GREATER
        // TODO: this should be available in net6/7 but the 9.0.0 nuget is causing a compile error
        var stats = c.GetCurrentStatistics()!;
        Assert.That(stats.TotalHits, Is.EqualTo(1));
        Assert.That(stats.TotalMisses, Is.EqualTo(2));
#endif
    }

    [Test]
    [TestCase("MaxDuration")]
    [TestCase("RecentlyUsed")]
    [TestCase("Manual-MaxDuration")]
    [TestCase("Manual-RecentlyUsed")]
    // proper way to get infinite expiration is to pass an empty MemoryCacheOptions
    public async Task GetOrCreateInfinite_Fails(string mode)
    {
        using var c = MemoryCacheUtil.Create();
        var getOrCreate = MakeGetOrCreate<Obj>(mode);

        var o1 = new Obj { Value = "value1" };

        if (mode.Contains("MaxDuration"))
        {
            Assert.ThrowsAsync<ArgumentOutOfRangeException>(async () => await getOrCreate(c, "key1", TimeSpan.MaxValue, () => VA(o1)));
        }
        else
        {
            var r1 = await getOrCreate(c, "key1", TimeSpan.MaxValue, () => VA(o1));
            Assert.That(o1, Is.SameAs(r1));
            Assert.That(r1.Value, Is.EqualTo("value1"));

            var o2 = new Obj { Value = "value2" };
            var r2 = await getOrCreate(c, "key1", TimeSpan.MaxValue, () => VA(o2));
            Assert.That(o1, Is.SameAs(r2));
            Assert.That(o2, Is.Not.SameAs(r2));
            Assert.That(r2.Value, Is.EqualTo("value1"));
        }
    }

    [Test]
    [TestCase("MaxDuration")]
    [TestCase("RecentlyUsed")]
    [TestCase("Manual-MaxDuration")]
    [TestCase("Manual-RecentlyUsed")]
    public async Task Expiration(string mode)
    {
        using var c = MemoryCacheUtil.Create();
        var getOrCreate = MakeGetOrCreate<Obj>(mode);

        var o1 = new Obj { Value = "value1" };
        var r1 = await getOrCreate(c, "key1", TimeSpan.FromMilliseconds(50), () => VA(o1));
        Assert.That(o1, Is.SameAs(r1));
        Assert.That(r1.Value, Is.EqualTo("value1"));

        await Task.Delay(75);

        var o2 = new Obj { Value = "value2" };
        var r2 = await getOrCreate(c, "key1", TimeSpan.FromMilliseconds(50), () => VA(o2));
        Assert.That(o2, Is.SameAs(r2));
        Assert.That(o2, Is.Not.SameAs(r1));
        Assert.That(r2.Value, Is.EqualTo("value2"));
    }

    [Test]
    [TestCase("MaxDuration")]
    [TestCase("RecentlyUsed")]
    [TestCase("Manual-MaxDuration")]
    [TestCase("Manual-RecentlyUsed")]
    public void ValueFactoryFails(string mode)
    {
        using var c = MemoryCacheUtil.Create();
        var getOrCreate = MakeGetOrCreate<string>(mode);

        Assert.ThrowsAsync<Exception>(async () => await getOrCreate(c, "key1", TimeSpan.FromHours(1), () => func()));

        Assert.That(c.Get("key1"), Is.Null);

        static async Task<string> func()
        {
            await Task.Yield();
            throw new Exception();
        }
    }

    [Test]
    [TestCase("MaxDuration")]
    [TestCase("RecentlyUsed")]
    [TestCase("Manual-MaxDuration")]
    [TestCase("Manual-RecentlyUsed")]
    public void MultiThread_SingleInit(string mode)
    {
        // confirms that only a single thread gets to initialize the cached entry

        using var c = MemoryCacheUtil.Create();
        var getOrCreate = MakeGetOrCreate<string>(mode);
        var cd = new ConcurrentDictionary<int, bool>();
        var cde = new CountdownEvent(1);
        var cde_start = new CountdownEvent(32);
        var cde2 = new CountdownEvent(32);

        for (int i = 0; i < 32; i++)
        {
            var t = new Thread(async () =>
            {
                int tid = Environment.CurrentManagedThreadId;
                cde_start.Signal();
                cde.Wait();
                //Console.WriteLine("{0}: get or add: {1}", tid, DateTime.UtcNow.ToString("O"));
                try
                {
                    await getOrCreate(c, "k1", TimeSpan.FromHours(1), () => gen_cache(tid));
                }
                catch (Exception /*e*/)
                {
                    //Console.WriteLine("{0}: exception: {1} {2}", tid, e.Message, DateTime.UtcNow.ToString("O"));
                }
                //Console.WriteLine("{0}: got: {1}", tid, DateTime.UtcNow.ToString("O"));
                cde2.Signal();
            });
            t.Start();
        }

        cde_start.Wait();
        cde.Signal();
        cde2.Wait();

        //Console.WriteLine("{0}", string.Join(",", cd.Keys));
        Assert.That(cd, Has.Count.EqualTo(1));

        async Task<string> gen_cache(int tid)
        {
            await Task.Yield();
            //Console.WriteLine("{0}: got IN: {1}", Environment.CurrentManagedThreadId, DateTime.UtcNow.ToString("O"));
            cd.TryAdd(tid, true);
            //Console.WriteLine("{0}: ret cache: {1}", Environment.CurrentManagedThreadId, DateTime.UtcNow.ToString("O"));
            return "abc";
        }
    }

    [Test]
    [TestCase("MaxDuration")]
    [TestCase("RecentlyUsed")]
    [TestCase("Manual-MaxDuration")]
    [TestCase("Manual-RecentlyUsed")]
    public void MultiThread_Exception(string mode)
    {
        // confirms that everybody gets to initialize the cached entry if it fails

        using var c = MemoryCacheUtil.Create();
        var getOrCreate = MakeGetOrCreate<string>(mode);
        var cd = new ConcurrentDictionary<Guid, bool>();
        var cde = new CountdownEvent(1);
        var cde2 = new CountdownEvent(32);
        int concurrentTest = 0;

        for (int i = 0; i < 32; i++)
        {
            var t = new Thread(async () =>
            {
                var tid = Guid.NewGuid();
                cde.Wait();
                //Console.WriteLine("{0}: get or add: {1}", tid, DateTime.UtcNow.ToString("O"));
                try
                {
                    await getOrCreate(c, "k1", TimeSpan.FromHours(1), () => gen_cache(tid));
                }
                catch (Exception /*e*/)
                {
                    //Console.WriteLine("{0}: exception: {1} {2}", tid, e.Message, DateTime.UtcNow.ToString("O"));
                }
                //Console.WriteLine("{0}: got: {1}", tid, DateTime.UtcNow.ToString("O"));
                cde2.Signal();
            });
            t.Start();
        }

        cde.Signal();
        cde2.Wait();

        //Console.WriteLine("{0}", string.Join(",", cd.Keys));
        Assert.That(cd, Has.Count.EqualTo(32));

        async Task<string> gen_cache(Guid tid)
        {
            if (Interlocked.Increment(ref concurrentTest) != 1)
                throw new Exception("concurrent access detected");
            await Task.Yield();
            //Console.WriteLine("{0}: got IN: {1}", tid, DateTime.UtcNow.ToString("O"));
            cd.TryAdd(tid, true);
            //Console.WriteLine("{0}: throw: {1}", tid, DateTime.UtcNow.ToString("O"));
            if (Interlocked.Decrement(ref concurrentTest) != 0)
                throw new Exception("concurrent access detected");
            throw new Exception("bad");
        }
    }
}
