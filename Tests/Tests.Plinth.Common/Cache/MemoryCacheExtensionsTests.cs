using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using Plinth.Common.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Plinth.Common.Cache;

[TestFixture]
public class MemoryCacheExtensionsTests
{
    private class Obj
    {
        public string? Value { get; set; }
    }

    private static async Task<T> VA<T>(T v)
    {
        await Task.Yield();
        return v;
    }

    [Test]
    public async Task TestInifiteExpiration()
    {
        using var c = new MemoryCache(Options.Create(new MemoryCacheOptions()));

        Assert.That(c.Get("key1"), Is.Null);

        var o1 = new Obj { Value = "value1" };
        var r1 = await c.GetOrCreateAtomicAsync("key1", new MemoryCacheEntryOptions(), () => VA(o1));
        Assert.That(o1, Is.SameAs(r1));
        Assert.That(r1.Value, Is.EqualTo("value1"));
        Assert.That(c.Get("key1"), Is.Not.Null);

        var r1a = await c.GetOrCreateAtomicAsync("key1", new MemoryCacheEntryOptions(), () => VA(o1));
        Assert.That(o1, Is.SameAs(r1a));
        Assert.That(r1, Is.SameAs(r1a));
        Assert.That(r1.Value, Is.EqualTo("value1"));
        Assert.That(c.Get("key1"), Is.Not.Null);

        c.Remove("key1");
        Assert.That(c.Get("key1"), Is.Null);

        var o2 = new Obj { Value = "value1" };
        var r2 = await c.GetOrCreateAtomicAsync("key1", new MemoryCacheEntryOptions(), () => VA(o2));
        Assert.That(o2, Is.SameAs(r2));
        Assert.That(o2, Is.Not.SameAs(r1));
        Assert.That(r2.Value, Is.EqualTo("value1"));
        Assert.That(c.Get("key1"), Is.Not.Null);
    }

    [Test]
    public async Task TestInifiteExpiration_AsyncLambda()
    {
        using var c = new MemoryCache(Options.Create(new MemoryCacheOptions()));

        Assert.That(c.Get("key1"), Is.Null);

        var o1 = new Obj { Value = "value1" };
        var r1 = await c.GetOrCreateAtomicAsync("key1", new MemoryCacheEntryOptions(), async () => await VA(o1));
        Assert.That(o1, Is.SameAs(r1));
        Assert.That(r1.Value, Is.EqualTo("value1"));
        Assert.That(c.Get("key1"), Is.Not.Null);

        var r1a = await c.GetOrCreateAtomicAsync("key1", new MemoryCacheEntryOptions(), async () => await VA(o1));
        Assert.That(o1, Is.SameAs(r1a));
        Assert.That(r1, Is.SameAs(r1a));
        Assert.That(r1.Value, Is.EqualTo("value1"));
        Assert.That(c.Get("key1"), Is.Not.Null);

        c.Remove("key1");
        Assert.That(c.Get("key1"), Is.Null);

        var o2 = new Obj { Value = "value1" };
        var r2 = await c.GetOrCreateAtomicAsync("key1", new MemoryCacheEntryOptions(), async () => await VA(o2));
        Assert.That(o2, Is.SameAs(r2));
        Assert.That(o2, Is.Not.SameAs(r1));
        Assert.That(r2.Value, Is.EqualTo("value1"));
        Assert.That(c.Get("key1"), Is.Not.Null);
    }
}
