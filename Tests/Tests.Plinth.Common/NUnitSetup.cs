using Plinth.Logging.Host;
using NUnit.Framework;

namespace Tests.Plinth.Common;

[SetUpFixture]
public class NUnitSetup
{
    [OneTimeSetUp]
    public void Setup()
    {
        StaticLogManagerSetup.ConfigureForConsoleLogging();
    }
}
