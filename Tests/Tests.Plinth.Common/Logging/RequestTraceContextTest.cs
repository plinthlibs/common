﻿using Plinth.Common.Logging;
using NUnit.Framework;

namespace Tests.Plinth.Common.Logging;

[TestFixture]
public class RequestTraceContextTest
{
    [Test]
    public void Test_ContextProperties_NoContext()
    {
        RequestTraceProperties.SetRequestTraceUserName("imbanker");
        RequestTraceProperties.SetRequestTraceSessionId("abc123");
        RequestTraceProperties.SetRequestTraceProperty("CustomProperty1", "CustomValue1");

        Assert.That(RequestTraceProperties.GetRequestTraceProperties()?.Count, Is.EqualTo(3));

        Assert.That(RequestTraceProperties.GetRequestTraceUserName(), Is.EqualTo("imbanker"));
        Assert.That(RequestTraceProperties.GetRequestTraceSessionId(), Is.EqualTo("abc123"));
        Assert.That(RequestTraceProperties.GetRequestTraceProperty("CustomProperty1"), Is.EqualTo("CustomValue1"));
    }

    [Test]
    public void Test_ContextProperties_New()
    {
        RequestTraceProperties.SetRequestTraceUserName("imbanker");
        RequestTraceProperties.SetRequestTraceSessionId("abc123");
        RequestTraceProperties.SetRequestTraceProperty("CustomProperty1", "CustomValue1");

        using (var traceContext = new RequestTraceContext())
        {
            traceContext.SetRequestTraceProperty("CustomProperty2", "CustomValue2");
            Assert.That(RequestTraceProperties.GetRequestTraceProperties()?.Count, Is.EqualTo(4));
            Assert.That(RequestTraceProperties.GetRequestTraceProperty("CustomProperty2"), Is.EqualTo("CustomValue2"));

            traceContext.SetRequestTraceProperty("CustomProperty3", "CustomValue3");
            Assert.That(RequestTraceProperties.GetRequestTraceProperties()?.Count, Is.EqualTo(5));
            Assert.That(RequestTraceProperties.GetRequestTraceProperty("CustomProperty3"), Is.EqualTo("CustomValue3"));
        }

        Assert.That(RequestTraceProperties.GetRequestTraceProperties()?.Count, Is.EqualTo(3));

        Assert.That(RequestTraceProperties.GetRequestTraceUserName(), Is.EqualTo("imbanker"));
        Assert.That(RequestTraceProperties.GetRequestTraceSessionId(), Is.EqualTo("abc123"));
        Assert.That(RequestTraceProperties.GetRequestTraceProperty("CustomProperty1"), Is.EqualTo("CustomValue1"));
    }

    [Test]
    public void Test_ContextProperties_DictContructor()
    {
        RequestTraceProperties.SetRequestTraceUserName("imbanker");
        RequestTraceProperties.SetRequestTraceSessionId("abc123");
        RequestTraceProperties.SetRequestTraceProperty("CustomProperty1", "CustomValue1");

        using (var traceContext = new RequestTraceContext(
            new Dictionary<string, string?>()
            {
                { "CustomProperty2", "CustomValue2" },
                { "CustomProperty3", "CustomValue3" }
            }))
        {
            Assert.That(RequestTraceProperties.GetRequestTraceProperties()?.Count, Is.EqualTo(5));
            Assert.That(RequestTraceProperties.GetRequestTraceProperty("CustomProperty2"), Is.EqualTo("CustomValue2"));
            Assert.That(RequestTraceProperties.GetRequestTraceProperty("CustomProperty3"), Is.EqualTo("CustomValue3"));
        }

        Assert.That(RequestTraceProperties.GetRequestTraceProperties()?.Count, Is.EqualTo(3));

        Assert.That(RequestTraceProperties.GetRequestTraceUserName(), Is.EqualTo("imbanker"));
        Assert.That(RequestTraceProperties.GetRequestTraceSessionId(), Is.EqualTo("abc123"));
        Assert.That(RequestTraceProperties.GetRequestTraceProperty("CustomProperty1"), Is.EqualTo("CustomValue1"));
    }

    [Test]
    public void Test_ContextProperties_ObjContructor()
    {
        RequestTraceProperties.SetRequestTraceUserName("imbanker");
        RequestTraceProperties.SetRequestTraceSessionId("abc123");
        RequestTraceProperties.SetRequestTraceProperty("CustomProperty1", "CustomValue1");

        using (var traceContext = new RequestTraceContext(
            new 
            {
                CustomProperty2 = "CustomValue2",
                CustomProperty3 = "CustomValue3"
            }))
        {
            Assert.That(RequestTraceProperties.GetRequestTraceProperties()?.Count, Is.EqualTo(5));
            Assert.That(RequestTraceProperties.GetRequestTraceProperty("CustomProperty2"), Is.EqualTo("CustomValue2"));
            Assert.That(RequestTraceProperties.GetRequestTraceProperty("CustomProperty3"), Is.EqualTo("CustomValue3"));
        }

        Assert.That(RequestTraceProperties.GetRequestTraceProperties()?.Count, Is.EqualTo(3));

        Assert.That(RequestTraceProperties.GetRequestTraceUserName(), Is.EqualTo("imbanker"));
        Assert.That(RequestTraceProperties.GetRequestTraceSessionId(), Is.EqualTo("abc123"));
        Assert.That(RequestTraceProperties.GetRequestTraceProperty("CustomProperty1"), Is.EqualTo("CustomValue1"));
    }

    [Test]
    public void Test_ContextProperties_CollidingOverride()
    {
        RequestTraceProperties.SetRequestTraceUserName("imbanker");
        RequestTraceProperties.SetRequestTraceSessionId("abc123");
        RequestTraceProperties.SetRequestTraceProperty("CustomProperty1", "CustomValue1");

        using (var traceContext = new RequestTraceContext())
        {
            traceContext.SetRequestTraceProperty("CustomProperty1", "CustomValue2");
            Assert.That(RequestTraceProperties.GetRequestTraceProperties()?.Count, Is.EqualTo(3));
            Assert.That(RequestTraceProperties.GetRequestTraceProperty("CustomProperty1"), Is.EqualTo("CustomValue2"));

            traceContext.SetRequestTraceProperty("CustomProperty2", "CustomValue2");
            Assert.That(RequestTraceProperties.GetRequestTraceProperties()?.Count, Is.EqualTo(4));
            Assert.That(RequestTraceProperties.GetRequestTraceProperty("CustomProperty2"), Is.EqualTo("CustomValue2"));
        }

        Assert.That(RequestTraceProperties.GetRequestTraceProperties()?.Count, Is.EqualTo(2));

        Assert.That(RequestTraceProperties.GetRequestTraceUserName(), Is.EqualTo("imbanker"));
        Assert.That(RequestTraceProperties.GetRequestTraceSessionId(), Is.EqualTo("abc123"));
    }

    [Test]
    public void Test_ContextProperties_CollidingNoOverride()
    {
        RequestTraceProperties.SetRequestTraceUserName("imbanker");
        RequestTraceProperties.SetRequestTraceSessionId("abc123");
        RequestTraceProperties.SetRequestTraceProperty("CustomProperty1", "CustomValue1");

        using (var traceContext = new RequestTraceContext())
        {
            traceContext.SetRequestTraceProperty("CustomProperty1", "CustomValue2", false);
            Assert.That(RequestTraceProperties.GetRequestTraceProperties()?.Count, Is.EqualTo(3));
            Assert.That(RequestTraceProperties.GetRequestTraceProperty("CustomProperty1"), Is.EqualTo("CustomValue1"));

            traceContext.SetRequestTraceProperty("CustomProperty2", "CustomValue2");
            Assert.That(RequestTraceProperties.GetRequestTraceProperties()?.Count, Is.EqualTo(4));
            Assert.That(RequestTraceProperties.GetRequestTraceProperty("CustomProperty2"), Is.EqualTo("CustomValue2"));
        }

        Assert.That(RequestTraceProperties.GetRequestTraceProperties()?.Count, Is.EqualTo(3));

        Assert.That(RequestTraceProperties.GetRequestTraceUserName(), Is.EqualTo("imbanker"));
        Assert.That(RequestTraceProperties.GetRequestTraceSessionId(), Is.EqualTo("abc123"));
        Assert.That(RequestTraceProperties.GetRequestTraceProperty("CustomProperty1"), Is.EqualTo("CustomValue1"));
    }
}
