using Plinth.Common.Logging;
using NUnit.Framework;

namespace Tests.Plinth.Common.Logging;

[TestFixture]
public class RequestTraceChainTest
{
    private async Task<string> CreateNew()
    {
        await Task.Yield();

        return RequestTraceChain.Instance.Create();
    }

    [Test]
    public void Test_Instance()
    {
        RequestTraceChain.Instance.Set("123");
        CreateNew().Wait();

        Assert.That(RequestTraceChain.Instance.Get(), Is.EqualTo("123"));
    }

    [Test]
    public void Test_Generate()
    {
        RequestTraceChain.Instance.Create();
        Assert.That(RequestTraceChain.Instance.GenerateNextChain(), Is.EqualTo("0.0"));
        Assert.That(RequestTraceChain.Instance.GenerateNextChain(), Is.EqualTo("0.1"));

        RequestTraceChain.Instance.Set("123");
        Assert.That(RequestTraceChain.Instance.GenerateNextChain(), Is.EqualTo("123.0"));
        Assert.That(RequestTraceChain.Instance.GenerateNextChain(), Is.EqualTo("123.1"));
    }

    [Test]
    public void Test_Generate_Async()
    {
        int expectedResult = 0;
        RequestTraceChain.Instance.Create();
        Assert.That(RequestTraceChain.Instance.GenerateNextChain(), Is.EqualTo($"0.{expectedResult++}"));
        Assert.That(RequestTraceChain.Instance.GenerateNextChain(), Is.EqualTo($"0.{expectedResult++}"));

        int iterations = 100;
        expectedResult += iterations;

        var tasks = new List<Task>();
        for (int i = 0; i < iterations; i++)
        {
            tasks.Add(Task.Run(async () =>
            {
                await Task.Delay(0);
                RequestTraceChain.Instance.GenerateNextChain();
            }));
        }

        Task.WaitAll(tasks.ToArray());

        Assert.That(RequestTraceChain.Instance.GenerateNextChain(), Is.EqualTo($"0.{expectedResult++}"));
    }

    [Test]
    public void Test_Set()
    {
        var r1 = new RequestTraceChain();

        r1.Set("abcd1234ab");
        Assert.That(r1.Get(), Is.EqualTo("abcd1234ab"));

        r1.Set("12345678EF");
        Assert.That(r1.Get(), Is.EqualTo("12345678EF"));
    }

    [Test]
    public void Test_Set_Null()
    {
        var r1 = new RequestTraceChain();

        r1.Set(null!);
        Assert.That(r1.GenerateNextChain(), Is.EqualTo("0"));
    }
}
