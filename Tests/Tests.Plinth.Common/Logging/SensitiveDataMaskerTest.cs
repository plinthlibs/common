using NUnit.Framework;
using Plinth.Common.Logging;

namespace Tests.Plinth.Common.Utils;

[TestFixture]
public class SensitiveDataMaskerTest
{
    private SensitiveDataMasker _masker = new SensitiveDataMasker(new SensitiveDataMaskerSettings());

    [SetUp]
    public void SetUp()
    {
        var settings = new SensitiveDataMaskerSettings();
        settings.FieldsToRedact.Add(("ssn", "\\d{3}[\\-]?\\d{2}[\\-]?\\d{4}"));
        settings.FieldsToRedact.Add(("[a-z]*password", null));
        settings.FieldsToRedact.Add(("[a-z]*cardNumber", null));

        _masker = new SensitiveDataMasker(settings);
    }

    #region mask all
    [Test]
    public void Test_DoNotMaskNonSensitiveJsonValues()
    {
        string s = "{ \"borrower\": \"buddy\", \"cosigner\": \"buddy's buddy\", \"primary_phone\": \"123-435-6678\", \"work_phone\": \"1234356678\" }";

        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo(s));
    }

    [Test]
    public void Test_DoNotMaskNonSensitiveXmlValues()
    {
        string s =
            "<info>" +
            "   <borrower>buddy</borrower>" +
            "   <cosigner>buddy's buddy</cosigner>" +
            "   <primary_phone>123-435-6678</primary_phone>" +
            "   <work_phone>1234356678</work_phone>" +
            "</info>";

        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo(s));
    }

    [Test]
    public void Test_DoNotMaskNonSensitiveQueryStringValues()
    {
        string s = "www.Plinth.com?borrower=buddy&cosigner=buddy%27s%20buddy&primary_phone=123-435-6678&work_phone=1234356678";

        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo(s));
    }
    #endregion

    #region ssn
    [Test]
    public void Test_MaskJsonSsn()
    {
        string s = "{ \"borrower\": \"buddy\", \"ssn\": \"123-45-6678\" }";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("{ \"borrower\": \"buddy\", \"ssn\": \"_REDACTED_\" }"));

        s = "{ \"ssnab\": \"buddy\", \"Ssn\":\"123456789\" }";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("{ \"ssnab\": \"buddy\", \"Ssn\":\"_REDACTED_\" }"));

        s = "{ \"ssn\": \"123-45-6789\", \"Ssn\":\"123456789\" }";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("{ \"ssn\": \"_REDACTED_\", \"Ssn\":\"_REDACTED_\" }"));

        s = "<noField>11</noField>";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("<noField>11</noField>"));
    }

    [Test]
    public void Test_MaskXmlSsn()
    {
        string s =
            "<info>" +
            "   <ssn>111-22-3344</ssn>" +
            "   <phone>1234356678</phone>" +
            "   <ssn>111-22-3344</ssn>" +
            "</info>";

        Assert.That(
            _masker.MaskSensitiveFields(s), Is.EqualTo("<info>" +
            "   <ssn>_REDACTED_</ssn>" +
            "   <phone>1234356678</phone>" +
            "   <ssn>_REDACTED_</ssn>" +
            "</info>"));

        s =
            "<info>" +
            "   <ssnRule>111-22-3344</ssn>" +
            "   <phone>1234356678</phone>" +
            "   <ssn>111-22-3344</ssn>" +
            "</info>";

        Assert.That(
            _masker.MaskSensitiveFields(s), Is.EqualTo("<info>" +
            "   <ssnRule>111-22-3344</ssn>" +
            "   <phone>1234356678</phone>" +
            "   <ssn>_REDACTED_</ssn>" +
            "</info>"));
    }

    [Test]
    public void Test_MaskQueryStringSsn()
    {
        string s = "www.Plinth.com?borrower=buddy&ssn=123-45-6678";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("www.Plinth.com?borrower=buddy&ssn=_REDACTED_"));

        s = "www.Plinth.com?ssnRule=buddy&ssn=123-45-6678";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("www.Plinth.com?ssnRule=buddy&ssn=_REDACTED_"));

        s = "www.Plinth.com?ssnRule=buddy&ssn=123-45-6678&a=b&ssn=098-12-5678";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("www.Plinth.com?ssnRule=buddy&ssn=_REDACTED_&a=b&ssn=_REDACTED_"));
    }

    [Test]
    public void Test_MaskSsnCase()
    {
        string s = "{ \"borrower\": \"buddy\", \"Ssn\":\"123456789\" }";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("{ \"borrower\": \"buddy\", \"Ssn\":\"_REDACTED_\" }"));
    }
    #endregion

    #region password
    [Test]
    public void Test_MaskJsonPassword()
    {
        string s = "{ \"borrower\": \"buddy\", \"password\": \"abc123\", \"password\": \"123\" }";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("{ \"borrower\": \"buddy\", \"password\": \"_REDACTED_\", \"password\": \"_REDACTED_\" }"));

        s = "{ \"PasswordRule\": \"buddy\", \"password\": \"abc123\" }";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("{ \"PasswordRule\": \"buddy\", \"password\": \"_REDACTED_\" }"));

        s = "<noField>11</noField>";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("<noField>11</noField>"));
    }

    [Test]
    public void Test_MaskXmlPassword()
    {
        string s =
            "<info>" +
            "   <password>Plinth123</password>" +
            "   <phone>1234356678</phone>" +
            "   <password>Plinth123</password>" +
            "   <userPassword>Plinth123</userPassword>" +
            "</info>";

        Assert.That(
            _masker.MaskSensitiveFields(s), Is.EqualTo("<info>" +
            "   <password>_REDACTED_</password>" +
            "   <phone>1234356678</phone>" +
            "   <password>_REDACTED_</password>" +
            "   <userPassword>_REDACTED_</userPassword>" +
            "</info>"));

        s =
            "<info>" +
            "   <passwordRule>Plinth123</passwordRule>" +
            "   <phone>1234356678</phone>" +
            "   <password>Plinth123</password>" +
            "</info>";

        Assert.That(
            _masker.MaskSensitiveFields(s), Is.EqualTo("<info>" +
            "   <passwordRule>Plinth123</passwordRule>" +
            "   <phone>1234356678</phone>" +
            "   <password>_REDACTED_</password>" +
            "</info>"));
    }

    [Test]
    public void Test_MaskQueryStringPassword()
    {
        string s = "www.Plinth.com?borrower=buddy&password=Plinth123";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("www.Plinth.com?borrower=buddy&password=_REDACTED_"));

        s = "www.Plinth.com?passwordRule=buddy&password=Plinth123";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("www.Plinth.com?passwordRule=buddy&password=_REDACTED_"));

        s = "www.Plinth.com?passwordRule=buddy&password=Plinth123&a=b&password=123";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("www.Plinth.com?passwordRule=buddy&password=_REDACTED_&a=b&password=_REDACTED_"));
    }

    [Test]
    public void Test_MaskPrefixedPassword()
    {
        string s = "{ \"borrower\": \"buddy\", \"newPassword\": \"abc123\", \"oldPassword\": \"123\" }";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("{ \"borrower\": \"buddy\", \"newPassword\": \"_REDACTED_\", \"oldPassword\": \"_REDACTED_\" }"));

        s = "{ \"newPasswordRule\": \"buddy\", \"newPassword\": \"abc123\" }";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("{ \"newPasswordRule\": \"buddy\", \"newPassword\": \"_REDACTED_\" }"));
    }

    [Test]
    public void Test_MaskPasswordCase()
    {
        string s = "{ \"borrower\": \"buddy\", \"PaSsWoRd\": \"abc123\" }";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("{ \"borrower\": \"buddy\", \"PaSsWoRd\": \"_REDACTED_\" }"));

        s = "{ \"PaSsWoRdRule\": \"buddy\", \"PaSsWoRd\": \"abc123\" }";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("{ \"PaSsWoRdRule\": \"buddy\", \"PaSsWoRd\": \"_REDACTED_\" }"));
    }
    #endregion

    #region card number
    [Test]
    public void Test_MaskJsonCreditCardNumber()
    {
        string s = "{ \"firstName\": \"buddy\", \"cardNumber\": \"4111111111111111\", \"cardNumber\": \"2\" }";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("{ \"firstName\": \"buddy\", \"cardNumber\": \"_REDACTED_\", \"cardNumber\": \"_REDACTED_\" }"));

        s = "{ \"cardNumberRule\": \"buddy\", \"cardNumber\": \"4111111111111111\" }";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("{ \"cardNumberRule\": \"buddy\", \"cardNumber\": \"_REDACTED_\" }"));

        s = "<noField>11</noField>";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("<noField>11</noField>"));
    }

    [Test]
    public void Test_MaskXmlCreditCardNumber()
    {
        string s =
            "<info>" +
            "   <cardNumber>Plinth123</cardNumber>" +
            "   <firstName>1234356678</firstName>" +
            "   <cardNumber>Plinth123</cardNumber>" +
            "   <creditCardNumber>Plinth123</creditCardNumber>" +
            "</info>";

        Assert.That(
            _masker.MaskSensitiveFields(s), Is.EqualTo("<info>" +
            "   <cardNumber>_REDACTED_</cardNumber>" +
            "   <firstName>1234356678</firstName>" +
            "   <cardNumber>_REDACTED_</cardNumber>" +
            "   <creditCardNumber>_REDACTED_</creditCardNumber>" +
            "</info>"));

        s =
            "<info>" +
            "   <cardNumberRule>Plinth123</cardNumberRule>" +
            "   <firstName>1234356678</firstName>" +
            "   <cardNumber>Plinth123</cardNumber>" +
            "</info>";

        Assert.That(
            _masker.MaskSensitiveFields(s), Is.EqualTo("<info>" +
            "   <cardNumberRule>Plinth123</cardNumberRule>" +
            "   <firstName>1234356678</firstName>" +
            "   <cardNumber>_REDACTED_</cardNumber>" +
            "</info>"));
    }

    [Test]
    public void Test_MaskQueryStringCreditCardNumber()
    {
        string s = "www.Plinth.com?firstName=buddy&cardNumber=Plinth123";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("www.Plinth.com?firstName=buddy&cardNumber=_REDACTED_"));

        s = "www.Plinth.com?cardNumberRule=buddy&cardNumber=Plinth123";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("www.Plinth.com?cardNumberRule=buddy&cardNumber=_REDACTED_"));

        s = "www.Plinth.com?cardNumberRule=buddy&cardNumber=Plinth123&a=b&cardNumber=Plinth123";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("www.Plinth.com?cardNumberRule=buddy&cardNumber=_REDACTED_&a=b&cardNumber=_REDACTED_"));
    }

    [Test]
    public void Test_MaskPrefixedCreditCardNumber()
    {
        string s = "{ \"firstName\": \"buddy\", \"creditCardNumber\": \"8756756785674\", \"oldCardNumber\": \"875674\" }";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("{ \"firstName\": \"buddy\", \"creditCardNumber\": \"_REDACTED_\", \"oldCardNumber\": \"_REDACTED_\" }"));

        s = "{ \"creditCardNumberRule\": \"buddy\", \"creditCardNumber\": \"8756756785674\" }";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("{ \"creditCardNumberRule\": \"buddy\", \"creditCardNumber\": \"_REDACTED_\" }"));
    }

    [Test]
    public void Test_MaskJsonCreditCardNumberCase()
    {
        string s = "{ \"firstName\": \"buddy\", \"cArDNuMbeR\": \"4111111111111111\" }";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("{ \"firstName\": \"buddy\", \"cArDNuMbeR\": \"_REDACTED_\" }"));

        s = "{ \"cardNumberRule\": \"buddy\", \"cArDNuMbeR\": \"4111111111111111\" }";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("{ \"cardNumberRule\": \"buddy\", \"cArDNuMbeR\": \"_REDACTED_\" }"));
    }
    #endregion

    #region sensitive fields
    [Test]
    public void Test_MaskJsonSensitiveFields()
    {
        string s = "{ \"borrower\": \"buddy\", \"ssn\": \"123-45-6678\", \"password\": \"Plinth123\", \"cardNumber\": \"4111111111111111\" }";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("{ \"borrower\": \"buddy\", \"ssn\": \"_REDACTED_\", \"password\": \"_REDACTED_\", \"cardNumber\": \"_REDACTED_\" }"));

        s = "<noField>11</noField>";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("<noField>11</noField>"));
    }

    [Test]
    public void Test_MaskXmlSensitiveFields()
    {
        string s =
            "<info>" +
            "   <ssn>111-22-3344</ssn>" +
            "   <phone>1234356678</phone>" +
            "   <password>Plinth123</password>" +
            "   <cardNumber>4111111111111111</cardNumber>" +
            "</info>";

        Assert.That(
            _masker.MaskSensitiveFields(s), Is.EqualTo("<info>" +
            "   <ssn>_REDACTED_</ssn>" +
            "   <phone>1234356678</phone>" +
            "   <password>_REDACTED_</password>" +
            "   <cardNumber>_REDACTED_</cardNumber>" +
            "</info>"));
    }

    [Test]
    public void Test_MaskQueryStringSensitiveFields()
    {
        string s = "www.Plinth.com?borrower=buddy&ssn=123-45-6678&password=Plinth123&cardNumber=4111111111111111";
        Assert.That(_masker.MaskSensitiveFields(s), Is.EqualTo("www.Plinth.com?borrower=buddy&ssn=_REDACTED_&password=_REDACTED_&cardNumber=_REDACTED_"));
    }
    #endregion
}
