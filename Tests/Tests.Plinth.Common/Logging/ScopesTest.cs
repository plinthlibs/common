﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static Plinth.Common.Logging.Scopes;

namespace Tests.Plinth.Common.Logging;

[TestFixture]
internal class ScopesTest
{
    [Test]
    public void TestCreate()
    {
        var o = new object();
        var s = CreateScope(
            Kvp("Item1", 273),
            Kvp("Item2", "a87432"),
            Kvp("Item3", o),
            Kvp("Item4", "1111"),
            Kvp("Item5", 273)
        );

        Assert.That(s, Has.Length.EqualTo(5));
        Assert.That(s[0].Key, Is.EqualTo("Item1"));
        Assert.That(s[0].Value, Is.EqualTo(273));
        Assert.That(s[1].Key, Is.EqualTo("Item2"));
        Assert.That(s[1].Value, Is.EqualTo("a87432"));
        Assert.That(s[2].Key, Is.EqualTo("Item3"));
        Assert.That(s[2].Value, Is.SameAs(o));
        Assert.That(s[3].Key, Is.EqualTo("Item4"));
        Assert.That(s[3].Value, Is.EqualTo("1111"));
        Assert.That(s[4].Key, Is.EqualTo("Item5"));
        Assert.That(s[4].Value, Is.EqualTo(273));
    }
}
