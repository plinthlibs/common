using Microsoft.Extensions.Logging;
using Plinth.Common.Logging;
using Plinth.Logging.Host;
using NUnit.Framework;

namespace Tests.Plinth.Common.Logging;

[TestFixture]
public class StaticLogManagerTest
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    [Test]
    public void TestGetLogger()
    {
        Assert.That(log, Is.Not.Null);
        log.Warn("test?");
    }

    [Test]
    public void TestGetLogger_NonStatic()
    {
        Assert.Throws<ArgumentException>(() => StaticLogManager.GetLogger());
    }

    [Test]
    public void TestStoreRestore()
    {
        string? ctx = StaticLogManager.GetContextId();
        string traceid = RequestTraceId.Instance.Create();
        string chain = RequestTraceChain.Instance.Create();
        RequestTraceProperties.SetRequestTraceUserName("userName", true);

        var ti = StaticLogManager.GetTraceInfo();
        using (ExecutionContext.SuppressFlow())
        {
            var t = Task.Run(() =>
            {
                Assert.That(RequestTraceId.Instance.Get(), Is.Null);

                StaticLogManager.LoadTraceInfo(ti);

                Assert.That(StaticLogManager.GetContextId(), Is.EqualTo(ctx));
                Assert.That(RequestTraceId.Instance.Get(), Is.EqualTo(traceid));
                Assert.That(RequestTraceChain.Instance.Get(), Is.EqualTo(chain));
                Assert.That(RequestTraceProperties.GetRequestTraceUserName(), Is.EqualTo("userName"));
            });

            t.Wait();
        }
    }

    [Test]
    public void TestStoreRestoreNulls()
    {
        RequestTraceId.Instance.Set(null!);
        RequestTraceChain.Instance.Set(null!);
        RequestTraceProperties.SetRequestTraceProperties(null);
        var ti = StaticLogManager.GetTraceInfo();
        using (ExecutionContext.SuppressFlow())
        {
            var t = Task.Run(() =>
            {
                StaticLogManager.LoadTraceInfo(ti);

                Assert.That(StaticLogManager.GetContextId(), Is.Null);
                Assert.That(RequestTraceId.Instance.Get(), Is.Null);
                Assert.That(RequestTraceChain.Instance.Get(), Is.Null);
                Assert.That(RequestTraceProperties.GetRequestTraceProperties(), Is.Null);
            });

            t.Wait();
        }
    }

    private class MockLogger : ILogger
    {
        public List<LogEvent> Logs { get; set; } = [];
        public string Cat { get; set; }

        public MockLogger(string cat = "none")
        {
            Cat = cat;
        }

        public class LogEvent
        {
            public LogLevel logLevel; public EventId eventId; public object? state; public Exception? exception; public string? message;
        }

        private class MockIDisposable : IDisposable
        {
            public void Dispose()
            {
            }
        }

        public IDisposable? BeginScope<TState>(TState state) where TState : notnull
        {
            return new MockIDisposable();
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception? exception, Func<TState, Exception?, string> formatter)
        {
            Logs.Add(new LogEvent { logLevel = logLevel, eventId = eventId, state = state, exception = exception, message = formatter(state, exception) });
        }
    }

    private class MockLoggerFactory : ILoggerFactory
    {
        public Dictionary<string, MockLogger> Logs = [];

        public void AddProvider(ILoggerProvider provider)
        {
        }

        public ILogger CreateLogger(string categoryName)
        {
            var l = new MockLogger(categoryName);
            Logs.Add(categoryName, l);
            return l;
        }

        public void Dispose() { }
    }

    [Test]
    public void Test_ConfigureForSingleILogger()
    {
        var mock1 = new MockLogger();
        var mock2 = new MockLogger();
        var mock3 = new MockLogger();
        var mock4 = new MockLogger();
        var mock5 = new MockLogger();
        ILogger? logger = null;
        using (ExecutionContext.SuppressFlow())
        {
            var t1 = Task.Run(() =>
            {
                // these 2 simulate StaticLogManagerSetup.ConfigureForSingleThread(mock1);
                // and private static readonly ILogger logger = StaticLogManager.GetLogger();
                StaticLogManagerSetup.AsyncLocalLoggerFactory.SetCurrentLogger(mock1);
                logger = StaticLogManagerSetup._singleThreadSingleLoggerFactory.CreateLogger("cat");
                logger.Debug("message1");
            });
            t1.Wait();
            var t2 = new Thread(() =>
            {
                // note use of previously set logger, will actually use mock2
                StaticLogManagerSetup.AsyncLocalLoggerFactory.SetCurrentLogger(mock2);
                logger!.Debug("message2");
            });
            t2.Start();
            var t3 = new Thread(() =>
            {
                // note use of previously set logger, will actually use mock3
                StaticLogManagerSetup.AsyncLocalLoggerFactory.SetCurrentLogger(mock3);
                logger!.Debug("message3");
            });
            t3.Start();
            var t4 = new Thread(() =>
            {
                // note use of previously set logger, without a logger set yet, this will noop
                logger!.Debug("message4"); 
                StaticLogManagerSetup.AsyncLocalLoggerFactory.SetCurrentLogger(mock4);
                // but this will log
                logger!.Debug("message4-a"); 
            });
            t4.Start();
            var t5 = new Thread(() =>
            {
                // example of class that hadn't loaded its logger yet
                StaticLogManagerSetup.AsyncLocalLoggerFactory.SetCurrentLogger(mock5);
                var logger5 = StaticLogManagerSetup._singleThreadSingleLoggerFactory.CreateLogger("cat2");
                logger5.Debug("message5");
            });
            t5.Start();

            t2.Join();
            t3.Join();
            t4.Join();
            t5.Join();
        }

        Assert.That(mock1.Logs, Has.Count.EqualTo(1));
        Assert.That(mock1.Logs[0].message, Is.EqualTo("message1"));
        Assert.That(mock2.Logs, Has.Count.EqualTo(1));
        Assert.That(mock2.Logs[0].message, Is.EqualTo("message2"));
        Assert.That(mock3.Logs, Has.Count.EqualTo(1));
        Assert.That(mock3.Logs[0].message, Is.EqualTo("message3"));
        Assert.That(mock4.Logs, Has.Count.EqualTo(1));
        Assert.That(mock4.Logs[0].message, Is.EqualTo("message4-a"));
        Assert.That(mock5.Logs, Has.Count.EqualTo(1));
        Assert.That(mock5.Logs[0].message, Is.EqualTo("message5"));
    }

    [Test]
    public void Test_ConfigureForSingleILoggerFactory()
    {
        var fac1 = new MockLoggerFactory();
        var fac2 = new MockLoggerFactory();
        var fac3 = new MockLoggerFactory();
        var fac4 = new MockLoggerFactory();
        var fac5 = new MockLoggerFactory();
        ILogger? logger = null;
        using (ExecutionContext.SuppressFlow())
        {
            var t1 = Task.Run(() =>
            {
                // these 2 simulate StaticLogManagerSetup.ConfigureForSingleThread(fac1);
                // and private static readonly ILogger logger = StaticLogManager.GetLogger();
                StaticLogManagerSetup.AsyncLocalLoggerFactoryWrapper.SetCurrentFactory(fac1);
                logger = StaticLogManagerSetup._singleThreadFactory.CreateLogger("cat1");
                logger.Debug("message1");
            });
            t1.Wait();
            var t2 = new Thread(() =>
            {
                // note use of previously set logger
                StaticLogManagerSetup.AsyncLocalLoggerFactoryWrapper.SetCurrentFactory(fac2);
                logger!.Debug("message2");
            });
            t2.Start();
            var t3 = new Thread(() =>
            {
                // note use of previously set logger
                StaticLogManagerSetup.AsyncLocalLoggerFactoryWrapper.SetCurrentFactory(fac3);
                logger!.Debug("message3");
            });
            t3.Start();
            var t4 = new Thread(() =>
            {
                // note use of previously set logger, without a fac set yet, this will noop
                logger!.Debug("message4"); 
                StaticLogManagerSetup.AsyncLocalLoggerFactoryWrapper.SetCurrentFactory(fac4);
                // but this will log
                logger!.Debug("message4-a"); 
            });
            t4.Start();
            var t5 = new Thread(() =>
            {
                // example of class that hadn't loaded its logger yet
                StaticLogManagerSetup.AsyncLocalLoggerFactoryWrapper.SetCurrentFactory(fac5);
                var logger5 = StaticLogManagerSetup._singleThreadFactory.CreateLogger("cat2");
                logger5.Debug("message5");
            });
            t5.Start();
            t2.Join();
            t3.Join();
            t4.Join();
            t5.Join();
        }

        Assert.That(fac1.Logs, Has.Count.EqualTo(1));
        Assert.That(fac1.Logs.First().Value.Logs, Has.Count.EqualTo(1));
        Assert.That(fac1.Logs.First().Value.Logs[0].message, Is.EqualTo("message1"));

        Assert.That(fac2.Logs, Has.Count.EqualTo(1));
        Assert.That(fac2.Logs.First().Value.Logs, Has.Count.EqualTo(1));
        Assert.That(fac2.Logs.First().Value.Logs[0].message, Is.EqualTo("message2"));

        Assert.That(fac3.Logs, Has.Count.EqualTo(1));
        Assert.That(fac3.Logs.First().Value.Logs, Has.Count.EqualTo(1));
        Assert.That(fac3.Logs.First().Value.Logs[0].message, Is.EqualTo("message3"));

        Assert.That(fac4.Logs, Has.Count.EqualTo(1));
        Assert.That(fac4.Logs.First().Value.Logs, Has.Count.EqualTo(1));
        Assert.That(fac4.Logs.First().Value.Logs[0].message, Is.EqualTo("message4-a"));

        Assert.That(fac5.Logs, Has.Count.EqualTo(1));
        Assert.That(fac5.Logs.First().Value.Logs, Has.Count.EqualTo(1));
        Assert.That(fac5.Logs.First().Value.Logs[0].message, Is.EqualTo("message5"));
    }
}
