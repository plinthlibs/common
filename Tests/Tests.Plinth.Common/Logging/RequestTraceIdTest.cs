using Plinth.Common.Logging;
using NUnit.Framework;

namespace Tests.Plinth.Common.Logging;

[TestFixture]
public class RequestTraceIdTest
{
    private async Task<string> GetNew()
    {
        await Task.Yield();

        return RequestTraceId.Instance.Create();
    }

    [Test]
    public void Test_Instance()
    {
        var s1 = GetNew().Result;
        var s2 = GetNew().Result;

        Assert.That(s2, Is.Not.EqualTo(s1));
    }

    [Test]
    public void Test_Lots()
    {
        var set = new HashSet<string>();

        for (int i = 0; i < 50000; i++)
        {
            var s = GetNew().Result;
            set.Add(s);
        }

        Assert.That(set, Has.Count.EqualTo(50000));
    }

    [Test]
    public void Test_2Machines()
    {
        var r1 = new RequestTraceId();
        var r2 = new RequestTraceId();

        Assert.That(r2.Create(), Is.Not.EqualTo(r1.Create()));
        Assert.That(r2.Get(), Is.Not.EqualTo(r1.Get()));

#pragma warning disable NUnit2009 // The same value has been provided as both the actual and the expected argument
        Assert.That(r1.Get(), Is.EqualTo(r1.Get()));
        Assert.That(r2.Get(), Is.EqualTo(r2.Get()));
#pragma warning restore NUnit2009 // The same value has been provided as both the actual and the expected argument
    }

    [Test]
    public void Test_Set()
    {
        var r1 = new RequestTraceId();

        r1.Set("abcd1234ab");
        Assert.That(r1.Get(), Is.EqualTo("abcd1234ab"));

        r1.Set("12345678EF");
        Assert.That(r1.Get(), Is.EqualTo("12345678EF"));
    }
}
