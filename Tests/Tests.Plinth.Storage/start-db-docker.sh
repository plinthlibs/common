#!/bin/bash

# this will ensure the database is fresh every time
docker compose up -d -V

# to stop, run docker-compose down
