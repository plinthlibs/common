using System.Diagnostics;
using System.Reflection;
using Plinth.Storage;
using NUnit.Framework;

namespace Tests.Plinth.Storage;

[TestFixture]
class CompressionUtilTest
{
    private static readonly string Dot = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!;

    [Test]
    public void TestCompressDecompress()
    {
        byte[] data = File.ReadAllBytes(Path.Combine(Dot, "Integration/Files/CuttingEdge.Conditions.xml"));

        Stopwatch sw = Stopwatch.StartNew();
        byte[] z = CompressionUtil.Compress(data);
        sw.Stop();

        Assert.That(z, Is.Not.Empty);
        Assert.That(z, Has.Length.LessThan(data.Length));

        //Console.WriteLine($"el = {sw.Elapsed}, before = {data.Length}, after = {z.Length}");

        byte[] unz = CompressionUtil.Decompress(z);

        Assert.That(unz, Has.Length.EqualTo(data.Length));
        Assert.That(data, Is.EqualTo(unz).AsCollection);
    }

    [Test]
    public void TestCorruption()
    {
        byte[] data = File.ReadAllBytes(Path.Combine(Dot, "Integration/Files/CuttingEdge.Conditions.xml"));

        byte[] z = CompressionUtil.Compress(data);

        Assert.That(z, Is.Not.Empty);
        Assert.That(z, Has.Length.LessThan(data.Length));

        z[20]++;

        Assert.Throws<InvalidDataException>(() => CompressionUtil.Decompress(z));
    }

    [Test]
    public void TestCompressDecompress_Stream()
    {
        var fn = Path.Combine(Dot, "Integration/Files/CuttingEdge.Conditions.xml");
        var s = File.OpenRead(fn);
        var data = File.ReadAllBytes(fn);

        var zms = new MemoryStream();
        Stopwatch sw = Stopwatch.StartNew();
        var zs = CompressionUtil.CompressStream(zms);
        sw.Stop();

        Stopwatch sw2 = Stopwatch.StartNew();
        s.CopyTo(zs);
        zs.Dispose();
        sw2.Stop();
        byte[] z = zms.ToArray();

        s.Close();

        Assert.That(z, Is.Not.Empty);
        Assert.That(z, Has.Length.LessThan(data.Length));

        //Console.WriteLine($"el = {sw.Elapsed}, el2 = {sw2.Elapsed}, before = {data.Length}, after = {z.Length}");

        Stopwatch sw3 = Stopwatch.StartNew();
        var unzs = CompressionUtil.DecompressStream(new MemoryStream(z));
        sw3.Stop();

        var unzms = new MemoryStream();
        Stopwatch sw4 = Stopwatch.StartNew();
        unzs.CopyTo(unzms);
        unzs.Close();
        sw4.Stop();
        var unz = unzms.ToArray();

        Assert.That(unz, Has.Length.EqualTo(data.Length));
        Assert.That(data, Is.EqualTo(unz).AsCollection);
    }

    [Test]
    public void TestCorruption_Stream()
    {
        byte[] data = File.ReadAllBytes(Path.Combine(Dot, "Integration/Files/CuttingEdge.Conditions.xml"));

        byte[] z = CompressionUtil.Compress(data);

        Assert.That(z, Is.Not.Empty);
        Assert.That(z, Has.Length.LessThan(data.Length));

        z[20]++;

        var s = CompressionUtil.DecompressStream(new MemoryStream(z));

#if NET7_0_OR_GREATER
        Assert.Throws<InvalidDataException>(() => s.ReadExactly(new byte[1_000_000]));
#else
#pragma warning disable CA2022 // Avoid inexact read with 'Stream.Read'
        Assert.Throws<InvalidDataException>(() => s.Read(new byte[1_000_000], 0, 1_000_000));
#pragma warning restore CA2022 // Avoid inexact read with 'Stream.Read'
#endif
    }
}
