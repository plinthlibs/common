// uncomment to run
//#define RUN_STORAGE_INT_TESTS
//#define TEST_AWS
//#define TEST_AZURE
//#define TEST_TRANSFERS_FULL // enables heavy transfer tests which take 10+ minutes
//#define RUN_NETWORK_TESTS     // uncomment to enable FSPath2 (unc path to network share)
#if RUNNING_IN_CI
// disable for now, using too much memory for bitbucket
//#define RUN_STORAGE_INT_TESTS
#endif
using System.Diagnostics;
using Plinth.Storage;
using Plinth.Storage.Exceptions;
using Plinth.Storage.Models;
#if SQLSERVER
using Plinth.Storage.MSSql;
using Microsoft.Data.SqlClient;
#elif PGSQL
using Plinth.Storage.PgSql;
using Npgsql;
#endif
using Plinth.Security.Crypto;
using Plinth.Common.Utils;
using NUnit.Framework;
using Plinth.Common.Extensions;

using StorageFS = Plinth.Storage.Providers.FileSystem;
using StorageAWS = Plinth.Storage.Providers.S3;
using StorageAzure = Plinth.Storage.Providers.AzureBlob;
using Omu.ValueInjecter;
using Plinth.Storage.Providers.FileSystem;
using Plinth.Storage.Providers.Database;
using Plinth.Common.Exceptions;
using Plinth.Storage.AWS;
using Plinth.Storage.Azure;

namespace Tests.Plinth.Storage.Integration;

#if !RUN_STORAGE_INT_TESTS
[AttributeUsage(AttributeTargets.Method)]
class TestAttribute : Attribute { }
[AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
public class TestCaseAttribute : Attribute { public TestCaseAttribute(params object[] _) {} }
#endif

[TestFixture]
#if !RUN_STORAGE_INT_TESTS
[System.Diagnostics.CodeAnalysis.SuppressMessage("Structure", "NUnit1028:The non-test method is public", Justification = "<Pending>")]
#endif
class StorageIntTest
{
    // update this with the key when running azure tests
    private const string AzureBlobAccountKey = "";

    // update these with the access info when running aws tests
    private const string S3_AccessID = "";
    private const string S3_AccessKey = "";

    private readonly ISecureData _crypto = new SecureData("12345678901234567890123456789012");

    private readonly string FSPath1 = Path.Combine(IntegrationTestSetup.Temp, "FS1");
    private readonly string FSPath2 = Path.Combine(@"\\192.168.1.48\d$\temp", "FS1");
    private readonly string FSPath3 = Path.Combine(IntegrationTestSetup.Temp, "FS3");
    private readonly string F1 = Path.Combine(IntegrationTestSetup.Dot, "Files/test1.pdf");
    private readonly string F2 = Path.Combine(IntegrationTestSetup.Dot, "Files/test2.pdf");
    private readonly string F1xml = Path.Combine(IntegrationTestSetup.Dot, "Files/test1.xml");

    public enum FSNum
    {
        None = -1,
        BasePath = 1,
        NetworkPath = 2,
        BackupPath = 3
    }

    [OneTimeSetUp]
    public async Task SetUp()
    {
        await IntegrationTestSetup.StartUpDatabase();
    }

    [OneTimeTearDown]
    public async Task TearDown()
    {
        await IntegrationTestSetup.CleanUpDatabase();

        if (Directory.Exists(FSPath1)) Directory.Delete(FSPath1, true);
        if (Directory.Exists(FSPath3)) Directory.Delete(FSPath3, true);
#if RUN_NETWORK_TESTS
        if (Directory.Exists(FSPath2)) Directory.Delete(FSPath2, true);
#endif
    }

    [SetUp]
    public void TestSetUp()
    {
        if (Directory.Exists(FSPath1)) Directory.Delete(FSPath1, true);
        if (Directory.Exists(FSPath3)) Directory.Delete(FSPath3, true);
#if RUN_NETWORK_TESTS
        if (Directory.Exists(FSPath2)) Directory.Delete(FSPath2, true);
#endif

        IntegrationTestSetup.SetUpTest();
    }

    [TearDown]
    public void TestTearDown()
    {
        IntegrationTestSetup.CleanUpTest();
    }

    public enum WriterType
    {
        DB, File
    }

#region factories
    private StorageFactory GetStorageFactory(BlobFeatures? defaultFeatures = null, bool supplyCrypto = true)
    {
        var driverParam =
#if SQLSERVER
                (MSSqlBlobDriver.DriverSchemaParam, IntegrationTestSetup.Schema);
#elif PGSQL
                (PgSqlBlobDriver.DriverSchemaParam, IntegrationTestSetup.Schema);
#endif

        if (supplyCrypto)
            return new StorageFactory(_crypto, defaultFeatures ?? BlobFeatures.Encrypted, driverParam);
        else
            return new StorageFactory(defaultFeatures ?? BlobFeatures.Encrypted, driverParam);
    }

    private StorageFactory GetDBFactory(BlobFeatures? defaultFeatures = null)
    {
        var fac = GetStorageFactory(defaultFeatures);
        fac.AddDatabaseProvider();
        fac.SetDefaultWriteProviderAsDatabase();
        return fac;
    }

    private StorageFactory GetFileFactory(FSNum fsNum = FSNum.BasePath, BlobFeatures? defaultFeatures = null, FSNum? backupFs = null, StorageFS.DefaultIndexStrategy? strategy = null)
    {
        var fac = GetStorageFactory(defaultFeatures);
        fac.AddDatabaseProvider();
        var basePath = fsNum switch
        {
            FSNum.BasePath => FSPath1,
            FSNum.BackupPath => FSPath3,
            FSNum.NetworkPath => FSPath2,
            _ => throw new Exception()
        };

        var backupBasePath = backupFs switch
        {
            FSNum.BasePath => FSPath1,
            FSNum.BackupPath => FSPath3,
            FSNum.NetworkPath => FSPath2,
            _ => null
        };
        fac.AddFileSystemProvider(new StorageFS.FileSystemSettings()
        {
            BasePath = basePath,
            DefaultIndexStrategy = strategy.GetValueOrDefault()
        });

        fac.SetDefaultWriteProviderAsFileSystem();
        if (backupBasePath != null)
        {
            fac.AddBackupFileSystemProvider(new StorageFS.FileSystemSettings
            {
                BasePath = backupBasePath,
                DefaultIndexStrategy = strategy.GetValueOrDefault()
            });
        }

        return fac;
    }

    private StorageFactory GetFileOrDBFactory(WriterType writerType, FSNum fsNum = FSNum.BasePath, BlobFeatures? defaultFeatures = null, StorageFS.DefaultIndexStrategy? strategy = null, FSNum? backupFs = null)
    {
        return writerType switch
        {
            WriterType.DB => GetDBFactory(defaultFeatures),
            WriterType.File => GetFileFactory(fsNum, defaultFeatures, backupFs, strategy),
            _ => throw new Exception()
        };
    }
#endregion

#region Test Basic
    [Test]
    [TestCase(false, WriterType.DB)]
    [TestCase(true, WriterType.DB)]
    [TestCase(false, WriterType.File)]
    [TestCase(true, WriterType.File)]
    public async Task TestAsync_FlipEncrypt(bool encrypt, WriterType type)
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetFileOrDBFactory(type).Get(c, "TestUser");

                Assert.That(await bs.ReadBlobAsync(Guid.NewGuid()), Is.Null);

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                var g1 = await bs.CreateNewBlobAsync(b1, null, encrypt ? BlobFeatures.Encrypted : BlobFeatures.None);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));

                var b1r = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b1r.Guid!.Value, Is.EqualTo(g1));
                Assert.That(b1r.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(encrypt));
                Assert.That(b1r.Name, Is.EqualTo("Test1"));
                Assert.That(b1r.MimeType, Is.EqualTo("application/pdf"));
                switch (type)
                {
                    case WriterType.DB: Assert.That(b1r.Index, Is.Null); break;
                    case WriterType.File: Assert.That(b1r.Index, Is.EqualTo(string.Empty)); break;
                }
                if (!encrypt)
                    Assert.That(b1r.BlobSize, Is.EqualTo(b1d.Length));
                Assert.That(b1r.Data, Is.EqualTo(b1d).AsCollection);

                var b2d = File.ReadAllBytes(F2);
                b1r.Data = b2d;
                b1r.Name = "Test2";
                b1r.MimeType = "application/x-pdf";
                var b2g = await bs.RewriteBlobAsync(b1r, null, encrypt ? BlobFeatures.None : BlobFeatures.Encrypted);
                switch (type)
                {
                    case WriterType.DB: Assert.That(b2g, Is.Null); break;
                    case WriterType.File:
                        Assert.That(b2g, Is.Not.Null);
                        g1 = b2g.Value;
                        b1r.Guid = b2g.Value;
                        break;
                }

                var b2r = await bs.ReadBlobAsync(b1r.Guid.Value) ?? throw new Exception();

                Assert.That(b2r.Guid!.Value, Is.EqualTo(g1));
                Assert.That(b2r.Data, Is.Not.Null);
                if (encrypt)
                    Assert.That(b2r.BlobSize, Is.EqualTo(b2d.Length));
                Assert.That(b2r.Data, Is.EqualTo(b2d).AsCollection);
                Assert.That(b2r.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(!encrypt));
                Assert.That(b2r.Name, Is.EqualTo("Test2"));
                Assert.That(b2r.MimeType, Is.EqualTo("application/x-pdf"));
                switch (type)
                {
                    case WriterType.DB: Assert.That(b2r.Index, Is.Null); break;
                    case WriterType.File: Assert.That(b2r.Index, Is.EqualTo(string.Empty)); break;
                }
            });
    }

    [Test]
    [TestCase(false, WriterType.DB, FSNum.None)]
    [TestCase(true, WriterType.DB, FSNum.None)]
    [TestCase(false, WriterType.File, FSNum.BasePath)]
    [TestCase(true, WriterType.File, FSNum.BasePath)]
#if RUN_NETWORK_TESTS
    [TestCase(false, WriterType.File, FSNum.NetworkPath)]
    [TestCase(true, WriterType.File, FSNum.NetworkPath)]
#endif
    public async Task TestAsync(bool encrypt, WriterType type, FSNum fs)
    {

        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetFileOrDBFactory(type, fs).Get(c, "TestUser");

                Assert.That(await bs.ReadBlobAsync(Guid.NewGuid()), Is.Null);

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                var g1 = await bs.CreateNewBlobAsync(b1, null, encrypt ? BlobFeatures.Encrypted : BlobFeatures.None);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));

                var b1i = await bs.ReadBlobIndexAsync(g1) ?? throw new Exception();
                Assert.That(b1i.Guid!.Value, Is.EqualTo(g1));
                Assert.That(b1i.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(encrypt));
                Assert.That(b1i.Features.HasFlag(BlobFeatures.Compressed), Is.False);
                Assert.That(b1i.Name, Is.EqualTo("Test1"));
                Assert.That(b1i.MimeType, Is.EqualTo("application/pdf"));

                var b1r = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b1r.Guid!.Value, Is.EqualTo(g1));
                Assert.That(b1r.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(encrypt));
                Assert.That(b1r.Name, Is.EqualTo("Test1"));
                Assert.That(b1r.MimeType, Is.EqualTo("application/pdf"));
                switch (type)
                {
                    case WriterType.DB: Assert.That(b1r.Index, Is.Null); break;
                    case WriterType.File: Assert.That(b1r.Index, Is.EqualTo(string.Empty)); break;
                }
                if (!encrypt)
                    Assert.That(b1r.BlobSize, Is.EqualTo(b1d.Length));
                Assert.That(b1r.Data, Is.EqualTo(b1d).AsCollection);

                //File.WriteAllBytes(Path.Combine(IntegrationTestSetup.Temp, "test1.pdf"), b1.Data);

                var b2d = File.ReadAllBytes(F2);
                b1r.Data = b2d;
                b1r.Name = "Test2";
                b1r.MimeType = "application/x-pdf";
                var b2g = await bs.RewriteBlobAsync(b1r, null, encrypt ? BlobFeatures.Encrypted : BlobFeatures.None);
                switch (type)
                {
                    case WriterType.DB: Assert.That(b2g, Is.Null); break;
                    case WriterType.File:
                        Assert.That(b2g, Is.Not.Null);
                        g1 = b2g.Value;
                        b1r.Guid = b2g.Value;
                        break;
                }

                var b2r = await bs.ReadBlobAsync(b1r.Guid.Value) ?? throw new Exception();

                Assert.That(b2r.Guid!.Value, Is.EqualTo(g1));
                Assert.That(b2r.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(encrypt));
                Assert.That(b2r.Name, Is.EqualTo("Test2"));
                Assert.That(b2r.MimeType, Is.EqualTo("application/x-pdf"));
                switch (type)
                {
                    case WriterType.DB: Assert.That(b2r.Index, Is.Null); break;
                    case WriterType.File: Assert.That(b2r.Index, Is.EqualTo(string.Empty)); break;
                }
                if (!encrypt)
                    Assert.That(b2r.BlobSize, Is.EqualTo(b2d.Length));
                Assert.That(b2r.Data, Is.EqualTo(b2d).AsCollection);

                //File.WriteAllBytes(Path.Combine(IntegrationTestSetup.Temp, $"test2-{encrypt}.pdf"), b2r.Data);

                var b3 = await bs.ReadBlobAsync(g1);
                Assert.That(b3, Is.Not.Null);

                switch (type)
                {
                    case WriterType.DB:
#if SQLSERVER
                        c.ExecuteRaw($"UPDATE {IntegrationTestSetup.Schema}.BLOB SET Data = CONVERT(VARBINARY(200), '0x9473FBCCBC01AF12345678abcd123456', 1) WHERE GUID = '{g1}'");
#elif PGSQL
                        c.ExecuteRaw($"UPDATE {IntegrationTestSetup.Schema}.blob SET data = decode('9473FBCCBC01AF12345678abcd123456', 'hex') WHERE guid = '{g1}'");
#endif
                        break;

                    case WriterType.File:
                        File.WriteAllBytes(Path.Combine(fs == FSNum.BasePath ? FSPath1 : FSPath2, g1.ToString().ToLower()), Convert.FromHexString("9473FBCCBC01AF12345678abcd123456"));
                        break;
                }
                Assert.ThrowsAsync<CorruptedBlobException>(async () => await bs.ReadBlobAsync(g1));
            });
    }
#endregion

#region Test Features
    [Test]
    public async Task Features_Async()
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage db = GetFileOrDBFactory(WriterType.DB, FSNum.BasePath, BlobFeatures.Compressed | BlobFeatures.Encrypted).Get(c, "TestUser");

                IStorage file = GetFileOrDBFactory(WriterType.File, FSNum.BasePath, BlobFeatures.Compressed | BlobFeatures.Encrypted).Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };

                var b1_default = await db.CreateNewBlobAsync(b1);
                var b1_default_r = await db.ReadBlobAsync(b1_default) ?? throw new Exception();
                Assert.That(b1_default_r.Features, Is.EqualTo(BlobFeatures.Encrypted | BlobFeatures.Compressed));
                Assert.That(b1_default_r.StoredSize, Is.Not.EqualTo(b1d.Length));

                var b1_override = await db.CreateNewBlobAsync(b1, null, BlobFeatures.None);
                var b1_override_r = await db.ReadBlobAsync(b1_override) ?? throw new Exception();
                Assert.That(b1_override_r.Features, Is.EqualTo(BlobFeatures.None));
                Assert.That(b1_override_r.StoredSize, Is.EqualTo(b1d.Length));

                var b2_override = await db.CreateNewBlobAsync(b1, null, BlobFeatures.Compressed);
                var b2_override_r = await db.ReadBlobAsync(b2_override) ?? throw new Exception();
                Assert.That(b2_override_r.Features, Is.EqualTo(BlobFeatures.Compressed));
                Assert.That(b2_override_r.StoredSize, Is.Not.EqualTo(b1d.Length));

                b1_default = await file.CreateNewBlobAsync(b1);
                b1_default_r = await file.ReadBlobAsync(b1_default) ?? throw new Exception();
                Assert.That(b1_default_r.Features, Is.EqualTo(BlobFeatures.Encrypted | BlobFeatures.Compressed));
                Assert.That(b1_default_r.StoredSize, Is.Not.EqualTo(b1d.Length));

                b1_override = await file.CreateNewBlobAsync(b1, null, BlobFeatures.None);
                b1_override_r = await file.ReadBlobAsync(b1_override) ?? throw new Exception();
                Assert.That(b1_override_r.Features, Is.EqualTo(BlobFeatures.None));
                Assert.That(b1_override_r.StoredSize, Is.EqualTo(b1d.Length));

                b2_override = await file.CreateNewBlobAsync(b1, null, BlobFeatures.Compressed);
                b2_override_r = await file.ReadBlobAsync(b2_override) ?? throw new Exception();
                Assert.That(b2_override_r.Features, Is.EqualTo(BlobFeatures.Compressed));
                Assert.That(b2_override_r.StoredSize, Is.Not.EqualTo(b1d.Length));
            });
    }

    [Test]
    public async Task Features_Crypto_Async()
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                Assert.Throws<ArgumentException>(() => new StorageFactory(BlobFeatures.Encrypted));
                Assert.Throws<ArgumentException>(() => new StorageFactory(BlobFeatures.Compressed | BlobFeatures.Encrypted));

                var fac = GetStorageFactory(defaultFeatures: BlobFeatures.None, supplyCrypto: false);
                fac.AddDatabaseProvider();
                fac.SetDefaultWriteProviderAsDatabase();
                IStorage db = fac.Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };

                Assert.ThrowsAsync<InvalidOperationException>(async () => await db.CreateNewBlobAsync(b1, null, BlobFeatures.Encrypted));

                var b1_default = await db.CreateNewBlobAsync(b1);
                var b1_default_r = await db.ReadBlobAsync(b1_default) ?? throw new Exception();
                Assert.That(b1_default_r.Features, Is.EqualTo(BlobFeatures.None));
                Assert.That(b1_default_r.StoredSize, Is.EqualTo(b1d.Length));

                IStorage dbCrypto = GetDBFactory(BlobFeatures.Encrypted).Get(c, "TestUser");
                var g1crypto = await dbCrypto.CreateNewBlobAsync(b1);
                var g1compr = await dbCrypto.CreateNewBlobAsync(b1, null, BlobFeatures.Compressed);

                Assert.ThrowsAsync<InvalidOperationException>(async () => await db.ReadBlobAsync(g1crypto));

                var b1compr = await db.ReadBlobAsync(g1compr) ?? throw new Exception();
                Assert.That(b1compr.Features, Is.EqualTo(BlobFeatures.Compressed));
                Assert.That(b1d, Has.Length.GreaterThan(b1compr.StoredSize));
                Assert.That(b1d, Is.EqualTo(b1compr.Data).AsCollection);
            });
    }
#endregion

#region Test Perf
    [Test]
    [TestCase(WriterType.DB, FSNum.None)]
    [TestCase(WriterType.File, FSNum.BasePath)]
#if RUN_NETWORK_TESTS
    [TestCase(WriterType.File, FSNum.NetworkPath)]
#endif
    public async Task Async_Perf(WriterType type, FSNum fs)
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetFileOrDBFactory(type, fs).Get(c, "TestUser");

                Assert.That(await bs.ReadBlobAsync(Guid.NewGuid()), Is.Null);

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                var g1 = await bs.CreateNewBlobAsync(b1, null, BlobFeatures.None);

                var b2d = File.ReadAllBytes(F2);
                var b2 = new Blob()
                {
                    Name = "Test2",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                var g2 = await bs.CreateNewBlobAsync(b2, null, BlobFeatures.None);

                Stopwatch sw = Stopwatch.StartNew();
                for (int i = 0; i < 100; i++)
                {
                    var r = await bs.ReadBlobAsync(i % 2 == 0 ? g1 : g2);
                    //Console.WriteLine($"Async (local): {sw.Elapsed}");
                }
                sw.Stop();
                //Console.WriteLine($"Async (local): {sw.Elapsed}");
            });
    }
#endregion

#region File Indexing
    [Test]
    public async Task FileIndexing()
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetFileFactory().Get(c, "TestUser");

                Assert.That(await bs.ReadBlobAsync(Guid.NewGuid()), Is.Null);

                static string idx(Blob b) => Path.Combine(b.Guid!.Value.ToString()[..1], b.Guid.Value.ToString().Substring(1, 1));

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                var g1 = await bs.CreateNewBlobAsync(b1, idx, BlobFeatures.None);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));

                var b1r = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b1r.Guid!.Value, Is.EqualTo(g1));
                Assert.That(b1r.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(false));
                Assert.That(b1r.Name, Is.EqualTo("Test1"));
                Assert.That(b1r.MimeType, Is.EqualTo("application/pdf"));
                Assert.That(b1r.Index, Is.EqualTo(idx(b1r)));
                Assert.That(b1r.BlobSize, Is.EqualTo(b1d.Length));
                Assert.That(b1r.Data, Is.EqualTo(b1d).AsCollection);

                //File.WriteAllBytes(Path.Combine(IntegrationTestSetup.Temp, "test1.pdf"), b1.Data);

                var b2d = File.ReadAllBytes(F2);
                b1r.Data = b2d;
                b1r.Name = "Test2";
                b1r.MimeType = "application/x-pdf";
                var b2g = await bs.RewriteBlobAsync(b1r, null, BlobFeatures.None);
                Assert.That(b2g, Is.Not.Null);
                g1 = b2g.Value;
                b1r.Guid = b2g.Value;

                var b2r = await bs.ReadBlobAsync(b1r.Guid.Value) ?? throw new Exception();

                Assert.That(b2r.Guid!.Value, Is.EqualTo(g1));
                Assert.That(b2r.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(false));
                Assert.That(b2r.Name, Is.EqualTo("Test2"));
                Assert.That(b2r.MimeType, Is.EqualTo("application/x-pdf"));
                Assert.That(b2r.Index, Is.EqualTo(b1r.Index)); // blob is not reindexed when replaced
                Assert.That(b2r.BlobSize, Is.EqualTo(b2d.Length));
                Assert.That(b2r.Data, Is.EqualTo(b2d).AsCollection);

                //File.WriteAllBytes(Path.Combine(IntegrationTestSetup.Temp, "test2.pdf"), b2r.Data);

                var b3 = await bs.ReadBlobAsync(g1);
                Assert.That(b3, Is.Not.Null);

                File.WriteAllBytes(Path.Combine(FSPath1, b1r.Index!, g1.ToString().ToLowerInvariant()), Convert.FromHexString("9473FBCCBC01AF12345678abcd123456"));
                Assert.ThrowsAsync<CorruptedBlobException>(async () => await bs.ReadBlobAsync(g1));
            });
    }

    [Test]
    [TestCase(StorageFS.DefaultIndexStrategy.ByDate)]
    [TestCase(StorageFS.DefaultIndexStrategy.ByDateTime)]
    public async Task FileIndexingDefaults(StorageFS.DefaultIndexStrategy dis)
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetFileOrDBFactory(WriterType.File, FSNum.BasePath, strategy: dis).Get(c, "TestUser");

                Assert.That(await bs.ReadBlobAsync(Guid.NewGuid()), Is.Null);

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                DateTime dt = DateTime.UtcNow;
                var g1 = await bs.CreateNewBlobAsync(b1, null, BlobFeatures.None);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));
                string? expIndex = null;
                switch (dis)
                {
                    case StorageFS.DefaultIndexStrategy.ByDate:
                        expIndex = Path.Combine(dt.Year.ToString("D2"), dt.Month.ToString("D2"), dt.Day.ToString("D2"));
                        break;

                    case StorageFS.DefaultIndexStrategy.ByDateTime:
                        expIndex = Path.Combine(dt.Year.ToString("D2"), dt.Month.ToString("D2"), dt.Day.ToString("D2"), dt.Hour.ToString("D2"));
                        break;
                }
                Assert.That(b1.Index, Is.EqualTo(expIndex));

                var b1r = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b1r.Guid!.Value, Is.EqualTo(g1));
                Assert.That(b1r.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(false));
                Assert.That(b1r.Name, Is.EqualTo("Test1"));
                Assert.That(b1r.MimeType, Is.EqualTo("application/pdf"));
                Assert.That(b1r.Index, Is.EqualTo(expIndex));
                Assert.That(b1r.BlobSize, Is.EqualTo(b1d.Length));
                Assert.That(b1r.Data, Is.EqualTo(b1d).AsCollection);
            });
    }

    [Test]
    public async Task FileIndexing_Custom()
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                bool called = false;
                var fac = GetStorageFactory();
                fac.AddFileSystemProvider(new StorageFS.FileSystemSettings
                {
                    BasePath = FSPath1,
                    CustomIndexStrategy = b => { called = true; return $"{b.Data!.Length}/{b.Guid!.ToString()![0]}"; }
                });
                fac.SetDefaultWriteProviderAsFileSystem();
                var bs = fac.Get(c, "TestUser");

                Assert.That(await bs.ReadBlobAsync(Guid.NewGuid()), Is.Null);

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                DateTime dt = DateTime.UtcNow;
                var g1 = await bs.CreateNewBlobAsync(b1, null, BlobFeatures.None);
                Assert.That(called);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));
                var expIndex = $"{b1d.Length}/{b1.Guid!.ToString()![0]}";
                Assert.That(b1.Index, Is.EqualTo(expIndex));

                var b1r = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b1r.Guid!.Value, Is.EqualTo(g1));
                Assert.That(b1r.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(false));
                Assert.That(b1r.Name, Is.EqualTo("Test1"));
                Assert.That(b1r.MimeType, Is.EqualTo("application/pdf"));
                Assert.That(b1r.Index, Is.EqualTo(expIndex));
                Assert.That(b1r.BlobSize, Is.EqualTo(b1d.Length));
                Assert.That(b1r.Data, Is.EqualTo(b1d).AsCollection);

                Assert.That(File.Exists(Path.Combine(FSPath1, expIndex, b1.Guid!.ToString()!)));
            });
    }
#endregion

#region Test Orphan
    [Test]
    public async Task TestFileOrphanAsync()
    {
        Guid g1 = Guid.Empty;

        static string idx(Blob b) => Path.Combine(b.Guid!.Value.ToString()[..1], b.Guid.Value.ToString().Substring(1, 1));

        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetFileFactory().Get(c, "TestUser");

                Assert.That(await bs.ReadBlobAsync(Guid.NewGuid()), Is.Null);

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                g1 = await bs.CreateNewBlobAsync(b1, idx, BlobFeatures.None);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));

                var b1r = await bs.ReadBlobAsync(g1) ?? throw new Exception();

                var b2d = File.ReadAllBytes(F2);
                b1r.Data = b2d;
                b1r.Name = "Test2";
                b1r.MimeType = "application/x-pdf";
                var b2g = await bs.RewriteBlobAsync(b1r, null, BlobFeatures.None);
                Assert.That(b2g, Is.Not.Null);
                b1r.Guid = b2g.Value;

                var b2r = await bs.ReadBlobAsync(b1r.Guid!.Value) ?? throw new Exception();
                Assert.That(b2r.Index, Is.EqualTo(b1.Index));
            });

        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetFileFactory().Get(c, "TestUser");

                var b3 = await bs.ReadBlobAsync(g1);
                Assert.That(b3, Is.Null);

                Assert.That(File.Exists(Path.Combine(FSPath1, idx(new Blob() { Guid = g1 }), g1.ToString().ToLowerInvariant())), Is.False);
            });
    }
#endregion

#region Test Rollback
    [Test]
    [TestCase(WriterType.DB, FSNum.None)]
    [TestCase(WriterType.File, FSNum.BasePath)]
#if RUN_NETWORK_TESTS
    [TestCase(WriterType.File, FSNum.NetworkPath)]
#endif
    public async Task RollbackCreateAsync(WriterType type, FSNum fs)
    {
    Blob? b1 = null;
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetFileOrDBFactory(type, fs, backupFs: FSNum.BackupPath).Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                var g1 = await bs.CreateNewBlobAsync(b1);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));

                if (type == WriterType.File)
                {
                    Assert.That(File.Exists(Path.Combine(fs == FSNum.BasePath ? FSPath1 : FSPath2, b1.Index!, b1.Guid.ToString()!.ToLower())));
                    Assert.That(File.Exists(Path.Combine(FSPath3, b1.Index!, b1.Guid.ToString()!.ToLower())));
                }

                c.SetRollback();
            });

        if (type == WriterType.File)
        {
            Assert.That(File.Exists(Path.Combine(fs == FSNum.BasePath ? FSPath1 : FSPath2, b1!.Index!, b1.Guid.ToString()!.ToLower())), Is.False);
            Assert.That(File.Exists(Path.Combine(FSPath3, b1.Index!, b1.Guid.ToString()!.ToLower())), Is.False);
        }

        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetFileOrDBFactory(type, fs, null, null, fs).Get(c, "TestUser");

                Assert.That(await bs.ReadBlobAsync(b1!.Guid!.Value), Is.Null);
            });
    }

    [Test]
    [TestCase(WriterType.DB, FSNum.None)]
    [TestCase(WriterType.File, FSNum.BasePath)]
#if RUN_NETWORK_TESTS
    [TestCase(WriterType.File, FSNum.NetworkPath)]
#endif
    public async Task RollbackRewriteAsync(WriterType type, FSNum fs)
    {
        Blob? b1 = null;
        Guid g1 = Guid.NewGuid();
        Guid? g2 = null;
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetFileOrDBFactory(type, fs, backupFs: FSNum.BackupPath).Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                g1 = await bs.CreateNewBlobAsync(b1);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));
            });

        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetFileOrDBFactory(type, fs, backupFs: FSNum.BackupPath).Get(c, "TestUser");

                g2 = await bs.RewriteBlobAsync(b1!);

                if (type == WriterType.File)
                {
                    Assert.That(File.Exists(Path.Combine(fs == FSNum.BasePath ? FSPath1 : FSPath2, b1!.Index!, g2.Value.ToString().ToLower())));
                    Assert.That(File.Exists(Path.Combine(FSPath3, b1.Index!, g2.Value.ToString().ToLower())));
                }

                c.SetRollback();
            });

        if (type == WriterType.File)
        {
            Assert.That(File.Exists(Path.Combine(fs == FSNum.BasePath ? FSPath1 : FSPath2, b1!.Index!, g1.ToString().ToLower())));
            Assert.That(File.Exists(Path.Combine(FSPath3, b1.Index!, g1.ToString().ToLower())));
            Assert.That(File.Exists(Path.Combine(fs == FSNum.BasePath ? FSPath1 : FSPath2, b1.Index!, g2!.Value.ToString().ToLower())), Is.False);
            Assert.That(File.Exists(Path.Combine(FSPath3, b1.Index!, g2.Value.ToString().ToLower())), Is.False);

            await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
                async (c) =>
                {
                    IStorage bs = GetFileOrDBFactory(type, fs, backupFs: FSNum.BackupPath).Get(c, "TestUser");

                    Assert.That(await bs.ReadBlobAsync(g2.Value), Is.Null);
                });
        }
    }

    [Test]
    [TestCase(WriterType.DB, FSNum.None)]
    [TestCase(WriterType.File, FSNum.BasePath)]
#if RUN_NETWORK_TESTS
    [TestCase(WriterType.File, FSNum.NetworkPath)]
#endif
    public async Task RollbackDeleteAsync(WriterType type, FSNum fs)
    {
        Blob? b1 = null;
        Guid g1 = Guid.NewGuid();
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetFileOrDBFactory(type, fs, backupFs: FSNum.BackupPath).Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                g1 = await bs.CreateNewBlobAsync(b1);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));

                if (type == WriterType.File)
                {
                    Assert.That(File.Exists(Path.Combine(fs == FSNum.BasePath ? FSPath1 : FSPath2, b1!.Index!, g1.ToString().ToLower())));
                    Assert.That(File.Exists(Path.Combine(FSPath3, b1.Index!, g1.ToString().ToLower())));
                }
            });

        if (type == WriterType.File)
        {
            Assert.That(File.Exists(Path.Combine(fs == FSNum.BasePath ? FSPath1 : FSPath2, b1!.Index!, g1.ToString().ToLower())));
            Assert.That(File.Exists(Path.Combine(FSPath3, b1.Index!, g1.ToString().ToLower())));
        }

        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetFileOrDBFactory(type, fs, backupFs: FSNum.BasePath).Get(c, "TestUser");

                await bs.DeleteBlobAsync(b1!.Guid!.Value);

                if (type == WriterType.File)
                {
                    Assert.That(File.Exists(Path.Combine(fs == FSNum.BasePath ? FSPath1 : FSPath2, b1.Index!, g1.ToString().ToLower())));
                    Assert.That(File.Exists(Path.Combine(FSPath3, b1.Index!, g1.ToString().ToLower())));
                }

                c.SetRollback();
            });

        if (type == WriterType.File)
        {
            Assert.That(File.Exists(Path.Combine(fs == FSNum.BasePath ? FSPath1 : FSPath2, b1!.Index!, g1.ToString().ToLower())));
            Assert.That(File.Exists(Path.Combine(FSPath3, b1.Index!, g1.ToString().ToLower())));

            await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
                async (c) =>
                {
                    IStorage bs = GetFileOrDBFactory(type, fs, backupFs: FSNum.BasePath).Get(c, "TestUser");

                    Assert.That(await bs.ReadBlobAsync(b1.Guid!.Value), Is.Not.Null);
                });
        }
    }
#endregion

#region Test Backup
    [Test]
    public async Task FileSystem_WithBackup()
    {
        Blob? b1 = null;
        Guid g1 = Guid.NewGuid();
        Guid? g2 = null;
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetFileFactory(FSNum.BasePath, backupFs: FSNum.BackupPath).Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                g1 = await bs.CreateNewBlobAsync(b1);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));

                g2 = await bs.RewriteBlobAsync(b1);

                Assert.That(File.Exists(Path.Combine(FSPath1, b1.Index!, b1.Guid.ToString()!.ToLower())));
                Assert.That(File.Exists(Path.Combine(FSPath3, b1.Index!, b1.Guid.ToString()!.ToLower())));
            });

        Assert.That(File.Exists(Path.Combine(FSPath1, b1!.Index!, g1.ToString().ToLower())), Is.False);
        Assert.That(File.Exists(Path.Combine(FSPath1, b1.Index!, g2!.Value.ToString().ToLower())));
        Assert.That(File.Exists(Path.Combine(FSPath3, b1.Index!, g1.ToString().ToLower())), Is.False);
        Assert.That(File.Exists(Path.Combine(FSPath3, b1.Index!, g2.Value.ToString().ToLower())));
    }

    [Test]
    public async Task DB_WithDoubleBackup()
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                var fac = GetDBFactory();
                fac.AddBackupFileSystemProvider(new StorageFS.FileSystemSettings
                {
                    BasePath = FSPath1,
                    DefaultIndexStrategy = StorageFS.DefaultIndexStrategy.Flat
                });
                fac.AddBackupFileSystemProvider(new StorageFS.FileSystemSettings
                {
                    BasePath = FSPath3,
                    DefaultIndexStrategy = StorageFS.DefaultIndexStrategy.Flat
                });
                IStorage bs = fac.Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                var g1 = await bs.CreateNewBlobAsync(b1);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));

                var b1r = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b1r.Guid!.Value, Is.EqualTo(g1));
                Assert.That(b1r.Name, Is.EqualTo("Test1"));
                Assert.That(b1r.MimeType, Is.EqualTo("application/pdf"));

                Assert.That(File.Exists(Path.Combine(FSPath1, b1.Guid.ToString()!.ToLower())));
                Assert.That(File.Exists(Path.Combine(FSPath3, b1.Guid.ToString()!.ToLower())));
            });
    }
#endregion

#region Test Compress
    [Test]
    [TestCase(false, WriterType.DB)]
    [TestCase(true, WriterType.DB)]
    [TestCase(false, WriterType.File)]
    [TestCase(true, WriterType.File)]
    public async Task Compress_Async(bool encrypt, WriterType type)
    {
        var features = (encrypt ? BlobFeatures.Encrypted : BlobFeatures.None) | BlobFeatures.Compressed;

        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetFileOrDBFactory(type, FSNum.BasePath, features).Get(c, "TestUser");

                Assert.That(await bs.ReadBlobAsync(Guid.NewGuid()), Is.Null);

                var b1d = File.ReadAllBytes(F1xml);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/xml",
                    Data = b1d
                };
                var g1 = await bs.CreateNewBlobAsync(b1, null, features);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));

                var b1r = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b1r.Guid!.Value, Is.EqualTo(g1));
                Assert.That(b1.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(encrypt));
                Assert.That(b1.Features.HasFlag(BlobFeatures.Compressed));
                Assert.That(b1r.Name, Is.EqualTo("Test1"));
                Assert.That(b1r.MimeType, Is.EqualTo("application/xml"));
                switch (type)
                {
                    case WriterType.DB: Assert.That(b1r.Index, Is.Null); break;
                    case WriterType.File: Assert.That(b1r.Index, Is.EqualTo(string.Empty)); break;
                }
                Assert.That(b1r.StoredSize, Is.LessThan(b1d.Length));
                Assert.That(b1r.Data, Is.EqualTo(b1d).AsCollection);

                switch (type)
                {
                    case WriterType.DB:
#if SQLSERVER
                        c.ExecuteRaw($"UPDATE {IntegrationTestSetup.Schema}.BLOB SET Data = CONVERT(VARBINARY(200), '0x9473FBCCBC01AF12345678abcd123456', 1) WHERE GUID = '{g1}'");
#elif PGSQL
                        c.ExecuteRaw($"UPDATE {IntegrationTestSetup.Schema}.blob SET data = decode('9473FBCCBC01AF12345678abcd123456', 'hex') WHERE guid = '{g1}'");
#endif
                        break;

                    case WriterType.File:
                        File.WriteAllBytes(Path.Combine(FSPath1, g1.ToString().ToLower()), Convert.FromHexString("9473FBCCBC01AF12345678abcd123456"));
                        break;
                }

                Assert.ThrowsAsync<CorruptedBlobException>(async () => await bs.ReadBlobAsync(g1));
            });
    }
#endregion

#region File to DB Fallback
    [Test]
    public async Task FileToDBFallbackAsync()
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage dbbs = GetDBFactory().Get(c, "TestUser");

                IStorage bs = GetFileFactory(FSNum.BasePath).Get(c, "TestUser");

                Assert.That(await bs.ReadBlobAsync(Guid.NewGuid()), Is.Null);

                static string idx(Blob b) => Path.Combine(b.Guid!.Value.ToString()[..1], b.Guid.Value.ToString().Substring(1, 1));

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };

                var g1 = await dbbs.CreateNewBlobAsync(b1, idx, BlobFeatures.None);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));

                var b1r = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b1r.Guid!.Value, Is.EqualTo(g1));
                Assert.That(b1r.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(false));
                Assert.That(b1r.Name, Is.EqualTo("Test1"));
                Assert.That(b1r.MimeType, Is.EqualTo("application/pdf"));
                Assert.That(b1r.Index, Is.Null);
                Assert.That(b1r.BlobSize, Is.EqualTo(b1d.Length));
                Assert.That(b1r.Data, Is.EqualTo(b1d).AsCollection);

                var g2 = await bs.CreateNewBlobAsync(b1, idx, BlobFeatures.None);

                var b2r = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b2r.Guid!.Value, Is.EqualTo(g1));
                Assert.That(b2r.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(false));
                Assert.That(b2r.Name, Is.EqualTo("Test1"));
                Assert.That(b2r.MimeType, Is.EqualTo("application/pdf"));
                Assert.That(b1r.Index, Is.Null);
                Assert.That(b2r.BlobSize, Is.EqualTo(b1d.Length));
                Assert.That(b2r.Data, Is.EqualTo(b1d).AsCollection);

                var b3r = await bs.ReadBlobAsync(g2) ?? throw new Exception();
                Assert.That(b3r.Guid!.Value, Is.EqualTo(g2));
                Assert.That(b3r.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(false));
                Assert.That(b3r.Name, Is.EqualTo("Test1"));
                Assert.That(b3r.MimeType, Is.EqualTo("application/pdf"));
                Assert.That(b3r.Index, Is.EqualTo(idx(b3r)));
                Assert.That(b3r.BlobSize, Is.EqualTo(b1d.Length));
                Assert.That(b3r.Data, Is.EqualTo(b1d).AsCollection);
            });
    }
#endregion

#region Delete Blob
    [Test]
    public async Task DeleteDBAsync()
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetDBFactory().Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };

                var g1 = await bs.CreateNewBlobAsync(b1, (b) => null, BlobFeatures.None);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));

                var b1r = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b1r.Data, Is.EqualTo(b1d).AsCollection);

                await bs.DeleteBlobAsync(g1);

                Assert.That(await bs.ReadBlobAsync(g1), Is.Null);
            });
    }

    [Test]
    public async Task DeleteFileAsync()
    {
        Blob? b1r = null;

        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetFileFactory(FSNum.BasePath, strategy: StorageFS.DefaultIndexStrategy.ByDateTime, backupFs: FSNum.BackupPath).Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };

                var g1 = await bs.CreateNewBlobAsync(b1, null, BlobFeatures.None);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));

                b1r = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b1r.Data, Is.EqualTo(b1d).AsCollection);

                await bs.DeleteBlobAsync(g1);

                Assert.That(await bs.ReadBlobAsync(g1), Is.Null);

                Assert.That(File.Exists(Path.Combine(FSPath1, b1r.Index!, b1r.Guid!.ToString()!.ToLower())));
                Assert.That(File.Exists(Path.Combine(FSPath3, b1r.Index!, b1r.Guid!.ToString()!.ToLower())));
            });

        Assert.That(File.Exists(Path.Combine(FSPath1, b1r!.Index!, b1r.Guid!.ToString()!.ToLower())), Is.False);
        Assert.That(File.Exists(Path.Combine(FSPath3, b1r.Index!, b1r.Guid!.ToString()!.ToLower())), Is.False);
    }
#endregion

#region Test Multi Read
    [Test]
    public async Task MultiRead()
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                var fsFac = GetFileFactory();
                fsFac.AddDatabaseProvider();
                IStorage fs = fsFac.Get(c, "TestUser");

                var dbFac = GetFileFactory();
                dbFac.AddDatabaseProvider();
                dbFac.SetDefaultWriteProviderAsDatabase();
                IStorage db = dbFac.Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                var g1 = await db.CreateNewBlobAsync(b1);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));

                var g2 = await fs.CreateNewBlobAsync(b1, b => b.Guid!.Value.ToString("N"));
                Assert.That(b1.Guid.Value, Is.EqualTo(g2));

                {
                    var b1i = await db.ReadBlobIndexAsync(g1) ?? throw new Exception();
                    Assert.That(b1i.Guid!.Value, Is.EqualTo(g1));
                    Assert.That(b1i.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(true));
                    Assert.That(b1i.Features.HasFlag(BlobFeatures.Compressed), Is.False);
                    Assert.That(b1i.Name, Is.EqualTo("Test1"));
                    Assert.That(b1i.MimeType, Is.EqualTo("application/pdf"));

                    var b1r = await db.ReadBlobAsync(g1) ?? throw new Exception();
                    Assert.That(b1r.Guid!.Value, Is.EqualTo(g1));
                    Assert.That(b1r.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(true));
                    Assert.That(b1r.Features.HasFlag(BlobFeatures.Compressed), Is.False);
                    Assert.That(b1r.Name, Is.EqualTo("Test1"));
                    Assert.That(b1r.MimeType, Is.EqualTo("application/pdf"));
                    Assert.That(b1r.Index, Is.Null);

                    var b2i = await db.ReadBlobIndexAsync(g2) ?? throw new Exception();
                    Assert.That(b2i.Guid!.Value, Is.EqualTo(g2));
                    Assert.That(b2i.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(true));
                    Assert.That(b2i.Features.HasFlag(BlobFeatures.Compressed), Is.False);
                    Assert.That(b2i.Name, Is.EqualTo("Test1"));
                    Assert.That(b2i.MimeType, Is.EqualTo("application/pdf"));

                    var b2r = await db.ReadBlobAsync(g2) ?? throw new Exception();
                    Assert.That(b2r.Guid!.Value, Is.EqualTo(g2));
                    Assert.That(b2r.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(true));
                    Assert.That(b2r.Features.HasFlag(BlobFeatures.Compressed), Is.False);
                    Assert.That(b2r.Name, Is.EqualTo("Test1"));
                    Assert.That(b2r.MimeType, Is.EqualTo("application/pdf"));
                    Assert.That(b2r.Index, Is.EqualTo(g2.ToString("N")));
                    Assert.That(b2r.BlobSize, Is.EqualTo(b1d.Length));
                    Assert.That(b2r.Data, Is.EqualTo(b1d).AsCollection);
                }

                {
                    var b1i = await fs.ReadBlobIndexAsync(g1) ?? throw new Exception();
                    Assert.That(b1i.Guid!.Value, Is.EqualTo(g1));
                    Assert.That(b1i.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(true));
                    Assert.That(b1i.Features.HasFlag(BlobFeatures.Compressed), Is.False);
                    Assert.That(b1i.Name, Is.EqualTo("Test1"));
                    Assert.That(b1i.MimeType, Is.EqualTo("application/pdf"));

                    var b1r = await fs.ReadBlobAsync(g1) ?? throw new Exception();
                    Assert.That(b1r.Guid!.Value, Is.EqualTo(g1));
                    Assert.That(b1r.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(true));
                    Assert.That(b1r.Features.HasFlag(BlobFeatures.Compressed), Is.False);
                    Assert.That(b1r.Name, Is.EqualTo("Test1"));
                    Assert.That(b1r.MimeType, Is.EqualTo("application/pdf"));
                    Assert.That(b1r.Index, Is.Null);

                    var b2i = await fs.ReadBlobIndexAsync(g2) ?? throw new Exception();
                    Assert.That(b2i.Guid!.Value, Is.EqualTo(g2));
                    Assert.That(b2i.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(true));
                    Assert.That(b2i.Features.HasFlag(BlobFeatures.Compressed), Is.False);
                    Assert.That(b2i.Name, Is.EqualTo("Test1"));
                    Assert.That(b2i.MimeType, Is.EqualTo("application/pdf"));

                    var b2r = await fs.ReadBlobAsync(g2) ?? throw new Exception();
                    Assert.That(b2r.Guid!.Value, Is.EqualTo(g2));
                    Assert.That(b2r.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(true));
                    Assert.That(b2r.Features.HasFlag(BlobFeatures.Compressed), Is.False);
                    Assert.That(b2r.Name, Is.EqualTo("Test1"));
                    Assert.That(b2r.MimeType, Is.EqualTo("application/pdf"));
                    Assert.That(b2r.Index, Is.EqualTo(g2.ToString("N")));
                    Assert.That(b2r.BlobSize, Is.EqualTo(b1d.Length));
                    Assert.That(b2r.Data, Is.EqualTo(b1d).AsCollection);
                }
            });
    }

    [Test]
    public async Task RewriteDB2FS()
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage db = GetDBFactory().Get(c, "TestUser");
                IStorage fs = GetFileFactory().Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                var g1 = await db.CreateNewBlobAsync(b1);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));

                var g2 = await fs.RewriteBlobAsync(b1);
                Assert.That(g2.Value, Is.Not.EqualTo(g1));

                Assert.That(await db.ReadBlobAsync(g1), Is.Null);

                var b = await fs.ReadBlobAsync(g2.Value) ?? throw new Exception();
                Assert.That(b.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(true));
                Assert.That(b.Features.HasFlag(BlobFeatures.Compressed), Is.False);
                Assert.That(b.Name, Is.EqualTo("Test1"));
                Assert.That(b.MimeType, Is.EqualTo("application/pdf"));
            });
    }

    [Test]
    public async Task RewriteFS2DB()
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                var fsFac = GetFileFactory();
                fsFac.AddDatabaseProvider();
                IStorage fs = fsFac.Get(c, "TestUser");

                var dbFac = GetFileFactory();
                dbFac.AddDatabaseProvider();
                dbFac.SetDefaultWriteProviderAsDatabase();
                IStorage db = dbFac.Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                var g1 = await fs.CreateNewBlobAsync(b1);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));

                var g2 = await db.RewriteBlobAsync(b1);
                Assert.That(g2, Is.Null);

                var b = await db.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(true));
                Assert.That(b.Features.HasFlag(BlobFeatures.Compressed), Is.False);
                Assert.That(b.Name, Is.EqualTo("Test1"));
                Assert.That(b.MimeType, Is.EqualTo("application/pdf"));
            });
    }

    [Test]
    [TestCase(WriterType.DB)]
    [TestCase(WriterType.File)]
    public async Task RewriteNoOrig(WriterType type)
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetFileOrDBFactory(type).Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                Guid g = Guid.NewGuid();
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d,
                    Guid = g
                };

                var g1 = await bs.RewriteBlobAsync(b1);
                Assert.That(g1.Value, Is.Not.EqualTo(g));

                var b = await bs.ReadBlobAsync(g1.Value) ?? throw new Exception();
                Assert.That(b.Features.HasFlag(BlobFeatures.Encrypted), Is.EqualTo(true));
                Assert.That(b.Features.HasFlag(BlobFeatures.Compressed), Is.False);
                Assert.That(b.Name, Is.EqualTo("Test1"));
                Assert.That(b.MimeType, Is.EqualTo("application/pdf"));
            });
    }
#endregion

#region Test Stream
    [Test]
    [TestCase(WriterType.DB, FSNum.None)]
    [TestCase(WriterType.File, FSNum.BasePath)]
    //[TestCase(WriterType.File, FSNum.NetworkPath)]
    public async Task TestAsync_Stream(WriterType type, FSNum fs)
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetFileOrDBFactory(type, fs).Get(c, "TestUser");

                Assert.That(await bs.ReadBlobAsync(Guid.NewGuid()), Is.Null);

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf"
                };
                var g1 = await bs.CreateNewBlobAsync(b1, File.OpenRead(F1), -1, null, BlobFeatures.None);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));

                (Blob? blob, Stream? stream) b1r = default;
#if PGSQL
                if (type != WriterType.DB)
                {
#endif
                    b1r = await bs.ReadBlobStreamAsync(g1);
                    b1r.blob!.Data = await b1r.stream.ToBytesAsync();
                    Assert.That(b1d, Is.EqualTo(b1r.blob.Data));
                    Assert.That(b1r.blob.Data, Is.EqualTo(b1d).AsCollection);
#if PGSQL
                }
                else
                {
                    b1r.blob = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                    b1r.stream = new MemoryStream(b1r.blob.Data!);
                }
#endif

                var bi = await bs.ReadBlobIndexAsync(g1) ?? throw new Exception();
                Assert.That(bi.BlobSize, Is.EqualTo(bi.StoredSize));

                long realSize = 0;
                switch (type)
                {
                    case WriterType.DB:
#if SQLSERVER
                        realSize = c.ExecuteRawQueryOne($"SELECT DATALENGTH(Data) AS DL FROM {IntegrationTestSetup.Schema}.Blob WHERE GUID = @GUID", r => r.GetLong("DL"), new SqlParameter("@GUID", g1)).Value;
#elif PGSQL
                        realSize = c.ExecuteRawQueryOne($"SELECT octet_length(data) AS dl FROM {IntegrationTestSetup.Schema}.blob WHERE guid = @i_guid", r => r.GetLong("dl"), new NpgsqlParameter("@i_guid", g1)).Value;
#endif
                        break;

                    case WriterType.File:
                        realSize = new FileInfo(Path.Combine(fs == FSNum.BasePath ? FSPath1 : FSPath2, g1.ToString().ToLower())).Length;
                        break;
                }
                Assert.That(realSize, Is.EqualTo(bi.StoredSize));

                var b2d = File.ReadAllBytes(F2);
                b1r.blob.Name = "Test2";
                b1r.blob.MimeType = "application/x-pdf";
                var b2g = await bs.RewriteBlobAsync(b1r.blob, File.OpenRead(F2), -1, null, BlobFeatures.None);
                switch (type)
                {
                    case WriterType.DB: Assert.That(b2g, Is.Null); break;
                    case WriterType.File:
                        Assert.That(b2g, Is.Not.Null);
                        g1 = b2g.Value;
                        b1r.blob.Guid = b2g.Value;
                        break;
                }

                var b2r = await bs.ReadBlobAsync(b1r.blob.Guid!.Value) ?? throw new Exception();

                Assert.That(b2r.Data, Is.EqualTo(b2d).AsCollection);

#if !PGSQL
                // pgsql cannot read blobs as a stream
                var b3 = await bs.ReadBlobStreamAsync(g1);
                Assert.That(b3.blob, Is.Not.Null);
                b3.stream!.Dispose();
#endif

                switch (type)
                {
                    case WriterType.DB:
#if SQLSERVER
                        c.ExecuteRaw($"UPDATE {IntegrationTestSetup.Schema}.BLOB SET Data = CONVERT(VARBINARY(200), '0x9473FBCCBC01AF12345678abcd123456', 1) WHERE GUID = '{g1}'");
#elif PGSQL
                        c.ExecuteRaw($"UPDATE {IntegrationTestSetup.Schema}.blob SET data = decode('9473FBCCBC01AF12345678abcd123456', 'hex') WHERE guid = '{g1}'");
#endif
                        break;

                    case WriterType.File:
                        File.WriteAllBytes(Path.Combine(fs == FSNum.BasePath ? FSPath1 : FSPath2, g1.ToString().ToLower()), Convert.FromHexString("9473FBCCBC01AF12345678abcd123456"));
                        break;
                }

#if PGSQL
                Assert.ThrowsAsync<CorruptedBlobException>(async () => await bs.ReadBlobAsync(g1));
#else
                b3 = await bs.ReadBlobStreamAsync(g1);
                Assert.That(b3.blob, Is.Not.Null);
                Assert.Throws<CorruptedBlobException>(() => b3.stream.ToBytes());
#endif
            });
    }

    [Test]
    public async Task TestAsync_Stream_FileBackup()
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = GetFileFactory(FSNum.BasePath, BlobFeatures.None, FSNum.BackupPath).Get(c, "TestUser");

                Assert.That((await bs.ReadBlobStreamAsync(Guid.NewGuid())).blob, Is.Null);

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                var g1 = await bs.CreateNewBlobAsync(b1, File.OpenRead(F1), -1, null, BlobFeatures.None);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));

                var bi = await bs.ReadBlobIndexAsync(g1) ?? throw new Exception();
                Assert.That(bi.BlobSize, Is.EqualTo(bi.StoredSize));

                long realSize = new FileInfo(Path.Combine(FSPath1, g1.ToString().ToLower())).Length;
                Assert.That(realSize, Is.EqualTo(bi.StoredSize));

                realSize = new FileInfo(Path.Combine(FSPath3, g1.ToString().ToLower())).Length;
                Assert.That(realSize, Is.EqualTo(bi.StoredSize));
                var b1db = File.ReadAllBytes(Path.Combine(FSPath3, g1.ToString().ToLower()));

                var b1r = await bs.ReadBlobStreamAsync(g1);
                b1r.blob!.Data = await b1r.stream.ToBytesAsync();

                Assert.That(b1r.blob.Data, Is.EqualTo(b1d).AsCollection);
                Assert.That(b1r.blob.Data, Is.EqualTo(b1db).AsCollection);

                var b2d = File.ReadAllBytes(F2);
                b1r.blob.Name = "Test2";
                b1r.blob.MimeType = "application/x-pdf";
                var b2g = await bs.RewriteBlobAsync(b1r.blob, File.OpenRead(F2), -1, null, BlobFeatures.None);
                Assert.That(b2g, Is.Not.Null);
                g1 = b2g.Value;
                b1r.blob.Guid = b2g.Value;

                var b2r = await bs.ReadBlobAsync(b1r.blob.Guid.Value) ?? throw new Exception();

                var b2db = File.ReadAllBytes(Path.Combine(FSPath3, g1.ToString().ToLower()));
                Assert.That(b2r.Data, Is.EqualTo(b2d).AsCollection);
                Assert.That(b2r.Data, Is.EqualTo(b2db).AsCollection);
            });
    }
#endregion

#region S3

    private async Task<(Amazon.S3.AmazonS3Client rawClient, Amazon.S3.AmazonS3Client? backupRawClient)> ClearS3Buckets(StorageAWS.S3Settings settings, StorageAWS.S3Settings? backupSettings)
    {
        // clear out existing bucket
        var config = new Amazon.S3.AmazonS3Config { ServiceURL = settings.S3Url };
        var rawClient = new Amazon.S3.AmazonS3Client(S3_AccessID, S3_AccessKey, config);
        var deleteList = await rawClient.ListObjectsV2Async(new Amazon.S3.Model.ListObjectsV2Request { BucketName = settings.BucketName });
        if (deleteList.KeyCount > 0)
            await rawClient.DeleteObjectsAsync(new Amazon.S3.Model.DeleteObjectsRequest { BucketName = settings.BucketName, Objects = deleteList.S3Objects.Select(o => new Amazon.S3.Model.KeyVersion { Key = o.Key }).ToList() });

        Amazon.S3.AmazonS3Client? backupRawClient = null;
        if (backupSettings != null)
        {
            var backupConfig = new Amazon.S3.AmazonS3Config { ServiceURL = backupSettings.S3Url };
            backupRawClient = new Amazon.S3.AmazonS3Client(S3_AccessID, S3_AccessKey, backupConfig);
            deleteList = await backupRawClient.ListObjectsV2Async(new Amazon.S3.Model.ListObjectsV2Request { BucketName = backupSettings.BucketName });
            if (deleteList.KeyCount > 0)
                await rawClient.DeleteObjectsAsync(new Amazon.S3.Model.DeleteObjectsRequest { BucketName = backupSettings.BucketName, Objects = deleteList.S3Objects.Select(o => new Amazon.S3.Model.KeyVersion { Key = o.Key }).ToList() });
        }
        // clear out existing bucket

        return (rawClient, backupRawClient);
    }

#if TEST_AWS
    [Test]
    [TestCase(true)]
    [TestCase(false)]
#endif
    public async Task S3_Stream(bool sysAuth)
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                // for IAM/EC2/beanstalk auth
                //C:\users\awsuser\.aws\credentials
                //[default]
                //aws_access_key_id = { accessKey}
                //aws_secret_access_key = { secretKey}

                var fac = GetStorageFactory(BlobFeatures.None);
                fac.AddS3Provider(new StorageAWS.S3Settings()
                {
                    S3Url = "https://s3.us-west-1.amazonaws.com",
                    BucketName = "plinth-s3-test",
                    UseSystemAuth = sysAuth,
                    ClientId = sysAuth ? null : S3_AccessID,
                    ClientSecret = sysAuth ? null : S3_AccessKey,
                    DefaultIndexStrategy = StorageAWS.DefaultIndexStrategy.ByDateTime
                });
                fac.SetDefaultWriteProviderAsS3();
                IStorage bs = fac.Get(c, "TestUser");

                var file = F1;
                var b1d = File.ReadAllBytes(file);
                Guid g = Guid.NewGuid();
                var b1 = new Blob()
                {
                    Name = Path.GetFileNameWithoutExtension(file),
                    MimeType = "video/mp4", //"application/pdf",
                    Guid = g
                };

                var g1 = await bs.CreateNewBlobAsync(b1, File.OpenRead(file), b1d.LongLength);

                var b2 = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b2, Is.Not.Null);
                Assert.That(b2.Index!, Does.Not.Contain('\\'));

                var b1r = await bs.ReadBlobStreamAsync(g1);
                b1r.blob!.Data = b1r.stream.ToBytes();

                Assert.That(b1r.blob.Data, Is.EqualTo(b1d).AsCollection);

                await bs.DeleteBlobAsync(g1);
            });
    }

#if TEST_AWS
    [Test]
#endif
    public async Task S3()
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                var fac = GetStorageFactory();
                fac.AddS3Provider(new StorageAWS.S3Settings()
                {
                    S3Url = "https://s3.us-west-1.amazonaws.com",
                    BucketName = "plinth-s3-test",
                    ClientId = S3_AccessID,
                    ClientSecret = S3_AccessKey,
                    DefaultIndexStrategy = StorageAWS.DefaultIndexStrategy.Flat
                });
                fac.SetDefaultWriteProviderAsS3();
                IStorage bs = fac.Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                Guid g = Guid.NewGuid();
                var b1 = new Blob()
                {
                    Name = "Test1.pdf",
                    MimeType = "application/pdf",
                    Data = b1d,
                    Guid = g
                };

                var g1 = await bs.CreateNewBlobAsync(b1);

                var b2 = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b2, Is.Not.Null);

                Assert.That(b2.Data, Is.EqualTo(b1d).AsCollection);

                await bs.DeleteBlobAsync(g1);
            });
    }

#if TEST_AWS
    [Test]
    [TestCase(null)]
    [TestCase("backup-prefix/")]
#endif
    public async Task S3_Backup_And_Prefixes(string backupPrefix)
    {
        var fac = GetStorageFactory();
        var settings = new StorageAWS.S3Settings()
        {
            S3Url = "https://s3.us-west-1.amazonaws.com",
            BucketName = "plinth-s3-test",
            BlobPrefix = "my-prefix/",
            ClientId = S3_AccessID,
            ClientSecret = S3_AccessKey,
            DefaultIndexStrategy = StorageAWS.DefaultIndexStrategy.Flat
        };
        var backupSettings = Mapper.Map<StorageAWS.S3Settings>(settings);
        backupSettings.BucketName = "plinth-s3-test-backup";
        backupSettings.BlobPrefix = backupPrefix;

        fac.AddS3Provider(settings);
        fac.AddBackupS3Provider(backupSettings);
        fac.SetDefaultWriteProviderAsS3();

        // clear out existing bucket
        (var rawClient, var backupRawClient) = await ClearS3Buckets(settings, backupSettings);

        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = fac.Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                Guid g = Guid.NewGuid();
                var b1 = new Blob()
                {
                    Name = "Test1.pdf",
                    MimeType = "application/pdf",
                    Data = b1d,
                    Guid = g
                };

                var g1 = await bs.CreateNewBlobAsync(b1);

                var b2 = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b2, Is.Not.Null);

                Assert.That(b2.Data, Is.EqualTo(b1d).AsCollection);

                var list = await rawClient.ListObjectsV2Async(new Amazon.S3.Model.ListObjectsV2Request { BucketName = settings.BucketName });
                Assert.That(list.S3Objects.First().Key, Is.EqualTo($"my-prefix/{g1}.pdf"));

                list = await backupRawClient!.ListObjectsV2Async(new Amazon.S3.Model.ListObjectsV2Request { BucketName = backupSettings.BucketName });
                Assert.That(list.S3Objects.First().Key, Is.EqualTo($"{backupPrefix}{g1}.pdf"));

                await bs.DeleteBlobAsync(g1);
            });

            var list = await rawClient.ListObjectsV2Async(new Amazon.S3.Model.ListObjectsV2Request { BucketName = settings.BucketName });
        Assert.That(list.KeyCount, Is.EqualTo(0));

            list = await backupRawClient!.ListObjectsV2Async(new Amazon.S3.Model.ListObjectsV2Request { BucketName = backupSettings.BucketName });
        Assert.That(list.KeyCount, Is.EqualTo(0));
    }

#if TEST_AWS
    [Test]
#endif
    public async Task S3_CustomIndex()
    {
        var fac = GetStorageFactory();
        bool called = false, called2 = false;
        var settings = new StorageAWS.S3Settings()
        {
            S3Url = "https://s3.us-west-1.amazonaws.com",
            BucketName = "plinth-s3-test",
            ClientId = S3_AccessID,
            ClientSecret = S3_AccessKey,
            CustomIndexStrategy = b => { called = true; return $"hello/{b.Guid!.ToString()![0]}"; }
        };
        var backupSettings = Mapper.Map<StorageAWS.S3Settings>(settings);
        backupSettings.BucketName = "plinth-s3-test-backup";
        backupSettings.CustomIndexStrategy = b => { called2 = true; return $"there/{b.Guid!.ToString()![0]}"; };

        fac.AddS3Provider(settings);
        fac.AddBackupS3Provider(backupSettings);
        fac.SetDefaultWriteProviderAsS3();

        // clear out existing bucket
        (var rawClient, var backupRawClient) = await ClearS3Buckets(settings, backupSettings);

        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = fac.Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                Guid g = Guid.NewGuid();
                var b1 = new Blob()
                {
                    Name = "Test1.pdf",
                    MimeType = "application/pdf",
                    Data = b1d,
                    Guid = g
                };

                var g1 = await bs.CreateNewBlobAsync(b1);
                Assert.That(called);
                Assert.That(called2, Is.False);

                var b2 = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b2, Is.Not.Null);

                Assert.That(b2.Data, Is.EqualTo(b1d).AsCollection);

                var list = await rawClient.ListObjectsV2Async(new Amazon.S3.Model.ListObjectsV2Request { BucketName = settings.BucketName });
                Assert.That(list.S3Objects.First().Key, Is.EqualTo($"hello/{g1.ToString()![0]}/{g1}.pdf"));

                list = await backupRawClient!.ListObjectsV2Async(new Amazon.S3.Model.ListObjectsV2Request { BucketName = backupSettings.BucketName });
                Assert.That(list.S3Objects.First().Key, Is.EqualTo($"hello/{g1.ToString()![0]}/{g1}.pdf"));

                await bs.DeleteBlobAsync(g1);
            });

            var list = await rawClient.ListObjectsV2Async(new Amazon.S3.Model.ListObjectsV2Request { BucketName = settings.BucketName });
        Assert.That(list.KeyCount, Is.EqualTo(0));

            list = await backupRawClient!.ListObjectsV2Async(new Amazon.S3.Model.ListObjectsV2Request { BucketName = backupSettings.BucketName });
        Assert.That(list.KeyCount, Is.EqualTo(0));
    }

#if TEST_AWS
    [Test]
    [TestCase(0)]
    [TestCase(1)]
    [TestCase(2)]
#endif
    public async Task S3_DisableExt(int testcase)
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                bool? disable = testcase switch
                {
                    0 => true,
                    1 => false,
                    2 => null,
                    _ => throw new NotImplementedException()
                };

                var fac = GetStorageFactory();
                var settings = new StorageAWS.S3Settings()
                {
                    S3Url = "https://s3.us-west-1.amazonaws.com",
                    BucketName = "plinth-s3-test",
                    ClientId = S3_AccessID,
                    ClientSecret = S3_AccessKey,
                    DisableBlobExtensions = disable,
                    DefaultIndexStrategy = StorageAWS.DefaultIndexStrategy.Flat
                };
                fac.AddS3Provider(settings);
                fac.SetDefaultWriteProviderAsS3();
                IStorage bs = fac.Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                Guid g = Guid.NewGuid();
                var b1 = new Blob()
                {
                    Name = "Test1.pdf",
                    MimeType = "application/pdf",
                    Data = b1d,
                    Guid = g
                };

                var g1 = await bs.CreateNewBlobAsync(b1);

                var b2 = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b2, Is.Not.Null);

                Assert.That(b2.Data, Is.EqualTo(b1d).AsCollection);

                var config = new Amazon.S3.AmazonS3Config
                {
                    ServiceURL = settings.S3Url
                };
                var rawClient = new Amazon.S3.AmazonS3Client(S3_AccessID, S3_AccessKey, config);
                var list = await rawClient.ListObjectsV2Async(new Amazon.S3.Model.ListObjectsV2Request { BucketName = settings.BucketName });

                switch (testcase)
                {
                    case 0:
                        Assert.That(list.S3Objects[0].Key, Is.EqualTo(g1.ToString())); break;
                    case 1:
                    case 2:
                        Assert.That(list.S3Objects[0].Key, Is.EqualTo(g1.ToString() + ".pdf")); break;
                }

                await bs.DeleteBlobAsync(g1);
            });
    }

#if TEST_AWS
    [Test]
#endif
    public async Task S3_UrlRefs()
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                var fac = GetStorageFactory(BlobFeatures.None);
                fac.AddS3Provider(new StorageAWS.S3Settings()
                {
                    S3Url = "https://s3.us-west-1.amazonaws.com",
                    BucketName = "plinth-s3-test",
                    ClientId = S3_AccessID,
                    ClientSecret = S3_AccessKey,
                    AllowPublicRead = true,
                    DefaultIndexStrategy = StorageAWS.DefaultIndexStrategy.Flat
                });
                fac.SetDefaultWriteProviderAsS3();
                IStorage bs = fac.Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                Guid g = Guid.NewGuid();
                var b1 = new Blob()
                {
                    Name = "Test1.pdf",
                    MimeType = "application/pdf",
                    Data = b1d,
                    Guid = g
                };

                var g1 = await bs.CreateNewBlobAsync(b1);

                var b2 = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b2, Is.Not.Null);

                Assert.That(b2.Data, Is.EqualTo(b1d).AsCollection);

                var url1 = await bs.GetUrlReferenceAsync(b2.Guid!.Value);
                Assert.That(url1, Is.Not.Null);
                using (var client = new HttpClient())
                {
                    var d = await client.GetByteArrayAsync(url1);
                    Assert.That(b1d, Is.EqualTo(d).AsCollection);
                }
                //Console.WriteLine(url1);
                var url2 = await bs.GetUrlReferenceAsync(b2.Guid.Value);
                Assert.That(url2, Is.Not.Null);
                using (var client = new HttpClient())
                {
                    var d = await client.GetByteArrayAsync(url2);
                    Assert.That(b1d, Is.EqualTo(d).AsCollection);
                }
                //Console.WriteLine(url2);

                var url3 = await bs.GetTemporaryUrlReferenceAsync(b2.Guid.Value, TimeSpan.FromSeconds(180));
                Assert.That(url3, Is.Not.Null);
                //Console.WriteLine(url3);
                using (var client = new HttpClient())
                {
                    var d = await client.GetByteArrayAsync(url3);
                    Assert.That(b1d, Is.EqualTo(d).AsCollection);
                }
                var url4 = await bs.GetTemporaryUrlReferenceAsync(b2.Guid.Value, TimeSpan.FromSeconds(5));
                Assert.That(url4, Is.Not.Null);
                //Console.WriteLine(url4);
                using (var client = new HttpClient())
                {
                    var d = await client.GetByteArrayAsync(url4);
                    Assert.That(b1d, Is.EqualTo(d).AsCollection);
                }

                await Task.Delay(TimeSpan.FromSeconds(6));
                using (var client = new HttpClient())
                {
                    try
                    {
                        var d = await client.GetByteArrayAsync(url3);
                    }
                    catch (System.Net.WebException e)
                    {
                        Assert.That((e.Response as System.Net.HttpWebResponse)?.StatusCode, Is.EqualTo(System.Net.HttpStatusCode.Forbidden));
                    }
                }

                // comment to test URLs
                await bs.DeleteBlobAsync(g1);
            });
    }
#endregion

#region Azure Blob

    private async Task<(Azure.Storage.Blobs.BlobContainerClient container, Azure.Storage.Blobs.BlobContainerClient? backupContainer)>
        ClearAzureContainers(StorageAzure.AzureBlobSettings settings, StorageAzure.AzureBlobSettings? backupSettings)
    {
        // clear out existing bucket
        var container = new Azure.Storage.Blobs.BlobContainerClient(settings.ConnectionString, settings.ContainerName);
        var deleteList = container.GetBlobsAsync();
        await foreach (var b in deleteList)
            await container.DeleteBlobAsync(b.Name);

        Azure.Storage.Blobs.BlobContainerClient? backupContainer = null;
        if (backupSettings != null)
        {
            backupContainer = new Azure.Storage.Blobs.BlobContainerClient(backupSettings.ConnectionString, backupSettings.ContainerName);
            deleteList = backupContainer.GetBlobsAsync();
            await foreach (var b in deleteList)
                await container.DeleteBlobAsync(b.Name);
        }
        // clear out existing bucket

        return (container, backupContainer);
    }

#if TEST_AZURE
    [TestCase(null, false, false)]
    [TestCase(null, false, true)]
    [TestCase(null, true, false)]
    [TestCase(null, true, true)]
    [TestCase("testbackup", false, false)]
    [TestCase("testbackup", false, true)]
    [TestCase("testbackup", true, false)]
    [TestCase("testbackup", true, true)]
#endif
    public async Task Azure_BlobAsync(string backup, bool encrypt, bool compress)
    {
        var fac = GetStorageFactory();
        var settings = new StorageAzure.AzureBlobSettings()
        {
            ConnectionString = $"DefaultEndpointsProtocol=https;AccountName=plinthtest;AccountKey={AzureBlobAccountKey};EndpointSuffix=core.windows.net",
            ContainerName = "test",
            DefaultIndexStrategy = StorageAzure.DefaultIndexStrategy.Flat,
        };
        fac.AddAzureBlobProvider(settings);

        var backupSettings = backup == null ? null : new StorageAzure.AzureBlobSettings()
        {
            ConnectionString = $"DefaultEndpointsProtocol=https;AccountName=plinthtest;AccountKey={AzureBlobAccountKey};EndpointSuffix=core.windows.net",
            ContainerName = backup,
            DefaultIndexStrategy = StorageAzure.DefaultIndexStrategy.Flat,
        };
        if (backup != null)
            fac.AddBackupAzureBlobProvider(backupSettings!);
        fac.SetDefaultWriteProviderAsAzureBlob();

        (var container, var backupContainer) = await ClearAzureContainers(settings, backupSettings);

        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = fac.Get(c, "TestUser");

                var b1d = await File.ReadAllBytesAsync(F1);
                Guid g = Guid.NewGuid();
                var b1 = new Blob()
                {
                    Name = "Test1.pdf",
                    MimeType = "application/pdf",
                    Data = b1d,
                    Guid = g
                };

                var feat = (encrypt ? BlobFeatures.Encrypted : BlobFeatures.None) |
                    (compress ? BlobFeatures.Compressed : BlobFeatures.None);
                var g1 = await bs.CreateNewBlobAsync(b1, features: feat);

                var b2 = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b2, Is.Not.Null);

                var list = container.GetBlobsAsync();
                Assert.That((await list.FirstAsync()).Name, Is.EqualTo($"{b2.Guid}.pdf"));

                if (backup != null)
                {
                    list = backupContainer!.GetBlobsAsync();
                    Assert.That((await list.FirstAsync()).Name, Is.EqualTo($"{b2.Guid}.pdf"));
                }

                await bs.DeleteBlobAsync(b2.Guid!.Value);
            });

        var list = container.GetBlobsAsync();
        Assert.That(await list.CountAsync(), Is.EqualTo(0));

        if (backup != null)
        {
            list = backupContainer!.GetBlobsAsync();
            Assert.That(await list.CountAsync(), Is.EqualTo(0));
        }
    }

#if TEST_AZURE
    [Test]
#endif
    public async Task Azure_CustomIndex()
    {
        bool called = false, called2 = false;
        var fac = GetStorageFactory();
        var settings = new StorageAzure.AzureBlobSettings()
        {
            ConnectionString = $"DefaultEndpointsProtocol=https;AccountName=plinthtest;AccountKey={AzureBlobAccountKey};EndpointSuffix=core.windows.net",
            ContainerName = "test",
            CustomIndexStrategy = b => { called = true; return $"hello/{b.Guid!.ToString()![0]}"; }
        };
        fac.AddAzureBlobProvider(settings);

        var backupSettings = new StorageAzure.AzureBlobSettings()
        {
            ConnectionString = $"DefaultEndpointsProtocol=https;AccountName=plinthtest;AccountKey={AzureBlobAccountKey};EndpointSuffix=core.windows.net",
            ContainerName = "testbackup",
            CustomIndexStrategy = b => { called2 = true; return $"there/{b.Guid!.ToString()![0]}"; }
        };
        fac.AddBackupAzureBlobProvider(backupSettings);
        fac.SetDefaultWriteProviderAsAzureBlob();

        // clear out existing bucket
        (var container, var backupContainer) = await ClearAzureContainers(settings, backupSettings);

        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = fac.Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                Guid g = Guid.NewGuid();
                var b1 = new Blob()
                {
                    Name = "Test1.pdf",
                    MimeType = "application/pdf",
                    Data = b1d,
                    Guid = g
                };

                var g1 = await bs.CreateNewBlobAsync(b1);
                Assert.That(called);
                Assert.That(called2, Is.False);

                var b2 = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b2, Is.Not.Null);

                Assert.That(b2.Data, Is.EqualTo(b1d).AsCollection);

                var list = container.GetBlobsAsync();
                Assert.That((await list.FirstAsync()).Name, Is.EqualTo($"hello/{g1.ToString()![0]}/{g1}.pdf"));

                list = backupContainer!.GetBlobsAsync();
                Assert.That((await list.FirstAsync()).Name, Is.EqualTo($"hello/{g1.ToString()![0]}/{g1}.pdf"));

                await bs.DeleteBlobAsync(g1);
            });

        var list = container.GetBlobsAsync();
        Assert.That(await list.CountAsync(), Is.EqualTo(0));

        list = backupContainer!.GetBlobsAsync();
        Assert.That(await list.CountAsync(), Is.EqualTo(0));
    }

#if TEST_AZURE
    [Test]
    [TestCase(0)]
    [TestCase(1)]
    [TestCase(2)]
#endif
    public async Task Azure_DisableExt(int testcase)
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                bool? disable = testcase switch
                {
                    0 => true,
                    1 => false,
                    2 => null,
                    _ => throw new NotImplementedException()
                };

                var fac = GetStorageFactory();
                var settings = new StorageAzure.AzureBlobSettings()
                {
                    ConnectionString = $"DefaultEndpointsProtocol=https;AccountName=plinthtest;AccountKey={AzureBlobAccountKey};EndpointSuffix=core.windows.net",
                    ContainerName = "test",
                    DefaultIndexStrategy = StorageAzure.DefaultIndexStrategy.Flat,
                    DisableBlobExtensions = disable
                };
                fac.AddAzureBlobProvider(settings);
                fac.SetDefaultWriteProviderAsAzureBlob();
                IStorage bs = fac.Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                Guid g = Guid.NewGuid();
                var b1 = new Blob()
                {
                    Name = "Test1.pdf",
                    MimeType = "application/pdf",
                    Data = b1d,
                    Guid = g
                };

                var g1 = await bs.CreateNewBlobAsync(b1);

                var b2 = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b2, Is.Not.Null);

                Assert.That(b2.Data, Is.EqualTo(b1d).AsCollection);

                var container = new Azure.Storage.Blobs.BlobContainerClient(settings.ConnectionString, settings.ContainerName);
                var blobs = container.GetBlobsAsync();
                var list = await blobs.AsPages().ToListAsync();

                switch (testcase)
                {
                    case 0:
                        Assert.That(list[0].Values[0].Name, Is.EqualTo(g1.ToString())); break;
                    case 1:
                    case 2:
                        Assert.That(list[0].Values[0].Name, Is.EqualTo(g1.ToString() + ".pdf")); break;
                }

                await bs.DeleteBlobAsync(g1);
            });
    }

#if TEST_AZURE
    [TestCase(null)]
    [TestCase("testbackup")]
#endif
    public async Task Azure_Stream(string backup)
    {
        var fac = GetStorageFactory(BlobFeatures.None);
        var settings = new StorageAzure.AzureBlobSettings()
        {
            ConnectionString = $"DefaultEndpointsProtocol=https;AccountName=plinthtest;AccountKey={AzureBlobAccountKey};EndpointSuffix=core.windows.net",
            ContainerName = "test",
            DefaultIndexStrategy = StorageAzure.DefaultIndexStrategy.Flat,
        };
        fac.AddAzureBlobProvider(settings);

        var backupSettings = new StorageAzure.AzureBlobSettings()
        {
            ConnectionString = $"DefaultEndpointsProtocol=https;AccountName=plinthtest;AccountKey={AzureBlobAccountKey};EndpointSuffix=core.windows.net",
            ContainerName = "testbackup",
            DefaultIndexStrategy = StorageAzure.DefaultIndexStrategy.Flat,
        };
        if (backup != null)
            fac.AddBackupAzureBlobProvider(backupSettings);
        fac.SetDefaultWriteProviderAsAzureBlob();

        // clear out existing bucket
        (var container, var backupContainer) = await ClearAzureContainers(settings, backupSettings);

        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = fac.Get(c, "TestUser");

                var file = F1;
                var b1d = File.ReadAllBytes(file);
                Guid g = Guid.NewGuid();
                var b1 = new Blob()
                {
                    Name = Path.GetFileName(file),
                    MimeType = "video/mp4", //"application/pdf",
                    Guid = g
                };

                var g1 = await bs.CreateNewBlobAsync(b1, File.OpenRead(file), b1d.LongLength);

                var b2 = await bs.ReadBlobAsync(g1);
                Assert.That(b2, Is.Not.Null);

                var b1r = await bs.ReadBlobStreamAsync(g1);
                b1r.blob!.Data = b1r.stream.ToBytes();

                Assert.That(b1r.blob.Data, Is.EqualTo(b1d).AsCollection);

                var list = container.GetBlobsAsync();
                Assert.That((await list.FirstAsync()).Name, Is.EqualTo($"{g1}.pdf"));

                if (backup != null)
                {
                    list = backupContainer!.GetBlobsAsync();
                    Assert.That((await list.FirstAsync()).Name, Is.EqualTo($"{g1}.pdf"));
                }

                await bs.DeleteBlobAsync(g1);
            });

        var list = container.GetBlobsAsync();
        Assert.That(await list.CountAsync(), Is.EqualTo(0));

        if (backup != null)
        {
            list = backupContainer!.GetBlobsAsync();
            Assert.That(await list.CountAsync(), Is.EqualTo(0));
        }
    }

#if TEST_AZURE
    [Test]
    [TestCase(null)]
    [TestCase("backup-prefix/")]
#endif
    public async Task Azure_Backup_And_Prefixes(string backupPrefix)
    {
        var fac = GetStorageFactory();
        var settings = new StorageAzure.AzureBlobSettings()
        {
            ConnectionString = $"DefaultEndpointsProtocol=https;AccountName=plinthtest;AccountKey={AzureBlobAccountKey};EndpointSuffix=core.windows.net",
            ContainerName = "test",
            BlobPrefix = "my-prefix/",
            DefaultIndexStrategy = StorageAzure.DefaultIndexStrategy.Flat,
        };
        fac.AddAzureBlobProvider(settings);

        var backupSettings = new StorageAzure.AzureBlobSettings()
        {
            ConnectionString = $"DefaultEndpointsProtocol=https;AccountName=plinthtest;AccountKey={AzureBlobAccountKey};EndpointSuffix=core.windows.net",
            ContainerName = "testbackup",
            BlobPrefix = backupPrefix,
            DefaultIndexStrategy = StorageAzure.DefaultIndexStrategy.ByDate,
        };
        fac.AddBackupAzureBlobProvider(backupSettings);
        fac.SetDefaultWriteProviderAsAzureBlob();

        // clear out existing bucket
        (var container, var backupContainer) = await ClearAzureContainers(settings, backupSettings);

        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = fac.Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                Guid g = Guid.NewGuid();
                var b1 = new Blob()
                {
                    Name = "Test1.pdf",
                    MimeType = "application/pdf",
                    Data = b1d,
                    Guid = g
                };

                var g1 = await bs.CreateNewBlobAsync(b1);

                var b2 = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b2, Is.Not.Null);

                Assert.That(b2.Data, Is.EqualTo(b1d).AsCollection);

                var list = container.GetBlobsAsync();
                Assert.That((await list.FirstAsync()).Name, Is.EqualTo($"my-prefix/{g1}.pdf"));

                list = backupContainer!.GetBlobsAsync();
                Assert.That((await list.FirstAsync()).Name, Is.EqualTo($"{backupPrefix}{g1}.pdf"));

                await bs.DeleteBlobAsync(g1);
            });

            var list = container.GetBlobsAsync();
        Assert.That(await list.CountAsync(), Is.EqualTo(0));

            list = backupContainer!.GetBlobsAsync();
        Assert.That(await list.CountAsync(), Is.EqualTo(0));
    }
    #endregion

#region Test Transfer
    [Test]
    public async Task TransferDB2FS()
    {
        var tests = new List<(BlobFeatures sourceFeatures, BlobFeatures? targetFeatures, bool retain)>()
        {
            (BlobFeatures.None, null, true),
            (BlobFeatures.None, null, false),
            (BlobFeatures.None, BlobFeatures.None, true),
            (BlobFeatures.None, BlobFeatures.None, false),
            (BlobFeatures.Compressed, null, true),
            (BlobFeatures.Compressed, null, false),
            (BlobFeatures.Compressed, BlobFeatures.None, true),
            (BlobFeatures.Compressed, BlobFeatures.None, false),
            (BlobFeatures.Encrypted, null, true),
            (BlobFeatures.Encrypted, null, false),
            (BlobFeatures.Encrypted, BlobFeatures.None, true),
            (BlobFeatures.Encrypted, BlobFeatures.None, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, null, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, null, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.None, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.None, false),
            (BlobFeatures.None, BlobFeatures.Compressed, true),
            (BlobFeatures.None, BlobFeatures.Compressed, false),
            (BlobFeatures.None, BlobFeatures.Encrypted, true),
            (BlobFeatures.None, BlobFeatures.Encrypted, false),
            (BlobFeatures.None, BlobFeatures.Compressed | BlobFeatures.Encrypted, true),
            (BlobFeatures.None, BlobFeatures.Compressed | BlobFeatures.Encrypted, false),
            (BlobFeatures.Compressed, BlobFeatures.Compressed, true),
            (BlobFeatures.Compressed, BlobFeatures.Compressed, false),
            (BlobFeatures.Compressed, BlobFeatures.Encrypted, true),
            (BlobFeatures.Compressed, BlobFeatures.Encrypted, false),
            (BlobFeatures.Compressed, BlobFeatures.Compressed | BlobFeatures.Encrypted, true),
            (BlobFeatures.Compressed, BlobFeatures.Compressed | BlobFeatures.Encrypted, false),
            (BlobFeatures.Encrypted, BlobFeatures.Compressed, true),
            (BlobFeatures.Encrypted, BlobFeatures.Compressed, false),
            (BlobFeatures.Encrypted, BlobFeatures.Encrypted, true),
            (BlobFeatures.Encrypted, BlobFeatures.Encrypted, false),
            (BlobFeatures.Encrypted, BlobFeatures.Compressed | BlobFeatures.Encrypted, true),
            (BlobFeatures.Encrypted, BlobFeatures.Compressed | BlobFeatures.Encrypted, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Compressed, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Compressed, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Encrypted, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Encrypted, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Compressed | BlobFeatures.Encrypted, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Compressed | BlobFeatures.Encrypted, false),
        };

        foreach (var test in tests)
        {
#if !TEST_TRANSFERS_FULL
            if (test.retain || test.targetFeatures == null)
                continue;
#endif
            Blob b1 = null!;
            await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
                async (c) =>
                {
                    var fac = GetFileFactory(defaultFeatures: test.sourceFeatures);
                    fac.SetDefaultWriteProviderAsDatabase();
                    var s = fac.Get(c, "TestUser");

                    var b1d = File.ReadAllBytes(F1);
                    b1 = new Blob()
                    {
                        Name = "Test1",
                        MimeType = "application/pdf",
                        Data = b1d
                    };
                    var g1 = await s.CreateNewBlobAsync(b1);
                    Assert.That(b1.Guid!.Value, Is.EqualTo(g1));
                    var b1r = await s.ReadBlobAsync(g1) ?? throw new Exception();

                    await s.TransferBlobAsync(g1, FileSystemProviderId.Key, retainOriginal: test.retain, features: test.targetFeatures);

                    var b = await s.ReadBlobAsync(g1) ?? throw new Exception();
                    Assert.That(b.Features, Is.EqualTo(test.targetFeatures ?? test.sourceFeatures));
                    Assert.That(b.Name, Is.EqualTo("Test1"));
                    Assert.That(b.MimeType, Is.EqualTo("application/pdf"));
                    Assert.That(b.Data, Is.EqualTo(b1d).AsCollection);

                    Assert.That(b.BlobSize, Is.EqualTo(933850));
                    if (b.Features == (BlobFeatures.Encrypted | BlobFeatures.Compressed))
                        Assert.That(b.StoredSize, Is.EqualTo(635890));
                    else if (b.Features == (BlobFeatures.Encrypted))
                        Assert.That(b.StoredSize, Is.EqualTo(933906));
                    else if (b.Features == (BlobFeatures.Compressed))
#if NET8_0_OR_GREATER
                        Assert.That(b.StoredSize, Is.EqualTo(635835));
#else
                        Assert.That(b.StoredSize, Is.EqualTo(635837)); // net6 gzip is slightly less good
#endif
                    else
                        Assert.That(b.StoredSize, Is.EqualTo(933850));

                    Assert.That(new FileInfo(Path.Combine(FSPath1, b1.Guid!.ToString()!)).Length, Is.EqualTo(b.StoredSize));

                    Console.WriteLine($"s[{test.sourceFeatures} {b1r.StoredSize} {b1r.BlobSize}] t[{b.Features} {b.StoredSize} {b.BlobSize}]");

#if SQLSERVER
                    var isNull = c.ExecuteRawQueryOne($"SELECT 1 AS dl FROM {IntegrationTestSetup.Schema}.blob WHERE GUID = @GUID AND Data IS NULL", r => r.GetInt("dl"), new SqlParameter("@GUID", g1));
#elif PGSQL
                    var isNull = c.ExecuteRawQueryOne($"SELECT true AS dl FROM {IntegrationTestSetup.Schema}.blob WHERE guid = @i_guid AND Data IS NULL", r => r.GetBool("dl"), new NpgsqlParameter("@i_guid", g1));
#endif
                    Assert.That(isNull.RowReturned, Is.EqualTo(!test.retain));
                });

            Assert.That(File.Exists(Path.Combine(FSPath1, b1.Guid!.ToString()!)));
        }
    }

    [Test]
    public async Task TransferFS2DB()
    {
        var tests = new List<(BlobFeatures sourceFeatures, BlobFeatures? targetFeatures, bool retain)>()
        {
            (BlobFeatures.None, null, true),
            (BlobFeatures.None, null, false),
            (BlobFeatures.None, BlobFeatures.None, true),
            (BlobFeatures.None, BlobFeatures.None, false),
            (BlobFeatures.Compressed, null, true),
            (BlobFeatures.Compressed, null, false),
            (BlobFeatures.Compressed, BlobFeatures.None, true),
            (BlobFeatures.Compressed, BlobFeatures.None, false),
            (BlobFeatures.Encrypted, null, true),
            (BlobFeatures.Encrypted, null, false),
            (BlobFeatures.Encrypted, BlobFeatures.None, true),
            (BlobFeatures.Encrypted, BlobFeatures.None, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, null, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, null, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.None, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.None, false),
            (BlobFeatures.None, BlobFeatures.Compressed, true),
            (BlobFeatures.None, BlobFeatures.Compressed, false),
            (BlobFeatures.None, BlobFeatures.Encrypted, true),
            (BlobFeatures.None, BlobFeatures.Encrypted, false),
            (BlobFeatures.None, BlobFeatures.Compressed | BlobFeatures.Encrypted, true),
            (BlobFeatures.None, BlobFeatures.Compressed | BlobFeatures.Encrypted, false),
            (BlobFeatures.Compressed, BlobFeatures.Compressed, true),
            (BlobFeatures.Compressed, BlobFeatures.Compressed, false),
            (BlobFeatures.Compressed, BlobFeatures.Encrypted, true),
            (BlobFeatures.Compressed, BlobFeatures.Encrypted, false),
            (BlobFeatures.Compressed, BlobFeatures.Compressed | BlobFeatures.Encrypted, true),
            (BlobFeatures.Compressed, BlobFeatures.Compressed | BlobFeatures.Encrypted, false),
            (BlobFeatures.Encrypted, BlobFeatures.Compressed, true),
            (BlobFeatures.Encrypted, BlobFeatures.Compressed, false),
            (BlobFeatures.Encrypted, BlobFeatures.Encrypted, true),
            (BlobFeatures.Encrypted, BlobFeatures.Encrypted, false),
            (BlobFeatures.Encrypted, BlobFeatures.Compressed | BlobFeatures.Encrypted, true),
            (BlobFeatures.Encrypted, BlobFeatures.Compressed | BlobFeatures.Encrypted, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Compressed, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Compressed, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Encrypted, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Encrypted, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Compressed | BlobFeatures.Encrypted, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Compressed | BlobFeatures.Encrypted, false),
        };

        foreach (var test in tests)
        {
#if !TEST_TRANSFERS_FULL
            if (test.retain || test.targetFeatures == null)
                continue;
#endif
            Blob b1 = null!;
            await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
                async (c) =>
                {
                    var fac = GetFileFactory(defaultFeatures: test.sourceFeatures);
                    var s = fac.Get(c, "TestUser");

                    var b1d = File.ReadAllBytes(F1);
                    b1 = new Blob()
                    {
                        Name = "Test1",
                        MimeType = "application/pdf",
                        Data = b1d
                    };
                    var g1 = await s.CreateNewBlobAsync(b1);
                    Assert.That(b1.Guid!.Value, Is.EqualTo(g1));
                    var b1r = await s.ReadBlobAsync(g1) ?? throw new Exception();

                    await s.TransferBlobAsync(g1, DatabaseProviderId.Key, retainOriginal: test.retain, features: test.targetFeatures);

                    var b = await s.ReadBlobAsync(g1) ?? throw new Exception();
                    Assert.That(b.Features, Is.EqualTo(test.targetFeatures ?? test.sourceFeatures));
                    Assert.That(b.Name, Is.EqualTo("Test1"));
                    Assert.That(b.MimeType, Is.EqualTo("application/pdf"));
                    Assert.That(b.Data, Is.EqualTo(b1d).AsCollection);

                    Assert.That(b.BlobSize, Is.EqualTo(933850));
                    if (b.Features == (BlobFeatures.Encrypted | BlobFeatures.Compressed))
                        Assert.That(b.StoredSize, Is.EqualTo(635890));
                    else if (b.Features == (BlobFeatures.Encrypted))
                        Assert.That(b.StoredSize, Is.EqualTo(933906));
                    else if (b.Features == (BlobFeatures.Compressed))
#if NET8_0_OR_GREATER
                        Assert.That(b.StoredSize, Is.EqualTo(635835));
#else
                        Assert.That(b.StoredSize, Is.EqualTo(635837)); // net6 gzip is slightly less good
#endif
                    else
                        Assert.That(b.StoredSize, Is.EqualTo(933850));

#if SQLSERVER
                    var datalen = c.ExecuteRawQueryOne($"SELECT DATALENGTH(Data) AS dl FROM {IntegrationTestSetup.Schema}.blob WHERE GUID = @GUID AND Data IS NOT NULL", r => r.GetLong("dl"), new SqlParameter("@GUID", g1));
#elif PGSQL
                    var datalen = c.ExecuteRawQueryOne($"SELECT octet_length(data) AS dl FROM {IntegrationTestSetup.Schema}.blob WHERE guid = @i_guid AND data IS NOT NULL", r => r.GetLong("dl"), new NpgsqlParameter("@i_guid", g1));
#endif
                    Assert.That(datalen.RowReturned);
                    Assert.That(datalen.Value, Is.EqualTo(b.StoredSize));

                    Console.WriteLine($"s[{test.sourceFeatures} {b1r.StoredSize} {b1r.BlobSize}] t[{b.Features} {b.StoredSize} {b.BlobSize}]");
                });

            Assert.That(File.Exists(Path.Combine(FSPath1, b1.Guid!.ToString()!)), Is.EqualTo(test.retain));
        }
    }

    [Test]
    public async Task TransferFS2DB_Failure()
    {
        Blob b1 = null!;
        Guid g1 = Guid.Empty;
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                var fac = GetFileFactory(defaultFeatures: BlobFeatures.None);
                var s = fac.Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                g1 = await s.CreateNewBlobAsync(b1);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));
                var b1r = await s.ReadBlobAsync(g1) ?? throw new Exception();
            });

        Assert.That(File.Exists(Path.Combine(FSPath1, b1.Guid!.ToString()!)));

        Assert.ThrowsAsync<Exception>(async () => {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                var fac = GetFileFactory(defaultFeatures: BlobFeatures.None);
                var s = fac.Get(c, "TestUser");

                await s.TransferBlobAsync(g1, DatabaseProviderId.Key, retainOriginal: false);

                var b1d = File.ReadAllBytes(F1);
                var b = await s.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b.Name, Is.EqualTo("Test1"));
                Assert.That(b.MimeType, Is.EqualTo("application/pdf"));
                Assert.That(b.Data, Is.EqualTo(b1d).AsCollection);

                throw new Exception("something happened");
            });
        });

        Assert.That(File.Exists(Path.Combine(FSPath1, b1.Guid!.ToString()!)));
    }

    [Test]
    public async Task TransferFS2FS()
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                var fac = GetFileFactory();
                var s = fac.Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                var g1 = await s.CreateNewBlobAsync(b1);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));
                var b1r = await s.ReadBlobAsync(g1) ?? throw new Exception();

                await s.TransferBlobAsync(g1, FileSystemProviderId.Key);

                var b = await s.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b.Name, Is.EqualTo("Test1"));
                Assert.That(b.MimeType, Is.EqualTo("application/pdf"));
                Assert.That(b.Data, Is.EqualTo(b1d).AsCollection);
            });
    }

    [Test]
    public async Task TransferDB2DB()
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                var fac = GetDBFactory();
                var s = fac.Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                var b1 = new Blob()
                {
                    Name = "Test1",
                    MimeType = "application/pdf",
                    Data = b1d
                };
                var g1 = await s.CreateNewBlobAsync(b1);
                Assert.That(b1.Guid!.Value, Is.EqualTo(g1));
                var b1r = await s.ReadBlobAsync(g1) ?? throw new Exception();

                await s.TransferBlobAsync(g1, DatabaseProviderId.Key);

                var b = await s.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b.Name, Is.EqualTo("Test1"));
                Assert.That(b.MimeType, Is.EqualTo("application/pdf"));
                Assert.That(b.Data, Is.EqualTo(b1d).AsCollection);
            });
    }

    [Test]
    [TestCase(WriterType.DB)]
    [TestCase(WriterType.File)]
    public async Task TransferNoOrig(WriterType type)
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                await Task.Yield();
                IStorage bs = GetFileOrDBFactory(type).Get(c, "TestUser");
                Guid g = Guid.NewGuid();
                var target = type switch { WriterType.DB => FileSystemProviderId.Key, WriterType.File => DatabaseProviderId.Key, _ => throw new Exception() };
                Assert.ThrowsAsync<LogicalNotFoundException>(async () => await bs.TransferBlobAsync(g, target));
            });
    }

#if TEST_AWS
    [Test]
#endif
    public async Task Transfer_FS2AWS_AndBack()
    {
        var tests = new List<(BlobFeatures sourceFeatures, BlobFeatures? targetFeatures, bool retain)>()
        {
            (BlobFeatures.None, null, true),
            (BlobFeatures.None, null, false),
            (BlobFeatures.None, BlobFeatures.None, true),
            (BlobFeatures.None, BlobFeatures.None, false),
            (BlobFeatures.Compressed, null, true),
            (BlobFeatures.Compressed, null, false),
            (BlobFeatures.Compressed, BlobFeatures.None, true),
            (BlobFeatures.Compressed, BlobFeatures.None, false),
            (BlobFeatures.Encrypted, null, true),
            (BlobFeatures.Encrypted, null, false),
            (BlobFeatures.Encrypted, BlobFeatures.None, true),
            (BlobFeatures.Encrypted, BlobFeatures.None, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, null, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, null, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.None, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.None, false),
            (BlobFeatures.None, BlobFeatures.Compressed, true),
            (BlobFeatures.None, BlobFeatures.Compressed, false),
            (BlobFeatures.None, BlobFeatures.Encrypted, true),
            (BlobFeatures.None, BlobFeatures.Encrypted, false),
            (BlobFeatures.None, BlobFeatures.Compressed | BlobFeatures.Encrypted, true),
            (BlobFeatures.None, BlobFeatures.Compressed | BlobFeatures.Encrypted, false),
            (BlobFeatures.Compressed, BlobFeatures.Compressed, true),
            (BlobFeatures.Compressed, BlobFeatures.Compressed, false),
            (BlobFeatures.Compressed, BlobFeatures.Encrypted, true),
            (BlobFeatures.Compressed, BlobFeatures.Encrypted, false),
            (BlobFeatures.Compressed, BlobFeatures.Compressed | BlobFeatures.Encrypted, true),
            (BlobFeatures.Compressed, BlobFeatures.Compressed | BlobFeatures.Encrypted, false),
            (BlobFeatures.Encrypted, BlobFeatures.Compressed, true),
            (BlobFeatures.Encrypted, BlobFeatures.Compressed, false),
            (BlobFeatures.Encrypted, BlobFeatures.Encrypted, true),
            (BlobFeatures.Encrypted, BlobFeatures.Encrypted, false),
            (BlobFeatures.Encrypted, BlobFeatures.Compressed | BlobFeatures.Encrypted, true),
            (BlobFeatures.Encrypted, BlobFeatures.Compressed | BlobFeatures.Encrypted, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Compressed, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Compressed, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Encrypted, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Encrypted, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Compressed | BlobFeatures.Encrypted, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Compressed | BlobFeatures.Encrypted, false),
        };

        for (int direction = 0; direction < 2; direction++)
        {
            foreach (var test in tests)
            {
#if !TEST_TRANSFERS_FULL
                if (test.retain || test.targetFeatures == null)
                    continue;
#endif
                var fac = GetFileFactory(defaultFeatures: test.sourceFeatures);
                var s3settings = new StorageAWS.S3Settings()
                {
                    S3Url = "https://s3.us-west-1.amazonaws.com",
                    BucketName = "plinth-s3-test-backup",
                    ClientId = S3_AccessID,
                    ClientSecret = S3_AccessKey,
                    DefaultIndexStrategy = StorageAWS.DefaultIndexStrategy.Flat
                };

                fac.AddS3Provider(s3settings);
                switch (direction)
                {
                    case 0: break;
                    case 1: fac.SetDefaultWriteProviderAsS3(); break;
                }

                // clear out existing bucket
                (var rawS3Client, var _) = await ClearS3Buckets(s3settings, null);

                Guid g1 = Guid.NewGuid();
                await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
                    async (c) =>
                    {
                        IStorage bs = fac.Get(c, "TestUser");

                        var b1d = File.ReadAllBytes(F1);
                        var b1 = new Blob()
                        {
                            Name = "Test1.pdf",
                            MimeType = "application/pdf",
                            Data = b1d
                        };

                        g1 = await bs.CreateNewBlobAsync(b1);

                        var b = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                        Assert.That(b, Is.Not.Null);
                        Assert.That(b.Data, Is.EqualTo(b1d).AsCollection);

                        switch (direction)
                        {
                            case 0: // fs->s3
                                {
                                    var list = await rawS3Client.ListObjectsV2Async(new Amazon.S3.Model.ListObjectsV2Request { BucketName = s3settings.BucketName });
                                    Assert.That(list.S3Objects, Is.Empty);

                                    await bs.TransferBlobAsync(g1, S3ProviderId.Key, retainOriginal: test.retain, features: test.targetFeatures);

                                    b = await bs.ReadBlobAsync(g1) ?? throw new Exception();

                                    list = await rawS3Client.ListObjectsV2Async(new Amazon.S3.Model.ListObjectsV2Request { BucketName = s3settings.BucketName });
                                    Assert.That(list.S3Objects.First().Key, Is.EqualTo($"{g1}.pdf"));
                                    Assert.That(list.S3Objects.First().Size, Is.EqualTo(b.StoredSize), $"features == {b.Features}");
                                }
                                break;
                            case 1: // s3->fs
                                {
                                    var list = await rawS3Client.ListObjectsV2Async(new Amazon.S3.Model.ListObjectsV2Request { BucketName = s3settings.BucketName });
                                    Assert.That(list.S3Objects.First().Key, Is.EqualTo($"{g1}.pdf"));

                                    await bs.TransferBlobAsync(g1, FileSystemProviderId.Key, retainOriginal: test.retain, features: test.targetFeatures);

                                    b = await bs.ReadBlobAsync(g1) ?? throw new Exception();

                                    list = await rawS3Client.ListObjectsV2Async(new Amazon.S3.Model.ListObjectsV2Request { BucketName = s3settings.BucketName });
                                    Assert.That(list.S3Objects, Has.Count.EqualTo(1)); // deletion occurs in post commit hook
                                }
                                break;
                        }

                        b = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                        Assert.That(b.Data, Is.EqualTo(b1d).AsCollection);
                        Assert.That(b.BlobSize, Is.EqualTo(933850));
                        if (b.Features == (BlobFeatures.Encrypted | BlobFeatures.Compressed))
                            Assert.That(b.StoredSize, Is.EqualTo(635890));
                        else if (b.Features == (BlobFeatures.Encrypted))
                            Assert.That(b.StoredSize, Is.EqualTo(933906));
                        else if (b.Features == (BlobFeatures.Compressed))
#if NET8_0_OR_GREATER
                            Assert.That(b.StoredSize, Is.EqualTo(635835));
#else
                            Assert.That(b.StoredSize, Is.EqualTo(635837)); // net6 gzip is slightly less good
#endif
                        else
                            Assert.That(b.StoredSize, Is.EqualTo(933850));
                    });

                var list = await rawS3Client.ListObjectsV2Async(new Amazon.S3.Model.ListObjectsV2Request { BucketName = s3settings.BucketName });
                Assert.That(list.S3Objects, Has.Count.EqualTo((test.retain && direction == 1) || (direction == 0) ? 1 : 0));

                // d  0 0 1
                // r  f t X
                //    f t t

                Assert.That(File.Exists(Path.Combine(FSPath1, g1.ToString())), Is.EqualTo((test.retain && direction == 0) || (direction == 1)));
            }
        }
    }

#if TEST_AZURE
    [Test]
#endif
    public async Task Transfer_FS2Azure_AndBack()
    {
        var tests = new List<(BlobFeatures sourceFeatures, BlobFeatures? targetFeatures, bool retain)>()
        {
            (BlobFeatures.None, null, true),
            (BlobFeatures.None, null, false),
            (BlobFeatures.None, BlobFeatures.None, true),
            (BlobFeatures.None, BlobFeatures.None, false),
            (BlobFeatures.Compressed, null, true),
            (BlobFeatures.Compressed, null, false),
            (BlobFeatures.Compressed, BlobFeatures.None, true),
            (BlobFeatures.Compressed, BlobFeatures.None, false),
            (BlobFeatures.Encrypted, null, true),
            (BlobFeatures.Encrypted, null, false),
            (BlobFeatures.Encrypted, BlobFeatures.None, true),
            (BlobFeatures.Encrypted, BlobFeatures.None, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, null, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, null, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.None, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.None, false),
            (BlobFeatures.None, BlobFeatures.Compressed, true),
            (BlobFeatures.None, BlobFeatures.Compressed, false),
            (BlobFeatures.None, BlobFeatures.Encrypted, true),
            (BlobFeatures.None, BlobFeatures.Encrypted, false),
            (BlobFeatures.None, BlobFeatures.Compressed | BlobFeatures.Encrypted, true),
            (BlobFeatures.None, BlobFeatures.Compressed | BlobFeatures.Encrypted, false),
            (BlobFeatures.Compressed, BlobFeatures.Compressed, true),
            (BlobFeatures.Compressed, BlobFeatures.Compressed, false),
            (BlobFeatures.Compressed, BlobFeatures.Encrypted, true),
            (BlobFeatures.Compressed, BlobFeatures.Encrypted, false),
            (BlobFeatures.Compressed, BlobFeatures.Compressed | BlobFeatures.Encrypted, true),
            (BlobFeatures.Compressed, BlobFeatures.Compressed | BlobFeatures.Encrypted, false),
            (BlobFeatures.Encrypted, BlobFeatures.Compressed, true),
            (BlobFeatures.Encrypted, BlobFeatures.Compressed, false),
            (BlobFeatures.Encrypted, BlobFeatures.Encrypted, true),
            (BlobFeatures.Encrypted, BlobFeatures.Encrypted, false),
            (BlobFeatures.Encrypted, BlobFeatures.Compressed | BlobFeatures.Encrypted, true),
            (BlobFeatures.Encrypted, BlobFeatures.Compressed | BlobFeatures.Encrypted, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Compressed, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Compressed, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Encrypted, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Encrypted, false),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Compressed | BlobFeatures.Encrypted, true),
            (BlobFeatures.Encrypted | BlobFeatures.Compressed, BlobFeatures.Compressed | BlobFeatures.Encrypted, false),
        };

        for (int direction = 0; direction < 2; direction++)
        {
            foreach (var test in tests)
            {
#if !TEST_TRANSFERS_FULL
                if (test.retain || test.targetFeatures == null)
                    continue;
#endif
                Console.WriteLine($"{test.sourceFeatures} => {test.targetFeatures}, retain={test.retain}, dir={direction}");
                var fac = GetFileFactory(defaultFeatures: test.sourceFeatures);

                var azureSettings = new StorageAzure.AzureBlobSettings()
                {
                    ConnectionString = $"DefaultEndpointsProtocol=https;AccountName=plinthtest;AccountKey={AzureBlobAccountKey};EndpointSuffix=core.windows.net",
                    ContainerName = "testbackup",
                    DefaultIndexStrategy = StorageAzure.DefaultIndexStrategy.Flat,
                };

                fac.AddAzureBlobProvider(azureSettings);
                switch (direction)
                {
                    case 0: break;
                    case 1: fac.SetDefaultWriteProviderAsAzureBlob(); break;
                }

                // clear out existing bucket
                (var rawAzClient, var _) = await ClearAzureContainers(azureSettings, null);

                Guid g1 = Guid.NewGuid();
                await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
                    async (c) =>
                    {
                        IStorage bs = fac.Get(c, "TestUser");

                        var b1d = File.ReadAllBytes(F1);
                        var b1 = new Blob()
                        {
                            Name = "Test1.pdf",
                            MimeType = "application/pdf",
                            Data = b1d
                        };

                        g1 = await bs.CreateNewBlobAsync(b1);

                        var b = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                        Assert.That(b, Is.Not.Null);
                        Assert.That(b.Data, Is.EqualTo(b1d).AsCollection);

                        switch (direction)
                        {
                            case 0: // fs->az
                                {
                                    var azlist = rawAzClient.GetBlobsAsync();
                                    Assert.That((await azlist.CountAsync()), Is.EqualTo(0));

                                    await bs.TransferBlobAsync(g1, AzureBlobProviderId.Key, retainOriginal: test.retain, features: test.targetFeatures);

                                    b = await bs.ReadBlobAsync(g1) ?? throw new Exception();

                                    azlist = rawAzClient.GetBlobsAsync();
                                    var azobj = await azlist.FirstAsync();
                                    Assert.That(azobj.Name, Is.EqualTo($"{g1}.pdf"));
                                    Assert.That(azobj.Properties.ContentLength, Is.EqualTo(b.StoredSize), $"features == {b.Features}");
                                }
                                break;
                            case 1: // az->fs
                                {
                                    var azlist = rawAzClient.GetBlobsAsync();
                                    var azobj = await azlist.FirstAsync();
                                    Assert.That(azobj.Name, Is.EqualTo($"{g1}.pdf"));

                                    await bs.TransferBlobAsync(g1, FileSystemProviderId.Key, retainOriginal: test.retain, features: test.targetFeatures);

                                    b = await bs.ReadBlobAsync(g1) ?? throw new Exception();

                                    azlist = rawAzClient.GetBlobsAsync();
                                    Assert.That(await azlist.CountAsync(), Is.EqualTo(1)); // deletion occurs in post commit hook
                                }
                                break;
                        }

                        Assert.That(b.Data, Is.EqualTo(b1d).AsCollection);
                        Assert.That(b.BlobSize, Is.EqualTo(933850));
                        if (b.Features == (BlobFeatures.Encrypted | BlobFeatures.Compressed))
                            Assert.That(b.StoredSize, Is.EqualTo(635890));
                        else if (b.Features == (BlobFeatures.Encrypted))
                            Assert.That(b.StoredSize, Is.EqualTo(933906));
                        else if (b.Features == (BlobFeatures.Compressed))
#if NET8_0_OR_GREATER
                            Assert.That(b.StoredSize, Is.EqualTo(635835));
#else
                            Assert.That(b.StoredSize, Is.EqualTo(635837)); // net6 gzip is slightly less good
#endif
                        else
                            Assert.That(b.StoredSize, Is.EqualTo(933850));
                    });

                var azlist = rawAzClient.GetBlobsAsync();
                Assert.That((await azlist.CountAsync()), Is.EqualTo((test.retain && direction == 1) || (direction == 0) ? 1 : 0));

                // d  0 0 1
                // r  f t X
                //    f t t

                Assert.That(File.Exists(Path.Combine(FSPath1, g1.ToString())), Is.EqualTo((test.retain && direction == 0) || (direction == 1)));
            }
        }
    }
#endregion

#if TEST_AWS && TEST_AZURE
    [Test]
#endif
    public async Task DB_Backup_S3Azure()
    {
        var fac = GetStorageFactory();
        fac.AddDatabaseProvider();
        fac.SetDefaultWriteProviderAsDatabase();
        var s3settings = new StorageAWS.S3Settings()
        {
            S3Url = "https://s3.us-west-1.amazonaws.com",
            BucketName = "plinth-s3-test-backup",
            ClientId = S3_AccessID,
            ClientSecret = S3_AccessKey,
            DefaultIndexStrategy = StorageAWS.DefaultIndexStrategy.Flat
        };

        fac.AddBackupS3Provider(s3settings);

        var azureSettings = new StorageAzure.AzureBlobSettings()
        {
            ConnectionString = $"DefaultEndpointsProtocol=https;AccountName=plinthtest;AccountKey={AzureBlobAccountKey};EndpointSuffix=core.windows.net",
            ContainerName = "testbackup",
            DefaultIndexStrategy = StorageAzure.DefaultIndexStrategy.Flat,
        };

        fac.AddBackupAzureBlobProvider(azureSettings);

        // clear out existing bucket
        (var rawS3Client, var _) = await ClearS3Buckets(s3settings, null);
        (var rawAzClient, var _) = await ClearAzureContainers(azureSettings, null);

        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                IStorage bs = fac.Get(c, "TestUser");

                var b1d = File.ReadAllBytes(F1);
                Guid g = Guid.NewGuid();
                var b1 = new Blob()
                {
                    Name = "Test1.pdf",
                    MimeType = "application/pdf",
                    Data = b1d,
                    Guid = g
                };

                var g1 = await bs.CreateNewBlobAsync(b1);

                var b2 = await bs.ReadBlobAsync(g1) ?? throw new Exception();
                Assert.That(b2, Is.Not.Null);

                Assert.That(b2.Data, Is.EqualTo(b1d).AsCollection);

                var list = await rawS3Client.ListObjectsV2Async(new Amazon.S3.Model.ListObjectsV2Request { BucketName = s3settings.BucketName });
                Assert.That(list.S3Objects.First().Key, Is.EqualTo($"{g1}.pdf"));

                var azlist = rawAzClient.GetBlobsAsync();
                Assert.That((await azlist.FirstAsync()).Name, Is.EqualTo($"{g1}.pdf"));

                await bs.DeleteBlobAsync(g1);
            });

            var list = await rawS3Client.ListObjectsV2Async(new Amazon.S3.Model.ListObjectsV2Request { BucketName = s3settings.BucketName });
        Assert.That(list.KeyCount, Is.EqualTo(0));

            var azlist = rawAzClient.GetBlobsAsync();
        Assert.That(await azlist.CountAsync(), Is.EqualTo(0));
    }
}
