
-- Plinth Storage Procedures
CREATE PROCEDURE dbo.usp_InsertBLOB
@GUID UNIQUEIDENTIFIER,
@Name NVARCHAR(255),
@Provider VARCHAR(10),
@BLOBIndex VARCHAR(1000) = NULL,
@MIMEType VARCHAR(255),
@Data VARBINARY(MAX) = NULL,
@OrigSize BIGINT,
@StoredSize BIGINT,
@IsEncrypted BIT = 0,
@IsCompressed BIT = 0,
@CallingUser NVARCHAR(255)
AS
BEGIN
    SET NOCOUNT OFF;
    /*
        EXECUTE dbo.usp_InsertBLOB
            @GUID = '7c393bb9-60de-4185-b9b4-0240c4ad0e15',
            @Name = 'My File',
            @BLOBIndex = '123\45\678',
            @MIMEType = 'application/pdf',
            @Data = 0xFF00,
            @DataByteCount = 4,
            @CallingUser = 'SampleUser';
    */

    INSERT INTO dbo.BLOB (
        GUID,
        Name,
        Provider,
        BLOBIndex,
        MIMEType,
        Data,
        OrigSize,
        StoredSize,
        IsEncrypted,
        IsCompressed,
        DateInserted,
        InsertedBy,
        DateUpdated,
        UpdatedBy
    )
    SELECT
        @GUID,
        @Name,
        @Provider,
        @BLOBIndex,
        @MIMEType,
        @Data,
        @OrigSize,
        @StoredSize,
        @IsEncrypted,
        @IsCompressed,
        SYSUTCDATETIME(),
        @CallingUser,
        SYSUTCDATETIME(),
        @CallingUser
    WHERE NOT EXISTS(
        SELECT * FROM dbo.BLOB
        WHERE GUID = @GUID
    );
END;
GO

CREATE PROCEDURE dbo.usp_UpdateBLOB
@GUID UNIQUEIDENTIFIER,
@Name NVARCHAR(255) = NULL,
@Provider VARCHAR(10) = NULL,
@BLOBIndex VARCHAR(1000) = NULL,
@MIMEType VARCHAR(255) = NULL,
@Data VARBINARY(MAX) = NULL,
@OrigSize BIGINT = NULL,
@StoredSize BIGINT = NULL,
@IsEncrypted BIT = NULL,
@IsCompressed BIT = NULL,
@CallingUser NVARCHAR(255)
AS
BEGIN
    SET NOCOUNT OFF;
    /*
        EXECUTE dbo.usp_UpdateBLOB
            @GUID = '7c393bb9-60de-4185-b9b4-0240c4ad0e15',
            @Name = 'My Document',
            @Data = 0xFF01,
            @CallingUser = 'SampleUser';
    */

    UPDATE dbo.BLOB SET
        Name = ISNULL(@Name, b.Name),
        Provider = ISNULL(@Provider, b.Provider),
        BLOBIndex = ISNULL(@BLOBIndex, b.BLOBIndex),
        MIMEType = ISNULL(@MIMEType, b.MIMEType),
        Data = ISNULL(@Data, b.Data),
        OrigSize = ISNULL(@OrigSize, b.OrigSize),
        StoredSize = ISNULL(@StoredSize, b.StoredSize),
        IsEncrypted = ISNULL(@IsEncrypted, b.IsEncrypted),
        IsCompressed = ISNULL(@IsCompressed, b.IsCompressed),
        DateUpdated = SYSUTCDATETIME(),
        UpdatedBy = @CallingUser
        FROM dbo.BLOB AS b
    WHERE b.GUID = @GUID;
END;
GO

CREATE PROCEDURE dbo.usp_UpdateBLOBData
@GUID UNIQUEIDENTIFIER,
@Data VARBINARY(MAX) = NULL,
@CallingUser NVARCHAR(255)
AS
BEGIN
    SET NOCOUNT OFF;
    /*
        EXECUTE dbo.usp_UpdateBLOBData
            @GUID = '7c393bb9-60de-4185-b9b4-0240c4ad0e15',
            @Data = 0xFF01,
            @CallingUser = 'SampleUser';
    */

    UPDATE dbo.BLOB SET
        Data = @Data,
        DateUpdated = SYSUTCDATETIME(),
        UpdatedBy = @CallingUser
        FROM dbo.BLOB AS b
    WHERE b.GUID = @GUID;
END;
GO


CREATE PROCEDURE dbo.usp_UpdateBLOBSizes
@GUID UNIQUEIDENTIFIER,
@OrigSize BIGINT,
@StoredSize BIGINT,
@CallingUser NVARCHAR(255)
AS
BEGIN
    SET NOCOUNT OFF;
    /*
        EXECUTE dbo.usp_UpdateBLOBSizes
            @GUID = '7c393bb9-60de-4185-b9b4-0240c4ad0e15',
            @OrigSize = 1764,
            @StoredSize = 1732,
            @CallingUser = 'SampleUser';
    */

    UPDATE dbo.BLOB SET
        OrigSize = @OrigSize,
        StoredSize = @StoredSize,
        DateUpdated = SYSUTCDATETIME(),
        UpdatedBy = @CallingUser
        FROM dbo.BLOB AS b
    WHERE b.GUID = @GUID;
END;
GO

CREATE PROCEDURE dbo.usp_DeleteBLOBByGUID
@GUID UNIQUEIDENTIFIER,
@CallingUser NVARCHAR(255)
AS
BEGIN
    SET NOCOUNT OFF;
    /*
        EXECUTE dbo.usp_DeleteBLOBByGUID,
            @GUID = '7c393bb9-60de-4185-b9b4-0240c4ad0e15',
            @CallingUser = 'SampleUser';
    */

    DELETE
    FROM dbo.BLOB
    WHERE GUID = @GUID
END;
GO

CREATE PROCEDURE dbo.usp_GetBLOBByGUID
@GUID UNIQUEIDENTIFIER
AS
BEGIN
    SET NOCOUNT ON;
    /*
        EXECUTE dbo.usp_GetBLOBByGUID
            @GUID = '7c393bb9-60de-4185-b9b4-0240c4ad0e15'
    */

    SELECT
        b.GUID,
        b.Name,
        b.Provider,
        b.BLOBIndex,
        b.MIMEType,
        b.Data,
        b.OrigSize,
        b.StoredSize,
        b.IsEncrypted,
        b.IsCompressed,
        b.DateInserted,
        b.InsertedBy,
        b.DateUpdated,
        b.UpdatedBy
        FROM dbo.BLOB AS b
    WHERE b.GUID = @GUID
END;
GO

-- Plinth Storage Procedures
