using NUnit.Framework;
using NSubstitute;
using Plinth.Storage.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Plinth.Storage.UnitTests.Providers;

[TestFixture]
internal class TestStreamReadAndCopy
{
    private static readonly byte[] _testBytes = Encoding.UTF8.GetBytes("My Test String");

    [Test]
    public void BasicReadSync_OK()
    {
        var inner = new MemoryStream(_testBytes);
        using var sc = new StreamReadAndCopy(inner);

        Assert.That(sc.CopyOfStream.Length, Is.EqualTo(0));

        var buffer = new byte[512];
        var count = sc.Read(buffer);

        Assert.That(count, Is.EqualTo(inner.Length));
        Assert.That(sc.CopyOfStream.Length, Is.EqualTo(inner.Length));
        Assert.That(sc.Position, Is.EqualTo(inner.Length));
        Assert.That(sc.Length, Is.EqualTo(inner.Length));
        Assert.That(sc.CopyOfStream.ToArray(), Is.EqualTo(_testBytes).AsCollection);
    }

    [Test]
    public async Task BasicReadAsync_OK()
    {
        var inner = new MemoryStream(_testBytes);
        using var sc = new StreamReadAndCopy(inner);

        Assert.That(sc.CopyOfStream.Length, Is.EqualTo(0));

        var buffer = new byte[512];
        var count = await sc.ReadAsync(buffer);

        Assert.That(count, Is.EqualTo(inner.Length));
        Assert.That(sc.CopyOfStream.Length, Is.EqualTo(inner.Length));
        Assert.That(sc.Position, Is.EqualTo(inner.Length));
        Assert.That(sc.Length, Is.EqualTo(inner.Length));
        Assert.That(sc.CopyOfStream.ToArray(), Is.EqualTo(_testBytes).AsCollection);
    }

    [Test]
    public void CloseCalled_OnDispose()
    {
        var inner = Substitute.For<Stream>();

        {
            using var sc = new StreamReadAndCopy(inner);
        }

        inner.Received(1).Close();
    }

    [Test]
    public void NotSupported_Throws()
    {
        var inner = new MemoryStream();
        using var sc = new StreamReadAndCopy(inner);

        Assert.Throws<NotSupportedException>(() => sc.SetLength(10));
        Assert.Throws<NotSupportedException>(() => sc.Position = 0);
        Assert.Throws<NotSupportedException>(() => sc.Write(new byte[10]));
        Assert.ThrowsAsync<NotSupportedException>(async () => await sc.WriteAsync(new byte[10]));
    }

    [Test]
    public void InvalidParam_Throws()
    {
        Assert.Throws<ArgumentNullException>(() => new StreamReadAndCopy(null!));
    }

    [Test]
    public async Task StreamPassthrough_OK()
    {
        var inner = Substitute.For<Stream>();
        inner.CanRead.Returns(true);
        inner.CanWrite.Returns(true);
        inner.CanSeek.Returns(true);

        using var sc = new StreamReadAndCopy(inner);

        Assert.That(sc.CanRead);
        Assert.That(sc.CanWrite, Is.False);
        Assert.That(sc.Position, Is.EqualTo(0));
        sc.Flush();
        await sc.FlushAsync();

        Assert.That(sc.CanSeek);
        sc.Seek(0, SeekOrigin.Current);

        inner.Received(1).Seek(Arg.Any<long>(), Arg.Any<SeekOrigin>());

        inner.Received(1).Flush();
        await inner.Received(1).FlushAsync(Arg.Any<CancellationToken>());
    }
}
