using NUnit.Framework;
using Plinth.Storage.Models;
using Plinth.Storage.Providers.FileSystem;
using Plinth.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plinth.Storage.Providers;
using System.Reflection;

namespace Tests.Plinth.Storage.UnitTests.Providers.FileSystem;

[TestFixture]
internal class TestFileProvider
{
    private static readonly byte[] _testBytes = Encoding.UTF8.GetBytes("My Test String");
    private static readonly string Temp = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!;

    [Test]
    public async Task ExpectedBehavior_OK()
    {
        var provider = new FileSystemProvider(new FileSystemSettings
        {
            BasePath = Temp
        });

        Assert.That(provider.Name, Is.EqualTo($"FS:{Temp}"));
        Assert.That(provider.WritesDataToIndex, Is.False);
        Assert.That(provider.RewriteKeepsOriginalIndex);

        var blob = new Blob()
        {
            Data = _testBytes,
            BlobSize = _testBytes.Length,
            Guid = Guid.NewGuid(),
            Provider = FileSystemProviderId.Key
        };

        await provider.WriteBlobAsync(blob, _testBytes, null, CancellationToken.None);

        var read = await provider.ReadBlobDataAsync(blob, CancellationToken.None);
        Assert.That(read, Is.EqualTo(_testBytes).AsCollection);

        Assert.That(provider.ConstructBlobIndex(blob, _ => "abc"), Is.EqualTo("abc"));

        Assert.That(File.ReadAllBytes(Path.Combine(Temp, blob.Guid!.ToString()!)), Is.EqualTo(_testBytes).AsCollection);

        await provider.DeleteBlobAsync(blob, CancellationToken.None);

        Assert.That(File.Exists(Path.Combine(Temp, blob.Guid!.ToString()!)), Is.False);

        Assert.That(await provider.GetUrlReferenceAsync(blob, CancellationToken.None), Is.Null);
        Assert.That(await provider.GetTemporaryUrlReferenceAsync(blob, TimeSpan.FromMinutes(1), CancellationToken.None), Is.Null);
    }

    [Test]
    public async Task WriteReadStreamDelete_OK()
    {
        var provider = new FileSystemProvider(new FileSystemSettings
        {
            BasePath = Temp
        });

        var blob = new Blob()
        {
            Data = _testBytes,
            BlobSize = _testBytes.Length,
            Guid = Guid.NewGuid(),
            Provider = FileSystemProviderId.Key
        };

        var write = await provider.WriteBlobStreamAsync(blob, new StreamCounter(new MemoryStream(_testBytes)), _testBytes.LongLength, null, CancellationToken.None);
        Assert.That(write.origLen, Is.EqualTo(14));
        Assert.That(write.storedLen, Is.EqualTo(14));
        Assert.That(write.wrote, Is.EqualTo(true));

        var read = await provider.ReadBlobDataAsync(blob, CancellationToken.None);
        Assert.That(read, Is.EqualTo(_testBytes).AsCollection);

        var readStream = await provider.ReadBlobDataStreamAsync(blob, new MemoryStream(_testBytes), CancellationToken.None);
        Assert.That(readStream.ToBytes(), Is.EqualTo(_testBytes).AsCollection);

        Assert.That(provider.ConstructBlobIndex(blob, _ => "abc"), Is.EqualTo("abc"));

        Assert.That(File.ReadAllBytes(Path.Combine(Temp, blob.Guid!.ToString()!)), Is.EqualTo(_testBytes).AsCollection);

        await provider.DeleteBlobAsync(blob, CancellationToken.None);

        Assert.That(File.Exists(Path.Combine(Temp, blob.Guid!.ToString()!)), Is.False);
    }

    [Test]
    public void InvalidParams_Fail()
    {
        Assert.Throws<ArgumentNullException>(() => new FileSystemProvider(new FileSystemSettings { }));
    }
}
