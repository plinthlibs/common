using NUnit.Framework;
using NSubstitute;
using Plinth.Storage.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plinth.Storage.Providers.Database;
using Plinth.Storage.Models;
using Plinth.Common.Extensions;

namespace Tests.Plinth.Storage.UnitTests.Providers.Database;

[TestFixture]
internal class TestDatabaseProvider
{
    private static readonly byte[] _testBytes = Encoding.UTF8.GetBytes("My Test String");

    [Test]
    public async Task ExpectedBehavior_OK()
    {
        var provider = new DatabaseProvider();

        Assert.That(provider.Name, Is.EqualTo("Database"));
        Assert.That(provider.WritesDataToIndex);
        Assert.That(provider.RewriteKeepsOriginalIndex);

        var blob = new Blob()
        {
            Data = _testBytes,
            BlobSize = _testBytes.Length,
            Guid = Guid.NewGuid(),
            Provider = DatabaseProviderId.Key
        };

        var read = await provider.ReadBlobDataAsync(blob, CancellationToken.None);
        Assert.That(read, Is.EqualTo(_testBytes).AsCollection);

        var readStream = await provider.ReadBlobDataStreamAsync(blob, new MemoryStream(_testBytes), CancellationToken.None);
        Assert.That(readStream.ToBytes(), Is.EqualTo(_testBytes).AsCollection);

        Assert.That(provider.ConstructBlobIndex(blob, _ => string.Empty), Is.Null);

        await provider.WriteBlobAsync(blob, _testBytes, null, CancellationToken.None);
        var write = await provider.WriteBlobStreamAsync(blob, new StreamCounter(new MemoryStream(_testBytes)), _testBytes.LongLength, null, CancellationToken.None);
        Assert.That(write.origLen, Is.EqualTo(0));
        Assert.That(write.storedLen, Is.EqualTo(0));
        Assert.That(write.wrote, Is.EqualTo(false));

        await provider.DeleteBlobAsync(blob, CancellationToken.None);

        Assert.That(await provider.GetUrlReferenceAsync(blob, CancellationToken.None), Is.Null);
        Assert.That(await provider.GetTemporaryUrlReferenceAsync(blob, TimeSpan.FromMinutes(1), CancellationToken.None), Is.Null);
    }
}
