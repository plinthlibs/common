using NUnit.Framework;
using NSubstitute;
using Plinth.Storage.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plinth.Storage.Exceptions;

namespace Tests.Plinth.Storage.UnitTests.Providers;

[TestFixture]
internal class TestValidityCheckReadStream
{
    private static readonly byte[] _testBytes = Encoding.UTF8.GetBytes("My Test String");

    [Test]
    public void BasicReadSync_OK()
    {
        var inner = new MemoryStream(_testBytes);
        using var sc = new ValidityCheckReadStream(inner, _testBytes.Length);

        Assert.That(sc.Length, Is.EqualTo(_testBytes.Length));
        Assert.That(sc.Position, Is.EqualTo(0));

        var buffer = new byte[512];
        var count = sc.Read(buffer);

        Assert.That(count, Is.EqualTo(inner.Length));
        Assert.That(sc.Position, Is.EqualTo(inner.Length));
        Assert.That(sc.Length, Is.EqualTo(inner.Length));
        Assert.That(buffer.AsSpan(0, _testBytes.Length).ToArray(), Is.EqualTo(_testBytes).AsCollection);
    }

    [Test]
    public async Task BasicReadAsync_OK()
    {
        var inner = new MemoryStream(_testBytes);
        using var sc = new ValidityCheckReadStream(inner, _testBytes.Length);

        Assert.That(sc.Length, Is.EqualTo(_testBytes.Length));
        Assert.That(sc.Position, Is.EqualTo(0));

        var buffer = new byte[512];
        var count = await sc.ReadAsync(buffer);

        Assert.That(count, Is.EqualTo(inner.Length));
        Assert.That(sc.Position, Is.EqualTo(inner.Length));
        Assert.That(sc.Length, Is.EqualTo(inner.Length));
        Assert.That(buffer.AsSpan(0, _testBytes.Length).ToArray(), Is.EqualTo(_testBytes).AsCollection);
    }

    [Test]
    [TestCase(-5)]
    [TestCase(5)]
    public async Task ReadAsync_Corrupted_Fails(int delta)
    {
        var inner = new MemoryStream(_testBytes);
        var sc = new ValidityCheckReadStream(inner, _testBytes.Length + delta);

        Assert.That(sc.Length, Is.EqualTo(_testBytes.Length));
        Assert.That(sc.Position, Is.EqualTo(0));

        var buffer = new byte[512];
        var count = await sc.ReadAsync(buffer);

        Assert.Throws<CorruptedBlobException>(sc.Dispose);
    }

    [Test]
    public void CloseCalled_OnDispose()
    {
        var inner = Substitute.For<Stream>();

        {
            using var sc = new ValidityCheckReadStream(inner, 0);
        }

        inner.Received(1).Close();
    }

    [Test]
    public void NotSupported_Throws()
    {
        var inner = new MemoryStream();
        using var sc = new ValidityCheckReadStream(inner, 0);

        Assert.Throws<NotSupportedException>(() => sc.SetLength(10));
        Assert.Throws<NotSupportedException>(() => sc.Position = 0);
        Assert.Throws<NotSupportedException>(() => sc.Seek(0, SeekOrigin.Begin));
    }

    [Test]
    public void InvalidParam_Throws()
    {
        Assert.Throws<ArgumentNullException>(() => new ValidityCheckReadStream(null!, 10));
        Assert.Throws<ArgumentNullException>(() => new ValidityCheckReadStream(null!, -1));
        Assert.Throws<ArgumentOutOfRangeException>(() => new ValidityCheckReadStream(new MemoryStream(), -1));
    }

    [Test]
    public async Task StreamPassthrough_OK()
    {
        var inner = Substitute.For<Stream>();
        inner.CanRead.Returns(true);
        inner.CanWrite.Returns(true);
#pragma warning disable CA2012 // Use ValueTasks correctly
        inner.WriteAsync(Arg.Any<ReadOnlyMemory<byte>>(), Arg.Any<CancellationToken>()).Returns(ValueTask.CompletedTask);
#pragma warning restore CA2012 // Use ValueTasks correctly

        using var sc = new ValidityCheckReadStream(inner, 0);

        Assert.That(sc.CanRead);
        Assert.That(sc.CanWrite, Is.False);
        Assert.That(sc.Position, Is.EqualTo(0));
        sc.Flush();
        await sc.FlushAsync();

        sc.Write(_testBytes);
        await sc.WriteAsync(_testBytes);

        Assert.That(sc.CanSeek, Is.False);

        inner.Received(1).Flush();
        await inner.Received(1).FlushAsync(Arg.Any<CancellationToken>());

        inner.Received(1).Write(Arg.Any<byte[]>(), Arg.Any<int>(), Arg.Any<int>());
        await inner.Received(1).WriteAsync(Arg.Any<ReadOnlyMemory<byte>>(), Arg.Any<CancellationToken>());
    }
}
