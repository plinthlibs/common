using NUnit.Framework;
using NSubstitute;
using Plinth.Storage.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Plinth.Storage.UnitTests.Providers;

[TestFixture]
internal class TestStreamCounters
{
    private static readonly byte[] _testBytes = Encoding.UTF8.GetBytes("My Test String");

    [Test]
    public void BasicReadSync_OK()
    {
        var inner = new MemoryStream(_testBytes);
        using var sc = new StreamCounter(inner);

        Assert.That(sc.ReadCalled, Is.False);
        Assert.That(sc.WriteCalled, Is.False);

        var buffer = new byte[512];
        var count = sc.Read(buffer);

        Assert.That(count, Is.EqualTo(inner.Length));
        Assert.That(sc.BytesRead, Is.EqualTo(inner.Length));
        Assert.That(sc.BytesRead, Is.EqualTo(_testBytes.Length));
        Assert.That(sc.Length, Is.EqualTo(inner.Length));
        Assert.That(buffer.AsSpan(0, _testBytes.Length).ToArray(), Is.EqualTo(_testBytes).AsCollection);

        Assert.That(sc.ReadCalled);
        Assert.That(sc.WriteCalled, Is.False);
    }

    [Test]
    public async Task BasicReadAsync_OK()
    {
        var inner = new MemoryStream(_testBytes);
        using var sc = new StreamCounter(inner);

        Assert.That(sc.ReadCalled, Is.False);
        Assert.That(sc.WriteCalled, Is.False);

        var buffer = new byte[512];
        var count = await sc.ReadAsync(buffer);

        Assert.That(count, Is.EqualTo(inner.Length));
        Assert.That(sc.BytesRead, Is.EqualTo(inner.Length));
        Assert.That(sc.BytesRead, Is.EqualTo(_testBytes.Length));
        Assert.That(sc.Length, Is.EqualTo(inner.Length));
        Assert.That(buffer.AsSpan(0, _testBytes.Length).ToArray(), Is.EqualTo(_testBytes).AsCollection);

        Assert.That(sc.ReadCalled);
        Assert.That(sc.WriteCalled, Is.False);
    }

    [Test]
    public void CloseCalled_OnDispose()
    {
        var inner = Substitute.For<Stream>();

        {
            using var sc = new StreamCounter(inner);
        }

        inner.Received(1).Close();
    }

    [Test]
    public void BasicWriteSync_OK()
    {
        var inner = new MemoryStream();
        using var sc = new StreamCounter(inner);

        Assert.That(sc.ReadCalled, Is.False);
        Assert.That(sc.WriteCalled, Is.False);

        sc.Write(_testBytes);

        Assert.That(_testBytes, Has.Length.EqualTo(inner.Length));
        Assert.That(sc.BytesWritten, Is.EqualTo(inner.Length));
        Assert.That(sc.Length, Is.EqualTo(inner.Length));
        Assert.That(sc.BytesWritten, Is.EqualTo(_testBytes.Length));

        Assert.That(sc.ReadCalled, Is.False);
        Assert.That(sc.WriteCalled);
    }

    [Test]
    public async Task BasicWriteAsync_OK()
    {
        var inner = new MemoryStream();
        using var sc = new StreamCounter(inner);

        Assert.That(sc.ReadCalled, Is.False);
        Assert.That(sc.WriteCalled, Is.False);

        await sc.WriteAsync(Encoding.UTF8.GetBytes("My Test String"));

        Assert.That(_testBytes, Has.Length.EqualTo(inner.Length));
        Assert.That(sc.BytesWritten, Is.EqualTo(inner.Length));
        Assert.That(sc.Length, Is.EqualTo(inner.Length));
        Assert.That(sc.BytesWritten, Is.EqualTo(_testBytes.Length));

        Assert.That(sc.ReadCalled, Is.False);
        Assert.That(sc.WriteCalled);
    }

    [Test]
    public void BasicReadWriteSync_OK()
    {
        var inner = new MemoryStream();
        using var sc = new StreamCounter(inner);

        Assert.That(sc.ReadCalled, Is.False);
        Assert.That(sc.WriteCalled, Is.False);
        Assert.That(sc.Position, Is.EqualTo(0));

        sc.Write(_testBytes);

        Assert.That(_testBytes, Has.Length.EqualTo(inner.Length));
        Assert.That(sc.BytesWritten, Is.EqualTo(inner.Length));
        Assert.That(sc.BytesWritten, Is.EqualTo(_testBytes.Length));
        Assert.That(sc.Position, Is.EqualTo(_testBytes.Length));

        Assert.That(sc.ReadCalled, Is.False);
        Assert.That(sc.WriteCalled);

        sc.Seek(0, SeekOrigin.Begin);
        Assert.That(sc.Position, Is.EqualTo(0));

        var buffer = new byte[512];
        var count = sc.Read(buffer);

        Assert.That(count, Is.EqualTo(inner.Length));
        Assert.That(sc.BytesRead, Is.EqualTo(inner.Length));
        Assert.That(sc.BytesRead, Is.EqualTo(_testBytes.Length));
        Assert.That(sc.Position, Is.EqualTo(_testBytes.Length));

        Assert.That(sc.ReadCalled);
        Assert.That(sc.WriteCalled);
    }

    [Test]
    public async Task BasicReadWriteAsync_OK()
    {
        var inner = new MemoryStream();
        using var sc = new StreamCounter(inner);

        Assert.That(sc.ReadCalled, Is.False);
        Assert.That(sc.WriteCalled, Is.False);
        Assert.That(sc.Position, Is.EqualTo(0));

        await sc.WriteAsync(_testBytes);

        Assert.That(_testBytes, Has.Length.EqualTo(inner.Length));
        Assert.That(sc.BytesWritten, Is.EqualTo(inner.Length));
        Assert.That(sc.BytesWritten, Is.EqualTo(_testBytes.Length));

        Assert.That(sc.ReadCalled, Is.False);
        Assert.That(sc.WriteCalled);
        Assert.That(sc.Position, Is.EqualTo(_testBytes.Length));

        sc.Seek(0, SeekOrigin.Begin);
        Assert.That(sc.Position, Is.EqualTo(0));

        var buffer = new byte[512];
        var count = await sc.ReadAsync(buffer);

        Assert.That(count, Is.EqualTo(inner.Length));
        Assert.That(sc.BytesRead, Is.EqualTo(inner.Length));
        Assert.That(sc.BytesRead, Is.EqualTo(_testBytes.Length));
        Assert.That(sc.Position, Is.EqualTo(_testBytes.Length));

        Assert.That(sc.ReadCalled);
        Assert.That(sc.WriteCalled);
    }

    [Test]
    public void NotSupported_Throws()
    {
        var inner = new MemoryStream();
        using var sc = new StreamCounter(inner);

        Assert.Throws<NotSupportedException>(() => sc.SetLength(10));
        Assert.Throws<NotSupportedException>(() => sc.Position = 0);
    }

    [Test]
    public void InvalidParam_Throws()
    {
        Assert.Throws<ArgumentNullException>(() => new StreamCounter(null!));
    }

    [Test]
    public async Task StreamPassthrough_OK()
    {
        var inner = Substitute.For<Stream>();
        inner.CanRead.Returns(true);
        inner.CanWrite.Returns(true);

        using var sc = new StreamCounter(inner);

        Assert.That(sc.CanRead);
        Assert.That(sc.CanWrite);
        Assert.That(sc.Position, Is.EqualTo(0));
        sc.Flush();
        await sc.FlushAsync();

        Assert.That(sc.CanSeek, Is.False);

        inner.Received(1).Flush();
        await inner.Received(1).FlushAsync(Arg.Any<CancellationToken>());
    }
}
