#define BASIC
#define BASIC_APPSETTINGS
//#define BASIC_CONSOLE
//#define BASIC_DEBUG
//#define BASIC_FILE
//#define SINGLE_LOGGER
//#define SINGLE_FACTORY
using Microsoft.Extensions.Logging;
using Plinth.Logging.Host;

[assembly: NUnit.Framework.NonTestAssembly]

namespace Tests.Plinth.Logging.Host;

static class Program
{
#if BASIC
    static void Main(string[] _)
    {
#if BASIC_APPSETTINGS
        var log = StaticLogManagerSetup.FromAppSettings();
#elif BASIC_CONSOLE
        var log = StaticLogManagerSetup.ConfigureForConsoleLogging(LogLevel.Trace);
#elif BASIC_DEBUG
        var log = StaticLogManagerSetup.ConfigureForDebugLogging(LogLevel.Trace);
#elif BASIC_FILE
        var log = StaticLogManagerSetup.ConfigureForFileLogging(LogLevel.Trace);
#endif

        log.Trace("trace log");
        log.Debug("debug log");
        log.Info("info => {}", Environment.CurrentDirectory);
        log.Warn("warn {0}", Environment.MachineName);
        log.Error("errrrr");
        log.Fatal("F!F!F!");
        log.Info("NET Version = {NetVersion}", Environment.Version);

        /*
        // test for buffering, should show up to 9999
        for (int i = 0; i < 10000; i++)
            log.Info("i = {NetVersion}", i);
        */
    }
#endif

#if SINGLE_LOGGER
    static void Main(string[] _)
    {
        Console.WriteLine("SingleLogger");
        var loggerFactory = LoggerFactory.Create(builder =>
        {
            builder.AddConsole();
        });

        using (ExecutionContext.SuppressFlow())
        {
            Task.Run(() =>
            {
                var logger1 = loggerFactory.CreateLogger("logger1");
                StaticLogManagerSetup.ConfigureForSingleThread(logger1);
                new TestClass();
            }).Wait();
            Task.Run(() =>
            {
                var logger2 = loggerFactory.CreateLogger("logger2");
                StaticLogManagerSetup.ConfigureForSingleThread(logger2);
                new TestClass();
            }).Wait();
        }

        loggerFactory.Dispose();
    }
#endif

#if SINGLE_FACTORY
    static void Main(string[] _)
    {
        var loggerFactory1 = LoggerFactory.Create(builder =>
        {
            builder.AddConsole();
        });
        var loggerFactory2 = LoggerFactory.Create(builder =>
        {
            builder.AddSimpleConsole(o => { o.SingleLine = true; o.IncludeScopes = true; });
        });

        using (ExecutionContext.SuppressFlow())
        {
            Task.Run(() =>
            {
                StaticLogManagerSetup.ConfigureForSingleThread(loggerFactory1);
                var t = new TestClass2();
                t.Go();
            }).Wait();
            Task.Run(() =>
            {
                StaticLogManagerSetup.ConfigureForSingleThread(loggerFactory2);
                new TestClass();
            }).Wait();
        }

        loggerFactory1.Dispose();
        loggerFactory2.Dispose();
    }
#endif

    private class TestClass
    {
        private static readonly ILogger log = StaticLogManager.GetLogger();

        public TestClass()
        {
            log.Info("created class");
        }
    }

    private class TestClass2
    {
        private static readonly ILogger log = StaticLogManager.GetLogger();

        public TestClass2()
        {
            log.Info("created class");
        }

        public void Go()
        {
            log.Warn("Go!");
        }
    }
}
