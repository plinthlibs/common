using Plinth.Security.Tokens;
using Plinth.Common.Utils;
using NUnit.Framework;

namespace Tests.Plinth.Security.Tokens;

[TestFixture]
public class AuthenticationTokenExtensionsTest
{
    private readonly byte[] _key = Convert.FromHexString("12345678901234567890123456789012");
    private readonly Guid _guid = Guid.Parse("e651d1e2-58af-4aba-bb3e-794c0d699146");
    private readonly TimeSpan _exp = TimeSpan.FromHours(1);

    public enum TestEnum
    {
        EnumName1,
        EnumName2,
        EnumName3
    }

    private class TestObject
    {
        public string? StringField { get; set; }
        public int IntField { get; set; }
        public bool BoolField { get; set; }
        public List<string>? ListOfStrings { get; set; }
        public List<TestObject>? ListOfObjs { get; set; }
    }

    [Test]
    public void Test_GetDataField_Scalars()
    {
        var token = new AuthenticationTokenBuilder(_key, _guid, "name", _exp)
            .WithData(new Dictionary<string, object?>() {
                ["stringparam"] = "value1",
                ["intparam"] = 5573,
                ["longparam"] = 287472657252525L,
                ["boolparamT"] = true,
                ["boolparamF"] = false,
                ["doubleparam"] = 1.7462
            }).Build();

        Assert.That(token.GetDataField<string>("stringparam"), Is.EqualTo("value1"));
        Assert.That(token.GetDataField<int>("intparam"), Is.EqualTo(5573));
        Assert.That(token.GetDataField<long>("longparam"), Is.EqualTo(287472657252525L));
        Assert.That(token.GetDataField<bool>("boolparamT"), Is.EqualTo(true));
        Assert.That(token.GetDataField<bool>("boolparamF"), Is.EqualTo(false));
        Assert.That(token.GetDataField<double>("doubleparam"), Is.EqualTo(1.7462));

        token = AuthenticationToken.FromEncryptedToken(_key, token.Token);

        Assert.That(token.GetDataField<string>("stringparam"), Is.EqualTo("value1"));
        Assert.That(token.GetDataField<int>("intparam"), Is.EqualTo(5573));
        Assert.That(token.GetDataField<long>("longparam"), Is.EqualTo(287472657252525L));
        Assert.That(token.GetDataField<bool>("boolparamT"), Is.EqualTo(true));
        Assert.That(token.GetDataField<bool>("boolparamF"), Is.EqualTo(false));
        Assert.That(token.GetDataField<double>("doubleparam"), Is.EqualTo(1.7462));
    }

    [Test]
    public void Test_GetDataField_Defaults()
    {
        var token = new AuthenticationTokenBuilder(_key, _guid, "name", _exp)
            .WithData(new Dictionary<string, object?>() {
                ["stringparam"] = "value1",
                ["intparam"] = 5573
            }).Build();

        Assert.That(token.GetDataField<string>("stringparam"), Is.EqualTo("value1"));
        Assert.That(token.GetDataField<int>("intparam"), Is.EqualTo(5573));

        Assert.That(token.GetDataField("notthere", "default"), Is.EqualTo("default"));
        Assert.That(token.GetDataField("notthere", 55), Is.EqualTo(55));
        Assert.That(token.GetDataField<List<string>>("notthere"), Is.Null);

        token = AuthenticationToken.FromEncryptedToken(_key, token.Token);

        Assert.That(token.GetDataField<string>("stringparam"), Is.EqualTo("value1"));
        Assert.That(token.GetDataField<int>("intparam"), Is.EqualTo(5573));

        Assert.That(token.GetDataField("notthere", "default"), Is.EqualTo("default"));
        Assert.That(token.GetDataField("notthere", 55), Is.EqualTo(55));
        Assert.That(token.GetDataField<List<string>>("notthere"), Is.Null);
    }

    [Test]
    public void Test_GetDataField_ThereButNull()
    {
        var token = new AuthenticationTokenBuilder(_key, _guid, "name", _exp)
            .WithData(new Dictionary<string, object?>() {
                ["stringparam"] = null,
                ["intparam"] = null,
                ["longparam"] = null,
                ["boolparamT"] = null,
                ["doubleparam"] = null,
                ["objparam"] = null,
                ["listparam"] = null
            }).Build();

        Assert.That(token.GetDataField<string>("stringparam"), Is.Null);
        Assert.That(token.GetDataField<int?>("intparam"), Is.Null);
        Assert.That(token.GetDataField<long?>("longparam"), Is.Null);
        Assert.That(token.GetDataField<bool?>("boolparamT"), Is.Null);
        Assert.That(token.GetDataField<bool?>("boolparamF"), Is.Null);
        Assert.That(token.GetDataField<double?>("doubleparam"), Is.Null);
        Assert.That(token.GetDataField<TestObject>("objparam"), Is.Null);
        Assert.That(token.GetDataField<List<string>>("listparam"), Is.Null);

        token = AuthenticationToken.FromEncryptedToken(_key, token.Token);

        Assert.That(token.GetDataField<string>("stringparam"), Is.Null);
        Assert.That(token.GetDataField<int?>("intparam"), Is.Null);
        Assert.That(token.GetDataField<long?>("longparam"), Is.Null);
        Assert.That(token.GetDataField<bool?>("boolparamT"), Is.Null);
        Assert.That(token.GetDataField<bool?>("boolparamF"), Is.Null);
        Assert.That(token.GetDataField<double?>("doubleparam"), Is.Null);
        Assert.That(token.GetDataField<TestObject>("objparam"), Is.Null);
        Assert.That(token.GetDataField<List<string>>("listparam"), Is.Null);
    }

    [Test]
    public void Test_TryGetDataField_Defaults()
    {
        var token = new AuthenticationTokenBuilder(_key, _guid, "name", _exp)
            .WithData(new Dictionary<string, object?>() {
                ["stringparam"] = "value1",
                ["intparam"] = 5573
            }).Build();

        Assert.That(token.TryGetDataField("stringparam", out string? s));
        Assert.That(s, Is.EqualTo("value1"));
        Assert.That(token.TryGetDataField("intparam", out int i));
        Assert.That(i, Is.EqualTo(5573));

        Assert.That(token.TryGetDataField<string>("notthere", out var _), Is.False);
        Assert.That(token.TryGetDataField<int>("notthere", out var _), Is.False);
        Assert.That(token.TryGetDataField<List<string>>("notthere", out var _), Is.False);

        token = AuthenticationToken.FromEncryptedToken(_key, token.Token);

        Assert.That(token.TryGetDataField("stringparam", out string? s2));
        Assert.That(s2, Is.EqualTo("value1"));
        Assert.That(token.TryGetDataField("intparam", out int i2));
        Assert.That(i2, Is.EqualTo(5573));

        Assert.That(token.TryGetDataField<string>("notthere", out var _), Is.False);
        Assert.That(token.TryGetDataField<int>("notthere", out var _), Is.False);
        Assert.That(token.TryGetDataField<List<string>>("notthere", out var _), Is.False);
    }

    [Test]
    public void Test_GetDataField_Objects()
    {
        var obj = new TestObject()
        {
            StringField = "str1",
            IntField = 48,
            BoolField = true,
            ListOfStrings = ["a", "b", "c"],
            ListOfObjs = [new TestObject() { IntField = 5 }, new TestObject() { StringField = "hello" }]
        };

        var token = new AuthenticationTokenBuilder(_key, _guid, "name", _exp)
            .WithData(new Dictionary<string, object?>() {
                ["obj"] = obj,
                ["enum"] = TestEnum.EnumName2,
                ["enumlist"] = new List<TestEnum>() { TestEnum.EnumName2, TestEnum.EnumName3 },
                ["intlist"] = new List<int>() { 1, 5, 10 },
                ["stringlist"] = new List<string>() { "10", "4", "2" },
                ["set"] = new HashSet<string>() { "d", "e", "f" },
                ["listofobj"] = obj.ListOfObjs
            }).Build();

        Assert.That(token.GetDataField<TestEnum>("enum"), Is.EqualTo(TestEnum.EnumName2));
        TestObject to = token.GetDataField<TestObject>("obj")!;
        Assert.That(to.StringField, Is.EqualTo("str1"));
        Assert.That(to.IntField, Is.EqualTo(48));
        Assert.That(to.BoolField, Is.EqualTo(true));
        Assert.That(to.ListOfStrings, Is.EqualTo(new List<string>() { "a", "b", "c" }).AsCollection);
        Assert.That(to.ListOfObjs!, Is.Not.Null);
        Assert.That(to.ListOfObjs, Has.Count.EqualTo(2));
        Assert.That(to.ListOfObjs![0].IntField, Is.EqualTo(5));
        Assert.That(to.ListOfObjs[1].StringField, Is.EqualTo("hello"));

        Assert.That(token.GetDataField<List<TestEnum>>("enumlist"), Is.EqualTo(new List<TestEnum>() { TestEnum.EnumName2, TestEnum.EnumName3 }).AsCollection);
        Assert.That(token.GetDataField<List<int>>("intlist"), Is.EqualTo(new List<int>() { 1, 5, 10 }).AsCollection);
        Assert.That(token.GetDataField<List<string>>("stringlist"), Is.EqualTo(new List<string>() { "10", "4", "2" }).AsCollection);
        Assert.That(token.GetDataField<HashSet<string>>("set"), Is.EquivalentTo(new HashSet<string>() { "d", "e", "f" }));
        var list = token.GetDataField<List<TestObject>>("listofobj");
        Assert.That(list, Has.Count.EqualTo(2));
        Assert.That(list![0].IntField, Is.EqualTo(5));
        Assert.That(list[1].StringField, Is.EqualTo("hello"));

        token = AuthenticationToken.FromEncryptedToken(_key, token.Token);

        Assert.That(token.GetDataField<TestEnum>("enum"), Is.EqualTo(TestEnum.EnumName2));
        to = token.GetDataField<TestObject>("obj")!;
        Assert.That(to.StringField, Is.EqualTo("str1"));
        Assert.That(to.IntField, Is.EqualTo(48));
        Assert.That(to.BoolField, Is.EqualTo(true));
        Assert.That(to.ListOfStrings, Is.EqualTo(new List<string>() { "a", "b", "c" }).AsCollection);
        Assert.That(to.ListOfObjs, Has.Count.EqualTo(2));
        Assert.That(to.ListOfObjs![0].IntField, Is.EqualTo(5));
        Assert.That(to.ListOfObjs[1].StringField, Is.EqualTo("hello"));

        Assert.That(token.GetDataField<List<TestEnum>>("enumlist"), Is.EqualTo(new List<TestEnum>() { TestEnum.EnumName2, TestEnum.EnumName3 }).AsCollection);
        Assert.That(token.GetDataField<List<int>>("intlist"), Is.EqualTo(new List<int>() { 1, 5, 10 }).AsCollection);
        Assert.That(token.GetDataField<List<string>>("stringlist"), Is.EqualTo(new List<string>() { "10", "4", "2" }).AsCollection);
        Assert.That(token.GetDataField<HashSet<string>>("set"), Is.EquivalentTo(new HashSet<string>() { "d", "e", "f" }));
        list = token.GetDataField<List<TestObject>>("listofobj");
        Assert.That(list, Has.Count.EqualTo(2));
        Assert.That(list![0].IntField, Is.EqualTo(5));
        Assert.That(list[1].StringField, Is.EqualTo("hello"));
    }

    [Test]
    public void Test_GetDataField_CannotConvert()
    {
        var obj = new TestObject()
        {
            StringField = "str1",
            IntField = 48,
            BoolField = true
        };

        var token = new AuthenticationTokenBuilder(_key, _guid, "name", _exp)
            .WithData(new Dictionary<string, object?>() {
                ["str"] = "value1",
                ["obj"] = obj
            }).Build();

        Assert.That(token.TryGetDataField<TestObject>("str", out var _), Is.False);
        Assert.That(token.TryGetDataField<string>("obj", out var _), Is.False);
    }

    [Test]
    public void Test_GetBase64Guid()
    {
        var token = new AuthenticationTokenBuilder(_key, _guid, "name", _exp)
            .WithData(new Dictionary<string, object?> {
                ["g1"] = AuthenticationToken.ConvertGuidToBase64(new Guid("4bd76c4d-b8a9-4bb5-96fe-5db2e767e8dd")),
                ["g2"] = "badguid"
            }
        ).Build();

        Assert.That(token.GetBase64Guid("g1"), Is.EqualTo(new Guid("4bd76c4d-b8a9-4bb5-96fe-5db2e767e8dd")));
        Assert.That(token.GetBase64Guid("not_found"), Is.Null);
        Assert.That(token.GetBase64Guid("g2"), Is.Null);
    }
}
