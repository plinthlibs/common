using Plinth.Security.Tokens;
using Plinth.Serialization;
using NUnit.Framework;

namespace Tests.Plinth.Security.Tokens;

[TestFixture]
public class TokenUtilTest
{
    [Test]
    public void TestGetDeserializedValue()
    {
        var d = new Dictionary<string, object?>() {
            { "stringparam", "value1"},
            { "intparam", 5573},
            { "longparam", 287472657252525L},
            { "boolparamT", true},
            { "boolparamF", false},
            { "doubleparam", 1.7462}
        };

        Assert.That(TokenUtil.GetDeserializedValue<string>(d, "stringparam"), Is.EqualTo("value1"));
        Assert.That(TokenUtil.GetDeserializedValue<int>(d, "intparam"), Is.EqualTo(5573));
        Assert.That(TokenUtil.GetDeserializedValue<long>(d, "longparam"), Is.EqualTo(287472657252525L));
        Assert.That(TokenUtil.GetDeserializedValue<bool>(d, "boolparamT"), Is.EqualTo(true));
        Assert.That(TokenUtil.GetDeserializedValue<bool>(d, "boolparamF"), Is.EqualTo(false));
        Assert.That(TokenUtil.GetDeserializedValue<double>(d, "doubleparam"), Is.EqualTo(1.7462));

        d = JsonUtil.DeserializeObject<Dictionary<string, object?>>(JsonUtil.SerializeObject(d)) ?? throw new System.Exception();

        Assert.That(TokenUtil.GetDeserializedValue<string>(d, "stringparam"), Is.EqualTo("value1"));
        Assert.That(TokenUtil.GetDeserializedValue<int>(d, "intparam"), Is.EqualTo(5573));
        Assert.That(TokenUtil.GetDeserializedValue<long>(d, "longparam"), Is.EqualTo(287472657252525L));
        Assert.That(TokenUtil.GetDeserializedValue<bool>(d, "boolparamT"), Is.EqualTo(true));
        Assert.That(TokenUtil.GetDeserializedValue<bool>(d, "boolparamF"), Is.EqualTo(false));
        Assert.That(TokenUtil.GetDeserializedValue<double>(d, "doubleparam"), Is.EqualTo(1.7462));
    }

    [Test]
    public void Test_GetDataField_Defaults()
    {
        var d = new Dictionary<string, object?>() {
            { "stringparam", "value1"},
            { "intparam", 5573}
        };

        Assert.That(TokenUtil.GetDeserializedValue<string>(d, "stringparam"), Is.EqualTo("value1"));
        Assert.That(TokenUtil.GetDeserializedValue<int>(d, "intparam"), Is.EqualTo(5573));

        Assert.That(TokenUtil.GetDeserializedValue<string>(d, "notthere", "default"), Is.EqualTo("default"));
        Assert.That(TokenUtil.GetDeserializedValue<int>(d, "notthere", 55), Is.EqualTo(55));
        Assert.That(TokenUtil.GetDeserializedValue<List<string>>(d, "notthere"), Is.Null);

        d = JsonUtil.DeserializeObject<Dictionary<string, object?>>(JsonUtil.SerializeObject(d)) ?? throw new System.Exception();

        Assert.That(TokenUtil.GetDeserializedValue<string>(d, "stringparam"), Is.EqualTo("value1"));
        Assert.That(TokenUtil.GetDeserializedValue<int>(d, "intparam"), Is.EqualTo(5573));

        Assert.That(TokenUtil.GetDeserializedValue<string>(d, "notthere", "default"), Is.EqualTo("default"));
        Assert.That(TokenUtil.GetDeserializedValue<int>(d, "notthere", 55), Is.EqualTo(55));
        Assert.That(TokenUtil.GetDeserializedValue<List<string>>(d, "notthere"), Is.Null);
    }

    [Test]
    public void Test_GetDataField_ThereButNull()
    {
        var d = new Dictionary<string, object?>() {
            { "stringparam", (string?)null},
            { "intparam", (int?)null},
            { "longparam", (long?)null},
            { "boolparamT", (bool?)null},
            { "doubleparam", (double?)null},
            { "objparam", (System.Text.RegularExpressions.Regex?)null},
            { "listparam", (List<string>?)null }
        };

        Assert.That(TokenUtil.GetDeserializedValue<string>(d, "stringparam"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<int?>(d, "intparam"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<long?>(d, "longparam"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<bool?>(d, "boolparamT"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<bool?>(d, "boolparamF"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<double?>(d, "doubleparam"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<System.Text.RegularExpressions.Regex>(d, "objparam"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<List<string>>(d, "listparam"), Is.Null);

        var d2 = (IDictionary<string, object?>)d;
        Assert.That(TokenUtil.GetDeserializedValue<string>(d2, "stringparam"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<int?>(d2, "intparam"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<long?>(d2, "longparam"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<bool?>(d2, "boolparamT"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<bool?>(d2, "boolparamF"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<double?>(d2, "doubleparam"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<System.Text.RegularExpressions.Regex>(d2, "objparam"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<List<string>>(d2, "listparam"), Is.Null);

        d = JsonUtil.DeserializeObject<Dictionary<string, object?>>(JsonUtil.SerializeObject(d)) ?? throw new System.Exception();

        Assert.That(TokenUtil.GetDeserializedValue<string>(d, "stringparam"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<int?>(d, "intparam"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<long?>(d, "longparam"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<bool?>(d, "boolparamT"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<bool?>(d, "boolparamF"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<double?>(d, "doubleparam"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<System.Text.RegularExpressions.Regex>(d, "objparam"), Is.Null);
        Assert.That(TokenUtil.GetDeserializedValue<List<string>>(d, "listparam"), Is.Null);
    }

    [Test]
    public void Test_TryGetDataField_Defaults()
    {
        var d = new Dictionary<string, object?>() {
            { "stringparam", "value1"},
            { "intparam", 5573 }
        };

        Assert.That(TokenUtil.TryGetDeserializedValue(d, "stringparam", out string? s));
        Assert.That(s, Is.EqualTo("value1"));
        Assert.That(TokenUtil.TryGetDeserializedValue(d, "intparam", out int i));
        Assert.That(i, Is.EqualTo(5573));

        Assert.That(TokenUtil.TryGetDeserializedValue<string>(d, "notthere", out var _), Is.False);
        Assert.That(TokenUtil.TryGetDeserializedValue<int>(d, "notthere", out var _), Is.False);
        Assert.That(TokenUtil.TryGetDeserializedValue<List<string>>(d, "notthere", out var _), Is.False);

        var d2 = (IDictionary<string, object?>)d;
        Assert.That(TokenUtil.TryGetDeserializedValue(d2, "stringparam", out s));
        Assert.That(s, Is.EqualTo("value1"));
        Assert.That(TokenUtil.TryGetDeserializedValue(d2, "intparam", out i));
        Assert.That(i, Is.EqualTo(5573));

        Assert.That(TokenUtil.TryGetDeserializedValue<string>(d, "notthere", out var _), Is.False);
        Assert.That(TokenUtil.TryGetDeserializedValue<int>(d, "notthere", out var _), Is.False);
        Assert.That(TokenUtil.TryGetDeserializedValue<List<string>>(d, "notthere", out var _), Is.False);

        d = JsonUtil.DeserializeObject<Dictionary<string, object?>>(JsonUtil.SerializeObject(d)) ?? throw new System.Exception();

        Assert.That(TokenUtil.TryGetDeserializedValue(d, "stringparam", out string? s2));
        Assert.That(s2, Is.EqualTo("value1"));
        Assert.That(TokenUtil.TryGetDeserializedValue(d, "intparam", out int i2));
        Assert.That(i2, Is.EqualTo(5573));

        Assert.That(TokenUtil.TryGetDeserializedValue<string>(d, "notthere", out var _), Is.False);
        Assert.That(TokenUtil.TryGetDeserializedValue<int>(d, "notthere", out var _), Is.False);
        Assert.That(TokenUtil.TryGetDeserializedValue<List<string>>(d, "notthere", out var _), Is.False);
    }

    public enum TestEnum
    {
        EnumName1,
        EnumName2,
        EnumName3
    }

    private class TestObject
    {
        public string? StringField { get; set; }
        public int IntField { get; set; }
        public bool BoolField { get; set; }
        public List<string>? ListOfStrings { get; set; }
        public List<TestObject>? ListOfObjs { get; set; }
    }

    [Test]
    public void Test_GetDataField_Objects()
    {
        var obj = new TestObject()
        {
            StringField = "str1",
            IntField = 48,
            BoolField = true,
            ListOfStrings = ["a", "b", "c"],
            ListOfObjs = [new TestObject() { IntField = 5 }, new TestObject() { StringField = "hello" }]
        };

        var d = new Dictionary<string, object?>() {
            { "obj", obj},
            { "enum", TestEnum.EnumName2},
            { "enumlist", new List<TestEnum>() { TestEnum.EnumName2, TestEnum.EnumName3 }},
            { "intlist", new List<int>() { 1, 5, 10 }},
            { "stringlist", new List<string>() { "10", "4", "2" }},
            { "set", new HashSet<string>() { "d", "e", "f" }},
            { "listofobj", obj.ListOfObjs }
        };

        Assert.That(TokenUtil.GetDeserializedValue<TestEnum>(d, "enum"), Is.EqualTo(TestEnum.EnumName2));
        var to = TokenUtil.GetDeserializedValue<TestObject>(d, "obj") ?? throw new System.Exception();
        Assert.That(to.StringField, Is.EqualTo("str1"));
        Assert.That(to.IntField, Is.EqualTo(48));
        Assert.That(to.BoolField, Is.EqualTo(true));
        Assert.That(to.ListOfStrings, Is.EqualTo(new List<string>() { "a", "b", "c" }).AsCollection);
        Assert.That(to.ListOfObjs?.Count, Is.EqualTo(2));
        Assert.That(to.ListOfObjs?[0].IntField, Is.EqualTo(5));
        Assert.That(to.ListOfObjs?[1].StringField, Is.EqualTo("hello"));

        Assert.That(TokenUtil.GetDeserializedValue<List<TestEnum>>(d, "enumlist"), Is.EqualTo(new List<TestEnum>() { TestEnum.EnumName2, TestEnum.EnumName3 }).AsCollection);
        Assert.That(TokenUtil.GetDeserializedValue<List<int>>(d, "intlist"), Is.EqualTo(new List<int>() { 1, 5, 10 }).AsCollection);
        Assert.That(TokenUtil.GetDeserializedValue<List<string>>(d, "stringlist"), Is.EqualTo(new List<string>() { "10", "4", "2" }).AsCollection);
        Assert.That(TokenUtil.GetDeserializedValue<HashSet<string>>(d, "set"), Is.EquivalentTo(new HashSet<string>() { "d", "e", "f" }));
        var list = TokenUtil.GetDeserializedValue<List<TestObject>>(d, "listofobj");
        Assert.That(list?.Count, Is.EqualTo(2));
        Assert.That(list?[0].IntField, Is.EqualTo(5));
        Assert.That(list?[1].StringField, Is.EqualTo("hello"));

        d = JsonUtil.DeserializeObject<Dictionary<string, object?>>(JsonUtil.SerializeObject(d)) ?? throw new System.Exception();

        Assert.That(TokenUtil.GetDeserializedValue<TestEnum>(d, "enum"), Is.EqualTo(TestEnum.EnumName2));
        to = TokenUtil.GetDeserializedValue<TestObject>(d, "obj") ?? throw new System.Exception();
        Assert.That(to.StringField, Is.EqualTo("str1"));
        Assert.That(to.IntField, Is.EqualTo(48));
        Assert.That(to.BoolField, Is.EqualTo(true));
        Assert.That(to.ListOfStrings, Is.EqualTo(new List<string>() { "a", "b", "c" }).AsCollection);
        Assert.That(to.ListOfObjs?.Count, Is.EqualTo(2));
        Assert.That(to.ListOfObjs?[0].IntField, Is.EqualTo(5));
        Assert.That(to.ListOfObjs?[1].StringField, Is.EqualTo("hello"));

        Assert.That(TokenUtil.GetDeserializedValue<List<TestEnum>>(d, "enumlist"), Is.EqualTo(new List<TestEnum>() { TestEnum.EnumName2, TestEnum.EnumName3 }).AsCollection);
        Assert.That(TokenUtil.GetDeserializedValue<List<int>>(d, "intlist"), Is.EqualTo(new List<int>() { 1, 5, 10 }).AsCollection);
        Assert.That(TokenUtil.GetDeserializedValue<List<string>>(d, "stringlist"), Is.EqualTo(new List<string>() { "10", "4", "2" }).AsCollection);
        Assert.That(TokenUtil.GetDeserializedValue<HashSet<string>>(d, "set"), Is.EquivalentTo(new HashSet<string>() { "d", "e", "f" }));
        list = TokenUtil.GetDeserializedValue<List<TestObject>>(d, "listofobj");
        Assert.That(list?.Count, Is.EqualTo(2));
        Assert.That(list?[0].IntField, Is.EqualTo(5));
        Assert.That(list?[1].StringField, Is.EqualTo("hello"));
    }

    [Test]
    public void Test_GetDataField_CannotConvert()
    {
        var obj = new TestObject()
        {
            StringField = "str1",
            IntField = 48,
            BoolField = true
        };

        var d = new Dictionary<string, object?>() {
            { "str", "value1"},
            { "obj", obj }
        };

        Assert.That(TokenUtil.TryGetDeserializedValue<TestObject>(d, "str", out var _), Is.False);
        Assert.That(TokenUtil.TryGetDeserializedValue<string>(d, "obj", out var _), Is.False);
    }
}
