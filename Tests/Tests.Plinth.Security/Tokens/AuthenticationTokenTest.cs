using Plinth.Security.Crypto;
using Plinth.Security.Tokens;
using NUnit.Framework;
using System.Text;

#if !NET9_0_OR_GREATER
using Convert = Plinth.Common.Utils.BackPort.PlinthConvert;
#endif

namespace Tests.Plinth.Security.Tokens;

[TestFixture]
public class AuthenticationTokenTest
{
    private readonly byte[] _key = Convert.FromHexString("12345678901234567890123456789012");

    [Test]
    public void Test_Create()
    {
        var guid = new Guid("e651d1e2-58af-4aba-bb3e-794c0d699146");
        var token = new AuthenticationToken(_key, null, guid, "name", TimeSpan.FromHours(1), true);

        Assert.That(token.SubjectGuid, Is.EqualTo(guid));
        Assert.That(token.SubjectName, Is.EqualTo("name"));
        Assert.That(DateTime.UtcNow - token.Issued, Is.LessThan(TimeSpan.FromSeconds(2)));
        Assert.That(token.Expires - DateTime.UtcNow - TimeSpan.FromHours(1), Is.LessThan(TimeSpan.FromSeconds(2)));
        Assert.That(token.IsExpired, Is.False);
        Assert.That(token.Token, Is.Not.EqualTo(guid.ToString()));
        Assert.That(token.Roles, Is.Null);
        Assert.That(token.Data, Is.Null);
        Assert.That(token.SessionGuid, Is.Not.EqualTo(guid));
        Assert.That(token.OriginalIssued, Is.EqualTo(token.Issued));

        var sessGuid = new Guid("aaa1d1e2-58af-4aba-bb3e-794c0d699146");
        var ori = DateTimeOffset.UtcNow.AddDays(-1);
        token = new AuthenticationToken(_key, null, guid, "name", TimeSpan.FromHours(1), true, ori, sessGuid);

        AssertTokenRequired(guid, token);
        Assert.That(token.Token, Is.Not.EqualTo(guid.ToString()));
        Assert.That(token.Data, Is.Null);
        Assert.That(token.Roles, Is.Null);
        Assert.That(token.SessionGuid, Is.EqualTo(sessGuid));
        Assert.That(token.OriginalIssued.ToUnixTimeSeconds(), Is.EqualTo(ori.ToUnixTimeSeconds()));
    }

    [Test]
    public void Test_Create_Builder()
    {
        var guid = new Guid("e651d1e2-58af-4aba-bb3e-794c0d699146");
        var sessGuid = new Guid("aaa1d1e2-58af-4aba-bb3e-794c0d699146");
        var ori = DateTimeOffset.UtcNow.AddDays(-1);
        var token = new AuthenticationTokenBuilder(_key)
            .WithSubject(guid, "name")
            .WithLifetime(TimeSpan.FromHours(1))
            .WithSessionGuid(sessGuid)
            .WithOriginalIssue(ori)
            .WithRoles("user", "admin")
            .WithData(new Dictionary<string, object?> { ["a"] = 5, ["b"] = "hello" })
            .Build();

        //Console.WriteLine(token.Token);

        AssertTokenRequired(guid, token);
        Assert.That(token.Token, Is.Not.EqualTo(guid.ToString()));
        Assert.That(token.Roles, Is.EqualTo(new[] { "user", "admin" }).AsCollection);
        Assert.That(token.Data, Is.EqualTo(new Dictionary<string, object> { ["a"] = 5, ["b"] = "hello" }).AsCollection);
        Assert.That(token.SessionGuid, Is.EqualTo(sessGuid));
        Assert.That(token.OriginalIssued.ToUnixTimeSeconds(), Is.EqualTo(ori.ToUnixTimeSeconds()));
    }

    [Test]
    public void Test_Create_Builder1()
    {
        var guid = new Guid("e651d1e2-58af-4aba-bb3e-794c0d699146");
        var token = new AuthenticationTokenBuilder(_key, guid, "name", TimeSpan.FromHours(1)).Build();

        AssertTokenRequired(guid, token);
    }

    [Test]
    public void Test_Create_Builder2()
    {
        var guid = new Guid("e651d1e2-58af-4aba-bb3e-794c0d699146");
        var token = new AuthenticationTokenBuilder(Convert.ToHexStringLower(_key), guid, "name", TimeSpan.FromHours(1)).Build();

        AssertTokenRequired(guid, token);
    }

    [Test]
    public void Test_Create_Builder3()
    {
        var guid = new Guid("e651d1e2-58af-4aba-bb3e-794c0d699146");
        var token = new AuthenticationTokenBuilder(_key)
            .WithSubjectGuid(guid)
            .WithSubjectName("name")
            .WithLifetime(TimeSpan.FromHours(1)).Build();

        AssertTokenRequired(guid, token);
    }

    [Test]
    public void Test_Create_Builder4()
    {
        var guid = new Guid("e651d1e2-58af-4aba-bb3e-794c0d699146");
        var token = new AuthenticationTokenBuilder(Convert.ToHexStringLower(_key))
            .WithSubjectGuid(guid)
            .WithSubjectName("name")
            .WithLifetime(TimeSpan.FromHours(1)).Build();

        AssertTokenRequired(guid, token);
    }

    [Test]
    public void Test_Create_Builder5()
    {
        var guid = new Guid("e651d1e2-58af-4aba-bb3e-794c0d699146");
        var token = new AuthenticationTokenBuilder()
            .WithMasterKey(Convert.ToHexStringLower(_key))
            .WithSubjectGuid(guid)
            .WithSubjectName("name")
            .WithLifetime(TimeSpan.FromHours(1)).Build();

        AssertTokenRequired(guid, token);
    }

    [Test]
    public void Test_Create_Builder6_OriginalAsDateTime()
    {
        var guid = new Guid("e651d1e2-58af-4aba-bb3e-794c0d699146");
        var ori = DateTime.UtcNow.Subtract(TimeSpan.FromDays(1));
        var token = new AuthenticationTokenBuilder()
            .WithMasterKey(Convert.ToHexStringLower(_key))
            .WithSubjectGuid(guid)
            .WithSubjectName("name")
            .WithLifetime(TimeSpan.FromHours(1))
            .WithOriginalIssue(ori)
            .Build();

        AssertTokenRequired(guid, token);
        Assert.That(token.OriginalIssued, Is.EqualTo((DateTimeOffset)ori));
    }

    [Test]
    public void Test_Create_Builder7_Roles()
    {
        var guid = new Guid("e651d1e2-58af-4aba-bb3e-794c0d699146");
        var token = new AuthenticationTokenBuilder(_key, guid, "name", TimeSpan.FromHours(1))
            .WithRoles("user", "admin")
            .Build();

        AssertTokenRequired(guid, token);
        Assert.That(token.Roles, Is.EqualTo(new[] { "user", "admin" }).AsCollection);
        token = AuthenticationToken.FromEncryptedToken(_key, token.Token);
        Assert.That(token.Roles, Is.EqualTo(new[] { "user", "admin" }).AsCollection);

        token = new AuthenticationTokenBuilder(_key, guid, "name", TimeSpan.FromHours(1))
            .WithRoles(new List<string> { "user", "admin" })
            .Build();

        AssertTokenRequired(guid, token);
        Assert.That(token.Roles, Is.EqualTo(new[] { "user", "admin" }).AsCollection);
        token = AuthenticationToken.FromEncryptedToken(_key, token.Token);
        Assert.That(token.Roles, Is.EqualTo(new[] { "user", "admin" }).AsCollection);

        token = new AuthenticationTokenBuilder(_key, guid, "name", TimeSpan.FromHours(1))
            .WithRoles(new HashSet<string> { "user", "admin" })
            .Build();

        AssertTokenRequired(guid, token);
        Assert.That(token.Roles, Is.EqualTo(new[] { "user", "admin" }).AsCollection);
        token = AuthenticationToken.FromEncryptedToken(_key, token.Token);
        Assert.That(token.Roles, Is.EqualTo(new[] { "user", "admin" }).AsCollection);
    }

    [Test]
    public void Test_Create_Builder8_SecureData()
    {
        var guid = new Guid("e651d1e2-58af-4aba-bb3e-794c0d699146");
        var sd = new SecureData(Convert.ToHexStringLower(_key));
        var token = new AuthenticationTokenBuilder(sd, guid, "name", TimeSpan.FromHours(1))
            .Build();

        AssertTokenRequired(guid, token);

        var tk2 = AuthenticationToken.FromEncryptedToken(sd, token.Token);
        AssertTokenRequired(guid, tk2);

        token = new AuthenticationTokenBuilder().WithSecureData(sd)
            .WithSubject(guid, "name").WithLifetime(TimeSpan.FromHours(1))
            .Build();

        AssertTokenRequired(guid, token);
    }

    private static void AssertTokenRequired(Guid guid, AuthenticationToken token)
    {
        Assert.That(token.SubjectGuid, Is.EqualTo(guid));
        Assert.That(token.SubjectName, Is.EqualTo("name"));
        Assert.That(DateTime.UtcNow - token.Issued, Is.LessThan(TimeSpan.FromSeconds(2)));
        Assert.That(token.Expires - DateTime.UtcNow - TimeSpan.FromHours(1), Is.LessThan(TimeSpan.FromSeconds(2)));
        Assert.That(token.IsExpired, Is.False);
    }

    [Test]
    public void Test_Decrypt()
    {
        var guid = new Guid("e651d1e2-58af-4aba-bb3e-794c0d699146");
        var origToken = new AuthenticationToken(_key, null, guid, "name", TimeSpan.FromHours(1), true);

        var token = AuthenticationToken.FromEncryptedToken(_key, origToken.Token);

        Assert.That(token.SubjectGuid, Is.EqualTo(guid));
        Assert.That(token.SubjectName, Is.EqualTo("name"));
        Assert.That(token.Token, Is.EqualTo(origToken.Token));
        Assert.That(DateTime.UtcNow - token.Issued, Is.LessThan(TimeSpan.FromSeconds(2)));
        Assert.That(token.Expires - DateTime.UtcNow - TimeSpan.FromHours(1), Is.LessThan(TimeSpan.FromSeconds(2)));
        Assert.That(token.IsExpired, Is.False);
        Assert.That(token.Roles, Is.Null);
        Assert.That(token.SessionGuid, Is.Not.EqualTo(guid));
        Assert.That(token.OriginalIssued.ToUnixTimeSeconds(), Is.EqualTo(token.Issued.ToUnixTimeSeconds()));

        var sessGuid = new Guid("aaa1d1e2-58af-4aba-bb3e-794c0d699146");
        var ori = DateTimeOffset.UtcNow.AddDays(-1);
        origToken = new AuthenticationToken(_key, null, guid, "name", TimeSpan.FromHours(1), true, ori, sessGuid, ["user", "admin"]);

        token = AuthenticationToken.FromEncryptedToken(_key, origToken.Token);

        Assert.That(token.SubjectGuid, Is.EqualTo(guid));
        Assert.That(token.SubjectName, Is.EqualTo("name"));
        Assert.That(token.Token, Is.EqualTo(origToken.Token));
        Assert.That(DateTime.UtcNow - token.Issued, Is.LessThan(TimeSpan.FromSeconds(2)));
        Assert.That(token.Expires - DateTime.UtcNow - TimeSpan.FromHours(1), Is.LessThan(TimeSpan.FromSeconds(2)));
        Assert.That(token.IsExpired, Is.False);
        Assert.That(token.Roles, Is.EqualTo(new[] { "user", "admin" }).AsCollection);
        Assert.That(token.SessionGuid, Is.Not.EqualTo(guid));
        Assert.That(token.OriginalIssued.ToUnixTimeSeconds(), Is.EqualTo(ori.ToUnixTimeSeconds()));
        Assert.That(token.SessionGuid, Is.EqualTo(sessGuid));
        Assert.That(token.OriginalIssued.ToUnixTimeSeconds(), Is.EqualTo(ori.ToUnixTimeSeconds()));
    }

    [Test]
    public void Test_Invalid_Decrypt()
    {
        var guid = new Guid("e651d1e2-58af-4aba-bb3e-794c0d699146");
        var token = new AuthenticationToken(_key, null, guid, "name", TimeSpan.FromHours(1), true);

        var tokenContents = new StringBuilder(token.Token);
        tokenContents[12] = tokenContents[12] == 'a' ? 'b' : 'a';

        Assert.Throws<InvalidTokenException>(() => AuthenticationToken.FromEncryptedToken(_key, tokenContents.ToString()));
    }

    [Test]
    public void Test_Wrong_Key()
    {
        var guid = new Guid("e651d1e2-58af-4aba-bb3e-794c0d699146");
        var token = new AuthenticationToken(_key, null, guid, "name", TimeSpan.FromHours(1), true);

        var key2 = Convert.FromHexString("55555555555555555555555555555555");

        Assert.Throws<InvalidTokenException>(() => AuthenticationToken.FromEncryptedToken(key2, token.Token));
    }

    [Test]
    public void Test_Expired()
    {
        var guid = new Guid("e651d1e2-58af-4aba-bb3e-794c0d699146");
        var origToken = new AuthenticationToken(_key, null, guid, "name", TimeSpan.FromMilliseconds(1), true);

        System.Threading.Thread.Sleep(20);

        var token = AuthenticationToken.FromEncryptedToken(_key, origToken.Token, throwOnExpired:false);

        Assert.That(token.IsExpired);

        Assert.Throws<ExpiredTokenException>(() => AuthenticationToken.FromEncryptedToken(_key, origToken.Token, throwOnExpired:true));
    }

    [Test]
    public void Test_Data()
    {
        var guid = new Guid("e651d1e2-58af-4aba-bb3e-794c0d699146");
        AuthenticationToken? token = null;
        var data = new Dictionary<string, object?>()
        {
            { "test", "value" },
            { "test2", "value2" }
        };

        token = new AuthenticationTokenBuilder(_key, guid, "name", TimeSpan.FromHours(1)).WithData(data).Build();

        Assert.That(token.SubjectGuid, Is.EqualTo(guid));
        Assert.That(DateTime.UtcNow - token.Issued, Is.LessThan(TimeSpan.FromSeconds(2)));
        Assert.That(token.Expires - DateTime.UtcNow - TimeSpan.FromHours(1), Is.LessThan(TimeSpan.FromSeconds(2)));
        Assert.That(token.IsExpired, Is.False);
        Assert.That(token.Token, Is.Not.EqualTo(guid.ToString()));
        Assert.That(token.Data, Is.Not.Null);
        Assert.That(token.Data, Is.EquivalentTo(data));

        token = AuthenticationToken.FromEncryptedToken(_key, token.Token);

        Assert.That(token.SubjectGuid, Is.EqualTo(guid));
        Assert.That(token.Data, Is.EquivalentTo(data));

        token = new AuthenticationTokenBuilder(_key, guid, "name", TimeSpan.FromHours(1)).WithData(
            (IEnumerable<KeyValuePair<string,object?>>)data).Build();
        Assert.That(token.Data, Is.EquivalentTo(data));
        token = AuthenticationToken.FromEncryptedToken(_key, token.Token);
        Assert.That(token.Data, Is.EquivalentTo(data));

        token = new AuthenticationTokenBuilder(_key, guid, "name", TimeSpan.FromHours(1)).WithData(
            data.Select(kv => Tuple.Create(kv.Key, kv.Value))).Build();
        Assert.That(token.Data, Is.EquivalentTo(data));
        token = AuthenticationToken.FromEncryptedToken(_key, token.Token);
        Assert.That(token.Data, Is.EquivalentTo(data));

        token = new AuthenticationTokenBuilder(_key, guid, "name", TimeSpan.FromHours(1)).WithData(
            Tuple.Create<string,object?>("test", "value"), Tuple.Create<string,object?>("test2", "value2")).Build();
        Assert.That(token.Data, Is.EquivalentTo(data));
        token = AuthenticationToken.FromEncryptedToken(_key, token.Token);
        Assert.That(token.Data, Is.EquivalentTo(data));

        token = new AuthenticationTokenBuilder(_key, guid, "name", TimeSpan.FromHours(1)).WithData(
            new KeyValuePair<string,object?>("test", "value"), new KeyValuePair<string,object?>("test2", "value2")).Build();
        Assert.That(token.Data, Is.EquivalentTo(data));
        token = AuthenticationToken.FromEncryptedToken(_key, token.Token);
        Assert.That(token.Data, Is.EquivalentTo(data));
    }

    [Test]
    public void Test_Data_Null()
    {
        var guid = new Guid("e651d1e2-58af-4aba-bb3e-794c0d699146");
        AuthenticationToken token;

        token = new AuthenticationTokenBuilder(_key, guid, "name", TimeSpan.FromHours(1)).WithData((IDictionary<string,object?>?)null).Build();
        Assert.That(token.Data, Is.Null);
        token = AuthenticationToken.FromEncryptedToken(_key, token.Token);
        Assert.That(token.Data, Is.Null);

        token = new AuthenticationTokenBuilder(_key, guid, "name", TimeSpan.FromHours(1)).WithData((IEnumerable<KeyValuePair<string,object?>>?)null).Build();
        Assert.That(token.Data, Is.Null);
        token = AuthenticationToken.FromEncryptedToken(_key, token.Token);
        Assert.That(token.Data, Is.Null);

        token = new AuthenticationTokenBuilder(_key, guid, "name", TimeSpan.FromHours(1)).WithData((IEnumerable<Tuple<string,object?>>?)null).Build();
        Assert.That(token.Data, Is.Null);
        token = AuthenticationToken.FromEncryptedToken(_key, token.Token);
        Assert.That(token.Data, Is.Null);
    }

    [Test]
    [TestCase("4bd76c4d-b8a9-4bb5-96fe-5db2e767e8dd", "TWzXS6m4tUuW/l2y52fo3Q==")]
    [TestCase("87da1d77-6bb0-4f30-8722-7104b6b465a9", "dx3ah7BrME+HInEEtrRlqQ==")]
    public void Test_ConvertBase64Guid(string guid, string base64)
    {
        Assert.That(AuthenticationToken.ConvertGuidToBase64(new Guid(guid)), Is.EqualTo(base64));
    }
}
