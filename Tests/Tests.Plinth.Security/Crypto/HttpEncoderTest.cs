using Plinth.Security.Crypto;
using NUnit.Framework;

#if !NET9_0_OR_GREATER
using Convert = Plinth.Common.Utils.BackPort.PlinthConvert;
#endif

namespace Tests.Plinth.Security.Crypto;

[TestFixture]
class HttpEncoderTest
{
#if NET9_0_OR_GREATER
    // test legacy encoded tokens
    [TestCase("ESg_Vm2Em7LJ4PcO0", "11283f566d849bb2c9e0f70e")]
    [TestCase("Bx41", "071e")]
    [TestCase("EilAV26FnLPK4fgPJg2", "122940576e859cb3cae1f80f26")]
    [TestCase("EypBWG-GnbTL4vkQJz41", "132a41586f869db4cbe2f910273e")]
    public void TestDecodeLegacy(string token, string hex)
    {
        var legacyDecode = HttpEncoder.UrlTokenDecode(token);
        Assert.That(Convert.ToHexStringLower(legacyDecode), Is.EqualTo(hex));

        var legacyEncode = HttpEncoder.UrlTokenEncode(legacyDecode);
        Assert.That(legacyEncode, Is.EqualTo(token));
    }

    // this is new for net9, invalid padding throws rather than returns null
    [TestCase("ESg_Vm2Em7LJ4PcO/")]
    [TestCase("ESg_Vm2Em7LJ4PcO4")]
    [TestCase("ESg_Vm2Em7LJ4PcO:")]
    public void TestDecodeLegacyInvalidPadding(string value)
    {
        Assert.Throws<FormatException>(() => HttpEncoder.UrlTokenDecode(value));
    }
#else
    [TestCase("ESg_Vm2Em7LJ4PcO0", "11283f566d849bb2c9e0f70e")]
    [TestCase("Bx41", "071e")]
    [TestCase("EilAV26FnLPK4fgPJg2", "122940576e859cb3cae1f80f26")]
    [TestCase("EypBWG-GnbTL4vkQJz41", "132a41586f869db4cbe2f910273e")]
    public void TestDecodeLegacy(string token, string hex)
    {
        var legacyDecode = HttpEncoder.UrlTokenDecode(token);
        Assert.That(global::Plinth.Common.Utils.HexUtil.ToHex(legacyDecode), Is.EqualTo(hex));

        var legacyEncode = HttpEncoder.UrlTokenEncode(legacyDecode);
        Assert.That(legacyEncode, Is.EqualTo(token));
    }
#endif

    [Test]
    public void DataOfVariousLengthRoundTripCorrectly()
    {
        Parallel.For(0, 1100, length =>
        {
            var data = new byte[length];
            for (int index = 0; index != length; ++index)
            {
                data[index] = (byte)(5 + length + (index * 23));
            }
            string text = HttpEncoder.UrlTokenEncode(data);
            byte[] result = HttpEncoder.UrlTokenDecode(text);

            Assert.That(result, Is.EquivalentTo(data));
        });
    }

    [Test]
    public void Test_NoPlusesSlashesOrEquals()
    {
        var hex = "0207000024000000535232410400000000010001550d4d17bc084a8cebef70f9d2184a2df26d0611417ccd6e2fc6582b51f354707677dfd86bd1f11d7e016b597e2b863979841b139e6324e2a50c7e2d1402de2480ccd4f49e0c55333ec2f5a935169890ff021624450297c29a762391837f434a546e2cb0bd1c0a0bc8833a5f3e780b54f76b8ae24f7c4c3008514e5aade39479d2ed5140761b411f3611c6cd0d419476a58b2d22336e86576e002731f15ed9d676a5a57a9629bf3fbf68789065e2efee3d5f6ecd27fcfbfde3641849b0bfcf32a6a1a2218909d4c983bc656ed03d49b98f72ebb03744269f18d1ae701238d301d527eb523b15eba7c3c68d76dfffe392a48fc0636311dcab99fc73a6378fb7723239e22175b0290b049d8e761e78ec263bd772d2f7eddb56143bc88f4ffdd4c832186d96f59596e3dbd0287e81c23504547ad088cda649919210eec08b6336dd63616e018fb98c1da1b82cf464f1346eca8b9c766ee5e35fb5ca84b582aba08fa44101a24bb55ccadc2941468beded8db255956c0ca5bbfd054b26bfc53c849bdaaf8e4d82ee057ae7aa5b8f1655bd6f3624183a11860a12b110c8f1c8339b4bb9dc87142de50196df36592ee46bf6078021e707e14e9a7310c558e9fac447edde810008c9e1b55ec0f4db2159424ba2823c892e3406461f78eacb20c6064388fd8dfda7680f9c8b71c76dbccd21d571a3d0dbf4f46fe9fffe1081cf6a1414ddcc7b2fc777f357ff4347fd597d001013664f47aad10cb94bf1604b7c957bc3e11eef17217f6d085862199e8dfdc7507f2b3920d262bc1ca1f97afda8447e240e";
        var bytes = Convert.FromHexString(hex);

        var normal = Convert.ToBase64String(bytes);
        Assert.That(normal, Does.Contain('/'));
        Assert.That(normal, Does.Contain('+'));
        Assert.That(normal, Does.Contain('='));
        //Console.WriteLine(normal);

        var url = HttpEncoder.UrlTokenEncode(bytes);
        Assert.That(url, Does.Not.Contain('/'));
        Assert.That(url, Does.Not.Contain('+'));
        Assert.That(url, Does.Not.Contain('='));
        //Console.WriteLine(url);

        Assert.That(Convert.ToHexStringLower(HttpEncoder.UrlTokenDecode(url)!), Is.EqualTo(hex));
    }
}
