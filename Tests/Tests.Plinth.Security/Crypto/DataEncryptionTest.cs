using Plinth.Security.Crypto;
using NUnit.Framework;

#if !NET9_0_OR_GREATER
using Convert = Plinth.Common.Utils.BackPort.PlinthConvert;
#endif

namespace Tests.Plinth.Security.Crypto;

[TestFixture]
public class DataEncryptionTest
{
    private const string _key128 = "12345678901234567890123456789012";
    private const string _key192 = "123456789012345678901234567890121234567890123456";
    private const string _key256 = "1234567890123456789012345678901212345678901234567890123456789012";

    [Test]
    [TestCase(_key128)]
    [TestCase(_key192)]
    [TestCase(_key256)]
    public void Test_Encrypt_Bytes(string key)
    {
        byte[] data = Primitives.GetSecureRandomBytes(100000);

        byte[] result = DataEncryption.Encrypt(Convert.FromHexString(key), 0, data);

        byte[]? roundTrip = DataEncryption.Decrypt(Convert.FromHexString(key), result);

        Assert.That(roundTrip, Is.EqualTo(data).AsCollection);

        // encrypt again, different result
        byte[] result_again = DataEncryption.Encrypt(Convert.FromHexString(key), 0, data);
        Assert.That(result_again, Is.Not.EqualTo(result).AsCollection);
    }

    [Test]
    [TestCase(_key128)]
    [TestCase(_key192)]
    [TestCase(_key256)]
    public void Test_Encrypt_Url(string key)
    {
        byte[] data = Primitives.GetSecureRandomBytes(100000);

        string result = DataEncryption.EncryptToUrlSafeToken(Convert.FromHexString(key), 0, data);

        byte[]? roundTrip = DataEncryption.DecryptUrlSafeToken(Convert.FromHexString(key), result);

        Assert.That(roundTrip, Is.EqualTo(data).AsCollection);

        // encrypt again, different result
        string result_again = DataEncryption.EncryptToUrlSafeToken(Convert.FromHexString(key), 0, data);
        Assert.That(result_again, Is.Not.EqualTo(result).AsCollection);
    }

    [Test]
    [TestCase(_key128)]
    [TestCase(_key192)]
    [TestCase(_key256)]
    public void Test_Null_Inputs(string key)
    {
        Assert.Throws<ArgumentNullException>(() => DataEncryption.Encrypt(Convert.FromHexString(key), 0, null!));
        Assert.Throws<ArgumentNullException>(() => DataEncryption.Decrypt((byte[])null!, null));
        Assert.Throws<ArgumentNullException>(() => DataEncryption.Decrypt((byte[])null!, Primitives.GetSecureRandomBytes(10)));

        Assert.Throws<ArgumentNullException>(() => DataEncryption.Encrypt(null!, 0, null!));
        Assert.Throws<ArgumentNullException>(() => DataEncryption.Decrypt((Dictionary<byte, byte[]>)null!, null!));
        Assert.Throws<ArgumentNullException>(() => DataEncryption.Decrypt((Dictionary<byte, byte[]>)null!, Primitives.GetSecureRandomBytes(10)));

        Assert.That(DataEncryption.Decrypt(new Dictionary<byte, byte[]> { [0] = Primitives.GetSecureRandomBytes(16) }, null!), Is.Null);
        Assert.That(DataEncryption.Decrypt(Convert.FromHexString(key), null), Is.Null);

        Assert.That(DataEncryption.Encrypt(Convert.FromHexString(key), 0, []), Is.Not.Null);
    }

    [Test]
    [TestCase(_key128)]
    [TestCase(_key192)]
    [TestCase(_key256)]
    public void Test_Empty_Input(string key)
    {
        byte[] data = [];

        byte[] result = DataEncryption.Encrypt(Convert.FromHexString(key), 0, data);

        byte[]? roundTrip = DataEncryption.Decrypt(Convert.FromHexString(key), result);

        Assert.That(roundTrip, Is.EqualTo(data).AsCollection);

        // encrypt again, different result
        byte[] result_again = DataEncryption.Encrypt(Convert.FromHexString(key), 0, data);
        Assert.That(result_again, Is.Not.EqualTo(result).AsCollection);
    }


    [Test]
    [TestCase(_key128)]
    [TestCase(_key192)]
    [TestCase(_key256)]
    public void Test_Encrypt_Invalid(string key)
    {
        Assert.Throws<ArgumentException>(() =>
           DataEncryption.Encrypt(Primitives.GetSecureRandomBytes(2), 0, Primitives.GetSecureRandomBytes(10)));
        Assert.Throws<ArgumentException>(() =>
           DataEncryption.EncryptToUrlSafeToken(Primitives.GetSecureRandomBytes(2), 0, Primitives.GetSecureRandomBytes(10)));

        Assert.That(DataEncryption.Decrypt(Convert.FromHexString(key), Primitives.GetSecureRandomBytes(10)), Is.Null);
        Assert.That(DataEncryption.DecryptUrlSafeToken(Convert.FromHexString(key), "abcd1234"), Is.Null);

        // invalid version
        var cipher = Primitives.GetSecureRandomBytes(65);
        cipher[0] = 0xFF;
        Assert.That(DataEncryption.Decrypt(Convert.FromHexString(key), cipher), Is.Null);

        // invalid hmac
        cipher = Primitives.GetSecureRandomBytes(65);
        cipher[0] = 0x1;
        Assert.That(DataEncryption.Decrypt(Convert.FromHexString(key), cipher), Is.Null);

        // unspecified key
        cipher = DataEncryption.Encrypt(Primitives.GetSecureRandomBytes(16), 7, Primitives.GetSecureRandomBytes(10));
        Assert.That(DataEncryption.Decrypt(new Dictionary<byte, byte[]> { [0] = Convert.FromHexString(key) }, cipher), Is.Null);
    }

    [Test]
    public void Test_Decrypt_V2_Before192_256_support()
    {
        // encrypted using old code
        byte[] v2_128 = Convert.FromHexString("0246fc38298263b605971a23ce2e192a66d6fc6a46b424db4c3d907083b701be9c79768ccbba7d4082dc0c6723575edf60f3681b3c87236ef7b7e4fe43dd4cba21");

        byte[]? pt1 = DataEncryption.Decrypt(Convert.FromHexString(_key128), v2_128);
        Assert.That(Convert.ToHexStringLower(pt1!), Is.EqualTo("11223344"));

        // test keyring/legacy mode
        var keyRing = new Dictionary<byte, byte[]> { [0] = Convert.FromHexString(_key128) };
        pt1 = DataEncryption.Decrypt(keyRing, v2_128);
        Assert.That(Convert.ToHexStringLower(pt1!), Is.EqualTo("11223344"));
    }

    [Test]
    public void Test_Decrypt_V1()
    {
        // encrypted using old code
        byte[] v1 = Convert.FromHexString("019d921afba6ccca86993bc5af05ffa600e7b9b660456393d686b7ea9ce79d93fcb83e47e9d7422687d5a97cae8f445926a0b1511d34b9a3244932869dcf5a99c2");

        byte[]? pt1 = DataEncryption.Decrypt(Convert.FromHexString(_key128), v1);
        Assert.That(Convert.ToHexStringLower(pt1!), Is.EqualTo("11223344"));

        // "upgrade" to v2, confirm v2 cannot decrypt
        var v2 = new byte[v1.Length];
        Array.Copy(v1, v2, v1.Length);
        v2[0] = 0x02;
        byte[]? pt2 = DataEncryption.Decrypt(Convert.FromHexString(_key128), v2);
        Assert.That(pt2, Is.Null);

        // test keyring/legacy mode
        var keyRing = new Dictionary<byte, byte[]> { [0] = Convert.FromHexString(_key128) };
        pt1 = DataEncryption.Decrypt(keyRing, v1);
        Assert.That(Convert.ToHexStringLower(pt1!), Is.EqualTo("11223344"));
    }
}
