using Plinth.Security.Crypto;
using NUnit.Framework;

namespace Tests.Plinth.Security.Crypto;

[TestFixture]
public class SecureDataTest
{
    [Test]
    [TestCase("eb0fb87d0e6fd3b131db5d2a768482c1")] // AES-128
    [TestCase("26dfb8cf79a4803104ec496c76ca89dfc8f16275b497511a")] // AES-192
    [TestCase("582e36877a0373e6543748298cc689019e0bca9212d785697efeae22d651df27")] // AES-256
    public void Test_Encrypt_String(string key)
    {
        string ssn = "123-45-6789";

        ISecureData enc = new SecureData(key);

        string result = enc.EncryptToBase64(ssn);
        byte[] result2 = enc.Encrypt(ssn);
        string result3 = enc.EncryptToHex(ssn);

        string? roundTrip = enc.DecryptBase64ToString(result);
        string? roundTrip2 = enc.DecryptToString(result2);
        string? roundTrip3 = enc.DecryptHexToString(result3);

        Assert.That(roundTrip, Is.EqualTo(ssn));
        Assert.That(roundTrip2, Is.EqualTo(ssn));
        Assert.That(roundTrip3, Is.EqualTo(ssn));
        Assert.That(result, Is.Not.EqualTo(ssn));

        // encrypt again, different result
        string? result2d = enc.EncryptToBase64(ssn);
        Assert.That(result2d, Is.Not.EqualTo(result));

        string? result3d = enc.EncryptToHex(ssn);
        Assert.That(result3d, Is.Not.EqualTo(result));

        string? roundTrip2_2 = enc.DecryptToString(result2);
        Assert.That(roundTrip2_2, Is.EqualTo(roundTrip));
        Assert.That(roundTrip2_2, Is.EqualTo(roundTrip3));
    }

    [Test]
    [TestCase("eb0fb87d0e6fd3b131db5d2a768482c1")] // AES-128
    [TestCase("26dfb8cf79a4803104ec496c76ca89dfc8f16275b497511a")] // AES-192
    [TestCase("582e36877a0373e6543748298cc689019e0bca9212d785697efeae22d651df27")] // AES-256
    public void Test_Encrypt_Empty_String(string key)
    {
        string ssn = "";

        ISecureData enc = new SecureData(key);

        string result = enc.EncryptToBase64(ssn);
        byte[] result2 = enc.Encrypt(ssn);
        string result3 = enc.EncryptToHex(ssn);

        string? roundTrip = enc.DecryptBase64ToString(result);
        string? roundTrip2 = enc.DecryptToString(result2);
        string? roundTrip3 = enc.DecryptHexToString(result3);

        Assert.That(roundTrip, Is.EqualTo(ssn));
        Assert.That(roundTrip2, Is.EqualTo(ssn));
        Assert.That(roundTrip3, Is.EqualTo(ssn));
        Assert.That(result, Is.Not.EqualTo(ssn));

        // encrypt again, different result
        string? result2d = enc.EncryptToBase64(ssn);
        Assert.That(result2d, Is.Not.EqualTo(result));

        string? result3d = enc.EncryptToHex(ssn);
        Assert.That(result3d, Is.Not.EqualTo(result));

        string? roundTrip2_2 = enc.DecryptToString(result2);
        Assert.That(roundTrip2_2, Is.EqualTo(roundTrip));
        Assert.That(roundTrip2_2, Is.EqualTo(roundTrip3));
    }

    [Test]
    [TestCase("eb0fb87d0e6fd3b131db5d2a768482c1", "164df5dc779681b0b3bca58a2466e0ff", 1)] // AES-128
    [TestCase("eb0fb87d0e6fd3b131db5d2a768482c1", "164df5dc779681b0b3bca58a2466e0ff", 77)] // AES-128
    [TestCase("26dfb8cf79a4803104ec496c76ca89dfc8f16275b497511a", "a167c2112615f3c9c56943d59fd2a90ef3ba70715141b702", 1)] // AES-192
    [TestCase("26dfb8cf79a4803104ec496c76ca89dfc8f16275b497511a", "a167c2112615f3c9c56943d59fd2a90ef3ba70715141b702", 77)] // AES-192
    [TestCase("582e36877a0373e6543748298cc689019e0bca9212d785697efeae22d651df27", "4a93cc8ffe63ee5388ed76a04ca08da285e9d660da7843518580093aedf64cb0", 1)] // AES-256
    [TestCase("582e36877a0373e6543748298cc689019e0bca9212d785697efeae22d651df27", "4a93cc8ffe63ee5388ed76a04ca08da285e9d660da7843518580093aedf64cb0", 77)] // AES-256
    public void Test_Encrypt_MultiKey_String(string key1, string key2, byte keyId)
    {
        string ssn = "123-45-6789";

        var enc = new SecureData(keyId, (1, key1), (77, key2));

        string result = enc.EncryptToBase64(ssn, keyId);
        byte[] result2 = enc.Encrypt(ssn, keyId);
        string result3 = enc.EncryptToHex(ssn, keyId);

        string? roundTrip = enc.DecryptBase64ToString(result);
        string? roundTrip2 = enc.DecryptToString(result2);
        string? roundTrip3 = enc.DecryptHexToString(result3);

        Assert.That(roundTrip, Is.EqualTo(ssn));
        Assert.That(roundTrip2, Is.EqualTo(ssn));
        Assert.That(roundTrip3, Is.EqualTo(ssn));
        Assert.That(result, Is.Not.EqualTo(ssn));

        // encrypt again, different result
        string? result2d = enc.EncryptToBase64(ssn, keyId);
        Assert.That(result2d, Is.Not.EqualTo(result));

        string? result3d = enc.EncryptToHex(ssn, keyId);
        Assert.That(result3d, Is.Not.EqualTo(result));

        string? roundTrip2_2 = enc.DecryptToString(result2);
        Assert.That(roundTrip2_2, Is.EqualTo(roundTrip));
        Assert.That(roundTrip2_2, Is.EqualTo(roundTrip3));
    }

    [Test]
    [TestCase("eb0fb87d0e6fd3b131db5d2a768482c1")] // AES-128
    [TestCase("26dfb8cf79a4803104ec496c76ca89dfc8f16275b497511a")] // AES-192
    [TestCase("582e36877a0373e6543748298cc689019e0bca9212d785697efeae22d651df27")] // AES-256
    public void Test_Encrypt_MultiKey_WrongKey(string key)
    {
        string ssn = "123-45-6789";

        var enc = new SecureData(1, (1, key));

        string result = enc.EncryptToBase64(ssn, 1);
        byte[] result2 = enc.Encrypt(ssn, 1);
        string result3 = enc.EncryptToHex(ssn, 1);

        var enc2 = new SecureData(7, (7, key));

        Assert.That(enc2.DecryptBase64(result), Is.Null);
        Assert.That(enc2.Decrypt(result2), Is.Null);
        Assert.That(enc2.DecryptHex(result3), Is.Null);
    }

    [Test]
    [TestCase("eb0fb87d0e6fd3b131db5d2a768482c1")] // AES-128
    [TestCase("26dfb8cf79a4803104ec496c76ca89dfc8f16275b497511a")] // AES-192
    [TestCase("582e36877a0373e6543748298cc689019e0bca9212d785697efeae22d651df27")] // AES-256
    public void Test_Encrypt_Bytes(string key)
    {
        byte[] data = Primitives.GetSecureRandomBytes(100000);

        var enc = new SecureData(key);

        byte[] result = enc.Encrypt(data);
        string result2 = enc.EncryptToBase64(data);
        string result3 = enc.EncryptToHex(data);

        byte[]? roundTrip = enc.Decrypt(result);
        byte[]? roundTrip2 = enc.DecryptBase64(result2);
        byte[]? roundTrip3 = enc.DecryptHex(result3);

        Assert.That(roundTrip, Is.EqualTo(data).AsCollection);
        Assert.That(roundTrip2, Is.EqualTo(data).AsCollection);
        Assert.That(roundTrip3, Is.EqualTo(data).AsCollection);

        // encrypt again, different result
        byte[]? result_again = enc.Encrypt(data);
        Assert.That(result_again, Is.Not.EqualTo(result).AsCollection);

        byte[]? roundTrip2_2 = enc.DecryptBase64(result2);
        Assert.That(roundTrip2_2, Is.EqualTo(roundTrip).AsCollection);

        byte[]? roundTrip3_2 = enc.DecryptHex(result3);
        Assert.That(roundTrip3_2, Is.EqualTo(roundTrip).AsCollection);
    }

    [Test]
    [TestCase("eb0fb87d0e6fd3b131db5d2a768482c1")] // AES-128
    [TestCase("26dfb8cf79a4803104ec496c76ca89dfc8f16275b497511a")] // AES-192
    [TestCase("582e36877a0373e6543748298cc689019e0bca9212d785697efeae22d651df27")] // AES-256
    public void Test_Encrypt_EmptyBytes(string key)
    {
        byte[] data = [];

        var enc = new SecureData(key);

        byte[] result = enc.Encrypt(data);
        string result2 = enc.EncryptToBase64(data);
        string result3 = enc.EncryptToHex(data);

        byte[]? roundTrip = enc.Decrypt(result);
        byte[]? roundTrip2 = enc.DecryptBase64(result2);
        byte[]? roundTrip3 = enc.DecryptHex(result3);

        Assert.That(roundTrip, Is.EqualTo(data).AsCollection);
        Assert.That(roundTrip2, Is.EqualTo(data).AsCollection);
        Assert.That(roundTrip3, Is.EqualTo(data).AsCollection);

        // encrypt again, different result
        byte[]? result_again = enc.Encrypt(data);
        Assert.That(result_again, Is.Not.EqualTo(result).AsCollection);

        byte[]? roundTrip2_2 = enc.DecryptBase64(result2);
        Assert.That(roundTrip2_2, Is.EqualTo(roundTrip).AsCollection);

        byte[]? roundTrip3_2 = enc.DecryptHex(result3);
        Assert.That(roundTrip3_2, Is.EqualTo(roundTrip).AsCollection);
    }

    [Test]
    [TestCase("eb0fb87d0e6fd3b131db5d2a768482c1", "164df5dc779681b0b3bca58a2466e0ff", 1)] // AES-128
    [TestCase("eb0fb87d0e6fd3b131db5d2a768482c1", "164df5dc779681b0b3bca58a2466e0ff", 77)] // AES-128
    [TestCase("26dfb8cf79a4803104ec496c76ca89dfc8f16275b497511a", "a167c2112615f3c9c56943d59fd2a90ef3ba70715141b702", 1)] // AES-192
    [TestCase("26dfb8cf79a4803104ec496c76ca89dfc8f16275b497511a", "a167c2112615f3c9c56943d59fd2a90ef3ba70715141b702", 77)] // AES-192
    [TestCase("582e36877a0373e6543748298cc689019e0bca9212d785697efeae22d651df27", "4a93cc8ffe63ee5388ed76a04ca08da285e9d660da7843518580093aedf64cb0", 1)] // AES-256
    [TestCase("582e36877a0373e6543748298cc689019e0bca9212d785697efeae22d651df27", "4a93cc8ffe63ee5388ed76a04ca08da285e9d660da7843518580093aedf64cb0", 77)] // AES-256
    public void Test_Encrypt_Bytes_MultiKey(string key1, string key2, byte keyId)
    {
        byte[] data = Primitives.GetSecureRandomBytes(100000);

        var enc = new SecureData(keyId, (1, key1), (77, key2));

        byte[] result = enc.Encrypt(data, keyId);
        string result2 = enc.EncryptToBase64(data, keyId);
        string result3 = enc.EncryptToHex(data, keyId);

        byte[]? roundTrip = enc.Decrypt(result);
        byte[]? roundTrip2 = enc.DecryptBase64(result2);
        byte[]? roundTrip3 = enc.DecryptHex(result3);

        Assert.That(roundTrip, Is.EqualTo(data).AsCollection);
        Assert.That(roundTrip2, Is.EqualTo(data).AsCollection);
        Assert.That(roundTrip3, Is.EqualTo(data).AsCollection);

        // encrypt again, different result
        byte[]? result_again = enc.Encrypt(data, keyId);
        Assert.That(result_again, Is.Not.EqualTo(result).AsCollection);

        byte[]? roundTrip2_2 = enc.DecryptBase64(result2);
        Assert.That(roundTrip2_2, Is.EqualTo(roundTrip).AsCollection);

        byte[]? roundTrip3_2 = enc.DecryptHex(result3);
        Assert.That(roundTrip3_2, Is.EqualTo(roundTrip).AsCollection);
    }

    [Test]
    [TestCase("eb0fb87d0e6fd3b131db5d2a768482c1", "a167c2112615f3c9c56943d59fd2a90ef3ba70715141b702", "4a93cc8ffe63ee5388ed76a04ca08da285e9d660da7843518580093aedf64cb0")]
    public void Test_Encrypt_Bytes_MultiKey_DiffLengths(string key1, string key2, string key3)
    {
        byte[] data = Primitives.GetSecureRandomBytes(100000);

        var enc = new SecureData(1, (1, key1), (2, key2), (3, key3));

        for (byte keyId = 1; keyId <= 2; keyId++)
        {
            byte[] result1 = enc.Encrypt(data, keyId);
            string result2 = enc.EncryptToBase64(data, keyId);
            string result3 = enc.EncryptToHex(data, keyId);

            byte[]? roundTrip1 = enc.Decrypt(result1);
            byte[]? roundTrip2 = enc.DecryptBase64(result2);
            byte[]? roundTrip3 = enc.DecryptHex(result3);

            Assert.That(roundTrip1, Is.EqualTo(data).AsCollection);
            Assert.That(roundTrip2, Is.EqualTo(data).AsCollection);
            Assert.That(roundTrip3, Is.EqualTo(data).AsCollection);
        }
    }

    [Test]
    [TestCase("eb0fb87d0e6fd3b131db5d2a768482c1", "a167c2112615f3c9c56943d59fd2a90ef3ba70715141b702", "4a93cc8ffe63ee5388ed76a04ca08da285e9d660da7843518580093aedf64cb0")]
    public void Test_Encrypt_Bytes_MultiKey_DiffLengths_Staged(string key1, string key2, string key3)
    {
        byte[] data = Primitives.GetSecureRandomBytes(100000);

        var enc = new SecureData(1, (1, key1));
        byte[] result1 = enc.Encrypt(data, 1);

        var enc2 = new SecureData(2, (1, key1), (2, key2), (3, key3));

        byte[]? roundTrip1 = enc2.Decrypt(result1);

        Assert.That(roundTrip1, Is.EqualTo(data).AsCollection);
    }

    [Test]
    [TestCase("eb0fb87d0e6fd3b131db5d2a768482c1", "a167c2112615f3c9c56943d59fd2a90ef3ba70715141b702")]
    public void Test_Encrypt_Bytes_MultiKey_WithLegacy_Rotation(string key1, string key2)
    {
        byte[] data = Primitives.GetSecureRandomBytes(100000);

        var enc = new SecureData(key1); // legacy key
        byte[] result1 = enc.Encrypt(data);

        var enc2 = new SecureData(2, (0, key1), (2, key2)); // rotation to key2

        byte[]? roundTrip1 = enc2.Decrypt(result1); // can still decrypt legacy

        Assert.That(roundTrip1, Is.EqualTo(data).AsCollection);

        byte[] result2 = enc2.Encrypt(data); // encrypt with key2

        var enc3 = new SecureData(2, (2, key2)); // legacy removed

        byte[]? roundTrip2 = enc2.Decrypt(result2); // can still decrypt legacy
        byte[]? roundTrip3 = enc3.Decrypt(result2); // legacy key not available

        Assert.That(roundTrip2, Is.EqualTo(data).AsCollection);
        Assert.That(roundTrip3, Is.EqualTo(data).AsCollection);
    }

    [Test]
    [TestCase("eb0fb87d0e6fd3b131db5d2a768482c1", "164df5dc779681b0b3bca58a2466e0ff", 1)] // AES-128
    [TestCase("eb0fb87d0e6fd3b131db5d2a768482c1", "164df5dc779681b0b3bca58a2466e0ff", 0)] // AES-128
    [TestCase("26dfb8cf79a4803104ec496c76ca89dfc8f16275b497511a", "a167c2112615f3c9c56943d59fd2a90ef3ba70715141b702", 1)] // AES-192
    [TestCase("26dfb8cf79a4803104ec496c76ca89dfc8f16275b497511a", "a167c2112615f3c9c56943d59fd2a90ef3ba70715141b702", 0)] // AES-192
    [TestCase("582e36877a0373e6543748298cc689019e0bca9212d785697efeae22d651df27", "4a93cc8ffe63ee5388ed76a04ca08da285e9d660da7843518580093aedf64cb0", 1)] // AES-256
    [TestCase("582e36877a0373e6543748298cc689019e0bca9212d785697efeae22d651df27", "4a93cc8ffe63ee5388ed76a04ca08da285e9d660da7843518580093aedf64cb0", 0)] // AES-256
    public void Test_Encrypt_MultiKey_DefaultKey(string key1, string key2, byte keyId)
    {
        byte[] data = Primitives.GetSecureRandomBytes(100000);

        var enc = new SecureData(keyId, (keyId, key1), (77, key2));
        var enc2 = new SecureData(key1);

        byte[] result = enc.Encrypt(data, keyId);
        byte[] result2 = enc.Encrypt(data);

        byte[]? roundTrip = enc.Decrypt(result);
        byte[]? roundTrip2 = enc.Decrypt(result2);

        Assert.That(roundTrip, Is.EqualTo(data).AsCollection);
        Assert.That(roundTrip2, Is.EqualTo(data).AsCollection);

        byte[]? result3 = enc.Encrypt(data, 77);
        Assert.That(enc2.Decrypt(result3), Is.Null);
    }

    [Test]
    [TestCase("eb0fb87d0e6fd3b131db5d2a768482c1", "164df5dc779681b0b3bca58a2466e0ff", 1)] // AES-128
    [TestCase("eb0fb87d0e6fd3b131db5d2a768482c1", "164df5dc779681b0b3bca58a2466e0ff", 0)] // AES-128
    [TestCase("26dfb8cf79a4803104ec496c76ca89dfc8f16275b497511a", "a167c2112615f3c9c56943d59fd2a90ef3ba70715141b702", 1)] // AES-192
    [TestCase("26dfb8cf79a4803104ec496c76ca89dfc8f16275b497511a", "a167c2112615f3c9c56943d59fd2a90ef3ba70715141b702", 0)] // AES-192
    [TestCase("582e36877a0373e6543748298cc689019e0bca9212d785697efeae22d651df27", "4a93cc8ffe63ee5388ed76a04ca08da285e9d660da7843518580093aedf64cb0", 1)] // AES-256
    [TestCase("582e36877a0373e6543748298cc689019e0bca9212d785697efeae22d651df27", "4a93cc8ffe63ee5388ed76a04ca08da285e9d660da7843518580093aedf64cb0", 0)] // AES-256
    public void Test_SecureDataDefaultWrapper(string key1, string key2, byte keyId)
    {
        byte[] data = Primitives.GetSecureRandomBytes(100000);

        ISecureData enc = new SecureData(keyId, (keyId, key1), (77, key2));
        ISecureData enc2 = enc.WithNewDefaultKey(77);

        byte[] result = enc.Encrypt(data, keyId);
        byte[] result2 = enc2.Encrypt(data, keyId);

        byte[]? roundTrip = enc.Decrypt(result);
        byte[]? roundTrip2 = enc2.Decrypt(result2);

        Assert.That(roundTrip, Is.EqualTo(data).AsCollection);
        Assert.That(roundTrip2, Is.EqualTo(data).AsCollection);

        byte[]? result3 = enc.Encrypt(data, 77);
        byte[]? result4 = enc2.Encrypt(data);

        byte[]? roundTrip3 = enc.Decrypt(result3);
        byte[]? roundTrip4 = enc2.Decrypt(result4);

        Assert.That(roundTrip3, Is.EqualTo(data).AsCollection);
        Assert.That(roundTrip4, Is.EqualTo(data).AsCollection);
    }

    [Test]
    [TestCase("eb0fb87d0e6fd3b131db5d2a768482c1")] // AES-128
    [TestCase("26dfb8cf79a4803104ec496c76ca89dfc8f16275b497511a")] // AES-192
    [TestCase("582e36877a0373e6543748298cc689019e0bca9212d785697efeae22d651df27")] // AES-256
    public void Test_Null_Inputs(string key)
    {
        var enc = new SecureData((0, key));

        Assert.That(enc.Encrypt((string?)null), Is.Null);
        Assert.That(enc.Encrypt((byte[]?)null, 0), Is.Null);
        Assert.That(enc.EncryptToBase64((string?)null), Is.Null);
        Assert.That(enc.EncryptToBase64((string?)null, 0), Is.Null);
        Assert.That(enc.EncryptToHex((string?)null), Is.Null);
        Assert.That(enc.EncryptToHex((string?)null, 0), Is.Null);
        Assert.That(enc.Encrypt((byte[]?)null), Is.Null);
        Assert.That(enc.Encrypt((byte[]?)null, 0), Is.Null);
        Assert.That(enc.EncryptToBase64((byte[]?)null), Is.Null);
        Assert.That(enc.EncryptToBase64((byte[]?)null, 0), Is.Null);
        Assert.That(enc.EncryptToHex((byte[]?)null), Is.Null);
        Assert.That(enc.EncryptToHex((byte[]?)null, 0), Is.Null);

        Assert.That(enc.DecryptBase64(null), Is.Null);
        Assert.That(enc.DecryptHex(null), Is.Null);
        Assert.That(enc.DecryptBase64ToString(null), Is.Null);
        Assert.That(enc.DecryptHexToString(null), Is.Null);
        Assert.That(enc.Decrypt(null), Is.Null);
        Assert.That(enc.DecryptToString(null), Is.Null);
    }

    [Test]
    public void Test_Encrypt_NullKey()
    {
        byte k = 0;
        string? s = null;

        Assert.Throws<ArgumentNullException>(() => new SecureData((string?)null!));
        Assert.Throws<ArgumentNullException>(() => new SecureData(null!, (SecureData.KEY_DEFAULT, "164df5dc779681b0b3bca58a2466e0ff")));
        Assert.Throws<ArgumentNullException>(() => new SecureData("164df5dc779681b0b3bca58a2466e0ff", (5, s!)));

        Assert.Throws<ArgumentNullException>(() => new SecureData((k, s!)));
        Assert.Throws<ArgumentNullException>(() => new SecureData((5, "164df5dc779681b0b3bca58a2466e0ff"), (k, s!)));
    }

    [Test]
    public void Test_Encrypt_InvalidKey()
    {
        Assert.Throws<ArgumentException>(() => new SecureData("1234"));

        Assert.Throws<ArgumentException>(() => new SecureData((0, "164d")));
        Assert.Throws<ArgumentException>(() => new SecureData((0, "164d"), (5, "50634f56f2f892f56edbf3914a516206")));
        Assert.Throws<ArgumentException>(() => new SecureData((5, "164df5dc779681b0b3bca58a2466e0ff"), (5, "5063")));
    }

    [Test]
    public void Test_Encrypt_DuplicateKeys()
    {
        Assert.Throws<ArgumentException>(() => new SecureData((5, "164df5dc779681b0b3bca58a2466e0ff"), (5, "50634f56f2f892f56edbf3914a516206")));
    }
}
