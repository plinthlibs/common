using Plinth.Security.Crypto;
using NUnit.Framework;

#if !NET9_0_OR_GREATER
using Convert = Plinth.Common.Utils.BackPort.PlinthConvert;
#endif

namespace Tests.Plinth.Security.Crypto;

[TestFixture]
class PrimitivesHMACTest
{
    [Test]
    public void Test_HmacSHA256()
    {
        test_HmacSHA256("b0344c61d8db38535ca8afceaf0bf12b881dc200c9833da726e9376c2e32cff7", "4869205468657265", "0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b");
        test_HmacSHA256("b436e3e86cb3800b3864aeecc8d06c126f005e7645803461717a8e4b2de3a905", "616d6f756e743d3130302663757272656e63793d455552", "57617b5d2349434b34734345635073433835777e2d244c31715535255a366773755a4d70532a5879793238235f707c4f7865753f3f446e633a21575643303f66");

        static void test_HmacSHA256(string expected, string data, string key)
            => Assert.That(Convert.ToHexStringLower(Primitives.HmacSHA256(Convert.FromHexString(data), Convert.FromHexString(key))), Is.EqualTo(expected));
    }

    [Test]
    public void Test_HmacSHA384()
    {
        test_HmacSHA384("afd03944d84895626b0825f4ab46907f15f9dadbe4101ec682aa034c7cebc59cfaea9ea9076ede7f4af152e8b2fa9cb6", "4869205468657265", "0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b");
        test_HmacSHA384("949abda355454c149f619fe0e0f2ee162016e3d020266cd1f6bc2cdc130fd7f9b710f117068e385eb41cb39fedfa5c9d", "616d6f756e743d3130302663757272656e63793d455552", "57617b5d2349434b34734345635073433835777e2d244c31715535255a366773755a4d70532a5879793238235f707c4f7865753f3f446e633a21575643303f66");

        static void test_HmacSHA384(string expected, string data, string key)
            => Assert.That(Convert.ToHexStringLower(Primitives.HmacSHA384(Convert.FromHexString(data), Convert.FromHexString(key))), Is.EqualTo(expected));
    }

    [Test]
    public void Test_HmacSHA512()
    {
        test_HmacSHA512("87aa7cdea5ef619d4ff0b4241a1d6cb02379f4e2ce4ec2787ad0b30545e17cdedaa833b7d6b8a702038b274eaea3f4e4be9d914eeb61f1702e696c203a126854", "4869205468657265", "0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b");
        test_HmacSHA512("55b6501347a5202df62f0c67b646d0de25e9242839b6858142d14a5f2c9ef99e66c613384a214d62fcc0c0b8f4eb87b01d0f826fec4c71efcc5d94cf09221784", "616d6f756e743d3130302663757272656e63793d455552", "57617b5d2349434b34734345635073433835777e2d244c31715535255a366773755a4d70532a5879793238235f707c4f7865753f3f446e633a21575643303f66");

        static void test_HmacSHA512(string expected, string data, string key)
            => Assert.That(Convert.ToHexStringLower(Primitives.HmacSHA512(Convert.FromHexString(data), Convert.FromHexString(key))), Is.EqualTo(expected));
    }

    private static void Test_VerifyHmac(string hmac, string data, string key, Func<byte[], byte[], byte[], bool> hmacFunc, Func<byte[], int, int, int, byte[], bool> hmacFunc2)
    {
        byte[] hmacB = Convert.FromHexString(hmac);
        byte[] dataB = Convert.FromHexString(data);
        Assert.That(hmacFunc(hmacB, dataB, Convert.FromHexString(key)));

        byte[] buffer = new byte[hmacB.Length + dataB.Length + 10];
        Array.Copy(hmacB, 0, buffer, 5, hmacB.Length);
        Array.Copy(dataB, 0, buffer, 10 + hmacB.Length, dataB.Length);
        Assert.That(hmacFunc2(buffer, 5, 10 + hmacB.Length, dataB.Length, Convert.FromHexString(key)));

        // invalid hmac
        hmacB[3] = 0x74;
        Assert.That(hmacFunc(hmacB, dataB, Convert.FromHexString(key)), Is.False);

        buffer = new byte[hmacB.Length + dataB.Length + 10];
        Array.Copy(hmacB, 0, buffer, 5, hmacB.Length);
        Array.Copy(dataB, 0, buffer, 10 + hmacB.Length, dataB.Length);
        Assert.That(hmacFunc2(buffer, 5, 10 + hmacB.Length, dataB.Length, Convert.FromHexString(key)), Is.False);

        // invalid data
        hmacB = Convert.FromHexString(hmac);
        dataB[^1] = 0x86;
        Assert.That(hmacFunc(hmacB, dataB, Convert.FromHexString(key)), Is.False);

        buffer = new byte[hmacB.Length + dataB.Length + 10];
        Array.Copy(hmacB, 0, buffer, 5, hmacB.Length);
        Array.Copy(dataB, 0, buffer, 10 + hmacB.Length, dataB.Length);
        Assert.That(hmacFunc2(buffer, 5, 10 + hmacB.Length, dataB.Length, Convert.FromHexString(key)), Is.False);

        // invalid data and hmac
        hmacB[3] = 0x74;
        Assert.That(hmacFunc(hmacB, dataB, Convert.FromHexString(key)), Is.False);

        buffer = new byte[hmacB.Length + dataB.Length + 10];
        Array.Copy(hmacB, 0, buffer, 5, hmacB.Length);
        Array.Copy(dataB, 0, buffer, 10 + hmacB.Length, dataB.Length);
        Assert.That(hmacFunc2(buffer, 5, 10 + hmacB.Length, dataB.Length, Convert.FromHexString(key)), Is.False);
    }

    [Test]
    public void Test_VerifyHmacSHA256()
    {
        Test_VerifyHmac("b0344c61d8db38535ca8afceaf0bf12b881dc200c9833da726e9376c2e32cff7", "4869205468657265", "0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b", Primitives.VerifyHmacSHA256, Primitives.VerifyHmacSHA256);
        Test_VerifyHmac("b436e3e86cb3800b3864aeecc8d06c126f005e7645803461717a8e4b2de3a905", "616d6f756e743d3130302663757272656e63793d455552", "57617b5d2349434b34734345635073433835777e2d244c31715535255a366773755a4d70532a5879793238235f707c4f7865753f3f446e633a21575643303f66", Primitives.VerifyHmacSHA256, Primitives.VerifyHmacSHA256);
    }

    [Test]
    public void Test_VerifyHmacSHA384()
    {
        Test_VerifyHmac("afd03944d84895626b0825f4ab46907f15f9dadbe4101ec682aa034c7cebc59cfaea9ea9076ede7f4af152e8b2fa9cb6", "4869205468657265", "0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b", Primitives.VerifyHmacSHA384, Primitives.VerifyHmacSHA384);
        Test_VerifyHmac("949abda355454c149f619fe0e0f2ee162016e3d020266cd1f6bc2cdc130fd7f9b710f117068e385eb41cb39fedfa5c9d", "616d6f756e743d3130302663757272656e63793d455552", "57617b5d2349434b34734345635073433835777e2d244c31715535255a366773755a4d70532a5879793238235f707c4f7865753f3f446e633a21575643303f66", Primitives.VerifyHmacSHA384, Primitives.VerifyHmacSHA384);
    }

    [Test]
    public void Test_VerifyHmacSHA512()
    {
        Test_VerifyHmac("87aa7cdea5ef619d4ff0b4241a1d6cb02379f4e2ce4ec2787ad0b30545e17cdedaa833b7d6b8a702038b274eaea3f4e4be9d914eeb61f1702e696c203a126854", "4869205468657265", "0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b", Primitives.VerifyHmacSHA512, Primitives.VerifyHmacSHA512);
        Test_VerifyHmac("55b6501347a5202df62f0c67b646d0de25e9242839b6858142d14a5f2c9ef99e66c613384a214d62fcc0c0b8f4eb87b01d0f826fec4c71efcc5d94cf09221784", "616d6f756e743d3130302663757272656e63793d455552", "57617b5d2349434b34734345635073433835777e2d244c31715535255a366773755a4d70532a5879793238235f707c4f7865753f3f446e633a21575643303f66", Primitives.VerifyHmacSHA512, Primitives.VerifyHmacSHA512);
    }
}
