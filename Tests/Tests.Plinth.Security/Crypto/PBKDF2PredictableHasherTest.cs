﻿using Plinth.Security.Crypto;
using NUnit.Framework;

namespace Tests.Plinth.Security.Crypto;

[TestFixture]
public class PBKDF2PredictableHasherTest
{
    [Test]
    [TestCase(20, 1000)]
    [TestCase(32, 310000)]
    [TestCase(48, 310000)]
    [TestCase(64, 310000)]
    public void Test_PredictableHash(int len, int iter)
    {
        var sd = new PBKDF2PredictableHasher("12345678901234567890123456789012", len, iter);

        string h1 = sd.PredictableHash("123-45-6789");
        string h2 = sd.PredictableHash("123-45-6789");

        Assert.That(h2, Is.EqualTo(h1));
    }

    [Test]
    public void Test_Invalid()
    {
        Assert.Throws<ArgumentNullException>(() => new PBKDF2PredictableHasher(null!));
        Assert.Throws<ArgumentException>(() => new PBKDF2PredictableHasher("12345678901234567890123456789012", 85));
        Assert.Throws<ArgumentOutOfRangeException>(() => new PBKDF2PredictableHasher("12345678901234567890123456789012", 32, 0));
        Assert.Throws<ArgumentException>(() => new PBKDF2PredictableHasher("1234"));
    }
}
