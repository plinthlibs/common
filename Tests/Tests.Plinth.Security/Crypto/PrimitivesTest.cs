using Plinth.Security.Crypto;
using NUnit.Framework;
using System.Text;

#if !NET9_0_OR_GREATER
using Convert = Plinth.Common.Utils.BackPort.PlinthConvert;
#endif

namespace Tests.Plinth.Security.Crypto;

[TestFixture]
public class PrimitivesTest
{
    [Test]
    public void Test_GetSecureRandomBytes()
    {
        byte[] bytes = Primitives.GetSecureRandomBytes(6);
        Assert.That(bytes, Has.Length.EqualTo(6));

        byte[] bytes2 = Primitives.GetSecureRandomBytes(6);
        Assert.That(bytes2, Is.Not.EqualTo(bytes).AsCollection);

        byte[] empty = Primitives.GetSecureRandomBytes(0);
        Assert.That(empty, Is.EqualTo(Array.Empty<byte>()).AsCollection);
    }

    [Test]
    public void Test_GetSecureRandomBytes_Negative()
    {
        Assert.Throws<ArgumentOutOfRangeException>(() => Primitives.GetSecureRandomBytes(-5));
    }

    [Test]
    public void Test_HmacPBKDF2()
    {
        var salt = Convert.FromHexString("f8fe61afe0d709961d3057cdc9fcd647");
        Assert.That(Convert.ToHexStringLower(Primitives.HmacPBKDF2("123-45-6789", salt, 20000, 20)), Is.EqualTo("969a21f6ae211b729b33ebd7999e1809e9b0b925"));
        Assert.That(Convert.ToHexStringLower(Primitives.HmacPBKDF2("984-48-2735", salt, 20000, 20)), Is.EqualTo("4ddcc7f70fd1887a4af6fb93540c7bf791fb13fe"));
        Assert.That(Convert.ToHexStringLower(Primitives.HmacPBKDF2("984-48-2736", salt, 20000, 20)), Is.EqualTo("b58b8b17602dc024e559f4560a02d126b00e7bbf"));

        salt = Convert.FromHexString("eb5ac6cd4fa07ad581e1ad7ada4038d85c28407a40b7c91937c321600f315b96");
        Assert.That(Convert.ToHexStringLower(Primitives.HmacPBKDF2("123-45-6789", salt, 20000, 32)), Is.EqualTo("b11cda12439bca48807a204c99b69a6ba307f564cb4e56994e41bb3fd3e7f129"));
        Assert.That(Convert.ToHexStringLower(Primitives.HmacPBKDF2("984-48-2735", salt, 20000, 32)), Is.EqualTo("f04769a039a1610f25b717e4b03d30214efcecc3137450eee9036d15ae3b9ff2"));
        Assert.That(Convert.ToHexStringLower(Primitives.HmacPBKDF2("984-48-2736", salt, 20000, 32)), Is.EqualTo("b27e8f9411ba1c64e9e6342aa6d6bea22f29e15d7ac974ab89b205cca11d8e47"));

        salt = Convert.FromHexString("c63edac1d93e2e5da763531d6da152b18e4725dd944e808dd3fc9ae37ac6da87ff3a92f169a279821b6798f40d33adea");
        Assert.That(Convert.ToHexStringLower(Primitives.HmacPBKDF2("123-45-6789", salt, 20000, 48)), Is.EqualTo("69af6dad71fc6a9a46b8f1f5490692f3d734d2053db71475d76a34e19a600b1b8ae789ceda40ca5ac04073734918c8a1"));
        Assert.That(Convert.ToHexStringLower(Primitives.HmacPBKDF2("984-48-2735", salt, 20000, 48)), Is.EqualTo("5363e23828f5c632b3a8007cbd427e66dc8a5e56e684431bdaced3e8d2f3a1bb0819cd537af79e762d91f623b01fec5f"));
        Assert.That(Convert.ToHexStringLower(Primitives.HmacPBKDF2("984-48-2736", salt, 20000, 48)), Is.EqualTo("8b8f960b5a15ab1346166004fe9cca36b058e9b8422861fbfdfeb40bca09ebed8666ea858e462d18eb3e8283fc75f1fe"));

        salt = Convert.FromHexString("772ad72c624ecbfe1b51f986ca505f60b11642084fad45222908bd12f5ce65fb5148e157eee8832a3e190b1fee53bf51a66624cb112e2ddc9785f70666fa7a9f");
        Assert.That(Convert.ToHexStringLower(Primitives.HmacPBKDF2("123-45-6789", salt, 20000, 64)), Is.EqualTo("022ddf4c955e2cc5208df55cdd7c64b6d677071e2aabc76252387c0f5b0e1cc27d6886911eac1f579a861d901bcb9986b7b8845ff91b2c0e53710ef5ea2f0c23"));
        Assert.That(Convert.ToHexStringLower(Primitives.HmacPBKDF2("984-48-2735", salt, 20000, 64)), Is.EqualTo("1be599db445ec14c90e2731747a446116db717e929bb3190637afd0540d2352b991217753ceb7a62f2e4e8dfb9a019f132c17e877c9826ce222febb2c011237e"));
        Assert.That(Convert.ToHexStringLower(Primitives.HmacPBKDF2("984-48-2736", salt, 20000, 64)), Is.EqualTo("10e9b8906dc2ccd91e4c05a0a295910259e5e2ea0ebe70e31750142b9e1b1c6c5c403ae105046ec06f48ddd7c767508a30c12636454f460aef858032a2a6aa5e"));
    }

    [Test]
    public void Test_HmacPBKDF2_Null()
    {
        Assert.Throws<ArgumentNullException>(() => Primitives.HmacPBKDF2(null!, Convert.FromHexString("1234"), 100, 20));
        Assert.Throws<ArgumentNullException>(() => Primitives.HmacPBKDF2(null!, Convert.FromHexString("1234"), 100, 32));
    }

    [Test]
    public void Test_HmacPBKDF2_Null2()
    {
        Assert.Throws<ArgumentNullException>(() => Primitives.HmacPBKDF2("plain", null!, 100, 20));
        Assert.Throws<ArgumentNullException>(() => Primitives.HmacPBKDF2("plain", null!, 100, 32));
    }

    [Test]
    public void Test_HmacPBKDF2_IterTooSmall()
    {
        Assert.Throws<ArgumentOutOfRangeException>(() => Primitives.HmacPBKDF2("plain", Convert.FromHexString("1234"), 0, 20));
        Assert.Throws<ArgumentOutOfRangeException>(() => Primitives.HmacPBKDF2("plain", Convert.FromHexString("1234"), -5, 20));
        Assert.Throws<ArgumentOutOfRangeException>(() => Primitives.HmacPBKDF2("plain", Convert.FromHexString("1234"), 0, 32));
        Assert.Throws<ArgumentOutOfRangeException>(() => Primitives.HmacPBKDF2("plain", Convert.FromHexString("1234"), -5, 32));
    }

    [Test]
    public void Test_HmacPBKDF2_BadHashSize()
    {
        for (int i = 0; i < 100; i++)
        {
            if (i == 20 || i == 32 || i == 48 || i == 64) continue;
            Assert.Throws<NotSupportedException>(() => Convert.ToHexStringLower(Primitives.HmacPBKDF2("123-45-6789", Convert.FromHexString("1234"), 20000, i)));
        }
    }

    [Test]
    public void Test_EncryptAES()
    {
        byte[] key = Convert.FromHexString("9b9111948a32b817dee2dd6a513b7064");
        byte[] iv = Convert.FromHexString("0ee7f5ecffc33809732c63d764ea82ec");

        byte[] ct = Primitives.EncryptAES_CBC(key, Convert.FromHexString("11223344"), iv);
        Assert.That(Convert.ToHexStringLower(ct), Is.EqualTo("fbe0a340bbc33f2bbda63fe94a3d71b2"));

        byte[] pt = Primitives.DecryptAES_CBC(key, ct, iv);
        Assert.That(Convert.ToHexStringLower(pt), Is.EqualTo("11223344"));
    }

    [Test]
    public void Test_EncryptAES_Big()
    {
        byte[] key = Convert.FromHexString("9b9111948a32b817dee2dd6a513b7064");
        byte[] iv = Convert.FromHexString("0ee7f5ecffc33809732c63d764ea82ec");

        var sb = new StringBuilder();
        sb.Insert(0, "11223344", 128);
        string plain = sb.ToString();
        byte[] ct = Primitives.EncryptAES_CBC(key, Convert.FromHexString(plain), iv);
        Assert.That(Convert.ToHexStringLower(ct), Is.EqualTo("1101333bdcc4cf35221e4f1d8bf41f0aa8e08853ee62bc492ea927281ee9f55a741103797bb9cd0224359f5067e0de14f3149b67f52dccd6da9b3639180be826aee37600857458c4caee0ccc219ad43cc5c15cee25ca9a902bf093a8d076ab03728c3d2526070da0bfeebd142ac46626c52ddc15084ad4855e6eea5be6bdcb262a77067ad3e05d067fa4ae02db3510a79bf5197d276879ab226480d2f78e68d5a05c8b32ef92011c3f5b431fa2fb80157d0d29d7835006da6900a065e03f33257c363fc31c11a9bb6ee463f71b7d02f538f16120f8d6773eca606877624e36bd4b961170274cebc1a216813af5810000275761901a2b552b1df28d2a13ea09d9dd87227f6691a35957fb7e32bdebc75232dcdeaabbb050acefa838310eb55229c29615667577264f7b40bca6a45292ac09e82b09eb2ccf435745e9841c2e42a33a5ea6e5305241eb4453532e741ac3b679e2114df5a26c89d87cbff26787ecc6da69e4a410a1702df7fadb5cba4ed01f5e04b109339a287760cbd14ab06fc1bf5d23239e8d9eeacc173b2a5da8e4e99f5b25d7d364004b85da37d3a344c22e9bbc17fb9feb570c6f63030dc5b45edd0f90598c2088db761712ef84cefd7aee13f0d0d4ce106e20236696b668b2273f3e5966341de4f18dd6aa0d68db5b7009bfcd369843f4f8d8b2c9b61838decd4f0bcc5cbaa2b8b05aed7b311323fb70003395ed32f3bf9fb80563a39b323b851a7e"));

        byte[] pt = Primitives.DecryptAES_CBC(key, ct, iv);
        Assert.That(Convert.ToHexStringLower(pt), Is.EqualTo(plain));

        byte[] buffer = new byte[ct.Length + 10];
        Array.Copy(ct, 0, buffer, 5, ct.Length);
        byte[] pt2 = Primitives.DecryptAES_CBC(key, buffer, 5, ct.Length, iv);
        Assert.That(Convert.ToHexStringLower(pt2), Is.EqualTo(plain));
    }

    [Test]
    public void Test_EncryptAES_2Blocks()
    {
        byte[] key = Convert.FromHexString("9b9111948a32b817dee2dd6a513b7064");
        byte[] iv = Convert.FromHexString("0ee7f5ecffc33809732c63d764ea82ec");

        string plain = "1122334455667788990011223344556677889900112233";
        byte[] ct = Primitives.EncryptAES_CBC(key, Convert.FromHexString(plain), iv);
        Assert.That(Convert.ToHexStringLower(ct), Is.EqualTo("7cce0142c9a4e4f6f0be4bf22511a2a7ab07cefe44ea98e135b59dae558072c6"));

        byte[] pt = Primitives.DecryptAES_CBC(key, ct, iv);
        Assert.That(Convert.ToHexStringLower(pt), Is.EqualTo("1122334455667788990011223344556677889900112233"));

        byte[] buffer = new byte[ct.Length + 10];
        Array.Copy(ct, 0, buffer, 5, ct.Length);
        byte[] pt2 = Primitives.DecryptAES_CBC(key, buffer, 5, ct.Length, iv);
        Assert.That(Convert.ToHexStringLower(pt2), Is.EqualTo(plain));

        // 2nd block change only changes 2nd block
        byte[] ct2 = Primitives.EncryptAES_CBC(key, Convert.FromHexString("1122334455667788990011223344556677889900112234"), iv);
        Assert.That(Convert.ToHexStringLower(ct2), Is.EqualTo("7cce0142c9a4e4f6f0be4bf22511a2a762382df5a7134b23e8a92487afe67b75"));
        Assert.That(Convert.ToHexStringLower(ct2), Is.Not.EqualTo(Convert.ToHexStringLower(ct)));
    }

    [Test]
    public void Test_EncryptAES_IVChangesOutput()
    {
        byte[] key = Convert.FromHexString("9b9111948a32b817dee2dd6a513b7064");
        byte[] iv1 = Convert.FromHexString("0ee7f5ecffc33809732c63d764ea82ec");
        byte[] iv2 = Convert.FromHexString("0ee7f5ecffc33809732c63d764ea82e1");

        byte[] ct1 = Primitives.EncryptAES_CBC(key, Convert.FromHexString("11223344"), iv1);
        byte[] ct2 = Primitives.EncryptAES_CBC(key, Convert.FromHexString("11223344"), iv2);
        Assert.That(Convert.ToHexStringLower(ct1), Is.EqualTo("fbe0a340bbc33f2bbda63fe94a3d71b2"));
        Assert.That(Convert.ToHexStringLower(ct2), Is.EqualTo("f3aa851801d09fb182b11324d6e87f6a"));

        byte[] pt1 = Primitives.DecryptAES_CBC(key, ct1, iv1);
        Assert.That(Convert.ToHexStringLower(pt1), Is.EqualTo("11223344"));

        byte[] buffer1 = new byte[ct1.Length + 10];
        Array.Copy(ct1, 0, buffer1, 5, ct1.Length);
        byte[] pt1_buf = Primitives.DecryptAES_CBC(key, buffer1, 5, ct1.Length, iv1);
        Assert.That(Convert.ToHexStringLower(pt1_buf), Is.EqualTo("11223344"));

        byte[] pt2 = Primitives.DecryptAES_CBC(key, ct2, iv2);
        Assert.That(Convert.ToHexStringLower(pt2), Is.EqualTo("11223344"));

        byte[] buffer2 = new byte[ct2.Length + 10];
        Array.Copy(ct2, 0, buffer2, 5, ct2.Length);
        byte[] pt2_buf = Primitives.DecryptAES_CBC(key, buffer2, 5, ct2.Length, iv2);
        Assert.That(Convert.ToHexStringLower(pt2_buf), Is.EqualTo("11223344"));
    }

    [Test]
    public void Test_EncryptAES_PTChangesOutput()
    {
        byte[] key = Convert.FromHexString("9b9111948a32b817dee2dd6a513b7064");
        byte[] iv = Convert.FromHexString("0ee7f5ecffc33809732c63d764ea82ec");

        byte[] ct1 = Primitives.EncryptAES_CBC(key, Convert.FromHexString("11223344"), iv);
        byte[] ct2 = Primitives.EncryptAES_CBC(key, Convert.FromHexString("11223345"), iv);
        Assert.That(Convert.ToHexStringLower(ct1), Is.EqualTo("fbe0a340bbc33f2bbda63fe94a3d71b2"));
        Assert.That(Convert.ToHexStringLower(ct2), Is.EqualTo("1b220f7f6b09d35543da0e76bed8dfed"));

        byte[] pt1 = Primitives.DecryptAES_CBC(key, ct1, iv);
        Assert.That(Convert.ToHexStringLower(pt1), Is.EqualTo("11223344"));

        byte[] buffer1 = new byte[ct1.Length + 10];
        Array.Copy(ct1, 0, buffer1, 5, ct1.Length);
        byte[] pt1_buf = Primitives.DecryptAES_CBC(key, buffer1, 5, ct1.Length, iv);
        Assert.That(Convert.ToHexStringLower(pt1_buf), Is.EqualTo("11223344"));

        byte[] pt2 = Primitives.DecryptAES_CBC(key, ct2, iv);
        Assert.That(Convert.ToHexStringLower(pt2), Is.EqualTo("11223345"));

        byte[] buffer2 = new byte[ct2.Length + 10];
        Array.Copy(ct2, 0, buffer2, 5, ct2.Length);
        byte[] pt2_buf = Primitives.DecryptAES_CBC(key, buffer2, 5, ct2.Length, iv);
        Assert.That(Convert.ToHexStringLower(pt2_buf), Is.EqualTo("11223345"));
    }

    [Test]
    public void Test_EncryptAES_192bitKey()
    {
        byte[] key = Convert.FromHexString("130695aa06843d53a743966eebec6f7ed7d4bd9db10d499a");
        byte[] iv = Convert.FromHexString("2c3785531d412a817d980c1974281226");

        byte[] ct1 = Primitives.EncryptAES_CBC(key, Convert.FromHexString("11223344"), iv);
        Assert.That(Convert.ToHexStringLower(ct1), Is.EqualTo("bd7986a78059eeac7b0aeaa8f165ab6a"));

        byte[] pt1 = Primitives.DecryptAES_CBC(key, ct1, iv);
        Assert.That(Convert.ToHexStringLower(pt1), Is.EqualTo("11223344"));

        byte[] buffer = new byte[ct1.Length + 10];
        Array.Copy(ct1, 0, buffer, 5, ct1.Length);
        byte[] pt2 = Primitives.DecryptAES_CBC(key, buffer, 5, ct1.Length, iv);
        Assert.That(Convert.ToHexStringLower(pt2), Is.EqualTo("11223344"));
    }

    [Test]
    public void Test_EncryptAES_256bitKey()
    {
        byte[] key = Convert.FromHexString("9b9111948a32b817dee2dd6a513b7064c19b1aa3df43395600b762fea8b2b29b");
        byte[] iv = Convert.FromHexString("0ee7f5ecffc33809732c63d764ea82ec");

        byte[] ct1 = Primitives.EncryptAES_CBC(key, Convert.FromHexString("11223344"), iv);
        Assert.That(Convert.ToHexStringLower(ct1), Is.EqualTo("e375fcb6ac2ed41ead39d5044baea79d"));

        byte[] pt1 = Primitives.DecryptAES_CBC(key, ct1, iv);
        Assert.That(Convert.ToHexStringLower(pt1), Is.EqualTo("11223344"));

        byte[] buffer = new byte[ct1.Length + 10];
        Array.Copy(ct1, 0, buffer, 5, ct1.Length);
        byte[] pt2 = Primitives.DecryptAES_CBC(key, buffer, 5, ct1.Length, iv);
        Assert.That(Convert.ToHexStringLower(pt2), Is.EqualTo("11223344"));
    }

    [Test]
    public void Test_SecureEquals()
    {
        Assert.That(Primitives.SecureEquals(Convert.FromHexString("11"), Convert.FromHexString("11")));
        Assert.That(Primitives.SecureEquals(Convert.FromHexString("112233445566"), Convert.FromHexString("112233445566")));

        Assert.That(Primitives.SecureEquals(Convert.FromHexString("aaaa11aaaa"), 2, 1, Convert.FromHexString("bb11ccddee"), 1, 1));
        Assert.That(Primitives.SecureEquals(Convert.FromHexString("aabbccdd112233445566aabb"), 4, 6, Convert.FromHexString("aabbccddeeff112233445566"), 6, 6));

        Assert.That(Primitives.SecureEquals(Convert.FromHexString("11"), Convert.FromHexString("33")), Is.False);
        Assert.That(Primitives.SecureEquals(Convert.FromHexString("112233445566"), Convert.FromHexString("2764592f2861")), Is.False);

        Assert.That(Primitives.SecureEquals(Convert.FromHexString("aaaa11aaaa"), 2, 1, Convert.FromHexString("bb33ccddee"), 1, 1), Is.False);
        Assert.That(Primitives.SecureEquals(Convert.FromHexString("aabbccdd112233445566aabb"), 4, 6, Convert.FromHexString("aabbccddeeff2764592f2861"), 6, 6), Is.False);

        Assert.That(Primitives.SecureEquals(Convert.FromHexString("11"), Convert.FromHexString("0d5dd463cb69c57a19799b95af5c3b4e")), Is.False);
        Assert.That(Primitives.SecureEquals(Convert.FromHexString("112233445566"), Convert.FromHexString("edea5ca8fd7985935897ce6e2868a7302764592f2861")), Is.False);

        Assert.That(Primitives.SecureEquals(Convert.FromHexString("aaaa11aaaa"), 2, 1, Convert.FromHexString("bb0d5dd463cb69c57a19799b95af5c3b4eccddee"), 1, 16), Is.False);
        Assert.That(Primitives.SecureEquals(Convert.FromHexString("aabbccdd112233445566aabb"), 4, 6, Convert.FromHexString("aabbccddeeffedea5ca8fd7985935897ce6e2868a7302764592f2861"), 6, 16), Is.False);
    }
}
