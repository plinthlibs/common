using Plinth.Security.Crypto;
using NUnit.Framework;
using System.Diagnostics;
using System.Text;

namespace Tests.Plinth.Security.Crypto;

[TestFixture]
public class PBKDF2PasswordHasherTest
{
    [Test]
    public void Test_HashPassword()
    {
        var ph = new PBKDF2PasswordHasher(0xff);

        string password = "Password01";

        string hash = ph.HashPassword(password);
        string hash2 = ph.HashPassword(password);

        Assert.That(hash, Is.Not.EqualTo(hash2));

        Assert.That(ph.VerifyPasswordHash(password, hash));
        Assert.That(ph.VerifyPasswordHash(password, hash2));

        string hash3 = ph.HashPassword(password + "2");
        Assert.That(ph.VerifyPasswordHash(password, hash3), Is.False);
    }

    [Test]
    public void Test_HashPassword_InvalidIterations()
    {
        var ph = new PBKDF2PasswordHasher(0xff);

        string password = "Password01";
        string hash = ph.HashPassword(password);

        var sb = new StringBuilder(hash);
        sb[1] = '2';

        Assert.That(ph.VerifyPasswordHash(password, sb.ToString()), Is.False);
    }

    [Test]
    public void Test_HashPassword_InvalidData()
    {
        var ph = new PBKDF2PasswordHasher(0xff);

        string password = "Password01";

        Assert.That(ph.VerifyPasswordHash(password, null), Is.False);
        Assert.That(ph.VerifyPasswordHash(password, ""), Is.False);
        Assert.That(ph.VerifyPasswordHash(password, "01"), Is.False);
        Assert.That(ph.VerifyPasswordHash(password, "0102030405"), Is.False);
        Assert.That(ph.VerifyPasswordHash(password, "010102030405060708091011121314151617181920"), Is.False);
        Assert.That(ph.VerifyPasswordHash(password, "01010203040506070809101112131415161718192021222324252627282930313233343536373839"), Is.False);
        Assert.That(ph.VerifyPasswordHash(password, "010102030405060708091011121314151617181920212223242526272829303132333435363738394041"), Is.False);
    }

    [Test]
    public void Test_HashPassword_RealIterations()
    {
        var sds = new[]
        {
            (new PBKDF2PasswordHasher(1), 82),
            (new PBKDF2PasswordHasher(2), 82),
            (new PBKDF2PasswordHasher(3), 130),
            (new PBKDF2PasswordHasher(4), 130),
            (new PBKDF2PasswordHasher(5), 130)
        };

        string password = "Password01";

        int c = 0;
        foreach (var sd in sds)
        {
            Stopwatch sw = Stopwatch.StartNew();
            string hash = sd.Item1.HashPassword(password);
            sw.Stop();
            Assert.That(hash, Has.Length.EqualTo(sd.Item2));
            Console.WriteLine($"{c}: {sw.Elapsed}");

            foreach (var sdv in sds)
                Assert.That(sdv.Item1.VerifyPasswordHash(password, hash));

            c++;
        }
    }

    [Test]
    public void Test_Invalid()
    {
        Assert.Throws<ArgumentException>(() => new PBKDF2PasswordHasher(200));
    }

    [Test]
    public void Test_HashPassword_Legacy()
    {
        var sds = new[]
        {
            (new PBKDF2PasswordHasher(1), 82),
            (new PBKDF2PasswordHasher(2), 82),
            (new PBKDF2PasswordHasher(3), 130),
            (new PBKDF2PasswordHasher(4), 130),
            (new PBKDF2PasswordHasher(5), 130)
        };

        string password = "Password01";

        foreach (var sd1 in sds)
        {
            var hash = sd1.Item1.HashPassword(password);
            foreach (var sd2 in sds)
                Assert.That(sd2.Item1.VerifyPasswordHash(password, hash));
        }
    }
}
