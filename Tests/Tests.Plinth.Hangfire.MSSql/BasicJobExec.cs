using Microsoft.Extensions.Logging;
using Plinth.Hangfire;
using Plinth.Serialization;

namespace Tests.Plinth.Hangfire;

public class Dep
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    public string X {get;}

    public Dep(string x)
    {
        log.Debug($"x = {x}");
        X = x;
    }
}

public class BasicJobExec(Dep d) : IAsyncJob
{
    private static readonly ILogger log = StaticLogManager.GetLogger();
    private readonly HttpClient _httpClient = new HttpClient();

    private class MyMetaData
    {
        public int Meta1 { get; }
    }

    public async Task ExecuteAsync(PlinthJob job)
    {
        var md = JsonUtil.DeserializeObject<MyMetaData>(job.JobData!) ?? throw new Exception();
        log.Debug($"in job {job.JobData}, meta = {md.Meta1}, dep = {d.X}!");
        var res = await _httpClient.GetAsync("https://google.com");
        log.Debug($"got response from google: {res.StatusCode}");
    }
}
