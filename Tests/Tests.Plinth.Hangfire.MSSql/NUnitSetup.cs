using NUnit.Framework;
using Plinth.Logging.Host;

namespace Tests.Plinth;

[SetUpFixture]
public class NUnitSetup
{
    [OneTimeSetUp]
    public void Setup()
    {
        StaticLogManagerSetup.ConfigureForDebugLogging();
    }
}
