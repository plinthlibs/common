using Microsoft.Extensions.Logging;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Plinth.Hangfire;
using Tests.Plinth.Hangfire.Integration;
using Plinth.AspNetCore.AutoEndpoints.Diagnostics.Connectivity;
using Plinth.Logging.NLog.AppInsights;
using Microsoft.AspNetCore.Hosting;
#if SQLSERVER
using Plinth.Database.MSSql;
#elif PGSQL
using Plinth.Database.PgSql;
#endif

namespace Tests.Plinth.Hangfire;

public class Startup
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    public IConfiguration Configuration { get; }

    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public void ConfigureServices(IServiceCollection services)
    {
        log.Info("ConfigureServices()");

        ContainerBootstrap.Register(services);
        services.AddTransient<BasicJobExec>();

        services.AddPlinthAppInsightsTelemetry(c =>
        {
            c.EnabledPrefixes = ["/api"];
            c.CaptureSqlCommandText = true;
        });

        // hangfire
        IntegrationTestSetup.SetUpTest();
        services.AddPlinthHangfire(IntegrationTestSetup.DbConnection, r => r.RegisterAsyncGlobalHandler<BasicJobExec>());
        // hangfire

        services.AddPlinthServices(e =>
        {
            e.EnableServiceLogging();
            e.AutoEndpoints
                 .EnableVersion(c =>
                 {
                     // just an example, will useually use ISqlTransactionProvider
                     c.SetDbVersionResolver<ISqlTransactionProvider>(txn => txn.ExecuteRawTxn(cn => cn.ExecuteRawQueryOne("SELECT '1.25' AS Version", r => r.GetString("Version"))).Value!);
                 })
                 .EnableDiagnostics(c =>
                 {
                     c.SetConnectivityTesters(
                         new ConnectivityTesters.HttpHead("google", "https://www.google.com")
                     );
                 });
        });
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime applicationLifetime)
    {
        log.Info($"Configure app={env.ApplicationName}, dev={env.IsDevelopment()}");

        applicationLifetime.ApplicationStopping.Register(async () =>
        {
            IntegrationTestSetup.CleanUpTest();
            await IntegrationTestSetup.CleanUpDatabase();
        });

        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }

        app.UseRouting();

        app.UsePlinthServices();

        app.UseStaticFiles();

        // hangfire
        app.UsePlinthHangfire("");
        // hangfire
    }
}

