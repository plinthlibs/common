using Plinth.Hangfire;
using NUnit.Framework;

namespace Tests.Plinth.Hangfire.Logic;

[TestFixture]
public class CronHelperTests
{
    [Test]
    public void Test_EveryNSeconds()
    {
        Assert.That(CronHelper.EveryNSeconds(5), Is.EqualTo("*/5 * * * * *"));
        Assert.That(CronHelper.EveryNSeconds(1), Is.EqualTo("*/1 * * * * *"));
        Assert.That(CronHelper.EveryNSeconds(59), Is.EqualTo("*/59 * * * * *"));

        Assert.Throws<ArgumentOutOfRangeException>(() => CronHelper.EveryNSeconds(0));
        Assert.Throws<ArgumentOutOfRangeException>(() => CronHelper.EveryNSeconds(-1));
        Assert.Throws<ArgumentOutOfRangeException>(() => CronHelper.EveryNSeconds(60));
        Assert.Throws<ArgumentOutOfRangeException>(() => CronHelper.EveryNSeconds(61));
    }

    [Test]
    public void Test_Every_x_Seconds()
    {
        Assert.That(CronHelper.Every15Seconds(), Is.EqualTo("*/15 * * * * *"));
        Assert.That(CronHelper.Every30Seconds(), Is.EqualTo("*/30 * * * * *"));
    }

    [Test]
    public void Test_MinutelySeconds()
    {
        Assert.That(CronHelper.MinutelyAtSeconds(30), Is.EqualTo("30 * * * * *"));
        Assert.That(CronHelper.MinutelyAtSeconds(1), Is.EqualTo("1 * * * * *"));
        Assert.That(CronHelper.MinutelyAtSeconds(0), Is.EqualTo("0 * * * * *"));
        Assert.That(CronHelper.MinutelyAtSeconds(59), Is.EqualTo("59 * * * * *"));

        Assert.Throws<ArgumentOutOfRangeException>(() => CronHelper.MinutelyAtSeconds(-1));
        Assert.Throws<ArgumentOutOfRangeException>(() => CronHelper.MinutelyAtSeconds(60));
    }

    [Test]
    public void Test_MinutelySecondsList()
    {
        Assert.That(CronHelper.MinutelyAtSeconds([0]), Is.EqualTo("0 * * * * *"));
        Assert.That(CronHelper.MinutelyAtSeconds([1]), Is.EqualTo("1 * * * * *"));
        Assert.That(CronHelper.MinutelyAtSeconds([59]), Is.EqualTo("59 * * * * *"));
        Assert.That(CronHelper.MinutelyAtSeconds([0,5,10,15]), Is.EqualTo("0,5,10,15 * * * * *"));
        Assert.That(CronHelper.MinutelyAtSeconds([15,10,5,0]), Is.EqualTo("0,5,10,15 * * * * *"));
        Assert.That(CronHelper.MinutelyAtSeconds([7,28,14,21,0,35,49,42,56]), Is.EqualTo("0,7,14,21,28,35,42,49,56 * * * * *"));

        Assert.Throws<ArgumentOutOfRangeException>(() => CronHelper.MinutelyAtSeconds([]));
        Assert.Throws<ArgumentOutOfRangeException>(() => CronHelper.MinutelyAtSeconds([5, 7, -1]));
        Assert.Throws<ArgumentOutOfRangeException>(() => CronHelper.MinutelyAtSeconds([5, 7, 60]));
    }
}
