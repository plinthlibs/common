using Plinth.Hangfire.Impl;
using Plinth.Common.Utils;
using NUnit.Framework;

namespace Tests.Plinth.Hangfire.Logic;

[TestFixture]
public class TimeZoneLogicTest
{
    [Test]
    public void TestGetInfoFromTimeZone()
    {
        Assert.That(TimeZoneLogic.GetInfoFromTimeZone(null).Id, Is.EqualTo("UTC"));
        Assert.That(TimeZoneLogic.GetInfoFromTimeZone("not a valid timezone").Id, Is.EqualTo("UTC"));

        var tz = TimeZoneLogic.GetInfoFromTimeZone("America/Los_Angeles");// : "Pacific Standard Time");
        Assert.That(tz, Is.Not.Null);
        Assert.That(tz.BaseUtcOffset, Is.EqualTo(TimeSpan.FromHours(-8)));
        Assert.That(tz.SupportsDaylightSavingTime);

        tz = TimeZoneLogic.GetInfoFromTimeZone("America/New_York");// : "Eastern Standard Time");
        Assert.That(tz, Is.Not.Null);
        Assert.That(tz.BaseUtcOffset, Is.EqualTo(TimeSpan.FromHours(-5)));
        Assert.That(tz.SupportsDaylightSavingTime);

        tz = TimeZoneLogic.GetInfoFromTimeZone("Pacific Standard Time");
        Assert.That(tz, Is.Not.Null);
        Assert.That(tz.BaseUtcOffset, Is.EqualTo(TimeSpan.FromHours(-8)));
        Assert.That(tz.SupportsDaylightSavingTime);

        tz = TimeZoneLogic.GetInfoFromTimeZone("Eastern Standard Time");
        Assert.That(tz, Is.Not.Null);
        Assert.That(tz.BaseUtcOffset, Is.EqualTo(TimeSpan.FromHours(-5)));
        Assert.That(tz.SupportsDaylightSavingTime);
    }
}
