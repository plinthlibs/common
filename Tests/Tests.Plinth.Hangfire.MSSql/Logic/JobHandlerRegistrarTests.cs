﻿using Plinth.Hangfire;
using NUnit.Framework;

namespace Tests.Plinth.Hangfire.Logic;

[TestFixture]
public class JobHandlerRegistrarTests
{
    private class SyncHandler1 : ISyncJob
    {
        public void Execute(PlinthJob job) => throw new NotImplementedException();
    }

    private class SyncHandler2 : ISyncJob
    {
        public void Execute(PlinthJob job) => throw new NotImplementedException();
    }

    private class AsyncHandler1 : IAsyncJob
    {
        public Task ExecuteAsync(PlinthJob job) => throw new NotImplementedException();
    }

    private class AsyncHandler2 : IAsyncJob
    {
        public Task ExecuteAsync(PlinthJob job) => throw new NotImplementedException();
    }

    private class BothHandler : ISyncJob, IAsyncJob
    {
        public void Execute(PlinthJob job) => throw new NotImplementedException();
        public Task ExecuteAsync(PlinthJob job) => throw new NotImplementedException();
    }

    private void AssertHandler(JobHandlerRegistrar.Handler h, Type t, bool c, bool sync)
    {
        Assert.That(sync ? h.SyncAction : h.AsyncAction, Is.SameAs(t));
        Assert.That(h.AllowConcurrent, Is.EqualTo(c));
    }

    [Test]
    public void Test_RegisterHandlers()
    {
        var r = new JobHandlerRegistrar();
        r.RegisterHandler<SyncHandler1>("job1", allowConcurrent: false);
        r.RegisterHandler<SyncHandler1>("job2", allowConcurrent: true);
        r.RegisterAsyncHandler<AsyncHandler1>("job3", allowConcurrent: false);
        r.RegisterAsyncHandler<AsyncHandler1>("job4", allowConcurrent: true);
        r.RegisterGlobalHandler<SyncHandler2>(allowConcurrent: true);

        AssertHandler(r.GetHandlerFor("job1"), typeof(SyncHandler1), false, true);
        AssertHandler(r.GetHandlerFor("job2"), typeof(SyncHandler1), true, true);

        AssertHandler(r.GetHandlerFor("job3"), typeof(AsyncHandler1), false, false);
        AssertHandler(r.GetHandlerFor("job4"), typeof(AsyncHandler1), true, false);

        AssertHandler(r.GetHandlerFor("job_N"), typeof(SyncHandler2), true, true);
    }

    [Test]
    public void Test_RegisterBoth()
    {
        var r = new JobHandlerRegistrar();
        r.RegisterHandler<BothHandler>("job1", allowConcurrent: false);
        r.RegisterAsyncHandler<BothHandler>("job2", allowConcurrent: true);

        AssertHandler(r.GetHandlerFor("job1"), typeof(BothHandler), false, true);
        AssertHandler(r.GetHandlerFor("job2"), typeof(BothHandler), true, false);
    }

    [Test]
    public void Test_DuplicateSpecificReg()
    {
        var r = new JobHandlerRegistrar();
        r.RegisterHandler<SyncHandler1>("job1", allowConcurrent: false);
        Assert.Throws<NotSupportedException>(() => r.RegisterHandler<SyncHandler1>("job1"));
        Assert.Throws<NotSupportedException>(() => r.RegisterHandler<SyncHandler2>("job1"));
    }

    [Test]
    public void Test_DuplicateGlobalReg()
    {
        var r = new JobHandlerRegistrar();
        r.RegisterGlobalHandler<SyncHandler1>(allowConcurrent: false);
        Assert.Throws<NotSupportedException>(() => r.RegisterGlobalHandler<SyncHandler1>());
        Assert.Throws<NotSupportedException>(() => r.RegisterGlobalHandler<SyncHandler2>());
    }

    [Test]
    public void Test_SyncAsyncGlobalReg()
    {
        var r = new JobHandlerRegistrar();
        r.RegisterGlobalHandler<SyncHandler1>(allowConcurrent: false);
        Assert.Throws<NotSupportedException>(() => r.RegisterAsyncGlobalHandler<AsyncHandler1>());
        Assert.Throws<NotSupportedException>(() => r.RegisterAsyncGlobalHandler<AsyncHandler2>());
    }

    [Test]
    public void Test_AsyncSyncGlobalReg()
    {
        var r = new JobHandlerRegistrar();
        r.RegisterAsyncGlobalHandler<AsyncHandler1>(allowConcurrent: false);
        Assert.Throws<NotSupportedException>(() => r.RegisterGlobalHandler<SyncHandler1>());
        Assert.Throws<NotSupportedException>(() => r.RegisterGlobalHandler<SyncHandler2>());
    }

    [Test]
    public void Test_SyncAsyncReg()
    {
        var r = new JobHandlerRegistrar();
        r.RegisterHandler<SyncHandler1>("abc", allowConcurrent: false);
        Assert.Throws<NotSupportedException>(() => r.RegisterAsyncHandler<AsyncHandler1>("abc"));
        Assert.Throws<NotSupportedException>(() => r.RegisterAsyncHandler<AsyncHandler2>("abc"));
    }

    [Test]
    public void Test_AsyncSyncReg()
    {
        var r = new JobHandlerRegistrar();
        r.RegisterAsyncHandler<AsyncHandler1>("abc", allowConcurrent: false);
        Assert.Throws<NotSupportedException>(() => r.RegisterHandler<SyncHandler1>("abc"));
        Assert.Throws<NotSupportedException>(() => r.RegisterHandler<SyncHandler2>("abc"));
    }

    [Test]
    public void Test_NoReg()
    {
        var r = new JobHandlerRegistrar();

        Assert.Throws<NotSupportedException>(() => r.GetHandlerFor("job1"));

        r.RegisterHandler<SyncHandler1>("job2", allowConcurrent: false);

        Assert.Throws<NotSupportedException>(() => r.GetHandlerFor("job1"));
    }
}
