#if SQLSERVER
using Microsoft.Data.SqlClient;
#elif PGSQL
using Npgsql;
#endif
using Microsoft.Extensions.Logging;
using Hangfire;
using Plinth.Logging.NLog;
using Tests.Plinth.Hangfire.Integration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore;
using Plinth.Common.Utils;

namespace Tests.Plinth.Hangfire;

class Program
{
    private static readonly ILogger log = StaticLogManagerSetup.BasicNLogSetup();

    static async Task Main(string[] args)
    {
        log.Debug("startup");

        // hangfire test prep
        await IntegrationTestSetup.StartUpDatabase();
        IntegrationTestSetup.SetUpTest();
        InsertJobs();
        // hangfire test prep

        // run as windows service
        /*WindowsServiceRunner.RunWebHost(
            BuildWebHost,
            args,
            callbacks: new Dictionary<ServiceState, Action>()
            {
                [ServiceState.Stopping] = () =>
                {
                    IntegrationTestSetup.CleanUpTest();
                    IntegrationTestSetup.CleanUpDatabase();
                }
            });*/


        // run as a standard asp.net core app
        BuildWebHost(args).Run();

        IntegrationTestSetup.CleanUpTest();
        await IntegrationTestSetup.CleanUpDatabase();
    }

    public static IWebHost BuildWebHost(string[] args) =>
        WebHost.CreateDefaultBuilder(args)
            .UseStartup<Startup>()
            .UseContentRoot(RuntimeUtil.FindExecutableHome())
            .ConfigureLogging(builder => builder.AddNLog())
            .CaptureStartupErrors(false)
            .Build();

    // hangfire test prep
    private static void InsertJobs()
    {
        IntegrationTestSetup.Exec(c =>
        {
#if SQLSERVER
                c.ExecuteProc("usp_SubmitJob",
                    new SqlParameter("@Code", "Job1"),
                    new SqlParameter("@Description", "desc"),
                    new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                    new SqlParameter("@CronExpression", Cron.Minutely()),
                    new SqlParameter("@TimeZone", null),
                    new SqlParameter("@IsActive", true),
                    new SqlParameter("@CallingUser", "System")
                );

                c.ExecuteProc("usp_SubmitJob",
                    new SqlParameter("@Code", "Job2"),
                    new SqlParameter("@Description", "desc2"),
                    new SqlParameter("@JobData", "{ \"Meta1\": 99 }"),
                    new SqlParameter("@CronExpression", Cron.Minutely()),
                    new SqlParameter("@TimeZone", null),
                    new SqlParameter("@IsActive", true),
                    new SqlParameter("@CallingUser", "System"));

#elif PGSQL
                c.ExecuteProc("fn_submit_job",
                    new NpgsqlParameter("@i_code", "Job1"),
                    new NpgsqlParameter("@i_description", "desc"),
                    new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                    new NpgsqlParameter("@i_cron_expression", Cron.Minutely()),
                    new NpgsqlParameter("@i_time_zone", null),
                    new NpgsqlParameter("@i_is_active", true),
                    new NpgsqlParameter("@i_calling_user", "System")
                );

                c.ExecuteProc("fn_submit_job",
                    new NpgsqlParameter("@i_code", "Job2"),
                    new NpgsqlParameter("@i_description", "desc2"),
                    new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 99 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                    new NpgsqlParameter("@i_cron_expression", Cron.Minutely()),
                    new NpgsqlParameter("@i_time_zone", null),
                    new NpgsqlParameter("@i_is_active", true),
                    new NpgsqlParameter("@i_calling_user", "System")
                );

#endif
        });
    }
    // hangfire test prep
}

