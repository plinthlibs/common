
-- Plinth Job table
CREATE TABLE dbo.Job (
    Id INT IDENTITY(1,1) NOT NULL,          -- auto-indentity PK, should not be used directly
    Code VARCHAR(100) NOT NULL,             -- unique textual code for the job (e.g. 'MyStuff.DoSomething')
    [Description] NVARCHAR(255) NOT NULL,   -- textual description, short enough to be displayed in UI (e.g. 'My very first job for the demo purposes')
    JobData NVARCHAR(4000) NOT NULL,        -- additional metadata for the job (e.g. 'UseThisFlag', '{ applyRules: true, maxLimit: 10 }', etc.)
    CronExpression VARCHAR(50) NOT NULL,    -- CRON expression for the schedule (e.g. '0 */4 * * *')
    TimeZone VARCHAR(100) NOT NULL,         -- time zone name for the schedule (e.g. 'Pacific Standard Time', 'UTC', etc.)
    IsActive BIT NOT NULL,                  -- whether the job is currently active
    -- audit fields
    DateInserted DATETIME2(3) NOT NULL,
    InsertedBy NVARCHAR(255) NOT NULL,
    DateUpdated DATETIME2(3) NOT NULL,
    UpdatedBy NVARCHAR(255) NOT NULL,
    -- PK and unique code
    CONSTRAINT PK_Job PRIMARY KEY (Id),
    CONSTRAINT UX_Job_Code UNIQUE (Code)
)
-- Plinth Job table
