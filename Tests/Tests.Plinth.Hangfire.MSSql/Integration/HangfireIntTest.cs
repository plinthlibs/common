// uncomment to run
//#define RUN_INT_TESTS
using System.Collections.Concurrent;
using Hangfire;
using Hangfire.Storage;
using Plinth.Serialization;
using NUnit.Framework;
using Plinth.Hangfire;
using Microsoft.AspNetCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
#if SQLSERVER
using Microsoft.Data.SqlClient;
#elif PGSQL
using Npgsql;
#endif
using Hangfire.Dashboard;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Hosting;
using Plinth.Common.Utils;
using Microsoft.Extensions.Configuration;
using System.Reflection;
using Microsoft.AspNetCore.Http;

namespace Tests.Plinth.Hangfire.Integration;

#if !RUN_INT_TESTS
[AttributeUsage(AttributeTargets.Method)]
sealed class TestAttribute : Attribute { }
[AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
public sealed class TestCaseAttribute : Attribute { public TestCaseAttribute(params object[] _) { } }
#endif

// warning, these tests take many minutes because hangfire is limited to 1 minute intervals
[TestFixture]
#if !RUN_INT_TESTS
[System.Diagnostics.CodeAnalysis.SuppressMessage("Structure", "NUnit1028:The non-test method is public", Justification = "<Pending>")]
#endif
class HangfireIntTest
{
    private const string ThirtyMins = "*/30 * * * *";

    [OneTimeSetUp]
    public async Task SetUp()
    {
        await IntegrationTestSetup.StartUpDatabase();
    }

    [OneTimeTearDown]
    public async Task TearDown()
    {
        await IntegrationTestSetup.CleanUpDatabase();
    }

    [SetUp]
    public void TestSetUp()
    {
        IntegrationTestSetup.SetUpTest();
    }

    [TearDown]
    public void TestTearDown()
    {
        IntegrationTestSetup.CleanUpTest();
    }

    public class BasicJobExec : ISyncJob, IAsyncJob
    {
        private static readonly ILogger log = StaticLogManager.GetLogger();

        public static int SyncCalledCount = 0;
        public static int AsyncCalledCount = 0;

        public static string AsyncExpectedJob = "Job1";

        private class MyMetaData
        {
            public int Meta1 { get; set; }
        }

        public static void Reset()
        {
            AsyncExpectedJob = "Job1";
            SyncCalledCount = 0;
            AsyncCalledCount = 0;
        }

        public void Execute(PlinthJob job)
        {
            log.Info("In sync Execute");
            
            var md = JsonUtil.DeserializeObject<MyMetaData>(job.JobData!) ?? throw new Exception();

            try
            {
                Assert.That(job.Description, Is.EqualTo("desc"));
                Assert.That(job.Code, Is.EqualTo("Job1"));
                Assert.That(job.IsActive);
                Assert.That(md.Meta1, Is.EqualTo(55));
                Assert.That(job.CronExpression, Is.Not.Null);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            log.Info($"job called {job.Code} {job.JobData} {md.Meta1}, Called = {SyncCalledCount}");
            Interlocked.Increment(ref SyncCalledCount);
            log.Info($"Called Now = {SyncCalledCount}");
        }

        public async Task ExecuteAsync(PlinthJob job)
        {
            log.Info("In async Execute");

            await Task.Yield();
            var md = JsonUtil.DeserializeObject<MyMetaData>(job.JobData!) ?? throw new Exception();

            try
            {
                Assert.That(job.Description, Is.EqualTo("desc"));
                Assert.That(job.Code, Is.EqualTo(AsyncExpectedJob));
                Assert.That(job.IsActive);
                Assert.That(md.Meta1, Is.EqualTo(55));
                Assert.That(job.CronExpression, Is.Not.Null);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            log.Info($"async job called {job.Code} {job.JobData} {md.Meta1}, Called = {AsyncCalledCount}");
            Interlocked.Increment(ref AsyncCalledCount);
            log.Info($"async Called Now = {AsyncCalledCount}");
        }
    }

    public static void WaitTilNextMinute()
    {
        // start near the next minute to normalize when the first job runs
        while (DateTime.UtcNow.Second < 50)
            Thread.Sleep(TimeSpan.FromSeconds(1));
    }

    public static void WaitTilNextHalfMinute()
    {
        // start near the next minute to normalize when the first job runs
        int tgt = 20;
        if (DateTime.UtcNow.Second > 20)
            tgt = 50;
        while (DateTime.UtcNow.Second < tgt)
            Thread.Sleep(TimeSpan.FromSeconds(1));
    }

    public static void WaitTilNextSecond(int nextMod)
    {
        // start near the next second to normalize when the first job runs
        while (DateTime.UtcNow.Second % 10 != nextMod)
            Thread.Sleep(TimeSpan.FromSeconds(1));
    }

    private static void Run(Action<IServiceProvider, IApplicationBuilder> a, Action<JobHandlerRegistrar> reg, string? dashboardPath = "", DashboardOptions? opts = null)
    {
        var config = new ConfigurationBuilder().AddCommandLine(["--urls", "http://localhost:9272"]).Build();
        var host =
            WebHost.CreateDefaultBuilder()
                .UseConfiguration(config)
                .ConfigureServices(sc =>
                {
                    sc.AddPlinthHangfireImpl(IntegrationTestSetup.DbConnection, reg, false, null, Cron.Minutely(),
                        o => o.SchedulePollingInterval = TimeSpan.FromMilliseconds(250));
                    sc.AddSingleton(IntegrationTestSetup.GetProvider());
                    sc.AddTransient<BasicJobExec>();
                    sc.AddTransient<MultiCount>();

                    sc.AddAuthenticationCore();
                })
                .Configure(app =>
                {
                    app.UseAuthentication();
                    app.UsePlinthHangfire(dashboardPath, dashboardOpts: opts);
                    app.StartPlinthSync(Cron.Minutely(), CancellationToken.None);
                    app.ApplicationServices.GetRequiredService<IJobSyncManager>().Sync();
                    Task.Run(() =>
                    {
                        try
                        {
                            a(app.ApplicationServices, app);
                        }
                        catch (Exception e)
                        {
                            Assert.Fail($"Exception occured: {e.Message}\r\n{e}");
                        }
                        finally
                        {
                            app.ApplicationServices.GetRequiredService<IHostApplicationLifetime>().StopApplication();
                        }
                    });
                })
                .UseContentRoot(RuntimeUtil.FindExecutableHome())
                .CaptureStartupErrors(false)
                .Build();
        host.Start();
        host.WaitForShutdown();
    }

#region Test Basic
    [Test]
    [TestCase(true)]
    [TestCase(false)]
    public void TestBasicHangfire(bool isSync)
    {
        BasicJobExec.Reset();

#region insert jobs
        IntegrationTestSetup.Exec(c =>
        {
#if SQLSERVER
            c.ExecuteProc("usp_SubmitJob",
                new SqlParameter("@Code", "Job1"),
                new SqlParameter("@Description", "desc"),
                new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                new SqlParameter("@CronExpression", CronHelper.EveryNSeconds(5)),
                new SqlParameter("@TimeZone", null),
                new SqlParameter("@IsActive", true),
                new SqlParameter("@CallingUser", "System")
            );
#elif PGSQL
            c.ExecuteProc("fn_submit_job",
                new NpgsqlParameter("@i_code", "Job1"),
                new NpgsqlParameter("@i_description", "desc"),
                new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                new NpgsqlParameter("@i_cron_expression", CronHelper.EveryNSeconds(5)),
                new NpgsqlParameter("@i_time_zone", null),
                new NpgsqlParameter("@i_is_active", true),
                new NpgsqlParameter("@i_calling_user", "System")
            );
#endif
        });
#endregion

        WaitTilNextSecond(1);
        // 0......5.......10.......15
        //  cfg.----sleep-12----
        //        Run     Run

        Run((sc, app) =>
        {
            var rjm = sc.GetRequiredService<IRecurringJobManager>();
            rjm.AddOrUpdate("should-be-removed", () => Console.WriteLine("a"), ThirtyMins);
            var jobStorage = sc.GetRequiredService<JobStorage>();
            using (var conn = jobStorage.GetConnection())
            {
                var hJobs = conn.GetRecurringJobs().Select(j => j.Id).ToHashSet();
                Assert.That(hJobs, Has.Count.EqualTo(3));
                Assert.That(hJobs, Does.Contain(JobSyncConstants.JobListSyncJobName));
            }

            sc.GetRequiredService<IJobSyncManager>().Sync();

            using (var conn = jobStorage.GetConnection())
            {
                var hJobs = conn.GetRecurringJobs().Select(j => j.Id).ToHashSet();
                Assert.That(hJobs, Has.Count.EqualTo(2));
                Assert.That(hJobs, Does.Not.Contain("should-be-removed"));
                Assert.That(hJobs, Does.Contain("Job1"));
                Assert.That(hJobs, Does.Contain(JobSyncConstants.JobListSyncJobName));
            }

            Thread.Sleep(TimeSpan.FromSeconds(12));

            Assert.That(BasicJobExec.SyncCalledCount, Is.EqualTo(isSync ? 2 : 0));
            Assert.That(BasicJobExec.AsyncCalledCount, Is.EqualTo(isSync ? 0 : 2));
        },
        r =>
        {
            if (isSync)
                r.RegisterGlobalHandler<BasicJobExec>(true);
            else
                r.RegisterAsyncGlobalHandler<BasicJobExec>(true);
        });
    }

    [Test]
    public void Test_TwoJobs()
    {
        BasicJobExec.Reset();
        BasicJobExec.AsyncExpectedJob = "Job2";

#region insert jobs
        IntegrationTestSetup.Exec(c =>
            {
#if SQLSERVER
                c.ExecuteProc("usp_SubmitJob",
                    new SqlParameter("@Code", "Job1"),
                    new SqlParameter("@Description", "desc"),
                    new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                    new SqlParameter("@CronExpression", CronHelper.EveryNSeconds(5)),
                    new SqlParameter("@TimeZone", null),
                    new SqlParameter("@IsActive", true),
                    new SqlParameter("@CallingUser", "System")
                );

                c.ExecuteProc("usp_SubmitJob",
                    new SqlParameter("@Code", "Job2"),
                    new SqlParameter("@Description", "desc"),
                    new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                    new SqlParameter("@CronExpression", CronHelper.EveryNSeconds(5)),
                    new SqlParameter("@TimeZone", null),
                    new SqlParameter("@IsActive", true),
                    new SqlParameter("@CallingUser", "System")
                );
#elif PGSQL
                c.ExecuteProc("fn_submit_job",
                    new NpgsqlParameter("@i_code", "Job1"),
                    new NpgsqlParameter("@i_description", "desc"),
                    new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                    new NpgsqlParameter("@i_cron_expression", CronHelper.EveryNSeconds(5)),
                    new NpgsqlParameter("@i_time_zone", null),
                    new NpgsqlParameter("@i_is_active", true),
                    new NpgsqlParameter("@i_calling_user", "System")
                );

                c.ExecuteProc("fn_submit_job",
                    new NpgsqlParameter("@i_code", "Job2"),
                    new NpgsqlParameter("@i_description", "desc"),
                    new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                    new NpgsqlParameter("@i_cron_expression", CronHelper.EveryNSeconds(5)),
                    new NpgsqlParameter("@i_time_zone", null),
                    new NpgsqlParameter("@i_is_active", true),
                    new NpgsqlParameter("@i_calling_user", "System")
                );
#endif
            });
#endregion

        WaitTilNextSecond(1);
        // 0......5......10......15
        //  cfg.----sleep-12---
        // J1     Run    Run
        // J2     Run    Run

        Run((sc, app) =>
        {
            var jobStorage = sc.GetRequiredService<JobStorage>();
            using (var conn = jobStorage.GetConnection())
            {
                var hJobs = new HashSet<string>(conn.GetRecurringJobs().Select(j => j.Id));
                Assert.That(hJobs, Has.Count.EqualTo(3));
                Assert.That(hJobs, Does.Contain("Job1"));
                Assert.That(hJobs, Does.Contain("Job2"));
                Assert.That(hJobs, Does.Contain(JobSyncConstants.JobListSyncJobName));
            }

            Thread.Sleep(TimeSpan.FromSeconds(12));

            Assert.That(BasicJobExec.SyncCalledCount, Is.EqualTo(2));
            Assert.That(BasicJobExec.AsyncCalledCount, Is.EqualTo(2));
        },
        r =>
        {
            r.RegisterHandler<BasicJobExec>("Job1");
            r.RegisterAsyncHandler<BasicJobExec>("Job2");
        });
    }


    [Test]
    public void Test_TwoJobs_DiffSched()
    {
        BasicJobExec.Reset();
        BasicJobExec.AsyncExpectedJob = "Job2";

#region insert jobs
        IntegrationTestSetup.Exec(c =>
            {
#if SQLSERVER
                c.ExecuteProc("usp_SubmitJob",
                    new SqlParameter("@Code", "Job1"),
                    new SqlParameter("@Description", "desc"),
                    new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                    new SqlParameter("@CronExpression", CronHelper.EveryNSeconds(5)),
                    new SqlParameter("@TimeZone", null),
                    new SqlParameter("@IsActive", true),
                    new SqlParameter("@CallingUser", "System")
                );

                c.ExecuteProc("usp_SubmitJob",
                    new SqlParameter("@Code", "Job2"),
                    new SqlParameter("@Description", "desc"),
                    new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                    new SqlParameter("@CronExpression", CronHelper.MinutelyAtSeconds([8, 18, 28, 38, 48, 58])),
                    new SqlParameter("@TimeZone", null),
                    new SqlParameter("@IsActive", true),
                    new SqlParameter("@CallingUser", "System")
                );
#elif PGSQL
                c.ExecuteProc("fn_submit_job",
                    new NpgsqlParameter("@i_code", "Job1"),
                    new NpgsqlParameter("@i_description", "desc"),
                    new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                    new NpgsqlParameter("@i_cron_expression", CronHelper.EveryNSeconds(5)),
                    new NpgsqlParameter("@i_time_zone", null),
                    new NpgsqlParameter("@i_is_active", true),
                    new NpgsqlParameter("@i_calling_user", "System")
                );

                c.ExecuteProc("fn_submit_job",
                    new NpgsqlParameter("@i_code", "Job2"),
                    new NpgsqlParameter("@i_description", "desc"),
                    new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                    new NpgsqlParameter("@i_cron_expression", CronHelper.MinutelyAtSeconds([8, 18, 28, 38, 48, 58])),
                    new NpgsqlParameter("@i_time_zone", null),
                    new NpgsqlParameter("@i_is_active", true),
                    new NpgsqlParameter("@i_calling_user", "System")
                );
#endif
            });
#endregion

        WaitTilNextSecond(1);
        // 0......5...8..10......15
        //  cfg.----sleep-12---
        // J1     Run    Run
        // J2        Run

        Run((sc, app) =>
        {
            var jobStorage = sc.GetRequiredService<JobStorage>();
            using (var conn = jobStorage.GetConnection())
            {
                var hJobs = new HashSet<string>(conn.GetRecurringJobs().Select(j => j.Id));
                Assert.That(hJobs, Has.Count.EqualTo(3));
                Assert.That(hJobs, Does.Contain("Job1"));
                Assert.That(hJobs, Does.Contain("Job2"));
                Assert.That(hJobs, Does.Contain(JobSyncConstants.JobListSyncJobName));
            }

            Thread.Sleep(TimeSpan.FromSeconds(12));

            Assert.That(BasicJobExec.SyncCalledCount, Is.EqualTo(2));
            Assert.That(BasicJobExec.AsyncCalledCount, Is.EqualTo(1));
        },
        r =>
        {
            r.RegisterHandler<BasicJobExec>("Job1");
            r.RegisterAsyncHandler<BasicJobExec>("Job2");
        });
    }

    class NotRegistered : ISyncJob, IAsyncJob
    {
        public static bool SyncCalled = false;
        public static bool AsyncCalled = false;
        public static string? SyncJob = null, AsyncJob = null;

        public void Execute(PlinthJob job)
        {
            SyncCalled = true;
            SyncJob = job.Code;
        }

        public Task ExecuteAsync(PlinthJob job)
        {
            AsyncCalled = true;
            AsyncJob = job.Code;
            return Task.CompletedTask;
        }
    }

    class NotRegistered2 : ISyncJob, IAsyncJob
    {
        public static bool SyncCalled = false;
        public static bool AsyncCalled = false;
        public static string? SyncJob = null, AsyncJob = null;

        public NotRegistered2(BasicJobExec _)
        {
        }

        public void Execute(PlinthJob job)
        {
            SyncCalled = true;
            SyncJob = job.Code;
        }

        public Task ExecuteAsync(PlinthJob job)
        {
            AsyncCalled = true;
            AsyncJob = job.Code;
            return Task.CompletedTask;
        }
    }

    [Test]
    public void TestNoContainer()
    {
#region insert jobs
        IntegrationTestSetup.Exec(c =>
        {
#if SQLSERVER
            c.ExecuteProc("usp_SubmitJob",
                new SqlParameter("@Code", "Job1"),
                new SqlParameter("@Description", "desc"),
                new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                new SqlParameter("@CronExpression", CronHelper.EveryNSeconds(5)),
                new SqlParameter("@TimeZone", null),
                new SqlParameter("@IsActive", true),
                new SqlParameter("@CallingUser", "System")
            );
            c.ExecuteProc("usp_SubmitJob",
                new SqlParameter("@Code", "Job2"),
                new SqlParameter("@Description", "desc"),
                new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                new SqlParameter("@CronExpression", CronHelper.EveryNSeconds(5)),
                new SqlParameter("@TimeZone", null),
                new SqlParameter("@IsActive", true),
                new SqlParameter("@CallingUser", "System")
            );
#elif PGSQL
            c.ExecuteProc("fn_submit_job",
                new NpgsqlParameter("@i_code", "Job1"),
                new NpgsqlParameter("@i_description", "desc"),
                new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                new NpgsqlParameter("@i_cron_expression", CronHelper.EveryNSeconds(5)),
                new NpgsqlParameter("@i_time_zone", null),
                new NpgsqlParameter("@i_is_active", true),
                new NpgsqlParameter("@i_calling_user", "System")
            );
            c.ExecuteProc("fn_submit_job",
                new NpgsqlParameter("@i_code", "Job2"),
                new NpgsqlParameter("@i_description", "desc"),
                new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                new NpgsqlParameter("@i_cron_expression", CronHelper.EveryNSeconds(5)),
                new NpgsqlParameter("@i_time_zone", null),
                new NpgsqlParameter("@i_is_active", true),
                new NpgsqlParameter("@i_calling_user", "System")
            );
#endif
        });
#endregion

        WaitTilNextSecond(1);
        // 0......5............
        //  cfg.----sleep-6----
        //        Run    

        Run((sc, app) =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(6));

            Assert.That(NotRegistered.SyncCalled);
            Assert.That(NotRegistered.AsyncCalled);
        },
        r =>
        {
            r.RegisterHandler<NotRegistered>("Job1");
            r.RegisterAsyncHandler<NotRegistered>("Job2");
        });
    }

    [Test]
    public void TestNoContainerWithDeps()
    {
#region insert jobs
        IntegrationTestSetup.Exec(c =>
        {
#if SQLSERVER
            c.ExecuteProc("usp_SubmitJob",
                new SqlParameter("@Code", "Job1"),
                new SqlParameter("@Description", "desc"),
                new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                new SqlParameter("@CronExpression", CronHelper.EveryNSeconds(5)),
                new SqlParameter("@TimeZone", null),
                new SqlParameter("@IsActive", true),
                new SqlParameter("@CallingUser", "System")
            );
            c.ExecuteProc("usp_SubmitJob",
                new SqlParameter("@Code", "Job2"),
                new SqlParameter("@Description", "desc"),
                new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                new SqlParameter("@CronExpression", CronHelper.EveryNSeconds(5)),
                new SqlParameter("@TimeZone", null),
                new SqlParameter("@IsActive", true),
                new SqlParameter("@CallingUser", "System")
            );
#elif PGSQL
            c.ExecuteProc("fn_submit_job",
                new NpgsqlParameter("@i_code", "Job1"),
                new NpgsqlParameter("@i_description", "desc"),
                new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                new NpgsqlParameter("@i_cron_expression", CronHelper.EveryNSeconds(5)),
                new NpgsqlParameter("@i_time_zone", null),
                new NpgsqlParameter("@i_is_active", true),
                new NpgsqlParameter("@i_calling_user", "System")
            );
            c.ExecuteProc("fn_submit_job",
                new NpgsqlParameter("@i_code", "Job2"),
                new NpgsqlParameter("@i_description", "desc"),
                new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                new NpgsqlParameter("@i_cron_expression", CronHelper.EveryNSeconds(5)),
                new NpgsqlParameter("@i_time_zone", null),
                new NpgsqlParameter("@i_is_active", true),
                new NpgsqlParameter("@i_calling_user", "System")
            );
#endif
        });
#endregion

        WaitTilNextSecond(1);
        // 0......5............
        //  cfg.----sleep-6----
        //        Run    

        Run((sc, app) =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(6));

            Assert.That(NotRegistered2.SyncCalled);
            Assert.That(NotRegistered2.AsyncCalled);
        },
        r =>
        {
            r.RegisterHandler<NotRegistered2>("Job1");
            r.RegisterAsyncHandler<NotRegistered2>("Job2");
        });
    }

    [Test]
    public void Test_Handlers()
    {
#region insert jobs
        IntegrationTestSetup.Exec(c =>
        {
#if SQLSERVER
            c.ExecuteProc("usp_SubmitJob",
                new SqlParameter("@Code", "Job1"),
                new SqlParameter("@Description", "desc"),
                new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                new SqlParameter("@CronExpression", CronHelper.EveryNSeconds(5)),
                new SqlParameter("@TimeZone", null),
                new SqlParameter("@IsActive", true),
                new SqlParameter("@CallingUser", "System")
            );
            c.ExecuteProc("usp_SubmitJob",
                new SqlParameter("@Code", "Job2"),
                new SqlParameter("@Description", "desc"),
                new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                new SqlParameter("@CronExpression", CronHelper.EveryNSeconds(5)),
                new SqlParameter("@TimeZone", null),
                new SqlParameter("@IsActive", true),
                new SqlParameter("@CallingUser", "System")
            );
            c.ExecuteProc("usp_SubmitJob",
                new SqlParameter("@Code", "Job3"),
                new SqlParameter("@Description", "desc"),
                new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                new SqlParameter("@CronExpression", CronHelper.EveryNSeconds(5)),
                new SqlParameter("@TimeZone", null),
                new SqlParameter("@IsActive", true),
                new SqlParameter("@CallingUser", "System")
            );
#elif PGSQL
            c.ExecuteProc("fn_submit_job",
                new NpgsqlParameter("@i_code", "Job1"),
                new NpgsqlParameter("@i_description", "desc"),
                new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                new NpgsqlParameter("@i_cron_expression", CronHelper.EveryNSeconds(5)),
                new NpgsqlParameter("@i_time_zone", null),
                new NpgsqlParameter("@i_is_active", true),
                new NpgsqlParameter("@i_calling_user", "System")
            );
            c.ExecuteProc("fn_submit_job",
                new NpgsqlParameter("@i_code", "Job2"),
                new NpgsqlParameter("@i_description", "desc"),
                new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                new NpgsqlParameter("@i_cron_expression", CronHelper.EveryNSeconds(5)),
                new NpgsqlParameter("@i_time_zone", null),
                new NpgsqlParameter("@i_is_active", true),
                new NpgsqlParameter("@i_calling_user", "System")
            );
            c.ExecuteProc("fn_submit_job",
                new NpgsqlParameter("@i_code", "Job3"),
                new NpgsqlParameter("@i_description", "desc"),
                new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                new NpgsqlParameter("@i_cron_expression", CronHelper.EveryNSeconds(5)),
                new NpgsqlParameter("@i_time_zone", null),
                new NpgsqlParameter("@i_is_active", true),
                new NpgsqlParameter("@i_calling_user", "System")
            );
#endif
        });
#endregion

        WaitTilNextSecond(1);
        // 0......5............
        //  cfg.----sleep-6----
        // J1     Run    
        // J2     Run    
        // J3     Run    

        Run((sc, app) =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(6));

            Assert.That(NotRegistered.SyncCalled);
            Assert.That(NotRegistered.SyncJob, Is.EqualTo("Job1"));
            Assert.That(NotRegistered.AsyncCalled);
            Assert.That(NotRegistered.AsyncJob, Is.EqualTo("Job2"));

            Assert.That(NotRegistered2.SyncCalled, Is.False);
            Assert.That(NotRegistered2.SyncJob, Is.Null);
            Assert.That(NotRegistered2.AsyncCalled);
            Assert.That(NotRegistered2.AsyncJob, Is.EqualTo("Job3"));
        },
        r =>
        {
            r.RegisterHandler<NotRegistered>("Job1");
            r.RegisterAsyncHandler<NotRegistered>("Job2");
            r.RegisterAsyncGlobalHandler<NotRegistered2>();
        });
    }
#endregion

#region Allow/Disallow Multiple
    public class MultiCount : ISyncJob
    {
        private static readonly ILogger log = StaticLogManager.GetLogger();
        public static ConcurrentDictionary<int, int> ThreadCounts = new();

        public void Execute(PlinthJob job)
        {
            int thread = Environment.CurrentManagedThreadId;
            log.Info($"called {thread} {DateTimeOffset.UtcNow.ToUnixTimeSeconds()}");
            if (!ThreadCounts.TryGetValue(thread, out int count))
                ThreadCounts[thread] = 1;
            else
                ThreadCounts[thread] = count + 1;

            log.Info($"sleep {thread} {DateTimeOffset.UtcNow.ToUnixTimeSeconds()}");
            Thread.Sleep(TimeSpan.FromSeconds(6));
            log.Info($"exit {thread} {DateTimeOffset.UtcNow.ToUnixTimeSeconds()}");
        }
    }
    
    [Test]
    [TestCase(true)]
    [TestCase(false)]
    public void TestOverlap(bool allowConcurrent)
    {
        MultiCount.ThreadCounts.Clear();

#region insert jobs
        IntegrationTestSetup.Exec(c =>
        {
#if SQLSERVER
            c.ExecuteProc("usp_SubmitJob",
                new SqlParameter("@Code", "Job1"),
                new SqlParameter("@Description", "desc"),
                new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                new SqlParameter("@CronExpression", CronHelper.EveryNSeconds(5)),
                new SqlParameter("@TimeZone", null),
                new SqlParameter("@IsActive", true),
                new SqlParameter("@CallingUser", "System")
            );
#elif PGSQL
            c.ExecuteProc("fn_submit_job",
                new NpgsqlParameter("@i_code", "Job1"),
                new NpgsqlParameter("@i_description", "desc"),
                new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                new NpgsqlParameter("@i_cron_expression", CronHelper.EveryNSeconds(5)),
                new NpgsqlParameter("@i_time_zone", null),
                new NpgsqlParameter("@i_is_active", true),
                new NpgsqlParameter("@i_calling_user", "System")
            );
#endif
        });
#endregion

        WaitTilNextSecond(1);
        // 0......5......10......15......20......25
        //  cfg.----sleep-22-------------------
        // ac     Run=========   Run=========

        //!ac     Run=========   Run=========
        //               Run=========    Run==========

        Run((sc, app) =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(22));

            Assert.That(MultiCount.ThreadCounts.Sum(kv => kv.Value), Is.EqualTo(allowConcurrent ? 4 : 2));
        },
        r => r.RegisterGlobalHandler<MultiCount>(allowConcurrent));
    }

    [Test]
    [TestCase(true)]
    [TestCase(false)]
    public void TestMultiple_DifferentParams_JobCodeAndMeta(bool allowConcurrent)
    {
        MultiCount.ThreadCounts.Clear();

#region insert jobs
        IntegrationTestSetup.Exec(c =>
        {
#if SQLSERVER
            c.ExecuteProc("usp_SubmitJob",
                new SqlParameter("@Code", "Job1"),
                new SqlParameter("@Description", "desc"),
                new SqlParameter("@JobData", "{ \"Meta1\": 77 }"),
                new SqlParameter("@CronExpression", CronHelper.EveryNSeconds(5)),
                new SqlParameter("@TimeZone", null),
                new SqlParameter("@IsActive", true),
                new SqlParameter("@CallingUser", "System")
            );
            c.ExecuteProc("usp_SubmitJob",
                new SqlParameter("@Code", "Job2"),
                new SqlParameter("@Description", "desc"),
                new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                new SqlParameter("@CronExpression", CronHelper.EveryNSeconds(5)),
                new SqlParameter("@TimeZone", null),
                new SqlParameter("@IsActive", true),
                new SqlParameter("@CallingUser", "System")
            );
#elif PGSQL
            c.ExecuteProc("fn_submit_job",
                new NpgsqlParameter("@i_code", "Job1"),
                new NpgsqlParameter("@i_description", "desc"),
                new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                new NpgsqlParameter("@i_cron_expression", CronHelper.EveryNSeconds(5)),
                new NpgsqlParameter("@i_time_zone", null),
                new NpgsqlParameter("@i_is_active", true),
                new NpgsqlParameter("@i_calling_user", "System")
            );
            c.ExecuteProc("fn_submit_job",
                new NpgsqlParameter("@i_code", "Job2"),
                new NpgsqlParameter("@i_description", "desc"),
                new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 77 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                new NpgsqlParameter("@i_cron_expression", CronHelper.EveryNSeconds(5)),
                new NpgsqlParameter("@i_time_zone", null),
                new NpgsqlParameter("@i_is_active", true),
                new NpgsqlParameter("@i_calling_user", "System")
            );
#endif
        });
#endregion

        WaitTilNextSecond(1);
        // 0......5......10......15......20......25
        //  cfg.----sleep-22-------------------
        // J1     Run=========   Run=========
        // J2     Run=========   Run=========

        // J1     Run=========   Run=========
        // J1            Run=========   Run=========
        // J2     Run=========   Run=========
        // J2            Run=========   Run=========

        Run((sc, app) =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(22));

            Assert.That(MultiCount.ThreadCounts.Sum(kv => kv.Value), Is.EqualTo(allowConcurrent ? 8 : 4));
        },
        r => r.RegisterGlobalHandler<MultiCount>(allowConcurrent));
    }

    [Test]
    [TestCase(true)]
    [TestCase(false)]
    public void TestxMultiple_DifferentParams_JobCode(bool allowConcurrent)
    {
        MultiCount.ThreadCounts.Clear();

#region insert jobs
        IntegrationTestSetup.Exec(c =>
        {
#if SQLSERVER
            c.ExecuteProc("usp_SubmitJob",
                new SqlParameter("@Code", "Job1"),
                new SqlParameter("@Description", "desc"),
                new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                new SqlParameter("@CronExpression", CronHelper.EveryNSeconds(5)),
                new SqlParameter("@TimeZone", null),
                new SqlParameter("@IsActive", true),
                new SqlParameter("@CallingUser", "System")
            );
            c.ExecuteProc("usp_SubmitJob",
                new SqlParameter("@Code", "Job2"),
                new SqlParameter("@Description", "desc"),
                new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                new SqlParameter("@CronExpression", CronHelper.EveryNSeconds(5)),
                new SqlParameter("@TimeZone", null),
                new SqlParameter("@IsActive", true),
                new SqlParameter("@CallingUser", "System")
            );
#elif PGSQL
            c.ExecuteProc("fn_submit_job",
                new NpgsqlParameter("@i_code", "Job1"),
                new NpgsqlParameter("@i_description", "desc"),
                new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                new NpgsqlParameter("@i_cron_expression", CronHelper.EveryNSeconds(5)),
                new NpgsqlParameter("@i_time_zone", null),
                new NpgsqlParameter("@i_is_active", true),
                new NpgsqlParameter("@i_calling_user", "System")
            );
            c.ExecuteProc("fn_submit_job",
                new NpgsqlParameter("@i_code", "Job2"),
                new NpgsqlParameter("@i_description", "desc"),
                new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                new NpgsqlParameter("@i_cron_expression", CronHelper.EveryNSeconds(5)),
                new NpgsqlParameter("@i_time_zone", null),
                new NpgsqlParameter("@i_is_active", true),
                new NpgsqlParameter("@i_calling_user", "System")
            );
#endif
        });
#endregion

        WaitTilNextSecond(1);
        // 0......5......10......15......20......25
        //  cfg.----sleep-22-------------------
        // J1     Run=========   Run=========
        // J2     Run=========   Run=========

        // J1     Run=========   Run=========
        // J1            Run=========   Run=========
        // J2     Run=========   Run=========
        // J2            Run=========   Run=========

        Run((sc, app) =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(22));

            Assert.That(MultiCount.ThreadCounts.Sum(kv => kv.Value), Is.EqualTo(allowConcurrent ? 8 : 4));
        },
        r => r.RegisterGlobalHandler<MultiCount>(allowConcurrent));
    }

#endregion

#region Test Reschedule
    // this test confirms this bug is fixed (rescheduling an existing job in the past causes immediate re-run)
    // https://github.com/HangfireIO/Hangfire/issues/728
    [Test]
    public void TestReschedule()
    {
        BasicJobExec.Reset();

        WaitTilNextSecond(0);

        var now = DateTime.UtcNow;
        string cron1 = $"{now.Second + 3} {now.Minute} {now.Hour} * * *";

#region insert job
        IntegrationTestSetup.Exec(c =>
        {
#if SQLSERVER
            c.ExecuteProc("usp_SubmitJob",
                new SqlParameter("@Code", "Job1"),
                new SqlParameter("@Description", "desc"),
                new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                new SqlParameter("@CronExpression", cron1),
                new SqlParameter("@TimeZone", null),
                new SqlParameter("@IsActive", true),
                new SqlParameter("@CallingUser", "System")
            );
#elif PGSQL
            c.ExecuteProc("fn_submit_job",
                new NpgsqlParameter("@i_code", "Job1"),
                new NpgsqlParameter("@i_description", "desc"),
                new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                new NpgsqlParameter("@i_cron_expression", cron1),
                new NpgsqlParameter("@i_time_zone", null),
                new NpgsqlParameter("@i_is_active", true),
                new NpgsqlParameter("@i_calling_user", "System")
            );
#endif
        });
#endregion

        // 0.............3............8.....10
        //  cfg.---sleep-7-----x-----
        // J1            Run        resched for x

        Run((sc, app) =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(7));

            string cron2 = $"{now.Second + 5} {now.Minute} {now.Hour} * * *";
            IntegrationTestSetup.Exec(c =>
            {
#if SQLSERVER
                c.ExecuteProc("usp_SubmitJob",
                    new SqlParameter("@Code", "Job1"),
                    new SqlParameter("@CronExpression", cron2),
                    new SqlParameter("@CallingUser", "System")
                );
#elif PGSQL
                c.ExecuteProc("fn_submit_job",
                    new NpgsqlParameter("@i_code", "Job1"),
                    new NpgsqlParameter("@i_cron_expression", cron2),
                    new NpgsqlParameter("@i_calling_user", "System"));
#endif
            });

            sc.GetRequiredService<IJobSyncManager>().Sync();

            Thread.Sleep(TimeSpan.FromSeconds(5));

            Assert.That(BasicJobExec.SyncCalledCount, Is.EqualTo(1));
        },
        r => r.RegisterGlobalHandler<BasicJobExec>());
    }
#endregion

#region Test Reschedule during run
    // if the job was scheduled to run in between the remove and the add, it won't run
    [Test]
    public void TestRescheduleDuringRun_NoChanges()
    {
        BasicJobExec.Reset();

        WaitTilNextSecond(0);

        var now = DateTime.UtcNow;
        string cron1 = $"{now.Second + 3} {now.Minute} {now.Hour} * * *";

#region insert job
        IntegrationTestSetup.Exec(c =>
        {
#if SQLSERVER
            c.ExecuteProc("usp_SubmitJob",
                new SqlParameter("@Code", "Job1"),
                new SqlParameter("@Description", "desc"),
                new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                new SqlParameter("@CronExpression", cron1),
                new SqlParameter("@TimeZone", null),
                new SqlParameter("@IsActive", true),
                new SqlParameter("@CallingUser", "System")
            );
#elif PGSQL
            c.ExecuteProc("fn_submit_job",
                new NpgsqlParameter("@i_code", "Job1"),
                new NpgsqlParameter("@i_description", "desc"),
                new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                new NpgsqlParameter("@i_cron_expression", cron1),
                new NpgsqlParameter("@i_time_zone", null),
                new NpgsqlParameter("@i_is_active", true),
                new NpgsqlParameter("@i_calling_user", "System")
            );
#endif
        });
#endregion

        // 0.............3............8.....10
        //  cfg.---sleep-5-------
        // J1            Run    resched no change

        Run((sc, app) =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(5));

            IntegrationTestSetup.Exec(c =>
            {
#if SQLSERVER
                c.ExecuteProc("usp_SubmitJob",
                    new SqlParameter("@Code", "Job1"),
                    new SqlParameter("@CronExpression", cron1),
                    new SqlParameter("@JobData", "{ \"H\": 5 }"),
                    new SqlParameter("@CallingUser", "System")
                );
#elif PGSQL
                c.ExecuteProc("fn_submit_job",
                    new NpgsqlParameter("@i_code", "Job1"),
                    new NpgsqlParameter("@i_cron_expression", cron1),
                    new NpgsqlParameter("@i_calling_user", "System"));
#endif
            });

            sc.GetRequiredService<IJobSyncManager>().Sync();

            Thread.Sleep(TimeSpan.FromSeconds(5));

            Assert.That(BasicJobExec.SyncCalledCount, Is.EqualTo(1));
        },
        r => r.RegisterGlobalHandler<BasicJobExec>());
    }
#endregion

#region Test Dashboard Config 
    class TestDashboardAuthorizationFilter : IDashboardAuthorizationFilter
    {
        private static readonly ILogger log = StaticLogManager.GetLogger();

        public static int CallCount = 0;

        public bool Authorize(DashboardContext context)
        {
            log.Warn($"authorizing: {context.Request.Path} from {context.Request.RemoteIpAddress}");
            Console.WriteLine($"authorizing: {context.Request.Path} from {context.Request.RemoteIpAddress}");
            Interlocked.Increment(ref CallCount);
            if (OperatingSystem.IsWindows())
                return System.Security.Principal.WindowsIdentity.GetCurrent().IsAuthenticated;
            else
                return true;
        }
    }

    [Test]
    public void TestDashboard()
    {
        // NOTE!!: .AddAuthentication() or equivalent MUST be before AddPlinthHangfire()
        //         (see Run() above), or the authorization filter won't run

        BasicJobExec.Reset();
        var dashOpts = new DashboardOptions
        {
            AppPath = "http://www.plinthlibs.com",
            StatsPollingInterval = 5000,
            Authorization = [new TestDashboardAuthorizationFilter()],
            DashboardTitle = "My Job Dashboard",
            IsReadOnlyFunc = r => true
        };

#region insert job
        IntegrationTestSetup.Exec(c =>
        {
#if SQLSERVER
            c.ExecuteProc("usp_SubmitJob",
                new SqlParameter("@Code", "Job1"),
                new SqlParameter("@Description", "desc"),
                new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                new SqlParameter("@CronExpression", CronHelper.EveryNSeconds(5)),
                new SqlParameter("@TimeZone", null),
                new SqlParameter("@IsActive", true),
                new SqlParameter("@CallingUser", "System")
            );
#elif PGSQL
            c.ExecuteProc("fn_submit_job",
                new NpgsqlParameter("@i_code", "Job1"),
                new NpgsqlParameter("@i_description", "desc"),
                new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                new NpgsqlParameter("@i_cron_expression", CronHelper.EveryNSeconds(5)),
                new NpgsqlParameter("@i_time_zone", null),
                new NpgsqlParameter("@i_is_active", true),
                new NpgsqlParameter("@i_calling_user", "System")
            );
#endif
        });
#endregion

        WaitTilNextSecond(1);

        Run((sc, app) =>        {
            Thread.Sleep(TimeSpan.FromSeconds(12));
            using (HttpClient client = new())
            {
                var request = new HttpRequestMessage(HttpMethod.Get, "http://localhost:9272/dashboard");

                using var response = client.SendAsync(request).Result;
            }
            Thread.Sleep(TimeSpan.FromSeconds(80));

            Assert.That(TestDashboardAuthorizationFilter.CallCount, Is.GreaterThanOrEqualTo(1));
            Assert.That(BasicJobExec.SyncCalledCount, Is.EqualTo(18));
        }, 
        r => r.RegisterGlobalHandler<BasicJobExec>(),
        "/dashboard", 
        dashOpts);
    }

    [Test]
    public void TestDashboard_Disabled()
    {
        // NOTE!!: .AddAuthentication() or equivalent MUST be before AddPlinthHangfire()
        //         (see Run() above), or the authorization filter won't run

        BasicJobExec.Reset();

#region insert job
        IntegrationTestSetup.Exec(c =>
        {
#if SQLSERVER
            c.ExecuteProc("usp_SubmitJob",
                new SqlParameter("@Code", "Job1"),
                new SqlParameter("@Description", "desc"),
                new SqlParameter("@JobData", "{ \"Meta1\": 55 }"),
                new SqlParameter("@CronExpression", CronHelper.EveryNSeconds(5)),
                new SqlParameter("@TimeZone", null),
                new SqlParameter("@IsActive", true),
                new SqlParameter("@CallingUser", "System")
            );
#elif PGSQL
            c.ExecuteProc("fn_submit_job",
                new NpgsqlParameter("@i_code", "Job1"),
                new NpgsqlParameter("@i_description", "desc"),
                new NpgsqlParameter("@i_job_data", "{ \"Meta1\": 55 }") { NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json },
                new NpgsqlParameter("@i_cron_expression", CronHelper.EveryNSeconds(5)),
                new NpgsqlParameter("@i_time_zone", null),
                new NpgsqlParameter("@i_is_active", true),
                new NpgsqlParameter("@i_calling_user", "System")
            );
#endif
        });
#endregion

        WaitTilNextSecond(1);

        Run((sc, app) =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(2));
            var path = GetMappedPath(app);
            using HttpClient client = new();
            var request = new HttpRequestMessage(HttpMethod.Get, "http://localhost:9272" + path);

            using var response = client.SendAsync(request).Result;
            Assert.That((int)response.StatusCode, Is.EqualTo(StatusCodes.Status401Unauthorized));
        }, 
        r => r.RegisterGlobalHandler<BasicJobExec>(),
        null, 
        null);

        static string? GetMappedPath(IApplicationBuilder app)
        {
            FieldInfo _componentsField = app.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance).Single(pi => pi.Name == "_components");
            var _componentsValue =
                _componentsField.GetValue(app) as List<Func<RequestDelegate, RequestDelegate>>;
            return _componentsValue!.Select(x =>
                {
                    var mapOptions = x.Target!.GetType().GetRuntimeFields().FirstOrDefault(pi => pi.Name == "options");
                    if (mapOptions == null)
                        return null;
                    var opts = mapOptions.GetValue(x.Target);
                    if (opts == null)
                        return null;
                    return (opts as Microsoft.AspNetCore.Builder.Extensions.MapOptions)?.PathMatch.Value;
                }
            ).Where(x => x != null).FirstOrDefault();
        }
    }
    #endregion

    #region async setup
    [Test]
    public async Task Test_AsyncSetup()
    {
        var connStr =
#if SQLSERVER
            "Data Source=notavalidhost";
#elif PGSQL
            "Host=notavalidhost";
#endif

        var config = new ConfigurationBuilder().AddCommandLine(["--urls", "http://localhost:9272"]).Build();
        var host =
            WebHost.CreateDefaultBuilder()
                .UseConfiguration(config)
                .ConfigureServices(sc =>
                {
                    sc.AddPlinthHangfire(connStr, r => r.RegisterGlobalHandler<BasicJobExec>(), null, Cron.Minutely(),
                        o => o.SchedulePollingInterval = TimeSpan.FromMilliseconds(250),
                        o =>
                        {
#if PGSQL
                            o.PrepareSchemaIfNecessary = false;
#endif
                        });
                    sc.AddSingleton(IntegrationTestSetup.GetProvider());
                    sc.AddTransient<BasicJobExec>();
                    sc.AddTransient<MultiCount>();

                    sc.AddAuthenticationCore();
                })
                .Configure(app =>
                {
                    app.UseAuthentication();
                    app.UsePlinthHangfire("", dashboardOpts: null);
                })
                .UseContentRoot(RuntimeUtil.FindExecutableHome())
                .CaptureStartupErrors(false)
                .Build();

        await host.StartAsync();
        Thread.Sleep(1000);
        await host.StopAsync();
    }
#endregion
}
