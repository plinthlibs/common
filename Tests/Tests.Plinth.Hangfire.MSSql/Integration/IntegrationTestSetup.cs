#if SQLSERVER
using Plinth.Database.MSSql;
using System.Text.RegularExpressions;
#elif PGSQL
using Plinth.Database.PgSql;
#endif
using System.Reflection;
using Integration.Utilities.Container;
using Integration.Utilities.Database;

namespace Tests.Plinth.Hangfire.Integration;

internal static class IntegrationTestSetup
{
    private static readonly string DbHost = DockerUtil.ContainerHost;

#if SQLSERVER
    private static readonly string DbConnectionTemplate = @"Data Source=tcp:[" + DbHost + @"],1434;Initial Catalog={0};User ID={1};Password={2};Persist Security Info=True;MultipleActiveResultSets=False;TrustServerCertificate=True";

    public const string DatabaseUser = "TestUser";
    public const string DatabasePass = "TestPassword123$";

    private const string DatabaseSaUser = "sa";
    private const string DatabaseSaPass = "sasasa123!";

    private const string MasterDatabase = "master";

#elif PGSQL
    public static string DbConnectionTemplate = @"Host=" + DbHost + @";Port=5433;Database={0};Username={1};Password={2};";

    public const string DatabaseUser = "testuser";
    public const string DatabasePass = "TestPassword123$";

    private const string DatabaseSaUser = "postgres";
    private const string DatabaseSaPass = "postgres123!";

    private const string MasterDatabase = "postgres";
#endif

    public static string DbConnection { get; private set; } = string.Empty;

    private static SqlTransactionFactory? _txnF = null;
    private static string? _dbName = null;

    public static string Dot = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!, "Integration");
    public static string Temp = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!;

    public static void SetUpTest()
    {
        _dbName = "pl" + Guid.NewGuid().ToString("N").ToLowerInvariant();

        DbConnection = string.Format(DbConnectionTemplate, _dbName, DatabaseUser, DatabasePass);

        _txnF = new SqlTransactionFactory(
                _dbName,
                DbConnection,
                30, 3, 200, true);

        var cstr = string.Format(DbConnectionTemplate, MasterDatabase, DatabaseSaUser, DatabaseSaPass);

#if SQLSERVER
        SqlServerDatabase.CreateDB(cstr, _dbName, DatabaseUser, DatabasePass);
#elif PGSQL
        PostgresDatabase.CreateDB(cstr, _dbName, DatabaseUser, DatabasePass);
#endif

        _txnF.GetDefault().ExecuteRawTxn(
            (c) =>
            {
#if SQLSERVER
                c.ExecuteRaw(File.ReadAllText(Path.Combine(Dot, "Schema/Tables.sql")));

                var text = File.ReadAllText(Path.Combine(Dot, "Schema/Procedures.sql"));
                foreach (var s in Regex.Split(text, @"^GO\s*$", RegexOptions.Multiline))
                {
                    if (string.IsNullOrWhiteSpace(s))
                        continue;
                    c.ExecuteRaw(s);
                }
#elif PGSQL
                string sql = File.ReadAllText(Path.Combine(Dot, $"../PgSchema/Hangfire_Tables.sql"));
                c.ExecuteRaw(sql);

                var text = File.ReadAllText(Path.Combine(Dot, $"../PgSchema/Hangfire_Procedures.sql"));
                c.ExecuteRaw(text);
#endif
            });
    }

    public static ISqlTransactionProvider GetProvider()
    {
        return _txnF!.GetDefault();
    }

    public static void Exec(Action<IRawSqlConnection> action)
    {
        _txnF!.GetDefault().ExecuteRawTxn(action);
    }

    public static void CleanUpTest()
    {
        _txnF = null;
        GC.Collect();

        var cstr = string.Format(DbConnectionTemplate, MasterDatabase, DatabaseSaUser, DatabaseSaPass);
#if SQLSERVER
        SqlServerDatabase.DeleteDatabase(cstr, _dbName!);
#elif PGSQL
        PostgresDatabase.DeleteDatabase(cstr, _dbName!);
#endif
    }

    public static async Task StartUpDatabase()
    {
#if SQLSERVER
        await DockerCompose.Up(1434);
#elif PGSQL
        await DockerCompose.Up(5433);
#endif
    }

    public static async Task CleanUpDatabase()
    {
#if SQLSERVER
        await SqlServerDatabase.WaitForDeletes();
#elif PGSQL
        await PostgresDatabase.WaitForDeletes();
#endif

        await DockerCompose.Down();
    }
}
