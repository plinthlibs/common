﻿using Microsoft.Extensions.DependencyInjection;
using Tests.Plinth.Hangfire.Integration;

namespace Tests.Plinth.Hangfire;

static class ContainerBootstrap
{
    public static void Register(IServiceCollection services)
    {
        services.AddSingleton(IntegrationTestSetup.GetProvider());

        services.AddTransient(sp => new Dep("a"));

        /*
        var txnFac = new SqlTransactionFactory(
            config,
            "DB",
            config.GetConnectionString("DB"));

        services.AddSingleton(txnFac);
        services.AddSingleton(txnFac.GetDefault());
        */
    }
}
