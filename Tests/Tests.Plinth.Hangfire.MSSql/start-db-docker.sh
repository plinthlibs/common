#!/bin/bash

# this will ensure the database is fresh every time
docker compose up -d -V --wait

# to stop, run docker-compose down
