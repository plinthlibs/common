using Plinth.Logging.Host;
using NUnit.Framework;

namespace Tests.Plinth.Templating;

[SetUpFixture]
public class NUnitSetup
{
    [OneTimeSetUp]
    public void Setup()
    {
        StaticLogManagerSetup.ConfigureForConsoleLogging();
    }
}
