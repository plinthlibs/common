using NUnit.Framework;
using Plinth.Templating;
using Tests.Plinth.Templating.Templates.Models;
using System.Dynamic;
using System.Reflection;

namespace Tests.Plinth.Templating;

[TestFixture]
class TemplateEngineTests
{
    private readonly TemplateEngine engine;

    public TemplateEngineTests()
    {
        engine = new TemplateEngineBuilder()
            .AddTemplateAssembly(typeof(TestModel1).Assembly, ["Layouts", "Shared/Components", "Views"] )
            .PrecompileInBackground()
            .Build();
    }

    [Test]
    public async Task Test_RenderWithoutModel()
    {
        var result = await engine.RenderAsync("NoModelTest");
        Assert.That(result, Is.EqualTo(@"<!DOCTYPE html><html><head></head><body><span>Hello There</span></body></html>
"));
    }

    [Test]
    public async Task Test_PrecompileAsync()
    {
        var e = new TemplateEngineBuilder()
            .AddTemplateAssembly(typeof(TestModel1).Assembly, ["Layouts", "Shared/Components", "Views"] )
            .Build();
        await e.PrecompileTemplatesAsync();
        var result = await e.RenderAsync("NoModelTest");
        Assert.That(result, Is.EqualTo(@"<!DOCTYPE html><html><head></head><body><span>Hello There</span></body></html>
"));
    }

    [Test]
    public async Task Test_RenderWithModel()
    {
        var model = new TestModel1
        {
            Name = "Bobby",
            Id = 19,
            Aliases = ["Rob", "Bobbert"]
        };
        var result = await engine.RenderAsync("ModelTest", model);
        Assert.That(result, Is.EqualTo(@"<!DOCTYPE html><html><head></head><body><span>Hello There, Bobby [19]</span><span>Rob</span><span>Bobbert</span></body></html>
"));
    }

    [Test]
    public async Task Test_RenderWithModel_AndIncludes()
    {
        var model = new TestModel1
        {
            Name = "Bobby",
            Id = 19,
            Aliases = ["Rob", "Bobbert"]
        };
        var result = await engine.RenderAsync("ModelIncludeTest", model);
        Assert.That(result, Is.EqualTo(@"<!DOCTYPE html><html><head></head><body><span>Hello There, Bobby [19]</span><span>Rob</span><span>Bobbert</span>
<button><span>Perform Button1</span></button>
<button><span>Perform Bobby</span></button>
<button><span>Perform Button3</span></button></body></html>
"));
    }
    
    [Test]
    public async Task Test_RenderWithModel_AndIncludes_AndFunctions()
    {
        var model = new TestModel1
        {
            Name = "Bobby",
            Id = 10,
            Aliases = ["Rob", "Bobbert"]
        };
        var result = await engine.RenderAsync("ModelIncludeFuncTest", model);
        Assert.That(result, Is.EqualTo(@"<!DOCTYPE html><html><head></head><body><span>Hello There, Bobby [10]</span><span>Rob</span><span>Bobbert</span>
<span>2x = 20</span>
<span>3x = 30</span>


Functions can't be included! 4x = 40 5x = 50


</body></html>
"));
    }

    [Test]
    public async Task Test_RenderWithViewBag()
    {
        dynamic vb = new ExpandoObject();
        vb.PersonName = "Tyrone Johnson";
        vb.Age = TimeSpan.Parse("28203.13:42:11.17456");

        var result = await engine.RenderWithViewBagAsync("ViewBagTest", vb);
        Assert.That(result, Is.EqualTo(@"<span>Hello Tyrone Johnson, are you 77 years old?</span>
"));
    }

    [Test]
    public async Task Test_RenderWithViewBagAndModel()
    {
        dynamic vb = new ExpandoObject();
        vb.PersonName = "Tyrone Johnson";
        vb.Age = TimeSpan.Parse("28203.13:42:11.17456");

        var model = new TestModel1
        {
            Name = "Bobby",
            Id = 10,
            Aliases = ["Rob", "Bobbert"]
        };

        var result = await engine.RenderWithViewBagAsync("ViewBagModelTest", model, vb);
        Assert.That(result, Is.EqualTo(@"<span>Hello Bobby (Tyrone Johnson), are you 77 years old?</span>
"));
    }

    [Test]
    public async Task Test_RenderThings()
    {
        var tempEngine = new TemplateEngineBuilder()
            .AddTemplateAssembly(typeof(TestModel1).Assembly)
            .Build();
        var result = await tempEngine.RenderAsync("Views/Things");
        Assert.That(result, Is.EqualTo(@"<div>OtherThing</div><div>RootThing</div>"));
    }

    [Test]
    public void Test_BadBuild()
    {
        Assert.Throws<ArgumentNullException>(() => new TemplateEngineBuilder().AddTemplateAssembly(null!));
        Assert.Throws<ArgumentNullException>(() => new TemplateEngineBuilder().AddTemplateAssembly(null!, ["a", "b"]));
        Assert.Throws<ArgumentNullException>(() => new TemplateEngineBuilder().AddTemplateAssembly(null!, "abc"));
        Assert.Throws<ArgumentNullException>(() => new TemplateEngineBuilder().AddTemplateAssembly(null!, "abc", ["a", "b"]));

        Assert.Throws<ArgumentNullException>(() => new TemplateEngineBuilder().AddTemplateAssembly(typeof(TestModel1).Assembly, (string?)null!));
        Assert.Throws<ArgumentNullException>(() => new TemplateEngineBuilder().AddTemplateAssembly(typeof(TestModel1).Assembly, null!, ["a", "b"]));
#if NET8_0_OR_GREATER
        Assert.Throws<ArgumentException>(() => new TemplateEngineBuilder().AddTemplateAssembly(typeof(TestModel1).Assembly, ""));
        Assert.Throws<ArgumentException>(() => new TemplateEngineBuilder().AddTemplateAssembly(typeof(TestModel1).Assembly, "", ["a", "b"]));
#else
        Assert.Throws<ArgumentNullException>(() => new TemplateEngineBuilder().AddTemplateAssembly(typeof(TestModel1).Assembly, ""));
        Assert.Throws<ArgumentNullException>(() => new TemplateEngineBuilder().AddTemplateAssembly(typeof(TestModel1).Assembly, "", ["a", "b"]));
#endif

        Assert.Throws<NotSupportedException>(() => new TemplateEngineBuilder().AddTemplateAssembly(Assembly.GetExecutingAssembly())
            .AddTemplateAssembly(Assembly.GetExecutingAssembly()));
    }

    [Test]
    public void Test_BadRender()
    {
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderAsync(null!) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderAsync(null!, "a") );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderWithViewBagAsync(null!, new ExpandoObject()) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderWithViewBagAsync(null!, "a", new ExpandoObject()) );

        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawAsync(null!, null!) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawAsync(null!, "content") );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawAsync("id", null!) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawAsync(null!, null!, 5) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawAsync(null!, "content", 5) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawAsync("id", null!, 5) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawWithViewBagAsync(null!, null!, new ExpandoObject()) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawWithViewBagAsync(null!, "content", new ExpandoObject()) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawWithViewBagAsync("id", null!, new ExpandoObject()) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawWithViewBagAsync(null!, null!, 5, new ExpandoObject()) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawWithViewBagAsync(null!, "content", 5, new ExpandoObject()) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawWithViewBagAsync("id", null!, 5, new ExpandoObject()) );

        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawAsync("", null!) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawAsync("", "content") );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawAsync("", null!, 5) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawAsync("", "content", 5) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawWithViewBagAsync("", null!, new ExpandoObject()) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawWithViewBagAsync("", "content", new ExpandoObject()) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawWithViewBagAsync("", null!, 5, new ExpandoObject()) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawWithViewBagAsync("", "content", 5, new ExpandoObject()) );

        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawOnceAsync(null!) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawOnceAsync(null!, 5) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawOnceWithViewBagAsync(null!, new ExpandoObject()) );
        Assert.ThrowsAsync<ArgumentNullException>(async () => await engine.RenderRawOnceWithViewBagAsync(null!, 5, new ExpandoObject()) );
    }
}
