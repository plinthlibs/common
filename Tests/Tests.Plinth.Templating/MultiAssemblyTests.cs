using NUnit.Framework;
using Plinth.Templating;
using Tests.Plinth.Templating.Templates.Models;
using Tests.Plinth.Templating.TemplatesAlt;

namespace Tests.Plinth.Templating;

[TestFixture]
class MultiEngineTests
{
    public MultiEngineTests()
    {
    }

    [Test]
    public async Task Test_RenderFromSingleAssembly()
    {
        var engine = new TemplateEngineBuilder()
            .AddTemplateAssembly(typeof(LoadHelper).Assembly, ["Html"])
            .AddTemplateAssembly(typeof(TestModel1).Assembly, ["Layouts", "Shared/Components", "Views"])
            .PrecompileInBackground()
            .Build();

        var result = await engine.RenderAsync("AltModelTest");
        Assert.That(result, Is.EqualTo(@"<!DOCTYPE html><html><head><title>alt</title></head><body><span>Hello There</span></body></html>
"));

        result = await engine.RenderAsync("NoModelTest");
        Assert.That(result, Is.EqualTo(@"<!DOCTYPE html><html><head></head><body><span>Hello There</span></body></html>
"));
    }

    [Test]
    public async Task Test_RenderFromMultipleAssemblies()
    {
        var engine = new TemplateEngineBuilder()
            .AddTemplateAssembly(typeof(LoadHelper).Assembly, ["Html"])
            .AddTemplateAssembly(typeof(TestModel1).Assembly, ["Layouts", "Shared/Components", "Views"])
            .PrecompileInBackground()
            .Build();

        var result = await engine.RenderAsync("MultiTest");
        Assert.That(result, Is.EqualTo(@"<!DOCTYPE html><html><head></head><body><span>Hello There</span>
<button><span>Perform Test</span></button></body></html>
"));
    }

    [Test]
    public void Test_BadBuildWithDupeNames()
    {
        Assert.Throws<NotSupportedException>(() => new TemplateEngineBuilder()
            .AddTemplateAssembly(typeof(LoadHelper).Assembly, ["Html", "Dupes"])
            .AddTemplateAssembly(typeof(TestModel1).Assembly, ["Layouts", "Shared/Components", "Views"])
            .Build());

        Assert.Throws<NotSupportedException>(() => new TemplateEngineBuilder()
            .AddTemplateAssembly(typeof(LoadHelper).Assembly, ["Html", "Dupes", "Dupes2"])
            .Build());

        Assert.Throws<NotSupportedException>(() => new TemplateEngineBuilder()
            .AddTemplateAssembly(typeof(LoadHelper).Assembly, ["Html", "Dupes", "Dupes2"])
            .AddTemplateAssembly(typeof(TestModel1).Assembly, ["Layouts", "Shared/Components", "Views"])
            .Build());
    }

    [Test]
    public async Task Test_RunWithFQDupeNames()
    {
        var e = new TemplateEngineBuilder()
            .AddTemplateAssembly(typeof(LoadHelper).Assembly)
            .AddTemplateAssembly(typeof(TestModel1).Assembly)
            .Build();

        var result = await e.RenderAsync("Dupes/Button", "B");
        Assert.That(result, Is.EqualTo("<button><span>Perform B</span></button>"));

        result = await e.RenderAsync("Dupes2/Button", "B2");
        Assert.That(result, Is.EqualTo("<button><span>Perform B2</span></button>"));
    }

    [Test]
    public void Test_BadRunDupeNames()
    {
        var e = new TemplateEngineBuilder()
            .AddTemplateAssembly(typeof(LoadHelper).Assembly, ["HtmlDupe"])
            .AddTemplateAssembly(typeof(TestModel1).Assembly)
            .Build();

        Assert.ThrowsAsync<NotSupportedException>(async () => await e.RenderAsync("MultiTestDupe", "B"));
    }
}
