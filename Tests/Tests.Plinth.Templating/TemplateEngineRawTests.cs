﻿using NUnit.Framework;
using Plinth.Templating;

namespace Tests.Plinth.Templating;

[TestFixture]
class TemplateEngineRawTests
{
    [Test]
    public async Task Test_RenderWithoutModel()
    {
        var engine = new TemplateEngineBuilder()
            .AddTemplateAssembly(typeof(TemplateEngineRawTests).Assembly)
            .Build();

        var result = await engine.RenderRawOnceAsync($"Hello There");
        Assert.That(result, Is.EqualTo("Hello There"));
    }

    [Test]
    public async Task Test_RenderWithModel()
    {
        var engine = new TemplateEngineBuilder()
            .AddTemplateAssembly(typeof(TemplateEngineRawTests).Assembly)
            .Build();

        var result = await engine.RenderRawOnceAsync($"@model string {Environment.NewLine}Hello There, '@Model'", "abc123");
        Assert.That(result, Is.EqualTo("Hello There, 'abc123'"));
    }

    [Test]
    public async Task Test_RenderLoop()
    {
        var engine = new TemplateEngineBuilder()
            .AddTemplateAssembly(typeof(TemplateEngineRawTests).Assembly)
            .Build();

        var result = await engine.RenderRawOnceAsync(@"@using System.Collections.Generic;
@model List<int>
Hello There, @foreach (var i in Model) {{<span>@i</span>}}",
            new List<int>() {1, 7, 14});
        Assert.That(result, Is.EqualTo("Hello There, <span>1</span><span>7</span><span>14</span>"));
    }

    [Test]
    public async Task Test_RenderWithoutModel_Cache()
    {
        var engine = new TemplateEngineBuilder()
            .AddTemplateAssembly(typeof(TemplateEngineRawTests).Assembly)
            .Build();

        var result = await engine.RenderRawAsync("Test_RenderWithoutModel_Cache", $"Hello There");
        Assert.That(result, Is.EqualTo("Hello There"));

        result = await engine.RenderRawAsync("Test_RenderWithoutModel_Cache", $"not the same");
        Assert.That(result, Is.EqualTo("Hello There"));
    }

    [Test]
    public async Task Test_RenderWithModel_Cache()
    {
        var engine = new TemplateEngineBuilder()
            .AddTemplateAssembly(typeof(TemplateEngineRawTests).Assembly)
            .Build();

        var result = await engine.RenderRawAsync("Test_RenderWithModel_Cache", $"@model string {Environment.NewLine}Hello There, '@Model'", "abc123");
        Assert.That(result, Is.EqualTo("Hello There, 'abc123'"));

        result = await engine.RenderRawAsync("Test_RenderWithModel_Cache", $"not the same", "def678");
        Assert.That(result, Is.EqualTo("Hello There, 'def678'"));
    }

    [Test]
    public async Task Test_RenderLoop_Cache()
    {
        var engine = new TemplateEngineBuilder()
            .AddTemplateAssembly(typeof(TemplateEngineRawTests).Assembly)
            .Build();

        var result = await engine.RenderRawAsync("Test_RenderLoop_Cache", @"@using System.Collections.Generic;
@model List<int>
Hello There, @foreach (var i in Model) {{<span>@i</span>}}",
            new List<int>() {1, 7, 14});
        Assert.That(result, Is.EqualTo("Hello There, <span>1</span><span>7</span><span>14</span>"));

        result = await engine.RenderRawAsync("Test_RenderLoop_Cache", @"not the same",
            new List<int>() {2, 14, 28});
        Assert.That(result, Is.EqualTo("Hello There, <span>2</span><span>14</span><span>28</span>"));
    }
}
