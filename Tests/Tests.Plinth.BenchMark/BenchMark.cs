using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using Microsoft.Extensions.Logging;
using Plinth.Common.Extensions;
using Plinth.Common.Logging;
using Plinth.Security.Crypto;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;

namespace Tests.Plinth.BenchMark;

[MemoryDiagnoser]
//[ThreadingDiagnoser]
//[SimpleJob(runtimeMoniker: RuntimeMoniker.Net60)]
//[SimpleJob(runtimeMoniker: RuntimeMoniker.Net80)]
[SimpleJob(runtimeMoniker: RuntimeMoniker.Net90)]
#pragma warning disable IDE0059 // Unnecessary assignment of a value
public class BenchMark
{
    //ILogger _logger = null!;

    [GlobalSetup]
    public void GlobalSetup()
    {
        RequestTraceId.Instance.Create();
        RequestTraceChain.Instance.Create();
        RequestTraceChain.Instance.Set(RequestTraceChain.Instance.GenerateNextChain()!);
    }

    /*[Benchmark(Baseline = true)]
    public void Baseline()
    {
        var x = "abcdefghijklmnop"[5..10];
    }*/

    [Benchmark(Baseline = true)]
    public void SB()
    {
        var x = LogTraceInfo(true, true);
    }

    [Benchmark]
    public void StackAlloc()
    {
        var x = LogTraceInfo_Stack(true, true);
    }

    [Benchmark]
    public void StackAllocFunc()
    {
        var x = LogTraceInfo_StackFunc(true, true);
    }

    private static string LogTraceInfo(bool newTraceId, bool newTraceChain)
    {
        string? x = string.Empty;
        var sb = new StringBuilder(128);

        if (newTraceId)
            sb.Append("New trace id: ").Append(RequestTraceId.Instance.Get());

        if (newTraceChain)
        {
            if (newTraceId)
                sb.Append(", ");
            sb.Append("New trace chain: ").Append(RequestTraceChain.Instance.Get());
        }

        if (newTraceId || newTraceChain)
            x = sb.ToString();
        return x;
    }

    private static string LogTraceInfo_StackFunc(bool newTraceId, bool newTraceChain)
    {
        const string newTraceIdPreamble = "New trace id: ";
        const string newTraceChainPreamble = "New trace chain: ";

        if (!newTraceId && !newTraceChain)
            return string.Empty;

        Span<char> sb = stackalloc char[128];

        int pos = 0;
        if (newTraceId)
        {
            appendStack(ref sb, ref pos, newTraceIdPreamble, RequestTraceId.Instance.Get());
        }

        if (newTraceChain)
        {
            if (newTraceId)
            {
                sb[pos] = ',';
                sb[pos + 1] = ' ';
                pos += 2;
            }

            appendStack(ref sb, ref pos, newTraceChainPreamble, RequestTraceChain.Instance.Get());
        }

        return new string(sb[..pos]);

        //[MethodImpl(MethodImplOptions.AggressiveInlining)]
        static void appendStack(ref Span<char> sb, ref int pos, string s1, string? s2)
        {
            s1.TryCopyTo(sb[pos..]);
            pos += s1.Length;
            if (s2 != null)
            {
                s2.TryCopyTo(sb[pos..]);
                pos += s2.Length;
            }
        }
    }

    private static string LogTraceInfo_Stack(bool newTraceId, bool newTraceChain)
    {
        const string newTraceIdPreamble = "New trace id: ";
        const string newTraceChainPreamble = "New trace chain: ";

        Span<char> sb = stackalloc char[128];

        int pos = 0;
        if (newTraceId)
        {
            newTraceIdPreamble.CopyTo(sb);
            pos += newTraceIdPreamble.Length;
            var s = RequestTraceId.Instance.Get();
            if (s != null)
            {
                s.TryCopyTo(sb[pos..]);
                pos += s.Length;
            }
        }

        if (newTraceChain)
        {
            if (newTraceId)
            {
                sb[pos] = ',';
                sb[pos + 1] = ' ';
                pos += 2;
            }

            newTraceChainPreamble.TryCopyTo(sb[pos..]);
            pos += newTraceChainPreamble.Length;
            var s = RequestTraceChain.Instance.Get();
            if (s != null)
            {
                s.TryCopyTo(sb[pos..]);
                pos += s.Length;
            }
        }

        if (newTraceId || newTraceChain)
            return new string(sb[..pos]);
        return string.Empty;
    }
}
#pragma warning restore IDE0059 // Unnecessary assignment of a value
