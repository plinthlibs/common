using BenchmarkDotNet.Running;
using Plinth.Logging.Host;

namespace Tests.Plinth.BenchMark;

class Program
{
    static void Main(string[] _)
    {
        StaticLogManagerSetup.ConfigureForDebugLogging();

#if DEBUG
        var b = new BenchMark();
        b.GlobalSetup();
        b.StackAllocFunc();
#else
        BenchmarkRunner.Run<BenchMark>();
#endif

        Console.WriteLine("Done.");
        Console.ReadKey();
    }
}
