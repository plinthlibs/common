using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Plinth.Logging.NLog;
using Plinth.WindowsService;

[assembly: NUnit.Framework.NonTestAssembly]

namespace Tests.Plinth.WindowsService.Core;

static class Program
{
    private static readonly ILogger log = StaticLogManagerSetup.BasicNLogSetup();

    static void Main(string[] args)
    {
        log.Debug("startup! {0}", Environment.Version);

        var mycallbacks = new Dictionary<ServiceState, Action>()
        {
            [ServiceState.Starting] = () => log.Debug("Main: Starting..."),
            [ServiceState.Started] = () => log.Debug("Main: Started"),
            [ServiceState.Stopping] = () => log.Debug("Main: Stopping..."),
            [ServiceState.Stopped] = () => log.Debug("Main: Stopped"),
        };

        try
        {
            // option 1, make your own host and hosted service
            WindowsServiceRunner.RunHost(BuildHost, args, callbacks: mycallbacks);

            // option 2, make your own hosted service
            //WindowsServiceRunner.RunHostedService(new LoggerService(), args, callbacks: mycallbacks);

            // option 3, a cancellable user action
            //WindowsServiceRunner.RunAction(MyAction, args, callbacks: mycallbacks);

            // option 4, a cancellable user action (async)
            //WindowsServiceRunner.RunAsyncAction(MyAsyncAction, args, callbacks: mycallbacks);
        }
        catch (Exception e)
        {
            log.Fatal(e, "failed during startup");
            throw;
        }
    }

    public static IHost BuildHost(string[] _) =>
        new HostBuilder()
            .UseContentRoot(WindowsServiceRunner.FindContentRoot())
            .ConfigureServices((hostContext, services) =>
            {
                services.AddHostedService<LoggerService>();
                services.AddNLog();
            })
            .Build();

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>")]
    private static void MyAction(CancellationToken token)
    {
        while (!token.IsCancellationRequested)
        {
            log.Debug("timer");
            Task.Delay(TimeSpan.FromSeconds(2), token).Wait(token);
        }
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>")]
    private static async Task MyAsyncAction(CancellationToken token)
    {
        while (!token.IsCancellationRequested)
        {
            log.Debug("timer");
            await Task.Delay(TimeSpan.FromSeconds(2), token);
        }
    }
}

public class LoggerService : IHostedService, IDisposable
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private Timer? _timer;

    public Task StartAsync(CancellationToken cancellationToken)
    {
        log.Debug("service start()");
        _timer = new Timer(
            (e) => WriteTimeToFile(),
            null,
            TimeSpan.Zero,
            TimeSpan.FromSeconds(2));

        log.Debug("service start done()");
        return Task.CompletedTask;
    }

    public static void WriteTimeToFile()
    {
        log.Debug("timer");
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        log.Debug("service stop()");
        _timer?.Change(Timeout.Infinite, 0);

        log.Debug("service stop done()");
        return Task.CompletedTask;
    }

    public void Dispose()
    {
        _timer?.Dispose();
        GC.SuppressFinalize(this);
    }
}
