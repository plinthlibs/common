// uncomment to run
//#define RUN_DB_PGSQL_INT_TESTS
#if RUNNING_IN_CI
#define RUN_DB_PGSQL_INT_TESTS
#endif
using System.Data;
using Npgsql;
using Plinth.Database.PgSql;
using NUnit.Framework;
using NpgsqlTypes;

#if !NET9_0_OR_GREATER
using Convert = Plinth.Common.Utils.BackPort.PlinthConvert;
#endif

namespace Tests.Plinth.Database.PgSql.Integration;

#if !RUN_DB_PGSQL_INT_TESTS
[AttributeUsage(AttributeTargets.Method)]
sealed class TestAttribute : Attribute { }
[AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
public sealed class TestCaseAttribute : Attribute { public TestCaseAttribute(params object[] _) { } }
#endif

[TestFixture]
#if !RUN_DB_PGSQL_INT_TESTS
[System.Diagnostics.CodeAnalysis.SuppressMessage("Structure", "NUnit1028:The non-test method is public", Justification = "<Pending>")]
#endif
partial class DatabaseIntTest
{
    [OneTimeSetUp]
    public async Task SetUp()
    {
        await IntegrationTestSetup.StartUpDatabase();
    }

    [OneTimeTearDown]
    public async Task TearDown()
    {
        await IntegrationTestSetup.CleanUpDatabase();
    }

    [SetUp]
    public void TestSetUp()
    {
        IntegrationTestSetup.SetUpTest();
    }

    [TearDown]
    public void TestTearDown()
    {
        IntegrationTestSetup.CleanUpTest();
    }

    class TestRecord
    {
        public long Id { get; set; }
        public DateTime? Date { get; set; }
        public string? String { get; set; }
        public int? IntField { get; set; }
        public decimal? Money { get; set; }
        public byte[]? Blob { get; set; }
        public int? IntField2 { get; set; }

        public Dictionary<string, object>? Others { get; set; }
    }

#region DateTime2

    [Test]
    public void TestDateTime2()
    {
        static Tuple<DateTime, DateTime> GetDateTime(IRawSqlConnection c, int id) =>
             c.ExecuteQueryProcOne("fn_get_datetime_by_id", r => Tuple.Create(r.GetDateTime("dt1"), r.GetDateTime("dt2")), new NpgsqlParameter("@i_id", id)).Value!;

        static Tuple<DateTime, DateTime> GetDateTimeRaw(IRawSqlConnection c, int id) =>
             c.ExecuteRawQueryOne("SELECT dt1, dt2 FROM datetime2test WHERE id = :i_id", r => Tuple.Create(r.GetDateTime("dt1"), r.GetDateTime("dt2")), new NpgsqlParameter("@i_id", id)).Value!;

        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                var dt = new DateTime(2017, 5, 4, 8, 57, 23, 112, DateTimeKind.Utc);
                var dt2 = new DateTime(2017, 2, 7, 16, 23, 36, 812, DateTimeKind.Utc);
                Tuple<DateTime, DateTime> r;

                // insert without type spec
                c.ExecuteProc(
                    "fn_insert_datetime",
                    new NpgsqlParameter<long>("@i_id", 1),
                    new NpgsqlParameter<DateTime>("@i_dt1", dt),
                    new NpgsqlParameter<DateTime>("@i_dt2", dt));
                r = GetDateTime(c, 1);
                Assert.That(r.Item1.Millisecond, Is.EqualTo(112));
                //Assert.AreEqual(113, r.Item2.Millisecond); // fixed by auto conversion to datetime2
                Assert.That(r.Item2.Millisecond, Is.EqualTo(112));
                Assert.That(r.Item1, Is.EqualTo(dt));
                Assert.That(r.Item2, Is.EqualTo(dt));

                // insert with type spec
                c.ExecuteProc(
                    "fn_insert_datetime",
                    new NpgsqlParameter("@i_id", 2),
                    new NpgsqlParameter("@i_dt1", dt),
                    new NpgsqlParameter("@i_dt2", SqlDbType.DateTime2) { Value = dt });
                r = GetDateTime(c, 2);
                Assert.That(r.Item1.Millisecond, Is.EqualTo(112));
                Assert.That(r.Item2.Millisecond, Is.EqualTo(112));
                Assert.That(r.Item1, Is.EqualTo(dt));
                Assert.That(r.Item2, Is.EqualTo(dt));

                // insert both with type spec
                c.ExecuteProc(
                    "fn_insert_datetime",
                    new NpgsqlParameter("@i_id", 3),
                    new NpgsqlParameter("@i_dt1", SqlDbType.DateTime2) { Value = dt },
                    new NpgsqlParameter("@i_dt2", SqlDbType.DateTime2) { Value = dt });
                r = GetDateTime(c, 3);
                Assert.That(r.Item1.Millisecond, Is.EqualTo(112));
                Assert.That(r.Item2.Millisecond, Is.EqualTo(112));
                Assert.That(r.Item1, Is.EqualTo(dt));
                Assert.That(r.Item2, Is.EqualTo(dt));

                for (int i = 4; i <= 6; i++)
                {
                    // insert without type spec
                    c.ExecuteProc(
                        "fn_insert_datetime",
                        new NpgsqlParameter("@i_id", i),
                        new NpgsqlParameter("@i_dt1", dt),
                        new NpgsqlParameter("@i_dt2", dt));
                }

                // update without type spec
                c.ExecuteProc(
                    "fn_update_datetime",
                        new NpgsqlParameter("@i_id", 4),
                        new NpgsqlParameter("@i_dt1", dt2),
                        new NpgsqlParameter("@i_dt2", dt2));
                r = GetDateTime(c, 4);
                Assert.That(r.Item1.Millisecond, Is.EqualTo(812));
                //Assert.AreEqual(113, r.Item2.Millisecond); // fixed by auto conversion to datetime2
                Assert.That(r.Item2.Millisecond, Is.EqualTo(812));
                Assert.That(r.Item1, Is.EqualTo(dt2));
                Assert.That(r.Item2, Is.EqualTo(dt2));

                // update with type spec
                c.ExecuteProc(
                    "fn_update_datetime",
                    new NpgsqlParameter("@i_id", 5),
                    new NpgsqlParameter("@i_dt1", dt2),
                    new NpgsqlParameter("@i_dt2", SqlDbType.DateTime2) { Value = dt2 });
                r = GetDateTime(c, 5);
                Assert.That(r.Item1.Millisecond, Is.EqualTo(812));
                Assert.That(r.Item2.Millisecond, Is.EqualTo(812));
                Assert.That(r.Item1, Is.EqualTo(dt2));
                Assert.That(r.Item2, Is.EqualTo(dt2));

                // update both with type spec
                c.ExecuteProc(
                    "fn_update_datetime",
                    new NpgsqlParameter("@i_id", 6),
                    new NpgsqlParameter("@i_dt1", SqlDbType.DateTime2) { Value = dt2 },
                    new NpgsqlParameter("@i_dt2", SqlDbType.DateTime2) { Value = dt2 });
                r = GetDateTime(c, 6);
                Assert.That(r.Item1.Millisecond, Is.EqualTo(812));
                Assert.That(r.Item2.Millisecond, Is.EqualTo(812));
                Assert.That(r.Item1, Is.EqualTo(dt2));
                Assert.That(r.Item2, Is.EqualTo(dt2));

                // RAW
                c.ExecuteRaw("INSERT INTO datetime2test (id, dt1, dt2) VALUES (:i_id, :i_dt1, :i_dt2)",
                    new NpgsqlParameter("@i_id", 7),
                    new NpgsqlParameter("@i_dt1", dt), // SqlDbType.DateTime2) { Value = dt2 },
                    new NpgsqlParameter("@i_dt2", dt)); // SqlDbType.DateTime2) { Value = dt2 });
                r = GetDateTime(c, 7);
                Assert.That(r.Item1.Millisecond, Is.EqualTo(112));
                Assert.That(r.Item2.Millisecond, Is.EqualTo(112));
                Assert.That(r.Item1, Is.EqualTo(dt));
                Assert.That(r.Item2, Is.EqualTo(dt));

                c.ExecuteRaw("UPDATE datetime2test SET dt1=:i_dt1, dt2=:i_dt2 WHERE id = :i_id",
                    new NpgsqlParameter("@i_id", 7),
                    new NpgsqlParameter("@i_dt1", dt2), // SqlDbType.DateTime2) { Value = dt2 },
                    new NpgsqlParameter("@i_dt2", dt2)); // SqlDbType.DateTime2) { Value = dt2 });
                r = GetDateTime(c, 7);
                Assert.That(r.Item1.Millisecond, Is.EqualTo(812));
                Assert.That(r.Item2.Millisecond, Is.EqualTo(812));
                Assert.That(r.Item1, Is.EqualTo(dt2));
                Assert.That(r.Item2, Is.EqualTo(dt2));

                r = GetDateTimeRaw(c, 7);
                Assert.That(r.Item1.Millisecond, Is.EqualTo(812));
                Assert.That(r.Item2.Millisecond, Is.EqualTo(812));
                Assert.That(r.Item1, Is.EqualTo(dt2));
                Assert.That(r.Item2, Is.EqualTo(dt2));

                var sv = c.ExecuteRawQueryOne("SELECT dt1 FROM datetime2test WHERE dt1 < :i_dt",
                    rs => rs.GetDateTime("dt1"),
                    new NpgsqlParameter("@i_dt", dt2)); // SqlDbType.DateTime2) { Value = dt2 });
                Assert.That(sv.RowReturned, Is.False);

                sv = c.ExecuteRawQueryOne("SELECT dt2 FROM datetime2test WHERE dt2 <= :i_dt",
                    rs => rs.GetDateTime("dt2"),
                    new NpgsqlParameter("@i_dt", dt2)); // SqlDbType.DateTime2) { Value = dt2 });
                Assert.That(sv.RowReturned);
                Assert.That(sv.Value.Millisecond, Is.EqualTo(812));
                Assert.That(r.Item2, Is.EqualTo(dt2));
            });
    }

#endregion DateTime2

#region Rollback Action

    [Test]
    public void TestRollbackAction_Sync_1call()
    {
        bool called = false;
        Assert.Throws<Exception>(() => IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddRollbackAction("f", () => { called = true; File.Delete(fn); });

                Assert.That(File.Exists(fn));

                if (File.Exists(fn))
                    throw new Exception("failure!");
                return true;
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public void TestRollbackAction_Sync_2calls()
    {
        bool first_called = false;
        bool first_called_second = false;
        bool second_called = false;
        bool second_called_first = false;
        Assert.Throws<Exception>(() => IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddRollbackAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2.tmp")), Is.False);
                    first_called = true;
                    if (second_called)
                        first_called_second = true;
                    File.Delete(fn);
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddRollbackAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2.tmp")));
                    second_called = true;
                    if (!first_called)
                        second_called_first = true;
                    File.Delete(fn2);
                });

                if (File.Exists(fn))
                    throw new Exception("failure!");
                return true;
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_second);
        Assert.That(second_called_first);
    }

    [Test]
    public void TestRollbackAction_Sync_2calls_throws()
    {
        bool first_called = false;
        bool first_called_second = false;
        bool second_called = false;
        bool second_called_first = false;
        Assert.Throws<Exception>(() => IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddRollbackAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws.tmp")), Is.False);
                    first_called = true;
                    if (second_called)
                        first_called_second = true;
                    File.Delete(fn);

                    throw new IOException("failed!");
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddRollbackAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws.tmp")));
                    second_called = true;
                    if (!first_called)
                        second_called_first = true;
                    File.Delete(fn2);

                    throw new IOException("failed!");
                });

                if (File.Exists(fn))
                    throw new Exception("failure!");
                return true;
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_second);
        Assert.That(second_called_first);
    }

    [Test]
    public void TestRollbackAction_Sync_2calls_1stThrow()
    {
        bool first_called = false;
        bool first_called_second = false;
        bool second_called = false;
        bool second_called_first = false;
        Assert.Throws<Exception>(() => IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws-1stthrow.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddRollbackAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws-1stthrow.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws-1stthrow.tmp")));
                    first_called = true;
                    if (second_called)
                        first_called_second = true;
                    File.Delete(fn);

                    throw new IOException("failed!");
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws-1stthrow.tmp");

                File.WriteAllText(fn2, "this is a file2");
                if (fn2 != null)
                    throw new Exception("failure!");
                c.AddRollbackAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws-1stthrow.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws-1stthrow.tmp")));
                    second_called = true;
                    if (!first_called)
                        second_called_first = true;
                    File.Delete(fn2!);

                    throw new IOException("failed!");
                });
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws-1stthrow.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws-1stthrow.tmp")));
        Assert.That(first_called);
        Assert.That(second_called, Is.False);
        Assert.That(first_called_second, Is.False);
        Assert.That(second_called_first, Is.False);
    }

    [Test]
    public void TestRollbackAction_Async_1call()
    {
        bool called = false;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncRollbackAction("f", async () => { called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));

                await Task.Delay(10);
                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public void TestRollbackAction_Sync_AsyncAction()
    {
        bool called = false;
        Assert.Throws<Exception>(() => IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncRollbackAction("f", async () => { called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));

                if (File.Exists(fn))
                    throw new Exception("failure!");
                return true;
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public void TestRollbackAction_Async_AsyncActionSyncCall()
    {
        bool called = false;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddRollbackAction("f", () => { called = true; File.Delete(fn); });

                Assert.That(File.Exists(fn));

                await Task.Delay(10);
                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public void TestRollbackAction_Async_AsyncActionAsyncCall_WrongMethod()
    {
        bool called = false;
        Assert.Throws<NotSupportedException>(() => IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddRollbackAction("f", async () => { called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));

                if (File.Exists(fn))
                    throw new Exception("failure!");
                return true;
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")));
        Assert.That(called, Is.False);
    }

    [Test]
    public void TestRollbackAction_Async_2calls()
    {
        bool first_called = false;
        bool first_called_second = false;
        bool second_called = false;
        bool second_called_first = false;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncRollbackAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")), Is.False);
                    first_called = true;
                    if (second_called)
                        first_called_second = true;
                    File.Delete(fn);
                    await Task.Delay(10);
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddAsyncRollbackAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")));
                    second_called = true;
                    if (!first_called)
                        second_called_first = true;
                    File.Delete(fn2);
                    await Task.Delay(10);
                });

                await Task.Delay(10);
                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_second);
        Assert.That(second_called_first);
    }

    [Test]
    public void TestRollbackAction_Async_2calls_throws()
    {
        bool first_called = false;
        bool first_called_second = false;
        bool second_called = false;
        bool second_called_first = false;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncRollbackAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")), Is.False);
                    first_called = true;
                    if (second_called)
                        first_called_second = true;
                    File.Delete(fn);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddAsyncRollbackAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")));
                    second_called = true;
                    if (!first_called)
                        second_called_first = true;
                    File.Delete(fn2);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                await Task.Delay(10);
                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_second);
        Assert.That(second_called_first);
    }

    [Test]
    public void TestRollbackAction_Async_2calls_throws_1stThrows()
    {
        bool first_called = false;
        bool first_called_second = false;
        bool second_called = false;
        bool second_called_first = false;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws-1stThrows.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncRollbackAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws-1stThrows.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws-1stThrows.tmp")));
                    first_called = true;
                    if (second_called)
                        first_called_second = true;
                    File.Delete(fn);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws-1stThrows.tmp");
                File.WriteAllText(fn2, "this is a file2");

                if (fn2 != null)
                    throw new Exception("failure!");
                c.AddAsyncRollbackAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws-1stThrows.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws-1stThrows.tmp")));
                    second_called = true;
                    if (!first_called)
                        second_called_first = true;
                    File.Delete(fn2!);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                await Task.Delay(10);
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws-1stThrows.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws-1stThrows.tmp")));
        Assert.That(first_called);
        Assert.That(second_called, Is.False);
        Assert.That(first_called_second, Is.False);
        Assert.That(second_called_first, Is.False);
    }

#endregion Rollback Action

#region Post Commit Action

    [Test]
    public void TestPostCommitAction_Sync_1call()
    {
        bool called = false;
        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddPostCommitAction("f", () => { called = true; File.Delete(fn); });

                Assert.That(File.Exists(fn));
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public void TestPostCommitAction_Sync_2calls()
    {
        bool first_called = false;
        bool first_called_first = false;
        bool second_called = false;
        bool second_called_second = false;
        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddPostCommitAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2.tmp")));
                    first_called = true;
                    if (!second_called)
                        first_called_first = true;
                    File.Delete(fn);
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddPostCommitAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls.tmp")), Is.False);
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2.tmp")));
                    second_called = true;
                    if (first_called)
                        second_called_second = true;
                    File.Delete(fn2);
                });
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_first);
        Assert.That(second_called_second);
    }

    [Test]
    public void TestPostCommitAction_Sync_2calls_throws()
    {
        bool first_called = false;
        bool first_called_first = false;
        bool second_called = false;
        bool second_called_second = false;
        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddPostCommitAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws.tmp")));
                    first_called = true;
                    if (!second_called)
                        first_called_first = true;
                    File.Delete(fn);

                    throw new IOException("failed!");
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddPostCommitAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws.tmp")), Is.False);
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws.tmp")));
                    second_called = true;
                    if (first_called)
                        second_called_second = true;
                    File.Delete(fn2);

                    throw new IOException("failed!");
                });
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_first);
        Assert.That(second_called_second);
    }

    [Test]
    public async Task TestPostCommitAction_Async_1call()
    {
        bool called = false;
        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncPostCommitAction("f", async () => { called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));

                await Task.Delay(10);
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public void TestPostCommitAction_Sync_AsyncAction()
    {
        bool called = false;
        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncPostCommitAction("f", async () => { called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public async Task TestPostCommitAction_Async_AsyncActionSyncCall()
    {
        bool called = false;
        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddPostCommitAction("f", () => { called = true; File.Delete(fn); });

                Assert.That(File.Exists(fn));

                await Task.Delay(10);
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public void TestPostCommitAction_Async_AsyncActionAsyncCall_WrongMethod()
    {
        bool called = false;
        Assert.Throws<NotSupportedException>(() => IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddPostCommitAction("f", async () => { called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")));
        Assert.That(called, Is.False);
    }

    [Test]
    public async Task TestPostCommitAction_Async_2calls()
    {
        bool first_called = false;
        bool first_called_first = false;
        bool second_called = false;
        bool second_called_second = false;
        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncPostCommitAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")));
                    first_called = true;
                    if (!second_called)
                        first_called_first = true;
                    File.Delete(fn);
                    await Task.Delay(10);
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddAsyncPostCommitAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")), Is.False);
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")));
                    second_called = true;
                    if (first_called)
                        second_called_second = true;
                    File.Delete(fn2);
                    await Task.Delay(10);
                });

                await Task.Delay(10);
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_first);
        Assert.That(second_called_second);
    }

    [Test]
    public async Task TestPostCommitAction_Async_2calls_throws()
    {
        bool first_called = false;
        bool first_called_first = false;
        bool second_called = false;
        bool second_called_second = false;
        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncPostCommitAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")));
                    first_called = true;
                    if (!second_called)
                        first_called_first = true;
                    File.Delete(fn);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddAsyncPostCommitAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")), Is.False);
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")));
                    second_called = true;
                    if (first_called)
                        second_called_second = true;
                    File.Delete(fn2);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                await Task.Delay(10);
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_first);
        Assert.That(second_called_second);
    }

    [Test]
    public void TestPostCommitAction_Sync_Rollback()
    {
        bool called = false;
        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddPostCommitAction("f", () => { called = true; File.Delete(fn); });

                Assert.That(File.Exists(fn));

                c.SetRollback();
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback.tmp")));
        Assert.That(called, Is.False);
    }

    [Test]
    public void TestPostCommitAction_Sync_Throw()
    {
        bool called = false;
        bool caught = false;
        try
        {
            IntegrationTestSetup.Get().ExecuteRawTxn(
                (c) =>
                {
                    string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback-throw.tmp");
                    File.WriteAllText(fn, "this is a file");
                    c.AddPostCommitAction("f", () => { called = true; File.Delete(fn); });

                    Assert.That(File.Exists(fn));

                    if (File.Exists(fn))
                        throw new Exception("failure");
                    return true;
                });
        }
        catch (Exception e)
        {
            caught = true;
            Assert.That(e.Message, Is.EqualTo("failure"));
        }

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback-throw.tmp")));
        Assert.That(called, Is.False);
        Assert.That(caught);
    }

    [Test]
    public async Task TestPostCommitAction_Async_Rollback()
    {
        bool called = false;
        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                await Task.Delay(10);
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncPostCommitAction("f", async () => { await Task.Delay(10); called = true; File.Delete(fn); });

                Assert.That(File.Exists(fn));

                c.SetRollback();
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback.tmp")));
        Assert.That(called, Is.False);
    }

    [Test]
    public async Task TestPostCommitAction_Async_Throw()
    {
        bool called = false;
        bool caught = false;
        try
        {
            await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
                async (c) =>
                {
                    await Task.Delay(10);
                    string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback-throw.tmp");
                    File.WriteAllText(fn, "this is a file");
                    c.AddAsyncPostCommitAction("f", async () => { await Task.Delay(10); called = true; File.Delete(fn); });

                    Assert.That(File.Exists(fn));

                    throw new Exception("failure");
                });
        }
        catch (Exception e)
        {
            caught = true;
            Assert.That(e.Message, Is.EqualTo("failure"));
        }

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback-throw.tmp")));
        Assert.That(called, Is.False);
        Assert.That(caught);
    }
#endregion Post Commit Action

#region ExecuteQueryProc

    [Test]
    public void TestExecuteQueryProc()
    {
        static TestRecord load(IResult r)
        {
            return new TestRecord() { Id = r.GetLong("id"), Date = r.GetDateTimeNull("DT"), IntField = r.GetIntNull("intfield"), Money = r.GetDecimalNull("moneyfield"), String = r.GetString("string") };
        };

        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                Assert.That(c.IsAsync(), Is.False);

                c.ExecuteProc(
                    "fn_insert_test_record",
                    new NpgsqlParameter("@i_id", 1L),
                    new NpgsqlParameter("@i_date", null),
                    new NpgsqlParameter("@i_string", "s1"),
                    new NpgsqlParameter("@i_int", 10),
                    new NpgsqlParameter("@i_money", null),
                    new NpgsqlParameter("@i_blob", null),
                    new NpgsqlParameter("@i_int_field_2", null));

                int rows = c.ExecuteProcUnchecked(
                    "fn_insert_test_record",
                    new NpgsqlParameter("@i_id", 2L),
                    new NpgsqlParameter("@i_date", null),
                    new NpgsqlParameter("@i_string", "s2"),
                    new NpgsqlParameter("@i_int", 10),
                    new NpgsqlParameter("@i_money", null),
                    new NpgsqlParameter("@i_blob", null),
                    new NpgsqlParameter("@i_int_field_2", null));
                Assert.That(rows, Is.EqualTo(1));

                Assert.That(c.ExecuteQueryProcOne(
                    "fn_get_test_record_by_id",
                    (r) => Assert.Fail("no record"),
                    new NpgsqlParameter("@i_id", -86L)), Is.False);

                Assert.That(c.ExecuteQueryProcOne(
                    "fn_get_test_record_by_id",
                    (r) => Assert.That(r.GetString("string"), Is.EqualTo("s1")),
                    new NpgsqlParameter("@i_id", 1L)));

                var sr = c.ExecuteQueryProcOne(
                    "fn_get_test_record_by_id",
                    load,
                    new NpgsqlParameter("@i_id", -86L));
                Assert.That(sr.RowReturned, Is.False);
                Assert.That(sr.Value, Is.Null);

                sr = c.ExecuteQueryProcOne(
                    "fn_get_test_record_by_id",
                    load,
                    new NpgsqlParameter("@i_id", 1L));
                Assert.That(sr.RowReturned);
                Assert.That(sr.Value, Is.Not.Null);
                Assert.That(sr.Value?.Id, Is.EqualTo(1));
                Assert.That(sr.Value?.String, Is.EqualTo("s1"));

                Assert.That(c.ExecuteQueryProcOne(
                    "fn_echo_bit",
                    r => r.GetBool("bitval"),
                    new NpgsqlParameter("@i_bit", true)).Value);

                Assert.That(c.ExecuteQueryProcOne(
                    "fn_echo_bit",
                    r => r.GetBool("bitval"),
                    new NpgsqlParameter("@i_bit", false)).Value, Is.False);

                var list = c.ExecuteQueryProcList(
                    "fn_get_test_record_by_int",
                    load,
                    new NpgsqlParameter("@i_int", 10));
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                const string GetSql = "SELECT * FROM Test WHERE id = :i_id";
                Assert.That(c.ExecuteRawQueryOne(
                    GetSql,
                    (r) => Assert.Fail("no record"),
                    new NpgsqlParameter("@i_id", -86L)), Is.False);

                Assert.That(c.ExecuteRawQueryOne(
                    GetSql,
                    (r) => Assert.That(r.GetString("string"), Is.EqualTo("s1")),
                    new NpgsqlParameter("@i_id", 1L)));

                sr = c.ExecuteRawQueryOne(
                    GetSql,
                    load,
                    new NpgsqlParameter("@i_id", -86L));
                Assert.That(sr.RowReturned, Is.False);
                Assert.That(sr.Value, Is.Null);

                sr = c.ExecuteRawQueryOne(
                    GetSql,
                    load,
                    new NpgsqlParameter("@i_id", 1L));
                Assert.That(sr.RowReturned);
                Assert.That(sr.Value, Is.Not.Null);
                Assert.That(sr.Value?.Id, Is.EqualTo(1));
                Assert.That(sr.Value?.String, Is.EqualTo("s1"));

                const string GetSqlByid = "SELECT * FROM Test WHERE intfield = :i_int"; 
                list = c.ExecuteRawQueryList(
                    GetSqlByid,
                    load,
                    new NpgsqlParameter("@i_int", 10));
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                Assert.That(c.ExecuteRaw("UPDATE Test SET intfield = 10"), Is.EqualTo(2));
            });
    }

    [Test]
    public void TestExecuteQueryProc_Default()
    {
        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                Assert.That(c.IsAsync(), Is.False);

                c.ExecuteProc(
                    "fn_insert_test_record",
                    new NpgsqlParameter("@i_id", 1),
                    new NpgsqlParameter("@i_string", "s1"),
                    new NpgsqlParameter("@i_int", 10));

                c.ExecuteProc(
                    "fn_insert_test_record",
                    new NpgsqlParameter("@i_id", 2),
                    new NpgsqlParameter("@i_string", "s2"),
                    new NpgsqlParameter("@i_int", 10));

                Assert.That(c.ExecuteQueryProcOne(
                    "fn_get_test_record_by_id",
                    (r) => Assert.Fail("no record"),
                    new NpgsqlParameter("@i_id", -86)), Is.False);

                Assert.That(c.ExecuteQueryProcOne(
                    "fn_get_test_record_by_id",
                    (r) => Assert.That(r.GetString("string"), Is.EqualTo("s1")),
                    new NpgsqlParameter("@i_id", 1)));

                c.ExecuteProc(
                    "fn_insert_test_record",
                    new NpgsqlParameter("@i_id", 4),
                    new NpgsqlParameter("@i_string", "s1"));

                Assert.That(c.ExecuteQueryProcOne(
                    "fn_get_test_record_by_id",
                    (r) => Assert.That(r.GetIntNull("intfield"), Is.EqualTo(8866)),
                    new NpgsqlParameter("@i_id", 4)));

                c.ExecuteProc(
                    "fn_insert_test_record",
                    new NpgsqlParameter("@i_id", 5),
                    new NpgsqlParameter("@i_money", 4.2m));

                Assert.That(c.ExecuteQueryProcOne(
                    "fn_get_test_record_by_id",
                    (r) => {
                        Assert.That(r.GetIntNull("intfield"), Is.EqualTo(8866));
                        Assert.That(r.GetDecimalNull("moneyfield"), Is.EqualTo(4.2m));
                    },
                    new NpgsqlParameter("@i_id", 5)));
            });
    }

    [Test]
    public async Task TestExecuteQueryProcAsync()
    {
        static Task<TestRecord> load(IResult r)
        {
            return Task.FromResult(new TestRecord() { Id = r.GetLong("id"), Date = r.GetDateTimeNull("DT"), IntField = r.GetIntNull("intfield"), Money = r.GetDecimalNull("moneyfield"), String = r.GetString("string") });
        };

        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                Assert.That(c.IsAsync());

                c.ExecuteProc(
                    "fn_insert_test_record",
                    new NpgsqlParameter("@i_id", 1L),
                    new NpgsqlParameter("@i_string", "s1"),
                    new NpgsqlParameter("@i_int", 10));

                int rows = c.ExecuteProcUnchecked(
                    "fn_insert_test_record",
                    new NpgsqlParameter("@i_id", 2L),
                    new NpgsqlParameter("@i_string", "s2"),
                    new NpgsqlParameter("@i_int", 10));
                Assert.That(rows, Is.EqualTo(1));

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", -86)), Is.False);

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.That(r.GetString("string"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 1)));

                var sr = await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    load,
                    new NpgsqlParameter("@i_id", -86));
                Assert.That(sr.RowReturned, Is.False);
                Assert.That(sr.Value, Is.Null);

                sr = await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    load,
                    new NpgsqlParameter("@i_id", 1));
                Assert.That(sr.RowReturned);
                Assert.That(sr.Value, Is.Not.Null);
                Assert.That(sr.Value?.Id, Is.EqualTo(1));
                Assert.That(sr.Value?.String, Is.EqualTo("s1"));

                var list = await c.ExecuteQueryProcListAsync(
                    "fn_get_test_record_by_int",
                    load,
                    new NpgsqlParameter("@i_int", 10));
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                const string GetSql = "SELECT * FROM Test WHERE id = :i_id";
                Assert.That(await c.ExecuteRawQueryOneAsync(
                    GetSql,
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", -86)), Is.False);

                Assert.That(await c.ExecuteRawQueryOneAsync(
                    GetSql,
                    (r) => { Assert.That(r.GetString("string"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 1)));

                sr = await c.ExecuteRawQueryOneAsync(
                    GetSql,
                    load,
                    new NpgsqlParameter("@i_id", -86));
                Assert.That(sr.RowReturned, Is.False);
                Assert.That(sr.Value, Is.Null);

                sr = await c.ExecuteRawQueryOneAsync(
                    GetSql,
                    load,
                    new NpgsqlParameter("@i_id", 1));
                Assert.That(sr.RowReturned);
                Assert.That(sr.Value, Is.Not.Null);
                Assert.That(sr.Value?.Id, Is.EqualTo(1));
                Assert.That(sr.Value?.String, Is.EqualTo("s1"));

                const string GetSqlByid = "SELECT * FROM Test WHERE intfield = :i_int"; 
                list = await c.ExecuteRawQueryListAsync(
                    GetSqlByid,
                    load,
                    new NpgsqlParameter("@i_int", 10));
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                Assert.That(await c.ExecuteRawAsync("UPDATE Test SET intfield = 10"), Is.EqualTo(2));
            });
    }

    [Test]
    public async Task TestExecuteQueryProcAsync_Default()
    {
        static Task<TestRecord> load(IResult r)
        {
            return Task.FromResult(new TestRecord() { Id = r.GetLong("id"), Date = r.GetDateTimeNull("dT"), IntField = r.GetIntNull("intfield"), Money = r.GetDecimalNull("moneyfield"), String = r.GetString("string") });
        };

        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                Assert.That(c.IsAsync());

                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 1), new NpgsqlParameter("@i_string", "s1"), new NpgsqlParameter("@i_int", 10));
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 2), new NpgsqlParameter("@i_string", "s2"), new NpgsqlParameter("@i_int", 10));

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", -86)), Is.False);

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.That(r.GetString("string"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 1)));

                var sr = await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    load,
                    new NpgsqlParameter("@i_id", -86));
                Assert.That(sr.RowReturned, Is.False);
                Assert.That(sr.Value, Is.Null);

                sr = await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    load,
                    new NpgsqlParameter("@i_id", 1));
                Assert.That(sr.RowReturned);
                Assert.That(sr.Value, Is.Not.Null);
                Assert.That(sr.Value?.Id, Is.EqualTo(1));
                Assert.That(sr.Value?.String, Is.EqualTo("s1"));
            });
    }

#endregion ExecuteQueryProc

#region ExecuteQueryProc Null Blob

    [Test]
    public void TestExecuteQueryProc_NullBlob()
    {
        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                Assert.That(c.IsAsync(), Is.False);

                c.ExecuteProc(
                    "fn_insert_test_record",
                    new NpgsqlParameter("@i_id", 1),
                    new NpgsqlParameter("@i_string", "s1"),
                    new NpgsqlParameter("@i_int", 10),
                    new NpgsqlParameter("@i_blob", Convert.FromHexString("aabbcc")));

                c.ExecuteProc(
                    "fn_insert_test_record",
                    new NpgsqlParameter("@i_id", 2),
                    new NpgsqlParameter("@i_string", "s2"),
                    new NpgsqlParameter("@i_int", 10));

                Assert.That(c.ExecuteQueryProcOne(
                    "fn_get_test_record_by_id",
                    (r) => Convert.ToHexStringLower(r.GetBytes("blobfield")!),
                    new NpgsqlParameter("@i_id", 1)).Value, Is.EqualTo("aabbcc"));

                Assert.That(c.ExecuteQueryProcOne(
                    "fn_get_test_record_by_id",
                    (r) => r.GetBytes("blobfield"),
                    new NpgsqlParameter("@i_id", 2)).Value, Is.Null);
            });
    }
#endregion

#region ExecuteQueryProc Multiple Result Sets

    [Test]
    public void TestExecuteQueryProcMultipleResultSets()
    {
        static TestRecord load(IResult r)
        {
            return new TestRecord()
            {
                Id = r.GetLong("id"), Date = r.GetDateTimeNull("dt"), IntField = r.GetIntNull("intfield"), Money = r.GetDecimalNull("moneyfield"), String = r.GetString("string"),
                Others = r.GetColumns().Where(c => c.StartsWith("Num")).ToDictionary(c => c, c => (object)r.GetInt(c))
            };
        };

        static void process(IResultSet rs, int idx, int p)
        {
            var items = rs.GetList(load);
            Assert.That(items, Has.Count.EqualTo(3));
            for (int i = 0; i < 3; i++)
            {
                Assert.That(items[i].Id, Is.EqualTo(p * 10));
                Assert.That(items[i].String, Is.EqualTo($"s{p}"));
                Assert.That(items[i].IntField, Is.EqualTo(idx * 10));
                p++;

                Assert.That(items[i].Others?[$"Num{idx}"], Is.EqualTo(idx));
            }
        };

        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 10), new NpgsqlParameter("@i_string", "s1"), new NpgsqlParameter("@i_int", 10));
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 20), new NpgsqlParameter("@i_string", "s2"), new NpgsqlParameter("@i_int", 10));
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 30), new NpgsqlParameter("@i_string", "s3"), new NpgsqlParameter("@i_int", 10));

                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 40), new NpgsqlParameter("@i_string", "s4"), new NpgsqlParameter("@i_int", 20));
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 50), new NpgsqlParameter("@i_string", "s5"), new NpgsqlParameter("@i_int", 20));
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 60), new NpgsqlParameter("@i_string", "s6"), new NpgsqlParameter("@i_int", 20));

                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 70), new NpgsqlParameter("@i_string", "s7"), new NpgsqlParameter("@i_int", 30));
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 80), new NpgsqlParameter("@i_string", "s8"), new NpgsqlParameter("@i_int", 30));
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 90), new NpgsqlParameter("@i_string", "s9"), new NpgsqlParameter("@i_int", 30));

                c.ExecuteQueryProcMultiResultSet(
                    "fn_get_test_records_by_ints", 
                    (mrs) =>
                    {
                        IResultSet? rs;
                        rs = mrs.NextResultSet();
                        process(rs!, 1, 1);

                        rs = mrs.NextResultSet();
                        process(rs!, 2, 4);

                        rs = mrs.NextResultSet();
                        process(rs!, 3, 7);

                        Assert.That(mrs.NextResultSet(), Is.Null);
                    },
                    new NpgsqlParameter("@i_int1", 10), 
                    new NpgsqlParameter("@i_int2", 20), 
                    new NpgsqlParameter("@i_int3", 30));

                c.ExecuteQueryProcMultiResultSet(
                    "fn_get_test_records_by_ints", 
                    (mrs) =>
                    {
                        IResultSet? rs;
                        rs = mrs.NextResultSet();
                        var _ = rs!.Take(2).ToList();

                        rs = mrs.NextResultSet();
                        _ = rs!.Take(1).ToList();

                        rs = mrs.NextResultSet();
                        _ = rs!.Take(3).ToList();

                        Assert.That(mrs.NextResultSet(), Is.Null);
                    },
                    new NpgsqlParameter("@i_int1", 10), 
                    new NpgsqlParameter("@i_int2", 20), 
                    new NpgsqlParameter("@i_int3", 30));

                c.ExecuteQueryProcMultiResultSet(
                    "fn_get_test_records_by_ints", 
                    (mrs) =>
                    {
                        int p = 1;
                        foreach (var rs in mrs)
                        {
                            process(rs, p, (p-1) * 3 + 1);
                            p++;
                        }

                        Assert.That(mrs.NextResultSet(), Is.Null);
                    },
                    new NpgsqlParameter("@i_int1", 10), 
                    new NpgsqlParameter("@i_int2", 20), 
                    new NpgsqlParameter("@i_int3", 30));
            });
    }

    [Test]
    public async Task TestExecuteQueryProcMultipleResultSetsAsync()
    {
        static Task<TestRecord> load(IResult r)
        {
            return Task.FromResult(new TestRecord()
            {
                Id = r.GetLong("id"), Date = r.GetDateTimeNull("dt"), IntField = r.GetIntNull("intfield"), Money = r.GetDecimalNull("moneyfield"), String = r.GetString("string"),
                Others = r.GetColumns().Where(c => c.StartsWith("Num")).ToDictionary(c => c, c => (object)r.GetInt(c))
            });
        };

        static async Task process(IAsyncResultSet rs, int idx, int p)
        {
            var items = await rs.GetListAsync(load);
            Assert.That(items, Has.Count.EqualTo(3));
            for (int i = 0; i < 3; i++)
            {
                Assert.That(items[i].Id, Is.EqualTo(p * 10));
                Assert.That(items[i].String, Is.EqualTo($"s{p}"));
                Assert.That(items[i].IntField, Is.EqualTo(idx * 10));
                p++;

                Assert.That(items[i].Others?[$"Num{idx}"], Is.EqualTo(idx));
            }
        };

        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 10), new NpgsqlParameter("@i_string", "s1"), new NpgsqlParameter("@i_int", 10));
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 20), new NpgsqlParameter("@i_string", "s2"), new NpgsqlParameter("@i_int", 10));
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 30), new NpgsqlParameter("@i_string", "s3"), new NpgsqlParameter("@i_int", 10));

                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 40), new NpgsqlParameter("@i_string", "s4"), new NpgsqlParameter("@i_int", 20));
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 50), new NpgsqlParameter("@i_string", "s5"), new NpgsqlParameter("@i_int", 20));
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 60), new NpgsqlParameter("@i_string", "s6"), new NpgsqlParameter("@i_int", 20));

                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 70), new NpgsqlParameter("@i_string", "s7"), new NpgsqlParameter("@i_int", 30));
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 80), new NpgsqlParameter("@i_string", "s8"), new NpgsqlParameter("@i_int", 30));
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@i_id", 90), new NpgsqlParameter("@i_string", "s9"), new NpgsqlParameter("@i_int", 30));

                await c.ExecuteQueryProcMultiResultSetAsync(
                    "fn_get_test_records_by_ints", 
                    async (mrs) =>
                    {
                        IAsyncResultSet? rs;
                        rs = await mrs.NextResultSetAsync();
                        await process(rs!, 1, 1);

                        rs = await mrs.NextResultSetAsync();
                        await process(rs!, 2, 4);

                        rs = await mrs.NextResultSetAsync();
                        await process(rs!, 3, 7);

                        Assert.That(await mrs.NextResultSetAsync(), Is.Null);
                    },
                    new NpgsqlParameter("@i_int1", 10), 
                    new NpgsqlParameter("@i_int2", 20), 
                    new NpgsqlParameter("@i_int3", 30));
            });
    }

    #endregion ExecuteQueryProc Multiple Result Sets

#region ExecuteTxn / ExecuteTxnAsync
    /* uncomment to test compiler error
    [Test]
    public void TestExecuteTxn_AsyncLambda_SyncTxn()
    {
        Assert.ThrowsAsync<NotSupportedException>(async () =>
        {
            await IntegrationTestSetup.GetProvider("PlinthDBTest").ExecuteRawTxn(
                async (c) =>
                {
                    await Task.Delay(10);
                    var dt = c.ExecuteRawQueryOne("SELECT SYSUTCDATETIME() AS t", r => r.GetDateTimeNull("t")).Value;
                    Assert.IsNotNull(dt);
                });
        });

        Assert.ThrowsAsync<NotSupportedException>(async () =>
        {
            int x = await IntegrationTestSetup.GetProvider("PlinthDBTest").ExecuteRawTxn(
                async (c) =>
                {
                    await Task.Delay(10);
                    var dt = c.ExecuteRawQueryOne("SELECT SYSUTCDATETIME() AS t", r => r.GetDateTimeNull("t")).Value;
                    Assert.IsNotNull(dt);
                    return 55;
                });

            Assert.Fail();
        });
    }
    */

    [Test]
    public async Task TestExecuteTxn_AsyncLambda_AsyncTxn()
    {
        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                await Task.Delay(10);
                var dt = c.ExecuteRawQueryOne("SELECT LOCALTIMESTAMP AS t", r => r.GetDateTimeNull("t")).Value;
                Assert.That(dt, Is.Not.Null);
            });

        var x = await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                await Task.Delay(10);
                var dt = c.ExecuteRawQueryOne("SELECT LOCALTIMESTAMP AS t", r => r.GetDateTimeNull("t")).Value;
                Assert.That(dt, Is.Not.Null);
                return dt;
            });
    }

    [Test]
    public void TestExecuteTxn_SyncLambda_SyncTxn()
    {
        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                var dt = c.ExecuteRawQueryOne("SELECT LOCALTIMESTAMP AS t", r => r.GetDateTimeNull("t")).Value;
                Assert.That(dt, Is.Not.Null);
            });

        var x = IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                var dt = c.ExecuteRawQueryOne("SELECT LOCALTIMESTAMP AS t", r => r.GetDateTimeNull("t")).Value;
                Assert.That(dt, Is.Not.Null);
                return dt;
            });
    }

    [Test]
    public async Task TestExecuteTxn_SyncLambda_AsyncTxn()
    {
        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            (c) =>
            {
                var dt = c.ExecuteRawQueryOne("SELECT LOCALTIMESTAMP AS t", r => r.GetDateTimeNull("t")).Value;
                Assert.That(dt, Is.Not.Null);
                return Task.CompletedTask;
            });

        var x = await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            (c) =>
            {
                var dt = c.ExecuteRawQueryOne("SELECT LOCALTIMESTAMP AS t", r => r.GetDateTimeNull("t")).Value;
                Assert.That(dt, Is.Not.Null);
                return Task.FromResult(dt);
            });
    }
    #endregion

#region Structured Type
    [Test]
    public void TestStringArray()
    {
        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                var list = new List<string> { "abc", "123", "def" };
                var ret = c.ExecuteQueryProcList("fn_echo_nvarchar_array", 
                    r => r.GetString("val"), 
                    new NpgsqlParameter("@i_array", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Varchar) { Value = list });
                Assert.That(ret, Is.EquivalentTo(list));

                ret = c.ExecuteQueryProcList("fn_echo_nvarchar_array", 
                    r => r.GetString("val"), 
                    new NpgsqlParameter("@i_array", list));
                Assert.That(ret, Is.EquivalentTo(list));
            });
    }

    [Test]
    public void TestIntArray()
    {
        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                var list = new List<int> { 5, 8, 2, 99 };
                var ret = c.ExecuteQueryProcList("fn_echo_int_array", 
                    r => r.GetInt("val"), 
                    new NpgsqlParameter("@i_array", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Integer) { Value = list });
                Assert.That(ret, Is.EquivalentTo(list));

                ret = c.ExecuteQueryProcList("fn_echo_int_array", 
                    r => r.GetInt("val"), 
                    new NpgsqlParameter("@i_array", list));
                Assert.That(ret, Is.EquivalentTo(list));
            });
    }

    [Test]
    public void TestGuidArray()
    {
        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                var list = new List<Guid> { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() };
                var ret = c.ExecuteQueryProcList("fn_echo_uuid_array", 
                    r => r.GetGuid("val"), 
                    new NpgsqlParameter("@i_array", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Uuid) { Value = list });
                Assert.That(ret, Is.EquivalentTo(list));

                ret = c.ExecuteQueryProcList("fn_echo_uuid_array", 
                    r => r.GetGuid("val"), 
                    new NpgsqlParameter("@i_array", list));
                Assert.That(ret, Is.EquivalentTo(list));
            });
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "unit test")]
    class MyCompositeType
    {
        public Guid guid { get; set; }
        public long id { get; set; }
        public DateTime? dt { get; set; }
        public decimal? moneyfield { get; set; }
        [PgName("string")]
        public string? stringf { get; set; }
        public int? intfield { get; set; }
    }

    /*
     * NPGSQL dropped support for dynamic table types in 5.0.0
    [Test]
    public void TestStructuredTypeSingle()
    {
        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                var t = new TestRecord { Id = 5, String = "hello", Int = 77 };

                dynamic r = new ExpandoObject();
                r.guid = Guid.NewGuid();
                r.id = t.Id;
                r.dt = DateTime.UtcNow;
                r.moneyfield = 55.2m;
                (r as IDictionary<string, object>)["string"] = t.String;
                r.intfield = t.Int;

                var tl = c.ExecuteQueryProcOne("fn_echo_table_type_single",
                    r => new TestRecord
                    {
                        Id = r.GetLong("id"),
                        String = r.GetString("string"),
                        Int = r.GetInt("intfield")
                    },
                    new NpgsqlParameter("i_table", r) { DataTypeName = "tabletypetest" })
                .Value;
                Assert.IsTrue(t.Id == tl.Id && t.String == tl.String && t.Int == tl.Int);
            });
    }
    */

    [Test]
    public void TestStructuredTypeSingle_Nulls()
    {
        IntegrationTestSetup.GetCustom(dsb =>
            dsb.MapComposite<MyCompositeType>("tabletypetest"))
        .ExecuteRawTxn(
            (c) =>
            {
                var t = new TestRecord { Id = 5, String = "hello", IntField = 77 };

                var p = new MyCompositeType
                {
                    guid = Guid.NewGuid(),
                    id = t.Id
                };

                var tl = c.ExecuteQueryProcOne("fn_echo_table_type_single",
                    r => new TestRecord
                    {
                        Id = r.GetLong("id"),
                        String = r.GetString("string"),
                        IntField = r.GetIntNull("intfield"),
                        Money = r.GetDecimalNull("moneyfield"),
                        Date = r.GetDateTimeNull("dt")
                    },
                    new NpgsqlParameter("i_table", p))// { DataTypeName = "tabletypetest" })
                .Value!;
                Assert.That(tl.String, Is.Null);
                Assert.That(tl.IntField, Is.Null);
                Assert.That(tl.Money, Is.Null);
                Assert.That(tl.Date, Is.Null);
            });
    }

    /*
     * NPGSQL dropped support for dynamic table types in 5.0.0
    [Test]
    public void TestStructuredType()
    {
        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                List<TestRecord> testList = new List<TestRecord>
                {
                    new TestRecord { Id = 5, String = "hello", Int = 77 },
                    new TestRecord { Id = 6, String = "there", Int = 78 },
                    new TestRecord { Id = 7, String = "table", Int = 79 }
                };

                var table = new List<ExpandoObject>();
                foreach (var t in testList)
                {
                    dynamic r = new ExpandoObject();
                    r.guid = Guid.NewGuid();
                    r.id = t.Id;
                    r.dt = DateTime.UtcNow;
                    r.moneyfield = 55.2m;
                    (r as IDictionary<string, object>)["string"] = t.String;
                    r.intfield = t.Int;
                    table.Add(r);
                }

                var testRet = c.ExecuteQueryProcList("fn_echo_table_type",
                    r => new TestRecord
                    {
                        Id = r.GetLong("id"),
                        String = r.GetString("string"),
                        Int = r.GetInt("intfield")
                    },
                    new NpgsqlParameter("i_table", table) { DataTypeName = "tabletypetest[]" });
                foreach (var tl in testList)
                    Assert.IsTrue(testRet.Any(t => t.Id == tl.Id && t.String == tl.String && t.Int == tl.Int));
            });
    }
    */
    #endregion

#region stream

    [Test]
    public void TestStream_Read()
    {
        var bigString = string.Join("", Enumerable.Repeat("a2", 1_000_000));

        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                Assert.That(c.IsAsync(), Is.False);

                c.ExecuteProc(
                    "fn_insert_test_record",
                    new NpgsqlParameter("i_id", 1),
                    new NpgsqlParameter("i_string", "s1"),
                    new NpgsqlParameter("i_int", 10),
                    new NpgsqlParameter("i_blob", Convert.FromHexString(bigString)));

                c.ExecuteQueryProcOne(
                    "fn_get_test_record_by_id",
                    (r) =>
                    {
                        var stream = r.GetStream("blobfield");
                        var ms = new MemoryStream();
                        stream?.CopyTo(ms);
                        var data = Convert.ToHexStringLower(ms.ToArray());
                        Assert.That(data, Is.EqualTo(bigString));
                        return true;
                    },
                    new NpgsqlParameter("i_id", 1));
            });
    }

    [Test]
    public async Task TestStream_ReadAsync()
    {
        var bigString = string.Join("", Enumerable.Repeat("a2", 1_000_000));

        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                Assert.That(c.IsAsync());

                await c.ExecuteProcAsync(
                    "fn_insert_test_record",
                    new NpgsqlParameter("i_id", 1),
                    new NpgsqlParameter("i_string", "s1"),
                    new NpgsqlParameter("i_int", 10),
                    new NpgsqlParameter("i_blob", Convert.FromHexString(bigString)));

                await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    async (r) =>
                    {
                        var stream = r.GetStream("blobfield")!;
                        var ms = new MemoryStream();
                        await stream.CopyToAsync(ms);
                        var data = Convert.ToHexStringLower(ms.ToArray());
                        Assert.That(data, Is.EqualTo(bigString));
                        return true;
                    },
                    new NpgsqlParameter("i_id", 1));
            });
    }

    /*
     * Npgsql does not support sending a stream as a parameter
     */
    [Test]
    public void TestStream_Write()
    {
        var bigBytes = new MemoryStream(1_000_000);
        for (int i = 0; i < bigBytes.Capacity; i++)
            bigBytes.WriteByte(0xa2);
        bigBytes.Position = 0;

        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                Assert.That(c.IsAsync(), Is.False);

                c.ExecuteProc(
                    "fn_insert_test_record",
                    new NpgsqlParameter("i_id", 1),
                    new NpgsqlParameter("i_string", "s1"),
                    new NpgsqlParameter("i_int", 10),
                    new NpgsqlParameter("i_blob", bigBytes.ToArray()));

                c.ExecuteQueryProcOne(
                    "fn_get_test_record_by_id",
                    (r) =>
                    {
                        var stream = r.GetStream("blobfield");
                        var ms = new MemoryStream();
                        stream?.CopyTo(ms);

                        Assert.That(ms.ToArray(), Is.EqualTo(bigBytes.ToArray()).AsCollection);
                        return true;
                    },
                    new NpgsqlParameter("i_id", 1));
            });

        bigBytes.Dispose();
    }

    [Test]
    public async Task TestStream_WriteAsync()
    {
        var bigBytes = new MemoryStream(1_000_000);
        for (int i = 0; i < bigBytes.Capacity; i++)
            bigBytes.WriteByte(0xa2);
        bigBytes.Position = 0;

        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                Assert.That(c.IsAsync());

                await c.ExecuteProcAsync(
                    "fn_insert_test_record",
                    new NpgsqlParameter("i_id", 1),
                    new NpgsqlParameter("i_string", "s1"),
                    new NpgsqlParameter("i_int", 10),
                    new NpgsqlParameter("i_blob", bigBytes.ToArray()));

                await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    async (r) =>
                    {
                        var stream = r.GetStream("blobfield")!;
                        var ms = new MemoryStream();
                        await stream.CopyToAsync(ms);

                        Assert.That(ms.ToArray(), Is.EqualTo(bigBytes.ToArray()).AsCollection);
                        return true;
                    },
                    new NpgsqlParameter("i_id", 1));
            });

        bigBytes.Dispose();
    }
    #endregion

#region json

    class JsonTestObj
    {
        public int X { get; set; }
        public string? Z { get; set; }
        public string? Y { get; set; }
        public object? N { get; set; }
    }

    [Test]
    public void TestJson()
    {
        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                // insert without type spec
                c.ExecuteProc(
                    "fn_insert_jsontest",
                    new NpgsqlParameter<long>("@i_id", 1),
                    new NpgsqlParameter("@i_json", new { x = 5, z = "99a" }) { NpgsqlDbType = NpgsqlDbType.Json },
                    new NpgsqlParameter("@i_json2", new { x = 10, y = "abc" }) { NpgsqlDbType = NpgsqlDbType.Jsonb });

                var ret1 = c.ExecuteQueryProcOne("fn_get_jsontest_by_id",
                    r => (r.GetJson("json_val1"), r.GetJson("json_val2")),
                    new NpgsqlParameter<long>("@i_id", 1));

                Assert.That(ret1.Value.Item1?["x"], Is.EqualTo(5));
                Assert.That(ret1.Value.Item1?["z"], Is.EqualTo("99a"));
                Assert.That(ret1.Value.Item2?["x"], Is.EqualTo(10));
                Assert.That(ret1.Value.Item2?["y"], Is.EqualTo("abc"));

                var ret2 = c.ExecuteQueryProcOne("fn_get_jsontest_by_id",
                    r => (r.GetValue("json_val1"), r.GetValue("json_val2")),
                    new NpgsqlParameter<long>("@i_id", 1));

                Assert.That(ret2.Value.Item1, Is.InstanceOf<string>());
                Assert.That(ret2.Value.Item2, Is.InstanceOf<string>());

                ret1.Value.Item1!["n"] = 77;
                ret1.Value.Item1!["x"] = 8;
                ret1.Value.Item2!["n"] = "qpqp";
                ret1.Value.Item2!["y"] = "abcdef";
                c.ExecuteProc(
                    "fn_update_jsontest",
                    new NpgsqlParameter<long>("@i_id", 1),
                    new NpgsqlParameter("@i_json", ret1.Value.Item1) { NpgsqlDbType = NpgsqlDbType.Json },
                    new NpgsqlParameter("@i_json2", ret1.Value.Item2) { NpgsqlDbType = NpgsqlDbType.Jsonb });

                var ret3 = c.ExecuteQueryProcOne("fn_get_jsontest_by_id",
                    r => (r.GetJson("json_val1"), r.GetJson("json_val2")),
                    new NpgsqlParameter<long>("@i_id", 1));

                Assert.That(ret3.Value.Item1!["x"], Is.EqualTo(8));
                Assert.That(ret3.Value.Item1!["z"], Is.EqualTo("99a"));
                Assert.That(ret3.Value.Item1!["n"], Is.EqualTo(77));
                Assert.That(ret3.Value.Item2!["x"], Is.EqualTo(10));
                Assert.That(ret3.Value.Item2!["y"], Is.EqualTo("abcdef"));
                Assert.That(ret3.Value.Item2!["n"], Is.EqualTo("qpqp"));

                var ret4 = c.ExecuteQueryProcOne("fn_get_jsontest_by_id",
                    r => (r.GetJsonObject<JsonTestObj>("json_val1"), r.GetJsonObject<JsonTestObj>("json_val2")),
                    new NpgsqlParameter<long>("@i_id", 1));

                Assert.That(ret4.Value.Item1!.X, Is.EqualTo(8));
                Assert.That(ret4.Value.Item1!.Z, Is.EqualTo("99a"));
                Assert.That(ret4.Value.Item1!.N, Is.EqualTo(77));
                Assert.That(ret4.Value.Item2!.X, Is.EqualTo(10));
                Assert.That(ret4.Value.Item2!.Y, Is.EqualTo("abcdef"));
                Assert.That(ret4.Value.Item2!.N, Is.EqualTo("qpqp"));
            });
    }

#endregion json

#region without txn
    [Test]
    public async Task WithoutTxn_ReadOnly()
    {
        static Task<TestRecord> load(IResult r)
        {
            return Task.FromResult(new TestRecord() { Id = r.GetLong("id"), Date = r.GetDateTimeNull("dT"), IntField = r.GetIntNull("intfield"), Money = r.GetDecimalNull("moneyfield"), String = r.GetString("string") });
        };

        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                await c.ExecuteProcAsync("fn_insert_test_record", new NpgsqlParameter("@i_id", 1), new NpgsqlParameter("@i_string", "s1"), new NpgsqlParameter("@i_int", 10));
                await c.ExecuteProcAsync("fn_insert_test_record", new NpgsqlParameter("@i_id", 2), new NpgsqlParameter("@i_string", "s2"), new NpgsqlParameter("@i_int", 10));
            }
        );

        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", -86)), Is.False);
            });

        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", -86)), Is.False);

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.That(r.GetString("string"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 1)));

                var sr = await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    load,
                    new NpgsqlParameter("@i_id", -86));
                Assert.That(sr.RowReturned, Is.False);
                Assert.That(sr.Value, Is.Null);

                sr = await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    load,
                    new NpgsqlParameter("@i_id", 1));
                Assert.That(sr.RowReturned);
                Assert.That(sr.Value, Is.Not.Null);
                Assert.That(sr.Value?.Id, Is.EqualTo(1));
                Assert.That(sr.Value?.String, Is.EqualTo("s1"));

                var list = await c.ExecuteQueryProcListAsync(
                    "fn_get_test_record_by_int",
                    load,
                    new NpgsqlParameter("@i_int", 10));
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                const string GetSql = "SELECT * FROM test WHERE id = @i_id";
                Assert.That(await c.ExecuteRawQueryOneAsync(
                    GetSql,
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", -86)), Is.False);

                Assert.That(await c.ExecuteRawQueryOneAsync(
                    GetSql,
                    (r) => { Assert.That(r.GetString("string"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 1)));

                sr = await c.ExecuteRawQueryOneAsync(
                    GetSql,
                    load,
                    new NpgsqlParameter("@i_id", -86));
                Assert.That(sr.RowReturned, Is.False);
                Assert.That(sr.Value, Is.Null);

                sr = await c.ExecuteRawQueryOneAsync(
                    GetSql,
                    load,
                    new NpgsqlParameter("@i_id", 1));
                Assert.That(sr.RowReturned);
                Assert.That(sr.Value, Is.Not.Null);
                Assert.That(sr.Value?.Id, Is.EqualTo(1));
                Assert.That(sr.Value?.String, Is.EqualTo("s1"));

                const string GetSql_by_id = "SELECT * FROM test WHERE intfield = @i_int_field";
                list = await c.ExecuteRawQueryListAsync(
                    GetSql_by_id,
                    load,
                    new NpgsqlParameter("@i_int_field", 10));
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));
            });
    }

    [Test]
    public async Task WithoutTxn_OneWrite_OK()
    {
        // single op, 1 write
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.ExecuteProcAsync("fn_insert_test_record", new NpgsqlParameter("@i_id", 1), new NpgsqlParameter("@i_string", "s1"), new NpgsqlParameter("@i_int", 10));
            });

        // read, then 1 write
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.That(r.GetString("string"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 1)));

                await c.ExecuteProcAsync("fn_insert_test_record", new NpgsqlParameter("@i_id", 2), new NpgsqlParameter("@i_string", "s2"), new NpgsqlParameter("@i_int", 10));
            });

        // 1 write, then read
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.ExecuteProcAsync("fn_insert_test_record", new NpgsqlParameter("@i_id", 3), new NpgsqlParameter("@i_string", "s3"), new NpgsqlParameter("@i_int", 10));

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.That(r.GetString("string"), Is.EqualTo("s2")); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 2)));
            });

        // 1 write, then multiple read
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.ExecuteProcAsync("fn_insert_test_record", new NpgsqlParameter("@i_id", 4), new NpgsqlParameter("@i_string", "s4"), new NpgsqlParameter("@i_int", 10));

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.That(r.GetString("string"), Is.EqualTo("s3")); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 3)));

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.That(r.GetString("string"), Is.EqualTo("s4")); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 4)));
            });

        // read, 1 write, then multiple read
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.That(r.GetString("string"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 1)));

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.That(r.GetString("string"), Is.EqualTo("s2")); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 2)));

                await c.ExecuteProcAsync("fn_insert_test_record", new NpgsqlParameter("@i_id", 5), new NpgsqlParameter("@i_string", "s5"), new NpgsqlParameter("@i_int", 10));

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.That(r.GetString("string"), Is.EqualTo("s3")); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 3)));

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.That(r.GetString("string"), Is.EqualTo("s5")); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 5)));
            });
    }

    [Test]
    public async Task WithoutTxn_OneWrite_NoRollback()
    {
        // confirm row does not exist
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 1)), Is.False);
            });

        // 1 write, fail
        Assert.ThrowsAsync<NotFiniteNumberException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    await c.ExecuteProcAsync("fn_insert_test_record", new NpgsqlParameter("@i_id", 1), new NpgsqlParameter("@i_string", "s1"), new NpgsqlParameter("@i_int", 10));

                    throw new NotFiniteNumberException();
                });
        });

        // confirm write succeeded
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.That(r.GetString("string"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 1)));
            });
    }

    [Test]
    public async Task WithoutTxn_TwoWrites_NoRollbackAndFail()
    {
        // confirm rows do not exist
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 1)), Is.False);

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 2)), Is.False);
            });

        // 2 write, fail
        Assert.ThrowsAsync<NotSupportedException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    await c.ExecuteProcAsync("fn_insert_test_record", new NpgsqlParameter("@i_id", 1), new NpgsqlParameter("@i_string", "s1"), new NpgsqlParameter("@i_int", 10));

                    await c.ExecuteProcAsync("fn_insert_test_record", new NpgsqlParameter("@i_id", 2), new NpgsqlParameter("@i_string", "s2"), new NpgsqlParameter("@i_int", 10));
                });
        });

        // confirm first write succeeded, second didn't happen
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.That(r.GetString("string"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 1)));

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 2)), Is.False);
            });
    }

    [Test]
    public async Task WithoutTxn_OneRead_NoRollback()
    {
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.ExecuteProcAsync("fn_insert_test_record", new NpgsqlParameter("@i_id", 1), new NpgsqlParameter("@i_string", "s1"), new NpgsqlParameter("@i_int", 10));
            });

        // confirm write succeeded, but fail
        bool errorCalled = false;
        Assert.ThrowsAsync<NotFiniteNumberException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    c.AddErrorAction("should get called", e => { errorCalled = true; });

                    Assert.That(await c.ExecuteQueryProcOneAsync(
                        "fn_get_test_record_by_id",
                        (r) => { Assert.That(r.GetString("string"), Is.EqualTo("s1")); return Task.CompletedTask; },
                        new NpgsqlParameter("@i_id", 1)));

                    throw new NotFiniteNumberException();
                });
        });

        Assert.That(errorCalled);
    }

    [Test]
    public async Task WithoutTxn_OneRead_TransientRetry()
    {
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.ExecuteProcAsync("fn_insert_test_record", new NpgsqlParameter("@i_id", 1), new NpgsqlParameter("@i_string", "s1"), new NpgsqlParameter("@i_int", 10));
            });

        int retries = 0;
        int errorCalled = 0;
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                c.AddErrorAction("should not get called", e => { errorCalled++; });

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.That(r.GetString("string"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 1)));

                retries++;
                if (retries < 3)
                    throw new TimeoutException("should retry");
            });

        Assert.That(retries, Is.EqualTo(3));
        Assert.That(errorCalled, Is.EqualTo(2));
    }

    [Test]
    public async Task WithoutTxn_OneWrite_NoTransientRetry()
    {
        int retries = 0;
        Assert.ThrowsAsync<TimeoutException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    await c.ExecuteProcAsync("fn_insert_test_record", new NpgsqlParameter("@i_id", 1), new NpgsqlParameter("@i_string", "s1"), new NpgsqlParameter("@i_int", 10));

                    retries++;
                    if (retries < 3)
                        throw new TimeoutException("should retry");
                });
        });

        Assert.That(retries, Is.EqualTo(1));

        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => { Assert.That(r.GetString("string"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new NpgsqlParameter("@i_id", 1)));
            });
    }
    #endregion

#region without txn - post error
    [Test]
    public void WithoutTxn_PostError_Async_1call()
    {
        bool called = false;
        Exception? sent = null;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncErrorAction("f", async (e) => { sent = e; called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));

                await Task.Delay(10);
                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
        Assert.That(sent?.Message, Is.EqualTo("failure!"));
    }

    [Test]
    public void WithoutTxn_PostError_Sync_AsyncAction()
    {
        bool called = false;
        Exception? sent = null;
        Assert.Throws<Exception>(() => IntegrationTestSetup.ExecWithout(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncErrorAction("f", async (e) => { sent = e; called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));

                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
        Assert.That(sent?.Message, Is.EqualTo("failure!"));
    }

    [Test]
    public void WithoutTxn_PostError_Async_AsyncActionSyncCall()
    {
        bool called = false;
        Exception? sent = null;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddErrorAction("f", (e) => { sent = e; called = true; File.Delete(fn); });

                Assert.That(File.Exists(fn));

                await Task.Delay(10);
                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
        Assert.That(sent?.Message, Is.EqualTo("failure!"));
    }

    [Test]
    public void WithoutTxn_PostError_Async_AsyncActionAsyncCall_WrongMethod()
    {
        bool called = false;
        Exception? sent = null;
        Assert.Throws<NotSupportedException>(() => IntegrationTestSetup.ExecWithout(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddErrorAction("f", async (e) => { sent = e; called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));

                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")));
        Assert.That(called, Is.False);
        Assert.That(sent, Is.Null);
    }

    [Test]
    public void WithoutTxn_PostError_Async_2calls()
    {
        bool first_called = false;
        bool first_called_second = false;
        bool second_called = false;
        bool second_called_first = false;
        Exception? sent_e1 = null, sent_e2 = null;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncErrorAction("f", async (e1) =>
                {
                    sent_e1 = e1;
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")), Is.False);
                    first_called = true;
                    if (second_called)
                        first_called_second = true;
                    File.Delete(fn);
                    await Task.Delay(10);
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddAsyncErrorAction("f", async (e2) =>
                {
                    sent_e2 = e2;
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")));
                    second_called = true;
                    if (!first_called)
                        second_called_first = true;
                    File.Delete(fn2);
                    await Task.Delay(10);
                });

                await Task.Delay(10);
                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_second);
        Assert.That(second_called_first);
        Assert.That(sent_e1?.Message, Is.EqualTo("failure!"));
        Assert.That(sent_e2?.Message, Is.EqualTo("failure!"));
    }

    [Test]
    public void WithoutTxn_PostError_Async_2calls_throws()
    {
        bool first_called = false;
        bool first_called_second = false;
        bool second_called = false;
        bool second_called_first = false;
        Exception? sent_e1 = null, sent_e2 = null;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncErrorAction("f", async (e1) =>
                {
                    sent_e1 = e1;
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")), Is.False);
                    first_called = true;
                    if (second_called)
                        first_called_second = true;
                    File.Delete(fn);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddAsyncErrorAction("f", async (e2) =>
                {
                    sent_e2 = e2;
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")));
                    second_called = true;
                    if (!first_called)
                        second_called_first = true;
                    File.Delete(fn2);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                await Task.Delay(10);
                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_second);
        Assert.That(second_called_first);
        Assert.That(sent_e1?.Message, Is.EqualTo("failure!"));
        Assert.That(sent_e2?.Message, Is.EqualTo("failure!"));
    }

    [Test]
    public void WithoutTxn_PostError_Async_2calls_throws_1stThrows()
    {
        bool first_called = false;
        bool first_called_second = false;
        bool second_called = false;
        bool second_called_first = false;
        Exception? sent_e1 = null, sent_e2 = null;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws-1stThrows.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncErrorAction("f", async (e1) =>
                {
                    sent_e1 = e1;
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws-1stThrows.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws-1stThrows.tmp")));
                    first_called = true;
                    if (second_called)
                        first_called_second = true;
                    File.Delete(fn);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws-1stThrows.tmp");
                File.WriteAllText(fn2, "this is a file2");

                if (fn2 != null)
                    throw new Exception("failure!");
                c.AddAsyncErrorAction("f", async (e2) =>
                {
                    sent_e2 = e2;
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws-1stThrows.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws-1stThrows.tmp")));
                    second_called = true;
                    if (!first_called)
                        second_called_first = true;
                    File.Delete(fn2!);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                await Task.Delay(10);
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws-1stThrows.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws-1stThrows.tmp")));
        Assert.That(first_called);
        Assert.That(second_called, Is.False);
        Assert.That(first_called_second, Is.False);
        Assert.That(second_called_first, Is.False);
        Assert.That(sent_e1?.Message, Is.EqualTo("failure!"));
        Assert.That(sent_e2, Is.Null);
    }
    #endregion

#region without txn - Post Close Action
    [Test]
    public async Task WithoutTxn_PostClose_Async_1call()
    {
        bool called = false;
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncPostCloseAction("f", async () => { called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));

                await Task.Delay(10);
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public void WithoutTxn_PostClose_Sync_AsyncAction()
    {
        bool called = false;
        IntegrationTestSetup.ExecWithout(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncPostCloseAction("f", async () => { called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public async Task WithoutTxn_PostClose_Async_AsyncActionSyncCall()
    {
        bool called = false;
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddPostCloseAction("f", () => { called = true; File.Delete(fn); });

                Assert.That(File.Exists(fn));

                await Task.Delay(10);
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public void WithoutTxn_PostClose_Async_AsyncActionAsyncCall_WrongMethod()
    {
        bool called = false;
        Assert.Throws<NotSupportedException>(() => IntegrationTestSetup.ExecWithout(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddPostCloseAction("f", async () => { called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")));
        Assert.That(called, Is.False);
    }

    [Test]
    public async Task WithoutTxn_PostClose_Async_2calls()
    {
        bool first_called = false;
        bool first_called_first = false;
        bool second_called = false;
        bool second_called_second = false;
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncPostCloseAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")));
                    first_called = true;
                    if (!second_called)
                        first_called_first = true;
                    File.Delete(fn);
                    await Task.Delay(10);
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddAsyncPostCloseAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")), Is.False);
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")));
                    second_called = true;
                    if (first_called)
                        second_called_second = true;
                    File.Delete(fn2);
                    await Task.Delay(10);
                });

                await Task.Delay(10);
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_first);
        Assert.That(second_called_second);
    }

    [Test]
    public async Task WithoutTxn_PostClose_Async_2calls_throws()
    {
        bool first_called = false;
        bool first_called_first = false;
        bool second_called = false;
        bool second_called_second = false;
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncPostCloseAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")));
                    first_called = true;
                    if (!second_called)
                        first_called_first = true;
                    File.Delete(fn);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddAsyncPostCloseAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")), Is.False);
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")));
                    second_called = true;
                    if (first_called)
                        second_called_second = true;
                    File.Delete(fn2);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                await Task.Delay(10);
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_first);
        Assert.That(second_called_second);
    }

    [Test]
    public async Task WithoutTxn_PostClose_Async_Throw()
    {
        bool called = false;
        bool caught = false;
        try
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    await Task.Delay(10);
                    string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback-throw.tmp");
                    File.WriteAllText(fn, "this is a file");
                    c.AddAsyncPostCloseAction("f", async () => { await Task.Delay(10); called = true; File.Delete(fn); });

                    Assert.That(File.Exists(fn));

                    throw new Exception("failure");
                });
        }
        catch (Exception e)
        {
            caught = true;
            Assert.That(e.Message, Is.EqualTo("failure"));
        }

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback-throw.tmp")));
        Assert.That(called, Is.False);
        Assert.That(caught);
    }
    #endregion without txn - Post Close Action

    #region isolation level
    // [Test] postgres does not support read uncommitted, it behaves like read committed
    public async Task IsolationLevel_ReadUncommitted()
    {
        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                await c.ExecuteProcAsync("fn_insert_test_record", new NpgsqlParameter("@i_id", 1), new NpgsqlParameter("@i_string", "s1"), new NpgsqlParameter("@i_int", 10));

                // confirms uncommitted row above can be read with read uncommitted
                await IntegrationTestSetup.Get().ExecuteTxnAsync(
                    async (c2) =>
                    {
                        Assert.That(await c2.ExecuteQueryProcOneAsync(
                            "fn_get_test_record_by_id",
                            (r) => { Assert.That(r.GetString("string"), Is.EqualTo("s1")); return Task.CompletedTask; },
                            new NpgsqlParameter("@i_id", 1)));
                    },
                    commandTimeout: 1,
                    isolationLevel: IsolationLevel.ReadUncommitted);
            });
    }

    [Test]
    [TestCase(IsolationLevel.ReadUncommitted)]
    [TestCase(IsolationLevel.ReadCommitted)]
    public async Task IsolationLevel_ReadCommitted(IsolationLevel isolationLevel)
    {
        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                await c.ExecuteProcAsync("fn_insert_test_record", new NpgsqlParameter("@i_id", 1), new NpgsqlParameter("@i_string", "s1"), new NpgsqlParameter("@i_int", 10));

                // sub transaction will not see above row due to transaction isolation
                // postgres behaves differently than sql server (which blocks on this)
                await IntegrationTestSetup.Get().ExecuteTxnAsync(
                    async (c2) =>
                    {
                        Assert.That(await c2.ExecuteQueryProcOneAsync(
                            "fn_get_test_record_by_id",
                            (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                            new NpgsqlParameter("@i_id", 1)), Is.False);
                    },
                    commandTimeout: 1,
                    isolationLevel: isolationLevel);
            });
    }

    [Test]
    public async Task IsolationLevel_Serializable()
    {
        await IntegrationTestSetup.Get().ExecuteWithoutTxnAsync(
            async (c) =>
            {
                await c.ExecuteProcAsync("fn_insert_test_record", new NpgsqlParameter("@i_id", 1), new NpgsqlParameter("@i_string", "s1"), new NpgsqlParameter("@i_int", 10));
            });

        bool done = false;
        Task? t = null;
        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                Assert.That((await c.ExecuteRawQueryOneAsync(
                    "SELECT COUNT(*) AS C FROM Test",
                    (r) => Task.FromResult(r.GetInt("C")))).Value, Is.EqualTo(1));

                t = Task.Run(async () => 
                {
                    await IntegrationTestSetup.Get().ExecuteTxnAsync(
                        async (c2) =>
                        {
                            await c2.ExecuteProcAsync("fn_insert_test_record", new NpgsqlParameter("@i_id", 2), new NpgsqlParameter("@i_string", "s2"), new NpgsqlParameter("@i_int", 10));
                        },
                        commandTimeout: 5);
                    done = true;
                });

                // insert txn should be blocked and we still get one row
                await Task.Delay(100);

                Assert.That((await c.ExecuteRawQueryOneAsync(
                    "SELECT COUNT(*) AS C FROM Test",
                    (r) => Task.FromResult(r.GetInt("C")))).Value, Is.EqualTo(1));
            },
            isolationLevel: IsolationLevel.Serializable,
            commandTimeout: 1);

        while (!done)
            await Task.Delay(10);

        // now insert txn is complete, we have 2 rows
        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                Assert.That((await c.ExecuteRawQueryOneAsync(
                    "SELECT COUNT(*) AS C FROM Test",
                    (r) => Task.FromResult(r.GetInt("C")))).Value, Is.EqualTo(2));
            });
    }
    #endregion isolation level
}
