// uncomment #define in DatabaseIntTest to run
using NUnit.Framework;
using Plinth.Database.Dapper.PgSql;
using Npgsql;

#if !NET9_0_OR_GREATER
using Convert = Plinth.Common.Utils.BackPort.PlinthConvert;
#endif

namespace Tests.Plinth.Database.PgSql.Integration;

[TestFixture]
partial class DatabaseIntTest
{
    class TestRecordDapper
    {
        public long ID { get; set; }
        public DateTime? DT { get; set; }
        public string? String { get; set; }
        public int? IntField { get; set; }
        public decimal? MoneyField { get; set; }
        public byte[]? BlobField { get; set; }
    }

    #region ExecuteQueryProc

    [Test]
    public void Dapper_TestExecuteQueryProc()
    {
        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                Assert.That(c.IsAsync(), Is.False);

                var dt = new DateTime(2017, 5, 4, 8, 57, 23, 112, DateTimeKind.Utc);
                var dt2 = new DateTime(2017, 2, 7, 16, 23, 36, 812, DateTimeKind.Utc);

                c.Dapper().ExecuteProc(
                    "fn_insert_test_record",
                    new { i_id = 1, i_string = "s1", i_int = 10, i_int_field_2 = 55  });

                c.Dapper().ExecuteProc(
                    "fn_insert_test_record",
                    new { i_id = 2, i_string = "s2", i_int = 10});

                var obj = c.Dapper().ExecuteQueryProcOne<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 86 });
                Assert.That(obj, Is.Null);

                obj = c.Dapper().ExecuteQueryProcOne<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj!.String, Is.EqualTo("s1"));

                var sr = c.Dapper().ExecuteQueryProcOne<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = -86 });
                Assert.That(sr, Is.Null);

                sr = c.Dapper().ExecuteQueryProcOne<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 1 });
                Assert.That(sr, Is.Not.Null);
                Assert.That(sr!.Id, Is.EqualTo(1));
                Assert.That(sr.String, Is.EqualTo("s1"));
                Assert.That(sr.IntField2, Is.EqualTo(55));

                var d = c.Dapper().ExecuteQueryProcOne<dynamic>(
                    "fn_echo_bit",
                    new { i_bit = true });
                Assert.That((bool)d!.bitval);

                d = c.Dapper().ExecuteQueryProcOne<dynamic>(
                    "fn_echo_bit",
                    new { i_bit = true });
                Assert.That((bool)d!.bitval);

                var list = c.Dapper().ExecuteQueryProcList<TestRecord>(
                    "fn_get_test_record_by_int",
                    new { i_int = 10 })
                    .ToList();
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                // raw

                const string GetSql = "select * from test where id = @i_id";
                Assert.That(c.Dapper().ExecuteRawQueryOne<TestRecord>(
                    GetSql,
                    new { i_id = -86 }), Is.Null);

                sr = c.Dapper().ExecuteRawQueryOne<TestRecord>(
                    GetSql,
                    new { i_id = 1 });
                Assert.That(sr!.String, Is.EqualTo("s1"));
                Assert.That(sr.Id, Is.EqualTo(1));

                const string GetSqlByID = "select * from test where intfield = @i_int";
                list = c.Dapper().ExecuteRawQueryList<TestRecord>(
                    GetSqlByID,
                    new { i_int = 10 })
                    .ToList();
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                Assert.That(c.Dapper().ExecuteRaw("update test set intfield = 10"), Is.EqualTo(2));
                Assert.That(c.Dapper().ExecuteRaw("update test set intfield = @i_new_int", new { i_new_int = 20 }), Is.EqualTo(2));
            });
    }

    [Test]
    public void Dapper_TestExecuteQueryProc_Default()
    {
        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                Assert.That(c.IsAsync(), Is.False);

                c.Dapper().ExecuteProc(
                    "fn_insert_test_record",
                    new { i_id = 1, i_string = "s1", i_int = 10 });

                c.Dapper().ExecuteProc(
                    "fn_insert_test_record",
                    new { i_id = 2, i_string = "s2", i_int = 10 });

                Assert.That(c.Dapper().ExecuteQueryProcOne<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = -86 }), Is.Null);

                var d = c.Dapper().ExecuteQueryProcOne<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 1 });

                Assert.That(d!.String, Is.EqualTo("s1"));
            });
    }

    [Test]
    public async Task Dapper_TestExecuteQueryProcAsync()
    {
        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                Assert.That(c.IsAsync());

                await c.Dapper().ExecuteProcAsync(
                    "fn_insert_test_record",
                    new { i_id = 1, i_string = "s1", i_int = 10 });

                await c.Dapper().ExecuteProcAsync(
                    "fn_insert_test_record",
                    new { i_id = 2, i_string = "s2", i_int = 10 });

                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 86 });
                Assert.That(obj, Is.Null);

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj!.String, Is.EqualTo("s1"));

                var sr = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = -86 });
                Assert.That(sr, Is.Null);

                sr = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 1 });
                Assert.That(sr, Is.Not.Null);
                Assert.That(sr!.Id, Is.EqualTo(1));
                Assert.That(sr.String, Is.EqualTo("s1"));

                var d = await c.Dapper().ExecuteQueryProcOneAsync<dynamic>(
                    "fn_echo_bit",
                    new { i_bit = true });
                Assert.That((bool)d!.bitval);

                d = await c.Dapper().ExecuteQueryProcOneAsync<dynamic>(
                    "fn_echo_bit",
                    new { i_bit = true });
                Assert.That((bool)d!.bitval);

                var list = (await c.Dapper().ExecuteQueryProcListAsync<TestRecord>(
                    "fn_get_test_record_by_int",
                    new { i_int = 10 }))
                    .ToList();
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                // raw

                const string GetSql = "select * from test where id = @i_id";
                Assert.That(await c.Dapper().ExecuteRawQueryOneAsync<TestRecord>(
                    GetSql,
                    new { i_id = -86 }), Is.Null);

                sr = await c.Dapper().ExecuteRawQueryOneAsync<TestRecord>(
                    GetSql,
                    new { i_id = 1 });
                Assert.That(sr!.String, Is.EqualTo("s1"));
                Assert.That(sr.Id, Is.EqualTo(1));

                const string GetSqlByID = "select * from test where intfield = @i_int";
                list = (await c.Dapper().ExecuteRawQueryListAsync<TestRecord>(
                    GetSqlByID,
                    new { i_int = 10 }))
                    .ToList();
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                Assert.That(await c.Dapper().ExecuteRawAsync("update test set intfield = 10"), Is.EqualTo(2));
                Assert.That(await c.Dapper().ExecuteRawAsync("update test set intfield = @i_new_int", new { i_new_int = 20 }), Is.EqualTo(2));
            });
    }

    public enum Test2Type
    {
        Value1 = 2,
        ValueAnother = 7
    }

    class Test2
    {
        public Test2Type? EnumStr { get; set; }
        public Test2Type? EnumInt { get; set; }
    }

    [Test]
    public async Task Dapper_TestEnumsAsync()
    {
        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                await c.Dapper().ExecuteRawAsync("insert into test2 (enumstr, enumint) VALUES (@EnumStr, @EnumInt)",
                    new Test2 { EnumInt = Test2Type.Value1, EnumStr = Test2Type.Value1 });

                var r = await c.Dapper().ExecuteRawQueryOneAsync<Test2>("select * from test2 LIMIT 1");
                Assert.That(r!.EnumInt, Is.EqualTo(Test2Type.Value1));
                Assert.That(r!.EnumStr, Is.EqualTo(Test2Type.Value1));

                dynamic? r2 = await c.Dapper().ExecuteRawQueryOneAsync<dynamic>("select * from test2 LIMIT 1");
                Assert.That(r2!.enumint, Is.EqualTo(2));
                Assert.That(r2.enumstr, Is.EqualTo("2"));

                await c.Dapper().ExecuteRawAsync("truncate table test2");
                await c.Dapper().ExecuteRawAsync("insert into test2 (enumStr, enumInt) VALUES (@i_enumStr, @i_enumInt)",
                    new { i_enumInt = Test2Type.Value1, i_enumStr = Test2Type.Value1.ToString() });

                r = await c.Dapper().ExecuteRawQueryOneAsync<Test2>("select * from test2 LIMIT 1");
                Assert.That(r!.EnumInt, Is.EqualTo(Test2Type.Value1));
                Assert.That(r!.EnumStr, Is.EqualTo(Test2Type.Value1));

                r2 = await c.Dapper().ExecuteRawQueryOneAsync<dynamic>("select * from test2 LIMIT 1");
                Assert.That(r2!.enumint, Is.EqualTo(2));
                Assert.That(r2.enumstr, Is.EqualTo("Value1"));
            });
    }

    [Test]
    public async Task Dapper_TestExecuteQueryProc_DefaultAsync()
    {
        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                Assert.That(c.IsAsync());

                await c.Dapper().ExecuteProcAsync(
                    "fn_insert_test_record",
                    new { i_id = 1, i_string = "s1", i_int = 10 });

                await c.Dapper().ExecuteProcAsync(
                    "fn_insert_test_record",
                    new { i_id = 2, i_string = "s2", i_int = 10 });

                Assert.That(await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = -86 }), Is.Null);

                var d = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 1 });

                Assert.That(d!.String, Is.EqualTo("s1"));
            });
    }
#endregion ExecuteQueryProc

#region ExecuteQueryProc Null Blob

    [Test]
    public void Dapper_TestExecuteQueryProc_NullBlob()
    {
        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                Assert.That(c.IsAsync(), Is.False);

                c.Dapper().ExecuteProc(
                    "fn_insert_test_record",
                    new { i_id = 1, i_string = "s1", i_int = 10, i_blob = Convert.FromHexString("aabbcc") });

                c.Dapper().ExecuteProc(
                    "fn_insert_test_record",
                    new { i_id = 2, i_string = "s2", i_int = 10 });

                var d = c.Dapper().ExecuteQueryProcOne<TestRecordDapper>(
                    "fn_get_test_record_by_id",
                    new { i_id = 1 });

                Assert.That(Convert.ToHexStringLower(d!.BlobField!), Is.EqualTo("aabbcc"));

                d = c.Dapper().ExecuteQueryProcOne<TestRecordDapper>(
                    "fn_get_test_record_by_id",
                    new { i_id = 2 });
                Assert.That(d!.BlobField, Is.Null);
            });
    }
#endregion

#region ExecuteQueryProc Multiple Result Sets

    [Test]
    public void Dapper_TestExecuteQueryProcMultipleResultSets()
    {
        static void process(List<TestRecord> items, int idx, int p)
        {
            Assert.That(items, Has.Count.EqualTo(3));
            for (int i = 0; i < 3; i++)
            {
                Assert.That(items[i].Id, Is.EqualTo(p * 10));
                Assert.That(items[i].String, Is.EqualTo($"s{p}"));
                Assert.That(items[i].IntField, Is.EqualTo(idx * 10));
                p++;
            }
        };

        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                c.Dapper().ExecuteProc("fn_insert_test_record", new { i_id = 10, i_string = "s1", i_int = 10 });
                c.Dapper().ExecuteProc("fn_insert_test_record", new { i_id = 20, i_string = "s2", i_int = 10 });
                c.Dapper().ExecuteProc("fn_insert_test_record", new { i_id = 30, i_string = "s3", i_int = 10 });

                c.Dapper().ExecuteProc("fn_insert_test_record", new { i_id = 40, i_string = "s4", i_int = 20 });
                c.Dapper().ExecuteProc("fn_insert_test_record", new { i_id = 50, i_string = "s5", i_int = 20 });
                c.Dapper().ExecuteProc("fn_insert_test_record", new { i_id = 60, i_string = "s6", i_int = 20 });

                c.Dapper().ExecuteProc("fn_insert_test_record", new { i_id = 70, i_string = "s7", i_int = 30 });
                c.Dapper().ExecuteProc("fn_insert_test_record", new { i_id = 80, i_string = "s8", i_int = 30 });
                c.Dapper().ExecuteProc("fn_insert_test_record", new { i_id = 90, i_string = "s9", i_int = 30 });

                c.Dapper().ExecuteQueryProcMultiResultSet(
                    "fn_get_test_records_by_ints",
                    (mrs) =>
                    {
                        Assert.That(mrs.ResultSetCount, Is.EqualTo(3));

                        var r = mrs.GetList<TestRecord>().ToList();

                        process(r, 1, 1);

                        r = mrs.GetList<TestRecord>().ToList();
                        process(r, 2, 4);

                        var o = mrs.GetOne<TestRecord>()!;
                        Assert.That(o!.Id, Is.EqualTo(70));
                        Assert.That(o.IntField, Is.EqualTo(30));

                        Assert.Throws<InvalidOperationException>(() => mrs.GetOne<TestRecord>());
                    },
                    new { i_int1 = 10, i_int2 = 20, i_int3 = 30 });
            });
    }

    [Test]
    public async Task Dapper_TestExecuteQueryProcMultipleResultSetsAsync()
    {
        static void process(List<TestRecord> items, int idx, int p)
        {
            Assert.That(items, Has.Count.EqualTo(3));
            for (int i = 0; i < 3; i++)
            {
                Assert.That(items[i].Id, Is.EqualTo(p * 10));
                Assert.That(items[i].String, Is.EqualTo($"s{p}"));
                Assert.That(items[i].IntField, Is.EqualTo(idx * 10));
                p++;
            }
        };

        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 10, i_string = "s1", i_int = 10 });
                await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 20, i_string = "s2", i_int = 10 });
                await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 30, i_string = "s3", i_int = 10 });

                await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 40, i_string = "s4", i_int = 20 });
                await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 50, i_string = "s5", i_int = 20 });
                await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 60, i_string = "s6", i_int = 20 });

                await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 70, i_string = "s7", i_int = 30 });
                await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 80, i_string = "s8", i_int = 30 });
                await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 90, i_string = "s9", i_int = 30 });

                await c.Dapper().ExecuteQueryProcMultiResultSetAsync(
                    "fn_get_test_records_by_ints",
                    async (mrs) =>
                    {
                        Assert.That(mrs.ResultSetCount, Is.EqualTo(3));

                        var r = (await mrs.GetListAsync<TestRecord>()).ToList();

                        process(r, 1, 1);

                        r = (await mrs.GetListAsync<TestRecord>()).ToList();
                        process(r, 2, 4);

                        var o = await mrs.GetOneAsync<TestRecord>()!;
                        Assert.That(o!.Id, Is.EqualTo(70));
                        Assert.That(o.IntField, Is.EqualTo(30));

                        Assert.ThrowsAsync<InvalidOperationException>(async () => await mrs.GetOneAsync<TestRecord>());
                    },
                    new { i_int1 = 10, i_int2 = 20, i_int3 = 30 });
            });
    }

    #endregion ExecuteQueryProc Multiple Result Sets

    #region without txn
    [Test]
    public async Task Dapper_WithoutTxn_ReadOnly()
    {
        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                Assert.That(c.IsAsync());

                await c.Dapper().ExecuteProcAsync(
                    "fn_insert_test_record",
                    new { i_id = 1, i_string = "s1", i_int = 10 });

                await c.Dapper().ExecuteProcAsync(
                    "fn_insert_test_record",
                    new { i_id = 2, i_string = "s2", i_int = 10 });
            }
        );

        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 86 });
                Assert.That(obj, Is.Null);
            });

        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 86 });
                Assert.That(obj, Is.Null);

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s1"));

                var sr = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = -86 });
                Assert.That(sr, Is.Null);

                sr = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 1 });
                Assert.That(sr, Is.Not.Null);
                Assert.That(sr?.Id, Is.EqualTo(1));
                Assert.That(sr?.String, Is.EqualTo("s1"));

                var d = await c.Dapper().ExecuteQueryProcOneAsync<dynamic>(
                    "fn_echo_bit",
                    new { i_bit = true });
                Assert.That((bool)d?.bitval);

                d = await c.Dapper().ExecuteQueryProcOneAsync<dynamic>(
                    "fn_echo_bit",
                    new { i_bit = true });
                Assert.That((bool)d?.bitval);

                var list = (await c.Dapper().ExecuteQueryProcListAsync<TestRecord>(
                    "fn_get_test_record_by_int",
                    new { i_int = 10 }))
                    .ToList();
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                // raw

                const string GetSql = "SELECT * FROM test WHERE id = @i_id";
                Assert.That(await c.Dapper().ExecuteRawQueryOneAsync<TestRecord>(
                    GetSql,
                    new { i_id = -86 }), Is.Null);

                sr = await c.Dapper().ExecuteRawQueryOneAsync<TestRecord>(
                    GetSql,
                    new { i_id = 1 });
                Assert.That(sr?.String, Is.EqualTo("s1"));
                Assert.That(sr?.Id, Is.EqualTo(1));

                const string GetSqlByID = "SELECT * FROM test WHERE intfield = @i_int";
                list = (await c.Dapper().ExecuteRawQueryListAsync<TestRecord>(
                    GetSqlByID,
                    new { i_int = 10 }))
                    .ToList();
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));
            });
    }

    [Test]
    public async Task Dapper_WithoutTxn_OneWrite_OK()
    {
        // single op, 1 write
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 1, i_string = "s1", i_int = 10 });
            });

        // read, then 1 write
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s1"));

                await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 2, i_string = "s2", i_int = 10 });
            });

        // 1 write, then read
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 3, i_string = "s3", i_int = 10 });

                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 2 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s2"));
            });

        // 1 write, then multiple read
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 4, i_string = "s4", i_int = 10 });

                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 3 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s3"));

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 4 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s4"));
            });

        // read, 1 write, then multiple read
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s1"));

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 2 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s2"));

                await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 5, i_string = "s5", i_int = 10 });

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 3 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s3"));

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 5 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s5"));
            });
    }

    [Test]
    public async Task Dapper_WithoutTxn_OneWrite_NoRollback()
    {
        // confirm row does not exist
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 1 });
                Assert.That(obj, Is.Null);
            });

        // 1 write, fail
        Assert.ThrowsAsync<NotFiniteNumberException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 1, i_string = "s1", i_int = 10 });

                    throw new NotFiniteNumberException();
                });
        });

        // confirm write succeeded
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s1"));
            });
    }

    [Test]
    public async Task Dapper_WithoutTxn_TwoWrites_NoRollbackAndFail()
    {
        // confirm rows do not exist
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 1 });
                Assert.That(obj, Is.Null);

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 2 });
                Assert.That(obj, Is.Null);
            });

        // 2 write, fail
        Assert.ThrowsAsync<NotSupportedException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 1, i_string = "s1", i_int = 10 });

                    await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 2, i_string = "s2", i_int = 10 });
                });
        });

        // confirm first write succeeded, second didn't happen
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s1"));

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 2 });
                Assert.That(obj, Is.Null);
            });
    }

    [Test]
    public async Task Dapper_WithoutTxn_TwoWritesMixed_NoRollbackAndFail()
    {
        // confirm rows do not exist
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 1 });
                Assert.That(obj, Is.Null);

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 2 });
                Assert.That(obj, Is.Null);
            });

        // 2 write [dapper then native], fail
        Assert.ThrowsAsync<NotSupportedException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 1, i_string = "s1", i_int = 10 });

                    await c.ExecuteProcAsync("fn_insert_test_record", new NpgsqlParameter("@i_id", 2), new NpgsqlParameter("@i_string", "s2"), new NpgsqlParameter("@i_int", 10));
                });
        });

        // confirm first write succeeded, second didn't happen
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s1"));

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 2 });
                Assert.That(obj, Is.Null);
            });

        // 2 write [native then dapper], fail
        Assert.ThrowsAsync<NotSupportedException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    await c.ExecuteProcAsync("fn_insert_test_record", new NpgsqlParameter("@i_id", 3), new NpgsqlParameter("@i_string", "s3"), new NpgsqlParameter("@i_int", 10));

                    await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 4, i_string = "s4", i_int = 10 });
                });
        });

        // confirm first write succeeded, second didn't happen
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 3 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s3"));

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 4 });
                Assert.That(obj, Is.Null);
            });
    }

    [Test]
    public async Task Dapper_WithoutTxn_OneRead_NoRollback()
    {
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 1, i_string = "s1", i_int = 10 });
            });

        // confirm write succeeded, but fail
        Assert.ThrowsAsync<NotFiniteNumberException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                        "fn_get_test_record_by_id",
                        new { i_id = 1 });
                    Assert.That(obj, Is.Not.Null);
                    Assert.That(obj?.String, Is.EqualTo("s1"));

                    throw new NotFiniteNumberException();
                });
        });
    }

    [Test]
    public async Task Dapper_WithoutTxn_OneRead_TransientRetry()
    {
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 1, i_string = "s1", i_int = 10 });
            });

        int retries = 0;
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s1"));

                retries++;
                if (retries < 3)
                    throw new TimeoutException("should retry");
            });

        Assert.That(retries, Is.EqualTo(3));
    }

    [Test]
    public async Task Dapper_WithoutTxn_OneWrite_NoTransientRetry()
    {
        int retries = 0;
        Assert.ThrowsAsync<TimeoutException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    await c.Dapper().ExecuteProcAsync("fn_insert_test_record", new { i_id = 1, i_string = "s1", i_int = 10 });

                    retries++;
                    if (retries < 3)
                        throw new TimeoutException("should retry");
                });
        });

        Assert.That(retries, Is.EqualTo(1));

        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "fn_get_test_record_by_id",
                    new { i_id = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s1"));
            });
    }
    #endregion

    /*
#region ExecuteRaw Multiple Result Sets
    // the code for multiple result sets work, but there isn't a way to return multiple result sets in a raw query

    private const string Usp_GetTestRecordsByInts = @"
        SELECT
            id,
            dt,
            string,
            intfield,
            moneyfield,
            blobfield,
            int_field_2,
    		1 AS ""Num1""
            FROM public.test
        WHERE intfield = @i_int1
        ORDER BY id ASC;

        SELECT
            id,
            dt,
            string,
            intfield,
            moneyfield,
            blobfield,
            int_field_2,
    		2 AS ""Num2""
            FROM public.test
        WHERE intfield = @i_int2
        ORDER BY id ASC;

        SELECT
            id,
            dt,
            string,
            intfield,
            moneyfield,
            blobfield,
            int_field_2,
    		3 AS ""Num3""
            FROM public.test
        WHERE intfield = @i_int3
        ORDER BY id ASC;    
";

    [Test]
    public void Dapper_TestExecuteRawMultipleResultSets()
    {
        static void process(List<TestRecord> items, int idx, int p)
        {
            Assert.AreEqual(3, items.Count);
            for (int i = 0; i < 3; i++)
            {
                Assert.AreEqual(p * 10, items[i].Id);
                Assert.AreEqual($"s{p}", items[i].String);
                Assert.AreEqual(idx * 10, items[i].IntField);
                p++;
            }
        };

        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                c.Dapper().ExecuteProc("fn_insert_test_record", new { i_id = 10, i_string = "s1", i_int = 10 });
                c.Dapper().ExecuteProc("fn_insert_test_record", new { i_id = 20, i_string = "s2", i_int = 10 });
                c.Dapper().ExecuteProc("fn_insert_test_record", new { i_id = 30, i_string = "s3", i_int = 10 });

                c.Dapper().ExecuteProc("fn_insert_test_record", new { i_id = 40, i_string = "s4", i_int = 20 });
                c.Dapper().ExecuteProc("fn_insert_test_record", new { i_id = 50, i_string = "s5", i_int = 20 });
                c.Dapper().ExecuteProc("fn_insert_test_record", new { i_id = 60, i_string = "s6", i_int = 20 });

                c.Dapper().ExecuteProc("fn_insert_test_record", new { i_id = 70, i_string = "s7", i_int = 30 });
                c.Dapper().ExecuteProc("fn_insert_test_record", new { i_id = 80, i_string = "s8", i_int = 30 });
                c.Dapper().ExecuteProc("fn_insert_test_record", new { i_id = 90, i_string = "s9", i_int = 30 });

                c.Dapper().ExecuteRawQueryMultiResultSet(
                    Usp_GetTestRecordsByInts,
                    (mrs) =>
                    {
                        var r = mrs.GetList<TestRecord>().ToList();

                        process(r, 1, 1);

                        r = mrs.GetList<TestRecord>().ToList();
                        process(r, 2, 4);

                        var o = mrs.GetOne<TestRecord>()!;
                        Assert.AreEqual(70, o!.Id);
                        Assert.AreEqual(30, o.IntField);
                    },
                    new { i_int1 = 10, i_int2 = 20, i_int3 = 30 });
            });
    }

    /*
    [Test]
    public async Task TestExecuteRawMultipleResultSetsAsync()
    {
        static Task<TestRecord> load(IResult r)
        {
            return Task.FromResult(new TestRecord()
            {
                Id = r.GetLong("ID"), Date = r.GetDateTimeNull("DT"), i_int = r.GetIntNull("IntField"), Money = r.GetDecimalNull("MoneyField"), i_string = r.GetString("String"),
                Others = r.GetColumns().Where(c => c.StartsWith("Num")).ToDictionary(c => c, c => (object)r.GetInt(c))
            });
        };

        static async Task process(IAsyncResultSet rs, int idx, int p)
        {
            var items = await rs.GetListAsync(load);
            Assert.AreEqual(3, items.Count);
            for (int i = 0; i < 3; i++)
            {
                Assert.AreEqual(p * 10, items[i].Id);
                Assert.AreEqual($"s{p}", items[i].String);
                Assert.AreEqual(idx * 10, items[i].Int);
                p++;

                Assert.AreEqual(idx, items[i].Others[$"Num{idx}"]);
            }
        };

        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@ID", 10), new NpgsqlParameter("@String", "s1"), new NpgsqlParameter("@Int", 10));
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@ID", 20), new NpgsqlParameter("@String", "s2"), new NpgsqlParameter("@Int", 10));
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@ID", 30), new NpgsqlParameter("@String", "s3"), new NpgsqlParameter("@Int", 10));

                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@ID", 40), new NpgsqlParameter("@String", "s4"), new NpgsqlParameter("@Int", 20));
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@ID", 50), new NpgsqlParameter("@String", "s5"), new NpgsqlParameter("@Int", 20));
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@ID", 60), new NpgsqlParameter("@String", "s6"), new NpgsqlParameter("@Int", 20));

                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@ID", 70), new NpgsqlParameter("@String", "s7"), new NpgsqlParameter("@Int", 30));
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@ID", 80), new NpgsqlParameter("@String", "s8"), new NpgsqlParameter("@Int", 30));
                c.ExecuteProc("fn_insert_test_record", new NpgsqlParameter("@ID", 90), new NpgsqlParameter("@String", "s9"), new NpgsqlParameter("@Int", 30));

                await c.ExecuteRawQueryMultiResultSetAsync(
                    Usp_GetTestRecordsByInts, 
                    async (mrs) =>
                    {
                        IAsyncResultSet rs;
                        rs = await mrs.NextResultSetAsync();
                        await process(rs, 1, 1);

                        rs = await mrs.NextResultSetAsync();
                        await process(rs, 2, 4);

                        rs = await mrs.NextResultSetAsync();
                        await process(rs, 3, 7);

                        Assert.IsNull(await mrs.NextResultSetAsync());
                    },
                    new NpgsqlParameter("@Int1", 10), 
                    new NpgsqlParameter("@Int2", 20), 
                    new NpgsqlParameter("@Int3", 30));
            });
    }

    #endregion ExecuteQueryProc Multiple Result Sets
    */
    #region Structured Type

    /*
     * NPGSQL dropped support for dynamic table types in 5.0.0
    [Test]
    public void Dapper_TestStructuredType()
    {
        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                var list = new List<string> { "abc", "123", "def" };
                var ret = c.Dapper()
                    .ExecuteQueryProcList<string>("usp_EchoNVARCHARList",
                    new { Table = list }).ToList();
                CollectionAssert.AreEquivalent(list, ret);

                var testList = new List<TestRecord>
                {
                    new TestRecord { Id = 5, i_string = "hello", i_int = 77 },
                    new TestRecord { Id = 6, i_string = "there", i_int = 78 },
                    new TestRecord { Id = 7, i_string = "table", i_int = 79 }
                };

                var table = new DataTable();
                table.Columns.Add("GUID", typeof(Guid));
                table.Columns.Add("ID", typeof(long));
                table.Columns.Add("DT", typeof(DateTime));
                table.Columns.Add("String", typeof(string));
                table.Columns.Add("IntField", typeof(int));
                table.Columns.Add("MoneyField", typeof(decimal));
                foreach (var t in testList)
                {
                    var r = table.Rows.Add();
                    r["GUID"] = Guid.NewGuid();
                    r["ID"] = t.Id;
                    r["String"] = t.String;
                    r["IntField"] = t.Int;
                }

                var testRet = c.Dapper().ExecuteQueryProcList<TestRecordDapper>("usp_EchoTableType", 
                    new { Table = table }).ToList();
                foreach (var tl in testList)
                    Assert.IsTrue(testRet.Any(t => t.i_id == tl.Id && t.i_string == tl.String && t.IntField == tl.Int));
            });
    }
    */
    #endregion
    /*
#region stream

    [Test]
    public void TestStream_Read()
    {
        Stream stream = null;
        var bigi_string = string.Join("", Enumerable.Repeat("a2", 1_000_000));

        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                Assert.IsFalse(c.IsAsync());

                c.ExecuteProc(
                    "fn_insert_test_record",
                    new NpgsqlParameter("@ID", 1),
                    new NpgsqlParameter("@String", "s1"),
                    new NpgsqlParameter("@Int", 10),
                    new NpgsqlParameter("@Blob", HexUtil.ToBytes(bigString)));

                stream = c.ExecuteQueryProcOne(
                    "fn_get_test_record_by_id",
                    (r) => r.GetStream("BlobField"),
                    new NpgsqlParameter("@ID", 1)).Value;

            });

        var ms = new MemoryStream();
        stream.CopyTo(ms);
        var data = HexUtil.ToHex(ms.ToArray());
        Assert.AreEqual(bigString, data);
    }

    [Test]
    public async Task TestStream_ReadAsync()
    {
        Stream stream = null;
        var bigi_string = string.Join("", Enumerable.Repeat("a2", 1_000_000));

        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                Assert.IsTrue(c.IsAsync());

                await c.ExecuteProcAsync(
                    "fn_insert_test_record",
                    new NpgsqlParameter("@ID", 1),
                    new NpgsqlParameter("@String", "s1"),
                    new NpgsqlParameter("@Int", 10),
                    new NpgsqlParameter("@Blob", HexUtil.ToBytes(bigString)));

                var res = await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => Task.FromResult(r.GetStream("BlobField")),
                    new NpgsqlParameter("@ID", 1));
                stream = res.Value;
            });

        var ms = new MemoryStream();
        stream.CopyTo(ms);
        var data = HexUtil.ToHex(ms.ToArray());
        Assert.AreEqual(bigString, data);
    }

    [Test]
    public void TestStream_Write()
    {
        Stream stream = null;
        var bigBytes = new MemoryStream(1_000_000);
        for (int i = 0; i < bigBytes.Capacity; i++)
            bigBytes.WriteByte(0xa2);
        bigBytes.Position = 0;

        IntegrationTestSetup.Get().ExecuteRawTxn(
            (c) =>
            {
                Assert.IsFalse(c.IsAsync());

                c.ExecuteProc(
                    "fn_insert_test_record",
                    new NpgsqlParameter("@ID", 1),
                    new NpgsqlParameter("@String", "s1"),
                    new NpgsqlParameter("@Int", 10),
                    new NpgsqlParameter("@Blob", SqlDbType.VarBinary) { Value = bigBytes });

                stream = c.ExecuteQueryProcOne(
                    "fn_get_test_record_by_id",
                    (r) => r.GetStream("BlobField"),
                    new NpgsqlParameter("@ID", 1)).Value;

                Assert.AreEqual(1_000_000, bigBytes.Position);
                bigBytes.Position = 0;

                var ms = new MemoryStream();
                stream.CopyTo(ms);
                CollectionAssert.AreEqual(bigBytes.ToArray(), ms.ToArray());
            });

        bigBytes.Dispose();
    }

    [Test]
    public async Task TestStream_WriteAsync()
    {
        Stream stream = null;
        var bigBytes = new MemoryStream(1_000_000);
        for (int i = 0; i < bigBytes.Capacity; i++)
            bigBytes.WriteByte(0xa2);
        bigBytes.Position = 0;

        await IntegrationTestSetup.Get().ExecuteRawTxnAsync(
            async (c) =>
            {
                Assert.IsTrue(c.IsAsync());

                await c.ExecuteProcAsync(
                    "fn_insert_test_record",
                    new NpgsqlParameter("@ID", 1),
                    new NpgsqlParameter("@String", "s1"),
                    new NpgsqlParameter("@Int", 10),
                    new NpgsqlParameter("@Blob", SqlDbType.VarBinary) { Value = bigBytes });

                var res = await c.ExecuteQueryProcOneAsync(
                    "fn_get_test_record_by_id",
                    (r) => Task.FromResult(r.GetStream("BlobField")),
                    new NpgsqlParameter("@ID", 1));
                stream = res.Value;

                Assert.AreEqual(1_000_000, bigBytes.Position);
                bigBytes.Position = 0;

                var ms = new MemoryStream();
                stream.CopyTo(ms);
                CollectionAssert.AreEqual(bigBytes.ToArray(), ms.ToArray());
            });

        bigBytes.Dispose();
    }
    #endregion
    */
}
