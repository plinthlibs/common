CREATE OR REPLACE FUNCTION public.fn_insert_datetime (
IN i_id bigint,
IN i_dt1 timestamp with time zone = NULL,
IN i_dt2 timestamp with time zone = NULL
)
RETURNS INT
LANGUAGE plpgsql    
AS $$
DECLARE v_rc bigint;
BEGIN
    -- CALL public.usp_InsertDateTime(1, now() at time zone 'utc', now());
    
    INSERT INTO public.datetime2test (
        id,
        dt1,
        dt2
    )
    VALUES (
        i_id,
        i_dt1,
        i_dt2
    );
    
    GET DIAGNOSTICS v_rc = ROW_COUNT;
    RETURN v_rc;
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_update_datetime (
IN i_id bigint,
IN i_dt1 timestamp with time zone = NULL,
IN i_dt2 timestamp with time zone = NULL
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE v_rc bigint;
BEGIN
    -- CALL public.usp_UpdateDateTime(1, now() at time zone 'utc', now());
    UPDATE public.datetime2test SET
        dt1 = COALESCE(i_dt1, dt1),
        dt2 = COALESCE(i_dt2, dt2)
    WHERE id = i_id;
    
    GET DIAGNOSTICS v_rc = ROW_COUNT;
    RETURN v_rc;
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_get_datetime_by_id (
IN i_id bigint
)
RETURNS SETOF public.datetime2test
LANGUAGE plpgsql
AS $$
BEGIN
    -- SELECT public.usp_GetDateTimeByid(1);
    
    RETURN QUERY
    SELECT
        id,
        dt1,
        dt2
        FROM public.datetime2test
    WHERE id = i_id;
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_insert_test_record (
IN i_id bigint,
IN i_date timestamp default NULL,
IN i_string varchar(255) default NULL,
IN i_int int default 8866,
IN i_money decimal(5,3) default NULL,
IN i_blob bytea default NULL,
IN i_int_field_2 int default NULL
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE v_rc bigint;
BEGIN
    -- CALL public.usp_InserttestRecord(1, now(), 'test', 1, 0.99);
    
    INSERT INTO public.test (
        id,
        dt,
        string,
        intfield,
        moneyfield,
        blobfield,
        int_field_2
    )
    VALUES (
        i_id,
        i_date,
        i_string,
        i_int,
        i_money,
        i_blob,
        i_int_field_2
    );

    GET DIAGNOSTICS v_rc = ROW_COUNT;
    RETURN v_rc;
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_update_test_record (
IN i_id bigint,
IN i_date timestamp = NULL,
IN i_string varchar(255) = NULL,
IN i_int int = NULL,
IN i_money decimal(5,3) = NULL,
IN i_blob bytea = NULL
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE v_rc bigint;
BEGIN
    -- CALL public.usp_UpdatetestRecord(1, now());
    UPDATE public.test SET
        dt = COALESCE(i_date, dt),
        string = COALESCE(i_string, string),
        intfield = COALESCE(i_int, intfield),
        moneyfield = COALESCE(i_money, moneyfield),
        blobfield = COALESCE(i_blob, blobfield)
    WHERE id = i_id;
    
    GET DIAGNOSTICS v_rc = ROW_COUNT;
    RETURN v_rc;
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_get_test_record_by_id (
IN i_id bigint
)
RETURNS SETOF public.test
LANGUAGE plpgsql
AS $$
BEGIN
    -- SELECT public.usp_GettestRecordByid(1);
    
    RETURN QUERY
    SELECT
        id,
        dt,
        string,
        intfield,
        moneyfield,
        blobfield,
        int_field_2
        FROM public.test
    WHERE id = i_id;
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_get_test_record_by_int (
IN i_int int
)
RETURNS SETOF public.test
LANGUAGE plpgsql
AS $$
BEGIN
    -- SELECT public.usp_GettestRecordByInt(1);
    
    RETURN QUERY
    SELECT
        id,
        dt,
        string,
        intfield,
        moneyfield,
        blobfield,
        int_field_2
        FROM public.test
    WHERE intfield = i_int
    ORDER BY id ASC;
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_get_test_records_by_ints (
IN i_int1 int,
IN i_int2 int,
IN i_int3 int
)
RETURNS SETOF refcursor
LANGUAGE plpgsql
AS $$
DECLARE 
    ref1 refcursor;
    ref2 refcursor; 
    ref3 refcursor; 
BEGIN
    -- SELECT public.usp_GettestRecordsByInts(1, 2, 3);
    
    OPEN ref1 FOR
        SELECT
            id,
            dt,
            string,
            intfield,
            moneyfield,
            blobfield,
            int_field_2,
    		1 AS "Num1"
            FROM public.test
        WHERE intfield = i_int1
        ORDER BY id ASC;
    RETURN NEXT ref1;

    OPEN ref2 FOR
        SELECT
            id,
            dt,
            string,
            intfield,
            moneyfield,
            blobfield,
            int_field_2,
    		2 AS "Num2"
            FROM public.test
        WHERE intfield = i_int2
        ORDER BY id ASC;
    RETURN NEXT ref2;

    OPEN ref3 FOR
        SELECT
            id,
            dt,
            string,
            intfield,
            moneyfield,
            blobfield,
            int_field_2,
    		3 AS "Num3"
            FROM public.test
        WHERE intfield = i_int3
        ORDER BY id ASC;    
    RETURN NEXT ref3;

    RETURN;
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_echo_table_type_single (
IN i_table public.tabletypetest
)
RETURNS public.tabletypetest
LANGUAGE plpgsql
AS $$
BEGIN
    RETURN i_table;
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_echo_table_type (
IN i_table tabletypetest[]
)
RETURNS SETOF public.tabletypetest
LANGUAGE plpgsql
AS $$
BEGIN
    RETURN QUERY
    SELECT * FROM UNNEST(i_table);
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_echo_nvarchar_array (
IN i_array varchar[]
)
RETURNS TABLE(val VARCHAR)
LANGUAGE plpgsql
AS $$
BEGIN
    RETURN QUERY
    SELECT * FROM UNNEST(i_array);
END;
$$;


CREATE OR REPLACE FUNCTION public.fn_echo_int_array (
IN i_array int[]
)
RETURNS TABLE(val INT)
LANGUAGE plpgsql
AS $$
BEGIN
    RETURN QUERY
    SELECT * FROM UNNEST(i_array);
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_echo_uuid_array (
IN i_array UUID[]
)
RETURNS TABLE(val UUID)
LANGUAGE plpgsql
AS $$
BEGIN
    RETURN QUERY
    SELECT * FROM UNNEST(i_array);
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_echo_bit (
IN i_bit boolean
)
RETURNS TABLE (bitval boolean)
LANGUAGE plpgsql
AS $$
BEGIN
    -- SELECT public.usp_EchoBIT(1::bit);

    RETURN QUERY
    SELECT i_bit;
END;
$$;


CREATE OR REPLACE FUNCTION public.fn_insert_jsontest (
IN i_id bigint,
IN i_json JSON = NULL,
IN i_json2 JSONB = NULL
)
RETURNS INT
LANGUAGE plpgsql    
AS $$
DECLARE v_rc bigint;
BEGIN
    -- CALL public.usp_InsertDateTime(1, now() at time zone 'utc', now());
    
    INSERT INTO public.jsontest (
        id,
        json_val1,
        json_val2
    )
    VALUES (
        i_id,
        i_json,
        i_json2
    );
    
    GET DIAGNOSTICS v_rc = ROW_COUNT;
    RETURN v_rc;
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_get_jsontest_by_id (
IN i_id bigint
)
RETURNS SETOF public.jsontest
LANGUAGE plpgsql
AS $$
BEGIN
    -- SELECT public.usp_GetDateTimeByid(1);
    
    RETURN QUERY
    SELECT
        id,
        json_val1,
        json_val2
        FROM public.jsontest
    WHERE id = i_id;
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_update_jsontest (
IN i_id bigint,
IN i_json JSON = NULL,
IN i_json2 JSONB = NULL
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE v_rc bigint;
BEGIN
    -- CALL public.usp_UpdateDateTime(1, now() at time zone 'utc', now());
    UPDATE public.jsontest SET
        json_val1 = COALESCE(i_json, json_val1),
        json_val2 = COALESCE(i_json2, json_val2)
    WHERE id = i_id;
    
    GET DIAGNOSTICS v_rc = ROW_COUNT;
    RETURN v_rc;
END;
$$;
