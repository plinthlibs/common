CREATE TYPE public.tabletypetest AS (
    guid uuid,
    id bigint,
    dt timestamp,
    string varchar(255),
    intfield int,
    moneyfield decimal(5,3)
);

CREATE TABLE public.datetime2test (
    id bigint NOT NULL,
    dt1 timestamp NULL,
    dt2 timestamp NULL,
    CONSTRAINT PK_DateTime2 PRIMARY KEY (id)
);

CREATE TABLE public.test (
    id bigint,
    dt timestamp NULL,
    string varchar(255) NULL,
    intfield int NULL,
    moneyfield decimal(5,3) NULL,
    blobfield bytea NULL,
    int_field_2 int NULL,
    CONSTRAINT PK_Test PRIMARY KEY (id)
);

CREATE TABLE public.test2 (
	id bigint GENERATED ALWAYS AS IDENTITY,
	enumStr varchar(255) NULL,
	enumInt int NULL,
    CONSTRAINT PK_Test2 PRIMARY KEY (ID)
);

CREATE TABLE public.jsontest (
    id bigint,
    json_val1 JSON NULL,
    json_val2 JSONB NULL
);
