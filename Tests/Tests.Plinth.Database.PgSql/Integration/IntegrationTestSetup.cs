using Dapper;
using Microsoft.Extensions.Logging;
using Npgsql;
using Plinth.Database.PgSql;
using Integration.Utilities.Container;
using System.Reflection;
using Integration.Utilities.Database;

namespace Tests.Plinth.Database.PgSql.Integration;

internal static class IntegrationTestSetup
{
    private static readonly ILogger logger = StaticLogManager.GetLogger();

    private static readonly string DbHost = DockerUtil.ContainerHost;
    private static readonly string DbConnectionTemplate = @"Host=" + DbHost + @";Port=5433;Database={0};Username={1};Password={2}";

    private static SqlTransactionFactory? _txnF = null;
    private static string? _dbName = null;

    public const string DatabaseUser = "testuser";
    public const string DatabasePass = "TestPassword123$";

    private const string DatabaseSaUser = "postgres";
    private const string DatabaseSaPass = "postgres123!";

    public static string Dot = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!, "Integration");
    public static string Temp = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!;

    public static void SetUpTest()
    {
        _dbName = "pl" + Guid.NewGuid().ToString("N").ToLowerInvariant();

        var cstr = string.Format(DbConnectionTemplate, "postgres", DatabaseSaUser, DatabaseSaPass);
        _txnF = new SqlTransactionFactory(
                _dbName,
                string.Format(DbConnectionTemplate, _dbName, DatabaseUser, DatabasePass),
                30, 3, 200, true,
                configureDataSource: b => b.EnableDynamicJson());

        PostgresDatabase.CreateDB(cstr, _dbName, DatabaseUser, DatabasePass);

        using (logger.LogExecTiming("Seeding Database"))
        {
            _txnF.GetDefault().ExecuteRawTxn(
                (c) =>
                {
                    c.ExecuteRaw(File.ReadAllText(Path.Combine(Dot, "Schema/Tables.sql")));

                    var text = File.ReadAllText(Path.Combine(Dot, "Schema/Procedures.sql"));
                    c.ExecuteRaw(text);

                    // because we create the types, we need to reload.  Not normally needed
                    (c as global::Plinth.Database.PgSql.Impl.PlinthSqlConnection)?.ReloadTypes();
                });
        }
    }

    public static ISqlTransactionProvider Get() => _txnF!.GetDefault();

    public static ISqlTransactionProvider GetCustom(Action<NpgsqlDataSourceBuilder> custom)
    {
        return new SqlTransactionFactory(
                _dbName!,
                string.Format(DbConnectionTemplate, _dbName, DatabaseUser, DatabasePass),
                30, 3, 200, true, false, custom).GetDefault();
    }

    public static void Exec(Action<IRawSqlConnection> action)
    {
        _txnF!.GetDefault().ExecuteRawTxn(action);
    }

    public static void ExecWithout(Action<INoTxnRawSqlConnection> action)
    {
        _txnF!.GetDefault().ExecuteRawWithoutTxn(action);
    }

    public static async Task ExecAsync(Func<IRawSqlConnection, Task> action)
    {
        await _txnF!.GetDefault().ExecuteRawTxnAsync(action);
    }

    public static async Task ExecWithoutTxnAsync(Func<INoTxnRawSqlConnection, Task> action)
    {
        await _txnF!.GetDefault().ExecuteRawWithoutTxnAsync(action);
    }
    
    public static void CleanUpTest()
    {
        _txnF = null;
        GC.Collect();

        var cstr = string.Format(DbConnectionTemplate, "postgres", DatabaseSaUser, DatabaseSaPass);
        PostgresDatabase.DeleteDatabase(cstr, _dbName!);
    }

    public static async Task StartUpDatabase()
    {
        // allow mapping between 'some_value' and 'SomeValue'
        DefaultTypeMap.MatchNamesWithUnderscores = true;

        await DockerCompose.Up(5433);
    }

    public static async Task CleanUpDatabase()
    {
        await PostgresDatabase.WaitForDeletes();

        await DockerCompose.Down();
    }
}
