using Microsoft.Extensions.Configuration;
using Plinth.Database.PgSql;
using NUnit.Framework;

namespace Tests.Plinth.Database.PgSql;

[TestFixture]
class SqlTransactionFactoryTest
{
    [Test]
    public void TestUserPass()
    {
        var str = SqlTransactionFactory.BuildUserPassConnectionString(
            host: "my.db.com",
            database: "Database1",
            user: "my-user",
            password: "$ecuReP@$$word");

        Assert.That(
            str, Is.EqualTo("Host=my.db.com;Port=5432;Database=Database1;Username=my-user;Password=$ecuReP@$$word;Persist Security Info=True"));

        str = SqlTransactionFactory.BuildUserPassConnectionString(
            host: "my.db.com",
            database: "Database1",
            user: "my-user",
            password: "$ecuReP@$$word",
            port: 1433);

        Assert.That(
            str, Is.EqualTo("Host=my.db.com;Port=1433;Database=Database1;Username=my-user;Password=$ecuReP@$$word;Persist Security Info=True"));
    }

    [Test]
    public void TestDomain()
    {
        var str = SqlTransactionFactory.BuildDomainConnectionString(
            host: "my.db.com",
            database: "Database1");

        Assert.That(
            str, Is.EqualTo("Host=my.db.com;Port=5432;Database=Database1;Persist Security Info=True"));

        str = SqlTransactionFactory.BuildDomainConnectionString(
            host: "my.db.com",
            database: "Database1",
            port: 1433);

        Assert.That(
            str, Is.EqualTo("Host=my.db.com;Port=1433;Database=Database1;Persist Security Info=True"));
    }

    [Test]
    [TestCase(true)]
    [TestCase(false)]
    public void TestCreate(bool def)
    {
        var fac = new SqlTransactionFactory("db", "Host=localhost", 33, 2, 457, false, true);
        if (!def)
            fac.MapConnectionString("db2", "Host=1.2.3.4", 20, false);
        var s = def ? fac.GetDefaultFactorySettings() : fac.GetFactorySettings("db2");

        Assert.That(s.CommandTimeout, Is.EqualTo(TimeSpan.FromSeconds(def ? 33 : 20)));
        Assert.That(s.RetryCount, Is.EqualTo(2));
        Assert.That(s.RetryInterval, Is.EqualTo(TimeSpan.FromMilliseconds(457)));
        Assert.That(s.RetryFastFirst, Is.EqualTo(false));
        Assert.That(s.DisableTransientRetry, Is.EqualTo(def));
    }

    [Test]
    public void TestConfig()
    {
        var json = $"{Guid.NewGuid()}.json";
        File.WriteAllText(Path.Combine(Path.GetTempPath(),json), @"
{
    ""PlinthPgSqlSettings"": {
      ""SqlCommandTimeout"": ""00:00:33"",
      ""SqlRetryCount"": 2,
      ""SqlRetryInterval"": ""00:00:00.457"",
      ""SqlRetryFastFirst"": false,
      ""DisableTransientRetry"": true
    }
}
");
        var cfg = new ConfigurationBuilder().SetBasePath(Path.GetTempPath()).AddJsonFile(json).Build();

        var fac = new SqlTransactionFactory(cfg, "db", "Host=localhost;Port=1010;Database=MyDb");
        fac.MapConnectionString("db2", "Host=localhost;Port=1010;Database=MyDb2", 20, false);

        var s = fac.GetDefaultFactorySettings();

        Assert.That(s.CommandTimeout, Is.EqualTo(TimeSpan.FromSeconds(33)));
        Assert.That(s.RetryCount, Is.EqualTo(2));
        Assert.That(s.RetryInterval, Is.EqualTo(TimeSpan.FromMilliseconds(457)));
        Assert.That(s.RetryFastFirst, Is.EqualTo(false));
        Assert.That(s.DisableTransientRetry, Is.EqualTo(true));

        s = fac.GetFactorySettings("db2");

        Assert.That(s.CommandTimeout, Is.EqualTo(TimeSpan.FromSeconds(20)));
        Assert.That(s.RetryCount, Is.EqualTo(2));
        Assert.That(s.RetryInterval, Is.EqualTo(TimeSpan.FromMilliseconds(457)));
        Assert.That(s.RetryFastFirst, Is.EqualTo(false));
        Assert.That(s.DisableTransientRetry, Is.EqualTo(false));

        File.Delete(Path.Combine(Path.GetTempPath(),json));
    }

    [Test]
    public void TestConfig_Defaults()
    {
        var json = $"{Guid.NewGuid()}.json";
        File.WriteAllText(Path.Combine(Path.GetTempPath(),json), @" { } ");

        var cfg = new ConfigurationBuilder().SetBasePath(Path.GetTempPath()).AddJsonFile(json).Build();

        var fac = new SqlTransactionFactory(cfg, "db", "Host=localhost;Port=1010;Database=MyDb");
        fac.MapConnectionString("db2", "Host=localhost;Port=1010;Database=MyDb2", 20, false);

        var s = fac.GetDefaultFactorySettings();

        Assert.That(s.CommandTimeout, Is.EqualTo(TimeSpan.FromSeconds(50)));
        Assert.That(s.RetryCount, Is.EqualTo(3));
        Assert.That(s.RetryInterval, Is.EqualTo(TimeSpan.FromMilliseconds(200)));
        Assert.That(s.RetryFastFirst, Is.EqualTo(true));
        Assert.That(s.DisableTransientRetry, Is.EqualTo(false));

        s = fac.GetFactorySettings("db2");

        Assert.That(s.CommandTimeout, Is.EqualTo(TimeSpan.FromSeconds(20)));
        Assert.That(s.RetryCount, Is.EqualTo(3));
        Assert.That(s.RetryInterval, Is.EqualTo(TimeSpan.FromMilliseconds(200)));
        Assert.That(s.RetryFastFirst, Is.EqualTo(true));
        Assert.That(s.DisableTransientRetry, Is.EqualTo(false));

        File.Delete(Path.Combine(Path.GetTempPath(),json));
    }

    [Test]
    public void TestConfig_Millis()
    {
        var json = $"{Guid.NewGuid()}.json";
        File.WriteAllText(Path.Combine(Path.GetTempPath(),json), @"
{
    ""PlinthPgSqlSettings"": {
      ""SqlCommandTimeout"": 25,
      ""SqlRetryCount"": 3,
      ""SqlRetryInterval"": 910,
      ""SqlRetryFastFirst"": true,
      ""DisableTransientRetry"": false
    }
}
");
        var cfg = new ConfigurationBuilder().SetBasePath(Path.GetTempPath()).AddJsonFile(json).Build();

        var fac = new SqlTransactionFactory(cfg, "db", "Host=localhost;Port=1010;Database=MyDb");
        fac.MapConnectionString("db2", "Host=localhost;Port=1010;Database=MyDb2", 20, false);

        var s = fac.GetDefaultFactorySettings();

        Assert.That(s.CommandTimeout, Is.EqualTo(TimeSpan.FromSeconds(25)));
        Assert.That(s.RetryCount, Is.EqualTo(3));
        Assert.That(s.RetryInterval, Is.EqualTo(TimeSpan.FromMilliseconds(910)));
        Assert.That(s.RetryFastFirst, Is.EqualTo(true));
        Assert.That(s.DisableTransientRetry, Is.EqualTo(false));

        s = fac.GetFactorySettings("db2");

        Assert.That(s.CommandTimeout, Is.EqualTo(TimeSpan.FromSeconds(20)));
        Assert.That(s.RetryCount, Is.EqualTo(3));
        Assert.That(s.RetryInterval, Is.EqualTo(TimeSpan.FromMilliseconds(910)));
        Assert.That(s.RetryFastFirst, Is.EqualTo(true));
        Assert.That(s.DisableTransientRetry, Is.EqualTo(false));

        File.Delete(Path.Combine(Path.GetTempPath(),json));
    }
}
