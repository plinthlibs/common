using Npgsql;
using Plinth.Database.PgSql;
using Plinth.Database.PgSql.Impl;
using NUnit.Framework;
using System.Data;
using NpgsqlTypes;

namespace Tests.Plinth.Database.PgSql.Impl;

[TestFixture]
public class SqlParameterProcessorTest
{
    [Test]
    public void Test_null()
    {
        string? s = null;
        var p = new NpgsqlParameter("@Null", s);
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.Value, Is.EqualTo(DBNull.Value));

        p = new NpgsqlParameter("@Null", DBNull.Value);
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.Value, Is.EqualTo(DBNull.Value));
    }

    [Test]
    public void Test_DateTime_LocalToUtc()
    {
        var dt = new DateTime(2015, 02, 15, 14, 05, 01, DateTimeKind.Local);
        var p = new NpgsqlParameter("@DateTime", dt);
        SqlParameterProcessor.ProcessParam(p);

        Assert.That((DateTime)p.Value!, Is.EqualTo(new DateTime(2015, 02, 15, 14, 05, 01, DateTimeKind.Local).ToUniversalTime()));
        Assert.That(p.DbType, Is.EqualTo(DbType.DateTime));
        Assert.That(p.NpgsqlDbType, Is.EqualTo(NpgsqlDbType.TimestampTz));
    }

    [Test]
    public void Test_DateTime_UtcToUtc()
    {
        var dt = new DateTime(2015, 02, 15, 14, 05, 01, DateTimeKind.Utc);
        var p = new NpgsqlParameter("@DateTime", dt);
        SqlParameterProcessor.ProcessParam(p);

        Assert.That((DateTime)p.Value!, Is.EqualTo(new DateTime(2015, 02, 15, 14, 05, 01, DateTimeKind.Utc)));
        //Assert.AreEqual(DbType.DateTime, p.DbType); // npgsql 8.0 stopped mapping DataTime
        Assert.That(p.NpgsqlDbType, Is.EqualTo(NpgsqlDbType.TimestampTz));
    }

    [Test]
    public void Test_DateTime_Utc_NoUtc()
    {
        var dt = new DateTime(2015, 02, 15, 14, 05, 01, DateTimeKind.Utc);
        var p = new NpgsqlParameter(SqlConstants.NoUtcDateTimeHeader + "@DateTime", dt);
        SqlParameterProcessor.ProcessParam(p);

        Assert.That((DateTime)p.Value!, Is.EqualTo(new DateTime(2015, 02, 15, 14, 05, 01, DateTimeKind.Utc)));
        //Assert.AreEqual(DbType.DateTime, p.DbType); // npgsql 8.0 stopped mapping DataTime
        //Assert.AreEqual(NpgsqlDbType.TimestampTz, p.NpgsqlDbType); // npgsql 8.0 stopped mapping DataTime
    }

    [Test]
    public void Test_DateTime_Local_NoUtc()
    {
        var dt = new DateTime(2015, 02, 15, 14, 05, 01, DateTimeKind.Local);
        var p = new NpgsqlParameter(SqlConstants.NoUtcDateTimeHeader + "@DateTime", dt);
        SqlParameterProcessor.ProcessParam(p);

        Assert.That((DateTime)p.Value!, Is.EqualTo(new DateTime(2015, 02, 15, 14, 05, 01, DateTimeKind.Local)));
        //Assert.AreEqual(DbType.DateTime2, p.DbType); // npgsql 8.0 stopped mapping DataTime
        //Assert.AreEqual(NpgsqlDbType.Timestamp, p.NpgsqlDbType);// npgsql 8.0 stopped mapping DataTime
    }

    private enum TestEnum
    {
        Value1,
        Value2
    }

    [Test]
    public void Test_Enum()
    {
        var p = new NpgsqlParameter("@Enum1", TestEnum.Value1);
        SqlParameterProcessor.ProcessParam(p);

        Assert.That(p.Value, Is.EqualTo("Value1"));

        p = new NpgsqlParameter("@Enum2", TestEnum.Value2.ToString());
        SqlParameterProcessor.ProcessParam(p);

        Assert.That(p.Value, Is.EqualTo("Value2"));
    }

    [Test]
    public void Test_Trim()
    {
        var p = new NpgsqlParameter("@String1", "Test");
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.Value, Is.EqualTo("Test"));

        p = new NpgsqlParameter("@String1", "Test   ");
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.Value, Is.EqualTo("Test"));

        p = new NpgsqlParameter("@String1", "   Test");
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.Value, Is.EqualTo("Test"));

        p = new NpgsqlParameter("@String1", " Test ");
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.Value, Is.EqualTo("Test"));

        p = new NpgsqlParameter("@String1", "\tTest \t");
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.Value, Is.EqualTo("Test"));
    }

    [Test]
    public void Test_NoTrim()
    {
        var p = new NpgsqlParameter(SqlConstants.DoNotTrimHeader + "@String1", "Test");
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.ParameterName, Is.EqualTo("@String1"));
        Assert.That(p.Value, Is.EqualTo("Test"));

        p = new NpgsqlParameter(SqlConstants.DoNotTrimHeader + "@String1", "  Test  ");
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.ParameterName, Is.EqualTo("@String1"));
        Assert.That(p.Value, Is.EqualTo("  Test  "));

        p = new NpgsqlParameter(SqlConstants.DoNotTrimHeader + "@String1", null);
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.ParameterName, Is.EqualTo("@String1"));
        Assert.That(p.Value, Is.EqualTo(DBNull.Value));
    }

    [Test]
    public void Test_Null()
    {
        var p = new NpgsqlParameter("@String1", null);
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.ParameterName, Is.EqualTo("@String1"));
        Assert.That(p.Value, Is.EqualTo(DBNull.Value));

        p = new NpgsqlParameter("@String1", DBNull.Value);
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.ParameterName, Is.EqualTo("@String1"));
        Assert.That(p.Value, Is.EqualTo(DBNull.Value));
    }
}
