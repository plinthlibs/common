using Plinth.Database.PgSql.Impl;
using NSubstitute;
using NUnit.Framework;
using System.Data;
using System.Drawing;

namespace Tests.Plinth.Database.PgSql.Impl;

[TestFixture]
public class ResultSetTest
{
    private enum TestEnum
    {
        Default,
        Value1,
        Value2,
        Value3
    }

    private static readonly ResultSet _rs;

    static ResultSetTest()
    {
        object[] values = [
            "abc", "123", DBNull.Value,              // string
            55, 78, 123,                     // int
            DBNull.Value, 102, DBNull.Value,                 // int?
            10284L, 999494L, 1929045859334L, // long
            DBNull.Value, 9485395L, DBNull.Value,            // long?
            12.74m, 485.23435m, 11.2233m,    // decimal
            3.1415m, DBNull.Value, DBNull.Value,             // decimal?
            true, false, true,               // bool
            DBNull.Value, true, DBNull.Value,                // bool?
            new DateTime(2014,10,3,15,20,55,DateTimeKind.Unspecified), new DateTime(2013,9,2,14,19,54,DateTimeKind.Unspecified), new DateTime(2015,11,4,16,21,56,DateTimeKind.Unspecified), // DateTime
            new DateTime(2011,7,4,4,17,24,DateTimeKind.Unspecified), DBNull.Value, DBNull.Value, // DateTime?
            new Guid("2bf20ca3-a086-4c9c-83e2-753ba572147d"), new Guid("5a4d7911-e864-4d0d-8689-fde8a59d628d"), new Guid("95bc8cf8-9c6b-4572-8678-9dcbd787741e"), // Guid
            DBNull.Value, new Guid("dc4b6fc3-d26c-4816-8f60-eba97f70b500"), DBNull.Value, // Guid?
            TestEnum.Value1.ToString(), TestEnum.Value2.ToString(), TestEnum.Value3.ToString(), // enum
            DBNull.Value, TestEnum.Value3.ToString(), DBNull.Value,     // enum?
            new byte[] {0x1, 0xff, 0x45}, DBNull.Value, new byte[] { 0x12, 0x34 }, // byte[]
            (byte)0x2, (byte)0xde, (byte)0x64,                 // byte
            DBNull.Value, (byte)0xcd, DBNull.Value,                // byte?
            (short)123, (short)9986, (short)32000,                // short
            DBNull.Value, (short)1122, DBNull.Value,                // short?
            TimeSpan.FromMilliseconds(1123), TimeSpan.FromDays(2), TimeSpan.FromMinutes(10), // timespan
            DBNull.Value, TimeSpan.FromDays(10), DBNull.Value, // timespan?
            0.123d, 3.1415d, 9.992d,         // double
            DBNull.Value, 1.234d, DBNull.Value,             // double?
        ];

        var ords = new Dictionary<string, int>();
        int i = 0;
        ords["StringCol1"] = i++;
        ords["StringCol2"] = i++;
        ords["StringCol3"] = i++;
        ords["IntCol1"] = i++;
        ords["IntCol2"] = i++;
        ords["IntCol3"] = i++;
        ords["NullIntCol1"] = i++;
        ords["NullIntCol2"] = i++;
        ords["NullIntCol3"] = i++;
        ords["LongCol1"] = i++;
        ords["LongCol2"] = i++;
        ords["LongCol3"] = i++;
        ords["NullLongCol1"] = i++;
        ords["NullLongCol2"] = i++;
        ords["NullLongCol3"] = i++;
        ords["DecimalCol1"] = i++;
        ords["DecimalCol2"] = i++;
        ords["DecimalCol3"] = i++;
        ords["NullDecimalCol1"] = i++;
        ords["NullDecimalCol2"] = i++;
        ords["NullDecimalCol3"] = i++;
        ords["BoolCol1"] = i++;
        ords["BoolCol2"] = i++;
        ords["BoolCol3"] = i++;
        ords["NullBoolCol1"] = i++;
        ords["NullBoolCol2"] = i++;
        ords["NullBoolCol3"] = i++;
        ords["DateTimeCol1"] = i++;
        ords["DateTimeCol2"] = i++;
        ords["DateTimeCol3"] = i++;
        ords["NullDateTimeCol1"] = i++;
        ords["NullDateTimeCol2"] = i++;
        ords["NullDateTimeCol3"] = i++;
        ords["GuidCol1"] = i++;
        ords["GuidCol2"] = i++;
        ords["GuidCol3"] = i++;
        ords["NullGuidCol1"] = i++;
        ords["NullGuidCol2"] = i++;
        ords["NullGuidCol3"] = i++;
        ords["EnumCol1"] = i++;
        ords["EnumCol2"] = i++;
        ords["EnumCol3"] = i++;
        ords["NullEnumCol1"] = i++;
        ords["NullEnumCol2"] = i++;
        ords["NullEnumCol3"] = i++;
        ords["BytesCol1"] = i++;
        ords["BytesCol2"] = i++;
        ords["BytesCol3"] = i++;
        ords["ByteCol1"] = i++;
        ords["ByteCol2"] = i++;
        ords["ByteCol3"] = i++;
        ords["NullByteCol1"] = i++;
        ords["NullByteCol2"] = i++;
        ords["NullByteCol3"] = i++;
        ords["ShortCol1"] = i++;
        ords["ShortCol2"] = i++;
        ords["ShortCol3"] = i++;
        ords["NullShortCol1"] = i++;
        ords["NullShortCol2"] = i++;
        ords["NullShortCol3"] = i++;
        ords["TimeSpanCol1"] = i++;
        ords["TimeSpanCol2"] = i++;
        ords["TimeSpanCol3"] = i++;
        ords["NullTimeSpanCol1"] = i++;
        ords["NullTimeSpanCol2"] = i++;
        ords["NullTimeSpanCol3"] = i++;
        ords["DoubleCol1"] = i++;
        ords["DoubleCol2"] = i++;
        ords["DoubleCol3"] = i++;
        ords["NullDoubleCol1"] = i++;
        ords["NullDoubleCol2"] = i++;
        ords["NullDoubleCol3"] = i++;

        var colByPos = ords.ToDictionary(kv => kv.Value, kv => kv.Key);

        var mock = Substitute.For<IDataReader>();
        mock.FieldCount.Returns(i);
        mock.GetName(Arg.Any<int>()).Returns(f => colByPos[f.Arg<int>()]);
        mock.GetString(Arg.Any<int>()).Returns(f => (string)values[f.Arg<int>()]);
        mock.GetInt32(Arg.Any<int>()).Returns(f => (int)values[f.Arg<int>()]);
        mock.GetInt64(Arg.Any<int>()).Returns(f => (long)values[f.Arg<int>()]);
        mock.GetDecimal(Arg.Any<int>()).Returns(f => (decimal)values[f.Arg<int>()]);
        mock.GetBoolean(Arg.Any<int>()).Returns(f => (bool)values[f.Arg<int>()]);
        mock.GetDateTime(Arg.Any<int>()).Returns(f => (DateTime)values[f.Arg<int>()]);
        mock.GetGuid(Arg.Any<int>()).Returns(f => (Guid)values[f.Arg<int>()]);
        mock.GetByte(Arg.Any<int>()).Returns(f => (byte)values[f.Arg<int>()]);
        mock.GetInt16(Arg.Any<int>()).Returns(f => (short)values[f.Arg<int>()]);
        mock.GetDouble(Arg.Any<int>()).Returns(f => (double)values[f.Arg<int>()]);
        mock.GetValue(Arg.Any<int>()).Returns(f => values[f.Arg<int>()]);
        mock.IsDBNull(Arg.Any<int>()).Returns(f => values[f.Arg<int>()] == null || values[f.Arg<int>()] == DBNull.Value);
        mock.GetOrdinal(Arg.Any<string>()).Returns(f => ords[f.Arg<string>()]);
        _rs = new ResultSet(mock);
    }

    [Test]
    public void Test_GetString()
    {
        Assert.That(_rs.GetString("StringCol1"), Is.EqualTo("abc"));
        Assert.That(_rs.GetString("StringCol2"), Is.EqualTo("123"));
        Assert.That(_rs.GetString("StringCol3"), Is.Null);
    }

    [Test]
    public void Test_GetInt()
    {
        Assert.That(_rs.GetInt("IntCol1"), Is.EqualTo(55));
        Assert.That(_rs.GetInt("IntCol2"), Is.EqualTo(78));
        Assert.That(_rs.GetInt("IntCol3"), Is.EqualTo(123));
    }

    [Test]
    public void Test_GetIntNull()
    {
        Assert.That(_rs.GetIntNull("NullIntCol1"), Is.Null);
        Assert.That(_rs.GetIntNull("NullIntCol2"), Is.EqualTo(102));
        Assert.That(_rs.GetIntNull("NullIntCol3"), Is.Null);
    }

    [Test]
    public void Test_GetLong()
    {
        Assert.That(_rs.GetLong("LongCol1"), Is.EqualTo(10284L));
        Assert.That(_rs.GetLong("LongCol2"), Is.EqualTo(999494L));
        Assert.That(_rs.GetLong("LongCol3"), Is.EqualTo(1929045859334L));
    }

    [Test]
    public void Test_GetLongNull()
    {
        Assert.That(_rs.GetLongNull("NullLongCol1"), Is.Null);
        Assert.That(_rs.GetLongNull("NullLongCol2"), Is.EqualTo(9485395L));
        Assert.That(_rs.GetLongNull("NullLongCol3"), Is.Null);
    }

    [Test]
    public void Test_GetDecimal()
    {
        Assert.That(_rs.GetDecimal("DecimalCol1"), Is.EqualTo(12.74m));
        Assert.That(_rs.GetDecimal("DecimalCol2"), Is.EqualTo(485.23435m));
        Assert.That(_rs.GetDecimal("DecimalCol3"), Is.EqualTo(11.2233m));
    }

    [Test]
    public void Test_GetDecimalNull()
    {
        Assert.That(_rs.GetDecimalNull("NullDecimalCol1"), Is.EqualTo(3.1415m));
        Assert.That(_rs.GetDecimalNull("NullDecimalCol2"), Is.Null);
        Assert.That(_rs.GetDecimalNull("NullDecimalCol3"), Is.Null);
    }

    [Test]
    public void Test_GetBool()
    {
        Assert.That(_rs.GetBool("BoolCol1"), Is.EqualTo(true));
        Assert.That(_rs.GetBool("BoolCol2"), Is.EqualTo(false));
        Assert.That(_rs.GetBool("BoolCol3"), Is.EqualTo(true));
    }

    [Test]
    public void Test_GetBoolNull()
    {
        Assert.That(_rs.GetBoolNull("NullBoolCol1"), Is.Null);
        Assert.That(_rs.GetBoolNull("NullBoolCol2"), Is.EqualTo(true));
        Assert.That(_rs.GetBoolNull("NullBoolCol3"), Is.Null);
    }

    [Test]
    public void Test_GetDateTime()
    {
        Assert.That(_rs.GetDateTime("DateTimeCol1"), Is.EqualTo(new DateTime(2014, 10, 3, 15, 20, 55, DateTimeKind.Utc)));
        Assert.That(_rs.GetDateTime("DateTimeCol2"), Is.EqualTo(new DateTime(2013, 9, 2, 14, 19, 54, DateTimeKind.Utc)));
        Assert.That(_rs.GetDateTime("DateTimeCol3"), Is.EqualTo(new DateTime(2015, 11, 4, 16, 21, 56, DateTimeKind.Utc)));
    }

    [Test]
    public void Test_GetDateTimeNull()
    {
        Assert.That(_rs.GetDateTimeNull("NullDateTimeCol1"), Is.EqualTo(new DateTime(2011, 7, 4, 4, 17, 24, DateTimeKind.Utc)));
        Assert.That(_rs.GetDateTimeNull("NullDateTimeCol2"), Is.Null);
        Assert.That(_rs.GetDateTimeNull("NullDateTimeCol3"), Is.Null);
    }

    [Test]
    public void Test_GetGuid()
    {
        Assert.That(_rs.GetGuid("GuidCol1"), Is.EqualTo(new Guid("2bf20ca3-a086-4c9c-83e2-753ba572147d")));
        Assert.That(_rs.GetGuid("GuidCol2"), Is.EqualTo(new Guid("5a4d7911-e864-4d0d-8689-fde8a59d628d")));
        Assert.That(_rs.GetGuid("GuidCol3"), Is.EqualTo(new Guid("95bc8cf8-9c6b-4572-8678-9dcbd787741e")));
    }

    [Test]
    public void Test_GetGuidNull()
    {
        Assert.That(_rs.GetGuidNull("NullGuidCol1"), Is.Null);
        Assert.That(_rs.GetGuidNull("NullGuidCol2"), Is.EqualTo(new Guid("dc4b6fc3-d26c-4816-8f60-eba97f70b500")));
        Assert.That(_rs.GetGuidNull("NullGuidCol3"), Is.Null);
    }

    [Test]
    public void Test_GetEnum()
    {
        Assert.That(_rs.GetEnum<TestEnum>("EnumCol1"), Is.EqualTo(TestEnum.Value1));
        Assert.That(_rs.GetEnum<TestEnum>("EnumCol2"), Is.EqualTo(TestEnum.Value2));
        Assert.That(_rs.GetEnum<TestEnum>("EnumCol3"), Is.EqualTo(TestEnum.Value3));
    }

    [Test]
    public void Test_GetEnumDefault()
    {
        Assert.That(_rs.GetEnumDefault("NullEnumCol1", TestEnum.Default), Is.EqualTo(TestEnum.Default));
        Assert.That(_rs.GetEnumDefault("NullEnumCol2", TestEnum.Default), Is.EqualTo(TestEnum.Value3));
        Assert.That(_rs.GetEnumDefault("NullEnumCol3", TestEnum.Default), Is.EqualTo(TestEnum.Default));
    }

    [Test]
    public void Test_GetEnumNull()
    {
        Assert.That(_rs.GetEnumNull<TestEnum>("NullEnumCol1"), Is.Null);
        Assert.That(_rs.GetEnumNull<TestEnum>("NullEnumCol2"), Is.EqualTo(TestEnum.Value3));
        Assert.That(_rs.GetEnumNull<TestEnum>("NullEnumCol3"), Is.Null);
    }

    [Test]
    public void Test_GetBytes()
    {
        Assert.That(_rs.GetBytes("BytesCol1"), Is.EqualTo(new byte[] { 0x1, 0xff, 0x45 }).AsCollection);
        Assert.That(_rs.GetBytes("BytesCol2"), Is.Null);
        Assert.That(_rs.GetBytes("BytesCol3"), Is.EqualTo(new byte[] { 0x12, 0x34 }).AsCollection);
    }

    [Test]
    public void Test_GetByte()
    {
        Assert.That(_rs.GetByte("ByteCol1"), Is.EqualTo(0x2));
        Assert.That(_rs.GetByte("ByteCol2"), Is.EqualTo(0xde));
        Assert.That(_rs.GetByte("ByteCol3"), Is.EqualTo(0x64));
    }

    [Test]
    public void Test_GetByteNull()
    {
        Assert.That(_rs.GetByteNull("NullByteCol1"), Is.Null);
        Assert.That(_rs.GetByteNull("NullByteCol2"), Is.EqualTo(0xcd));
        Assert.That(_rs.GetByteNull("NullByteCol3"), Is.Null);
    }

    [Test]
    public void Test_GetShort()
    {
        Assert.That(_rs.GetShort("ShortCol1"), Is.EqualTo(123));
        Assert.That(_rs.GetShort("ShortCol2"), Is.EqualTo(9986));
        Assert.That(_rs.GetShort("ShortCol3"), Is.EqualTo(32000));
    }

    [Test]
    public void Test_GetShortNull()
    {
        Assert.That(_rs.GetShortNull("NullShortCol1"), Is.Null);
        Assert.That(_rs.GetShortNull("NullShortCol2"), Is.EqualTo(1122));
        Assert.That(_rs.GetShortNull("NullShortCol3"), Is.Null);
    }

    [Test]
    public void Test_GetTimeSpan()
    {
        Assert.That(_rs.GetTimeSpan("TimeSpanCol1"), Is.EqualTo(TimeSpan.FromMilliseconds(1123)));
        Assert.That(_rs.GetTimeSpan("TimeSpanCol2"), Is.EqualTo(TimeSpan.FromDays(2)));
        Assert.That(_rs.GetTimeSpan("TimeSpanCol3"), Is.EqualTo(TimeSpan.FromMinutes(10)));
    }

    [Test]
    public void Test_GetTimeSpanNull()
    {
        Assert.That(_rs.GetTimeSpanNull("NullTimeSpanCol1"), Is.Null);
        Assert.That(_rs.GetTimeSpanNull("NullTimeSpanCol2"), Is.EqualTo(TimeSpan.FromDays(10)));
        Assert.That(_rs.GetTimeSpanNull("NullTimeSpanCol3"), Is.Null);
    }

    [Test]
    public void Test_GetDouble()
    {
        Assert.That(_rs.GetDouble("DoubleCol1"), Is.EqualTo(0.123d).Within(0.0000001));
        Assert.That(_rs.GetDouble("DoubleCol2"), Is.EqualTo(3.1415d).Within(0.0000001));
        Assert.That(_rs.GetDouble("DoubleCol3"), Is.EqualTo(9.992d).Within(0.0000001));
    }

    [Test]
    public void Test_GetDoubleNull()
    {
        Assert.That(_rs.GetDoubleNull("NullDoubleCol1"), Is.Null);
        Assert.That(_rs.GetDoubleNull("NullDoubleCol2") ?? 0.0, Is.EqualTo(1.234d).Within(0.0000001));
        Assert.That(_rs.GetDoubleNull("NullDoubleCol3"), Is.Null);
    }

    [Test]
    public void Test_IsNull()
    {
        Assert.That(_rs.IsNull("NullIntCol1"));
        Assert.That(_rs.IsNull("NullIntCol2"), Is.False);
        Assert.That(_rs.IsNull("NullIntCol3"));
    }

    [Test]
    public void Test_Next()
    {
        var mock = Substitute.For<IDataReader>();

        var readResults = new Queue<bool>([true, true, false]);
        mock.Read().Returns(_ => readResults.Dequeue());

        var rs = new ResultSet(mock);
        Assert.That(rs.NextResult(), Is.Not.Null);
        Assert.That(rs.NextResult(), Is.Not.Null);

        Assert.That(rs.NextResult(), Is.Null);

        rs.Dispose();
    }

    [Test]
    public void Test_Dispose()
    {
        var mock = Substitute.For<IDataReader>();
        var rs = new ResultSet(mock);

        rs.Dispose();

        mock.Received(1).Dispose();
    }

    [Test]
    public void Test_Enumerator()
    {
        var mock = Substitute.For<IDataReader>();

        var readResults = new Queue<bool>([true, true, false]);
        var getInt32Results = new Queue<int>([5, 10]);

        mock.Read().Returns(_ => readResults.Dequeue());
        mock.GetInt32(Arg.Any<int>()).Returns(_ => getInt32Results.Dequeue());

        var rs = new ResultSet(mock);

        int count = 0;
        foreach (var r in rs)
        {
            count++;
            if (count == 1)
                Assert.That(r.GetInt("X"), Is.EqualTo(5));
            else if (count == 2)
                Assert.That(r.GetInt("X"), Is.EqualTo(10));
        }

        Assert.That(count, Is.EqualTo(2));

        rs.Dispose();
    }

    [Test]
    public void Test_GetColumns()
    {
        var cols = _rs.GetColumns();
        Assert.That(cols, Has.Count.EqualTo(72));
        Assert.That(cols.First(), Is.EqualTo("StringCol1"));
        Assert.That(cols.Last(), Is.EqualTo("NullDoubleCol3"));
    }
}
