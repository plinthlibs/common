using Npgsql;
using Plinth.Database.PgSql.Impl;
using NUnit.Framework;

namespace Tests.Plinth.Database.PgSql.Impl;

[TestFixture]
public class StoredProcLoggerTest
{
    [Test]
    public void Test_ExecToString()
    {
        int? nullInt = null;
        Assert.That(StoredProcLogger.ExecToString("fn_do_this", [new NpgsqlParameter("@i_param1", 5),
                                                        new NpgsqlParameter("@i_param2", "abc"),
                                                        new NpgsqlParameter("@i_param3", nullInt),
                                                        new NpgsqlParameter("@i_param4", null)]), Is.EqualTo("SELECT * FROM fn_do_this(i_param1 := 5, i_param2 := 'abc', i_param3 := NULL, i_param4 := NULL)"));
    }

    [Test]
    public void Test_ExecToString_Nulls()
    {
        int? nullInt = null;
        Assert.That(StoredProcLogger.ExecToString("fn_do_this", [new NpgsqlParameter("@i_param1", 5),
                                                        null,
                                                        new NpgsqlParameter("@i_param3", nullInt),
                                                        new NpgsqlParameter("@i_param4", null)]), Is.EqualTo("SELECT * FROM fn_do_this(i_param1 := 5, i_param3 := NULL, i_param4 := NULL)"));
    }

    [Test]
    public void Test_ExecToString_Guid()
    {
        Assert.That(StoredProcLogger.ExecToString("fn_do_this", [new NpgsqlParameter("@i_param1", new Guid("f75da73e-fb3f-4867-b27c-183e241a0e1e"))]), Is.EqualTo("SELECT * FROM fn_do_this(i_param1 := 'f75da73e-fb3f-4867-b27c-183e241a0e1e')"));
    }

    [Test]
    public void Test_RawExecToString()
    {
        int? nullInt = null;
        Assert.That(StoredProcLogger.RawExecToString("UPDATE table SET P1=@Param1, P2=@Param2, P3=@Param3, P4=@Param4",
                [
                    new NpgsqlParameter("@Param1", 5),
                    new NpgsqlParameter("@Param2", "abc"),
                    new NpgsqlParameter("@Param3", nullInt),
                    new NpgsqlParameter("@Param4", null)]), Is.EqualTo("UPDATE table SET P1=@Param1, P2=@Param2, P3=@Param3, P4=@Param4 ==> VALUES (Param1 := 5, Param2 := 'abc', Param3 := NULL, Param4 := NULL)"));
    }

    [Test]
    public void Test_RawExecToString_Nulls()
    {
        int? nullInt = null;
        Assert.That(StoredProcLogger.RawExecToString("UPDATE table SET P1=@Param1, P3=@Param3, P4=@Param4",
                [
                    new NpgsqlParameter("@Param1", 5),
                    null,
                    new NpgsqlParameter("@Param3", nullInt),
                    new NpgsqlParameter("@Param4", null)]), Is.EqualTo("UPDATE table SET P1=@Param1, P3=@Param3, P4=@Param4 ==> VALUES (Param1 := 5, Param3 := NULL, Param4 := NULL)"));
    }

    [Test]
    public void Test_RawExecToString_Guid()
    {
        Assert.That(StoredProcLogger.RawExecToString(
                "SELECT * FROM fn_do_this(\"i_param1\" := $1)", [new NpgsqlParameter("@i_param1", new Guid("f75da73e-fb3f-4867-b27c-183e241a0e1e"))]), Is.EqualTo("SELECT * FROM fn_do_this(\"i_param1\" := $1) ==> VALUES (i_param1 := 'f75da73e-fb3f-4867-b27c-183e241a0e1e')"));
    }

    [Test]
    public void Test_RawExecToString_NoParamName()
    {
        Assert.That(StoredProcLogger.RawExecToString(
                "SELECT * FROM fn_do_this(\"i_param1\" := $1)", [new NpgsqlParameter() { Value = new Guid("f75da73e-fb3f-4867-b27c-183e241a0e1e") }]), Is.EqualTo("SELECT * FROM fn_do_this(\"i_param1\" := $1) ==> VALUES ($1 := 'f75da73e-fb3f-4867-b27c-183e241a0e1e')"));
    }

    [Test]
    public void Test_RawExecToString_NoValue()
    {
        Assert.That(StoredProcLogger.RawExecToString("SELECT * FROM TABLE", []), Is.EqualTo("SELECT * FROM TABLE"));
    }
}
