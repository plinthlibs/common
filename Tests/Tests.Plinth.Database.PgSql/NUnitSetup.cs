using Plinth.Logging.Host;
using NUnit.Framework;

namespace Tests.Plinth.Database.PgSql;

[SetUpFixture]
public class NUnitSetup
{
    [OneTimeSetUp]
    public void Setup()
    {
        StaticLogManagerSetup.ConfigureForConsoleLogging();
    }
}
