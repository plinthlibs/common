﻿
-- Plinth Job Procedures
CREATE OR REPLACE FUNCTION public.fn_submit_job (
IN i_code VARCHAR(100),
IN i_description VARCHAR(255) = NULL,
IN i_job_data JSON = NULL,
IN i_cron_expression VARCHAR(50) = NULL,
IN i_time_zone VARCHAR(100) = NULL,
IN i_is_active BOOLEAN = NULL,
IN i_calling_user VARCHAR(255) = NULL
)
RETURNS INT
LANGUAGE plpgsql    
AS $$
DECLARE v_rc bigint;
BEGIN

	/*
		-- create
		SELECT * FROM public.fn_submit_job(
			i_code := 'MyTestJob',
			i_description := 'This is my test job',
			i_cron_expression := '00 * * * *',
			i_time_zone := 'Pacific Standard Time',
			i_is_active := 1,
			i_calling_user := 'user');

		-- update
		SELECT * FROM public.fn_submit_job(
			i_code := 'MyTestJob',
			i_cron_expression := '00 6,18 * * *',
			i_time_zone := 'Eastern Standard Time');
	*/

	IF i_calling_user IS NULL THEN
		i_calling_user := (SELECT current_user);
	END IF;

	IF EXISTS(SELECT * FROM public.job WHERE code = i_code) THEN
        UPDATE public.job SET
    		description = COALESCE(i_description, job.description),
    		job_data = COALESCE(i_job_data, job.job_data),
    		cron_expression = COALESCE(i_cron_expression, job.cron_expression),
    		time_zone = COALESCE(i_time_zone, job.time_zone),
    		is_active = COALESCE(i_is_active, job.is_active),
    		date_updated = statement_timestamp(),
    		updated_by = i_calling_user
		WHERE code = i_code;
	ELSE
    	INSERT INTO public.job (
    		code,
    		description,
    		job_data,
    		cron_expression,
    		time_zone,
    		is_active,
    		date_inserted,
    		inserted_by,
    		date_updated,
    		updated_by)
    	VALUES (
    		i_code,
    		i_description,
    		COALESCE(i_job_data, '{}'),
    		i_cron_expression,
    		COALESCE(i_time_zone, 'UTC'),
    		COALESCE(i_is_active, true),
    		statement_timestamp(),
    		i_calling_user,
    		statement_timestamp(),
    		i_calling_user);
	END IF;

    GET DIAGNOSTICS v_rc = ROW_COUNT;
    RETURN v_rc;
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_get_job_list(
OUT o_code VARCHAR(100),
OUT o_description VARCHAR(255),
OUT o_job_data JSON,
OUT o_cron_expression VARCHAR(50),
OUT o_time_zone VARCHAR(100),
OUT o_is_active BOOLEAN,
OUT o_date_inserted TIMESTAMP,
OUT o_inserted_by VARCHAR(255),
OUT o_date_updated TIMESTAMP,
OUT o_updated_by VARCHAR(255)
)
RETURNS SETOF RECORD
LANGUAGE plpgsql    
AS $$
BEGIN
    /*
        SELECT * FROM public.fn_get_job_list();
    */

	RETURN QUERY
    SELECT
        j.code,
        j.description,
        j.job_data,
        j.cron_expression,
        j.time_zone,
        j.is_active,
        j.date_inserted,
        j.inserted_by,
        j.date_updated,
        j.updated_by
    FROM public.job AS j;

END;
$$;



-- Plinth Job Procedures

CREATE OR REPLACE FUNCTION public.fn_clear_db()
RETURNS VOID
AS $$
DECLARE
    r RECORD;
   _sql text;
BEGIN

    DROP SCHEMA IF EXISTS hangfire CASCADE;

    -- if the schema you operate on is not "current", you will want to
    -- replace current_schema() in query with 'schematodeletetablesfrom'
    -- *and* update the generate 'DROP...' accordingly.
    FOR r IN (SELECT tablename FROM pg_tables WHERE schemaname = current_schema()) LOOP
        EXECUTE 'DROP TABLE IF EXISTS ' || quote_ident(r.tablename) || ' CASCADE';
    END LOOP;

    SELECT INTO _sql
           string_agg(format('DROP %s %s;'
                           , CASE prokind
                               WHEN 'f' THEN 'FUNCTION'
                               WHEN 'a' THEN 'AGGREGATE'
                               WHEN 'p' THEN 'PROCEDURE'
                               WHEN 'w' THEN 'FUNCTION'  -- window function (rarely applicable)
                               -- ELSE NULL              -- not possible in pg 11
                              END
                           , oid::regprocedure)
                    , E'\n')
    FROM   pg_proc
    WHERE  pronamespace = 'public'::regnamespace  -- schema name here!
    -- AND    prokind = ANY ('{f,a,p,w}')         -- optionally only selected kinds
    ;

    IF _sql IS NOT NULL THEN
       RAISE NOTICE '%', _sql;  -- debug / check first
       EXECUTE _sql;         -- uncomment payload once you are sure
    ELSE 
       RAISE NOTICE 'No fuctions found in schema %', quote_ident('public');
    END IF;
END
$$
LANGUAGE plpgsql;