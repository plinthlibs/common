using Newtonsoft.Json;
using NUnit.Framework;
using Plinth.Serialization.JsonNet;

namespace Tests.Plinth.Serialization.JsonNet;

[TestFixture]
public class JsonNetStringNullableEnumConverterTest
{
    private enum TestEnum
    {
        DefaultVal = 0,
        Value1 = 1,
        Value2 = 2,
        Value3 = 3
    }

    private class TestObject
    {
        public TestEnum TestEnum { get; set; }
        public TestEnum? NullableTestEnum { get; set; }
    }

    [Test]
    public void Test_DeserializeStringEnum()
    {
        var obj = Deserialize<TestObject>("{\"TestEnum\": \"Value2\"}");
        Assert.That(obj.TestEnum, Is.EqualTo(TestEnum.Value2));
        Assert.That(obj.NullableTestEnum, Is.Null);

        obj = Deserialize<TestObject>("{\"TestEnum\": 3}");
        Assert.That(obj.TestEnum, Is.EqualTo(TestEnum.Value3));
        Assert.That(obj.NullableTestEnum, Is.Null);

        obj = Deserialize<TestObject>("{\"NullableTestEnum\": \"Value2\"}");
        Assert.That(obj.TestEnum, Is.EqualTo(TestEnum.DefaultVal));
        Assert.That(obj.NullableTestEnum, Is.EqualTo(TestEnum.Value2));

        obj = Deserialize<TestObject>("{\"NullableTestEnum\": 3}");
        Assert.That(obj.TestEnum, Is.EqualTo(TestEnum.DefaultVal));
        Assert.That(obj.NullableTestEnum, Is.EqualTo(TestEnum.Value3));

        obj = Deserialize<TestObject>("{\"NullableTestEnum\": \"InvalidValue\"}");
        Assert.That(obj.TestEnum, Is.EqualTo(TestEnum.DefaultVal));
        Assert.That(obj.NullableTestEnum, Is.Null);

        Assert.Throws<JsonSerializationException>(() => Deserialize<TestObject>("{\"TestEnum\": \"InvalidValue\"}"));
    }

    private static T Deserialize<T>(string json)
    {
        return JsonConvert.DeserializeObject<T>(
            json,
            new JsonSerializerSettings()
            {
                Converters = [new JsonNetStringNullableEnumConverter()]
            }) ?? throw new Exception();
    }
}
