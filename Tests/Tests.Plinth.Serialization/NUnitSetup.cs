using Plinth.Logging.Host;
using NUnit.Framework;

namespace Tests.Plinth.Serialization;

[SetUpFixture]
public class NUnitSetup
{
    [OneTimeSetUp]
    public void Setup()
    {
        StaticLogManagerSetup.ConfigureForConsoleLogging();
    }
}
