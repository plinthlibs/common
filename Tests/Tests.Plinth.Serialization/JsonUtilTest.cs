using Plinth.Serialization;
using Newtonsoft.Json.Serialization;
using NUnit.Framework;
using System.Reflection;

namespace Tests.Plinth.Serialization;

[TestFixture]
public class JsonUtilUtilTest
{
    private enum TestEnum
    {
        Value1,
        Value2
    }

    private class BasicObject
    {
#pragma warning disable IDE1006 // Naming Styles
        public int x { get; set; }
        public string? y { get; set; }
        public decimal z { get; set; }
        public TestEnum v { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }

    private class BasicObjectWithDate
    {
#pragma warning disable IDE1006 // Naming Styles
        public int x { get; set; }
        public string? y { get; set; }
        public decimal z { get; set; }
        public TestEnum v { get; set; }
        public DateTime d { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }

    private class CamelCaseObject
    {
        public int IntField { get; set; }
        public string? StringField { get; set; }
        public decimal DecimalField { get; set; }
        public DateTime? DateField { get; set; }
    }

    public enum TestDeserializedEnum
    {
        EnumName1,
        EnumName2,
        EnumName3
    }

    private class TestDeserializedObject
    {
        public string? StringField { get; set; }
        public int IntField { get; set; }
        public bool BoolField { get; set; }
        public List<string>? ListOfStrings { get; set; }
        public List<TestDeserializedObject>? ListOfObjs { get; set; }
    }

    [Test]
    public void Test_Deserialize()
    {
        var b = JsonUtil.DeserializeObject<BasicObject>("{\"x\": 55, \"y\": \"abc\", \"z\": 1.25, \"v\": \"Value2\"}") ?? throw new Exception();

        Assert.That(b.x, Is.EqualTo(55));
        Assert.That(b.y, Is.EqualTo("abc"));
        Assert.That(b.z, Is.EqualTo(1.25m));
        Assert.That(b.v, Is.EqualTo(TestEnum.Value2));
    }

    [Test]
    public void Test_DeserializeNotAffectedByDefaults()
    {
        Newtonsoft.Json.JsonConvert.DefaultSettings = () => new Newtonsoft.Json.JsonSerializerSettings
        {
            DateFormatString = "yyyy-dd-MM",
        };
        var b = JsonUtil.DeserializeObject<BasicObjectWithDate>("{\"x\": 55, \"y\": \"abc\", \"z\": 1.25, \"v\": \"Value2\", \"d\": \"2000-01-10\"}") ?? throw new Exception();

        Assert.That(b.x, Is.EqualTo(55));
        Assert.That(b.y, Is.EqualTo("abc"));
        Assert.That(b.z, Is.EqualTo(1.25m));
        Assert.That(b.v, Is.EqualTo(TestEnum.Value2));
        Assert.That(b.d, Is.EqualTo(new DateTime(2000, 1, 10)));
    }

    [Test]
    public void Test_TryDeserialize()
    {
        var r = JsonUtil.TryDeserializeObject<BasicObject>("{\"x\": 55, \"y\": \"abc\", \"z\": 1.25, \"v\": \"Value2\"}", out var b);
        if (b == null) throw new Exception();

        Assert.That(r);
        Assert.That(b.x, Is.EqualTo(55));
        Assert.That(b.y, Is.EqualTo("abc"));
        Assert.That(b.z, Is.EqualTo(1.25m));
        Assert.That(b.v, Is.EqualTo(TestEnum.Value2));

        r = JsonUtil.TryDeserializeObject<BasicObject>("\"x\": 55, \"y\": \"abc\", \"z\": 1.25, \"v\": \"Value2\"}", out b);
        Assert.That(r, Is.False);
        Assert.That(b, Is.Null);
    }

    [Test]
    public void Test_Serialize()
    {
        var b = new BasicObject()
        {
            x = 55,
            y = "abc",
            z = 1.25m,
            v = TestEnum.Value2
        };

        var s = JsonUtil.SerializeObject(b);

        Assert.That(s, Is.EqualTo("{\"x\":55,\"y\":\"abc\",\"z\":1.25,\"v\":\"Value2\"}"));
    }

    [Test]
    public void Test_SerializeNotAffectedByDefaults()
    {
        Newtonsoft.Json.JsonConvert.DefaultSettings = () => new Newtonsoft.Json.JsonSerializerSettings
        {
            Formatting = Newtonsoft.Json.Formatting.Indented,
        };
        var b = new BasicObject()
        {
            x = 55,
            y = "abc",
            z = 1.25m,
            v = TestEnum.Value2
        };

        var s = JsonUtil.SerializeObject(b);

        Assert.That(s, Is.EqualTo("{\"x\":55,\"y\":\"abc\",\"z\":1.25,\"v\":\"Value2\"}"));
    }

    [Test]
    public void Test_Customize()
    {
        var b = JsonUtil.DeserializeObject<CamelCaseObject>("{\"intField\": 55, \"stringField\": \"abc\", \"decimalField\": 1.25, \"dateField\": \"10-04-2014\"}",
            (settings) => settings.DateFormatString = "MM-DD-YYYY") ?? throw new Exception();

        Assert.That(b.IntField, Is.EqualTo(55));
        Assert.That(b.StringField, Is.EqualTo("abc"));
        Assert.That(b.DecimalField, Is.EqualTo(1.25m));
        Assert.That(b.DateField, Is.EqualTo(new DateTime(2014, 10, 4)));

        b = new CamelCaseObject()
        {
            IntField = 55,
            StringField = "abc",
            DecimalField = 1.25m,
            DateField = null
        };

        var s = JsonUtil.SerializeObject(b);
        Assert.That(s, Is.EqualTo("{\"IntField\":55,\"StringField\":\"abc\",\"DecimalField\":1.25}"));

        s = JsonUtil.SerializeObject(b, (settings) => settings.ContractResolver = new CamelCasePropertyNamesContractResolver());
        Assert.That(s, Is.EqualTo("{\"intField\":55,\"stringField\":\"abc\",\"decimalField\":1.25}"));
    }

    [Test]
    public void Test_DeepClone()
    {
        var b = new CamelCaseObject()
        {
            DateField = DateTime.Now,
            StringField = "hello"
        };

        var s = JsonUtil.DeepClone(b)!;

        Assert.That(s.DateField!.Value, Is.EqualTo(b.DateField.Value));
        // Assert.AreNotSame(b.DateField, s.DateField); // always fails
        Assert.That(s.StringField, Is.EqualTo(b.StringField));
        Assert.That(s.StringField, Is.Not.SameAs(b.StringField));
    }

    [Test]
    public void Test_DeserializeObjectFromFile()
    {
        var obj = new BasicObject
        {
            x = 55,
            y = "abc",
            z = 1.25m,
            v = TestEnum.Value2
        };

        var json = JsonUtil.SerializeObject(obj);
        string filename = Path.Combine(new FileInfo(Assembly.GetAssembly(typeof(JsonUtilUtilTest))!.Location).Directory!.FullName, "Test_DeserializeObjectFromFile.txt");
        try
        {
            File.WriteAllText(filename, json);
            var obj2 = JsonUtil.DeserializeObjectFromFile<BasicObject>(filename) ?? throw new Exception();
            Assert.That(obj2.x, Is.EqualTo(obj.x));
            Assert.That(obj2.y, Is.EqualTo(obj.y));
            Assert.That(obj2.z, Is.EqualTo(obj.z));
            Assert.That(obj2.v, Is.EqualTo(obj.v));
        }
        finally
        {
            if (File.Exists(filename))
                File.Delete(filename);
        }
    }

    [Test]
    public void Test_TryDeserializeObjectFromFile()
    {
        var obj = new BasicObject
        {
            x = 55,
            y = "abc",
            z = 1.25m,
            v = TestEnum.Value2
        };

        var json = JsonUtil.SerializeObject(obj);
        string filename = Path.Combine(new FileInfo(Assembly.GetAssembly(typeof(JsonUtilUtilTest))!.Location).Directory!.FullName, "Test_DeserializeObjectFromFile.txt");
        try
        {
            var r = JsonUtil.TryDeserializeObjectFromFile<BasicObject>("not exists " + Guid.NewGuid().ToString(), out var objbad);
            Assert.That(r, Is.False);
            Assert.That(objbad, Is.Null);

            File.WriteAllText(filename, "not valid json");
            r = JsonUtil.TryDeserializeObjectFromFile<BasicObject>(filename, out var objbad2);
            Assert.That(r, Is.False);
            Assert.That(objbad2, Is.Null);

            File.WriteAllText(filename, json);
            r = JsonUtil.TryDeserializeObjectFromFile<BasicObject>(filename, out var obj2);
            if (obj2 == null) throw new Exception();
            Assert.That(r);
            Assert.That(obj2.x, Is.EqualTo(obj.x));
            Assert.That(obj2.y, Is.EqualTo(obj.y));
            Assert.That(obj2.z, Is.EqualTo(obj.z));
            Assert.That(obj2.v, Is.EqualTo(obj.v));
        }
        finally
        {
            if (File.Exists(filename))
                File.Delete(filename);
        }
    }

    [Test]
    public void Test_ConvertDeserializedObject_Scalars()
    {
        Test_ConvertDeserializedObject<string>("value1");
        Test_ConvertDeserializedObject<int>(5573);
        Test_ConvertDeserializedObject<long>(287472657252525L);
        Test_ConvertDeserializedObject<bool>(true);
        Test_ConvertDeserializedObject<bool>(false);
        Test_ConvertDeserializedObject<double>(1.7462);
    }

    [Test]
    public void Test_TryConvertDeserializedObject_Scalars()
    {
        Test_TryConvertDeserializedObject<string>("value1");
        Test_TryConvertDeserializedObject<int>(5573);
        Test_TryConvertDeserializedObject<long>(287472657252525L);
        Test_TryConvertDeserializedObject<bool>(true);
        Test_TryConvertDeserializedObject<bool>(false);
        Test_TryConvertDeserializedObject<double>(1.7462);
    }

    [Test]
    public void Test_ConvertDeserializedObject_Defaults()
    {
        Assert.That(JsonUtil.ConvertDeserializedObject(null, "default"), Is.EqualTo("default"));
        Assert.That(JsonUtil.ConvertDeserializedObject(null, 55), Is.EqualTo(55));
        Assert.That(JsonUtil.ConvertDeserializedObject<string>(null), Is.Null);
        Assert.That(JsonUtil.ConvertDeserializedObject<int?>(null), Is.Null);
        Assert.That(JsonUtil.ConvertDeserializedObject<long?>(null), Is.Null);
        Assert.That(JsonUtil.ConvertDeserializedObject<bool?>(null), Is.Null);
        Assert.That(JsonUtil.ConvertDeserializedObject<double?>(null), Is.Null);
        Assert.That(JsonUtil.ConvertDeserializedObject<object>(null), Is.Null);
        Assert.That(JsonUtil.ConvertDeserializedObject<List<string>>(null), Is.Null);
    }

    [Test]
    public void Test_TryConvertDeserializedObject_Defaults()
    {
        Assert.That(JsonUtil.TryConvertDeserializedObject<string>(null, out var _), Is.False);
        Assert.That(JsonUtil.TryConvertDeserializedObject<int?>(null, out var _), Is.False);
        Assert.That(JsonUtil.TryConvertDeserializedObject<long?>(null, out var _), Is.False);
        Assert.That(JsonUtil.TryConvertDeserializedObject<bool?>(null, out var _), Is.False);
        Assert.That(JsonUtil.TryConvertDeserializedObject<double?>(null, out var _), Is.False);
        Assert.That(JsonUtil.TryConvertDeserializedObject<object>(null, out var _), Is.False);
        Assert.That(JsonUtil.TryConvertDeserializedObject<List<string>>(null, out var _), Is.False);
    }

    [Test]
    public void Test_ConvertDeserializedObject_Objects()
    {
        Test_ConvertDeserializedObject<TestDeserializedEnum>(TestDeserializedEnum.EnumName2);

        var obj = new TestDeserializedObject()
        {
            StringField = "str1",
            IntField = 48,
            BoolField = true,
            ListOfStrings = ["a", "b", "c"],
            ListOfObjs = [new TestDeserializedObject() { IntField = 5 }, new TestDeserializedObject() { StringField = "hello" }]
        };

        var jsonString = JsonUtil.SerializeObject(obj);
        var to = JsonUtil.DeserializeObject<TestDeserializedObject>(jsonString) ?? throw new Exception();

        Assert.That(to.StringField, Is.EqualTo("str1"));
        Assert.That(to.IntField, Is.EqualTo(48));
        Assert.That(to.BoolField, Is.EqualTo(true));
        Assert.That(to.ListOfStrings, Is.EqualTo(new List<string>() { "a", "b", "c" }).AsCollection);
        Assert.That(to.ListOfObjs?.Count, Is.EqualTo(2));
        Assert.That(to.ListOfObjs?[0].IntField, Is.EqualTo(5));
        Assert.That(to.ListOfObjs?[1].StringField, Is.EqualTo("hello"));

        Test_ConvertDeserializedObject<List<TestDeserializedEnum>>(
            new List<TestDeserializedEnum>() { TestDeserializedEnum.EnumName2, TestDeserializedEnum.EnumName3 });
        Test_ConvertDeserializedObject<List<int>>(
            new List<int>() { 1, 5, 10 });
        Test_ConvertDeserializedObject<List<string>>(
            new List<string>() { "10", "4", "2" });
        Test_ConvertDeserializedObject<HashSet<string>>(
            new HashSet<string>() { "d", "e", "f" });

        jsonString = JsonUtil.SerializeObject(
            new List<TestDeserializedObject>() { new TestDeserializedObject() { IntField = 5 }, new TestDeserializedObject() { StringField = "hello" } });
        var list = JsonUtil.DeserializeObject<List<TestDeserializedObject>>(jsonString);
        Assert.That(list?.Count, Is.EqualTo(2));
        Assert.That(list?[0].IntField, Is.EqualTo(5));
        Assert.That(list?[1].StringField, Is.EqualTo("hello"));
    }

    private static void Test_ConvertDeserializedObject<T>(object o)
    {
        var jsonString = JsonUtil.SerializeObject(o);
        var deserializedObject = JsonUtil.DeserializeObject<object>(jsonString);

        Assert.That(JsonUtil.ConvertDeserializedObject<T>(deserializedObject), Is.EqualTo(o));
    }

    private static void Test_TryConvertDeserializedObject<T>(object o)
    {
        var jsonString = JsonUtil.SerializeObject(o);
        var deserializedObject = JsonUtil.DeserializeObject<object>(jsonString);

        Assert.That(JsonUtil.TryConvertDeserializedObject(deserializedObject, out T? convertedVal));
        Assert.That(convertedVal, Is.EqualTo(o));
    }
}
