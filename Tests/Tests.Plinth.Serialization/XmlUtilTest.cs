﻿using Plinth.Serialization;
using NUnit.Framework;
using System.Reflection;

namespace Tests.Plinth.Serialization;

[TestFixture]
public class XmlUtilUtilTest
{
    public enum TestEnum
    {
        Value1,
        Value2
    }

#pragma warning disable IDE1006 // Naming Styles
    public class BasicObject
    {
        public int x { get; set; }
        public string? y { get; set; }
        public decimal z { get; set; }
        public TestEnum v { get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles

    [Test]
    public void Test_Deserialize()
    {
        var b = XmlUtil.Deserialize<BasicObject>(
            "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
            "\r\n<BasicObject xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" +
            "\r\n  <x>55</x>" +
            "\r\n  <y>abc</y>" +
            "\r\n  <z>1.25</z>" +
            "\r\n  <v>Value2</v>" +
            "\r\n</BasicObject>") ?? throw new Exception();

        Assert.That(b.x, Is.EqualTo(55));
        Assert.That(b.y, Is.EqualTo("abc"));
        Assert.That(b.z, Is.EqualTo(1.25m));
        Assert.That(b.v, Is.EqualTo(TestEnum.Value2));
    }

    [Test]
    public void Test_DeserializeFromFile()
    {
        var b = XmlUtil.DeserializeFromFile<BasicObject>(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!, "data/BasicObject.xml")) ?? throw new Exception();

        Assert.That(b.x, Is.EqualTo(55));
        Assert.That(b.y, Is.EqualTo("abc"));
        Assert.That(b.z, Is.EqualTo(1.25m));
        Assert.That(b.v, Is.EqualTo(TestEnum.Value2));
    }

    [Test]
    public void Test_DeserializeFromStream()
    {
        var s = new StreamReader(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!, "data/BasicObject.xml")) ?? throw new Exception();
        var b = XmlUtil.DeserializeFromStream<BasicObject>(s.BaseStream) ?? throw new Exception();

        Assert.That(b.x, Is.EqualTo(55));
        Assert.That(b.y, Is.EqualTo("abc"));
        Assert.That(b.z, Is.EqualTo(1.25m));
        Assert.That(b.v, Is.EqualTo(TestEnum.Value2));

        s.Dispose();
    }

    [Test]
    public void Test_Serialize()
    {
        var b = new BasicObject()
        {
            x = 55,
            y = "abc",
            z = 1.25m,
            v = TestEnum.Value2
        };

        var s = XmlUtil.Serialize(b, XmlUtil.SerializerSettings.OmitNamespaces);

        Assert.That(
s, Is.EqualTo("<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
            Environment.NewLine + "<BasicObject>" +
            Environment.NewLine + "  <x>55</x>" +
            Environment.NewLine + "  <y>abc</y>" +
            Environment.NewLine + "  <z>1.25</z>" +
            Environment.NewLine + "  <v>Value2</v>" +
            Environment.NewLine + "</BasicObject>"));

        s = XmlUtil.Serialize(b, XmlUtil.SerializerSettings.Default);

        // the serializer sometimes puts the namespaces out of order
        string opt1 =
            "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
            Environment.NewLine + "<BasicObject xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" +
            Environment.NewLine + "  <x>55</x>" +
            Environment.NewLine + "  <y>abc</y>" +
            Environment.NewLine + "  <z>1.25</z>" +
            Environment.NewLine + "  <v>Value2</v>" +
            Environment.NewLine + "</BasicObject>";

        string opt2 =
            "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
            Environment.NewLine + "<BasicObject xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
            Environment.NewLine + "  <x>55</x>" +
            Environment.NewLine + "  <y>abc</y>" +
            Environment.NewLine + "  <z>1.25</z>" +
            Environment.NewLine + "  <v>Value2</v>" +
            Environment.NewLine + "</BasicObject>";

        Assert.That(opt1 == s || opt2 == s);

        s = XmlUtil.Serialize(b);
        Assert.That(opt1 == s || opt2 == s);

        s = XmlUtil.Serialize(b, XmlUtil.SerializerSettings.OmitNamespaces | XmlUtil.SerializerSettings.OmitXmlDeclaration);

        Assert.That(
s, Is.EqualTo("<BasicObject>" +
            Environment.NewLine + "  <x>55</x>" +
            Environment.NewLine + "  <y>abc</y>" +
            Environment.NewLine + "  <z>1.25</z>" +
            Environment.NewLine + "  <v>Value2</v>" +
            Environment.NewLine + "</BasicObject>"));

        s = XmlUtil.Serialize(b, XmlUtil.SerializerSettings.OmitNamespaces | XmlUtil.SerializerSettings.OmitXmlDeclaration | XmlUtil.SerializerSettings.NoIndent);

        Assert.That(s, Is.EqualTo("<BasicObject><x>55</x><y>abc</y><z>1.25</z><v>Value2</v></BasicObject>"));

        s = XmlUtil.Serialize(b, XmlUtil.SerializerSettings.OmitNamespaces | XmlUtil.SerializerSettings.NoIndent);

        Assert.That(s, Is.EqualTo("<?xml version=\"1.0\" encoding=\"utf-8\"?><BasicObject><x>55</x><y>abc</y><z>1.25</z><v>Value2</v></BasicObject>"));
    }
}
