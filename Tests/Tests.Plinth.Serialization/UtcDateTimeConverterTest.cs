using Plinth.Serialization;
using NUnit.Framework;

namespace Tests.Plinth.Serialization;

[TestFixture]
public class UtcDateTimeConverterTest
{
    [Test]
    [TestCase("2016-10-31", 1477872000)]          // no time, TZ
    [TestCase("2016-10-31T14:30:00", 1477924200)] // no TZ
    [TestCase("2016-10-31T14:30:00Z", 1477924200)] // Zulu
    [TestCase("2016-10-31T14:30:00-00:00", 1477924200)] // UTC TZ
    [TestCase("2016-10-31T14:30:00+00:00", 1477924200)] // UTC TZ
    [TestCase("2016-10-31T07:30:00-07:00", 1477924200)] // Pacific TZ
    [TestCase("2016-10-31T09:30:00-05:00", 1477924200)] // East Coast TZ
    [TestCase("2009-06-15T13:45:30.1230000Z", 1245073530)] // "O" format
    public void TestUtc(string s, long unixTs)
    {
        var cv = new UtcDateTimeConverter();
        var dt = (DateTime)cv.ConvertFrom(s)!;
        Assert.That(new DateTimeOffset(dt).ToUnixTimeSeconds(), Is.EqualTo(unixTs));
    }

    [TestCase("2009-06-15T13:45:30.1230000Z", 1245073530123)] // "O" format
    [TestCase("2009-06-15T13:45:30.123000Z", 1245073530123)] // "O" format
    [TestCase("2009-06-15T13:45:30.12300Z", 1245073530123)] // "O" format
    [TestCase("2009-06-15T13:45:30.1230Z", 1245073530123)] // "O" format
    [TestCase("2009-06-15T13:45:30.123Z", 1245073530123)] // "O" format
    [TestCase("2009-06-15T13:45:30.12Z", 1245073530120)] // "O" format
    [TestCase("2009-06-15T13:45:30.1Z", 1245073530100)] // "O" format
    [TestCase("2009-06-15T13:45:30Z", 1245073530000)] // "O" format
    public void TestUtcMillis(string s, long unixTs)
    {
        var cv = new UtcDateTimeConverter();
        var dt = (DateTime)cv.ConvertFrom(s)!;
        Assert.That(new DateTimeOffset(dt).ToUnixTimeMilliseconds(), Is.EqualTo(unixTs));
    }
}
