using Plinth.HttpApiClient.Validation;
using NUnit.Framework;
using System.ComponentModel.DataAnnotations;

namespace Tests.Plinth.HttpApiClient.Validation;

[TestFixture]
public class RequiredOnlyOneOfAttributeTests : ValidationAttributeTestBase<RequiredOnlyOneOfAttribute>
{
    [SetUp]
    public void Init()
    {
        Target = new RequiredOnlyOneOfAttribute("Foo", "Bar", "Baz");
    }

    [Test]
    public void Invalid_when_none_set()
    {
        object obj = new { };
        AssertValidation(false, obj, new ValidationContext(obj));
    }

    [Test]
    public void Invalid_when_more_than_one_set()
    {
        object obj = new { Foo = "abc", Bar = "def" };
        AssertValidation(false, obj, new ValidationContext(obj));

        obj = new { Foo = "abc", Bar = "def", Baz = "hij" };
        AssertValidation(false, obj, new ValidationContext(obj));

        obj = new { Bar = "def", Baz = "hij" };
        AssertValidation(false, obj, new ValidationContext(obj));
    }

    [Test]
    public void Valid_when_only_one_set()
    {
        object obj = new { Foo = "abc" };
        AssertValidation(true, obj, new ValidationContext(obj));

        obj = new { Bar = "abc" };
        AssertValidation(true, obj, new ValidationContext(obj));
    }
}
