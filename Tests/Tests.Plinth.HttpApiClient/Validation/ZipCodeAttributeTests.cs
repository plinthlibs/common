using Plinth.HttpApiClient.Validation;
using NUnit.Framework;

namespace Tests.Plinth.HttpApiClient.Validation;

[TestFixture]
public class ZipCodeAttributeTests : ValidationAttributeTestBase<ZipCodeAttribute>
{
    [SetUp]
    public void Init()
    {
        Target = new ZipCodeAttribute();
    }

    [Test]
    public void Invalid_when_not_enough_digits()
    {
        AssertValidation(false, "0874");
        AssertValidation(false, "08742-123");
    }

    [Test]
    public void Invalid_when_too_many_digits()
    {
        AssertValidation(false, "1234567");
        AssertValidation(false, "08742-98765");
    }

    [Test]
    public void Invalid_when_not_a_zip()
    {
        AssertValidation(false, "foo bar");
        AssertValidation(false, "1,000");
        AssertValidation(false, "087424567");
    }

    [Test]
    public void Valid_standard_zip()
    {
        AssertValidation(true, "08742");
    }

    [Test]
    public void Valid_extended_zip()
    {
        AssertValidation(true, "08742-4567");
    }

    [Test]
    public void Valid_when_null()
    {
        AssertValidation(true, null);
    }
}
