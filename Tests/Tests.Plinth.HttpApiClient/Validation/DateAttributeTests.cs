using Plinth.HttpApiClient.Validation;
using NUnit.Framework;

namespace Tests.Plinth.HttpApiClient.Validation;

[TestFixture]
public class DateAttributeTests : ValidationAttributeTestBase<DateAttribute>
{
    [SetUp]
    public void Init()
    {
        Target = new DateAttribute();
    }

    [Test]
    public void Valid_DateTime()
    {
        AssertValidation(true, new DateTime(1975, 10, 25));
    }

    [Test]
    public void Valid_String()
    {
        AssertValidation(true, "10/25/1975");
    }

    [Test]
    public void Valid_when_null()
    {
        AssertValidation(true, null);
    }

    [Test]
    public void Invalid_String()
    {
        AssertValidation(false, "100/25/1975");
    }
}
