using NUnit.Framework;
using Plinth.HttpApiClient.Common.Validation;

namespace Tests.Plinth.HttpApiClient.Validation;

[TestFixture]
public class AgeValidationAttributeTests : ValidationAttributeTestBase<AgeValidationAttribute>
{
    private readonly int _minimumAge = 21;

    [SetUp]
    public void Init()
    {
        Target = new AgeValidationAttribute(_minimumAge);
    }

    [Test]
    public void Invalid_when_date_represents_age_less_than_minimum()
    {
        AssertValidation(false, DateTime.Now.AddYears(-(_minimumAge - 1)));
        AssertValidation(false, DateTime.Now.AddYears(-(_minimumAge - 5)));
    }

    [Test]
    public void Valid_when_date_represents_age_greater_than_minimum()
    {
        AssertValidation(true, DateTime.Now.AddYears(-(_minimumAge + 1)));
        AssertValidation(true, DateTime.Now.AddYears(-(_minimumAge + 5)));
    }

    [Test]
    public void Valid_when_date_represents_age_same_as_minimum()
    {
        AssertValidation(true, DateTime.Now.AddYears(-_minimumAge));
    }

    [Test]
    public void Invalid_when_date_in_future()
    {
        AssertValidation(false, DateTime.Now.AddYears(5));
    }

    [Test]
    public void Uses_supplied_error_message()
    {
        Target = new AgeValidationAttribute(_minimumAge, "x {0} {1}");
        var result = AssertValidation(false, DateTime.Now)!;
        Assert.That(result.ErrorMessage, Is.EqualTo($"x {DisplayName} {_minimumAge}"));
    }

    [Test]
    public void Null_succeeds()
    {
        AssertValidation(true, null);
    }
}
