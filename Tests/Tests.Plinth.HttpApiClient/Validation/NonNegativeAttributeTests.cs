using Plinth.HttpApiClient.Validation;
using NUnit.Framework;

namespace Tests.Plinth.HttpApiClient.Validation;

[TestFixture]
public class NonNegativeAttributeTests : ValidationAttributeTestBase<NonNegativeAttribute>
{
    [SetUp]
    public void Init()
    {
        Target = new NonNegativeAttribute();
    }

    [Test]
    public void Invalid_when_negative()
    {
        AssertValidation(false, -1);
        AssertValidation(false, -2.0m);
        AssertValidation(false, -2.5f);
        AssertValidation(false, -25274827842L);
    }

    [Test]
    public void Valid_when_non_negative()
    {
        AssertValidation(true, null);
        AssertValidation(true, 0);
        AssertValidation(true, 2.0m);
        AssertValidation(true, 2.5f);
        AssertValidation(true, 25274827842L);
    }
}
