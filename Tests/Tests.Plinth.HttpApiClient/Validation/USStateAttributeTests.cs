using Plinth.HttpApiClient.Validation;
using NUnit.Framework;
using System.ComponentModel.DataAnnotations;

namespace Tests.Plinth.HttpApiClient.Validation;

[TestFixture]
public class USStateAttributeTests : ValidationAttributeTestBase<USStateAttribute>
{
    [SetUp]
    public void Init()
    {
        Target = new USStateAttribute();
    }

    [Test]
    public void Number_of_valid_two_letter_codes_matches_number_of_states()
    {
        var letters = Enumerable.Range('A', 26).Select(c => (char)c).ToList();

        var codes = letters.SelectMany(c => letters.Select(d => c.ToString() + d))
            .Distinct();

        var count = codes.Count(c =>
            Target!.GetValidationResult(c, new ValidationContext(c)) == ValidationResult.Success);

        Assert.That(count, Is.EqualTo(51)); // DC is the 51st valid code
    }

    [Test]
    public void Invalid_state()
    {
        AssertValidation(false, "foo");
        AssertValidation(false, "A");
        AssertValidation(false, "ZZZ");
        AssertValidation(false, "XS");
    }

    [Test]
    public void Valid_state()
    {
        AssertValidation(true, "NJ");
        AssertValidation(true, "CA");
        AssertValidation(true, "kY"); // case doesn't matter
        AssertValidation(true, "Az");
        AssertValidation(true, "ne");
        AssertValidation(true, null); // null is ok
    }
}
