using Plinth.HttpApiClient.Validation;
using NUnit.Framework;

namespace Tests.Plinth.HttpApiClient.Validation;

[TestFixture]
public class IPAddressAttributeTests : ValidationAttributeTestBase<IPAddressAttribute>
{
    [SetUp]
    public void Init()
    {
        Target = new IPAddressAttribute();
    }

    [Test]
    public void Invalid_ip_address()
    {
        AssertValidation(false, "foo");
        AssertValidation(false, "google.com");
        AssertValidation(false, "123.4.3.1.1");
        AssertValidation(false, "123.4.3.1:80");
    }

    [Test]
    public void Valid_ip_address()
    {
        AssertValidation(true, null);
        AssertValidation(true, "127.0.0.1");
        AssertValidation(true, "::1");
        AssertValidation(true, "2001:db8:85a3:0:0:8a2e:370:7334");
        AssertValidation(true, "fe80::6c86:2214:7744:1b64%13");
    }
}
