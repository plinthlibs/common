using Plinth.HttpApiClient.Validation;
using NUnit.Framework;

namespace Tests.Plinth.HttpApiClient.Validation;

[TestFixture]
public class SocialSecurityNumberAttributeTests : ValidationAttributeTestBase<SocialSecurityNumberAttribute>
{
    [SetUp]
    public void Init()
    {
        Target = new SocialSecurityNumberAttribute();
    }

    [Test]
    public void Invalid_when_not_an_ssn()
    {
        AssertValidation(false, "foo");
        AssertValidation(false, "one two three");
        AssertValidation(false, "10/25/1975");
    }

    [Test]
    public void Invalid_when_missing_dash()
    {
        AssertValidation(false, "123-545445");
        AssertValidation(false, "12345-6567");
        AssertValidation(false, "123547687");
    }

    [Test]
    public void Invalid_when_dash_in_wrong_place()
    {
        AssertValidation(false, "1235-4-5445");
        AssertValidation(false, "123-5-45445");
        AssertValidation(false, "12-354-5445");
        AssertValidation(false, "1-2354-5445");
        AssertValidation(false, "123-545-445");
        AssertValidation(false, "123-5454-45");
        AssertValidation(false, "123-54544-5");
    }

    [Test]
    public void Invalid_when_a_digit_is_not_a_digit()
    {
        AssertValidation(false, "1X2-45-7687");
        AssertValidation(false, "152-45-7@87");
    }

    [Test]
    public void Valid_ssn()
    {
        AssertValidation(true, "121-34-5545");
    }

    [Test]
    public void Valid_when_null()
    {
        AssertValidation(true, null);
    }
}
