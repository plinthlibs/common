using Plinth.HttpApiClient.Validation;
using NUnit.Framework;

namespace Tests.Plinth.HttpApiClient.Validation;

[TestFixture]
public class YearAttributeTests : ValidationAttributeTestBase<YearAttribute>
{
    [SetUp]
    public void Init()
    {
        Target = new YearAttribute();
    }

    [Test]
    public void Invalid_when_not_enough_digits()
    {
        AssertValidation(false, "1");
        AssertValidation(false, "12");
        AssertValidation(false, "123");
    }

    [Test]
    public void Invalid_when_too_many_digits()
    {
        AssertValidation(false, "25671");
    }

    [Test]
    public void Invalid_when_not_a_year()
    {
        AssertValidation(false, "foo");
        AssertValidation(false, "19Q2");
        AssertValidation(false, "foo");
    }

    [Test]
    public void Valid_year()
    {
        AssertValidation(true, 1975);
    }

    [Test]
    public void Null_succeeds()
    {
        AssertValidation(true, null);
    }
}
