using Plinth.HttpApiClient.Validation;
using NUnit.Framework;

namespace Tests.Plinth.HttpApiClient.Validation;

[TestFixture]
public class IsTrueAttributeTests : ValidationAttributeTestBase<IsTrueAttribute>
{
    [SetUp]
    public void Init()
    {
        Target = new IsTrueAttribute();
    }

    [Test]
    public void Invalid_when_false()
    {
        AssertValidation(false, false);
    }

    [Test]
    public void Valid_when_true()
    {
        AssertValidation(true, true);
    }

    [Test]
    public void Valid_when_null()
    {
        AssertValidation(true, null);
    }

    [Test]
    public void Invalid_when_notbool()
    {
        AssertValidation(false, "abc");
    }
}
