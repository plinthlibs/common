using NUnit.Framework;
using System.ComponentModel.DataAnnotations;

namespace Tests.Plinth.HttpApiClient.Validation;

public abstract class ValidationAttributeTestBase<T> where T : ValidationAttribute
{
    protected T? Target;

    protected const string DisplayName = "Prop";

    protected ValidationResult? AssertValidation(bool expectValid, object? value, ValidationContext? context = null)
    {
        if (Target == null)
            throw new NullReferenceException(nameof(Target));

        var validationContext = context ?? new ValidationContext(new { Prop = value }) { DisplayName = DisplayName };

        var result = Target.GetValidationResult(value, validationContext);

        if (expectValid)
            Assert.That(result, Is.EqualTo(ValidationResult.Success));
        else
            Assert.That(result, Is.TypeOf<ValidationResult>());

        return result;
    }
}
