using Plinth.HttpApiClient.Validation;
using NUnit.Framework;
using System.ComponentModel.DataAnnotations;

namespace Tests.Plinth.HttpApiClient.Validation;

[TestFixture]
public class RequiredIfAttributeTests : ValidationAttributeTestBase<RequiredIfAttribute>
{
    [SetUp]
    public void Init()
    {
        Target = new RequiredIfAttribute("Foo", 1);
    }

    [Test]
    public void Invalid_when_condition_met_but_target_not_set()
    {
        AssertValidation(false, "", new ValidationContext(new { Foo = 1 }));
    }

    [Test]
    public void Valid_when_attribute_missing()
    {
        AssertValidation(true, "something", new ValidationContext(new { NotFoo = 1 }));
    }

    [Test]
    public void Valid_when_condition_met_and_target_set()
    {
        AssertValidation(true, "something", new ValidationContext(new { Foo = 1 }));
    }

    [Test]
    public void Valid_when_condition_not_met()
    {
        AssertValidation(true, "", new ValidationContext(new { Foo = 2 }));
        AssertValidation(true, "whatever", new ValidationContext(new { Foo = 2 }));
    }
}
