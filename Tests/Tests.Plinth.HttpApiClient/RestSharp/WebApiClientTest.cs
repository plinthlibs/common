using Plinth.Common.Constants;
using Plinth.Serialization;
using Plinth.HttpApiClient.RestSharp;
using NUnit.Framework;
using RestSharp;
using System.Net;
using System.Text;
using Plinth.Common.Logging;
using Plinth.HttpApiClient.Common;
using Plinth.HttpApiClient.RestSharp.Serialization;
using System.Reflection.Metadata.Ecma335;

namespace Tests.Plinth.HttpApiClient.RestSharp;

[TestFixture]
public class WebApiClientTest
{
    private class GetResponse
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string? V { get; set; }
    }

    public enum TestEnum
    {
        None = 0,
        Value1 = 1, Value2 = 2,
    }

    private class EchoObject
    {
        public string? PascalCase { get; set; }
        public Dictionary<string, object>? Dict { get; set; }
        public string? RawContent { get; set; }
        public TestEnum? TestEnumVal { get; set; }
    }

    private class MyApiClient : BaseHttpApiClient
    {
        public bool BeforeExecCalled = false;
        public bool AfterExecCalled = false;
        public bool AfterSuccessCalled = false;
        public bool AfterFailureCalled = false;

        public MyApiClient(string serviceId, string serverUrl, System.Net.Http.HttpClient httpClient, TimeSpan? timeout = null, ConfigureSerialization? cs = null) : base(serviceId, serverUrl, httpClient, timeout, cs)
        {
            BeforeExec = (r) =>
            {
                BeforeExecCalled = true;
            };

            AfterExec = (rq, rs) =>
            {
                AfterExecCalled = true;
            };

            AfterSuccess = (rq, rs) =>
            {
                AfterSuccessCalled = true;
            };

            AfterFailure = (rq, rs) =>
            {
                AfterFailureCalled = true;
            };
        }

        public MyApiClient(string serviceId, string serverUrl, TimeSpan? timeout = null, ConfigureSerialization? cs = null) : base(serviceId, serverUrl, timeout, cs)
        {
            BeforeExec = (r) =>
            {
                BeforeExecCalled = true;
            };

            AfterExec = (rq, rs) =>
            {
                AfterExecCalled = true;
            };

            AfterSuccess = (rq, rs) =>
            {
                AfterSuccessCalled = true;
            };

            AfterFailure = (rq, rs) =>
            {
                AfterFailureCalled = true;
            };
        }

        public GetResponse? GetThing(int x, int y)
        {
            var r = MakeRequest($"api/v1/thing/{x}/get", Method.Get);
            r.AddQueryParameter("y", y.ToString());
            return Execute<GetResponse>(r);
        }

        public GetResponse? GetThing(int x, int y, Func<RestRequest, Func<RestRequest, RestResponse>, RestResponse> interceptor)
        {
            var r = MakeRequest($"api/v1/thing/{x}/get", Method.Get);
            r.AddQueryParameter("y", y.ToString());
            return ExecuteIntercept<GetResponse>(r, new SyncInterceptor(interceptor));
        }

        public GetResponse? GetThing404Ok(int x, int y)
        {
            var r = MakeRequest($"api/v1/thing/{x}/get", Method.Get);
            r.AddQueryParameter("y", y.ToString());
            return Execute<GetResponse>(r, HttpStatusCode.NotFound);
        }

        public async Task<GetResponse?> GetThing404Ok2(int x, int y)
        {
            var r = MakeRequest($"api/v1/thing/{x}/get", Method.Get);
            r.AddQueryParameter("y", y.ToString());
            return await ReadOrNullAsync(async () => await ExecuteAsync<GetResponse?>(r));
        }

        public async Task<GetResponse?> GetThing404Ok3(int x, int y)
        {
            var cts = new CancellationTokenSource();
            var r = MakeRequest($"api/v1/thing/{x}/get", Method.Get);
            r.AddQueryParameter("y", y.ToString());
            return await ReadOrNullAsync(async () => await ExecuteAsync<GetResponse?>(r, cts.Token));
        }

        public EchoObject? Echo(EchoObject o)
        {
            var r = MakeRequest("api/v1/echo", Method.Post);
            r.AddJsonBody(o);
            return Execute<EchoObject>(r);
        }

        public string? EchoRaw(EchoObject o)
        {
            var r = MakeRequest("api/v1/echoraw", Method.Post);
            r.AddJsonBody(o);
            return Execute(r).Content;
        }

        public async Task<GetResponse?> GetThingAsync(int x, int y)
        {
            var r = MakeRequest($"api/v1/thing/{x}/get", Method.Get);
            r.AddQueryParameter("y", y.ToString());
            return await ExecuteAsync<GetResponse>(r);
        }

        public async Task<GetResponse?> GetThingAsync(int x, int y, Func<RestRequest, Func<RestRequest, Task<RestResponse>>, Task<RestResponse>> interceptor)
        {
            var r = MakeRequest($"api/v1/thing/{x}/get", Method.Get);
            r.AddQueryParameter("y", y.ToString());
            return await ExecuteInterceptAsync<GetResponse>(r, new AsyncInterceptor(interceptor));
        }

        public async Task<GetResponse?> GetThing404OkAsync(int x, int y)
        {
            var r = MakeRequest($"api/v1/thing/{x}/get", Method.Get);
            r.AddQueryParameter("y", y.ToString());
            return await ExecuteAsync<GetResponse>(r, HttpStatusCode.NotFound);
        }

        public async Task<EchoObject?> EchoAsync(EchoObject o)
        {
            var r = MakeRequest("api/v1/echo", Method.Post);
            r.AddJsonBody(o);
            return await ExecuteAsync<EchoObject>(r);
        }

        public async Task<EchoObject?> EchoShortcutAsync(EchoObject o)
        {
            return await ExecuteAsync<EchoObject>(HttpPost("api/v1/echo").AddJsonBody(o));
        }

        public EchoObject? EchoShortcut(EchoObject o)
        {
            return Execute<EchoObject>(HttpPost("api/v1/echo").AddJsonBody(o));
        }
    }

    [Test]
    [TestCase(1)]
    [TestCase(2)]
    public void TestClient(int tcase)
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/thing/"))
            {
                int x = req.Url.PathAndQuery.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Skip(3).Select(int.Parse).First();
                int y = int.Parse(req.QueryString.GetValues("y")!.First());
                resp.StatusCode = (int)HttpStatusCode.OK;

                resp.WriteBody(JsonUtil.SerializeObject(new GetResponse { V = "myString", X = x, Y = y }), ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = tcase switch
            {
                1 => new MyApiClient("service", $"http://localhost:{mhs.Port}/"),
                2 => new MyApiClient("service", $"http://localhost:{mhs.Port}/", new System.Net.Http.HttpClient()),
                _ => throw new NotImplementedException()
            };

            var g = client.GetThing(10, 20);

            Assert.That(g, Is.Not.Null);
            Assert.That(g?.X, Is.EqualTo(10));
            Assert.That(g?.Y, Is.EqualTo(20));
            Assert.That(g?.V, Is.EqualTo("myString"));

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled);
            Assert.That(client.AfterFailureCalled, Is.False);
        }
    }

    [Test]
    public void TestEmptyResult()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            resp.StatusCode = (int)HttpStatusCode.OK;
            resp.WriteBody(string.Empty, ContentTypes.ApplicationJson);
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", timeout: null);

            var g = client.GetThing(10, 20);
            Assert.That(g, Is.Null);
        }
    }

    [Test]
    public async Task TestClientAsync()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/thing/"))
            {
                int x = req.Url.PathAndQuery.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Skip(3).Select(int.Parse).First();
                int y = int.Parse(req.QueryString.GetValues("y")!.First());
                resp.StatusCode = (int)HttpStatusCode.OK;

                resp.WriteBody(JsonUtil.SerializeObject(new GetResponse { V = "myString", X = x, Y = y }), ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", timeout:null);

            var g = await client.GetThingAsync(10, 20);

            Assert.That(g, Is.Not.Null);
            Assert.That(g?.X, Is.EqualTo(10));
            Assert.That(g?.Y, Is.EqualTo(20));
            Assert.That(g?.V, Is.EqualTo("myString"));

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled);
            Assert.That(client.AfterFailureCalled, Is.False);
        }
    }

    [Test]
    public async Task TestClient404WithBody()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            resp.StatusCode = (int)HttpStatusCode.NotFound;
            resp.WriteBody(JsonUtil.SerializeObject(new GetResponse { Y = 10 }), ContentTypes.ApplicationJson);
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", timeout: null);

            try
            {
                client.GetThing(10, 20);
                Assert.Fail();
            }
            catch (RestException e)
            {
                Assert.That(e.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
            }

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled, Is.False);
            Assert.That(client.AfterFailureCalled);

            client.BeforeExecCalled = false;
            client.AfterExecCalled = false;
            client.AfterFailureCalled = false;

            // status codes
            var g = client.GetThing404Ok(10, 20);
            Assert.That(g, Is.Not.Null);
            Assert.That(g!.Y, Is.EqualTo(10));

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled);
            Assert.That(client.AfterFailureCalled, Is.False);

            g = client.GetThing404Ok(10, 40);
            Assert.That(g, Is.Not.Null);

            // ExecuteOrNull
            g = await client.GetThing404Ok2(10, 20);
            Assert.That(g, Is.Null);

            // ExecuteOrNull with token
            g = await client.GetThing404Ok3(10, 20);
            Assert.That(g, Is.Null);
        }
    }

    [Test]
    public void TestClientFailure()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            resp.StatusCode = (int)HttpStatusCode.NotFound;
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", timeout: null);

            try
            {
                client.GetThing(10, 20);
                Assert.Fail();
            }
            catch (RestException e)
            {
                Assert.That(e.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
            }

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled, Is.False);
            Assert.That(client.AfterFailureCalled);

            client.BeforeExecCalled = false;
            client.AfterExecCalled = false;
            client.AfterFailureCalled = false;

            var g = client.GetThing404Ok(10, 20);

            Assert.That(g, Is.Null);

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled);
            Assert.That(client.AfterFailureCalled, Is.False);
        }
    }

    [Test]
    public void TestRequestSerialization()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/echo"))
            {
                resp.StatusCode = (int)HttpStatusCode.OK;
                var bodyContent = req.ReadBody();
                EchoObject eo = JsonUtil.DeserializeObject<EchoObject>(bodyContent)!;
                eo.RawContent = bodyContent;
                // do it this way if you want to call the mock server more than once
                resp.ContentType = ContentTypes.ApplicationJsonUtf8;
                var content = JsonUtil.SerializeObject(eo);
                var contentBytes = Encoding.UTF8.GetBytes(content);
                resp.OutputStream.Write(contentBytes, 0, contentBytes.Length);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", timeout: null);

            var e = new EchoObject
            {
                PascalCase = "A Pascal Value",
                Dict = new Dictionary<string, object>()
                {
                    ["PascalKey"] = 5,
                    ["camelKey"] = 10
                }
            };

            assertEcho(client.Echo(e));
            assertEcho(client.EchoShortcut(e));
        }

        static void assertEcho(EchoObject? e2)
        {
            Assert.That(e2, Is.Not.Null);
            e2 = e2 ?? throw new Exception();
            Assert.That(e2.PascalCase, Is.EqualTo("A Pascal Value"));
            Assert.That(e2.Dict?["PascalKey"], Is.EqualTo(5));
            Assert.That(e2.Dict?["camelKey"], Is.EqualTo(10));

            Assert.That(e2.RawContent, Is.Not.Null);
            Assert.That(e2.RawContent, Does.Contain("\"PascalKey\":5"));
            Assert.That(e2.RawContent, Does.Contain("\"camelKey\":10"));
        }
    }

    [Test]
    public void TestCustomSerialization()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery == "/api/v1/echo")
            {
                resp.StatusCode = (int)HttpStatusCode.OK;
                var bodyContent = req.ReadBody();
                Assert.That(bodyContent, Is.EqualTo("{\"TestEnumVal\":1}"));
                EchoObject eo = JsonUtil.DeserializeObject<EchoObject>(bodyContent)!;
                eo.RawContent = bodyContent;
                // do it this way if you want to call the mock server more than once
                resp.ContentType = ContentTypes.ApplicationJsonUtf8;
                var content = JsonUtil.SerializeObject(eo);
                var contentBytes = Encoding.UTF8.GetBytes(content);
                resp.OutputStream.Write(contentBytes, 0, contentBytes.Length);
            }
            else if (req.Url!.PathAndQuery.StartsWith("/api/v1/echoraw"))
            {
                resp.StatusCode = (int)HttpStatusCode.OK;
                var bodyContent = req.ReadBody();
                Assert.That(bodyContent, Is.EqualTo("{\"TestEnumVal\":1}"));
                EchoObject eo = JsonUtil.DeserializeObject<EchoObject>(bodyContent)!;
                // do it this way if you want to call the mock server more than once
                resp.ContentType = ContentTypes.ApplicationJsonUtf8;
                var contentBytes = Encoding.UTF8.GetBytes(bodyContent);
                resp.OutputStream.Write(contentBytes, 0, contentBytes.Length);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();

            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", timeout: null,
                (c) => c.UseSerializer(() => new RestSharpJsonNetClientSerializer(m => m.Converters.Clear(), m => m.Converters.Clear())));

            var e = new EchoObject
            {
                TestEnumVal = TestEnum.Value1
            };

            var r = client.Echo(e);
            Assert.That(r, Is.Not.Null);
            Assert.That(r!.TestEnumVal, Is.EqualTo(TestEnum.Value1));

            var r2 = client.EchoRaw(e);
            Assert.That(r2, Is.Not.Null);
            Assert.That(r2!, Is.EqualTo("{\"TestEnumVal\":1}"));
        }
    }

    private class ShouldLogFalseClient : MyApiClient
    {
        public ShouldLogFalseClient(string serviceId, string serverUrl, TimeSpan? timeout) : base(serviceId, serverUrl, timeout)
        {
        }

        public override bool ShouldLogContent(string? contentType, bool isRequest)
        {
            return false;
        }
    }

    [Test]
    public void TestNoLog()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/echo"))
            {
                resp.StatusCode = (int)HttpStatusCode.OK;
                var bodyContent = req.ReadBody();
                EchoObject eo = JsonUtil.DeserializeObject<EchoObject>(bodyContent)!;
                eo.RawContent = bodyContent;
                resp.WriteBody(JsonUtil.SerializeObject(eo), ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new ShouldLogFalseClient("service", $"http://localhost:{mhs.Port}/", null);

            var e = new EchoObject
            {
                PascalCase = "A Pascal Value",
                Dict = new Dictionary<string, object>()
                {
                    ["PascalKey"] = 5,
                    ["camelKey"] = 10
                }
            };

            var e2 = client.Echo(e);

            Assert.That(e2, Is.Not.Null);
            Assert.That(e2!.PascalCase, Is.EqualTo("A Pascal Value"));
            Assert.That(e2!.Dict!["PascalKey"], Is.EqualTo(5));
            Assert.That(e2!.Dict!["camelKey"], Is.EqualTo(10));
        }
    }

    /*
    this test is slow (~4s) and restsharp is deprecated
    [Test]
    public void TestRequestFailedToConnect()
    {
        var client = new MyApiClient("service", $"http://localhost:{MockHttpServer.GetRandomUnusedPort()}/", null);

        var e = new EchoObject();

        Assert.Throws<WebException>(() => client.Echo(e));
    }
    */

    [Test]
    public void TestClientInterceptor()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/thing/"))
            {
                int x = req.Url.PathAndQuery.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Skip(3).Select(int.Parse).First();
                int y = int.Parse(req.QueryString.GetValues("y")!.First());
                resp.StatusCode = (int)HttpStatusCode.OK;

                resp.WriteBody(JsonUtil.SerializeObject(new GetResponse { V = "myString", X = x, Y = y }), ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", timeout: null);

            bool before = false, after = false;
            var g = client.GetThing(10, 20, (r, f) =>
            {
                before = true;
                try
                {
                    return f.Invoke(r);
                }
                finally
                {
                    after = true;
                }
            });

            Assert.That(g, Is.Not.Null);
            Assert.That(g?.X, Is.EqualTo(10));
            Assert.That(g?.Y, Is.EqualTo(20));
            Assert.That(g?.V, Is.EqualTo("myString"));

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled);
            Assert.That(client.AfterFailureCalled, Is.False);

            Assert.That(before);
            Assert.That(after);
        }
    }

    [Test]
    public async Task TestClientInterceptorAsync()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/thing/"))
            {
                int x = req.Url.PathAndQuery.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Skip(3).Select(int.Parse).First();
                int y = int.Parse(req.QueryString.GetValues("y")!.First());
                resp.StatusCode = (int)HttpStatusCode.OK;

                resp.WriteBody(JsonUtil.SerializeObject(new GetResponse { V = "myString", X = x, Y = y }), ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", timeout: null);

            bool before = false, after = false;
            var g = await client.GetThingAsync(10, 20, (r, f) =>
            {
                before = true;
                try
                {
                    return f.Invoke(r);
                }
                finally
                {
                    after = true;
                }
            });

            Assert.That(g, Is.Not.Null);
            Assert.That(g!.X, Is.EqualTo(10));
            Assert.That(g!.Y, Is.EqualTo(20));
            Assert.That(g!.V, Is.EqualTo("myString"));

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled);
            Assert.That(client.AfterFailureCalled, Is.False);

            Assert.That(before);
            Assert.That(after);
        }
    }
}
