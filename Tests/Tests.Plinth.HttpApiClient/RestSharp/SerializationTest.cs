using Newtonsoft.Json.Serialization;
using NUnit.Framework;
using RestSharp;
using Plinth.Common.Constants;
using Plinth.Serialization;
using Plinth.Serialization.JsonNet;
using Plinth.HttpApiClient.RestSharp.Serialization;

namespace Tests.Plinth.HttpApiClient.RestSharp;

[TestFixture]
public class SerializationTest
{
    [Test]
    public void TestJsonSerializer()
    {
        var r = new RestSharpJsonNetSerializer();

        Assert.That(r.ContentType.Value, Is.EqualTo(ContentTypes.ApplicationJson));

        Assert.That(r.Serialize(new { Value = "a", ValueTwo = 5, Null = (string?)null }), Is.EqualTo("{\"Value\":\"a\",\"ValueTwo\":5}"));

        r = new RestSharpJsonNetSerializer((s) => { s.NullValueHandling = Newtonsoft.Json.NullValueHandling.Include; s.ContractResolver = new CamelCasePropertyNamesContractResolver(); });

        Assert.That(r.Serialize(new { Value = "a", ValueTwo = 5, Null = (string?)null }), Is.EqualTo("{\"value\":\"a\",\"valueTwo\":5,\"null\":null}"));
    }

    [Test]
    public void TestJsonDeserializer()
    {
        var r = new RestSharpJsonNetDeserializer();

        var resp = new RestResponse() { Content = "{\"Value\":\"a\",\"ValueTwo\":5,\"Null\":null}" };

        var t = r.Deserialize<TestObj>(resp)!;
        Assert.That(t.Value, Is.EqualTo("a"));
        Assert.That(t.ValueTwo, Is.EqualTo(5));
        Assert.That(t.Null, Is.Null);

        r = new RestSharpJsonNetDeserializer(s => s.ContractResolver = new CamelCasePropertyNamesContractResolver());

        resp = new RestResponse() { Content = "{\"value\":\"a\",\"valueTwo\":5,\"null\":null}" };

        t = r.Deserialize<TestObj>(resp)!;
        Assert.That(t.Value, Is.EqualTo("a"));
        Assert.That(t.ValueTwo, Is.EqualTo(5));
        Assert.That(t.Null, Is.Null);
    }

    [Test]
    public void TestJsonNetUnderscorePropertyNamesContractResolver()
    {
        var r = new RestSharpJsonNetDeserializer(s => s.ContractResolver = new JsonNetUnderscorePropertyNamesContractResolver());

        var resp = new RestResponse() { Content = "{\"value\":\"a\",\"value_two\":5,\"null\":null}" };

        var t = r.Deserialize<TestObj>(resp)!;
        Assert.That(t.Value, Is.EqualTo("a"));
        Assert.That(t.ValueTwo, Is.EqualTo(5));
        Assert.That(t.Null, Is.Null);
    }

    [Test]
    public void TestJsonNetCamelCasePropertyNamesContractResolver()
    {
        var r = new RestSharpJsonNetSerializer(s => s.ContractResolver = new JsonNetCamelCasePropertyNamesContractResolver());

        Assert.That(r.ContentType.Value, Is.EqualTo(ContentTypes.ApplicationJson));

        var obj = new { Value = "a", ValueTwo = 5, Null = (string?)null, Dict = new Dictionary<string, object>() { { "ValueOne", 5 }, { "valueTwo", 10 } } };

        Assert.That(r.Serialize(obj), Is.EqualTo("{\"value\":\"a\",\"valueTwo\":5,\"dict\":{\"ValueOne\":5,\"valueTwo\":10}}"));

        var dr = new RestSharpJsonNetDeserializer(s => s.ContractResolver = new JsonNetCamelCasePropertyNamesContractResolver());

        var resp = new RestResponse() { Content = "{\"value\":\"a\",\"valueTwo\":5,\"dict\":{\"ValueOne\":5,\"valueTwo\":10}}" };
        var t = dr.Deserialize<TestObjWithDict>(resp)!;
        Assert.That(t.Value, Is.EqualTo("a"));
        Assert.That(t.ValueTwo, Is.EqualTo(5));
        Assert.That(t.Null, Is.Null);
        Assert.That(t.Dict?.Keys.Count, Is.EqualTo(2));
        Assert.That(t.Dict?["ValueOne"], Is.EqualTo(5));
        Assert.That(t.Dict?["valueTwo"], Is.EqualTo(10));

        resp = new RestResponse() { Content = "{\"ValueOne\":5,\"valueTwo\":10}" };
        var t2 = dr.Deserialize<Dictionary<string, object>>(resp);
        Assert.That(t.Dict?.Keys.Count, Is.EqualTo(2));
        Assert.That(t.Dict?["ValueOne"], Is.EqualTo(5));
        Assert.That(t.Dict?["valueTwo"], Is.EqualTo(10));
    }

    private class TestObj
    {
        public string? Value { get; set; }
        public int ValueTwo { get; set; }
        public string? Null { get; set; }
    }

    private class TestObjWithDict : TestObj
    {
        public Dictionary<string, object>? Dict { get; set; }
    }
}
