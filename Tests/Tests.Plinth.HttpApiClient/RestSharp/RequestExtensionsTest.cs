using Plinth.HttpApiClient.RestSharp;
using NUnit.Framework;
using RestSharp;
using FluentAssertions;
using Plinth.HttpApiClient.Response;

namespace Tests.Plinth.HttpApiClient.RestSharp;

[TestFixture]
public class RequestExtensionsTest
{
    private class TestApiClient : BaseHttpApiClient
    {
        private static readonly int _port = MockHttpServer.GetRandomUnusedPort();

        public TestApiClient()
            : base(nameof(TestApiClient), $"http://localhost:{_port}")
        {
        }

        public RestRequest New(string resource)
        {
            return HttpGet(resource);
        }

        public string? GetRequestUrl(RestRequest restRequest)
        {
            string? result = null;

            using (var server = new MockHttpServer(_port, (request, response) =>
            {
                result = request.RawUrl;
                response.StatusCode = 200;
            }))
            {
                Execute(restRequest);
            }

            return result;
        }
    }

    [Test]
    public void Can_Add_Query_Parameters_Using_RestSharp()
    {
        var client = new TestApiClient();

        var limit = 25;
        var expand = true;
        var orderBy = "Last Name";
        var filterBy = ApiErrorCode.ValidationError;

        var request = client.New("search")
            .AddQueryParameter("limit", limit.ToString())
            .AddQueryParameter("expand", expand.ToString())
            .AddQueryParameter("orderBy", orderBy)
            .AddQueryParameter("filterBy", filterBy.ToString());

        client.GetRequestUrl(request).Should().Be("/search?limit=25&expand=True&orderBy=Last%20Name&filterBy=ValidationError");
    }

    [Test]
    public void Can_Add_Query_Parameters_With_String_Conversion()
    {
        var client = new TestApiClient();

        var limit = 25;
        var expand = true;
        var orderBy = "Last Name";
        var filterBy = ApiErrorCode.ValidationError;

        var request = client.New("search")
            .AddQueryParameter("limit", limit)
            .AddQueryParameter("expand", expand)
            .AddQueryParameter("orderBy", orderBy)
            .AddQueryParameter("filterBy", filterBy);

        client.GetRequestUrl(request).Should().Be("/search?limit=25&expand=True&orderBy=Last%20Name&filterBy=ValidationError");

        string? a = null;
        int? b = null;
        ApiErrorCode? c = null;

        request = client.New("search")
            .AddQueryParameter("a", a!)
            .AddQueryParameter("b", b)
            .AddQueryParameter("c", c);

        client.GetRequestUrl(request).Should().Be("/search?a&b&c");
    }

    [Test]
    public void Can_Add_Optional_Query_Parameters()
    {
        var client = new TestApiClient();

        var limit = 25;
        var expand = true;
        var orderBy = "Last Name";
        var filterBy = ApiErrorCode.ValidationError;

        RestRequest Request() => client.New("search")
            .AddQueryOptional("limit", limit)
            .AddQueryOptional("expand", expand)
            .AddQueryOptional("orderBy", orderBy)
            .AddQueryOptional("filterBy", filterBy);

        client.GetRequestUrl(Request()).Should().Be("/search?limit=25&expand=True&orderBy=Last%20Name&filterBy=ValidationError");

        limit = 0;
        expand = false;
        orderBy = null;
        filterBy = ApiErrorCode.ServerError;

        client.GetRequestUrl(Request()).Should().Be("/search");
    }

    [Test]
    public void Check_Null_And_Repeatable_Query_Parameters()
    {
        // Here we just document the existing RestSharp behavior
        // cause other API libraries (including built-in NameValueCollection)
        // may behave different in those cases.

        var client = new TestApiClient();

        var request = client.New("search")
            .AddQueryParameter("a", null!)
            .AddQueryParameter("b", null!)
            .AddQueryParameter("c", null!);

        client.GetRequestUrl(request).Should().Be("/search?a&b&c");

        request = client.New("search")
            .AddQueryParameter("a", "1")
            .AddQueryParameter("b", "2")
            .AddQueryParameter("a", "3")
            .AddQueryParameter("b", null!)
            .AddQueryParameter("c", null!)
            .AddQueryParameter("c", null!);

        client.GetRequestUrl(request).Should().Be("/search?a=1&b=2&a=3&b&c&c");
    }

    [Test]
    public void Can_Add_List_Of_Query_Params()
    {
        var client = new TestApiClient();

        var request = client.New("search")
            .AddQueryParameter("a", new List<int?> { null, null });

        client.GetRequestUrl(request).Should().Be("/search?a&a");

        request = client.New("search")
            .AddQueryParameter("a", new List<int?> { 3, 86, 9 });

        client.GetRequestUrl(request).Should().Be("/search?a=3&a=86&a=9");

        request = client.New("search")
            .AddQueryParameter<string>("a", null);

        client.GetRequestUrl(request).Should().Be("/search");
    }

    [Test]
    public void Can_Add_Path_Segments_Using_RestSharp()
    {
        var client = new TestApiClient();
        var request = client.New("user/{userId}")
            .AddUrlSegment("userId", 158.ToString());

        client.GetRequestUrl(request).Should().Be("/user/158");

        client.Invoking(c => c.New("user/{login}").AddUrlSegment("login", null!))
            .Should().Throw<ArgumentNullException>();

        request = client.New("user/{login}");
        client.GetRequestUrl(request).Should().Be("/user/%7Blogin%7D");
    }

    [Test]
    public void Path_Segments_Should_Be_Used_Instead_Of_String_Interpolation()
    {
        // Note that creating resource URLs with string interpolation is technically not accurate,
        // since it can sometimes lead to improper behavior with slashes and other characters
        // which require URL encoding.

        var client = new TestApiClient();
        var id = "IDD_abcde/hello";

        var request = client.New($"id/{id}");

        // here we have incorrect path
        client.GetRequestUrl(request).Should().Be("/id/IDD_abcde/hello");

        // When using path segments, the corresponding portions of URL
        // are properly encoded.

        request = client.New("id/{id}")
            .AddUrlSegment("id", id);

        // here the path is correct
        client.GetRequestUrl(request).Should().Be("/id/IDD_abcde%2Fhello");
    }
}
