using System.Net;
using Plinth.HttpApiClient;
using NUnit.Framework;
using RestSharp;
using Plinth.HttpApiClient.RestSharp;

namespace Tests.Plinth.HttpApiClient.RestSharp;

[TestFixture]
class ResponseExtensionsTest
{
    [Test]
    public void Test_IsSuccess_Expected()
    {
        var r = new RestResponse { StatusCode = HttpStatusCode.OK };
        Assert.That(r.IsSuccess(HttpStatusCode.OK));

        for (int i = 300; i < 600; i++)
        {
            r = new RestResponse { StatusCode = (HttpStatusCode)i };
            Assert.That(r.IsSuccess(HttpStatusCode.OK, HttpStatusCode.Continue), Is.False);
        }

        r = new RestResponse { StatusCode = HttpStatusCode.NotFound };
        Assert.That(r.IsSuccess(HttpStatusCode.OK, HttpStatusCode.NotFound, HttpStatusCode.NotAcceptable));
    }

    [Test]
    public void Test_IsSuccess_T_Expected()
    {
        RestResponse<string> r = new RestResponse<string>(new RestRequest()) { StatusCode = HttpStatusCode.OK };
        Assert.That(r.IsSuccess(HttpStatusCode.OK));

        for (int i = 300; i < 600; i++)
        {
            r = new RestResponse<string>(new RestRequest()) { StatusCode = (HttpStatusCode)i };
            Assert.That(r.IsSuccess(HttpStatusCode.OK, HttpStatusCode.Continue), Is.False);
        }

        r = new RestResponse<string>(new RestRequest()) { StatusCode = HttpStatusCode.NotFound };
        Assert.That(r.IsSuccess(HttpStatusCode.OK, HttpStatusCode.NotFound, HttpStatusCode.NotAcceptable));
    }
}
