using Plinth.Common.Constants;
using Plinth.Serialization;
using Plinth.HttpApiClient.RestSharp;
using NUnit.Framework;
using RestSharp;
using System.Net;
using Plinth.Common.Logging;
using Polly;
using Plinth.HttpApiClient.Common;

namespace Tests.Plinth.HttpApiClient.RestSharp;

[TestFixture]
public class WebApiClientPollyTest
{
    private class GetResponse
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string? V { get; set; }
    }

    private class EchoObject
    {
        public string? PascalCase { get; set; }
        public Dictionary<string, object>? Dict { get; set; }
        public string? RawContent { get; set; }
    }

    private class MyApiClient : BaseHttpApiClientPolly
    {
        public bool BeforeExecCalled = false;
        public bool AfterExecCalled = false;
        public bool AfterSuccessCalled = false;
        public bool AfterFailureCalled = false;

        public MyApiClient(string serviceId, string serverUrl, TimeSpan? timeout) : base(serviceId, serverUrl, timeout)
        {
            BeforeExec = (r) =>
            {
                BeforeExecCalled = true;
            };

            AfterExec = (rq, rs) =>
            {
                AfterExecCalled = true;
            };

            AfterSuccess = (rq, rs) =>
            {
                AfterSuccessCalled = true;
            };

            AfterFailure = (rq, rs) =>
            {
                AfterFailureCalled = true;
            };
        }

        public GetResponse? GetThing(int x, int y, ISyncPolicy<RestResponse> p)
        {
            var r = MakeRequest($"api/v1/thing/{x}/get", Method.Get);
            r.AddQueryParameter("y", y.ToString());
            return ExecutePolly<GetResponse>(r, p);
        }

        public GetResponse? GetThingRetry(int x, int y)
        {
            var r = MakeRequest($"api/v1/thing/{x}/get", Method.Get);
            r.AddQueryParameter("y", y.ToString());
            return ExecuteWithRetries<GetResponse>(r, 10, TimeSpan.Zero);
        }

        public Task<GetResponse?> GetThingRetryAsync(int x, int y)
        {
            var r = MakeRequest($"api/v1/thing/{x}/get", Method.Get);
            r.AddQueryParameter("y", y.ToString());
            return ExecuteWithRetriesAsync<GetResponse>(r, 10, TimeSpan.Zero);
        }

        public GetResponse? GetThing404Ok(int x, int y)
        {
            var r = MakeRequest($"api/v1/thing/{x}/get", Method.Get);
            r.AddQueryParameter("y", y.ToString());
            return Execute<GetResponse>(r, HttpStatusCode.NotFound);
        }

        public EchoObject? Echo(EchoObject o)
        {
            var r = MakeRequest("api/v1/echo", Method.Post);
            r.AddJsonBody(o);
            return Execute<EchoObject>(r);
        }

        public async Task<GetResponse?> GetThingAsync(int x, int y, IAsyncPolicy<RestResponse> policy)
        {
            var r = MakeRequest($"api/v1/thing/{x}/get", Method.Get);
            r.AddQueryParameter("y", y.ToString());
            return await ExecutePollyAsync<GetResponse>(r, policy);
        }

        public async Task<GetResponse?> GetThingAsync(int x, int y, Func<RestRequest, Func<RestRequest, Task<RestResponse>>, Task<RestResponse>> interceptor)
        {
            var r = MakeRequest($"api/v1/thing/{x}/get", Method.Get);
            r.AddQueryParameter("y", y.ToString());
            return await ExecuteInterceptAsync<GetResponse>(r, new AsyncInterceptor(interceptor));
        }

        public async Task<GetResponse?> GetThing404OkAsync(int x, int y)
        {
            var r = MakeRequest($"api/v1/thing/{x}/get", Method.Get);
            r.AddQueryParameter("y", y.ToString());
            return await ExecuteAsync<GetResponse>(r, HttpStatusCode.NotFound);
        }

        public async Task<EchoObject?> EchoAsync(EchoObject o)
        {
            var r = MakeRequest("api/v1/echo", Method.Post);
            r.AddJsonBody(o);
            return await ExecuteAsync<EchoObject>(r);
        }

        public async Task<EchoObject?> EchoShortcutAsync(EchoObject o)
        {
            return await ExecuteAsync<EchoObject>(HttpPost("api/v1/echo").AddJsonBody(o));
        }

        public EchoObject? EchoShortcut(EchoObject o)
        {
            return Execute<EchoObject>(HttpPost("api/v1/echo").AddJsonBody(o));
        }
    }

    [Test]
    public void TestClient()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/thing/"))
            {
                int x = req.Url.PathAndQuery.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Skip(3).Select(int.Parse).First();
                int y = int.Parse(req.QueryString.GetValues("y")!.First());
                resp.StatusCode = (int)HttpStatusCode.OK;

                resp.WriteBody(JsonUtil.SerializeObject(new GetResponse { V = "myString", X = x, Y = y }), ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", null);

            bool ok = false;
            var policy = Policy.HandleResult<RestResponse>(r => { ok = true; return r.StatusCode != HttpStatusCode.OK; })
                .Fallback(new RestResponse { StatusCode = HttpStatusCode.BadRequest });

            var g = client.GetThing(10, 20, policy);

            Assert.That(ok);

            Assert.That(g, Is.Not.Null);
            Assert.That(g?.X, Is.EqualTo(10));
            Assert.That(g?.Y, Is.EqualTo(20));
            Assert.That(g?.V, Is.EqualTo("myString"));

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled);
            Assert.That(client.AfterFailureCalled, Is.False);
        }
    }

    [Test]
    public async Task TestClientAsync()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/thing/"))
            {
                int x = req.Url.PathAndQuery.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Skip(3).Select(int.Parse).First();
                int y = int.Parse(req.QueryString.GetValues("y")!.First());
                resp.StatusCode = (int)HttpStatusCode.OK;

                resp.WriteBody(JsonUtil.SerializeObject(new GetResponse { V = "myString", X = x, Y = y }), ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", null);

            bool ok = false;
            var policy = Policy.HandleResult<RestResponse>(r => { ok = true; return r.StatusCode != HttpStatusCode.OK; })
                .FallbackAsync(new RestResponse { StatusCode = HttpStatusCode.BadRequest });
            var g = await client.GetThingAsync(10, 20, policy);

            Assert.That(ok);

            Assert.That(g, Is.Not.Null);
            Assert.That(g?.X, Is.EqualTo(10));
            Assert.That(g?.Y, Is.EqualTo(20));
            Assert.That(g?.V, Is.EqualTo("myString"));

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled);
            Assert.That(client.AfterFailureCalled, Is.False);
        }
    }

    [Test]
    public void TestClientFailure()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            resp.StatusCode = (int)HttpStatusCode.NotFound;
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", null);

            bool tested = false;
            try
            {
                var policy = Policy.HandleResult<RestResponse>(r => { tested = true; return r.StatusCode == HttpStatusCode.NotFound; })
                    .Fallback(new RestResponse { StatusCode = HttpStatusCode.BadRequest });
                client.GetThing(10, 20, policy);
                Assert.Fail();
            }
            catch (RestException e)
            {
                Assert.That(e.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            }

            Assert.That(tested);

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled, Is.False);
            Assert.That(client.AfterFailureCalled);

            client.BeforeExecCalled = false;
            client.AfterExecCalled = false;
            client.AfterFailureCalled = false;

            var g = client.GetThing404Ok(10, 20);

            Assert.That(g, Is.Null);

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled);
            Assert.That(client.AfterFailureCalled, Is.False);
        }
    }

    /*
    this test is slow (~4s) and restsharp is deprecated
    [Test]
    public void TestRequestFailedToConnect()
    {
        var client = new MyApiClient("service", $"http://localhost:{MockHttpServer.GetRandomUnusedPort()}/", null);

        var policy = Policy.HandleResult<RestResponse>(r => r.StatusCode == 0).Fallback(new RestResponse { StatusCode = HttpStatusCode.InternalServerError });

        try
        {
            client.GetThing(5, 3, policy);
        }
        catch (RestException e)
        {
            Assert.AreEqual(HttpStatusCode.InternalServerError, e.StatusCode);
        }
    }
    */

    [TestCase(1)] // manual HandleResult
    [TestCase(2)] // PolicyExt.HandleFailedRequest
    [TestCase(3)] // ExecuteWithRetries extension
    [TestCase(4)] // not enough retries
    public void TestClientRetry(int tcase)
    {
        MockHttpServer? mhs = null;
        int n_tries = 0;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/thing/"))
            {
                if (n_tries < 4)
                {
                    resp.StatusCode = (int)HttpStatusCode.ServiceUnavailable;
                    n_tries++;
                    return;
                }

                int x = req.Url.PathAndQuery.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Skip(3).Select(int.Parse).First();
                int y = int.Parse(req.QueryString.GetValues("y")!.First());
                resp.StatusCode = (int)HttpStatusCode.OK;

                resp.WriteBody(JsonUtil.SerializeObject(new GetResponse { V = "myString", X = x, Y = y }), ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", null);

            Polly.Retry.RetryPolicy<RestResponse>? policy = null;

            switch (tcase)
            {
                case 1: policy = Policy.HandleResult<RestResponse>(r => !r.IsSuccessful).Retry(10); break;
                case 2: policy = PolicyExt.HandleFailedRequest().Retry(10); break;
                case 3: break;
                case 4: policy = PolicyExt.HandleFailedRequest().Retry(2); break;
            }

            try
            {
                var g = policy != null ?
                    client.GetThing(10, 20, policy) : client.GetThingRetry(10, 20);

                Assert.That(g, Is.Not.Null);
                Assert.That(g?.X, Is.EqualTo(10));
                Assert.That(g?.Y, Is.EqualTo(20));
                Assert.That(g?.V, Is.EqualTo("myString"));
            }
            catch (RestException e)
            {
                if (tcase != 4 || e.StatusCode != HttpStatusCode.ServiceUnavailable)
                    throw;
            }

            Assert.That(client.BeforeExecCalled);
            if (tcase != 4)
            {
                Assert.That(client.AfterExecCalled);
                Assert.That(client.AfterSuccessCalled);
                Assert.That(client.AfterFailureCalled, Is.False);
            }
            else
            {
                Assert.That(client.AfterExecCalled, Is.False);
                Assert.That(client.AfterSuccessCalled, Is.False);
                Assert.That(client.AfterFailureCalled);
            }
        }
    }

    [TestCase(1)] // manual HandleResult
    [TestCase(2)] // PolicyExt.HandleFailedRequest
    [TestCase(3)] // ExecuteWithRetries extension
    [TestCase(4)] // not enough retries
    public async Task TestClientRetryAsync(int tcase)
    {
        MockHttpServer? mhs = null;
        int n_tries = 0;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/thing/"))
            {
                if (n_tries < 4)
                {
                    resp.StatusCode = (int)HttpStatusCode.ServiceUnavailable;
                    n_tries++;
                    return;
                }

                int x = req.Url.PathAndQuery.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Skip(3).Select(int.Parse).First();
                int y = int.Parse(req.QueryString.GetValues("y")!.First());
                resp.StatusCode = (int)HttpStatusCode.OK;

                resp.WriteBody(JsonUtil.SerializeObject(new GetResponse { V = "myString", X = x, Y = y }), ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", null);

            Polly.Retry.AsyncRetryPolicy<RestResponse>? policy = null;

            switch (tcase)
            {
                case 1: policy = Policy.HandleResult<RestResponse>(r => !r.IsSuccessful).RetryAsync(10); break;
                case 2: policy = PolicyExt.HandleFailedRequest().RetryAsync(10); break;
                case 3: break;
                case 4: policy = PolicyExt.HandleFailedRequest().RetryAsync(2); break;
            }

            try
            {
                var g = policy != null ?
                    await client.GetThingAsync(10, 20, policy) : await client.GetThingRetryAsync(10, 20);

                Assert.That(g, Is.Not.Null);
                Assert.That(g?.X, Is.EqualTo(10));
                Assert.That(g?.Y, Is.EqualTo(20));
                Assert.That(g?.V, Is.EqualTo("myString"));
            }
            catch (RestException e)
            {
                if (tcase != 4 || e.StatusCode != HttpStatusCode.ServiceUnavailable)
                    throw;
            }

            Assert.That(client.BeforeExecCalled);
            if (tcase != 4)
            {
                Assert.That(client.AfterExecCalled);
                Assert.That(client.AfterSuccessCalled);
                Assert.That(client.AfterFailureCalled, Is.False);
            }
            else
            {
                Assert.That(client.AfterExecCalled, Is.False);
                Assert.That(client.AfterSuccessCalled, Is.False);
                Assert.That(client.AfterFailureCalled);
            }
        }
    }

    [Test]
    public async Task TestClientRetryableCasesAsync()
    {
        MockHttpServer? mhs = null;
        int n_tries = 0;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/thing/"))
            {
                n_tries++;

                int testCase = int.Parse(req.QueryString.GetValues("y")!.First());
                resp.StatusCode = (int) (testCase switch
                {
                    1 => HttpStatusCode.ServiceUnavailable,
                    2 => HttpStatusCode.BadGateway,
                    3 => HttpStatusCode.InternalServerError,
                    _ => throw new NotImplementedException()
                });

                if (n_tries == 3)
                    resp.StatusCode = (int)HttpStatusCode.OK;

                resp.WriteBody(JsonUtil.SerializeObject(new GetResponse { V = "myString" }), ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", null);

            Polly.Retry.AsyncRetryPolicy<RestResponse>? policy = PolicyExt.HandleFailedRetryableRequest().RetryAsync(3);

            var g = await client.GetThingAsync(10, 1, policy);
            Assert.That(g, Is.Not.Null);
            Assert.That(n_tries, Is.EqualTo(3));

            n_tries = 0;
            g = await client.GetThingAsync(10, 2, policy);
            Assert.That(g, Is.Not.Null);
            Assert.That(n_tries, Is.EqualTo(3));

            n_tries = 0;
            Assert.ThrowsAsync<RestException>(async () => await client.GetThingAsync(10, 3, policy));
        }
    }

    /*
    //this test is slow (~8s) and restsharp is deprecated
    [Test]
    public async Task TestRequestFailedToConnect()
    {
        var client = new MyApiClient("service", $"http://localhost:{MockHttpServer.GetRandomUnusedPort()}/", null);

        Polly.Retry.AsyncRetryPolicy<RestResponse>? policy = PolicyExt.HandleFailedRetryableRequest().RetryAsync(1);

        var g = await client.GetThingAsync(10, 1, policy);
    }
    */
}
