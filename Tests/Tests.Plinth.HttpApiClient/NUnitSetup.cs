using Plinth.Logging.Host;
using NUnit.Framework;

namespace Tests.Plinth.HttpApiClient;

[SetUpFixture]
public class NUnitSetup
{
    [OneTimeSetUp]
    public void Setup()
    {
        StaticLogManagerSetup.ConfigureForConsoleLogging();
    }
}
