using System.ComponentModel.DataAnnotations;
using Plinth.HttpApiClient.Validation;

// this exists to test swagger in AspNetCore test project
namespace Tests.Plinth.HttpApiClient;

/// <summary>
/// This is a test model
/// </summary>
public class TestModel
{
    /// <summary>
    /// This is a Thing
    /// </summary>
    [Required]
    public string Thing { get; set; } = null!;

    /// <summary>
    /// This is optional
    /// </summary>
    public string? OptionalThing { get; set; }

    /// <summary>
    /// Required if OptionalThing is "int"
    /// </summary>
    [RequiredIf(nameof(OptionalThing), "int")]
    public int? IntThing { get; set; }
}
