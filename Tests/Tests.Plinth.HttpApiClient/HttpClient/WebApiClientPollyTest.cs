using Plinth.Common.Constants;
using Plinth.Serialization;
using Plinth.HttpApiClient;
using NUnit.Framework;
using System.Net;
using Plinth.Common.Logging;
using Plinth.HttpApiClient.Common;
using Polly;
using Microsoft.Extensions.DependencyInjection;
using Plinth.HttpApiClient.Polly;

namespace Tests.Plinth.HttpApiClient.HttpClient;

[TestFixture]
public class WebApiClientPollyTest
{
    private class GetResponse
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string? V { get; set; }
    }

    private class EchoObject
    {
        public string? PascalCase { get; set; }
        public Dictionary<string, object>? Dict { get; set; }
        public string? RawContent { get; set; }
    }

    private class MyApiClient : BaseHttpApiClient
    {
        public bool BeforeExecCalled = false;
        public bool AfterExecCalled = false;
        public bool AfterSuccessCalled = false;
        public bool AfterFailureCalled = false;

        public MyApiClient(System.Net.Http.HttpClient httpClient)
            : base("MyApiClient", httpClient)
        {
            BeforeExec = (r) =>
            {
                BeforeExecCalled = true;
                return Task.CompletedTask;
            };

            AfterExec = (rq, rs) =>
            {
                AfterExecCalled = true;
                return Task.CompletedTask;
            };

            AfterSuccess = (rq, rs, c) =>
            {
                AfterSuccessCalled = true;
                return Task.CompletedTask;
            };

            AfterFailure = (rq, rs, e) =>
            {
                AfterFailureCalled = true;
                return Task.CompletedTask;
            };
        }

        public async Task<GetResponse?> GetThingAsync(int x, int y, int z)
        {
            return await HttpGet($"api/v1/thing/{x}/get")
                .AddQueryParameter("y", y.ToString())
                .AddQueryParameter("z", z)
                .AddQueryParameter("_null_", (int?)null)
                .ExecuteAsync<GetResponse>();
        }

        public async Task<GetResponse?> GetThing404OkAsync(int x, int y)
        {
            return await HttpGet($"api/v1/thing/{x}/get")
                .AddQueryParameter("y", y)
                .ExecuteAsync<GetResponse>(HttpStatusCode.NotFound);
        }
    }

    [Test]
    public async Task TestClientAsync()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/thing/"))
            {
                int x = req.Url.PathAndQuery.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Skip(3).Select(int.Parse).First();
                int y = int.Parse(req.QueryString.GetValues("y")!.First());
                resp.StatusCode = (int)HttpStatusCode.OK;

                resp.WriteBody(JsonUtil.SerializeObject(new GetResponse { V = "myString", X = x, Y = y }), ContentTypes.ApplicationJson);
            }
        }))
        {
            bool ok = false;

            var services = new ServiceCollection();
            var policy = Policy.HandleResult<HttpResponseMessage>(r => { ok = true; return r.StatusCode != HttpStatusCode.OK; })
                .FallbackAsync(new HttpResponseMessage { StatusCode = HttpStatusCode.BadRequest });
            services.AddHttpApiClient<MyApiClient>(policy, c =>
            {
                c.BaseAddress = new Uri($"http://localhost:{mhs.Port}/");
            });
            var sp = services.BuildServiceProvider();

            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = sp.GetRequiredService<MyApiClient>();

            var g = await client.GetThingAsync(10, 20, 30);

            Assert.That(ok);

            Assert.That(g, Is.Not.Null);
            Assert.That(g?.X, Is.EqualTo(10));
            Assert.That(g?.Y, Is.EqualTo(20));
            Assert.That(g?.V, Is.EqualTo("myString"));

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled);
            Assert.That(client.AfterFailureCalled, Is.False);
        }
    }

    [Test]
    public async Task TestClientFailure()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            resp.StatusCode = (int)HttpStatusCode.NotFound;
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var services = new ServiceCollection();
            bool tested = false;
            var policy = Policy.HandleResult<HttpResponseMessage>(r => { tested = true; return r.StatusCode == HttpStatusCode.NotFound; })
                .FallbackAsync(new HttpResponseMessage { StatusCode = HttpStatusCode.BadRequest });
            services.AddHttpApiClient<MyApiClient>(policy, c =>
            {
                c.BaseAddress = new Uri($"http://localhost:{mhs.Port}/");
            });
            var sp = services.BuildServiceProvider();

            var client = sp.GetRequiredService<MyApiClient>();
            try
            {
                await client.GetThingAsync(10, 20, 30);
                Assert.Fail();
            }
            catch (RestException e)
            {
                Assert.That(e.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            }

            Assert.That(tested);

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled, Is.False);
            Assert.That(client.AfterFailureCalled);
        }
    }

    [Test]
    public async Task TestRequestFailedToConnect()
    {
        var services = new ServiceCollection();
        var policy = HttpApiClientPollyUtil.HandleTransientHttpError()
            .FallbackAsync(new HttpResponseMessage { StatusCode = HttpStatusCode.InternalServerError });
        services.AddHttpApiClient<MyApiClient>(policy, c =>
        {
            c.BaseAddress = new Uri($"http://localhost:{MockHttpServer.GetRandomUnusedPort()}/");
        }).ConfigurePrimaryHttpMessageHandler(
            // this lets us short circuit the normal connection timeout (it is about 4 seconds otherwise)
            () => new SocketsHttpHandler() { ConnectTimeout = TimeSpan.FromMilliseconds(150) });

        var sp = services.BuildServiceProvider();

        var client = sp.GetRequiredService<MyApiClient>();

        try
        {
            await client.GetThingAsync(5, 3, 1);
            Assert.Fail();
        }
        // the normal connection timeout throws RestException
        //catch (RestException e)
        catch (Exception e) when (e is TaskCanceledException or HttpRequestException or RestException)
        {
            //Assert.AreEqual(HttpStatusCode.InternalServerError, e.StatusCode);
        }
    }

    [TestCase(1)] // manual HandleResult of status code only
    [TestCase(2)] // HttpPolicyExtensions.HandleTransientHttpError
    [TestCase(3)] // noop policy
    public async Task TestClientRetryAsync(int tcase)
    {
        MockHttpServer? mhs = null;
        int n_tries = 0;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/thing/"))
            {
                if (n_tries < 4)
                {
                    resp.StatusCode = (int)HttpStatusCode.ServiceUnavailable;
                    n_tries++;
                    return;
                }

                int x = req.Url.PathAndQuery.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Skip(3).Select(int.Parse).First();
                int y = int.Parse(req.QueryString.GetValues("y")!.First());
                resp.StatusCode = (int)HttpStatusCode.OK;

                resp.WriteBody(JsonUtil.SerializeObject(new GetResponse { V = "myString", X = x, Y = y }), ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var services = new ServiceCollection();
            IAsyncPolicy<HttpResponseMessage>? policy = tcase switch
            {
                1 => Policy.HandleResult<HttpResponseMessage>(r => !r.IsSuccessStatusCode).RetryAsync(10),
                2 => HttpApiClientPollyUtil.HandleTransientHttpError().OrResult(r => !r.IsSuccessStatusCode).RetryAsync(10),
                3 => Policy.NoOpAsync<HttpResponseMessage>(),
                _ => null
            };
            services.AddHttpApiClient<MyApiClient>(policy!, c =>
            {
                c.BaseAddress = new Uri($"http://localhost:{mhs.Port}/");
            });
            var sp = services.BuildServiceProvider();

            var client = sp.GetRequiredService<MyApiClient>();

            try
            {
                var g = await client.GetThingAsync(10, 20, 30);

                Assert.That(g, Is.Not.Null);
                Assert.That(g?.X, Is.EqualTo(10));
                Assert.That(g?.Y, Is.EqualTo(20));
                Assert.That(g?.V, Is.EqualTo("myString"));
            }
            catch (RestException e)
            {
                if (tcase != 3 || e.StatusCode != HttpStatusCode.ServiceUnavailable)
                    throw;
            }

            Assert.That(client.BeforeExecCalled);
            if (tcase != 3)
            {
                Assert.That(client.AfterExecCalled);
                Assert.That(client.AfterSuccessCalled);
                Assert.That(client.AfterFailureCalled, Is.False);
            }
            else
            {
                Assert.That(client.AfterExecCalled);
                Assert.That(client.AfterSuccessCalled, Is.False);
                Assert.That(client.AfterFailureCalled);
            }
        }
    }
}
