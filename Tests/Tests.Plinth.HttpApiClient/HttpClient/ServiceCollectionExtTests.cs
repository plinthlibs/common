using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using Plinth.HttpApiClient;

namespace Tests.Plinth.HttpApiClient.HttpClient;

[TestFixture]
public class ServiceCollectionExtTests
{
    private class ResolvedThing
    {
        public bool Value { get; set; }
    }

    private class MyApiClientSettings : BaseHttpApiClientSettings
    {
    }

    private class MyApiClient : BaseHttpApiClient, IMyApiClient
    {
        public bool BeforeExecCalled = false;
        public bool AfterExecCalled = false;
        public bool AfterSuccessCalled = false;
        public bool AfterFailureCalled = false;

        public MyApiClient(System.Net.Http.HttpClient httpClient, IOptions<MyApiClientSettings> settings)
            : base("MyApiClient", httpClient, settings)
        {
        }
    }

    public interface IMyApiClient
    {
    }

    private IConfiguration BuildConfig(Dictionary<string, string?> m)
        => new ConfigurationBuilder().AddInMemoryCollection(m).Build();

    #region from config
    [Test]
    public void TestFromConfig_Concrete()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["MyApiClient:BaseAddress"] = "http://localhost:9090/",
            ["MyApiClient:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(configuration);
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["MyApiClient:BaseAddress"]!)));
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["MyApiClient:Timeout"]!)));
    }

    [Test]
    public void TestFromConfig_Concrete_WithSp()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["MyApiClient:BaseAddress"] = "http://localhost:9090/",
            ["MyApiClient:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(configuration, (sp, h) =>
        {
            Assert.That(sp.GetRequiredService<ResolvedThing>().Value);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["MyApiClient:BaseAddress"]!)));
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["MyApiClient:Timeout"]!)));
    }

    [Test]
    public void TestFromConfig_Concrete_WithCfgClient()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["MyApiClient:BaseAddress"] = "http://localhost:9090",
            ["MyApiClient:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(configuration, c =>
        {
            c.DefaultRequestVersion = Version.Parse("1.2");
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["MyApiClient:BaseAddress"]!)));
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["MyApiClient:Timeout"]!)));
        Assert.That(client.InternalClient.DefaultRequestVersion.ToString(), Is.EqualTo("1.2"));
    }

    [Test]
    public void TestFromConfig_Interface()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["MyApiClient:BaseAddress"] = "http://localhost:9090",
            ["MyApiClient:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient, MyApiClientSettings>(configuration);
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That((client as MyApiClient)?.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["MyApiClient:BaseAddress"]!)));
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["MyApiClient:Timeout"]!)));
    }

    [Test]
    public void TestFromConfig_Interface_WithSp()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["MyApiClient:BaseAddress"] = "http://localhost:9090",
            ["MyApiClient:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient, MyApiClientSettings>(configuration, (sp, h) =>
        {
            Assert.That(sp.GetRequiredService<ResolvedThing>().Value);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That((client as MyApiClient)?.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["MyApiClient:BaseAddress"]!)));
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["MyApiClient:Timeout"]!)));
    }

    [Test]
    public void TestFromConfig_Interface_WithCfgClient()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["MyApiClient:BaseAddress"] = "http://localhost:9090",
            ["MyApiClient:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient, MyApiClientSettings>(configuration, c =>
        {
            c.DefaultRequestVersion = Version.Parse("1.2");
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That((client as MyApiClient)?.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["MyApiClient:BaseAddress"]!)));
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["MyApiClient:Timeout"]!)));
        Assert.That((client as MyApiClient)?.InternalClient.DefaultRequestVersion.ToString(), Is.EqualTo("1.2"));
    }

    [Test]
    public void TestFromConfig_Invalid_BadUrl()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["MyApiClient:BaseAddress"] = "not-a-uri",
            ["MyApiClient:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(configuration);
        var sp = services.BuildServiceProvider();
        Assert.Throws<ArgumentException>(() => sp.GetRequiredService<MyApiClient>());
    }

    [Test]
    public void TestFromConfig_Invalid_UrlHasPath()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["MyApiClient:BaseAddress"] = "http://localhost:9090/some/path",
            ["MyApiClient:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(configuration);
        var sp = services.BuildServiceProvider();
        Assert.Throws<ArgumentException>(() => sp.GetRequiredService<MyApiClient>());
    }
    #endregion

    #region from config section
    [Test]
    public void TestFromConfigSection_Concrete()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["Web:WebSettings:BaseAddress"] = "http://localhost:9090",
            ["Web:WebSettings:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(configuration.GetSection("Web:WebSettings"));
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["Web:WebSettings:BaseAddress"]!)));
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["Web:WebSettings:Timeout"]!)));
    }

    [Test]
    public void TestFromConfigSection_Concrete_WithSp()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["Web:WebSettings:BaseAddress"] = "http://localhost:9090",
            ["Web:WebSettings:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(configuration.GetSection("Web:WebSettings"), (sp, h) =>
        {
            Assert.That(sp.GetRequiredService<ResolvedThing>().Value);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["Web:WebSettings:BaseAddress"]!)));
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["Web:WebSettings:Timeout"]!)));
    }

    [Test]
    public void TestFromConfigSection_Concrete_WithCfgClient()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["Web:WebSettings:BaseAddress"] = "http://localhost:9090",
            ["Web:WebSettings:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(configuration.GetSection("Web:WebSettings"), c =>
        {
            c.DefaultRequestVersion = Version.Parse("1.2");
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["Web:WebSettings:BaseAddress"]!)));
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["Web:WebSettings:Timeout"]!)));
        Assert.That(client.InternalClient.DefaultRequestVersion.ToString(), Is.EqualTo("1.2"));
    }

    [Test]
    public void TestFromConfigSection_Interface()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["Web:WebSettings:BaseAddress"] = "http://localhost:9090",
            ["Web:WebSettings:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient, MyApiClientSettings>(configuration.GetSection("Web:WebSettings"));
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That((client as MyApiClient)?.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["Web:WebSettings:BaseAddress"]!)));
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["Web:WebSettings:Timeout"]!)));
    }

    [Test]
    public void TestFromConfigSection_Interface_WithCfgClient()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["Web:WebSettings:BaseAddress"] = "http://localhost:9090",
            ["Web:WebSettings:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient, MyApiClientSettings>(configuration.GetSection("Web:WebSettings"), c =>
        {
            c.DefaultRequestVersion = Version.Parse("1.2");
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That((client as MyApiClient)?.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["Web:WebSettings:BaseAddress"]!)));
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["Web:WebSettings:Timeout"]!)));
        Assert.That((client as MyApiClient)?.InternalClient.DefaultRequestVersion.ToString(), Is.EqualTo("1.2"));
    }

    [Test]
    public void TestFromConfigSection_Interface_WithSp()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["Web:WebSettings:BaseAddress"] = "http://localhost:9090",
            ["Web:WebSettings:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient, MyApiClientSettings>(configuration.GetSection("Web:WebSettings"), (sp, h) =>
        {
            Assert.That(sp.GetRequiredService<ResolvedThing>().Value);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That((client as MyApiClient)?.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["Web:WebSettings:BaseAddress"]!)));
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["Web:WebSettings:Timeout"]!)));
    }
    #endregion

    #region from settings action
    [Test]
    public void TestFromSettingsAction_Concrete()
    {
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>((m) =>
        {
            m.BaseAddress = "http://localhost:9090";
            m.Timeout = TimeSpan.Parse("00:00:32");
        });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client.InternalClient.BaseAddress, Is.EqualTo(new Uri("http://localhost:9090")));
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse("00:00:32")));
    }

    [Test]
    public void TestFromSettingsAction_Concrete_WithSp()
    {
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(m =>
        {
            m.BaseAddress = "http://localhost:9090";
            m.Timeout = TimeSpan.Parse("00:00:32");
        },
        (sp, h) =>
        {
            Assert.That(sp.GetRequiredService<ResolvedThing>().Value);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client.InternalClient.BaseAddress, Is.EqualTo(new Uri("http://localhost:9090")));
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse("00:00:32")));
    }

    [Test]
    public void TestFromSettingsAction_Concrete_WithCfgClient()
    {
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(m =>
        {
            m.BaseAddress = "http://localhost:9090";
            m.Timeout = TimeSpan.Parse("00:00:32");
        },
        c =>
        {
            c.DefaultRequestVersion = Version.Parse("1.2");
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client.InternalClient.BaseAddress, Is.EqualTo(new Uri("http://localhost:9090")));
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse("00:00:32")));
        Assert.That(client.InternalClient.DefaultRequestVersion.ToString(), Is.EqualTo("1.2"));
    }

    [Test]
    public void TestFromSettingsAction_Interface()
    {
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient, MyApiClientSettings>(m =>
        {
            m.BaseAddress = "http://localhost:9090";
            m.Timeout = TimeSpan.Parse("00:00:32");
        });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That((client as MyApiClient)?.InternalClient.BaseAddress, Is.EqualTo(new Uri("http://localhost:9090")));
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse("00:00:32")));
    }

    [Test]
    public void TestFromSettingsAction_Interface_WithSp()
    {
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient, MyApiClientSettings>(m =>
        {
            m.BaseAddress = "http://localhost:9090";
            m.Timeout = TimeSpan.Parse("00:00:32");
        },
        (sp, h) =>
        {
            Assert.That(sp.GetRequiredService<ResolvedThing>().Value);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That((client as MyApiClient)?.InternalClient.BaseAddress, Is.EqualTo(new Uri("http://localhost:9090")));
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse("00:00:32")));
    }

    [Test]
    public void TestFromSettingsAction_Interface_WithCfgClient()
    {
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient, MyApiClientSettings>(m =>
        {
            m.BaseAddress = "http://localhost:9090";
            m.Timeout = TimeSpan.Parse("00:00:32");
        },
        c =>
        {
            c.DefaultRequestVersion = Version.Parse("1.2");
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That((client as MyApiClient)?.InternalClient.BaseAddress, Is.EqualTo(new Uri("http://localhost:9090")));
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse("00:00:32")));
        Assert.That((client as MyApiClient)?.InternalClient.DefaultRequestVersion.ToString(), Is.EqualTo("1.2"));
    }
    #endregion

    #region from no settings
    [Test]
    public void TestNoSettingsAction_Concrete()
    {
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient>();
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client, Is.Not.Null);
    }

    [Test]
    public void TestNoSettingsAction_Concrete_WithCfgClient()
    {
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient>(
        (c) =>
        {
            c.Timeout = TimeSpan.FromSeconds(32);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client, Is.Not.Null);
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse("00:00:32")));
    }

    [Test]
    public void TestNoSettingsAction_Concrete_WithSp()
    {
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient>(
        (sp, h) =>
        {
            Assert.That(sp.GetRequiredService<ResolvedThing>().Value);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client, Is.Not.Null);
    }

    [Test]
    public void TestNoSettingsAction_Interface()
    {
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient>();
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That(client, Is.Not.Null);
    }

    [Test]
    public void TestNoSettingsAction_Interface_WithCfgClient()
    {
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient>(
        (c) =>
        {
            c.Timeout = TimeSpan.FromSeconds(32);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That(client, Is.Not.Null);
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse("00:00:32")));
    }

    [Test]
    public void TestNoSettingsAction_Interface_WithSp()
    {
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient>(
        (sp, h) =>
        {
            Assert.That(sp.GetRequiredService<ResolvedThing>().Value);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That(client, Is.Not.Null);
    }
    #endregion
}
