using Plinth.HttpApiClient;
using NUnit.Framework;
using FluentAssertions;
using Plinth.HttpApiClient.Response;
using Plinth.Common.Constants;
using Microsoft.Extensions.Options;

namespace Tests.Plinth.HttpApiClient.HttpClient;

[TestFixture]
public class RequestExtensionsTest
{
    private class TestApiClient : BaseHttpApiClient
    {
        public TestApiClient(string url)
            : base(nameof(TestApiClient), new System.Net.Http.HttpClient(), Options.Create(new BaseHttpApiClientSettings { BaseAddress = url }))
        {
        }

        public HttpRequestMessageExt New(string resource)
        {
            return HttpGet(resource);
        }

        public async Task<string?> GetRequestUrl(HttpRequestMessageExt restRequest)
        {
            return (await restRequest.ExecuteAsync()).content;
        }
    }

    [Test]
    public async Task Can_Add_Query_Parameters_Using()
    {
        using (var server = new MockHttpServer((request, response) =>
        {
            response.WriteBody(request.RawUrl ?? string.Empty, ContentTypes.TextPlain);
        }))
        {
            var client = new TestApiClient($"http://localhost:{server.Port}/");

            var limit = 25;
            var expand = true;
            var orderBy = "Last Name";
            var filterBy = ApiErrorCode.ValidationError;

            var request = client.New("search")
                .AddQueryParameter("limit", limit.ToString())
                .AddQueryParameter("expand", expand.ToString())
                .AddQueryParameter("orderBy", orderBy)
                .AddQueryParameter("filterBy", filterBy.ToString());

            (await client.GetRequestUrl(request)).Should().Be("/search?limit=25&expand=True&orderBy=Last%20Name&filterBy=ValidationError");
        }
    }

    [Test]
    public async Task Can_Add_Query_Parameters_With_String_Conversion()
    {
        using (var server = new MockHttpServer((request, response) =>
        {
            response.WriteBody(request.RawUrl ?? string.Empty, ContentTypes.TextPlain);
        }))
        {
            var client = new TestApiClient($"http://localhost:{server.Port}/");

            var limit = 25;
            var expand = true;
            var orderBy = "Last Name";
            var filterBy = ApiErrorCode.ValidationError;

            var request = client.New("search")
                .AddQueryParameter("limit", limit)
                .AddQueryParameter("expand", expand)
                .AddQueryParameter("orderBy", orderBy)
                .AddQueryParameter("filterBy", filterBy);

            (await client.GetRequestUrl(request)).Should().Be("/search?limit=25&expand=True&orderBy=Last%20Name&filterBy=ValidationError");

            string? a = null;
            int? b = null;
            ApiErrorCode? c = null;

            request = client.New("search")
                .AddQueryParameter("a", a!)
                .AddQueryParameter("b", b)
                .AddQueryParameter("c", c);

            (await client.GetRequestUrl(request)).Should().Be("/search");
        }
    }

    [Test]
    public async Task Can_Add_Optional_Query_Parameters()
    {
        using var server = new MockHttpServer((request, response) =>
        {
            response.WriteBody(request.RawUrl ?? string.Empty, ContentTypes.TextPlain);
        });

        var client = new TestApiClient($"http://localhost:{server.Port}/");

        var limit = 25;
        var expand = true;
        var orderBy = "Last Name";
        var filterBy = ApiErrorCode.ValidationError;

        HttpRequestMessageExt request() => client.New("search")
            .AddQueryOptional("limit", limit)
            .AddQueryOptional("expand", expand)
            .AddQueryOptional("orderBy", orderBy)
            .AddQueryOptional("filterBy", filterBy);

        (await client.GetRequestUrl(request())).Should().Be("/search?limit=25&expand=True&orderBy=Last%20Name&filterBy=ValidationError");

        limit = 0;
        expand = false;
        orderBy = null;
        filterBy = ApiErrorCode.ServerError;

        (await client.GetRequestUrl(request())).Should().Be("/search");
    }

    [Test]
    public async Task Check_Null_And_Repeatable_Query_Parameters()
    {
        using (var server = new MockHttpServer((request, response) =>
        {
            response.WriteBody(request.RawUrl ?? string.Empty, ContentTypes.TextPlain);
        }))
        {
            var client = new TestApiClient($"http://localhost:{server.Port}/");

            var request = client.New("search")
                .AddQueryParameter("a", (string?)null)
                .AddQueryParameter("b", (string?)null)
                .AddQueryParameter("c", (string?)null);

            (await client.GetRequestUrl(request)).Should().Be("/search");

            request = client.New("search")
                .AddQueryParameter("a", "1")
                .AddQueryParameter("b", "2")
                .AddQueryParameter("a", "3")
                .AddQueryParameter("b", (string?)null)
                .AddQueryParameter("c", (string?)null)
                .AddQueryParameter("c", (string?)null);

            (await client.GetRequestUrl(request)).Should().Be("/search?a=1&b=2&a=3");
        }
    }

    [Test]
    public async Task Can_Add_List_Of_Query_Params()
    {
        using (var server = new MockHttpServer((request, response) =>
        {
            response.WriteBody(request.RawUrl ?? string.Empty, ContentTypes.TextPlain);
        }))
        {
            var client = new TestApiClient($"http://localhost:{server.Port}/");

            var request = client.New("search")
                .AddQueryParameter("a", new List<int?> { null, null });

            (await client.GetRequestUrl(request)).Should().Be("/search");

            request = client.New("search")
                .AddQueryParameter("a", new List<int?> { 3, 86, 9 });

            (await client.GetRequestUrl(request)).Should().Be("/search?a=3&a=86&a=9");

            request = client.New("search")
                .AddQueryParameter("a", new List<int?> { 3, 86, 9 }.Select(i => i * 2));

            (await client.GetRequestUrl(request)).Should().Be("/search?a=6&a=172&a=18");

            request = client.New("search")
                .AddQueryParameter<string?>("a", (string?)null);

            (await client.GetRequestUrl(request)).Should().Be("/search");

            request = client.New("search")
                .AddQueryOptional("a", (string?)null);

            (await client.GetRequestUrl(request)).Should().Be("/search");

            request = client.New("search")
                .AddQueryParameter("a", ["o", "p", "q"]);

            (await client.GetRequestUrl(request)).Should().Be("/search?a=o&a=p&a=q");

            request = client.New("search")
                .AddQueryParameter("a", new List<string> { "o", "p", "q" }.Select(s => s + "1"));

            (await client.GetRequestUrl(request)).Should().Be("/search?a=o1&a=p1&a=q1");
        }
    }

    // borrowed from https://github.com/aspnet/HttpAbstractions/blob/master/test/Microsoft.AspNetCore.WebUtilities.Tests/QueryHelpersTests.cs
    // tweaked for server use cases
    [TestCase("/", "/?hello=world")]
    [TestCase("/someaction", "/someaction?hello=world")]
    [TestCase("/someaction?q=test", "/someaction?q=test&hello=world")]
    [TestCase("/someaction?q=test#anchor", "/someaction?q=test&hello=world")]
    [TestCase("/someaction#anchor", "/someaction?hello=world")]
    [TestCase("/#anchor", "/?hello=world")]
    [TestCase("/someaction?q=test#anchor?value", "/someaction?q=test&hello=world")]
    [TestCase("/someaction#anchor?stuff", "/someaction?hello=world")]
    [TestCase("/someaction?name?something", "/someaction?name?something&hello=world")]
    [TestCase("/someaction#name#something", "/someaction?hello=world")]
    public async Task AddQueryStringWithKeyAndValue(string uri, string expectedUri)
    {
        using (var server = new MockHttpServer((request, response) =>
        {
            response.WriteBody(request.RawUrl ?? string.Empty, ContentTypes.TextPlain);
        }))
        {
            var client = new TestApiClient("http://contoso.com");

            var request = client.New($"http://localhost:{server.Port}{uri}")
                .AddQueryParameter("hello", "world");

            (await client.GetRequestUrl(request)).Should().Be(expectedUri);
        }
    }
}
