using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using Plinth.HttpApiClient;
using Plinth.HttpApiClient.Polly;
using Polly;
using Polly.Timeout;
using System.Net;

namespace Tests.Plinth.HttpApiClient.HttpClient;

[TestFixture]
public class PollyServiceCollectionExtTests
{
    private class ResolvedThing
    {
        public bool Value { get; set; }
    }

    private class MyApiClientSettings : BaseHttpApiClientSettings
    {
    }

    private class MyApiClient : BaseHttpApiClient, IMyApiClient
    {
        public bool BeforeExecCalled = false;
        public bool AfterExecCalled = false;
        public bool AfterSuccessCalled = false;
        public bool AfterFailureCalled = false;

        public MyApiClient(System.Net.Http.HttpClient httpClient, IOptions<MyApiClientSettings> settings)
            : base("MyApiClient", httpClient, settings)
        {
        }

        public async Task<HttpStatusCode> CallIt()
        {
            var r = await HttpGet("/thing").ExecuteAsync(HttpStatusCode.NotImplemented);
            return r.response.StatusCode;
        }

        public async Task<HttpStatusCode> CallItFull()
        {
            var r = await HttpGet($"http://localhost:{MockHttpServer.GetRandomUnusedPort()}/thing").ExecuteAsync(HttpStatusCode.NotImplemented);
            return r.response.StatusCode;
        }
    }

    public interface IMyApiClient
    {
        Task<HttpStatusCode> CallIt();
        Task<HttpStatusCode> CallItFull();
    }

    private IConfiguration BuildConfig(Dictionary<string, string?> m)
        => new ConfigurationBuilder().AddInMemoryCollection(m).Build();

    private static readonly IAsyncPolicy<HttpResponseMessage> _policy =
        HttpApiClientPollyUtil.HandleTransientHttpError()
            .Or<TimeoutRejectedException>()
            .FallbackAsync(new HttpResponseMessage { StatusCode = HttpStatusCode.NotImplemented })
            .CreateTimeoutPolicy(TimeSpan.FromMilliseconds(25));

    #region from config
    [Test]
    public async Task TestFromConfig_Concrete()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["MyApiClient:BaseAddress"] = $"http://localhost:{MockHttpServer.GetRandomUnusedPort()}/",
            ["MyApiClient:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(configuration, _policy);
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["MyApiClient:BaseAddress"]!)));
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["MyApiClient:Timeout"]!)));

        Assert.That(await client.CallIt(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestFromConfig_Concrete_WithSp()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["MyApiClient:BaseAddress"] = $"http://localhost:{MockHttpServer.GetRandomUnusedPort()}/",
            ["MyApiClient:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(configuration, _policy, (sp, h) =>
        {
            Assert.That(sp.GetRequiredService<ResolvedThing>().Value);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["MyApiClient:BaseAddress"]!)));
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["MyApiClient:Timeout"]!)));
        Assert.That(await client.CallIt(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestFromConfig_Concrete_WithCfgClient()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["MyApiClient:BaseAddress"] = $"http://localhost:{MockHttpServer.GetRandomUnusedPort()}",
            ["MyApiClient:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(configuration, _policy, c =>
        {
            c.DefaultRequestVersion = Version.Parse("1.2");
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["MyApiClient:BaseAddress"]!)));
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["MyApiClient:Timeout"]!)));
        Assert.That(client.InternalClient.DefaultRequestVersion.ToString(), Is.EqualTo("1.2"));
        Assert.That(await client.CallIt(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestFromConfig_Interface()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["MyApiClient:BaseAddress"] = $"http://localhost:{MockHttpServer.GetRandomUnusedPort()}",
            ["MyApiClient:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient, MyApiClientSettings>(configuration, _policy);
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That((client as MyApiClient)?.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["MyApiClient:BaseAddress"]!)));
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["MyApiClient:Timeout"]!)));
        Assert.That(await client.CallIt(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestFromConfig_Interface_WithSp()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["MyApiClient:BaseAddress"] = $"http://localhost:{MockHttpServer.GetRandomUnusedPort()}",
            ["MyApiClient:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient, MyApiClientSettings>(configuration, _policy, (sp, h) =>
        {
            Assert.That(sp.GetRequiredService<ResolvedThing>().Value);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That((client as MyApiClient)?.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["MyApiClient:BaseAddress"]!)));
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["MyApiClient:Timeout"]!)));
        Assert.That(await client.CallIt(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestFromConfig_Interface_WithCfgClient()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["MyApiClient:BaseAddress"] = $"http://localhost:{MockHttpServer.GetRandomUnusedPort()}",
            ["MyApiClient:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient, MyApiClientSettings>(configuration, _policy, c =>
        {
            c.DefaultRequestVersion = Version.Parse("1.2");
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That((client as MyApiClient)?.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["MyApiClient:BaseAddress"]!)));
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["MyApiClient:Timeout"]!)));
        Assert.That((client as MyApiClient)?.InternalClient.DefaultRequestVersion.ToString(), Is.EqualTo("1.2"));
        Assert.That(await client.CallIt(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public void TestFromConfig_Invalid_BadUrl()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["MyApiClient:BaseAddress"] = "not-a-uri",
            ["MyApiClient:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(configuration, _policy);
        var sp = services.BuildServiceProvider();
        Assert.Throws<ArgumentException>(() => sp.GetRequiredService<MyApiClient>());
    }

    [Test]
    public void TestFromConfig_Invalid_UrlHasPath()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["MyApiClient:BaseAddress"] = "http://localhost:9090/some/path",
            ["MyApiClient:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(configuration, _policy);
        var sp = services.BuildServiceProvider();
        Assert.Throws<ArgumentException>(() => sp.GetRequiredService<MyApiClient>());
    }
    #endregion

    #region from config section
    [Test]
    public async Task TestFromConfigSection_Concrete()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["Web:WebSettings:BaseAddress"] = $"http://localhost:{MockHttpServer.GetRandomUnusedPort()}",
            ["Web:WebSettings:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(configuration.GetSection("Web:WebSettings"), _policy);
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["Web:WebSettings:BaseAddress"]!)));
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["Web:WebSettings:Timeout"]!)));
        Assert.That(await client.CallIt(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestFromConfigSection_Concrete_WithSp()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["Web:WebSettings:BaseAddress"] = $"http://localhost:{MockHttpServer.GetRandomUnusedPort()}",
            ["Web:WebSettings:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(configuration.GetSection("Web:WebSettings"), _policy, (sp, h) =>
        {
            Assert.That(sp.GetRequiredService<ResolvedThing>().Value);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["Web:WebSettings:BaseAddress"]!)));
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["Web:WebSettings:Timeout"]!)));
        Assert.That(await client.CallIt(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestFromConfigSection_Concrete_WithCfgClient()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["Web:WebSettings:BaseAddress"] = $"http://localhost:{MockHttpServer.GetRandomUnusedPort()}",
            ["Web:WebSettings:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(configuration.GetSection("Web:WebSettings"), _policy, c =>
        {
            c.DefaultRequestVersion = Version.Parse("1.2");
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["Web:WebSettings:BaseAddress"]!)));
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["Web:WebSettings:Timeout"]!)));
        Assert.That(client.InternalClient.DefaultRequestVersion.ToString(), Is.EqualTo("1.2"));
        Assert.That(await client.CallIt(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestFromConfigSection_Interface()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["Web:WebSettings:BaseAddress"] = $"http://localhost:{MockHttpServer.GetRandomUnusedPort()}",
            ["Web:WebSettings:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient, MyApiClientSettings>(configuration.GetSection("Web:WebSettings"), _policy);
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That((client as MyApiClient)?.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["Web:WebSettings:BaseAddress"]!)));
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["Web:WebSettings:Timeout"]!)));
        Assert.That(await client.CallIt(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestFromConfigSection_Interface_WithCfgClient()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["Web:WebSettings:BaseAddress"] = $"http://localhost:{MockHttpServer.GetRandomUnusedPort()}",
            ["Web:WebSettings:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient, MyApiClientSettings>(configuration.GetSection("Web:WebSettings"), _policy, c =>
        {
            c.DefaultRequestVersion = Version.Parse("1.2");
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That((client as MyApiClient)?.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["Web:WebSettings:BaseAddress"]!)));
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["Web:WebSettings:Timeout"]!)));
        Assert.That((client as MyApiClient)?.InternalClient.DefaultRequestVersion.ToString(), Is.EqualTo("1.2"));
        Assert.That(await client.CallIt(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestFromConfigSection_Interface_WithSp()
    {
        var inMemorySettings = new Dictionary<string, string?>
        {
            ["Web:WebSettings:BaseAddress"] = $"http://localhost:{MockHttpServer.GetRandomUnusedPort()}",
            ["Web:WebSettings:Timeout"] = "00:00:32",
        };

        var configuration = BuildConfig(inMemorySettings);
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient, MyApiClientSettings>(configuration.GetSection("Web:WebSettings"), _policy, (sp, h) =>
        {
            Assert.That(sp.GetRequiredService<ResolvedThing>().Value);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That((client as MyApiClient)?.InternalClient.BaseAddress, Is.EqualTo(new Uri(inMemorySettings["Web:WebSettings:BaseAddress"]!)));
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse(inMemorySettings["Web:WebSettings:Timeout"]!)));
        Assert.That(await client.CallIt(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }
    #endregion

    #region from settings action
    [Test]
    public async Task TestFromSettingsAction_Concrete()
    {
        var services = new ServiceCollection();
        int port = MockHttpServer.GetRandomUnusedPort();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>((m) =>
        {
            m.BaseAddress = $"http://localhost:{port}";
            m.Timeout = TimeSpan.Parse("00:00:32");
        }, _policy);
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client.InternalClient.BaseAddress, Is.EqualTo(new Uri($"http://localhost:{port}")));
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse("00:00:32")));
        Assert.That(await client.CallIt(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestFromSettingsAction_Concrete_WithSp()
    {
        var services = new ServiceCollection();
        int port = MockHttpServer.GetRandomUnusedPort();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(m =>
        {
            m.BaseAddress = $"http://localhost:{port}";
            m.Timeout = TimeSpan.Parse("00:00:32");
        }, _policy,
        (sp, h) =>
        {
            Assert.That(sp.GetRequiredService<ResolvedThing>().Value);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client.InternalClient.BaseAddress, Is.EqualTo(new Uri($"http://localhost:{port}")));
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse("00:00:32")));
        Assert.That(await client.CallIt(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestFromSettingsAction_Concrete_WithCfgClient()
    {
        var services = new ServiceCollection();
        int port = MockHttpServer.GetRandomUnusedPort();
        services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(m =>
        {
            m.BaseAddress = $"http://localhost:{port}";
            m.Timeout = TimeSpan.Parse("00:00:32");
        }, _policy,
        c =>
        {
            c.DefaultRequestVersion = Version.Parse("1.2");
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client.InternalClient.BaseAddress, Is.EqualTo(new Uri($"http://localhost:{port}")));
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse("00:00:32")));
        Assert.That(client.InternalClient.DefaultRequestVersion.ToString(), Is.EqualTo("1.2"));
        Assert.That(await client.CallIt(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestFromSettingsAction_Interface()
    {
        var services = new ServiceCollection();
        int port = MockHttpServer.GetRandomUnusedPort();
        services.AddHttpApiClient<IMyApiClient, MyApiClient, MyApiClientSettings>(m =>
        {
            m.BaseAddress = $"http://localhost:{port}";
            m.Timeout = TimeSpan.Parse("00:00:32");
        }, _policy);
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That((client as MyApiClient)?.InternalClient.BaseAddress, Is.EqualTo(new Uri($"http://localhost:{port}")));
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse("00:00:32")));
        Assert.That(await client.CallIt(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestFromSettingsAction_Interface_WithSp()
    {
        var services = new ServiceCollection();
        int port = MockHttpServer.GetRandomUnusedPort();
        services.AddHttpApiClient<IMyApiClient, MyApiClient, MyApiClientSettings>(m =>
        {
            m.BaseAddress = $"http://localhost:{port}";
            m.Timeout = TimeSpan.Parse("00:00:32");
        }, _policy,
        (sp, h) =>
        {
            Assert.That(sp.GetRequiredService<ResolvedThing>().Value);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That((client as MyApiClient)?.InternalClient.BaseAddress, Is.EqualTo(new Uri($"http://localhost:{port}")));
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse("00:00:32")));
        Assert.That(await client.CallIt(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestFromSettingsAction_Interface_WithCfgClient()
    {
        var services = new ServiceCollection();
        int port = MockHttpServer.GetRandomUnusedPort();
        services.AddHttpApiClient<IMyApiClient, MyApiClient, MyApiClientSettings>(m =>
        {
            m.BaseAddress = $"http://localhost:{port}";
            m.Timeout = TimeSpan.Parse("00:00:32");
        }, _policy,
        c =>
        {
            c.DefaultRequestVersion = Version.Parse("1.2");
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That((client as MyApiClient)?.InternalClient.BaseAddress, Is.EqualTo(new Uri($"http://localhost:{port}")));
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse("00:00:32")));
        Assert.That((client as MyApiClient)?.InternalClient.DefaultRequestVersion.ToString(), Is.EqualTo("1.2"));
        Assert.That(await client.CallIt(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }
    #endregion

    #region from no settings
    [Test]
    public async Task TestNoSettingsAction_Concrete()
    {
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient>(_policy);
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client, Is.Not.Null);
        Assert.That(await client.CallItFull(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestNoSettingsAction_Concrete_WithCfgClient()
    {
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient>(_policy,
        (c) =>
        {
            c.Timeout = TimeSpan.FromSeconds(32);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client, Is.Not.Null);
        Assert.That(client.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse("00:00:32")));
        Assert.That(await client.CallItFull(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestNoSettingsAction_Concrete_WithSp()
    {
        var services = new ServiceCollection();
        services.AddHttpApiClient<MyApiClient>(_policy,
        (sp, h) =>
        {
            Assert.That(sp.GetRequiredService<ResolvedThing>().Value);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<MyApiClient>();

        Assert.That(client, Is.Not.Null);
        Assert.That(await client.CallItFull(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestNoSettingsAction_Interface()
    {
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient>(_policy);
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That(client, Is.Not.Null);
        Assert.That(await client.CallItFull(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestNoSettingsAction_Interface_WithCfgClient()
    {
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient>(_policy,
        (c) =>
        {
            c.Timeout = TimeSpan.FromSeconds(32);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That(client, Is.Not.Null);
        Assert.That((client as MyApiClient)?.InternalClient.Timeout, Is.EqualTo(TimeSpan.Parse("00:00:32")));
        Assert.That(await client.CallItFull(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }

    [Test]
    public async Task TestNoSettingsAction_Interface_WithSp()
    {
        var services = new ServiceCollection();
        services.AddHttpApiClient<IMyApiClient, MyApiClient>(_policy,
        (sp, h) =>
        {
            Assert.That(sp.GetRequiredService<ResolvedThing>().Value);
        });
        services.AddSingleton(new ResolvedThing { Value = true });
        var sp = services.BuildServiceProvider();
        var client = sp.GetRequiredService<IMyApiClient>();

        Assert.That(client, Is.Not.Null);
        Assert.That(await client.CallItFull(), Is.EqualTo(HttpStatusCode.NotImplemented));
    }
    #endregion
}
