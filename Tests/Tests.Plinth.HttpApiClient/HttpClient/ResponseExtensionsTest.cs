﻿using System.Net;
using Plinth.HttpApiClient;
using NUnit.Framework;

namespace Tests.Plinth.HttpApiClient.HttpClient;

[TestFixture]
class ResponseExtensionsTest
{
    [Test]
    public void Test_IsSuccess_Expected()
    {
        var r = new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        Assert.That(r.IsSuccess(HttpStatusCode.OK));

        for (int i = 300; i < 600; i++)
        {
            r.StatusCode = (HttpStatusCode)i;
            Assert.That(r.IsSuccess(HttpStatusCode.OK, HttpStatusCode.Continue), Is.False);
        }

        r.StatusCode = HttpStatusCode.NotFound;
        Assert.That(r.IsSuccess(HttpStatusCode.OK, HttpStatusCode.NotFound, HttpStatusCode.NotAcceptable));
    }
}
