using Plinth.Common.Constants;
using Plinth.Serialization;
using NUnit.Framework;
using System.Net;
using Plinth.Common.Logging;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Http.Headers;
using Plinth.HttpApiClient.Handler;

#if !NET9_0_OR_GREATER
using Convert = Plinth.Common.Utils.BackPort.PlinthConvert;
#endif

namespace Tests.Plinth.HttpApiClient.HttpClient;

[TestFixture]
public class WebApiHandlerClientTest
{
    private class GetResponse
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }
        public string? V { get; set; }
    }

    private class EchoObject
    {
        public string? PascalCase { get; set; }
        public Dictionary<string, object>? Dict { get; set; }
        public string? RawContent { get; set; }
    }

    private interface ITestClient
    {
        Task<EchoObject?> DeleteEcho(EchoObject o);
        Task<EchoObject?> Echo(EchoObject o);
        Task<byte[]> GetBytes();
        Task<string?> GetString();
        Task<GetResponse?> GetThing(int x, int y, int z);
        Task<GetResponse?> GetThing404Ok(int x, int y);
        Task<GetResponse?> GetThingHeaders(int x, int y, int z, string scheme = "Bearer");
        Task<GetResponse?> GetThingFullUrl(int port, int x, int y, int z);
        Task<EchoObject?> PatchEcho(EchoObject o);
        Task<EchoObject?> PutEcho(EchoObject o);
    }

    private class MyApiClient : ITestClient
    {
        private readonly System.Net.Http.HttpClient _http;

        public MyApiClient(System.Net.Http.HttpClient client)
        {
            _http = client;
        }

        public async Task<GetResponse?> GetThing(int x, int y, int z)
        {
            return JsonUtil.DeserializeObject<GetResponse>(
                await _http.GetStringAsync($"api/v1/thing/{x}/get?y={y}&z={z}&_null_={(int?)null}"));
        }

        public async Task<GetResponse?> GetThingHeaders(int x, int y, int z, string scheme)
        {
            var req = new HttpRequestMessage(HttpMethod.Get, $"api/v1/thing/{x}/get?y={y}&z={z}");
            req.Headers.Authorization = new AuthenticationHeaderValue(scheme, "token1234");

            req.Headers.Accept.Clear();
            req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentTypes.ApplicationVndMsExcel));

            req.Headers.Add("TestHeader1", "Header1Val");
            req.Headers.Add("TestHeader2", (string?)null);

            return JsonUtil.DeserializeObject<GetResponse>(
                await (await _http.SendAsync(req)).Content.ReadAsStringAsync());
        }

        public async Task<GetResponse?> GetThingFullUrl(int port, int x, int y, int z)
        {
            return JsonUtil.DeserializeObject<GetResponse>(
                await _http.GetStringAsync($"http://localhost:{port}/api/v1/thing/{x}/get?y={y}&z={z}"));
        }

        public async Task<GetResponse?> GetThing404Ok(int x, int y)
        {
            var resp = await _http.GetAsync($"api/v1/thing/{x}/get?y={y}");
            if (resp.StatusCode == HttpStatusCode.NotFound)
                return null;
            return JsonUtil.DeserializeObject<GetResponse>(await resp.Content.ReadAsStringAsync());
        }

        public async Task<EchoObject?> Echo(EchoObject o)
        {
            var r = await _http.PostAsync("api/v1/echo", new StringContent(JsonUtil.SerializeObject(o)));
            return JsonUtil.DeserializeObject<EchoObject>(await r.Content.ReadAsStringAsync());
        }

        public async Task<EchoObject?> PutEcho(EchoObject o)
        {
            var r = await _http.PutAsync("api/v1/echo", new StringContent(JsonUtil.SerializeObject(o)));
            return JsonUtil.DeserializeObject<EchoObject>(await r.Content.ReadAsStringAsync());
        }

        public async Task<EchoObject?> PatchEcho(EchoObject o)
        {
            var r = await _http.PatchAsync("api/v1/echo", new StringContent(JsonUtil.SerializeObject(o)));
            return JsonUtil.DeserializeObject<EchoObject>(await r.Content.ReadAsStringAsync());
        }

        public async Task<EchoObject?> DeleteEcho(EchoObject o)
        {
            // technically, servers are supposed to ignore delete bodies
            var req = new HttpRequestMessage(HttpMethod.Delete, "api/v1/echo")
            {
                Content = new StringContent(JsonUtil.SerializeObject(o))
            };
            var r = await _http.SendAsync(req);
            return JsonUtil.DeserializeObject<EchoObject>(await r.Content.ReadAsStringAsync());
        }

        public async Task<string?> GetString()
        {
            return await _http.GetStringAsync("api/v1/string");
        }

        public async Task<byte[]> GetBytes()
        {
            return await (await _http.SendAsync(new HttpRequestMessage(HttpMethod.Post, "api/v1/bytes"))).Content.ReadAsByteArrayAsync();
        }
    }

    private static ITestClient CreateClient(Action<System.Net.Http.HttpClient>? c = null, Action<HttpApiClientHandlerSettings>? s = null, Action<IHttpClientBuilder>? b = null)
    {
        var services = new ServiceCollection();
        var builder = services.AddHttpApiHandlerClient<ITestClient, MyApiClient>("MyApi",
            configureClient: c,
            configureSettings: s
        );
        b?.Invoke(builder);
        var sp = services.BuildServiceProvider();
        return sp.GetRequiredService<ITestClient>();
    }

    [Test]
    public async Task TestClient()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/thing/"))
            {
                int x = req.Url.PathAndQuery.Split(['/'], StringSplitOptions.RemoveEmptyEntries).Skip(3).Select(int.Parse).First();
                int y = int.Parse(req.QueryString.GetValues("y")!.First());
                int z = int.Parse(req.QueryString.GetValues("z")!.First());

                resp.StatusCode = (int)HttpStatusCode.OK;
                resp.WriteBody(JsonUtil.SerializeObject(new GetResponse { V = "myString", X = x, Y = y, Z = z }), ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();

            bool BeforeExecCalled = false, AfterExecCalled = false, AfterSuccessCalled = false, AfterFailureCalled = false;
            var client = CreateClient(c =>
                {
                    c.BaseAddress = new Uri($"http://localhost:{mhs.Port}/");
                },
                s =>
                {
                    s.BeforeExec = (r) => { BeforeExecCalled = true; return Task.CompletedTask; };
                    s.AfterExec = (rq, rs) => { AfterExecCalled = true; return Task.CompletedTask; };
                    s.AfterSuccess = (rq, rs, ct) => { AfterSuccessCalled = true; return Task.CompletedTask; };
                    s.AfterFailure = (rq, rs, ex) => { AfterFailureCalled = true; return Task.CompletedTask; };
                });

            var g = await client.GetThing(10, 20, 30);

            Assert.That(g, Is.Not.Null);
            Assert.That(g?.X, Is.EqualTo(10));
            Assert.That(g?.Y, Is.EqualTo(20));
            Assert.That(g?.Z, Is.EqualTo(30));
            Assert.That(g?.V, Is.EqualTo("myString"));

            Assert.That(BeforeExecCalled);
            Assert.That(AfterExecCalled);
            Assert.That(AfterSuccessCalled);
            Assert.That(AfterFailureCalled, Is.False);

            g = await client.GetThingFullUrl(mhs.Port, 10, 20, 30);

            Assert.That(g, Is.Not.Null);
            Assert.That(g?.X, Is.EqualTo(10));
            Assert.That(g?.Y, Is.EqualTo(20));
            Assert.That(g?.Z, Is.EqualTo(30));
            Assert.That(g?.V, Is.EqualTo("myString"));
        }
    }

    [Test]
    public async Task TestClientNoBaseUrl()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/thing/"))
            {
                int x = req.Url.PathAndQuery.Split(['/'], StringSplitOptions.RemoveEmptyEntries).Skip(3).Select(int.Parse).First();
                int y = int.Parse(req.QueryString.GetValues("y")!.First());
                int z = int.Parse(req.QueryString.GetValues("z")!.First());

                resp.StatusCode = (int)HttpStatusCode.OK;
                resp.WriteBody(JsonUtil.SerializeObject(new GetResponse { V = "myString", X = x, Y = y, Z = z }), ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();

            var client = CreateClient();

            var g = await client.GetThingFullUrl(mhs.Port, 10, 20, 30);

            Assert.That(g, Is.Not.Null);
            Assert.That(g?.X, Is.EqualTo(10));
            Assert.That(g?.Y, Is.EqualTo(20));
            Assert.That(g?.Z, Is.EqualTo(30));
            Assert.That(g?.V, Is.EqualTo("myString"));
        }
    }

    [Test]
    public async Task TestClientHeaders()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/thing/"))
            {
                int x = req.Url.PathAndQuery.Split(['/'], StringSplitOptions.RemoveEmptyEntries).Skip(3).Select(int.Parse).First();
                int y = int.Parse(req.QueryString.GetValues("y")!.First());
                int z = int.Parse(req.QueryString.GetValues("z")!.First());

                Assert.That(req.Headers["TestHeader1"], Is.EqualTo("Header1Val"));
                Assert.That(req.Headers["TestHeader2"], Is.Empty);

                if (z == 30)
                    Assert.That(req.Headers["Authorization"], Is.EqualTo("Bearer token1234"));
                else if (z == 40)
                    Assert.That(req.Headers["Authorization"], Is.EqualTo("Basic token1234"));
                Assert.That(req.Headers["Accept"], Is.EqualTo(ContentTypes.ApplicationVndMsExcel));

                resp.StatusCode = (int)HttpStatusCode.OK;
                resp.WriteBody(JsonUtil.SerializeObject(new GetResponse { V = "myString", X = x, Y = y, Z = z }), ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();

            var client = CreateClient(c => c.BaseAddress = new Uri($"http://localhost:{mhs.Port}/"));

            var g = await client.GetThingHeaders(10, 20, 30);

            Assert.That(g, Is.Not.Null);
            Assert.That(g?.X, Is.EqualTo(10));
            Assert.That(g?.Y, Is.EqualTo(20));
            Assert.That(g?.Z, Is.EqualTo(30));
            Assert.That(g?.V, Is.EqualTo("myString"));

            g = await client.GetThingHeaders(10, 20, 40, "Basic");

            Assert.That(g, Is.Not.Null);
            Assert.That(g?.X, Is.EqualTo(10));
            Assert.That(g?.Y, Is.EqualTo(20));
            Assert.That(g?.Z, Is.EqualTo(40));
            Assert.That(g?.V, Is.EqualTo("myString"));
        }
    }

    [Test]
    public async Task TestClientFailure()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            int y = int.Parse(req.QueryString.GetValues("y")!.First());
            if (y == 20)
                resp.StatusCode = (int)HttpStatusCode.NotFound;
            else
            {
                resp.StatusCode = (int)HttpStatusCode.OK;
                resp.WriteBody("{}", ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();

            bool BeforeExecCalled = false, AfterExecCalled = false, AfterSuccessCalled = false, AfterFailureCalled = false;
            var client = CreateClient(c =>
                {
                    c.BaseAddress = new Uri($"http://localhost:{mhs.Port}/");
                },
                s =>
                {
                    s.BeforeExec = (r) => { BeforeExecCalled = true; return Task.CompletedTask; };
                    s.AfterExec = (rq, rs) => { AfterExecCalled = true; return Task.CompletedTask; };
                    s.AfterSuccess = (rq, rs, ct) => { AfterSuccessCalled = true; return Task.CompletedTask; };
                    s.AfterFailure = (rq, rs, ex) => { AfterFailureCalled = true; return Task.CompletedTask; };
                });

            try
            {
                await client.GetThing(10, 20, 30);
                Assert.Fail();
            }
            catch (HttpRequestException e)
            {
                Assert.That(e.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
            }

            Assert.That(BeforeExecCalled);
            Assert.That(AfterExecCalled);
            Assert.That(AfterSuccessCalled, Is.False);
            Assert.That(AfterFailureCalled);

            BeforeExecCalled = false;
            AfterExecCalled = false;
            AfterFailureCalled = false;

            client = CreateClient(c =>
                {
                    c.BaseAddress = new Uri($"http://localhost:{mhs.Port}/");
                },
                s =>
                {
                    s.BeforeExec = (r) => { BeforeExecCalled = true; return Task.CompletedTask; };
                    s.AfterExec = (rq, rs) => { AfterExecCalled = true; return Task.CompletedTask; };
                    s.AfterSuccess = (rq, rs, ct) => { AfterSuccessCalled = true; return Task.CompletedTask; };
                    s.AfterFailure = (rq, rs, ex) => { AfterFailureCalled = true; return Task.CompletedTask; };
                    s.IsSuccess = (rq, rs) => Task.FromResult(rs.IsSuccessStatusCode || rs.StatusCode == HttpStatusCode.NotFound);
                });

            // status codes
            var g = await client.GetThing404Ok(10, 20);
            Assert.That(g, Is.Null);

            Assert.That(BeforeExecCalled);
            Assert.That(AfterExecCalled);
            Assert.That(AfterSuccessCalled);
            Assert.That(AfterFailureCalled, Is.False);

            g = await client.GetThing404Ok(10, 40);
            Assert.That(g, Is.Not.Null);
        }
    }

    [Test]
    public async Task TestClientFailureWithMessage()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            resp.StatusCode = (int)HttpStatusCode.InternalServerError;
            resp.WriteBody(JsonUtil.SerializeObject(new { Message = "FailedToThing", OtherThing = 7 }), ContentTypes.ApplicationJson);
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            bool BeforeExecCalled = false, AfterExecCalled = false, AfterSuccessCalled = false, AfterFailureCalled = false;
            var client = CreateClient(c =>
                {
                    c.BaseAddress = new Uri($"http://localhost:{mhs.Port}/");
                },
                s =>
                {
                    s.BeforeExec = (r) => { BeforeExecCalled = true; return Task.CompletedTask; };
                    s.AfterExec = (rq, rs) => { AfterExecCalled = true; return Task.CompletedTask; };
                    s.AfterSuccess = (rq, rs, ct) => { AfterSuccessCalled = true; return Task.CompletedTask; };
                    s.AfterFailure = (rq, rs, ex) => { AfterFailureCalled = true; return Task.CompletedTask; };
                });

            try
            {
                await client.GetThing(10, 20, 30);
                Assert.Fail();
            }
            catch (HttpRequestException e)
            {
                Assert.That(e.StatusCode, Is.EqualTo(HttpStatusCode.InternalServerError));
                Assert.That(e.Message, Is.EqualTo("Response status code does not indicate success: 500 (Internal Server Error)."));
            }

            Assert.That(BeforeExecCalled);
            Assert.That(AfterExecCalled);
            Assert.That(AfterSuccessCalled, Is.False);
            Assert.That(AfterFailureCalled);
        }
    }

    [Test]
    public async Task TestClientFailureWithInvalidMessage()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            resp.StatusCode = (int)HttpStatusCode.InternalServerError;
            resp.WriteBody("a thing failed", ContentTypes.TextPlain);
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            bool BeforeExecCalled = false, AfterExecCalled = false, AfterSuccessCalled = false, AfterFailureCalled = false;
            var client = CreateClient(c =>
                {
                    c.BaseAddress = new Uri($"http://localhost:{mhs.Port}/");
                },
                s =>
                {
                    s.BeforeExec = (r) => { BeforeExecCalled = true; return Task.CompletedTask; };
                    s.AfterExec = (rq, rs) => { AfterExecCalled = true; return Task.CompletedTask; };
                    s.AfterSuccess = (rq, rs, ct) => { AfterSuccessCalled = true; return Task.CompletedTask; };
                    s.AfterFailure = (rq, rs, ex) => { AfterFailureCalled = true; return Task.CompletedTask; };
                });
            try
            {
                await client.GetThing(10, 20, 30);
                Assert.Fail();
            }
            catch (HttpRequestException e)
            {
                Assert.That(e.StatusCode, Is.EqualTo(HttpStatusCode.InternalServerError));
                Assert.That(e.Message, Is.EqualTo("Response status code does not indicate success: 500 (Internal Server Error)."));
            }

            Assert.That(BeforeExecCalled);
            Assert.That(AfterExecCalled);
            Assert.That(AfterSuccessCalled, Is.False);
            Assert.That(AfterFailureCalled);
        }
    }

    [Test]
    public async Task TestEmptyResult()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            resp.StatusCode = (int)HttpStatusCode.OK;
            resp.WriteBody(string.Empty, ContentTypes.ApplicationJson);
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = CreateClient(c => c.BaseAddress = new Uri($"http://localhost:{mhs.Port}/"));

            var g = await client.GetThing(10, 20, 30);
            Assert.That(g, Is.Null);
        }
    }

    [Test]
    public void TestTimeout()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            Thread.Sleep(100);
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = CreateClient(c => { c.BaseAddress = new Uri($"http://localhost:{mhs.Port}/"); c.Timeout = TimeSpan.FromMilliseconds(50); });

            Assert.ThrowsAsync<TaskCanceledException>(async () => await client.GetThing(10, 20, 30));
        }
    }

    [Test]
    public async Task TestRequestSerialization()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/echo"))
            {
                resp.StatusCode = (int)HttpStatusCode.OK;
                var bodyContent = req.ReadBody();
                EchoObject eo = JsonUtil.DeserializeObject<EchoObject>(bodyContent)!;
                eo.RawContent = bodyContent;
                resp.WriteBody(JsonUtil.SerializeObject(eo), ContentTypes.ApplicationJsonUtf8);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = CreateClient(c => c.BaseAddress = new Uri($"http://localhost:{mhs.Port}/"));

            var e = new EchoObject
            {
                PascalCase = "A Pascal Value",
                Dict = new Dictionary<string, object>()
                {
                    ["PascalKey"] = 5,
                    ["camelKey"] = 10
                }
            };

            assertEcho(await client.Echo(e));
            assertEcho(await client.Echo(e));
            assertEcho(await client.PutEcho(e));
            assertEcho(await client.PatchEcho(e));
            assertEcho(await client.DeleteEcho(e));
        }

        static void assertEcho(EchoObject? e2)
        {
            Assert.That(e2, Is.Not.Null);
            e2 = e2 ?? throw new Exception();
            Assert.That(e2.PascalCase, Is.EqualTo("A Pascal Value"));
            Assert.That(e2.Dict?["PascalKey"], Is.EqualTo(5));
            Assert.That(e2.Dict?["camelKey"], Is.EqualTo(10));

            Assert.That(e2.RawContent, Is.Not.Null);
            Assert.That(e2.RawContent, Does.Contain("\"PascalKey\":5"));
            Assert.That(e2.RawContent, Does.Contain("\"camelKey\":10"));
        }
    }

    [Test]
    public async Task TestRequestFailedToConnect()
    {
        bool BeforeExecCalled = false, AfterExecCalled = false, AfterSuccessCalled = false, AfterFailureCalled = false;
        var client = CreateClient(c =>
            {
                c.BaseAddress = new Uri($"http://localhost:{MockHttpServer.GetRandomUnusedPort()}/");
            },
            s =>
            {
                s.BeforeExec = (r) => { BeforeExecCalled = true; return Task.CompletedTask; };
                s.AfterExec = (rq, rs) => { AfterExecCalled = true; return Task.CompletedTask; };
                s.AfterSuccess = (rq, rs, ct) => { AfterSuccessCalled = true; return Task.CompletedTask; };
                s.AfterFailure = (rq, rs, ex) => { AfterFailureCalled = true; return Task.CompletedTask; };
            },
        b =>
        {
            // this lets us short circuit the normal connection timeout (it is about 4 seconds otherwise)
            b.ConfigurePrimaryHttpMessageHandler(() => new SocketsHttpHandler() { ConnectTimeout = TimeSpan.FromMilliseconds(150) });
        });

        var e = new EchoObject();

        try
        {
            await client.Echo(e);
            Assert.Fail();
        }
        catch (Exception ex) when (ex is TaskCanceledException or HttpRequestException)
        {
            //Assert.AreEqual(HttpStatusCode.InternalServerError, e.StatusCode);
        }

        Assert.That(BeforeExecCalled);
        Assert.That(AfterExecCalled, Is.False);
        Assert.That(AfterSuccessCalled, Is.False);
        Assert.That(AfterFailureCalled);
    }

    [Test]
    public async Task TestClientRaw()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/bytes"))
            {
                resp.StatusCode = (int)HttpStatusCode.OK;
                resp.WriteBody([0xCA, 0xFE, 0xBA, 0xBE], ContentTypes.ApplicationOctetStream);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();

            var client = CreateClient(c => c.BaseAddress = new Uri($"http://localhost:{mhs.Port}/"));

            var data = await client.GetBytes();

            Assert.That(data, Is.Not.Null);
            Assert.That(data, Has.Length.EqualTo(4));
            Assert.That(Convert.ToHexStringLower(data), Is.EqualTo("cafebabe"));
        }
    }

    [Test]
    public async Task TestClientString()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/string"))
            {
                resp.StatusCode = (int)HttpStatusCode.OK;
                resp.WriteBody("Hello From Mocked Server", ContentTypes.TextPlain);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();

            var client = CreateClient(c => c.BaseAddress = new Uri($"http://localhost:{mhs.Port}/"));

            var data = await client.GetString();

            Assert.That(data, Is.Not.Null);
            Assert.That(data, Is.EqualTo("Hello From Mocked Server"));
        }
    }
}
