using Plinth.Common.Constants;
using Plinth.Serialization;
using Plinth.HttpApiClient;
using NUnit.Framework;
using System.Net;
using Plinth.Common.Logging;
using Plinth.HttpApiClient.Common;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

#if !NET9_0_OR_GREATER
using Convert = Plinth.Common.Utils.BackPort.PlinthConvert;
#endif

namespace Tests.Plinth.HttpApiClient.HttpClient;

[TestFixture]
public class WebApiClientTest
{
    private class GetResponse
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }
        public string? V { get; set; }
    }

    private class EchoObject
    {
        public string? PascalCase { get; set; }
        public Dictionary<string, object>? Dict { get; set; }
        public string? RawContent { get; set; }
    }

    private class MyApiClient : BaseHttpApiClient
    {
        public bool BeforeExecCalled = false;
        public bool AfterExecCalled = false;
        public bool AfterSuccessCalled = false;
        public bool AfterFailureCalled = false;

        public MyApiClient(string serviceId, string? serverUrl, System.Net.Http.HttpClient client, TimeSpan? timeout)
            : base(serviceId, client, Options.Create(new BaseHttpApiClientSettings { BaseAddress = serverUrl, Timeout = timeout }))
        {
            BeforeExec = (r) =>
            {
                BeforeExecCalled = true;
                return Task.CompletedTask;
            };

            AfterExec = (rq, rs) =>
            {
                AfterExecCalled = true;
                return Task.CompletedTask;
            };

            AfterSuccess = (rq, rs, ct) =>
            {
                AfterSuccessCalled = true;
                return Task.CompletedTask;
            };

            AfterFailure = (rq, rs, ex) =>
            {
                AfterFailureCalled = true;
                return Task.CompletedTask;
            };
        }

        public async Task<GetResponse?> GetThing(int x, int y, int z)
        {
            return await HttpGet($"api/v1/thing/{x}/get")
                .AddQueryParameter("y", y.ToString())
                .AddQueryParameter("z", z)
                .AddQueryParameter("_null_", (int?)null)
                .ExecuteAsync<GetResponse>();
        }

        public async Task<GetResponse?> GetThingCustom(int x, int y, int z)
        {
            return await HttpGet($"api/v1/thing/{x}/get")
                .Configure(m =>
                {
                    m.AddQueryParameter("y", y);
                    m.AddQueryParameter("z", z);
                    m.SetAuthorization("token1234");
                    m.SetAcceptHeader(ContentTypes.ApplicationVndMsExcel);
                    m.AddHeader("TestHeader1", "Header1Val");
                    m.AddHeader("TestHeader2", null);
                    m.AddHeaders(("TestHeader3", "Header3Val"), ("TestHeader4", null));
                    m.AddHeaders(new Dictionary<string, string?> { ["TestHeader5"] = "Header5Val", ["TestHeader6"] = null });
                })
                .ExecuteAsync<GetResponse>();
        }

        public async Task<GetResponse?> GetThingHeaders(int x, int y, int z, string scheme = "Bearer")
        {
            return await HttpGet($"api/v1/thing/{x}/get")
                .AddQueryParameter("y", y.ToString())
                .AddQueryParameter("z", z)
                .SetAuthorization("token1234", scheme)
                .SetAcceptHeader(ContentTypes.ApplicationVndMsExcel)
                .AddHeader("TestHeader1", "Header1Val")
                .AddHeader("TestHeader2", null)
                .AddHeaders(("TestHeader3", "Header3Val"), ("TestHeader4", null))
                .AddHeaders(new Dictionary<string, string?> { ["TestHeader5"] = "Header5Val", ["TestHeader6"] = null })
                .ExecuteAsync<GetResponse>();
        }

        public async Task<GetResponse?> GetThingFullUrl(int port, int x, int y, int z)
        {
            return await HttpGet($"http://localhost:{port}/api/v1/thing/{x}/get")
                .AddQueryParameter("y", y.ToString())
                .AddQueryParameter("z", z)
                .ExecuteAsync<GetResponse>();
        }

        public async Task<GetResponse?> GetThing404Ok(int x, int y)
        {
            return await HttpGet($"api/v1/thing/{x}/get")
                .AddQueryParameter("y", y.ToString())
                .ExecuteAsync<GetResponse>(HttpStatusCode.NotFound);
        }

        public async Task<GetResponse?> GetThing404Ok2(int x, int y)
        {
            return await HttpGet($"api/v1/thing/{x}/get")
                .AddQueryParameter("y", y.ToString())
                .ExecuteOrNullAsync<GetResponse>();
        }

        public async Task<GetResponse?> GetThing404Ok3(int x, int y)
        {
            var cts = new CancellationTokenSource();
            return await HttpGet($"api/v1/thing/{x}/get")
                .AddQueryParameter("y", y.ToString())
                .ExecuteOrNullAsync<GetResponse>(cts.Token);
        }

        public async Task<string?> GetThing404Ok4(int x, int y)
        {
            return (await HttpGet($"api/v1/thing/{x}/get")
                .AddQueryParameter("y", y.ToString())
                .ExecuteOrNullAsync()).content;
        }

        public async Task<string?> GetThing404Ok5(int x, int y)
        {
            var cts = new CancellationTokenSource();
            return (await HttpGet($"api/v1/thing/{x}/get")
                .AddQueryParameter("y", y.ToString())
                .ExecuteOrNullAsync(cts.Token)).content;
        }

        public async Task<EchoObject?> Echo(EchoObject o)
        {
            return await HttpPost("api/v1/echo")
                .SetJsonBody(o)
                .ExecuteAsync<EchoObject>();
        }

        public async Task<EchoObject?> PutEcho(EchoObject o)
        {
            return await HttpPut("api/v1/echo")
                .SetJsonBody(o)
                .ExecuteAsync<EchoObject>();
        }

        public async Task<EchoObject?> PatchEcho(EchoObject o)
        {
            return await HttpPatch("api/v1/echo")
                .SetJsonBody(o)
                .ExecuteAsync<EchoObject>();
        }

        public async Task<EchoObject?> DeleteEcho(EchoObject o)
        {
            return await HttpDelete("api/v1/echo")
                .SetJsonBody(o)
                .ExecuteAsync<EchoObject>();
        }

        public async Task<string?> GetString()
        {
            return (await HttpPost("api/v1/string")
                .ExecuteAsync())
                .content;
        }

        public async Task<byte[]> GetBytes()
        {
            return await (await HttpPost("api/v1/bytes")
                .ExecuteRawAsync())
                .Content.ReadAsByteArrayAsync();
        }

        public async Task<byte[]> GetBytes2()
        {
            var cts = new CancellationTokenSource();
            return await (await HttpPost("api/v1/bytes")
                .ExecuteRawAsync(cts.Token))
                .Content.ReadAsByteArrayAsync();
        }
    }

    private static System.Net.Http.HttpClient CreateClient()
    {
        var services = new ServiceCollection();
        services.AddHttpClient();
        var fac = services.BuildServiceProvider().GetRequiredService<IHttpClientFactory>();
        return fac.CreateClient();
    }

    [Test]
    public async Task TestClient()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/thing/"))
            {
                int x = req.Url.PathAndQuery.Split([ '/' ], StringSplitOptions.RemoveEmptyEntries).Skip(3).Select(int.Parse).First();
                int y = int.Parse(req.QueryString.GetValues("y")!.First());
                int z = int.Parse(req.QueryString.GetValues("z")!.First());

                resp.StatusCode = (int)HttpStatusCode.OK;
                resp.WriteBody(JsonUtil.SerializeObject(new GetResponse { V = "myString", X = x, Y = y, Z = z }), ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();

            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", CreateClient(), null);

            var g = await client.GetThing(10, 20, 30);

            Assert.That(g, Is.Not.Null);
            Assert.That(g?.X, Is.EqualTo(10));
            Assert.That(g?.Y, Is.EqualTo(20));
            Assert.That(g?.Z, Is.EqualTo(30));
            Assert.That(g?.V, Is.EqualTo("myString"));

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled);
            Assert.That(client.AfterFailureCalled, Is.False);

            g = await client.GetThingFullUrl(mhs.Port, 10, 20, 30);

            Assert.That(g, Is.Not.Null);
            Assert.That(g?.X, Is.EqualTo(10));
            Assert.That(g?.Y, Is.EqualTo(20));
            Assert.That(g?.Z, Is.EqualTo(30));
            Assert.That(g?.V, Is.EqualTo("myString"));
        }
    }

    [Test]
    public async Task TestClientNoBaseUrl()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/thing/"))
            {
                int x = req.Url.PathAndQuery.Split(['/'], StringSplitOptions.RemoveEmptyEntries).Skip(3).Select(int.Parse).First();
                int y = int.Parse(req.QueryString.GetValues("y")!.First());
                int z = int.Parse(req.QueryString.GetValues("z")!.First());

                resp.StatusCode = (int)HttpStatusCode.OK;
                resp.WriteBody(JsonUtil.SerializeObject(new GetResponse { V = "myString", X = x, Y = y, Z = z }), ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();

            var client = new MyApiClient("service", null, CreateClient(), null);

            var g = await client.GetThingFullUrl(mhs.Port, 10, 20, 30);

            Assert.That(g, Is.Not.Null);
            Assert.That(g?.X, Is.EqualTo(10));
            Assert.That(g?.Y, Is.EqualTo(20));
            Assert.That(g?.Z, Is.EqualTo(30));
            Assert.That(g?.V, Is.EqualTo("myString"));

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled);
            Assert.That(client.AfterFailureCalled, Is.False);
        }
    }

    [Test]
    public async Task TestClientHeaders()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/thing/"))
            {
                int x = req.Url.PathAndQuery.Split(['/'], StringSplitOptions.RemoveEmptyEntries).Skip(3).Select(int.Parse).First();
                int y = int.Parse(req.QueryString.GetValues("y")!.First());
                int z = int.Parse(req.QueryString.GetValues("z")!.First());

                Assert.That(req.Headers["TestHeader1"], Is.EqualTo("Header1Val"));
                Assert.That(req.Headers["TestHeader2"], Is.Empty);
                Assert.That(req.Headers["TestHeader3"], Is.EqualTo("Header3Val"));
                Assert.That(req.Headers["TestHeader4"], Is.Empty);
                Assert.That(req.Headers["TestHeader5"], Is.EqualTo("Header5Val"));

                if (z == 30)
                    Assert.That(req.Headers["Authorization"], Is.EqualTo("Bearer token1234"));
                else if (z == 40)
                    Assert.That(req.Headers["Authorization"], Is.EqualTo("Basic token1234"));
                Assert.That(req.Headers["Accept"], Is.EqualTo(ContentTypes.ApplicationVndMsExcel));

                resp.StatusCode = (int)HttpStatusCode.OK;
                resp.WriteBody(JsonUtil.SerializeObject(new GetResponse { V = "myString", X = x, Y = y, Z = z }), ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();

            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", CreateClient(), null);

            var g = await client.GetThingHeaders(10, 20, 30);

            Assert.That(g, Is.Not.Null);
            Assert.That(g?.X, Is.EqualTo(10));
            Assert.That(g?.Y, Is.EqualTo(20));
            Assert.That(g?.Z, Is.EqualTo(30));
            Assert.That(g?.V, Is.EqualTo("myString"));

            g = await client.GetThingHeaders(10, 20, 40, "Basic");

            Assert.That(g, Is.Not.Null);
            Assert.That(g?.X, Is.EqualTo(10));
            Assert.That(g?.Y, Is.EqualTo(20));
            Assert.That(g?.Z, Is.EqualTo(40));
            Assert.That(g?.V, Is.EqualTo("myString"));

            g = await client.GetThingCustom(10, 20, 30);

            Assert.That(g, Is.Not.Null);
            Assert.That(g?.X, Is.EqualTo(10));
            Assert.That(g?.Y, Is.EqualTo(20));
            Assert.That(g?.Z, Is.EqualTo(30));
            Assert.That(g?.V, Is.EqualTo("myString"));
        }
    }

    [Test]
    public async Task TestClientFailure()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            int y = int.Parse(req.QueryString.GetValues("y")!.First());
            if (y == 20)
                resp.StatusCode = (int)HttpStatusCode.NotFound;
            else
            {
                resp.StatusCode = (int)HttpStatusCode.OK;
                resp.WriteBody("{}", ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", CreateClient(), null);

            try
            {
                await client.GetThing(10, 20, 30);
                Assert.Fail();
            }
            catch (RestException e)
            {
                Assert.That(e.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
            }

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled, Is.False);
            Assert.That(client.AfterFailureCalled);

            client.BeforeExecCalled = false;
            client.AfterExecCalled = false;
            client.AfterFailureCalled = false;

            // status codes
            var g = await client.GetThing404Ok(10, 20);
            Assert.That(g, Is.Null);

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled);
            Assert.That(client.AfterFailureCalled, Is.False);

            g = await client.GetThing404Ok(10, 40);
            Assert.That(g, Is.Not.Null);

            // ExecuteOrNull
            g = await client.GetThing404Ok2(10, 20);
            Assert.That(g, Is.Null);
            g = await client.GetThing404Ok2(10, 40);
            Assert.That(g, Is.Not.Null);

            // ExecuteOrNull with token
            g = await client.GetThing404Ok3(10, 20);
            Assert.That(g, Is.Null);
            g = await client.GetThing404Ok3(10, 40);
            Assert.That(g, Is.Not.Null);

            // ExecuteOrNull string
            var s = await client.GetThing404Ok4(10, 20);
            Assert.That(s, Is.Null);
            s = await client.GetThing404Ok4(10, 40);
            Assert.That(s, Is.Not.Null);

            // ExecuteOrNull string with token
            s = await client.GetThing404Ok5(10, 20);
            Assert.That(s, Is.Null);
            s = await client.GetThing404Ok5(10, 40);
            Assert.That(s, Is.Not.Null);
        }
    }

    [Test]
    public async Task TestClient404WithBody()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            resp.StatusCode = (int)HttpStatusCode.NotFound;
            resp.WriteBody(JsonUtil.SerializeObject(new GetResponse { Y = 10 }), ContentTypes.ApplicationJson);
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", CreateClient(), null);

            try
            {
                await client.GetThing(10, 20, 30);
                Assert.Fail();
            }
            catch (RestException e)
            {
                Assert.That(e.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
            }

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled, Is.False);
            Assert.That(client.AfterFailureCalled);

            client.BeforeExecCalled = false;
            client.AfterExecCalled = false;
            client.AfterFailureCalled = false;

            // status codes
            var g = await client.GetThing404Ok(10, 20);
            Assert.That(g, Is.Not.Null);
            Assert.That(g!.Y, Is.EqualTo(10));

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled);
            Assert.That(client.AfterFailureCalled, Is.False);

            g = await client.GetThing404Ok(10, 40);
            Assert.That(g, Is.Not.Null);

            // ExecuteOrNull
            g = await client.GetThing404Ok2(10, 20);
            Assert.That(g, Is.Null);

            // ExecuteOrNull with token
            g = await client.GetThing404Ok3(10, 20);
            Assert.That(g, Is.Null);

            // ExecuteOrNull string
            var s = await client.GetThing404Ok4(10, 20);
            Assert.That(s, Is.Null);

            // ExecuteOrNull string with token
            s = await client.GetThing404Ok5(10, 20);
            Assert.That(s, Is.Null);
        }
    }

    [Test]
    public async Task TestClientFailureWithMessage()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            resp.StatusCode = (int)HttpStatusCode.InternalServerError;
            resp.WriteBody(JsonUtil.SerializeObject(new { Message = "FailedToThing", OtherThing = 7 }), ContentTypes.ApplicationJson);
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", CreateClient(), null);

            try
            {
                await client.GetThing(10, 20, 30);
                Assert.Fail();
            }
            catch (RestException e)
            {
                Assert.That(e.StatusCode, Is.EqualTo(HttpStatusCode.InternalServerError));
                Assert.That(e.Message, Is.EqualTo("FailedToThing"));
            }

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled, Is.False);
            Assert.That(client.AfterFailureCalled);

            client.BeforeExecCalled = false;
            client.AfterExecCalled = false;
            client.AfterFailureCalled = false;
        }
    }

    [Test]
    public async Task TestClientFailureWithInvalidMessage()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            resp.StatusCode = (int)HttpStatusCode.InternalServerError;
            resp.WriteBody("a thing failed", ContentTypes.TextPlain);
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", CreateClient(), null);

            try
            {
                await client.GetThing(10, 20, 30);
                Assert.Fail();
            }
            catch (RestException e)
            {
                Assert.That(e.StatusCode, Is.EqualTo(HttpStatusCode.InternalServerError));
                Assert.That(e.Message, Is.EqualTo("<unknown error>"));
            }

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled, Is.False);
            Assert.That(client.AfterFailureCalled);

            client.BeforeExecCalled = false;
            client.AfterExecCalled = false;
            client.AfterFailureCalled = false;
        }
    }

    [Test]
    public async Task TestEmptyResult()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            resp.StatusCode = (int)HttpStatusCode.OK;
            resp.WriteBody(string.Empty, ContentTypes.ApplicationJson);
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", CreateClient(), null);

            var g = await client.GetThing(10, 20, 30);
            Assert.That(g, Is.Null);
        }
    }

    [Test]
    public void TestTimeout()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            Thread.Sleep(100);
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", CreateClient(), TimeSpan.FromMilliseconds(50));

            Assert.ThrowsAsync<TaskCanceledException>(async () => await client.GetThing(10, 20, 30));
        }
    }

    [Test]
    public async Task TestRequestSerialization()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/echo"))
            {
                resp.StatusCode = (int)HttpStatusCode.OK;
                var bodyContent = req.ReadBody();
                EchoObject eo = JsonUtil.DeserializeObject<EchoObject>(bodyContent)!;
                eo.RawContent = bodyContent;
                resp.WriteBody(JsonUtil.SerializeObject(eo), ContentTypes.ApplicationJsonUtf8);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", CreateClient(), null);

            var e = new EchoObject
            {
                PascalCase = "A Pascal Value",
                Dict = new Dictionary<string, object>()
                {
                    ["PascalKey"] = 5,
                    ["camelKey"] = 10
                }
            };

            assertEcho(await client.Echo(e));
            assertEcho(await client.Echo(e));
            assertEcho(await client.PutEcho(e));
            assertEcho(await client.PatchEcho(e));
            assertEcho(await client.DeleteEcho(e));
        }

        static void assertEcho(EchoObject? e2)
        {
            Assert.That(e2, Is.Not.Null);
            e2 = e2 ?? throw new Exception();
            Assert.That(e2.PascalCase, Is.EqualTo("A Pascal Value"));
            Assert.That(e2.Dict?["PascalKey"], Is.EqualTo(5));
            Assert.That(e2.Dict?["camelKey"], Is.EqualTo(10));

            Assert.That(e2.RawContent, Is.Not.Null);
            Assert.That(e2.RawContent, Does.Contain("\"PascalKey\":5"));
            Assert.That(e2.RawContent, Does.Contain("\"camelKey\":10"));
        }
    }

    private class ShouldLogFalseClient : MyApiClient
    {
        public ShouldLogFalseClient(string serviceId, string serverUrl, System.Net.Http.HttpClient httpClient, TimeSpan? timeout) : base(serviceId, serverUrl, httpClient, timeout)
        {
        }

        public override bool ShouldLogContent(string? contentType, bool isRequest)
        {
            return false;
        }
    }

    [Test]
    public async Task TestNoLog()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/echo"))
            {
                resp.StatusCode = (int)HttpStatusCode.OK;
                var bodyContent = req.ReadBody();
                EchoObject eo = JsonUtil.DeserializeObject<EchoObject>(bodyContent)!;
                eo.RawContent = bodyContent;
                resp.WriteBody(JsonUtil.SerializeObject(eo), ContentTypes.ApplicationJson);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();
            var client = new ShouldLogFalseClient("service", $"http://localhost:{mhs.Port}/", CreateClient(), null);

            var e = new EchoObject
            {
                PascalCase = "A Pascal Value",
                Dict = new Dictionary<string, object>()
                {
                    ["PascalKey"] = 5,
                    ["camelKey"] = 10
                }
            };

            var e2 = await client.Echo(e);

            Assert.That(e2, Is.Not.Null);
            Assert.That(e2!.PascalCase, Is.EqualTo("A Pascal Value"));
            Assert.That(e2!.Dict!["PascalKey"], Is.EqualTo(5));
            Assert.That(e2!.Dict!["camelKey"], Is.EqualTo(10));
        }
    }

    [Test]
    public async Task TestRequestFailedToConnect()
    {
        var services = new ServiceCollection();
        services.AddHttpClient("name").ConfigurePrimaryHttpMessageHandler(
            () =>
            {
                // this lets us short circuit the normal connection timeout (it is about 4 seconds otherwise)
                return new SocketsHttpHandler() { ConnectTimeout = TimeSpan.FromMilliseconds(150) };
            });

        var fac = services.BuildServiceProvider().GetRequiredService<IHttpClientFactory>();
        var httpClient = fac.CreateClient("name");

        var client = new MyApiClient("service", $"http://localhost:{MockHttpServer.GetRandomUnusedPort()}/", httpClient, null);

        var e = new EchoObject();

        try
        {
            await client.Echo(e);
            Assert.Fail();
        }
        // the normal connection timeout throws RestException
        //catch (RestException e)
        catch (Exception ex) when (ex is TaskCanceledException or HttpRequestException)
        {
            //Assert.AreEqual(HttpStatusCode.InternalServerError, e.StatusCode);
        }
    }

    [Test]
    public async Task TestClientRaw()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/bytes"))
            {
                resp.StatusCode = (int)HttpStatusCode.OK;
                resp.WriteBody([0xCA, 0xFE, 0xBA, 0xBE], ContentTypes.ApplicationOctetStream);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();

            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", CreateClient(), null);

            var data = await client.GetBytes();

            Assert.That(data, Is.Not.Null);
            Assert.That(data, Has.Length.EqualTo(4));
            Assert.That(Convert.ToHexStringLower(data), Is.EqualTo("cafebabe"));

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled);
            Assert.That(client.AfterFailureCalled, Is.False);

            data = await client.GetBytes2();

            Assert.That(data, Is.Not.Null);
            Assert.That(data, Has.Length.EqualTo(4));
            Assert.That(Convert.ToHexStringLower(data), Is.EqualTo("cafebabe"));
        }
    }

    [Test]
    public async Task TestClientString()
    {
        MockHttpServer? mhs = null;
        using (mhs = new MockHttpServer((req, resp) =>
        {
            if (req.Url!.PathAndQuery.StartsWith("/api/v1/string"))
            {
                resp.StatusCode = (int)HttpStatusCode.OK;
                resp.WriteBody("Hello From Mocked Server", ContentTypes.TextPlain);
            }
        }))
        {
            RequestTraceId.Instance.Create();
            RequestTraceChain.Instance.Create();

            var client = new MyApiClient("service", $"http://localhost:{mhs.Port}/", CreateClient(), null);

            var data = await client.GetString();

            Assert.That(data, Is.Not.Null);
            Assert.That(data, Is.EqualTo("Hello From Mocked Server"));

            Assert.That(client.BeforeExecCalled);
            Assert.That(client.AfterExecCalled);
            Assert.That(client.AfterSuccessCalled);
            Assert.That(client.AfterFailureCalled, Is.False);
        }
    }
}
