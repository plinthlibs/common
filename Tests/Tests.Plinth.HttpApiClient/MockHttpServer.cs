﻿using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Tests.Plinth.HttpApiClient;

public static class MockHttpServerExtensions
{
    public static string ReadBody(this HttpListenerRequest req)
    {
        using var body = req.InputStream;
        using var rdr = new StreamReader(body, req.ContentEncoding);
        return rdr.ReadToEnd();
    }

    public static void WriteBody(this HttpListenerResponse resp, string body, string contentType)
    {
        byte[] data = Encoding.UTF8.GetBytes(body);
        resp.ContentLength64 = data.Length;
        resp.ContentType = contentType;
        resp.OutputStream.Write(data, 0, data.Length);
    }

    public static void WriteBody(this HttpListenerResponse resp, byte[] data, string contentType)
    {
        resp.ContentType = contentType;
        resp.ContentLength64 = data.Length;
        resp.OutputStream.Write(data.AsSpan());
    }
}

public class MockHttpServer : IDisposable
{
    private readonly HttpListener _listener;
    private readonly Action<HttpListenerRequest, HttpListenerResponse> _handler;

    public int Port { get; }

    public MockHttpServer(Action<HttpListenerRequest, HttpListenerResponse> handler)
        : this(-1, handler)
    {
    }

    public MockHttpServer(int port, Action<HttpListenerRequest, HttpListenerResponse> handler)
    {
        _handler = handler;

        Port = port > 0 ? port : GetRandomUnusedPort();

        //create and start listener
        _listener = new HttpListener();
        _listener.Prefixes.Add($"http://localhost:{Port}/");
        _listener.Start();
        Task.Run(() => HandleRequests());
    }

    #region Private Methods

    internal static int GetRandomUnusedPort()
    {
        var listener = new TcpListener(IPAddress.Any, 0);
        listener.Start();
        var port = ((IPEndPoint)listener.LocalEndpoint).Port;
        listener.Stop();
        return port;
    }

    private async Task HandleRequests()
    {
        try
        {
            //listen for all requests
            while (_listener.IsListening)
            {
                //get the request
                var context = await _listener.GetContextAsync();

                try
                {
                    _handler.Invoke(context.Request, context.Response);
                }
                catch (Exception)
                {
                    try
                    {
                        context.Response.OutputStream.Close();
                    }
                    catch { }
                    throw;
                }

                context.Response.OutputStream.Close();
            }
        }
        catch (HttpListenerException ex)
        {
            //when the listener is stopped, it will throw an exception for being cancelled, so just ignore it
            if (ex.Message != "The I/O operation has been aborted because of either a thread exit or an application request")
                throw;

            Debug.WriteLine(ex);
        }
    }

    #endregion Private Methods

    #region IDisposable

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (disposing && (_listener?.IsListening ?? false))
        {
            _listener.Stop();
            ((IDisposable)_listener).Dispose();
        }
    }

    #endregion IDisposable
}
