using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Tests.Plinth.AspNetCore.Controllers;

#if !NET8_0_OR_GREATER
// net8.0 added an analyzer that flags the below as not ok
[Route("api/ignored")]
[Route("api/ignored2")]
[ApiController]
[ApiExplorerSettings(IgnoreApi = true)] // test ignoring controller in swagger
public class IgnoredController : ControllerBase
{
    [HttpGet]
    [Route("test")]
    public IActionResult MyAction()
    {
        return Ok();
    }

    [HttpGet]
    [Route("test")]
    public IActionResult MyAction(int x)
    {
        return Ok(x);
    }
}
#endif
