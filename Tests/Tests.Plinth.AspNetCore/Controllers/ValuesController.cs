using System.ComponentModel.DataAnnotations;
using Plinth.Common.Exceptions;
using Plinth.HttpApiClient.Models;
using Plinth.HttpApiClient.Response;
using Microsoft.AspNetCore.Mvc;
using Omu.ValueInjecter;
using Microsoft.AspNetCore.Authorization;
using Plinth.AspNetCore.SecureToken;
using Plinth.AspNetCore;
using Plinth.HttpApiClient.Validation;
using Tests.Plinth.AspNetCore.Core2.Managers;
using Tests.Plinth.AspNetCore.Managers;
using Swashbuckle.AspNetCore.Annotations;
using RestSharp;
using Tests.Plinth.HttpApiClient;

namespace Tests.Plinth.AspNetCore.Controllers;

public class ResolvedThing
{
    public ResolvedThing(int x)
    {
        X = x;
    }

    public int X { get; }
}

public class ResolvedThingScoped
{
    public ResolvedThingScoped()
    {
        X = Guid.NewGuid();
    }

    public Guid X { get; }
}

public enum TestEnumApi
{
    EnumVal1,
    EnumVal2
}

public enum TestEnumInternal
{
    EnumVal1,
    EnumVal2
}

public class ModelObject : BaseApiModel
{
    [Required]
    [MinLength(3, ErrorMessage = "must have 3 or more chars")]
    public string? Value { get; set; }

    public string? TestValue { get; set; }
    public TestEnumApi EnumerationVal { get; set; }
    public double TestDouble { get; set; }
    public object? RandomThing { get; set; }
    public TimeOnly? MyTime { get; set; }
    public DateOnly? MyDate { get; set; }

    public override void UpgradeModel()
    {
        Value += "Updated";
    }
}

public class ModelObjectInternal
{
    public string? Value { get; set; }
    public string? TestValue { get; set; }
    public TestEnumInternal EnumerationVal { get; set; }
    public double TestDouble { get; set; }
    public bool IsInternal { get; } = true;
}

public class AttrTest1
{
    [RequiredAtLeastOneOf(nameof(Value1), nameof(Value2))]
    public string? Value1 { get; set; }
    public string? Value2 { get; set; }

    [RequiredOnlyOneOf(nameof(Value3), nameof(Value4))]
    public string? Value3 { get; set; }
    public string? Value4 { get; set; }
}

public class RecursiveClient : global::Plinth.HttpApiClient.BaseHttpApiClient
{
    public RecursiveClient(HttpClient client) : base("Recursive", client)
    {
    }

    public async Task<ModelObject?> GetAll()
    {
        return await HttpGet("/api/values").ExecuteAsync<ModelObject>();
    }

    public async Task<ModelObject?> GetRecursive(int level)
    {
        return await HttpGet("/api/values/recursive").AddQueryParameter("maxLevel", level).ExecuteAsync<ModelObject>();
    }
}

public class RecursiveClientRestSharp : global::Plinth.HttpApiClient.RestSharp.BaseHttpApiClient
{
    public RecursiveClientRestSharp(string serviceId, string serverUrl, HttpClient httpClient, TimeSpan? timeout = null)
        : base(serviceId, serverUrl, httpClient, timeout)
    {
        base.MaxLoggingResponseBody = 15;
    }

    public async Task<ModelObject?> GetAll()
    {
        return await ExecuteAsync<ModelObject>(HttpGet("/api/values"));
    }

    public async Task<ModelObject?> GetRecursive(int level)
    {
        return await ExecuteAsync<ModelObject>(HttpGet("/api/values/recursive-rest-sharp").AddQueryParameter("maxLevel", level));
    }
}

// http://hamidmosalla.com/2017/03/29/asp-net-core-action-results-explained/
[Route("api/[controller]")]
[ApiController]
[DisableGlobalAuthorize]
public class ValuesController : Controller
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private readonly SecureTokenGenerator _tokenGen;
    private readonly IBackgroundTaskQueue _bgTaskQ;
    private readonly Manager.Factory<TestManager> _mgrFac;

    private readonly RecursiveClientRestSharp _rsClient;
    private readonly RecursiveClient _client;
    private readonly Client.INSwagClient _nswagClient;

    public ValuesController(ResolvedThing rt, ResolvedThingScoped rts, SecureTokenGenerator tokenGen, IBackgroundTaskQueue bgTaskQ, Manager.Factory<TestManager> mgrFac, RecursiveClient client, Client.INSwagClient nswagClient, RecursiveClientRestSharp rsClient)
    {
        _tokenGen = tokenGen;
        _bgTaskQ = bgTaskQ;
        _mgrFac = mgrFac;

        log.Debug($"resolved: {rt.X}");
        log.Debug($"resolved scoped: {rts.X}");

        _client = client;
        _rsClient = rsClient;
        _nswagClient = nswagClient;
    }

    // GET api/values
    /// <summary>
    /// Get All
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [Route("")]
    [ProducesResponseType(typeof(ModelObject), 200)]
    public IActionResult GetAll()
    {
        using (log.BeginScope("scope1"))
        using (log.BeginScope(new Dictionary<string, object>() { ["ScopeVar"] = "scopeVar1" }))
        {
            log.Info("LogTest {LogVar1}", "_logVar1_");
        }
        
        using (log.LogExecTiming("building result"))
        {
            return Ok(new ModelObject()
            {
                Value = "ABC", TestValue = null, EnumerationVal = TestEnumApi.EnumVal1, TestDouble = 1.2358726d,
                MyTime = new TimeOnly(12, 15, 22),
                MyDate = new DateOnly(2012, 11, 22),
            });
        }
    }

    // GET api/values/recursive
    /// <summary>
    /// Get All
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [Route("recursive")]
    [ProducesResponseType(typeof(ModelObject), 200)]
    public async Task<IActionResult> GetRecursive(int maxLevel = 3)
    {
        if (maxLevel == 0)
            return Ok(await _client.GetAll());
        else
            return Ok(await _client.GetRecursive(maxLevel - 1));
    }

    // GET api/values/recursive-rest-sharp
    /// <summary>
    /// Get All
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [Route("recursive-rest-sharp")]
    [ProducesResponseType(typeof(ModelObject), 200)]
    [SwaggerOperation(OperationId = "GetRecursiveRestSharp")]
    public async Task<IActionResult> GetRecursiveRestSharp(int maxLevel = 3)
    {
        if (maxLevel == 0)
            return Ok(await _rsClient.GetAll());
        else
            return Ok(await _rsClient.GetRecursive(maxLevel - 1));
    }

    // GET api/values/recursive-nswag
    /// <summary>
    /// Get All
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [Route("recursive-nswag", Name = "GetRecursiveNswagValues")]
    [ProducesResponseType(typeof(ModelObject), 200)]
    public async Task<IActionResult> GetRecursiveNswag(int maxLevel = 3)
    {
        if (maxLevel == 0)
            return Ok(await _nswagClient.Values_GetAll_GETAsync());
        else
            return Ok(await _nswagClient.GetRecursiveNswagValuesAsync(maxLevel - 1));
    }

    // GET error?type=
    [HttpGet]
    [Route("error")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status405MethodNotAllowed)]
#if NET8_0_OR_GREATER
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Maintainability", "CA1512:Use ArgumentOutOfRangeException throw helper", Justification = "<Pending>")]
#endif
    public IActionResult GetError(int type)
    {
        switch (type)
        {
            case 0: return Ok();
            case 1: return BadRequest();
            case 2: throw new Exception("unknown");
            case 3: throw new LogicalNotFoundException("not found");
            case 4: throw new LogicalPreconditionFailedException(new { val = type });
            case 5: throw new LogicalPreconditionFailedException(new ApiResponse(new ApiError(ApiErrorCode.ServerError, "this was an error", "input")));
            case 6: if (type < 2877658) throw new ArgumentOutOfRangeException(nameof(type)); break;
#pragma warning disable CA2208 // Instantiate argument exceptions correctly
            case 7: if (type < 2877658) throw new ArgumentOutOfRangeException(); break;
#pragma warning restore CA2208 // Instantiate argument exceptions correctly
            default:
                break;
        }

        return StatusCode(StatusCodes.Status405MethodNotAllowed);
    }

    // GET api/values/5
    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    public async Task<IActionResult> GetItem(int id)
    {
        await Task.Delay(25);
        return StatusCode(201, new { id, msg = "thanks", ts = DateTimeOffset.UtcNow.ToUnixTimeSeconds() });
    }

    [HttpGet("email/{email}")]
    public IActionResult GetEmail(string email)
    {
        return new JsonResult(email);
    }

    [HttpGet("manager")]
    public IActionResult GetManager(string user)
    {
        var m = _mgrFac(user);
        return Ok(new { u = m.GetCallingUser(), cfg = m.GetConfig(), x = m.GetResolved() });
    }

    [HttpGet("datetime/{dt1}")]
    public IActionResult GetDates(DateTime? dt1, DateTime? dt2)
    {
        log.Debug($"dt1 = {dt1}, kind = {dt1?.Kind}");
        log.Debug($"dt2 = {dt2}, kind = {dt2?.Kind}");
        return Ok();
    }

    /// <response code="403">not cool, man</response>
    [HttpPost]
    public IActionResult Post(ModelObject mo)
    {
        return Ok(Mapper.Map<ModelObjectInternal>(mo));
    }

    /// <summary>
    /// Put a string body for an id
    /// </summary>
    /// <param name="id">the id, an int</param>
    /// <param name="value">some string</param>
    /// <response code="201">on success</response>
    /// <response code="400">invalid id</response>
    /// <response code="401">your papers, comrade</response>
    [HttpPut("{id}")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(ModelObject), StatusCodes.Status400BadRequest)]
    public IActionResult Put(int id, [FromBody]string value)
    {
        return Created($"/api/values/{id}", new { id, value });
    }

    [HttpPost("modelTest/{id}")]
    public IActionResult ModelTest(int id, ModelObject obj)
    {
        return Ok($"obj {id} value = {obj.Value}");
    }

    [HttpPost("attrTest1")]
    public IActionResult AttrTest_1([FromBody] AttrTest1 obj)
    {
        return Ok(obj);
    }

    // DELETE api/values/5
    [HttpDelete("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public IActionResult Delete(int id)
    {
        log.Debug($"deleting {id}");
        return NoContent();
    }

    [HttpPost("files")]
    [Consumes("multipart/form-data")]
    public IActionResult UploadFiles([FromBody] List<IFormFile> files)
    {
        foreach (var f in files)
        {
            log.Debug($"{f.FileName}, {f.Name}, {f.Length}, {f.ContentType}, {f.ContentDisposition}");
        }
        return Ok();
    }

    [HttpGet("login")]
    [AllowAnonymous]
    public ActionResult<string> Login(string name = "user", [FromQuery] List<string>? roles = null)
    {
        return _tokenGen.Generate(Guid.NewGuid(), name, roles).Token;
    }

    [HttpGet("refresh")]
    public ActionResult<string> Refresh()
    {
        return _tokenGen.Refresh(this.GetAuthenticationToken()).Token;
    }

    [HttpGet("authorized")]
    [Authorize]
    public IActionResult GetAuthorized(int x = 5)
    {
        return Ok(new { x });
    }

    [HttpGet("requires-admin")]
    [Authorize(Roles = "admin")]
    public IActionResult GetAuthorizedForAdmin(int x = 5)
    {
        return Ok(new { x });
    }

    [HttpGet("requires-user-is-special")]
    [Authorize(Policy = "UserNameIs")]
    public IActionResult GetAuthorizedForSpecial(int x = 5)
    {
        return Ok(new { x });
    }

    [HttpGet("background-test")]
    [ProducesResponseType(StatusCodes.Status202Accepted)]
    public IActionResult BackgroundTest(int x = 5, bool useCancelToken = true)
    {
        _bgTaskQ.QueueBackgroundWorkItem(async ct =>
        {
            log.Debug($"starting bg process (use ct = {useCancelToken})");
            if (useCancelToken)
                await Task.Delay(x * 1000, ct);
            else
#pragma warning disable CA2016 // Forward the 'CancellationToken' parameter to methods
                await Task.Delay(x * 1000);
#pragma warning restore CA2016 // Forward the 'CancellationToken' parameter to methods
            log.Debug("ending bg process");
        });
        return Accepted(new { x });
    }

    public class FormAuthBody { public string? Token { get; set; } }

    [HttpPost]
    [Route("form-auth")]
    [Authorize(AuthenticationSchemes = "CustomAuth")]
    public IActionResult TestFormAuth([FromForm] FormAuthBody body)
    {
        log.Debug($"got from body = {body.Token}");
        return Ok();
    }

    [HttpPost("test-model")]
    [ProducesResponseType(typeof(TestModel), 200)]
    public IActionResult TestModel(TestModel testModel)
    {
        return Ok(testModel);
    }

    [HttpGet("test-model")]
    [ProducesResponseType(typeof(TestModel), 200)]
    public IActionResult TestModel()
    {
        return Ok(null);
    }
}
