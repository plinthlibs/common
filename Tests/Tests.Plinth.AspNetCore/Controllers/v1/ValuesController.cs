using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;

namespace Tests.Plinth.AspNetCore.Controllers.V1;

[Route("api/v1/[controller]")]
public class ValuesController : Controller
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    // GET api/values
    [HttpGet]
    [Route("")]
    public IEnumerable<string> Get()
    {
        return ["valueA", "valueB"];
    }

    // GET api/values/5
    [HttpGet("{id}")]
    public string GetById(int id)
    {
        log.Debug($"getting {id}");
        return "value";
    }

    // GET api/values/5
    [HttpGet("email/{email}")]
    public IActionResult GetEmail(string email)
    {
        return new JsonResult(email);
    }

    // POST api/values
    [HttpPost]
    public void Post([FromBody]string value)
    {
        log.Debug($"posted {value}");
    }

    // PUT api/values/5
    [HttpPut("{id}", Name = "Values_Put_PUT2")]
    public IActionResult Put(int id, [FromBody]string value)
    {
        return Created($"/api/values/{id}", new { id, value });
    }

    // DELETE api/values/5
    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        log.Debug($"deleting {id}");
        return NoContent();
    }
}
