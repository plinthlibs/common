﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Plinth.AspNetCore.SecureToken;

namespace Tests.Plinth.AspNetCore.Controllers;

[Route("api/[controller]")]
[Authorize]
public class AuthorizedController : Controller
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    [HttpGet("authorized")]
    public IActionResult GetAuthorized(int x = 5)
    {
        log.Debug($"un={this.GetAuthenticatedUserName()}");
        log.Debug($"id={this.GetAuthenticatedUserGuid()}");
        log.Debug($"se={this.GetAuthenticatedUserSessionGuid()}");
        log.Debug($"rl={string.Join(",", this.GetAuthenticatedUserRoles())}");
        log.Debug($"tk={this.GetAuthenticationToken().Token}");

        var s = this.GetSecureTokenInfo();
        log.Debug($"un={s.UserName}");
        log.Debug($"id={s.UserGuid}");
        log.Debug($"se={s.SessionGuid}");
        log.Debug($"rl={string.Join(",", s.Roles)}");
        log.Debug($"tk={s.Token.Token}");
        return Ok(new { x });
    }

    [HttpGet("anonymous")]
    [AllowAnonymous]
    public IActionResult GetAnonymous(int x = 10)
    {
        if (User.Identity?.IsAuthenticated ?? false)
        {
            log.Debug($"un={this.GetAuthenticatedUserName()}");
            log.Debug($"id={this.GetAuthenticatedUserGuid()}");
            log.Debug($"se={this.GetAuthenticatedUserSessionGuid()}");
            log.Debug($"rl={string.Join(",", this.GetAuthenticatedUserRoles())}");
            log.Debug($"tk={this.GetAuthenticationToken().Token}");
        }

        log.Debug($"t_un={this.TryGetAuthenticatedUserName()}");
        log.Debug($"it_d={this.TryGetAuthenticatedUserGuid()}");
        log.Debug($"st_e={this.TryGetAuthenticatedUserSessionGuid()}");
        log.Debug($"rt_l={(this.TryGetAuthenticatedUserRoles() == null ? "" : string.Join(",", this.GetAuthenticatedUserRoles()))}");
        log.Debug($"tt_k={this.TryGetAuthenticationToken()?.Token}");

        return Ok(new { x });
    }
}
