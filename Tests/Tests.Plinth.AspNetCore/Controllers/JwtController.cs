using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Plinth.AspNetCore;
using Plinth.AspNetCore.Jwt;
using Plinth.Security.Jwt;
using System.Security.Claims;

namespace Tests.Plinth.AspNetCore.Controllers;

[Route("api/[controller]")]
[ApiController]
[DisableGlobalAuthorize]
[Authorize(AuthenticationSchemes = "JwtAuth")]
public class JwtController : ControllerBase
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private readonly JwtGenerator _tokenGen;

    public JwtController(JwtManager tokenMgr)
    {
        _tokenGen = tokenMgr.Generator;
    }

    [HttpGet("login")]
    [AllowAnonymous]
    public ActionResult<JwtData> Login(string name = "user", [FromQuery] List<string>? roles = null)
    {
        return _tokenGen.GetBuilder(Guid.NewGuid(), name, roles)
            .AddClaim(new Claim("test", "myclaim"))
            .Build();
    }

    [HttpGet("refresh")]
    public ActionResult<JwtData> Refresh()
    {
        return _tokenGen.Refresh(this.GetJwt());
    }

    [HttpGet("authorized")]
    public ActionResult<int> GetAuthorized(int x = 5)
    {
        log.Info($"User is {User.Identity?.Name} expires at {User.JwtExpiration()}");

        return Ok(new { x });
    }

    [HttpGet("requires-admin")]
    [Authorize(AuthenticationSchemes = "JwtAuth", Roles = "admin")]
    public ActionResult<int> GetAuthorizedForAdmin(int x = 5)
    {
        return Ok(new { x });
    }

    [HttpGet("requires-user-is-special")]
    [Authorize(AuthenticationSchemes = "JwtAuth", Policy = "UserNameIs")]
    public ActionResult<int> GetAuthorizedForSpecial(int x = 5)
    {
        return Ok(new { x });
    }

}
