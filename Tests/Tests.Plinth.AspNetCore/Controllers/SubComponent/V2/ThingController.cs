﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Tests.Plinth.AspNetCore.Controllers.SubComponent.V2;

[Produces("application/json")]
[Route("api/v2/Thing")]
public class ThingController : Controller
{
    [HttpGet]
    public IActionResult Index()
    {
        return Ok();
    }
}
