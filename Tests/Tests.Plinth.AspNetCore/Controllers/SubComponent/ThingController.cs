﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Tests.Plinth.AspNetCore.Controllers.SubComponent;

[Produces("application/json")]
[Route("api/v1/Thing")]
public class ThingController : Controller
{
    [HttpGet]
    public IActionResult Index()
    {
        return Ok();
    }
}
