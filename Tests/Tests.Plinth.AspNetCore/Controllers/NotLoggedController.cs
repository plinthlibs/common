﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Plinth.AspNetCore.SecureToken;

namespace Tests.Plinth.AspNetCore.Controllers;

[Route("notlogged")]
[ApiController]
public class NotLoggedController : ControllerBase
{
    // GET api/values
    /// <summary>
    /// Get All
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [Route("")]
    public IActionResult GetAll()
    {
        return Ok(this.GetAuthenticatedUserName());
    }
}
