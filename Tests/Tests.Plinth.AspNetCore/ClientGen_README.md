﻿Using autorest

1. install nodejs at least 12+
2. `npm install -g nswag`
3. run this powershell


```powershell
$targetServiceSwaggerUrl = "http://localhost:5000/swagger/v1/swagger.json"
$targetServiceName = "NSwag"
$namespace = "Tests.Plinth.AspNetCore.Client"

$nswagParams = @(
    'openapi2csclient',
    '/runtime:net80',
    "/input:\`"${targetServiceSwaggerUrl}\`"",
    "/classname:${targetServiceName}Client",
    "/namespace:${namespace}",
    "/output:${targetServiceName}Client.cs",
    '/operationGenerationMode:SingleClientFromOperationId',
    '/generateClientInterfaces:true',
    '/injectHttpClient:true',
    '/useBaseUrl:false',
    '/disposeHttpClient:false',
    '/generateOptionalParameters:true'
    '/exceptionClass:NSwagException',
    '/generateExceptionClasses:true',
    '/parameterDateTimeFormat:o', # NSwag clients strip the offset information off of query parameter datetimeoffsets without this... Po
    '/generateNullableReferenceTypes:true',
    '/excludedParameterNames:X-Plinth-Auth'
)

nswag @nswagParams | Out-Host

# this fixes a big in nswag with IFormFileCollection
$content = Get-Content "${targetServiceName}Client.cs" -Raw
$content = $content -replace 'System.IO.Stream body = null', 'System.IO.Stream? body = null'
$content | Set-Content "${targetServiceName}Client.cs" -Encoding utf8 -NoNewline -Force
```

- docs for config options
https://github.com/RicoSuter/NSwag/wiki/NSwag-Configuration-Document
- or > nswag help openapi2csclient
