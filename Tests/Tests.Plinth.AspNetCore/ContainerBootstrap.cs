using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Tests.Plinth.AspNetCore.Client;
using Tests.Plinth.AspNetCore.Core2.Managers;
using Tests.Plinth.AspNetCore.Managers;

namespace Tests.Plinth.AspNetCore;

static class ContainerBootstrap
{
    public static void Register(IServiceCollection services)
    {
        services.AddSingleton(new Controllers.ResolvedThing(5));
        services.AddScoped<Controllers.ResolvedThingScoped>();

        services.AddSingleton(sp => sp.NewManagerFactory<TestManager>("myConfigSetting"));

        services.AddHttpApiClient<Controllers.RecursiveClient>(c =>
        {
            c.BaseAddress = new Uri("http://localhost:5000");
        });

        services.AddHttpApiHandlerClient<INSwagClient, NSwagClient>(
            "RecursiveNS",
            configureClient: c =>
            {
                c.BaseAddress = new Uri("http://localhost:5000");
            },
            configureSettings: c =>
            {
                c.MaxLoggingResponseBody = 100;
            });

        services.AddTransient(sp =>
            new Controllers.RecursiveClientRestSharp("RecursiveRS",
                "http://localhost:5000",
                sp.GetRequiredService<IHttpClientFactory>().CreateClient()));
    }
}
