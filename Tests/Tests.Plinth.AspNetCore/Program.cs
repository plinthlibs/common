﻿using System;
using Microsoft.Extensions.Logging;
using Plinth.Logging.NLog;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Tests.Plinth.AspNetCore;

public static class Program
{
    public static void Main(string[] args)
    {
        var log = StaticLogManagerSetup.BasicNLogSetup();

        log.Debug("startup!");

        try
        {
            CreateHostBuilder(args).Build().Run();
        }
        catch (Exception e)
        {
            log.Fatal(e, "failed during startup");
            throw;
        }
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
                webBuilder.CaptureStartupErrors(false);
                webBuilder.ConfigureLogging(builder => builder.AddNLog());
            });

}
