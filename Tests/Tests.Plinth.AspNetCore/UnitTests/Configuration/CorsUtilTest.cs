using Plinth.AspNetCore.Configuration;
using NUnit.Framework;
using Microsoft.AspNetCore.Cors.Infrastructure;

namespace Tests.Plinth.AspNetCore.UnitTests.Configuration;

[TestFixture]
public class CorsUtilTest
{
    [Test]
    [TestCase("http://foo.bar", "http://foo.bar")]
    [TestCase("http://foo.bar/", "http://foo.bar")]
    [TestCase("http://foo.bar/,http://foo.bar", "http://foo.bar")]
    [TestCase("http://foo.bar/,http://foo.bar/", "http://foo.bar")]
    [TestCase("http://foo.bar,http://foo.bar/", "http://foo.bar")]
    [TestCase("http://foo.bar , http://foo.bar/", "http://foo.bar")]
    [TestCase("http://foo.bar/ , http://foo.bar ", "http://foo.bar")]
    [TestCase("http://foo.bar/ , http://foo.bar/ ", "http://foo.bar")]
    [TestCase("http://foo.bar/ , http://bar.foo/ ", "http://bar.foo,http://foo.bar")]
    [TestCase("https://foo.bar/ , https://bar.foo/ ", "https://bar.foo,https://foo.bar")]
    [TestCase("https://foo.bar:8443/ , https://bar.foo:8443 ", "https://bar.foo:8443,https://foo.bar:8443")]
    public void Test_ParseOrigins(string input, string output)
    {
        Assert.That(string.Join(",", CorsUtil.ParseOrigins(input)), Is.EqualTo(output));
    }

    [Test]
    [TestCase(true)]
    [TestCase(false)]
    public async Task AllowAllOrigins_EmploysHack(bool callCustom)
    {
        bool called = false;

        // arrange
        var sc = new ServiceCollection();
        sc.AddCorsWithOrigins("*", callCustom ? c =>
        {
            called = true;
            c.WithExposedHeaders("my-custom-header");
        }
        : null);

        var sb = sc.BuildServiceProvider();

        var cors = sb.GetRequiredService<ICorsPolicyProvider>();

        // act
        var policy = await cors.GetPolicyAsync(new DefaultHttpContext(), null);

        // assert
        Assert.That(policy, Is.Not.Null);
        Assert.That(policy!.AllowAnyMethod);
        Assert.That(policy!.AllowAnyHeader);
        Assert.That(policy!.AllowAnyOrigin, Is.False);

        Assert.That(policy.IsOriginAllowed("abc"));
        Assert.That(policy.IsOriginAllowed("anything"));

        Assert.That(policy.SupportsCredentials);

        Assert.That(called, Is.EqualTo(callCustom));
        if (callCustom)
            Assert.That(policy.ExposedHeaders.Single(), Is.EqualTo("my-custom-header"));
    }

    [Test]
    [TestCase(true)]
    [TestCase(false)]
    public async Task AllowSpecificOrigins(bool callCustom)
    {
        bool called = false;

        // arrange
        var sc = new ServiceCollection();
        sc.AddCorsWithOrigins("http://localhost:4200,https://mysite.com", callCustom ? c =>
        {
            called = true;
            c.WithExposedHeaders("my-custom-header");
        }
        : null);

        var sb = sc.BuildServiceProvider();

        var cors = sb.GetRequiredService<ICorsPolicyProvider>();

        // act
        var policy = await cors.GetPolicyAsync(new DefaultHttpContext(), null);

        // assert
        Assert.That(policy, Is.Not.Null);
        Assert.That(policy!.AllowAnyMethod);
        Assert.That(policy!.AllowAnyHeader);
        Assert.That(policy!.AllowAnyOrigin, Is.False);

        Assert.That(policy.IsOriginAllowed("http://localhost:4200"));
        Assert.That(policy.IsOriginAllowed("https://mysite.com"));
        Assert.That(policy.IsOriginAllowed("http://localhost:8800"), Is.False);
        Assert.That(policy.IsOriginAllowed("http://mysite.com"), Is.False);

        Assert.That(policy.SupportsCredentials);

        Assert.That(called, Is.EqualTo(callCustom));
        if (callCustom)
            Assert.That(policy.ExposedHeaders.Single(), Is.EqualTo("my-custom-header"));
    }

    [Test]
    public async Task AllowSpecificOrigins_WithStar()
    {
        // arrange
        var sc = new ServiceCollection();
        sc.AddCorsWithOrigins("http://localhost:4200,*,https://mysite.com");

        var sb = sc.BuildServiceProvider();

        var cors = sb.GetRequiredService<ICorsPolicyProvider>();

        // act
        var policy = await cors.GetPolicyAsync(new DefaultHttpContext(), null);

        // assert
        Assert.That(policy, Is.Not.Null);
        Assert.That(policy!.AllowAnyMethod);
        Assert.That(policy!.AllowAnyHeader);
        Assert.That(policy!.AllowAnyOrigin);

        // allow any origin only puts '*' as a valid origin
        Assert.That(policy.IsOriginAllowed("*"));
        Assert.That(policy.IsOriginAllowed("http://localhost:4200"), Is.False);
        Assert.That(policy.IsOriginAllowed("https://mysite.com"), Is.False);
        Assert.That(policy.IsOriginAllowed("http://localhost:8800"), Is.False);
        Assert.That(policy.IsOriginAllowed("http://mysite.com"), Is.False);

        Assert.That(policy.SupportsCredentials, Is.False); // '*' in the origin list with other origins disables credentials
    }
}
