using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plinth.AspNetCore.Logging;
using NUnit.Framework;

namespace Tests.Plinth.AspNetCore.UnitTests.Logging;

[TestFixture]
public class ForkingWriteStreamTest
{
    private static void WriteString(Stream s, string str)
    {
        var b = Encoding.UTF8.GetBytes(str);
        s.Write(b, 0, b.Length);
    }

    private static async Task WriteStringAsync(Stream s, string str)
    {
        var b = Encoding.UTF8.GetBytes(str);
        await s.WriteAsync(b.AsMemory(0, b.Length));
    }

    [Test]
    public async Task TestWrite()
    {
        var inner = new MemoryStream(32);
        var fs = new ForkingWriteStream(inner, 16);

        Assert.That(fs.CanWrite);
        Assert.That(fs.CanRead, Is.False);
        Assert.That(fs.CanSeek, Is.False);
        Assert.That(fs.Length, Is.EqualTo(0));
        Assert.That(fs.Position, Is.EqualTo(0));
        Assert.Throws<NotSupportedException>(() => fs.Position = 5);
        fs.Flush();
        await fs.FlushAsync();
        Assert.Throws<NotSupportedException>(() => fs.Seek(0, SeekOrigin.Begin));
#if NET7_0_OR_GREATER
        Assert.Throws<NotSupportedException>(() => fs.ReadExactly(new byte[10], 0, 4));
#else
#pragma warning disable CA2022 // Avoid inexact read with 'Stream.Read'
        Assert.Throws<NotSupportedException>(() => fs.Read(new byte[10], 0, 4));
#pragma warning restore CA2022 // Avoid inexact read with 'Stream.Read'
#endif
        Assert.Throws<NotSupportedException>(() => fs.SetLength(0));

        fs.WriteByte((byte)'a');
        WriteString(fs, "-123--abc-");
        WriteString(fs, "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");

        string s = fs.GetMemoryAsString();
        Assert.That(s, Is.EqualTo("a-123--abc-12345"));

        Assert.That(fs.Length, Is.EqualTo(111));

        fs.Dispose();
    }

    [Test]
    public void TestWriteBig()
    {
        var inner = new MemoryStream(32);
        var fs = new ForkingWriteStream(inner, 16);

        WriteString(fs, "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");

        string s = fs.GetMemoryAsString();
        Assert.That(s, Is.EqualTo("1234567890123456"));

        Assert.That(fs.Length, Is.EqualTo(100));

        fs.Dispose();
    }

    [Test]
    public async Task TestWriteAsync()
    {
        var inner = new MemoryStream(32);
        var fs = new ForkingWriteStream(inner, 16);

        await WriteStringAsync(fs, "a-123--abc-");
        await WriteStringAsync(fs, "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");

        string s = fs.GetMemoryAsString();
        Assert.That(s, Is.EqualTo("a-123--abc-12345"));

        Assert.That(fs.Length, Is.EqualTo(111));

        fs.Dispose();
    }

    [Test]
    public async Task TestWriteBigAsync()
    {
        var inner = new MemoryStream(32);
        var fs = new ForkingWriteStream(inner, 16);

        await WriteStringAsync(fs, "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");

        string s = fs.GetMemoryAsString();
        Assert.That(s, Is.EqualTo("1234567890123456"));

        Assert.That(fs.Length, Is.EqualTo(100));

        fs.Dispose();
    }

    [Test]
    public void TestMidChar()
    {
        var inner = new MemoryStream(32);
        var fs = new ForkingWriteStream(inner, 16);

        WriteString(fs, "12345678901234" + '\u20AC' + "abcdef");

        string s = fs.GetMemoryAsString();
        Assert.That(s, Is.EqualTo("12345678901234"));

        Assert.That(fs.Length, Is.EqualTo(23));

        fs.Dispose();
    }

    [Test]
    public void Test3ByteChar()
    {
        var inner = new MemoryStream(32);
        var fs = new ForkingWriteStream(inner, 16);

        WriteString(fs, "1234567890123" + '\u20AC' + "abcdef");

        string s = fs.GetMemoryAsString();
        Assert.That(s, Is.EqualTo("1234567890123" + '\u20AC'));

        Assert.That(fs.Length, Is.EqualTo(22));

        fs.Dispose();
    }

    [Test]
    public async Task TestMidCharAsync()
    {
        var inner = new MemoryStream(32);
        var fs = new ForkingWriteStream(inner, 16);

        await WriteStringAsync(fs, "12345678901234" + '\u20AC' + "abcdef");

        string s = fs.GetMemoryAsString();
        Assert.That(s, Is.EqualTo("12345678901234"));

        Assert.That(fs.Length, Is.EqualTo(23));

        fs.Dispose();
    }

    [Test]
    public async Task Test3ByteCharAsync()
    {
        var inner = new MemoryStream(32);
        var fs = new ForkingWriteStream(inner, 16);

        await WriteStringAsync(fs, "1234567890123" + '\u20AC' + "abcdef");

        string s = fs.GetMemoryAsString();
        Assert.That(s, Is.EqualTo("1234567890123" + '\u20AC'));

        Assert.That(fs.Length, Is.EqualTo(22));

        fs.Dispose();
    }
}
