using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plinth.AspNetCore.Logging;
using NUnit.Framework;

namespace Tests.Plinth.AspNetCore.UnitTests.Logging;

[TestFixture]
public class ForkingReadStreamTest
{
    private static MemoryStream GetStream(string contents)
    {
        var b = Encoding.UTF8.GetBytes(contents);
        var s = new MemoryStream(b.Length);
        s.Write(b, 0, b.Length);
        s.Position = 0;
        return s;
    }

    private static string ReadString(Stream s)
    {
        using var sr = new StreamReader(s, Encoding.UTF8);
        return sr.ReadToEnd();
    }

    private static async Task<string> ReadStringAsync(Stream s)
    {
        using var sr = new StreamReader(s, Encoding.UTF8);
        return await sr.ReadToEndAsync();
    }

    [Test]
    public void TestStream()
    {
        var inner = GetStream("Small");
        var fs = new ForkingReadStream(inner, 128, inner.Length);

        Assert.That(fs.CanSeek, Is.False);
        Assert.That(fs.CanWrite, Is.False);
        Assert.That(fs.Length, Is.EqualTo(0));
        Assert.That(fs.Position, Is.EqualTo(0));
        Assert.Throws<NotSupportedException>(() => fs.Position = 5);
        Assert.Throws<NotSupportedException>(() => fs.Flush());
        Assert.Throws<NotSupportedException>(() => fs.Seek(0, SeekOrigin.Begin));
        Assert.Throws<NotSupportedException>(() => fs.Write(new byte[10], 0, 5));
        Assert.Throws<NotSupportedException>(() => fs.SetLength(0));
        Assert.ThrowsAsync<NotSupportedException>(async () => await fs.WriteAsync(new byte[10].AsMemory()));
#pragma warning disable CA1835 // Prefer the 'Memory'-based overloads for 'ReadAsync' and 'WriteAsync'
        Assert.ThrowsAsync<NotSupportedException>(async () => await fs.WriteAsync(new byte[10], 0, 5));
#pragma warning restore CA1835 // Prefer the 'Memory'-based overloads for 'ReadAsync' and 'WriteAsync'
        Assert.ThrowsAsync<NotSupportedException>(async () => await fs.FlushAsync());

        fs.Dispose();
    }

    [Test]
    public async Task TestSmallRead()
    {
        var inner = GetStream("Small");
        var fs = new ForkingReadStream(inner, 128, inner.Length);
        var s = await fs.ReadMemoryAsString();

        Assert.That(s, Is.EqualTo("Small"));
        Assert.That(ReadString(inner), Is.Empty);

        fs.Dispose();
    }

    [Test]
    public async Task TestBigRead()
    {
        var inner = GetStream("Bigger than 16 characters");
        var fs = new ForkingReadStream(inner, 16, inner.Length);
        var s = await fs.ReadMemoryAsString();

        Assert.That(s, Is.EqualTo("Bigger than 16 c"));
        Assert.That(ReadString(inner), Is.EqualTo("haracters"));

        fs.Dispose();
    }

    [Test]
    public async Task TestManualRead()
    {
        var inner = new MemoryStream(64000);
        for (int i = 0; i < 64000; i++)
            inner.WriteByte((byte)'x');
        inner.Position = 0;
        var fs = new ForkingReadStream(inner, 25000, inner.Length);
        var s = await fs.ReadMemoryAsString();
        Assert.That(s, Has.Length.EqualTo(25000));

        var ms = new MemoryStream();
        fs.CopyTo(ms, 4096);

        Assert.That(ms.Length, Is.EqualTo(64000));
        Assert.That(ms.ToArray().All(b => b == (byte)'x'));

        fs.Dispose();
    }

    [Test]
    public async Task TestManualReadAsync()
    {
        var inner = new MemoryStream(64000);
        for (int i = 0; i < 64000; i++)
            inner.WriteByte((byte)'x');
        inner.Position = 0;
        var fs = new ForkingReadStream(inner, 25000, inner.Length);
        var s = await fs.ReadMemoryAsString();
        Assert.That(s, Has.Length.EqualTo(25000));
        Assert.That(s.ToArray().All(b => b == 'x'));

        var ms = new MemoryStream();
        await fs.CopyToAsync(ms, 4096);

        Assert.That(ms.Length, Is.EqualTo(64000));
        Assert.That(ms.ToArray().All(b => b == (byte)'x'));

        fs.Dispose();
    }

    [Test]
    public async Task TestHugeRead()
    {
        var inner = new MemoryStream(64000);
        for (int i = 0; i < 64000; i++)
            inner.WriteByte((byte)'x');
        inner.Position = 0;
        var fs = new ForkingReadStream(inner, 45000, inner.Length); // bigger than internal bufffer (32k)
        var s = await fs.ReadMemoryAsString();
        Assert.That(s, Has.Length.EqualTo(45000));
        Assert.That(s.ToArray().All(b => b == 'x'));

        var ms = new MemoryStream();
        fs.CopyTo(ms, 4096);

        Assert.That(ms.Length, Is.EqualTo(64000));
        Assert.That(ms.ToArray().All(b => b == (byte)'x'));

        fs.Dispose();
    }

    [Test]
    public async Task TestZeroRead()
    {
        var inner = new MemoryStream(10)
        {
            Position = 0
        };
        var fs = new ForkingReadStream(inner, 100, inner.Length);
        var s = await fs.ReadMemoryAsString();
        Assert.That(s, Is.Empty);

        var ms = new MemoryStream();
        fs.CopyTo(ms, 4096);

        Assert.That(ms.Length, Is.EqualTo(0));

        fs.Dispose();
    }

    [Test]
    public async Task TestHugeReadAsync()
    {
        var inner = new MemoryStream(64000);
        for (int i = 0; i < 64000; i++)
            inner.WriteByte((byte)'x');
        inner.Position = 0;
        var fs = new ForkingReadStream(inner, 45000, inner.Length); // bigger than internal bufffer (32k)
        var s = await fs.ReadMemoryAsString();
        Assert.That(s, Has.Length.EqualTo(45000));
        Assert.That(s.ToArray().All(b => b == 'x'));

        var ms = new MemoryStream();
        await fs.CopyToAsync(ms, 4096);

        Assert.That(ms.Length, Is.EqualTo(64000));
        Assert.That(ms.ToArray().All(b => b == (byte)'x'));

        fs.Dispose();
    }

    [Test]
    public async Task TestMidChar()
    {
        var inner = GetStream("12345678901234" + '\u20AC' + "abcdef");
        var fs = new ForkingReadStream(inner, 16, inner.Length);
        var s = await fs.ReadMemoryAsString();

        Assert.That(s, Is.EqualTo("12345678901234"));
        Assert.That(ReadString(fs), Is.EqualTo("12345678901234" + '\u20AC' + "abcdef"));
    }

    [Test]
    public async Task Test3ByteChar()
    {
        var inner = GetStream("1234567890123" + '\u20AC' + "abcdef");
        var fs = new ForkingReadStream(inner, 16, inner.Length);
        var s = await fs.ReadMemoryAsString();

        Assert.That(s, Is.EqualTo("1234567890123" + '\u20AC'));
        Assert.That(ReadString(fs), Is.EqualTo("1234567890123" + '\u20AC' + "abcdef"));
    }

    [Test]
    public async Task TestMidCharAsync()
    {
        var inner = GetStream("12345678901234" + '\u20AC' + "abcdef");
        var fs = new ForkingReadStream(inner, 16, inner.Length);
        var s = await fs.ReadMemoryAsString();

        Assert.That(s, Is.EqualTo("12345678901234"));
        Assert.That(await ReadStringAsync(fs), Is.EqualTo("12345678901234" + '\u20AC' + "abcdef"));
    }

    [Test]
    public async Task Test3ByteCharAsync()
    {
        var inner = GetStream("1234567890123" + '\u20AC' + "abcdef");
        var fs = new ForkingReadStream(inner, 16, inner.Length);
        var s = await fs.ReadMemoryAsString();

        Assert.That(s, Is.EqualTo("1234567890123" + '\u20AC'));
        Assert.That(await ReadStringAsync(fs), Is.EqualTo("1234567890123" + '\u20AC' + "abcdef"));
    }
}
