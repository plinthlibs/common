using NUnit.Framework;
using Plinth.AspNetCore.Logging;
using Microsoft.Extensions.Options;
using System.Text;
using Plinth.Common.Exceptions;
using System.Net;
using Plinth.Common.Constants;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.Net.Http.Headers;

namespace Tests.Plinth.AspNetCore.UnitTests.Logging;

[TestFixture]
public class UnhandledExceptionMiddlewareTest
{
    [Test]
    public async Task NoException_NoAction()
    {
        // arrange
        var context = CreateContext();
        var middleware = new UnhandledExceptionMiddleware(
            (context) =>
            {
                context.Response.StatusCode = 200;
                context.Response.Body.Write(Encoding.UTF8.GetBytes("hello"));
                context.Response.ContentType = ContentTypes.TextPlain;
                context.Response.Headers.Append("X-My-Header", "value");
                return Task.CompletedTask;
            },
            CreateOptions());

        // act
        await middleware.Invoke(context);

        // assert
        Assert.That(context.Response.StatusCode, Is.EqualTo(200));
        Assert.That(context.Response.ContentType, Is.EqualTo(ContentTypes.TextPlain));
        Assert.That(GetResponseContent(context), Is.EqualTo("hello"));
        Assert.That(context.Response.Headers, Does.ContainKey("X-My-Header"));
        Assert.That(context.Response.Headers["X-My-Header"], Is.EqualTo("value"));
    }

    [Test]
    [TestCase(typeof(Exception), 500)]
    [TestCase(typeof(LogicalBadRequestException), 400)]
    [TestCase(typeof(LogicalForbiddenException), 403)]
    [TestCase(typeof(System.Security.SecurityException), 403)]
    [TestCase(typeof(global::Plinth.Security.SecurityException), 403)]
    [TestCase(typeof(LogicalNotFoundException), 404)]
    [TestCase(typeof(LogicalConflictException), 409)]
    [TestCase(typeof(LogicalPreconditionFailedException), 412)]
    public async Task Throws_SpecificException_ReturnsSpecificCode(Type exceptionType, int statusCode)
    {
        // arrange
        var context = CreateContext();
        var middleware = new UnhandledExceptionMiddleware(
            (context) =>
            {
                throw (exceptionType.GetConstructor([typeof(string)])!.Invoke(["failed"]) as Exception)!;
            },
            CreateOptions());

        // act
        await middleware.Invoke(context);

        // assert
        Assert.That(context.Response.StatusCode, Is.EqualTo(statusCode));
        Assert.That(context.Response.ContentType, Is.EqualTo(ContentTypes.ApplicationJsonUtf8));
        Assert.That(GetResponseContent(context), Is.EqualTo("{\"message\":\"failed\"}"));
    }

    [Test]
    public async Task Throws_LogicalExceptionCustomContent_ReturnsCustom()
    {
        // arrange
        var context = CreateContext();
        var middleware = new UnhandledExceptionMiddleware(
            (context) =>
            {
                throw new LogicalBadRequestException(new { myThing = "bad" });
            },
            CreateOptions());

        // act
        await middleware.Invoke(context);

        // assert
        Assert.That(context.Response.StatusCode, Is.EqualTo(400));
        Assert.That(context.Response.ContentType, Is.EqualTo(ContentTypes.ApplicationJsonUtf8));
        Assert.That(GetResponseContent(context), Is.EqualTo("{\"myThing\":\"bad\"}"));
    }

    [Test]
    public async Task Throws_LogicalExceptionNullCustomContent_ReturnsDefault()
    {
        // arrange
        var context = CreateContext();
        var middleware = new UnhandledExceptionMiddleware(
            (context) =>
            {
                throw new LogicalBadRequestException(null!);
            },
            CreateOptions());

        // act
        await middleware.Invoke(context);

        // assert
        Assert.That(context.Response.StatusCode, Is.EqualTo(400));
        Assert.That(context.Response.ContentType, Is.EqualTo(ContentTypes.ApplicationJsonUtf8));
        Assert.That(GetResponseContent(context), Is.EqualTo("{\"message\":\"Exception of type 'Plinth.Common.Exceptions.LogicalBadRequestException' was thrown.\"}"));
    }

    [Test]
    public async Task Throws_Exception_ReturnsWebAppResponse()
    {
        // arrange
        var context = CreateContext();
        var middleware = new UnhandledExceptionMiddleware(
            (context) =>
            {
                throw new LogicalBadRequestException("missing");
            },
            CreateOptions(c => c.UseWebAppResponse = true));

        // act
        await middleware.Invoke(context);

        // assert
        Assert.That(context.Response.StatusCode, Is.EqualTo(400));
        Assert.That(context.Response.ContentType, Is.EqualTo(ContentTypes.ApplicationJsonUtf8));
        Assert.That(GetResponseContent(context), Is.EqualTo("{\"Success\":false,\"Errors\":[{\"ErrorCode\":\"ServerError\",\"ErrorDescription\":\"missing\"}]}"));
    }

    [Test]
    [TestCase(404)]
    [TestCase(500)]
    [TestCase(200)]
    public async Task Throws_RestException_ReturnsSameCode(int statusCode)
    {
        // arrange
        var context = CreateContext();
        var middleware = new UnhandledExceptionMiddleware(
            (context) =>
            {
                throw new global::Plinth.HttpApiClient.Common.RestException((HttpStatusCode)statusCode, "failed", null);
            },
            CreateOptions());

        // act
        await middleware.Invoke(context);

        // assert
        Assert.That(context.Response.StatusCode, Is.EqualTo(statusCode));
        Assert.That(context.Response.ContentType, Is.EqualTo(ContentTypes.ApplicationJsonUtf8));
        Assert.That(GetResponseContent(context), Is.EqualTo("{\"message\":\"failed\"}"));
    }

    [Test]
    public async Task Throws_Exception_ClearsResponse()
    {
        // arrange
        var context = CreateContext();
        var middleware = new UnhandledExceptionMiddleware(
            (context) =>
            {
                context.Response.StatusCode = 200;
                context.Response.Body.Write(Encoding.UTF8.GetBytes("hello"));
                context.Response.ContentType = ContentTypes.TextPlain;
                context.Response.Headers.Append("X-My-Header", "value");

                throw new Exception("failed");
            },
            CreateOptions());

        // act
        await middleware.Invoke(context);

        // assert
        Assert.That(context.Response.StatusCode, Is.EqualTo(500));
        Assert.That(context.Response.ContentType, Is.EqualTo(ContentTypes.ApplicationJsonUtf8));
        Assert.That(GetResponseContent(context), Is.EqualTo("{\"message\":\"failed\"}"));
        Assert.That(context.Response.Headers, Does.Not.ContainKey("X-My-Header"));
    }

    [Test]
    public void ThrowsException_AfterStarted_Unhandled()
    {
        // arrange
        var context = CreateContext();
        var middleware = new UnhandledExceptionMiddleware(
            (context) =>
            {
                context.Response.StatusCode = 200;
                context.Response.Body.Write(Encoding.UTF8.GetBytes("hello"));
                context.Response.ContentType = ContentTypes.TextPlain;
                context.Response.Headers.Append("X-My-Header", "value");

                (context.Features.Get<IHttpResponseFeature>() as DummyResponseFeature)?.InvokeCallBack();

                throw new Exception("failed");
            },
            CreateOptions());

        // act/assert
        Assert.ThrowsAsync<Exception>(async () => await middleware.Invoke(context));

        // assert
        Assert.That(context.Response.StatusCode, Is.EqualTo(200));
        Assert.That(context.Response.ContentType, Is.EqualTo(ContentTypes.TextPlain));
        Assert.That(GetResponseContent(context), Is.EqualTo("hello"));
        Assert.That(context.Response.Headers, Does.ContainKey("X-My-Header"));
        Assert.That(context.Response.Headers["X-My-Header"], Is.EqualTo("value"));
    }

    [Test]
    public async Task Throws_Exception_MaintainsCors()
    {
        // arrange
        var context = CreateContext();
        var middleware = new UnhandledExceptionMiddleware(
            (context) =>
            {
                (context.Features.Get<IHttpResponseFeature>() as DummyResponseFeature)?.InvokeCallbackNotStarted();

                context.Response.StatusCode = 200;
                context.Response.Body.Write(Encoding.UTF8.GetBytes("hello"));
                context.Response.ContentType = ContentTypes.TextPlain;
                context.Response.Headers.Append("X-My-Header", "value");
                context.Response.Headers.Append("X-My-Other-Header", "value2");

                throw new Exception("failed");
            },
            CreateOptions());

        #region cors middleware setup
        var corsOptions = Options.Create(new CorsOptions());
        var policyBuilder = new CorsPolicyBuilder()
             .AllowAnyOrigin()
             .WithOrigins("https://mysite.com")
             .AllowCredentials()
             .WithExposedHeaders("X-My-Header","X-My-Other-Header")
             .AllowAnyMethod();
        corsOptions.Value.AddDefaultPolicy(policyBuilder.Build());

        var corsMiddleware = new CorsMiddleware(middleware.Invoke, new CorsService(corsOptions, new MockLoggerFactory()), new MockLoggerFactory());
        var policyProvider = new DefaultCorsPolicyProvider(corsOptions);
        #endregion

        context.Request.Headers.Append(CorsConstants.Origin, "https://mysite.com");
        context.Request.Headers.Authorization = "Bearer abcdef";
        context.Request.Method = HttpMethods.Get;

        // act
        await corsMiddleware.Invoke(context, policyProvider);

        // assert
        Assert.That(context.Response.StatusCode, Is.EqualTo(500));
        Assert.That(context.Response.ContentType, Is.EqualTo(ContentTypes.ApplicationJsonUtf8));
        Assert.That(GetResponseContent(context), Is.EqualTo("{\"message\":\"failed\"}"));
        Assert.That(context.Response.Headers[CorsConstants.AccessControlAllowOrigin].FirstOrDefault(), Is.EqualTo("https://mysite.com"));
        Assert.That(context.Response.Headers[CorsConstants.AccessControlAllowCredentials].FirstOrDefault(), Is.EqualTo("true"));
        Assert.That(context.Response.Headers[HeaderNames.Vary].FirstOrDefault(), Is.EqualTo("Origin"));
        Assert.That(context.Response.Headers[CorsConstants.AccessControlExposeHeaders].FirstOrDefault(), Is.EqualTo("X-My-Header,X-My-Other-Header"));
    }

    #region mock logger for cors middleware
    private class MockLogger : ILogger
    {
        private class MockIDisposable : IDisposable
        {
            public void Dispose() { }
        }

        public IDisposable? BeginScope<TState>(TState state) where TState : notnull => new MockIDisposable();

        public bool IsEnabled(LogLevel logLevel) => true;

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception? exception, Func<TState, Exception?, string> formatter)
        {
        }
    }

    private class MockLoggerFactory : ILoggerFactory
    {
        public void AddProvider(ILoggerProvider provider)
        {
        }

        public ILogger CreateLogger(string categoryName) => new MockLogger();

        public void Dispose() { }
    }
    #endregion

    [Test]
    public void ThrowsException_NoDefaultHandler_Unhandled()
    {
        // arrange
        var context = CreateContext();
        var middleware = new UnhandledExceptionMiddleware(
            (context) =>
            {
                context.Response.StatusCode = 200;
                throw new Exception("failed");
            },
            CreateOptions(c => c.UseDefaultHandler = false));

        // act/assert
        Assert.ThrowsAsync<Exception>(async () => await middleware.Invoke(context));

        // assert
        Assert.That(context.Response.StatusCode, Is.EqualTo(200));
    }

    [Test]
    public void ThrowsException_IgnoreResponse_Unhandled()
    {
        // arrange
        var context = CreateContext();
        var middleware = new UnhandledExceptionMiddleware(
            (context) =>
            {
                context.Response.StatusCode = 200;
                throw new Exception("failed");
            },
            CreateOptions(c => c.IgnoreResponse = true));

        // act/assert
        Assert.ThrowsAsync<Exception>(async () => await middleware.Invoke(context));

        // assert
        Assert.That(context.Response.StatusCode, Is.EqualTo(200));
    }

    [Test]
    public async Task Throws_CustomHandler_ReturnsCustom()
    {
        // arrange
        var context = CreateContext();
        var middleware = new UnhandledExceptionMiddleware(
            (context) =>
            {
                throw new InvalidCastException();
            },
            CreateOptions(c => c.CustomHandler = (e, l) =>
            {
                if (e is InvalidCastException)
                {
                    return new HandleExceptionResponse { StatusCode = 415, Content = "fourFifteen" };
                }
                return null;
            }));

        // act
        await middleware.Invoke(context);

        // assert
        Assert.That(context.Response.StatusCode, Is.EqualTo(415));
        Assert.That(context.Response.ContentType, Is.EqualTo(ContentTypes.ApplicationJsonUtf8));
        Assert.That(GetResponseContent(context), Is.EqualTo("{\"message\":\"fourFifteen\"}"));
    }

    [Test]
    public async Task Throws_CustomHandler_ReturnsCustomContent()
    {
        // arrange
        var context = CreateContext();
        var middleware = new UnhandledExceptionMiddleware(
            (context) =>
            {
                throw new InvalidCastException();
            },
            CreateOptions(c => c.CustomHandler = (e, l) =>
            {
                if (e is InvalidCastException)
                {
                    return new HandleExceptionResponse { StatusCode = 415, Content = new { myCode = 415 } };
                }
                return null;
            }));

        // act
        await middleware.Invoke(context);

        // assert
        Assert.That(context.Response.StatusCode, Is.EqualTo(415));
        Assert.That(context.Response.ContentType, Is.EqualTo(ContentTypes.ApplicationJsonUtf8));
        Assert.That(GetResponseContent(context), Is.EqualTo("{\"myCode\":415}"));
    }

    [Test]
    public async Task Throws_CustomHandlerDoesntHandle_ReturnsDefault()
    {
        // arrange
        var context = CreateContext();
        var middleware = new UnhandledExceptionMiddleware(
            (context) =>
            {
                throw new Exception("failed");
            },
            CreateOptions(c => c.CustomHandler = (e, l) =>
            {
                if (e is InvalidCastException)
                {
                    return new HandleExceptionResponse { StatusCode = 415, Content = "fourFifteen" };
                }
                return null;
            }));

        // act
        await middleware.Invoke(context);

        // assert
        Assert.That(context.Response.StatusCode, Is.EqualTo(500));
        Assert.That(context.Response.ContentType, Is.EqualTo(ContentTypes.ApplicationJsonUtf8));
        Assert.That(GetResponseContent(context), Is.EqualTo("{\"message\":\"failed\"}"));
    }

    private static HttpContext CreateContext()
    {
        var stream = new MemoryStream();
        var featureCollection = new FeatureCollection(10);
        featureCollection.Set<IHttpRequestFeature>(new HttpRequestFeature());
        featureCollection.Set<IHttpResponseFeature>(new DummyResponseFeature(stream));
        featureCollection.Set<IHttpResponseBodyFeature>(new StreamResponseBodyFeature(stream));
        var context = new DefaultHttpContext(featureCollection);
        return context;
    }

    private static string GetResponseContent(HttpContext context)
    {
        context.Response.Body.Position = 0;
        return new StreamReader(context.Response.Body).ReadToEnd();
    }

    private static IOptions<UnhandledExceptionMiddleware.Options> CreateOptions(Action<UnhandledExceptionMiddleware.Options>? configure = null)
    {
        var o = new UnhandledExceptionMiddleware.Options() { UseDefaultHandler = true };
        configure?.Invoke(o);
        return Options.Create(o);
    }

    private class DummyResponseFeature : IHttpResponseFeature
    {
        public DummyResponseFeature(Stream body)
        {
            Body = body;
        }

        public Stream Body { get; set; }

        public bool HasStarted { get { return hasStarted; } }

        public IHeaderDictionary Headers { get; set; } = new HeaderDictionary();

        public string? ReasonPhrase { get; set; }

        public int StatusCode { get; set; }

        public void OnCompleted(Func<object, Task> callback, object state)
        {
        }

        public void OnStarting(Func<object, Task> callback, object state)
        {
            this.callback = callback;
            this.state = state;
        }

        bool hasStarted = false;
        Func<object, Task>? callback;
        object? state;

        public Task InvokeCallBack()
        {
            hasStarted = true;
            return callback?.Invoke(state ?? new object()) ?? Task.CompletedTask;
        }

        public Task InvokeCallbackNotStarted()
        {
            return callback?.Invoke(state ?? new object()) ?? Task.CompletedTask;
        }
    }
}
