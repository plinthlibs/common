using Plinth.AspNetCore.Logging;
using NUnit.Framework;

namespace Tests.Plinth.AspNetCore.UnitTests.Logging;

[TestFixture]
public class OperationFormatterTest
{
    [Test]
    [TestCase("Plinth.Component.Service.Controllers.Service.SubService", "Thing", "Service.SubService.ThingController")]
    [TestCase("Plinth.Component.Service.Controllers", "Thing", "ThingController")]
    [TestCase("Something.Weird", "Weird", "WeirdController")]
    [TestCase("NoNamespace", "No", "NoController")]
    public void TestFormat(string nameSpace, string controller, string expected)
    {
        Assert.That(OperationFormatter.FormOperationString("ActionName", nameSpace, controller), Is.EqualTo(expected + "::ActionName"));
    }

    [TestCase(null, null, null, "UnknownController::UnknownAction")]
    [TestCase(null, null, "MyAction", "UnknownController::MyAction")]
    [TestCase(null, "Thing", "MyAction", "ThingController::MyAction")]
    [TestCase("NameSpace.Controllers", null, "MyAction", "UnknownController::MyAction")]
    [TestCase("Controllers", "Thing", "MyAction", "ThingController::MyAction")]
    [TestCase("Controllers.Place", "Thing", "MyAction", "Place.ThingController::MyAction")]
    [TestCase("Plinth.Component.Service.Controllers.Something", "My", null, "Something.MyController::UnknownAction")]
    public void TestFormat2(string? nameSpace, string? controller, string? actionName, string expected)
    {
        Assert.That(OperationFormatter.FormOperationString(actionName, nameSpace, controller), Is.EqualTo(expected));
    }
}
