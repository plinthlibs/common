﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plinth.AspNetCore.Logging;
using NUnit.Framework;

namespace Tests.Plinth.AspNetCore.UnitTests.Logging;

[TestFixture]
class ContentPreProcessorTest
{
    [Test]
    public void TestPreProcess()
    {
        var cp = new ContentPreProcessor();

        cp.AddRequestProcessor("/api/v1/test", r => r + "ABC");
        cp.AddResponseProcessor("/api/v1/test", r => r + "DEF");

        string? content = "";

        content = "{some req}";
        Assert.That(cp.PreProcessRequest("/api/v1/test", ref content));
        Assert.That(content, Is.EqualTo("{some req}ABC"));

        content = "{some resp}";
        Assert.That(cp.PreProcessResponse("/api/v1/test", ref content));
        Assert.That(content, Is.EqualTo("{some resp}DEF"));
    }

    [Test]
    public void TestReturnNull()
    {
        var cp = new ContentPreProcessor();

        cp.AddRequestProcessor("/api/v1/test", r => null!);
        cp.AddResponseProcessor("/api/v1/test", r => null!);

        string? content = "";

        content = "{some req}";
        Assert.That(cp.PreProcessRequest("/api/v1/test", ref content));
        Assert.That(content, Is.Null);

        content = "{some resp}";
        Assert.That(cp.PreProcessResponse("/api/v1/test", ref content));
        Assert.That(content, Is.Null);
    }

    [Test]
    public void TestNotUsed()
    {
        var cp = new ContentPreProcessor();

        string? content;

        content = "{some req}";
        Assert.That(cp.PreProcessRequest("/api/v1/test", ref content), Is.False);
        Assert.That(content, Is.EqualTo("{some req}"));

        content = "{some resp}";
        Assert.That(cp.PreProcessResponse("/api/v1/test", ref content), Is.False);
        Assert.That(content, Is.EqualTo("{some resp}"));
    }

    [Test]
    public void TestWrongApi()
    {
        var cp = new ContentPreProcessor();

        cp.AddRequestProcessor("/api/v1/test", r => r + "ABC");
        cp.AddResponseProcessor("/api/v1/test", r => r + "DEF");

        string? content = "";

        content = "{some req}";
        Assert.That(cp.PreProcessRequest("/api/v1/nottest", ref content), Is.False);
        Assert.That(content, Is.EqualTo("{some req}"));

        content = "{some resp}";
        Assert.That(cp.PreProcessResponse("/api/v1/nottest", ref content), Is.False);
        Assert.That(content, Is.EqualTo("{some resp}"));
    }

    [Test]
    public void TestInvalid()
    {
        var cp = new ContentPreProcessor();

        Assert.Throws<ArgumentNullException>(() => cp.AddRequestProcessor(null!, r => r));
        Assert.Throws<ArgumentNullException>(() => cp.AddRequestProcessor(null!, null!));

        Assert.Throws<ArgumentNullException>(() => cp.AddRequestProcessor("", null!));
        Assert.Throws<ArgumentException>(() => cp.AddRequestProcessor("noslash", null!));

        Assert.Throws<ArgumentNullException>(() => cp.AddRequestProcessor("/api", null!));
    }
}
