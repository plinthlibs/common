using NUnit.Framework;
using Plinth.AspNetCore.TokenAuth;
using Microsoft.Extensions.Primitives;

namespace Tests.Plinth.AspNetCore.UnitTests.TokenAuth;

[TestFixture]
public class TokenAuthQueryStringHandlerTests
{
    [Test]
    public void GetTokenFromQuery_NoHeaderNoQuery_Null()
    {
        var httpCtx = new DefaultHttpContext();
        Assert.That(TokenAuthQueryStringHandler.GetTokenFromQueryString(httpCtx.Request, "access_token"), Is.Null);
    }

    [Test]
    [TestCase(null, "1234abcd", "1234abcd")]
    [TestCase("1234abcd", null, null)]
    public void GetTokenFromQuery_Values_Extracts(string? header, string? query, string? result)
    {
        var httpCtx = new DefaultHttpContext();

        if (header != null)
            httpCtx.Request.Headers.Authorization = new StringValues(header);
        if (query != null)
            httpCtx.Request.QueryString = new QueryString($"?access_token={query}");

        Assert.That(TokenAuthQueryStringHandler.GetTokenFromQueryString(httpCtx.Request, "access_token"), Is.EqualTo(result));
    }

    [Test]
    public void GetTokenFromQuery_WrongQueryParam_Null()
    {
        var httpCtx = new DefaultHttpContext();

        httpCtx.Request.QueryString = new QueryString($"?not_right=1234");

        Assert.That(TokenAuthQueryStringHandler.GetTokenFromQueryString(httpCtx.Request, "access_token"), Is.Null);
    }

    [Test]
    public void GetTokenFromQuery_MultiQueryParam_Extracts()
    {
        var httpCtx = new DefaultHttpContext();

        httpCtx.Request.QueryString = new QueryString($"?not_right=1234&access_token=3456abcd");

        Assert.That(TokenAuthQueryStringHandler.GetTokenFromQueryString(httpCtx.Request, "access_token"), Is.EqualTo("3456abcd"));
    }

}
