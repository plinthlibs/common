using Plinth.AspNetCore.SecureToken;
using Plinth.Common.Utils;
using NUnit.Framework;
using Plinth.AspNetCore.TokenAuth;

namespace Tests.Plinth.AspNetCore.UnitTests.TokenAuth;

[TestFixture]
public class TokenHandlerUtilTests
{
    [Test]
    public void GetTokenFromAuthHeader_NullsEmpty_False()
    {
        Assert.That(TokenHandlerUtil.GetTokenFromAuthHeader(null, null!, null, false, out var _), Is.False);
        Assert.That(TokenHandlerUtil.GetTokenFromAuthHeader(string.Empty, null!, null, false, out var _), Is.False);
        Assert.That(TokenHandlerUtil.GetTokenFromAuthHeader(string.Empty, null!, string.Empty, false, out var _), Is.False);
        Assert.That(TokenHandlerUtil.GetTokenFromAuthHeader(null, null!, string.Empty, false, out var _), Is.False);
    }

    [Test]
    [TestCase("Bearer 123token456")]
    [TestCase("Bearer  123token456")]
    [TestCase("Bearer 123token456 ")]
    [TestCase("Bearer  123token456 ")]
    [TestCase("bEaReR 123token456")]
    [TestCase("Bearer 123token456")]
    [TestCase("BeArEr   123token456  ")]
    public void GetTokenFromAuthHeader_WithBearer_Succeeds(string h)
    {
        Assert.That(TokenHandlerUtil.GetTokenFromAuthHeader(h, "Bearer", null!, false, out var tk));
        Assert.That(tk, Is.EqualTo("123token456"));

        Assert.That(TokenHandlerUtil.GetTokenFromAuthHeader(h, "Bearer", h + "notused", false, out tk));
        Assert.That(tk, Is.EqualTo("123token456"));
    }

    [Test]
    [TestCase("Other 123token456", "Other")]
    [TestCase("Other  123token456", "Other")]
    [TestCase("Other 123token456 ", "Other")]
    [TestCase("Other  123token456 ", "Other")]
    [TestCase("oThEr 123token456", "Other")]
    [TestCase("Other 123token456", "Other")]
    [TestCase("OtHeR   123token456  ", "Other")]
    public void GetTokenFromAuthHeader_WithCustom_Succeeds(string h, string scheme)
    {
        Assert.That(TokenHandlerUtil.GetTokenFromAuthHeader(h, scheme, null!, false, out var tk));
        Assert.That(tk, Is.EqualTo("123token456"));

        Assert.That(TokenHandlerUtil.GetTokenFromAuthHeader(h, scheme, h + "notused", false, out tk));
        Assert.That(tk, Is.EqualTo("123token456"));
    }

    [TestCase("Other 123token456", "Other")]
    [TestCase("Other  123token456", "Other")]
    [TestCase("Other 123token456 ", "Other")]
    [TestCase("Other  123token456 ", "Other")]
    [TestCase("oThEr 123token456", "Other")]
    [TestCase("Other 123token456", "Other")]
    [TestCase("OtHeR   123token456  ", "Other")]
    [TestCase("Bearer 123token456", "Bearer")]
    [TestCase("Bearer  123token456", "Bearer")]
    [TestCase("Bearer 123token456 ", "Bearer")]
    [TestCase("Bearer  123token456 ", "Bearer")]
    [TestCase("bEaReR 123token456", "Bearer")]
    [TestCase("Bearer 123token456", "Bearer")]
    [TestCase("BeArEr   123token456  ", "Bearer")]
    [TestCase("123token456", "Other")]
    [TestCase("  123token456", "Other")]
    [TestCase("123token456 ", "Other")]
    [TestCase("  123token456 ", "Other")]
    [TestCase("123token456", "Other")]
    [TestCase("   123token456  ", "Other")]
    public void GetTokenFromAuthHeader_WithPlinth_Succeeds(string h, string scheme)
    {
        Assert.That(TokenHandlerUtil.GetTokenFromAuthHeader(null, scheme, h, false, out var tk));
        Assert.That(tk, Is.EqualTo("123token456"));
    }

    [TestCase("Bearer 123token456", "Other")]
    [TestCase("Bearer  123token456", "Other")]
    [TestCase("Bearer 123token456 ", "Other")]
    [TestCase("Bearer  123token456 ", "Other")]
    [TestCase("bEaReR 123token456", "Other")]
    [TestCase("BeArEr   123token456  ", "Other")]
    public void GetTokenFromAuthHeader_WrongSchemeWithBearer_Succeeds(string h, string scheme)
    {
        Assert.That(TokenHandlerUtil.GetTokenFromAuthHeader(h, scheme, null, false, out var tk));
        Assert.That(tk, Is.EqualTo("123token456"));
    }

    [Test]
    [TestCase("Random 123token456", "Other")]
    [TestCase("Random  123token456", "Other")]
    [TestCase("Random 123token456 ", "Other")]
    [TestCase("Random  123token456 ", "Other")]
    [TestCase("RaNdOm 123token456", "Other")]
    [TestCase("rAnDoM   123token456  ", "Other")]
    public void GetTokenFromAuthHeader_WrongScheme_False(string h, string scheme)
    {
        Assert.That(TokenHandlerUtil.GetTokenFromAuthHeader(h, scheme, null, true, out var _), Is.False);

        Assert.That(TokenHandlerUtil.GetTokenFromAuthHeader(h, scheme, h + "notused", true, out _), Is.False);
    }

    [Test]
    [TestCase("Bearer ")]
    [TestCase("Bearer")]
    [TestCase("")]
    [TestCase(" ")]
    [TestCase("noscheme")]
    [TestCase("  noscheme")]
    [TestCase("wrongscheme ")]
    [TestCase("  noscheme ")]
    [TestCase("noscheme")]
    [TestCase("   noscheme  ")]
    public void GetTokenFromAuthHeader_InvalidHeader_False(string h)
    {
        Assert.That(TokenHandlerUtil.GetTokenFromAuthHeader(h, "Bearer", null, true, out var _), Is.False);
        Assert.That(TokenHandlerUtil.GetTokenFromAuthHeader(h, "Bearer", null, true, out var _), Is.False);
    }
}
