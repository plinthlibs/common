using Newtonsoft.Json;
using NUnit.Framework;
using Plinth.AspNetCore.AutoEndpoints.Version;
using Plinth.AspNetCore.AutoEndpoints.Version.Models;

namespace Tests.Plinth.AspNetCore.UnitTests.AutoEndpoints.Version;

[TestFixture]
public class VersionGeneratorTest
{
    [Test]
    public void Test_FetchVersionResponse_WhenFlagIsNotDefined()
    {
        var uri = new Uri("http://localhost:1234/api/blah");
        //fetch default element keys
        var expected = JsonConvert.DeserializeObject<Dictionary<string, object?>>(JsonConvert.SerializeObject(new VersionResponseModel()));
        //action
        var generator = new VersionGenerator(new VersionBaseConfig());
        var actual = JsonConvert.DeserializeObject<Dictionary<string, object?>>(JsonConvert.SerializeObject(generator.GetVersionResponse(uri)));
        //assert
        expected?.Keys.ToList().ForEach(k =>
        {
            Assert.That(actual, Is.Not.Null);
            Assert.That(actual!.ContainsKey(k), k);
        });
    }

    [Test]
    public void Test_FetchVersionResponse_WhenFlagIsNone()
    {
        var uri = new Uri("http://localhost:1234/api/blah");
        //fetch default element keys
        var expected = JsonConvert.DeserializeObject<Dictionary<string, object?>>(JsonConvert.SerializeObject(new VersionResponseModel()));
        //action
        var generator = new VersionGenerator(new VersionBaseConfig { DisabledFields = VersionField.None });
        var actual = JsonConvert.DeserializeObject<Dictionary<string, object?>>(JsonConvert.SerializeObject(generator.GetVersionResponse(uri)));
        Console.WriteLine($"({JsonConvert.SerializeObject(expected)}, {JsonConvert.SerializeObject(actual)})");
        //assert
        expected?.Keys.ToList().ForEach(k =>
        {
            Assert.That(actual!, Is.Not.Null);
            Assert.That(actual.ContainsKey(k), k);
        });
    }

    [Test]
    public void Test_FetchVersionResponse_WhenFlagIsDefined()
    {
        var uri = new Uri("http://localhost:1234/api/blah");
        var expected = new List<string> { "environment", "machine", "machineUpTime", "process", "processUpTime", "host", "port" };
        var inFlag = VersionField.Environment | VersionField.MachineName | VersionField.MachineUpTime | VersionField.ProcessName | VersionField.ProcessUpTime | VersionField.RequestHost | VersionField.RequestPort;

        //action - serialize via plinth json util
        var config = new VersionBaseConfig(); config.SetDisabledFields(inFlag);
        var generator = new VersionGenerator(config);
        var actual = JsonConvert.DeserializeObject<Dictionary<string, object?>>(JsonConvert.SerializeObject(generator.GetVersionResponse(uri)));
        Console.WriteLine($"({JsonConvert.SerializeObject(expected)}, {JsonConvert.SerializeObject(actual)})");
        //assert
        expected.ForEach(k =>
        {
            var x = actual?.GetValueOrDefault(k, null);
            Assert.That(x == null || (x is string && VersionResponseModel.NotSet.Equals(x)), k);
        });
    }

    [Test]
    public void Test_FetchVersionResponse_WhenFlagIsAllDefined()
    {
        var uri = new Uri("http://localhost:1234/api/blah");
        var expected = new List<string> { "environment", "component", "assembly", "version", "release", "built", "machine", "machineUpTime", "process", "processUpTime", "runtime", "netcore", "host", "port", "db" };
        var inFlag = (VersionField)~0;

        //action - serialize via plinth json util
        var config = new VersionBaseConfig(); config.SetDisabledFields(inFlag);
        var generator = new VersionGenerator(config);
        var actual = JsonConvert.DeserializeObject<Dictionary<string, object?>>(JsonConvert.SerializeObject(generator.GetVersionResponse(uri)));
        Console.WriteLine($"({JsonConvert.SerializeObject(expected)}, {JsonConvert.SerializeObject(actual)})");
        //assert
        expected.ForEach(k =>
        {
            var x = actual?.GetValueOrDefault(k, null);
            Assert.That(x == null || (x is string && VersionResponseModel.NotSet.Equals(x)), k);
        });
    }
}
