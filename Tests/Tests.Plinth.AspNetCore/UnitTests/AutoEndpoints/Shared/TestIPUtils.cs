using System.Net;
using System.Diagnostics;
using NUnit.Framework;
using Plinth.AspNetCore.AutoEndpoints.Shared;

namespace Tests.Plinth.AspNetCore.UnitTests.AutoEndpoints.Shared;

[TestFixture]
public class TestIPUtils
{
    [Test]
    [TestCase("127.0.0.1")]
    [TestCase("127.0.0.90")]
    [TestCase("10.0.0.90")]
    [TestCase("10.2.3.90")]
    [TestCase("172.16.0.1")]
    [TestCase("172.16.60.51")]
    [TestCase("172.18.0.9")]
    [TestCase("192.168.0.1")]
    [TestCase("192.168.60.51")]
    public void TestIsLocal(string ip)
    {
        var ipAddr = IPAddress.Parse(ip);
        var ipAddrV6 = IPAddress.Parse("::ffff:" + ip);

        Assert.That(IPUtils.IsInternal(ipAddr));
        Assert.That(IPUtils.IsInternal(ipAddrV6));
    }

    [TestCase("::1")]
    public void TestIsLocal2(string ip)
    {
        var ipAddr = IPAddress.Parse(ip);

        Assert.That(IPUtils.IsInternal(ipAddr));
    }

    [Test]
    [TestCase("126.0.0.1")]
    [TestCase("126.0.0.90")]
    [TestCase("11.0.0.90")]
    [TestCase("9.2.3.90")]
    [TestCase("171.16.0.1")]
    [TestCase("173.16.60.51")]
    [TestCase("192.167.0.1")]
    [TestCase("192.169.60.51")]
    public void TestIsNotLocal(string ip)
    {
        var ipAddr = IPAddress.Parse(ip);

        Assert.That(IPUtils.IsInternal(ipAddr), Is.False);
    }
}