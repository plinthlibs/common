using NUnit.Framework;
using Plinth.AspNetCore.Jwt;
using Plinth.AspNetCore.TokenAuth;
using Plinth.Security.Jwt;

namespace Tests.Plinth.AspNetCore.UnitTests.Jwt;

[TestFixture]
public class JwtManagerTests
{
    [Test]
    public void AddScheme_Bearer_Succeeds()
    {
        var mgr = new JwtManager();
        mgr.AddScheme(TokenAuthHandlerOptionsBase.DefaultScheme, new JwtGenerationOptions());

        Assert.That(mgr.Generator, Is.Not.Null);
        Assert.That(mgr.Validator, Is.Not.Null);
        Assert.That(mgr.GeneratorForScheme(TokenAuthHandlerOptionsBase.DefaultScheme), Is.Not.Null);
        Assert.That(mgr.ValidatorForScheme(TokenAuthHandlerOptionsBase.DefaultScheme), Is.Not.Null);
    }

    [Test]
    public void AddScheme_NotBearer_Succeeds()
    {
        var mgr = new JwtManager();
        mgr.AddScheme("NotBearer", new JwtGenerationOptions());

        Assert.That(mgr.Generator, Is.Not.Null);
        Assert.That(mgr.Validator, Is.Not.Null);
        Assert.That(mgr.GeneratorForScheme("NotBearer"), Is.Not.Null);
        Assert.That(mgr.ValidatorForScheme("NotBearer"), Is.Not.Null);
    }

    [Test]
    public void AddScheme_TwoSchemes_Succeeds()
    {
        var mgr = new JwtManager();
        mgr.AddScheme("NotBearer", new JwtGenerationOptions());
        mgr.AddScheme("AlsoNotBearer", new JwtGenerationOptions());

        Assert.That(mgr.GeneratorForScheme("NotBearer"), Is.Not.Null);
        Assert.That(mgr.ValidatorForScheme("NotBearer"), Is.Not.Null);
        Assert.That(mgr.GeneratorForScheme("AlsoNotBearer"), Is.Not.Null);
        Assert.That(mgr.ValidatorForScheme("AlsoNotBearer"), Is.Not.Null);
    }

    [Test]
    public void AddScheme_TwoSchemesWithDefault_Succeeds()
    {
        var mgr = new JwtManager();
        mgr.AddScheme(TokenAuthHandlerOptionsBase.DefaultScheme, new JwtGenerationOptions());
        mgr.AddScheme("NotBearer", new JwtGenerationOptions());

        Assert.That(mgr.Generator, Is.Not.Null);
        Assert.That(mgr.Validator, Is.Not.Null);
        Assert.That(mgr.GeneratorForScheme("NotBearer"), Is.Not.Null);
        Assert.That(mgr.ValidatorForScheme("NotBearer"), Is.Not.Null);

        Assert.That(mgr.GeneratorForScheme("NotBearer"), Is.Not.SameAs(mgr.Generator));
        Assert.That(mgr.ValidatorForScheme("NotBearer"), Is.Not.SameAs(mgr.Validator));

        Assert.That(mgr.GeneratorForScheme(TokenAuthHandlerOptionsBase.DefaultScheme), Is.SameAs(mgr.Generator));
        Assert.That(mgr.ValidatorForScheme(TokenAuthHandlerOptionsBase.DefaultScheme), Is.SameAs(mgr.Validator));
    }

    [Test]
    public void AddScheme_TwoSchemesPropertyAccess_Fails()
    {
        var mgr = new JwtManager();
        mgr.AddScheme("NotBearer", new JwtGenerationOptions());
        mgr.AddScheme("AlsoNotBearer", new JwtGenerationOptions());

        object? x;
        Assert.Throws<ArgumentException>(() => x = mgr.Generator);
        Assert.Throws<ArgumentException>(() => x = mgr.Validator);
    }

    [Test]
    public void AddScheme_WrongScheme_Fails()
    {
        var mgr = new JwtManager();
        mgr.AddScheme("NotBearer", new JwtGenerationOptions());

        Assert.Throws<ArgumentException>(() => mgr.GeneratorForScheme("SomethingElse"));
        Assert.Throws<ArgumentException>(() => mgr.ValidatorForScheme("SomethingElse"));
    }

    [Test]
    public void AddScheme_InvalidInputs_Fails()
    {
        var mgr = new JwtManager();

        Assert.Throws<ArgumentNullException>(() => mgr.AddScheme(null!, new JwtGenerationOptions()));
        Assert.Throws<ArgumentNullException>(() => mgr.AddScheme(null!, null!));
        Assert.Throws<ArgumentNullException>(() => mgr.AddScheme("SomethingElse", null!));
    }

}
