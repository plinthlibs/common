using Microsoft.AspNetCore.Authentication;
using Microsoft.IdentityModel.Logging;
using NUnit.Framework;
using Plinth.AspNetCore.Jwt;
using Plinth.Security.Jwt;
using System.Text.Encodings.Web;

namespace Tests.Plinth.AspNetCore.UnitTests.Jwt;

[TestFixture]
public class JwtAuthHandlerTests
{
    private readonly byte[] _key = Convert.FromHexString("33674aec26664b4838113f7c5185fa51218b816e80302e70749c8968b5d95087");

    [SetUp]
    public void SetUp()
    {
        IdentityModelEventSource.ShowPII = true;
    }

    [Test]
    public void Construct_NullManager_Fails()
    {
        var services = new ServiceCollection().AddLogging();
        var sp = services.BuildServiceProvider();

        Assert.Throws<ArgumentNullException>(() => new JwtAuthHandler(null!,
            null!, sp.GetRequiredService<ILoggerFactory>(),
            UrlEncoder.Default
#if NET8_0_OR_GREATER
            ));
#else
            , new SystemClock()));
#endif
    }

    [Test]
    public async Task ValidateToken_ValidToken_Succeeds()
    {
        var services = new ServiceCollection()
            .AddLogging()
            .AddPlinthJwtAuth(c =>
            {
                c.SecurityMode = new JwtSecurityModeHmacSignature(_key);
            });
        var sp = services.BuildServiceProvider();

        var mgr = sp.GetRequiredService<JwtManager>();

        var token = mgr.Generator.GetBuilder(Guid.NewGuid(), "name").Build();

        var handler = sp.GetRequiredService<JwtAuthHandler>();

        var httpCtx = new DefaultHttpContext();
        httpCtx.Request.Path = "/api/thing";
        httpCtx.Request.Headers.Authorization = $"Bearer {token.Jwt}";
        await handler.InitializeAsync(new AuthenticationScheme("Bearer", null, typeof(JwtAuthHandler)),
            httpCtx);

        var ret = await handler.AuthenticateAsync();
        Assert.That(ret.Succeeded);
    }

    [Test]
    public async Task ValidateToken_ExpiredToken_Fails()
    {
        var services = new ServiceCollection()
            .AddLogging()
            .AddPlinthJwtAuth(c =>
            {
                c.SecurityMode = new JwtSecurityModeHmacSignature(_key);
                c.TokenLifetime = TimeSpan.FromSeconds(1);
                c.ClockSkew = TimeSpan.Zero;
            });
        var sp = services.BuildServiceProvider();

        var mgr = sp.GetRequiredService<JwtManager>();

        var token = mgr.Generator.GetBuilder(Guid.NewGuid(), "name").Build();
        await Task.Delay(TimeSpan.FromSeconds(1));

        var handler = sp.GetRequiredService<JwtAuthHandler>();

        var httpCtx = new DefaultHttpContext();
        httpCtx.Request.Path = "/api/thing";
        httpCtx.Request.Headers.Authorization = $"Bearer {token.Jwt}";
        await handler.InitializeAsync(new AuthenticationScheme("Bearer", null, typeof(JwtAuthHandler)),
            httpCtx);

        var ret = await handler.AuthenticateAsync();
        Assert.That(ret.Succeeded, Is.False);
    }
}
