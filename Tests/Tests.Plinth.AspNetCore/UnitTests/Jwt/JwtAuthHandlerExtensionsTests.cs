using Microsoft.AspNetCore.Authentication;
using Microsoft.IdentityModel.Logging;
using NUnit.Framework;
using Plinth.AspNetCore.Jwt;
using Plinth.Security.Jwt;

namespace Tests.Plinth.AspNetCore.UnitTests.Jwt;

[TestFixture]
public class JwtAuthHandlerExtensionsTests
{
    private readonly byte[] _key = Convert.FromHexString("33674aec26664b4838113f7c5185fa51218b816e80302e70749c8968b5d95087");

    [OneTimeSetUp]
    public void OneTimeSetUp()
    {
        IdentityModelEventSource.ShowPII = true;
    }

    [SetUp]
    public void SetUp()
    {
    }

    [Test]
    public void AddPlinthJwtAuth_DefaultScheme_Succeeds()
    {
        var services = new ServiceCollection()
            .AddLogging()
            .AddPlinthJwtAuth(c =>
            {
                c.SecurityMode = new JwtSecurityModeHmacSignature(_key);
            });
        var sp = services.BuildServiceProvider();

        var mgr = sp.GetRequiredService<JwtManager>();

        var token = mgr.Generator.GetBuilder(Guid.NewGuid(), "name").Build();
        mgr.Validator.Validate(token.Jwt);

        token = mgr.GeneratorForScheme("Bearer").GetBuilder(Guid.NewGuid(), "name").Build();
        mgr.ValidatorForScheme("Bearer").Validate(token.Jwt);

        Assert.That(sp.GetRequiredService<JwtAuthHandler>(), Is.Not.Null);
    }

    [Test]
    public void AddPlinthJwtAuth_CustomScheme_Succeeds()
    {
        var services = new ServiceCollection()
            .AddLogging()
            .AddPlinthJwtAuth("MyScheme", c =>
            {
                c.SecurityMode = new JwtSecurityModeHmacSignature(_key);
            });
        var sp = services.BuildServiceProvider();

        var mgr = sp.GetRequiredService<JwtManager>();

        var token = mgr.Generator.GetBuilder(Guid.NewGuid(), "name").Build();
        mgr.Validator.Validate(token.Jwt);

        token = mgr.GeneratorForScheme("MyScheme").GetBuilder(Guid.NewGuid(), "name").Build();
        mgr.ValidatorForScheme("MyScheme").Validate(token.Jwt);

        Assert.That(sp.GetRequiredService<JwtAuthHandler>(), Is.Not.Null);
    }

    [Test]
    public void AddPlinthJwtAuth_TwoSchemes_Succeeds()
    {
        var services = new ServiceCollection()
            .AddLogging()
            .AddPlinthJwtAuth(c =>
            {
                c.SecurityMode = new JwtSecurityModeAesEncryption(_key);
                c.TokenLifetime = TimeSpan.FromSeconds(30);
            })
            .AddPlinthJwtAuth("MyScheme", c =>
            {
                c.SecurityMode = new JwtSecurityModeHmacSignature(_key);
                c.TokenLifetime = TimeSpan.FromSeconds(90);
            });
        var sp = services.BuildServiceProvider();

        var mgr = sp.GetRequiredService<JwtManager>();

        var token = mgr.Generator.GetBuilder(Guid.NewGuid(), "name").Build();
        mgr.Validator.Validate(token.Jwt);
        Assert.That(token.ValidTo - token.ValidFrom, Is.LessThan(TimeSpan.FromMinutes(1)));

        token = mgr.GeneratorForScheme("Bearer").GetBuilder(Guid.NewGuid(), "name").Build();
        mgr.ValidatorForScheme("Bearer").Validate(token.Jwt);
        Assert.That(token.ValidTo - token.ValidFrom, Is.LessThan(TimeSpan.FromMinutes(1)));

        token = mgr.GeneratorForScheme("MyScheme").GetBuilder(Guid.NewGuid(), "name").Build();
        mgr.ValidatorForScheme("MyScheme").Validate(token.Jwt);
        Assert.That(token.ValidTo - token.ValidFrom, Is.GreaterThan(TimeSpan.FromMinutes(1)));

        Assert.That(sp.GetRequiredService<JwtAuthHandler>(), Is.Not.Null);
    }

    [Test]
    public async Task AddPlinthJwtAuthForSignalR_BearerScheme_Fails()
    {
        var services = new ServiceCollection()
            .AddLogging()
            .AddPlinthJwtAuthForSignalR(c =>
            {
                c.SecurityMode = new JwtSecurityModeHmacSignature(_key);
            });
        var sp = services.BuildServiceProvider();

        var mgr = sp.GetRequiredService<JwtManager>();

        var token = mgr.Generator.GetBuilder(Guid.NewGuid(), "name").Build();
        mgr.Validator.Validate(token.Jwt);

        var handler = sp.GetRequiredService<JwtAuthHandler>();

        var httpCtx = new DefaultHttpContext();
        httpCtx.Request.Path = "/api/thing";
        httpCtx.Request.QueryString = QueryString.Create("access_token", token.Jwt);
        await handler.InitializeAsync(new AuthenticationScheme("Bearer", null, typeof(JwtAuthHandler)),
            httpCtx);

        var ret = await handler.AuthenticateAsync();
        Assert.That(ret.Succeeded, Is.False);
    }

    [Test]
    public void AddPlinthJwtAuthForSignalR_Bearer_Fails()
    {
        var services = new ServiceCollection()
            .AddLogging();

        Assert.Throws<ArgumentException>(() =>
            services.AddPlinthJwtAuthForSignalR(c =>
            {
                c.SecurityMode = new JwtSecurityModeHmacSignature(_key);
            }, authScheme: "Bearer")
        );
    }

    [Test]
    public async Task AddPlinthJwtAuthForSignalR_CustomSource_Succeeds()
    {
        var services = new ServiceCollection()
            .AddLogging()
            .AddPlinthJwtAuthForSignalR(c =>
            {
                c.SecurityMode = new JwtSecurityModeHmacSignature(_key);
            },
            authScheme: "SignalR",
            configureHandler: c =>
            {
                c.CustomTokenSource = r => r.Query["custom_token"];
            });
        var sp = services.BuildServiceProvider();

        var mgr = sp.GetRequiredService<JwtManager>();
        var token = mgr.Generator.GetBuilder(Guid.NewGuid(), "name").Build();

        var handler = sp.GetRequiredService<JwtAuthHandler>();

        var httpCtx = new DefaultHttpContext();
        httpCtx.Request.Path = "/api/thing";
        httpCtx.Request.QueryString = QueryString.Create("custom_token", token.Jwt);
        await handler.InitializeAsync(new AuthenticationScheme("SignalR", null, typeof(JwtAuthHandler)),
            httpCtx);

        var ret = await handler.AuthenticateAsync();
        Assert.That(ret.Succeeded);
    }

    [Test]
    public async Task AddPlinthJwtAuthForSignalR_DefaultSource_Succeeds()
    {
        var services = new ServiceCollection()
            .AddLogging()
            .AddPlinthJwtAuthForSignalR(c =>
            {
                c.SecurityMode = new JwtSecurityModeHmacSignature(_key);
            },
            authScheme: "SignalR");
        var sp = services.BuildServiceProvider();

        var mgr = sp.GetRequiredService<JwtManager>();
        var token = mgr.Generator.GetBuilder(Guid.NewGuid(), "name").Build();

        var handler = sp.GetRequiredService<JwtAuthHandler>();

        var httpCtx = new DefaultHttpContext();
        httpCtx.Request.Path = "/api/thing";
        httpCtx.Request.QueryString = QueryString.Create("access_token", token.Jwt);
        await handler.InitializeAsync(new AuthenticationScheme("SignalR", null, typeof(JwtAuthHandler)),
            httpCtx);

        var ret = await handler.AuthenticateAsync();
        Assert.That(ret.Succeeded);
    }
}
