using Microsoft.AspNetCore.Authentication;
using NUnit.Framework;
using Plinth.AspNetCore.Jwt;
using Plinth.Security.Jwt;
using Plinth.Security;
using System.Security.Claims;

namespace Tests.Plinth.AspNetCore.UnitTests.Jwt;

[TestFixture]
public class HttpContextExtensionsTests
{
    private readonly byte[] _key = Convert.FromHexString("33674aec26664b4838113f7c5185fa51218b816e80302e70749c8968b5d95087");

    [Test]
    public async Task GetJwt_Valid_ReturnsToken()
    {
        var services = new ServiceCollection()
            .AddLogging()
            .AddPlinthJwtAuth(c =>
            {
                c.SecurityMode = new JwtSecurityModeHmacSignature(_key);
            });
        var sp = services.BuildServiceProvider();

        var mgr = sp.GetRequiredService<JwtManager>();

        var token = mgr.Generator.GetBuilder(Guid.NewGuid(), "name").Build();

        var handler = sp.GetRequiredService<JwtAuthHandler>();

        var httpCtx = new DefaultHttpContext();
        httpCtx.Request.Path = "/api/thing";
        httpCtx.Request.Headers.Authorization = $"Bearer {token.Jwt}";
        await handler.InitializeAsync(new AuthenticationScheme("Bearer", null, typeof(JwtAuthHandler)),
            httpCtx);

        var ret = await handler.AuthenticateAsync();
        Assert.That(ret.Succeeded);
        Assert.That(ret.Ticket, Is.Not.Null);
        httpCtx.User = ret.Ticket!.Principal;

        Assert.That(httpCtx.GetJwt(), Is.EqualTo(token.Jwt));
        Assert.That(httpCtx.TryGetJwt(), Is.EqualTo(token.Jwt));
    }

    [Test]
    public void GetJwt_Invalid_Fails()
    {
        var httpCtx = new DefaultHttpContext
        {
            User = new ClaimsPrincipal()
        };

        Assert.Throws<SecurityException>(() => httpCtx.GetJwt());
        Assert.That(httpCtx.TryGetJwt(), Is.Null);
    }

    [Test]
    public void GetJwt_NoJwtProperty_Fails()
    {
        var httpCtx = new DefaultHttpContext
        {
            User = new ClaimsPrincipal(new ClaimsIdentity())
        };

        Assert.Throws<SecurityException>(() => httpCtx.GetJwt());
        Assert.That(httpCtx.TryGetJwt(), Is.Null);
    }
}
