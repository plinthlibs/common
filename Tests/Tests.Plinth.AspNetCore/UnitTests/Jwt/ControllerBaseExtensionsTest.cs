using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Plinth.AspNetCore.Jwt;
using Plinth.Security.Jwt;
using Plinth.Security;
using System.Security.Claims;

namespace Tests.Plinth.AspNetCore.UnitTests.Jwt;

[TestFixture]
public class ControllerBaseExtensionsTest
{
    private readonly byte[] _key = Convert.FromHexString("33674aec26664b4838113f7c5185fa51218b816e80302e70749c8968b5d95087");

    private class MyController : Controller
    {
    }

    [Test]
    public async Task Gets_Valid_ReturnsData()
    {
        var services = new ServiceCollection()
            .AddLogging()
            .AddPlinthJwtAuth(c =>
            {
                c.SecurityMode = new JwtSecurityModeHmacSignature(_key);
            });
        var sp = services.BuildServiceProvider();

        var mgr = sp.GetRequiredService<JwtManager>();

        var id = Guid.NewGuid();
        Guid sessionGuid = Guid.NewGuid();
        var token = mgr.Generator.GetBuilder(id, "name")
            .SessionGuid(sessionGuid)
            .Roles("admin", "user")
            .Build();

        var handler = sp.GetRequiredService<JwtAuthHandler>();

        var httpCtx = new DefaultHttpContext();
        httpCtx.Request.Path = "/api/thing";
        httpCtx.Request.Headers.Authorization = $"Bearer {token.Jwt}";
        await handler.InitializeAsync(new AuthenticationScheme("Bearer", null, typeof(JwtAuthHandler)),
            httpCtx);

        var ret = await handler.AuthenticateAsync();
        Assert.That(ret.Succeeded);
        Assert.That(ret.Ticket, Is.Not.Null);
        httpCtx.User = ret.Ticket!.Principal;

        var controllerContext = new ControllerContext()
        {
            HttpContext = httpCtx,
        };
        var controller = new MyController()
        {
            ControllerContext = controllerContext,
        };

        Assert.That(controller.GetJwt(), Is.EqualTo(token.Jwt));
        Assert.That(controller.TryGetJwt(), Is.EqualTo(token.Jwt));

        Assert.That(controller.GetAuthenticatedUserId(), Is.EqualTo(id));
        Assert.That(controller.TryGetAuthenticatedUserId(), Is.EqualTo(id));
        Assert.That(controller.GetAuthenticatedUserName(), Is.EqualTo("name"));
        Assert.That(controller.TryGetAuthenticatedUserName(), Is.EqualTo("name"));
        Assert.That(controller.GetAuthenticatedUserSessionGuid(), Is.EqualTo(sessionGuid));
        Assert.That(controller.TryGetAuthenticatedUserSessionGuid(), Is.EqualTo(sessionGuid));
        Assert.That(controller.GetAuthenticatedUserRoles(), Is.EquivalentTo(new[] { "admin", "user" }));
        Assert.That(controller.TryGetAuthenticatedUserRoles(), Is.EquivalentTo(new[] { "admin", "user" }));

        var s = controller.GetAuthTokenInfo();
        Assert.That(s.UserId, Is.EqualTo(id));
        Assert.That(s.UserName, Is.EqualTo("name"));
        Assert.That(s.SessionGuid, Is.EqualTo(sessionGuid));
        Assert.That(s.Roles, Is.EquivalentTo(new[] { "admin", "user" }));

        s = controller.TryGetAuthTokenInfo()!;
        Assert.That(s, Is.Not.Null);
        Assert.That(s.UserId, Is.EqualTo(id));
        Assert.That(s.UserName, Is.EqualTo("name"));
        Assert.That(s.SessionGuid, Is.EqualTo(sessionGuid));
        Assert.That(s.Roles, Is.EquivalentTo(new[] { "admin", "user" }));
    }

    [Test]
    public void Gets_MissingClaims_FailsOrNull()
    {
        var httpCtx = new DefaultHttpContext
        {
            User = new ClaimsPrincipal(new ClaimsIdentity("Bearer"))
        };

        var controllerContext = new ControllerContext()
        {
            HttpContext = httpCtx,
        };
        var controller = new MyController()
        {
            ControllerContext = controllerContext,
        };

        Assert.Throws<InvalidDataException>(() => controller.GetJwt());
        Assert.That(controller.TryGetJwt(), Is.Null);

        Assert.Throws<InvalidDataException>(() => controller.GetAuthenticatedUserId());
        Assert.That(controller.TryGetAuthenticatedUserId(), Is.Null);

        Assert.Throws<InvalidDataException>(() => controller.GetAuthenticatedUserName());
        Assert.That(controller.TryGetAuthenticatedUserName(), Is.Null);

        Assert.Throws<InvalidDataException>(() => controller.GetAuthenticatedUserSessionGuid());
        Assert.That(controller.TryGetAuthenticatedUserSessionGuid(), Is.Null);

        Assert.That(controller.GetAuthenticatedUserRoles(), Is.Empty);
        Assert.That(controller.TryGetAuthenticatedUserRoles(), Is.Empty);

        var s = controller.GetAuthTokenInfo();
        object? x;
        Assert.Throws<InvalidDataException>(() => x = s.UserId);
        Assert.Throws<InvalidDataException>(() => x = s.UserName);
        Assert.That(s.Roles, Is.Empty);

        s = controller.TryGetAuthTokenInfo()!;
        Assert.Throws<InvalidDataException>(() => x = s.UserId);
        Assert.Throws<InvalidDataException>(() => x = s.UserName);
        Assert.That(s.Roles, Is.Empty);
    }

    [Test]
    public void Gets_NotAuthenticated_FailsOrNull()
    {
        var httpCtx = new DefaultHttpContext
        {
            User = new ClaimsPrincipal([])
        };

        var controllerContext = new ControllerContext()
        {
            HttpContext = httpCtx,
        };
        var controller = new MyController()
        {
            ControllerContext = controllerContext,
        };

        Assert.Throws<SecurityException>(() => controller.GetJwt());
        Assert.That(controller.TryGetJwt(), Is.Null);

        Assert.Throws<SecurityException>(() => controller.GetAuthenticatedUserId());
        Assert.That(controller.TryGetAuthenticatedUserId(), Is.Null);

        Assert.Throws<SecurityException>(() => controller.GetAuthenticatedUserName());
        Assert.That(controller.TryGetAuthenticatedUserName(), Is.Null);

        Assert.Throws<SecurityException>(() => controller.GetAuthenticatedUserSessionGuid());
        Assert.That(controller.TryGetAuthenticatedUserSessionGuid(), Is.Null);

        Assert.Throws<SecurityException>(() => controller.GetAuthenticatedUserRoles());
        Assert.That(controller.TryGetAuthenticatedUserRoles(), Is.Empty);

        Assert.Throws<SecurityException>(() => controller.GetAuthTokenInfo());
        Assert.That(controller.TryGetAuthTokenInfo(), Is.Null);
    }
}
