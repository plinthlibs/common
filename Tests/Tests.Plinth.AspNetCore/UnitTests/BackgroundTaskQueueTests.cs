using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Plinth.AspNetCore;
using NUnit.Framework;

namespace Tests.Plinth.AspNetCore.UnitTests;

[TestFixture]
public class BackgroundTaskQueueTests
{
    [Test]
    public async Task TestBG()
    {
        IBackgroundTaskQueueImpl q = new BackgroundTaskQueue();
        Func<CancellationToken, Task> x = t => Task.CompletedTask;
        q.QueueBackgroundWorkItem(x);
        using var cts = new CancellationTokenSource();
        Assert.That((await q.DequeueAsync(cts.Token)).Func, Is.SameAs(x));
    }

    [Test]
    public void Queue_NullItem_Fails()
    {
        IBackgroundTaskQueueImpl q = new BackgroundTaskQueue();
        using var svc = new QueuedHostedService(q, 2);

        Assert.Throws<ArgumentNullException>(() => q.QueueBackgroundWorkItem(null!));
    }

    [Test]
    public async Task TestSvc()
    {
        IBackgroundTaskQueueImpl q = new BackgroundTaskQueue();
        using var svc = new QueuedHostedService(q, 2);

        using var cts = new CancellationTokenSource();
        await svc.StartAsync(cts.Token);
        int count = 0, started = 0;

        async Task f(CancellationToken t)
        {
            Interlocked.Increment(ref started);
            await Task.Delay(100, t);
            Interlocked.Increment(ref count);
        }

        for (int i = 0; i < 10; i++)
            q.QueueBackgroundWorkItem(f);

        // make sure all are started
        while (Volatile.Read(ref started) < 10)
            await Task.Delay(25);

        int c = Interlocked.CompareExchange(ref count, 0, 0);
        Assert.That(c, Is.LessThan(10));

        await Task.Delay(250);

        using var cts2 = new CancellationTokenSource();
        await svc.StopAsync(cts2.Token);

        Assert.That(Interlocked.CompareExchange(ref count, 0, 0), Is.EqualTo(10));
    }

    [Test]
    public async Task TestSomeStillQd()
    {
        IBackgroundTaskQueueImpl q = new BackgroundTaskQueue();
        using var svc = new QueuedHostedService(q, 2);

        using var cts = new CancellationTokenSource();
        await svc.StartAsync(cts.Token);
        int count = 0, started = 0;

        async Task f(CancellationToken t)
        {
            Interlocked.Increment(ref started);
            await Task.Delay(50000, t);
            Interlocked.Increment(ref count);
        }

        for (int i = 0; i < 10; i++)
            q.QueueBackgroundWorkItem(f);

        // make sure 2 of them are started
        while (Volatile.Read(ref started) < 2)
            await Task.Delay(25);

        using var cts2 = new CancellationTokenSource();
        await svc.StopAsync(cts2.Token);

        Assert.That(Volatile.Read(ref started), Is.EqualTo(2));
        Assert.That(Volatile.Read(ref count), Is.EqualTo(0));
    }

    [Test]
    public async Task TestWaitForSome()
    {
        IBackgroundTaskQueueImpl q = new BackgroundTaskQueue();
        using var svc = new QueuedHostedService(q, 2);

        using var cts = new CancellationTokenSource();
        await svc.StartAsync(cts.Token);
        int count = 0, started = 0;

        async Task f(CancellationToken t)
        {
            Interlocked.Increment(ref started);
            await Task.Delay(50000, t);
            Interlocked.Increment(ref count);
        }

        async Task f2(CancellationToken t)
        {
            Interlocked.Increment(ref started);
            await Task.Delay(50, t);
            Interlocked.Increment(ref count);
        }

        q.QueueBackgroundWorkItem(f2);
        q.QueueBackgroundWorkItem(f2);
        for (int i = 0; i < 8; i++)
            q.QueueBackgroundWorkItem(f);

        // make sure fast are started
        while (Volatile.Read(ref started) < 2)
            Thread.Sleep(25);

        // make sure fast ones are done
        while (Volatile.Read(ref count) < 2)
            await Task.Delay(25);

        using var cts2 = new CancellationTokenSource();
        await svc.StopAsync(cts2.Token).WaitAsync(TimeSpan.FromMilliseconds(200));

        Assert.That(Interlocked.CompareExchange(ref count, 0, 0), Is.EqualTo(2));
    }

    [Test]
    public async Task TestStopByCancellation()
    {
        IBackgroundTaskQueueImpl q = new BackgroundTaskQueue();
        using var svc = new QueuedHostedService(q, 2);

        using var cts = new CancellationTokenSource();
        await svc.StartAsync(cts.Token);
        int count = 0, started = 0;

        async Task f(CancellationToken t)
        {
            Interlocked.Increment(ref started);
            try
            {
                await Task.Delay(5, t);
            }
            finally
            {
                Interlocked.Increment(ref count);
            }
        }

        for (int i = 0; i < 10; i++)
            q.QueueBackgroundWorkItem(f);

        // make sure all are started
        while (Volatile.Read(ref started) < 10)
            await Task.Delay(25);

        cts.Cancel();

        await Task.Delay(250);

        Assert.That(Interlocked.CompareExchange(ref count, 0, 0), Is.EqualTo(10));
    }
}
