using System;
using Plinth.AspNetCore.Configuration;
using Plinth.AspNetCore.DI;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.OpenApi.Writers;

namespace Tests.Plinth.AspNetCore.UnitTests.DI;

[TestFixture]
public class ActivatorTests
{
    class NoParams
    {
        public NoParams() { }
    }

    class NoParamsThrows
    {
        public NoParamsThrows() { throw new ArgumentException(); }
    }

    class OneParams
    {
        public readonly string _p1;
        public OneParams(string p1) { _p1 = p1; }
    }

    enum TestEnum
    {
        DefaultValue = -1,
        Value = 2
    }

    class LotsOfDefaults
    {
        public readonly string _p1;
        public readonly string? _p2;
        public readonly string? _p3;
        public readonly TestEnum _p4;
        public readonly TestEnum? _p5;
        public readonly TestEnum? _p6;
        public readonly int? _p7;
        public readonly int? _p8;

        public LotsOfDefaults(string p1 = "a", string? p2 = null, string? p3 = "b", TestEnum p4 = TestEnum.DefaultValue, TestEnum? p5 = null, TestEnum? p6 = TestEnum.Value, int? p7 = 5, int? p8 = null)
        { _p1 = p1; _p2 = p2; _p3 = p3; _p4 = p4; _p5 = p5; _p6 = p6; _p7 = p7; _p8 = p8; }
    }

    class TwoParams
    {
        public readonly string _p1;
        public readonly List<int> _p2;
        public TwoParams(string p1, List<int> p2) { _p1 = p1; _p2 = p2; }
    }

    class Injected1 { }
    class Injected2 { }

    class OneInjected
    {
        public readonly Injected1 _p1;
        public OneInjected(Injected1 p1) { _p1 = p1; }
    }

    class TwoInjected
    {
        public readonly Injected1 _p1;
        public readonly Injected2 _p2;
        public TwoInjected(Injected1 p1, Injected2 p2) { _p1 = p1; _p2 = p2; }
    }

    class OneInjectedOneRuntime
    {
        public readonly Injected1 _p1;
        public readonly string _p2;
        public OneInjectedOneRuntime(Injected1 p1, string p2) { _p1 = p1; _p2 = p2; }
    }

    class OneInjectedOneRuntimeWithDefault
    {
        public readonly Injected1 _p1;
        public readonly string _p2;
        public OneInjectedOneRuntimeWithDefault(Injected1 p1, string p2 = "abc") { _p1 = p1; _p2 = p2; }
    }

    class TwoInjectedOneRuntime
    {
        public readonly Injected1 _p1;
        public readonly Injected2 _p2;
        public readonly string _p3;
        public TwoInjectedOneRuntime(Injected1 p1, Injected2 p2, string p3) { _p1 = p1; _p2 = p2; _p3 = p3; }
    }

    class TwoInjectedTwoRuntime
    {
        public readonly Injected1 _p1;
        public readonly Injected2 _p2;
        public readonly string _p3;
        public readonly int _p4;
        public TwoInjectedTwoRuntime(Injected1 p1, Injected2 p2, string p3, int p4) { _p1 = p1; _p2 = p2; _p3 = p3; _p4 = p4; }
    }

    class OneInjectedFourRuntime
    {
        public readonly Injected1 _p1;
        public readonly string _p2;
        public readonly int _p3;
        public readonly double _p4;
        public readonly string _p5;
        public OneInjectedFourRuntime(Injected1 p1, string p2, int p3, double p4, string p5) { _p1 = p1; _p2 = p2; _p3 = p3; _p4 = p4; _p5 = p5; }
    }
    
    class TwoConstructors
    {
        public readonly Injected1 _p1;
        public readonly Injected1 _p2;
        public readonly string? _p3;
        public readonly int _p4;
        public TwoConstructors(Injected1 p1, Injected1 p2) { _p1 = p1; _p2 = p2; }
        public TwoConstructors(Injected1 p1, Injected1 p2, string p3, int p4) { _p1 = p1; _p2 = p2; _p3 = p3; _p4 = p4; }
    }

    class TwoConstructors2
    {
        public readonly Injected1 _p1;
        public readonly Injected2? _p2;
        public readonly string _p3;
        public readonly int _p4;
        public TwoConstructors2(Injected1 p1, Injected2 p2, string p3, int p4) { _p1 = p1; _p2 = p2; _p3 = p3; _p4 = p4; }
        public TwoConstructors2(Injected1 p1, string p3, int p4) { _p1 = p1; _p3 = p3; _p4 = p4; }
    }

    class TwoConstructors3
    {
        public readonly Injected1 _p1;
        public readonly Injected2? _p2;
        public TwoConstructors3(Injected1 p1, Injected2 p2) { _p1 = p1; _p2 = p2; }
        public TwoConstructors3(Injected1 p1) { _p1 = p1; }

        static TwoConstructors3()
        {
        }

#pragma warning disable IDE0051 // Remove unused private members
        private TwoConstructors3(int _) { _p1 = new Injected1(); }
#pragma warning restore IDE0051 // Remove unused private members
    }

    [Test]
    public void Test_NoParams()
    {
        var sc = new ServiceCollection();
        var sp = sc.BuildServiceProvider();
        NoParams x = PlinthActivatorUtil.CreateInstance<NoParams>(sp);

        Assert.That(x, Is.Not.Null);

        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<NoParams>(sp, "hello"));
    }

    [Test]
    public void Test_OneParams()
    {
        var sc = new ServiceCollection();
        var sp = sc.BuildServiceProvider();
        OneParams x = PlinthActivatorUtil.CreateInstance<OneParams>(sp, "hello");

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.EqualTo("hello"));

        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<OneParams>(sp));
        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<OneParams>(sp, "hello", "there"));
    }

    [Test]
    public void Test_TwoParams()
    {
        var sc = new ServiceCollection();
        var sp = sc.BuildServiceProvider();
        var p2 = new List<int>() { 5, 7 };
        TwoParams x = PlinthActivatorUtil.CreateInstance<TwoParams>(sp, "hello", p2);

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.EqualTo("hello"));
        Assert.That(x._p2, Is.EqualTo(new List<int>() { 5, 7 }).AsCollection);

        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<TwoParams>(sp));
        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<TwoParams>(sp, "hello"));
        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<TwoParams>(sp, p2, "hello"));
        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<TwoParams>(sp, p2));
    }

    [Test]
    public void Test_OneInjected_Scoped()
    {
        var sc = new ServiceCollection();
        sc.AddScoped<Injected1>();
        var sp = sc.BuildServiceProvider();
        OneInjected? x = null;

        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<OneInjected>(sp, "hello"));

        using (var scope = sp.CreateScope())
        {
            x = PlinthActivatorUtil.CreateInstance<OneInjected>(scope.ServiceProvider);

            Assert.That(x, Is.Not.Null);
            Assert.That(x._p1, Is.Not.Null);
        }

        using (var scope = sp.CreateScope())
        {
            OneInjected x2 = PlinthActivatorUtil.CreateInstance<OneInjected>(scope.ServiceProvider);

            Assert.That(x2, Is.Not.Null);
            Assert.That(x2._p1, Is.Not.Null);
            Assert.That(x._p1, Is.Not.SameAs(x2._p1));
        }
    }

    [Test]
    public void Test_OneInjected()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        var sp = sc.BuildServiceProvider();
        OneInjected x = PlinthActivatorUtil.CreateInstance<OneInjected>(sp);

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);

        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<OneInjected>(sp, "hello"));
    }

    [Test]
    public void Test_TwoInjected()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        sc.AddSingleton(new Injected2());
        var sp = sc.BuildServiceProvider();
        TwoInjected x = PlinthActivatorUtil.CreateInstance<TwoInjected>(sp);

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.Not.Null);

        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<TwoInjected>(sp, "hello"));
        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<TwoInjected>(sp, "there", "hello"));
        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<TwoInjected>(sp, 6));
    }

    [Test]
    public void Test_OneInjectedOneRuntime()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        var sp = sc.BuildServiceProvider();
        OneInjectedOneRuntime x = PlinthActivatorUtil.CreateInstance<OneInjectedOneRuntime>(sp, "hello");

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.EqualTo("hello"));

        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<OneInjectedOneRuntime>(sp));
        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<OneInjectedOneRuntime>(sp, 8));
        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<OneInjectedOneRuntime>(sp, "hello", 8));
    }

    [Test]
    public void Test_TwoInjectedOneRuntime()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        sc.AddSingleton(new Injected2());
        var sp = sc.BuildServiceProvider();
        TwoInjectedOneRuntime x = PlinthActivatorUtil.CreateInstance<TwoInjectedOneRuntime>(sp, "hello");

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.Not.Null);
        Assert.That(x._p3, Is.EqualTo("hello"));

        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<TwoInjectedOneRuntime>(sp));
        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<TwoInjectedOneRuntime>(sp, "there", "hello"));
        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<TwoInjectedOneRuntime>(sp, 6));
    }

    [Test]
    public void Test_TwoInjectedTwoRuntime()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        sc.AddSingleton(new Injected2());
        var sp = sc.BuildServiceProvider();
        TwoInjectedTwoRuntime x = PlinthActivatorUtil.CreateInstance<TwoInjectedTwoRuntime>(sp, "hello", 8);

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.Not.Null);
        Assert.That(x._p3, Is.EqualTo("hello"));
        Assert.That(x._p4, Is.EqualTo(8));

        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<TwoInjectedTwoRuntime>(sp));
        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<TwoInjectedTwoRuntime>(sp, "there", "hello"));
        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<TwoInjectedTwoRuntime>(sp, 6));
        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<TwoInjectedTwoRuntime>(sp, 6, "hello"));
    }

    [Test]
    public void Test_TwoInjectedTwoRuntime_UsingOther()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        sc.AddSingleton(new Injected2());
        var sp = sc.BuildServiceProvider();
        TwoInjectedTwoRuntime x = PlinthActivatorUtil.CreateInstance<TwoInjectedTwoRuntime>(sp, ["hello"], 8);

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.Not.Null);
        Assert.That(x._p3, Is.EqualTo("hello"));
        Assert.That(x._p4, Is.EqualTo(8));

        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<TwoInjectedTwoRuntime>(sp));
        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<TwoInjectedTwoRuntime>(sp, ["there"], "hello"));
        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<TwoInjectedTwoRuntime>(sp, [6]));
        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<TwoInjectedTwoRuntime>(sp, [6], "hello"));
    }

    [Test]
    public void Test_OneInjectedFourRuntime_UsingOther()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        var sp = sc.BuildServiceProvider();
        OneInjectedFourRuntime x = PlinthActivatorUtil.CreateInstance<OneInjectedFourRuntime>(sp, ["hello", 8], 1.74d, "there");

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.EqualTo("hello"));
        Assert.That(x._p3, Is.EqualTo(8));
        Assert.That(x._p4, Is.EqualTo(1.74d));
        Assert.That(x._p5, Is.EqualTo("there"));
    }

    [Test]
    public void Test_TwoConstructors_First()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        sc.AddSingleton(new Injected2());
        var sp = sc.BuildServiceProvider();
        TwoConstructors x = PlinthActivatorUtil.CreateInstance<TwoConstructors>(sp);

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.Not.Null);
        Assert.That(x._p3, Is.Null);
        Assert.That(x._p4, Is.EqualTo(0));
    }

    [Test]
    public void Test_TwoConstructors_Second()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        sc.AddSingleton(new Injected2());
        var sp = sc.BuildServiceProvider();
        TwoConstructors x = PlinthActivatorUtil.CreateInstance<TwoConstructors>(sp, "hello", 8);

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.Not.Null);
        Assert.That(x._p3, Is.EqualTo("hello"));
        Assert.That(x._p4, Is.EqualTo(8));
    }

    [Test]
    public void Test_TwoConstructors2_First()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        sc.AddSingleton(new Injected2());
        var sp = sc.BuildServiceProvider();
        TwoConstructors2 x = PlinthActivatorUtil.CreateInstance<TwoConstructors2>(sp, "hello", 8);

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.Not.Null);
        Assert.That(x._p3, Is.EqualTo("hello"));
        Assert.That(x._p4, Is.EqualTo(8));
    }

    [Test]
    public void Test_TwoConstructors3_First()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        sc.AddSingleton(new Injected2());
        var sp = sc.BuildServiceProvider();
        TwoConstructors3 x = PlinthActivatorUtil.CreateInstance<TwoConstructors3>(sp);

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.Not.Null);
    }

    [Test]
    public void Test_NullRuntime_SingleNull_Literal()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        sc.AddSingleton(new Injected2());
        var sp = sc.BuildServiceProvider();
        TwoInjectedOneRuntime x = PlinthActivatorUtil.CreateInstance<TwoInjectedOneRuntime>(sp, null!);

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.Not.Null);
        Assert.That(x._p3, Is.Null);
    }

    [Test]
    public void Test_NullRuntime_SingleNull_Object()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        sc.AddSingleton(new Injected2());
        var sp = sc.BuildServiceProvider();
        object? o1 = null;
        TwoInjectedOneRuntime x = PlinthActivatorUtil.CreateInstance<TwoInjectedOneRuntime>(sp, o1);

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.Not.Null);
        Assert.That(x._p3, Is.Null);
    }

    [Test]
    public void Test_NullRuntime_TwoNulls_Literals()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        sc.AddSingleton(new Injected2());
        var sp = sc.BuildServiceProvider();
        TwoInjectedTwoRuntime x = PlinthActivatorUtil.CreateInstance<TwoInjectedTwoRuntime>(sp, null, null);

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.Not.Null);
        Assert.That(x._p3, Is.Null);
        Assert.That(x._p4, Is.EqualTo(0));
    }

    [Test]
    public void Test_NullRuntime_TwoNulls_Objects()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        sc.AddSingleton(new Injected2());
        var sp = sc.BuildServiceProvider();
        object? o1 = null;
        object? o2 = null;
        TwoInjectedTwoRuntime x = PlinthActivatorUtil.CreateInstance<TwoInjectedTwoRuntime>(sp, o1, o2);

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.Not.Null);
        Assert.That(x._p3, Is.Null);
        Assert.That(x._p4, Is.EqualTo(0));
    }

    [Test]
    public void Test_NullRuntime_TwoNulls_Mix()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        sc.AddSingleton(new Injected2());
        var sp = sc.BuildServiceProvider();
        object? o1 = null;
        object? o2 = null;
        TwoInjectedTwoRuntime x = PlinthActivatorUtil.CreateInstance<TwoInjectedTwoRuntime>(sp, [o1], null!);

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.Not.Null);
        Assert.That(x._p3, Is.Null);
        Assert.That(x._p4, Is.EqualTo(0));

        x = PlinthActivatorUtil.CreateInstance<TwoInjectedTwoRuntime>(sp, null, [o2]);

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.Not.Null);
        Assert.That(x._p3, Is.Null);
        Assert.That(x._p4, Is.EqualTo(0));
    }

    [Test]
    public void Test_ConstructorThrows()
    {
        var sc = new ServiceCollection();
        var sp = sc.BuildServiceProvider();

        Assert.Throws<ArgumentException>(() => PlinthActivatorUtil.CreateInstance<NoParamsThrows>(sp));
    }

    [Test]
    public void Test_OneInjectedOneRuntime_WithDefault()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        var sp = sc.BuildServiceProvider();
        var x = PlinthActivatorUtil.CreateInstance<OneInjectedOneRuntimeWithDefault>(sp);

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.EqualTo("abc"));

        x = PlinthActivatorUtil.CreateInstance<OneInjectedOneRuntimeWithDefault>(sp, "def");

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.EqualTo("def"));
    }

    [Test]
    public void Test_OneInjectedOneRuntime_MissingValue()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        var sp = sc.BuildServiceProvider();
        Assert.Throws<InvalidOperationException>(() => PlinthActivatorUtil.CreateInstance<OneInjectedOneRuntime>(sp));
    }

    [Test]
    public void Test_LotsOfDefaults()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        var sp = sc.BuildServiceProvider();
        var x = PlinthActivatorUtil.CreateInstance<LotsOfDefaults>(sp);

        Assert.That(x._p1, Is.EqualTo("a"));
        Assert.That(x._p2, Is.Null);
        Assert.That(x._p3, Is.EqualTo("b"));
        Assert.That(x._p4, Is.EqualTo(TestEnum.DefaultValue));
        Assert.That(x._p5, Is.Null);
        Assert.That(x._p6, Is.EqualTo(TestEnum.Value));
        Assert.That(x._p7, Is.EqualTo(5));
        Assert.That(x._p8, Is.Null);
    }
}
