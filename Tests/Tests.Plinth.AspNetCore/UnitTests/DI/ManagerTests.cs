using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Plinth.AspNetCore.DI;
using NUnit.Framework;
using Tests.Plinth.AspNetCore.Managers;

namespace Tests.Plinth.AspNetCore.UnitTests.DI;

[TestFixture]
public class ManagerTests
{
    class OneParams
    {
        public readonly string _p1;
        public OneParams(string p1) { _p1 = p1; }
    }

    class Injected1 { }
    class Injected2 { }

    class OneInjectedOneRuntime
    {
        public readonly Injected1 _p1;
        public readonly string _p2;
        public OneInjectedOneRuntime(Injected1 p1, string p2) { _p1 = p1; _p2 = p2; }
    }

    class TwoConstructors
    {
        public readonly Injected1 _p1;
        public readonly Injected2? _p2;
        public readonly string? _p3;
        public TwoConstructors(Injected1 p1) { _p1 = p1; }
        public TwoConstructors(Injected1 p1, Injected2 p2, string p3) { _p1 = p1; _p2 = p2; _p3 = p3; }
    }

    class TwoInjectedTwoRuntime
    {
        public readonly Injected1 _p1;
        public readonly Injected2 _p2;
        public readonly string _p3;
        public readonly string _p4;
        public TwoInjectedTwoRuntime(Injected1 p1, Injected2 p2, string p3, string p4) { _p1 = p1; _p2 = p2; _p3 = p3; _p4 = p4; }
    }

    [Test]
    public void Test_BasicFactory()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        sc.AddSingleton(new Injected2());
        sc.AddSingleton(_sp => _sp.NewManagerFactory<OneParams>());
        var sp = sc.BuildServiceProvider();

        var f = sp.GetRequiredService<Manager.Factory<OneParams>>();
        OneParams x = f("hello");

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.EqualTo("hello"));
    }

    [Test]
    public void Test_BasicFactory2()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        sc.AddSingleton(new Injected2());
        sc.AddSingleton(_sp => _sp.NewManagerFactory<OneInjectedOneRuntime>());
        var sp = sc.BuildServiceProvider();

        var f = sp.GetRequiredService<Manager.Factory<OneInjectedOneRuntime>>();
        OneInjectedOneRuntime x = f("hello");

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.EqualTo("hello"));
    }

    [Test]
    public void Test_BasicFactory3()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        sc.AddSingleton(new Injected2());
        sc.AddSingleton(_sp => _sp.NewManagerFactory<TwoConstructors>());
        var sp = sc.BuildServiceProvider();

        var f = sp.GetRequiredService<Manager.Factory<TwoConstructors>>();
        TwoConstructors x = f("hello");

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.Not.Null);
        Assert.That(x._p3, Is.EqualTo("hello"));
    }

    [Test]
    public void Test_BasicFactory4()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(new Injected1());
        sc.AddSingleton(new Injected2());
        sc.AddSingleton(_sp => _sp.NewManagerFactory<TwoInjectedTwoRuntime>("config"));
        var sp = sc.BuildServiceProvider();

        var f = sp.GetRequiredService<Manager.Factory<TwoInjectedTwoRuntime>>();
        TwoInjectedTwoRuntime x = f("hello");

        Assert.That(x, Is.Not.Null);
        Assert.That(x._p1, Is.Not.Null);
        Assert.That(x._p2, Is.Not.Null);
        Assert.That(x._p3, Is.EqualTo("config"));
        Assert.That(x._p4, Is.EqualTo("hello"));
    }
}
