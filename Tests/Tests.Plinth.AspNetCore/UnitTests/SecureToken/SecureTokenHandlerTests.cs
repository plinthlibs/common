using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using Plinth.AspNetCore.SecureToken;
using Plinth.Security.Crypto;
using System.Text.Encodings.Web;

namespace Tests.Plinth.AspNetCore.UnitTests.SecureToken;

[TestFixture]
public class SecureTokenHandlerTests
{
    private readonly string _key = "33674aec26664b4838113f7c5185fa51218b816e80302e70749c8968b5d95087";

    private IOptionsMonitor<SecureTokenOptions> GetHandlerOptions(ISecureData sd)
        => new TestOptionsMonitor<SecureTokenOptions>(new SecureTokenOptions() { SecureData = sd });

    private class TestOptionsMonitor<T>(T currentValue) : IOptionsMonitor<T>
        where T : class, new()
    {
        public T Get(string? name)
        {
            return CurrentValue;
        }

        public IDisposable OnChange(Action<T, string> listener)
        {
            throw new NotImplementedException();
        }

        public T CurrentValue => currentValue;
    }

    private class MySecureTokenHandler : SecureTokenHandler
    {
        public MySecureTokenHandler(IOptionsMonitor<SecureTokenOptions> options, ILoggerFactory logger, UrlEncoder encoder)
#if NET8_0_OR_GREATER
        : base(options, logger, encoder)
#else
        : base(options, logger, encoder, new SystemClock())
#endif
        {
        }

        public AuthenticateResult CallValidateToken(HttpRequest request, string tokenValue, bool requestLoggingEnabled)
        {
            return base.ValidateToken(request, tokenValue, requestLoggingEnabled);
        }
    }

    [Test]
    public async Task ValidateToken_ValidToken_Succeeds()
    {
        SecureData sd = new SecureData(_key);
        var services = new ServiceCollection()
            .AddLogging()
            .AddSecureTokenAuth(c =>
            {
                c.SecureData = sd;
            });
        var sp = services.BuildServiceProvider();

        var handler = new MySecureTokenHandler(
            GetHandlerOptions(sd), sp.GetRequiredService<ILoggerFactory>(),
            UrlEncoder.Default);

        var token = new SecureTokenGenerator(new SecureData(_key), TimeSpan.FromMinutes(1))
            .GetBuilder(Guid.NewGuid(), "name").Build();

        var httpCtx = new DefaultHttpContext();
        await handler.InitializeAsync(new AuthenticationScheme("Bearer", null, typeof(SecureTokenHandler)),
            httpCtx);

        var ret = handler.CallValidateToken(httpCtx.Request, token.Token, true);
        Assert.That(ret.Succeeded);
    }

    [Test]
    public async Task ValidateToken_ExpiredToken_Fails()
    {
        SecureData sd = new SecureData(_key);
        var services = new ServiceCollection()
            .AddLogging()
            .AddSecureTokenAuth(c =>
            {
                c.SecureData = sd;
            });
        var sp = services.BuildServiceProvider();

        var handler = new MySecureTokenHandler(
            GetHandlerOptions(sd), sp.GetRequiredService<ILoggerFactory>(),
            UrlEncoder.Default);

        var token = new SecureTokenGenerator(new SecureData(_key), TimeSpan.FromSeconds(1))
            .GetBuilder(Guid.NewGuid(), "name").Build();

        await Task.Delay(TimeSpan.FromSeconds(1));

        var httpCtx = new DefaultHttpContext();
        await handler.InitializeAsync(new AuthenticationScheme("Bearer", null, typeof(SecureTokenHandler)),
            httpCtx);

        var ret = handler.CallValidateToken(httpCtx.Request, token.Token, true);
        Assert.That(ret.Succeeded, Is.False);
    }

    [Test]
    public async Task ValidateToken_CorruptToken_Fails()
    {
        SecureData sd = new SecureData(_key);
        var services = new ServiceCollection()
            .AddLogging()
            .AddSecureTokenAuth(c =>
            {
                c.SecureData = sd;
            });
        var sp = services.BuildServiceProvider();

        var handler = new MySecureTokenHandler(
            GetHandlerOptions(sd), sp.GetRequiredService<ILoggerFactory>(),
            UrlEncoder.Default);

        var token = new SecureTokenGenerator(new SecureData(_key), TimeSpan.FromSeconds(1))
            .GetBuilder(Guid.NewGuid(), "name").Build();

        var httpCtx = new DefaultHttpContext();
        await handler.InitializeAsync(new AuthenticationScheme("Bearer", null, typeof(SecureTokenHandler)),
            httpCtx);

        var ret = handler.CallValidateToken(httpCtx.Request, "not a valid token", true);
        Assert.That(ret.Succeeded, Is.False);
    }

    [Test]
    public async Task HandleAuthenticateAsync_ValidTokenInHeaderForSignalR_Succeeds()
    {
        SecureData sd = new SecureData(_key);
        var services = new ServiceCollection()
            .AddLogging()
            .AddSecureTokenAuthForSignalR(c =>
            {
                c.SecureData = sd;
            });
        var sp = services.BuildServiceProvider();

        var optionsFactory = sp.GetRequiredService<IOptionsFactory<SecureTokenOptions>>();
        var options = new TestOptionsMonitor<SecureTokenOptions>(optionsFactory.Create("SignalR"));
        var handler = new MySecureTokenHandler(
            options, sp.GetRequiredService<ILoggerFactory>(),
            UrlEncoder.Default);

        var token = new SecureTokenGenerator(new SecureData(_key), TimeSpan.FromMinutes(1))
            .GetBuilder(Guid.NewGuid(), "name").Build();

        var httpCtx = new DefaultHttpContext();
        httpCtx.Request.Headers.Authorization = $"SignalR {token.Token}";
        await handler.InitializeAsync(new AuthenticationScheme("SignalR", null, typeof(SecureTokenHandler)),
            httpCtx);

        var ret = await handler.AuthenticateAsync();
        Assert.That(ret.Succeeded);
    }

    [Test]
    public async Task HandleAuthenticateAsync_ValidTokenInQueryForSignalR_Succeeds()
    {
        SecureData sd = new SecureData(_key);
        var services = new ServiceCollection()
            .AddLogging()
            .AddSecureTokenAuthForSignalR(c =>
            {
                c.SecureData = sd;
            });
        var sp = services.BuildServiceProvider();

        var optionsFactory = sp.GetRequiredService<IOptionsFactory<SecureTokenOptions>>();
        var options = new TestOptionsMonitor<SecureTokenOptions>(optionsFactory.Create("SignalR"));
        var handler = new MySecureTokenHandler(
            options, sp.GetRequiredService<ILoggerFactory>(),
            UrlEncoder.Default);

        var token = new SecureTokenGenerator(new SecureData(_key), TimeSpan.FromMinutes(1))
            .GetBuilder(Guid.NewGuid(), "name").Build();

        var httpCtx = new DefaultHttpContext();
        httpCtx.Request.QueryString = new QueryString($"?access_token={Uri.EscapeDataString(token.Token)}");
        await handler.InitializeAsync(new AuthenticationScheme("SignalR", null, typeof(SecureTokenHandler)),
            httpCtx);

        var ret = await handler.AuthenticateAsync();
        Assert.That(ret.Succeeded);
    }
}
