using System.Security.Claims;
using Plinth.AspNetCore.SecureToken;
using Plinth.Security.Tokens;
using NUnit.Framework;
using Plinth.Serialization;

namespace Tests.Plinth.AspNetCore.UnitTests.SecureToken;

[TestFixture]
public class SecureTokenTests
{
    private static readonly byte[] _key = Convert.FromHexString("12345678901234567890123456789012");

    [Test]
    public void Test_ClaimsPrincipal()
    {
        var guid = new Guid("e651d1e2-58af-4aba-bb3e-794c0d699146");
        var sessGuid = new Guid("aaa1d1e2-58af-4aba-bb3e-794c0d699146");
        var ori = DateTimeOffset.UtcNow.AddDays(-1);
        var now = DateTimeOffset.UtcNow;
        var data = new Dictionary<string, object?>()
        {
            { "Key1", "Value1" },
            { "Key2", "Value1" },
            { "Key3", 2 },
            { "Key4", null },
            { "Key5", new[] { "a", "b", "c" } },
            { "Key6", Guid.Parse("16891e0d-d6ed-4223-ba0b-517889599b58") },
            { "Key7", new DateTime(2023, 5, 26, 0, 0, 0, DateTimeKind.Utc) }
        };

        var token = new AuthenticationTokenBuilder(_key, guid, "name", TimeSpan.FromHours(1))
            .WithOriginalIssue(ori)
            .WithSessionGuid(sessGuid)
            .WithRoles("user", "admin")
            .WithData(data)
            .Build();

        assertPrincipal(token.ToClaimsPrincipal());

        token = AuthenticationToken.FromEncryptedToken(_key, token.Token);

        assertPrincipal(token.ToClaimsPrincipal());

        void assertPrincipal(ClaimsPrincipal pr)
        {
            Assert.That(pr.Claims.Count(), Is.EqualTo(13));
            Assert.That(pr.HasClaim(ClaimTypes.Role, "user"));
            Assert.That(pr.HasClaim(ClaimTypes.Role, "admin"));
            Assert.That(pr.FindFirst(ClaimTypes.AuthenticationInstant)!.Value, Is.EqualTo(ori.ToString("u")));
            Assert.That(pr.FindFirst(ClaimTypes.AuthenticationInstant)!.ValueType, Is.EqualTo(ClaimValueTypes.DateTime));

            Assert.That(pr.FindFirst(ClaimTypes.Expiration)!.Value,
                Is.EqualTo((now + TimeSpan.FromHours(1)).ToString("u"))
                .Or.EqualTo((now + TimeSpan.FromHours(1) + TimeSpan.FromSeconds(1)).ToString("u")));

            Assert.That(pr.FindFirst(ClaimTypes.Expiration)!.ValueType, Is.EqualTo(ClaimValueTypes.DateTime));
            Assert.That(pr.FindFirst(ClaimTypes.Name)!.Value, Is.EqualTo("name"));
            Assert.That(pr.FindFirst(ClaimTypes.NameIdentifier)!.Value, Is.EqualTo(guid.ToString()));
            Assert.That(Guid.Parse(pr.FindFirst(PlinthClaimTypes.SessionId)!.Value), Is.EqualTo(sessGuid));

            foreach (var kvp in data)
            {
                var claim = pr.FindFirst($"{PlinthClaimTypes.Data}/{kvp.Key}");

                if (kvp.Value is string s)
                    Assert.That(claim!.Value, Is.EqualTo(s));
                else if (kvp.Value is not null)
                    Assert.That(claim!.Value, Is.EqualTo(JsonUtil.SerializeObject(kvp.Value).Trim('"')));
                else
                    Assert.That(claim, Is.Null);
            }
        }
    }
}
