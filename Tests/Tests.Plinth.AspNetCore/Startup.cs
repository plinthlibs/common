using Microsoft.AspNetCore.Authorization;
using Tests.Plinth.AspNetCore.SecureTokens;
using Plinth.AspNetCore.SecureToken;
using Plinth.AspNetCore.AutoEndpoints.Diagnostics.Connectivity;
using Plinth.Security.Crypto;
using Plinth.Security.Jwt;
using Plinth.Logging.NLog.AppInsights;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Plinth.AspNetCore.Jwt;
using System.Security.Claims;

namespace Tests.Plinth.AspNetCore;

public class Startup
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        log.Info("ConfigureServices()");

        services.AddPlinthAppInsightsTelemetry(config =>
        {
            config.EnabledPrefixes = ["/api"];
            config.CaptureSqlCommandText = true;
        });

        services.AddControllers()
                .AddGlobalAuthorizedFilter()
                .AddPlinthControllerDefaults();

                // example of overriding newtonsoft settings
                //.AddNewtonsoftJson(options => options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Include);

        services.AddPlinthSwaggerGen(
            new PlinthSwaggerOptions
            {
                ApiTitle = "AspNetCore Test API",
                ApiDescription = "This is an API for Demonstrating an AspNetCore Web API",
                ApiVersion = "v1",
                EnableXmlDocs = true,
                AddBearerTokenAuth = true,
                ConfigureSwaggerInfo = i =>
                {
                    i.Contact = new Microsoft.OpenApi.Models.OpenApiContact { Email = "api@plinthlibs.com", Name = "Api Writer", Url = new Uri("https://www.plinthlibs.com") };
                    i.License = new Microsoft.OpenApi.Models.OpenApiLicense { Name = "test", Url = new Uri("https://license.plinthlibs.com") };
                    i.TermsOfService = new Uri("https://www.plinthlibs.com/tos.html");
                    //i.Extensions.Add("55", 
                },
                ConfigureSwaggerGen = null,
                /*CustomOperationId = (apiDesc, baseBehavior) =>
                {
                    return "Test_" + baseBehavior(apiDesc);
                }*/
                /*
                ShowServersDropdown = true,
                CustomServers = h => new OpenApiServer[] { new OpenApiServer() { Url = "http://google.com", Description = "Google" }, 
                    new OpenApiServer() { Url = "https://microsoft.com", Description = "Microsoft" } },
                */
#if FALSE
                DebugAuthToken = "abcd"
#endif
                //DisableDuplicateOperationCheck = true
            }
        );

        ContainerBootstrap.Register(services);

        services.AddCorsWithOrigins("*");

        services.AddSecureTokenAuth(c =>
        {
            //c.EncryptionKey = "19a7c2ff4a154d9a0e1e6d2f9e8ca7fd";
            c.SecureData = new SecureData("ff99c6f6bc940579fd211132683ca6eb90b87e2dbe600ea8c2aca383837835ec");
            c.WWWAuthenticateRealm = "testing api";
            c.UrlPathPrefixes = ["/api", "/notlogged"];
            //c.UrlPathFilter = r => r.Path.StartsWithSegments("/api") && !r.Path.StartsWithSegments("/api/v1");
        });
        services.AddSingleton(new SecureTokenGenerator("ff99c6f6bc940579fd211132683ca6eb90b87e2dbe600ea8c2aca383837835ec", TimeSpan.FromMinutes(15)));

        services.AddCustomSecureTokenAuth(c =>
        {
            c.EncryptionKey = "19a7c2ff4a154d9a0e1e6d2f9e8ca7fd";
            c.CustomTokenSource = r =>
            {
                if (r.HasFormContentType && r.Form.TryGetValue("token", out var sv))
                    return sv.FirstOrDefault();
                return null;
            };
            c.CustomTokenSourceFallsBack = true;
        }, "CustomAuth");

        services.AddPlinthJwtAuth("JwtAuth", c =>
        {
            c.Audience = "PlinthTest";
            c.Issuer = "Plinth.AspNetCore";
            c.ClockSkew = TimeSpan.FromSeconds(1);
            c.TokenContentLogging = true;
            c.TokenLifetime = TimeSpan.FromMinutes(10);
            c.MaxTokenLifetime = TimeSpan.FromHours(2);
            c.SecurityMode = new JwtSecurityModeHmacSignature(
                Convert.FromHexString("f7d6d1c2be0875372eba054a2e1f1d4f8df4cafbf705b6de6e83e6d932f78312"));
            //c.SecurityMode = new JwtSecurityModeAesEncryption(
            //    HexUtil.ToBytes("f7d6d1c2be0875372eba054a2e1f1d4f8df4cafbf705b6de6e83e6d932f78312f7d6d1c2be0875372eba054a2e1f1d4f8df4cafbf705b6de6e83e6d932f78312"));

            c.CustomClaims.Add(new Claim("global", "55", ClaimValueTypes.Integer32));
        });

#if NET8_0_OR_GREATER
        services.AddAuthorizationBuilder()
            .AddPolicy("UserNameIs", policy => policy.Requirements.Add(new UserNameIsRequirement("special")));
#else
        services.AddAuthorization(options =>
        {
            options.AddPolicy("UserNameIs", policy => policy.Requirements.Add(new UserNameIsRequirement("special")));
        });
#endif

        services.AddSingleton<IAuthorizationHandler, UserNameIsHandler>();

        /* for testing hsts
        services.AddHsts(options =>
        {
            options.IncludeSubDomains = false;
            options.Preload = false;
            options.MaxAge = TimeSpan.FromSeconds(60);
            options.ExcludedHosts.Clear();
        });
        */

        services.AddPlinthServices(e =>
        {
            e.EnableServiceLogging("/api"); //, "/notlogged");
            e.EnableServiceExceptionHandling();
            //e.EnableServiceExceptionHandling(_ => null, false);
            /*e.EnableServiceExceptionHandling(ex => new HandleExceptionResponse
            {
                Content = $"I caught this!: {ex.Message}",
                StatusCode = StatusCodes.Status402PaymentRequired
            });*/
            //e.EnableWebExceptionHandling();
            //e.EnableWebExceptionHandling(_ => null, false);
            /*e.EnableWebExceptionHandling(ex => new HandleExceptionResponse
            {
                Content = $"I caught this!: {ex.Message}",
                StatusCode = StatusCodes.Status402PaymentRequired
            });*/

            e.AutoEndpoints
                .SetComponentName("Plinth.AspNetCore.Test")
                .EnableVersion(c =>
                {
                    // just an example, will usually use ISqlTransactionProvider
                    c.SetDbVersionResolver<SecureTokenGenerator>(stg => stg.Generate(Guid.NewGuid(), "abc").Token);
                    //disable some fields or comment for defaults
                    //c.SetDisabledFields(VersionField.MachineName | VersionField.MachineUpTime | VersionField.ProcessName | VersionField.ProcessUpTime | VersionField.RequestHost | VersionField.RequestPort);
                })
                .EnableClient(c =>
                {
                    //c.LoggingLogWebRequests = true;
                })
                .EnableClient(c =>
                {
                    c.UrlPrefix = "mobileapp";
                    c.LoggingOpName = "MobileApp";
                    c.LoggingPrefix = "MobileApp";
                    //c.LoggingLogWebRequests = true;
                })
                .EnableDiagnostics(c =>
                {
                    c.SetConnectivityTesters(
                        new ConnectivityTesters.HttpHead("google", "https://www.google.com")
                        //new ConnectivityTesters.SqlDatabase("db", @"Data Source=(localdb)\sql2019;Initial Catalog=master;Integrated Security=SSPI;Persist Security Info=True;MultipleActiveResultSets=True")
                        //new ConnectivityTesters.SqlDatabase("db", @"Host=localhost;Port=5432;Database=CommonDBTest;Username=test;Password=test123;Persist Security Info=True")
                    );
                });
        });

    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.VerifyControllers();

        var serverAddressesFeature = app.ServerFeatures.Get<IServerAddressesFeature>();
        log.Info($"Configure env={env.EnvironmentName}, urls={string.Join(", ", serverAddressesFeature!.Addresses)}");

        app.UseRouting();

        app.UseCors();

        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }

        // app.UseHsts();

        app.UsePlinthServices();

        app.UsePlinthSwaggerUI(
            enableSwaggerAtRoot: true,
            enableReDoc: true,
            customizeSwaggerUI: null,
            customizeReDocUI: null);

        app.UseStaticFiles();
        app.UseAuthentication();

        app.UseAuthorization();
        app.UseEndpoints(routes =>
        {
            routes.MapControllers();
        });
    }
}
