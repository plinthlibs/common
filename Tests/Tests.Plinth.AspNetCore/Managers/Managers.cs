﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Plinth.AspNetCore.DI;

namespace Tests.Plinth.AspNetCore.Managers;

public static class Manager
{
    /// <summary>
    /// Create a standard manager factory that takes a calling user as a runtime parameter
    /// </summary>
    /// <typeparam name="T">Manager class type</typeparam>
    /// <param name="sp">service provider</param>
    /// <returns>A factory for creating 'T'</returns>
    /// <example>
    /// <code>services.RegisterSingleton(sp => sp.NewManagerFactory&lt;MyManager&gt;());</code>
    /// </example>
    public static Factory<T> NewManagerFactory<T>(this IServiceProvider sp)
        => callingUser => PlinthActivatorUtil.CreateInstance<T>(sp, callingUser);

    /// <summary>
    /// Factory for a given manager type that takes a calling user as a functime parameter
    /// </summary>
    /// <typeparam name="T">Manager class type</typeparam>
    /// <param name="callingUser"></param>
    /// <returns></returns>
    public delegate T Factory<T>(string callingUser);

    /// <summary>
    /// Create a standard manager factory that takes a calling user as a runtime parameter, as well as some startup parameters
    /// </summary>
    /// <typeparam name="T">Manager class type</typeparam>
    /// <param name="sp">service provider</param>
    /// <param name="others">other parameters known at startup time</param>
    /// <returns>A factory for creating 'T'</returns>
    /// <remarks>the non-DI parameters MUST be in the same order as the constructor</remarks>
    /// <example>
    /// <code>services.RegisterSingleton(sp => sp.NewManagerFactory&lt;MyManager&gt;("some", other, "params"));</code>
    /// </example>
    public static Factory<T> NewManagerFactory<T>(this IServiceProvider sp, params object[] others)
        => u => PlinthActivatorUtil.CreateInstance<T>(sp, others, u);
}
