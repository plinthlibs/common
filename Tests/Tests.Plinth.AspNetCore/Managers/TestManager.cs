﻿using System;
using System.Collections.Generic;
using System.Text;
using Tests.Plinth.AspNetCore.Controllers;

namespace Tests.Plinth.AspNetCore.Core2.Managers;

public partial class TestManager
{
    private readonly string? _cfg;
    private readonly ResolvedThing? _rt;

    public TestManager(ResolvedThing rt, string configSetting, string callingUser)
    {
        CallingUser = callingUser;
        _cfg = configSetting;
        _rt = rt;
    }

    public string? GetConfig() => _cfg;

    public string GetCallingUser() => CallingUser;

    public int GetResolved() => _rt!.X;
}
