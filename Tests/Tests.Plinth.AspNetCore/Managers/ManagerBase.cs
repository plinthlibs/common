﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.Plinth.AspNetCore.Core2.Managers;

public partial class TestManager
{
    protected string CallingUser { get; }

    public TestManager(string callingUser)
    {
        CallingUser = callingUser;
    }
}
