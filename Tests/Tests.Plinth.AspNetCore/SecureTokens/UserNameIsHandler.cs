﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;

namespace Tests.Plinth.AspNetCore.SecureTokens;

public class UserNameIsHandler : AuthorizationHandler<UserNameIsRequirement>
{
    protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, UserNameIsRequirement requirement)
    {
        // if the claim isn't there or isn't satisfied, best practice is to simply return
        // only use context.Fail() when failing this requirement 100% guarantees failure
        // by just returning, you allow a policy to support multiple OR'd requirements where just one is needed to pass

        var claim = context.User.FindFirst(ClaimTypes.Name);
        if (claim?.Value == requirement.UserName)
            context.Succeed(requirement);

        /*
         * Access the request
        var authFilterCtx = (Microsoft.AspNetCore.Mvc.Filters.AuthorizationFilterContext)context.Resource;
        var httpContext = authFilterCtx.HttpContext;

        foreach (var p in authFilterCtx.ActionDescriptor.Parameters)
        {
            if (p.Name == "something")
            {
                var value = authFilterCtx.RouteData.Values[p.Name] as string;
                if (value == "1234")
                {
                    log.Debug($"access granted");
                    context.Succeed(requirement);
                }
                else
                {
                    log.Warn($"access DENIED");
                    context.Fail();
                }
            }
        }
        */

        return Task.CompletedTask;
    }
}