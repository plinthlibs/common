﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Tests.Plinth.AspNetCore.SecureTokens;

public class UserNameIsRequirement : IAuthorizationRequirement
{
    public string UserName { get; }

    public UserNameIsRequirement(string n)
    {
        UserName = n;
    }
}
