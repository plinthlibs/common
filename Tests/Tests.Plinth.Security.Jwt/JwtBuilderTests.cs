using Microsoft.IdentityModel.Logging;
using NUnit.Framework;
using Plinth.Security.Jwt;
using System.Security.Claims;

namespace Tests.Plinth.Security.Jwt;

[TestFixture]
public class JwtBuilderTests
{
    private readonly byte[] _key = Convert.FromHexString("33674aec26664b4838113f7c5185fa51218b816e80302e70749c8968b5d95087");

    [SetUp]
    public void SetUp()
    {
        IdentityModelEventSource.ShowPII = true;
    }

    private JwtGenerationOptions GetOptions(string secMode) => new JwtGenerationOptions
        {
            Issuer = "https://plinth.com/unittests",
            Audience = "https://plinth.com",
            TokenContentLogging = true,
            TokenLifetime = TimeSpan.FromMinutes(10),
            SecurityMode = secMode switch
            {
                "sign" => new JwtSecurityModeHmacSignature(_key),
                "encrypt" => new JwtSecurityModeAesEncryption(_key),
                "signRsa" => new JwtSecurityModeRsaSignature(_publicKey2048, _privateKey2048),
                "encryptRsa" => new JwtSecurityModeRsaEncryption(_privateKey2048),
                _ => throw new NotImplementedException()
            }
        };

    [Test]
    public void Construct_NullOptions_Fails()
    {
        Assert.Throws<ArgumentNullException>(() => new JwtBuilder(null!));
    }

    [Test]
    public void Build_EmptySubjectId_Fails()
    {
        Assert.Throws<ArgumentNullException>(() => new JwtBuilder(GetOptions("sign"))
            .SubjectId(Guid.Empty));
    }

    [Test]
    public void Build_NullSubjectName_Fails()
    {
        Assert.Throws<ArgumentNullException>(() => new JwtBuilder(GetOptions("sign"))
            .SubjectName(null!));
    }

    [Test]
    [TestCase("00000000-0000-0000-0000-000000000000", "name")]
    [TestCase("2cbbe090-93ae-492d-9079-11a04259505d", null)]
    [TestCase("00000000-0000-0000-0000-000000000000", null)]
    public void Build_EmptyNullSubject_Fails(string id, string? name)
    {
        Assert.Throws<ArgumentNullException>(() => new JwtBuilder(GetOptions("sign"))
            .Subject(Guid.Parse(id), name!));
    }

    [Test]
    public void Build_OriginalIssueInFuture_Fails()
    {
        Assert.Throws<ArgumentOutOfRangeException>(() => new JwtBuilder(GetOptions("sign"))
            .OriginalIssue(DateTimeOffset.UtcNow.AddHours(1)));
    }

    [Test]
    public void Build_OriginalIssueNull_Ok()
    {
        Assert.DoesNotThrow(() => new JwtBuilder(GetOptions("sign")).OriginalIssue(null));
    }

    [Test]
    public void Build_OriginalIssueInPast_Ok()
    {
        Assert.DoesNotThrow(() => new JwtBuilder(GetOptions("sign")).OriginalIssue(DateTimeOffset.UtcNow.AddSeconds(-2)));
    }

    [Test]
    public void Build_IEnumerableRoles_RolesOk()
    {
        var opts = GetOptions("sign");

        var token = new JwtBuilder(opts)
            .Subject(Guid.NewGuid(), "name")
            .Roles(genRoles())
            .Build();

        static IEnumerable<string> genRoles()
        {
            yield return "role1";
            yield return "role2";
        }

        var cp = new JwtValidator(opts).Validate(token.Jwt);

        Assert.That(cp.IsInRole("role1"));
        Assert.That(cp.IsInRole("role2"));
        Assert.That(cp.IsInRole("role3"), Is.False);
    }

    [Test]
    public void Build_ListRoles_RolesOk()
    {
        var opts = GetOptions("sign");

        var token = new JwtBuilder(opts)
            .Subject(Guid.NewGuid(), "name")
            .Roles(new List<string> { "role1", "role2" })
            .Build();

        var cp = new JwtValidator(opts).Validate(token.Jwt);

        Assert.That(cp.IsInRole("role1"));
        Assert.That(cp.IsInRole("role2"));
        Assert.That(cp.IsInRole("role3"), Is.False);
    }

    [Test]
    public void Build_EmtpyIEnumerableRoles_NoRoles()
    {
        var opts = GetOptions("sign");

        var token = new JwtBuilder(opts)
            .Subject(Guid.NewGuid(), "name")
            .Roles(Enumerable.Empty<string>())
            .Build();

        var cp = new JwtValidator(opts).Validate(token.Jwt);

        Assert.That(cp.FindAll(ClaimTypes.Role).Count(), Is.EqualTo(0));
    }

    [Test]
    public void Build_NullIEnumerableRoles_NoRoles()
    {
        var opts = GetOptions("sign");

        var token = new JwtBuilder(opts)
            .Subject(Guid.NewGuid(), "name")
            .Roles((IEnumerable<string>?)null)
            .Build();

        var cp = new JwtValidator(opts).Validate(token.Jwt);

        Assert.That(cp.FindAll(ClaimTypes.Role).Count(), Is.EqualTo(0));
    }

    [Test]
    public void Build_NullListRoles_NoRoles()
    {
        var opts = GetOptions("sign");

        var token = new JwtBuilder(opts)
            .Subject(Guid.NewGuid(), "name")
            .Roles((List<string>?)null)
            .Build();

        var cp = new JwtValidator(opts).Validate(token.Jwt);

        Assert.That(cp.FindAll(ClaimTypes.Role).Count(), Is.EqualTo(0));
    }

    [Test]
    [TestCase("type", null)]
    [TestCase(null, "value")]
    [TestCase(null, null)]
    public void AddClaimStrings_InvalidValues_Fails(string? type, string? value)
    {
        Assert.Throws<ArgumentNullException>(() => new JwtBuilder(GetOptions("sign"))
            .AddClaim(type!, value!));
    }

    [Test]
    [TestCase("type", null)]
    [TestCase(null, "value")]
    [TestCase(null, null)]
    public void AddClaim_InvalidValues_Fails(string? type, string? value)
    {
        Assert.Throws<ArgumentNullException>(() => new JwtBuilder(GetOptions("sign"))
            .AddClaim(new Claim(type!, value!)));
    }

    [Test]
    public void AddClaim_Role_Fails()
    {
        Assert.Throws<InvalidOperationException>(() => new JwtBuilder(GetOptions("sign"))
            .AddClaim("role", "role1"));
    }

    [Test]
    public void AddClaim_RoleEnum_Fails()
    {
        Assert.Throws<InvalidOperationException>(() => new JwtBuilder(GetOptions("sign"))
            .AddClaim(ClaimTypes.Role, "role1"));
    }

    [Test]
    public void AddClaim_Reserved_Fails()
    {
        Assert.Throws<InvalidOperationException>(() => new JwtBuilder(GetOptions("sign"))
            .AddClaim("sub", "id"));
    }

    [Test]
    public void Build_SubjectNotSet_Fails()
    {
        var opts = GetOptions("sign");

        Assert.Throws<InvalidOperationException>(() => new JwtBuilder(opts).Build());
    }

    [Test]
    public void Build_SubjectIdNotSet_Fails()
    {
        var opts = GetOptions("sign");

        Assert.Throws<InvalidOperationException>(() => new JwtBuilder(opts).SubjectName("name").Build());
    }

    [Test]
    public void Build_SubjectNameNotSet_Fails()
    {
        var opts = GetOptions("sign");

        Assert.Throws<InvalidOperationException>(() => new JwtBuilder(opts).SubjectId(Guid.NewGuid()).Build());
    }

    [Test]
    [TestCase("sign")]
    [TestCase("encrypt")]
    [TestCase("signRsa")]
    [TestCase("encryptRsa")]
    public void Generate_Build_RoundTrips(string secMode)
    {
        var guid = new Guid("e651d1e2-58af-4aba-bb3e-794c0d699146");
        var sessGuid = new Guid("aaa1d1e2-58af-4aba-bb3e-794c0d699146");

        while (DateTimeOffset.UtcNow.Millisecond switch { < 200 or > 800 => true, _ => false })
            Thread.Sleep(TimeSpan.FromMilliseconds(50));

        var ori = DateTimeOffset.UtcNow.AddDays(-1);
        var now = DateTimeOffset.UtcNow;

        var opts = GetOptions(secMode);
        opts.TokenLifetime = TimeSpan.FromHours(1);
        opts.CustomClaims.Add(new Claim("global", "55", ClaimValueTypes.Integer32));

        var token = new JwtBuilder(opts)
            .Subject(guid, "name")
            .OriginalIssue(ori)
            .SessionGuid(sessGuid)
            .Roles("user", "admin")
            .Build();

        Assert.That(new DateTimeOffset(token.ValidFrom).ToUnixTimeSeconds(), Is.EqualTo(now.ToUnixTimeSeconds()));
        Assert.That(new DateTimeOffset(token.ValidTo).ToUnixTimeSeconds(), Is.EqualTo((now + TimeSpan.FromHours(1)).ToUnixTimeSeconds()));

        //Console.WriteLine(token.Jwt);

        opts = GetOptions(secMode);
        var cp = new JwtValidator(opts).Validate(token.Jwt);
        assertPrincipal(cp);

        void assertPrincipal(ClaimsPrincipal pr)
        {
            Assert.That(pr.Claims.Count(), Is.EqualTo(12));
            Assert.That(pr.IsInRole("user"));
            Assert.That(pr.IsInRole("admin"));
            Assert.That(pr.JwtOriginalIssue()?.ToUnixTimeSeconds(), Is.EqualTo(ori.ToUnixTimeSeconds()));

            Assert.That(pr.JwtExpiration().ToUnixTimeSeconds(), 
                Is.EqualTo((now + TimeSpan.FromHours(1)).ToUnixTimeSeconds())
                .Or.EqualTo((now + TimeSpan.FromHours(1) + TimeSpan.FromSeconds(1)).ToUnixTimeSeconds())
                .Or.EqualTo((now + TimeSpan.FromHours(1) + TimeSpan.FromSeconds(2)).ToUnixTimeSeconds()));

            Assert.That(pr.JwtUserName(), Is.EqualTo("name"));
            Assert.That(pr.JwtSessionGuid(), Is.EqualTo(sessGuid));

            Assert.That(pr.FindFirst("global")?.Value, Is.EqualTo("55"));
        }
    }

    // openssl genrsa -out private.pem 2048
    private readonly string _privateKey2048 = @"-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAulGQ01HdwQ0pB5bP/gL51Mv3SjC/ph/PuL2HWyB5faxFcC4V
LApGAeRl7DqrhWIe+GwczW6cuiOeAKgxEDtyTMdk9S6S0nk2PwUGBNc499PF8dmn
WSXfs0fY0/yQbp0D6JdVCCS/RhQCni5ssDMCo3BdxLdNqGzb5tNVv+YYfkhh8c+p
YrvGeUWgQxftDih5inlBfunzEaZmr3yJ6deeKLpFGl6kz7sSFSaCtzXpBwdLJ2AP
UQHcki7iyfHs+H9/Eiiq+DRr/QRHbNbytW5zbikjQsO4ZORPhEJ/XzA9tcbxegxU
0KtcHK/ZwaqlErxEDTifiTm7cun2kOtStwTaJQIDAQABAoIBAQC5aiBo28iFrJYP
MlsMGmQLC2utjjO6m1+5JguEeR3OHdkAWSvCpvqiOWvgmUZFt67AY76KBbC8YGOF
ndstnysRE0D56f48rFl6f4JM4O2VW09abdJnGhSzMiWqWGZXXbQdDDSXpRIgKBuu
1N5evnS6t0DpWc1TdfmkjN2B2fa3+5c7r2yZ9gvhyu4MNMjgFoiaZCwW25cG6ioW
WGFlS6Hq55fELW3mDKceWI1idPK0k8oBfal+ma43C6g9FJT6TxB2v3glB0FXt7xZ
2NKEN8PrEi89eGBGhbDHq82fuSAOl72CFoYw/cM6Kr9RcXPzLTmgaIO8etNIsoFu
TgN5y2jlAoGBAOQp6srI9yjCiwEFaTc/qJc5hrA9kKPqCSKo+vt/z0R3xTGJb311
0uZ/9ITF6a4HI2AMMq8o88mT5Oqwd5+7vJmxkUb2kfef/zhGkL6EsQxj/G3fxN5f
HodCQSXNMwxCUtXOfbvI5akJiCtjLPj98FuXdMZqZe5d0mXTY92W46SbAoGBANEM
unLI4E0nIf9ooQ7R2Z+/l81hxceWPQ7kM5wnXsZ/JuSU+/kmN5zJX+8Z6TAx/HTo
iP+gQr9nDdNXOPA9twjqyZVEImZsyYFWFjxt5ts8OJPFt3rmFZPDpEsLeeERMqSB
KxOD7XwfLZg021iu5Y1pY2GFvKutNCH8kxS894g/AoGAR8GTEPGBIbWFR+3uuCxy
FhVGcbj6+0IIW3kvbdc1VUAhqUGi6Z8bk9hcC0G/Czc9feB4NDQ574RuswyKopbF
asS4fsve/jS4bHds4C4VDGOJqPh+Dr11eW5x1LSjQFC/gnRtgJhe+fmAdLy9ERWz
+Oo6FIizw/KHz/RabP8eWs8CgYEAikZGCWivXLkWts8pQRkzPoRHT7VtUgAhMPlm
3dH40YkHW8jkBj/uJwnYJ9pWsOdPWrD49US3Ac2Tiog2BEp+CCrs3YusyV0JyD/+
A0iam8bwkWYFYAhSm2OcB3t2yZVLjx5VNNpdum0RBo1nzPgapHfiIX6JdnDhe92K
F/yKKpcCgYAJju2rfx6U2DO9LmFqS9RqkOLG3ZaL2Qgrl3xgwEubwm29jytojczn
XXCAKdW8lBr1RdXS0l/MLnUvdZhE90TW7F+3nOdnPpjpmCJqcJVE82eUj53t/r0b
TUf2U5pT6Xwau/ooHkE56u7bSVgMg3t3ymfM7awjcCAbBhkKRDLpvw==
-----END RSA PRIVATE KEY-----";

    // openssl rsa -in private.pem -RSAPublicKey_out
    private readonly string _publicKey2048 = @"-----BEGIN RSA PUBLIC KEY-----
MIIBCgKCAQEAulGQ01HdwQ0pB5bP/gL51Mv3SjC/ph/PuL2HWyB5faxFcC4VLApG
AeRl7DqrhWIe+GwczW6cuiOeAKgxEDtyTMdk9S6S0nk2PwUGBNc499PF8dmnWSXf
s0fY0/yQbp0D6JdVCCS/RhQCni5ssDMCo3BdxLdNqGzb5tNVv+YYfkhh8c+pYrvG
eUWgQxftDih5inlBfunzEaZmr3yJ6deeKLpFGl6kz7sSFSaCtzXpBwdLJ2APUQHc
ki7iyfHs+H9/Eiiq+DRr/QRHbNbytW5zbikjQsO4ZORPhEJ/XzA9tcbxegxU0Ktc
HK/ZwaqlErxEDTifiTm7cun2kOtStwTaJQIDAQAB
-----END RSA PUBLIC KEY-----";
}
