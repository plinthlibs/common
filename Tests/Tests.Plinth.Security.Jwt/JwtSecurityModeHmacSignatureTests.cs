using Microsoft.IdentityModel.Tokens;
using NUnit.Framework;
using Plinth.Security.Jwt;

namespace Tests.Plinth.Security.Jwt;

[TestFixture]
public class JwtSecurityModeHmacSignatureTests
{
    private readonly byte[] _key = Convert.FromHexString("33674aec26664b4838113f7c5185fa51218b816e80302e70749c8968b5d95087");

    [Test]
    [TestCase(0)]
    [TestCase(1)]
    [TestCase(31)]
    [TestCase(33)]
    [TestCase(47)]
    [TestCase(49)]
    [TestCase(63)]
    [TestCase(65)]
    public void Constructor_InvalidKeyLength_Fails(int keyLength)
    {
        Assert.Throws<ArgumentException>(() => new JwtSecurityModeHmacSignature(new byte[keyLength]));
    }

    [Test]
    public void Constructor_NullKey_Fails()
    {
        Assert.Throws<ArgumentNullException>(() => new JwtSecurityModeHmacSignature(null!));
    }

    [Test]
    public void ApplySecurity_SetsSigningCredentials()
    {
        var s = new JwtSecurityModeHmacSignature(_key);
        var thumbprint = new SymmetricSecurityKey(_key).ComputeJwkThumbprint();

        var tokenDesc = new SecurityTokenDescriptor();
        s.ApplySecurity(tokenDesc);

        Assert.That(tokenDesc.EncryptingCredentials, Is.Null);
        Assert.That(tokenDesc.SigningCredentials, Is.Not.Null);
        Assert.That(tokenDesc.SigningCredentials.Key.ComputeJwkThumbprint(), Is.EqualTo(thumbprint).AsCollection);
    }

    [Test]
    public void ApplySecurity_ClearsEncryptionCredentials()
    {
        var s = new JwtSecurityModeHmacSignature(_key);

        var tokenDesc = new SecurityTokenDescriptor()
        {
            EncryptingCredentials = new EncryptingCredentials(new SymmetricSecurityKey(new byte[64]), SecurityAlgorithms.Aes128CbcHmacSha256)
        };
        s.ApplySecurity(tokenDesc);

        Assert.That(tokenDesc.EncryptingCredentials, Is.Null);
    }

    [Test]
    [TestCase(SecurityAlgorithms.HmacSha512, 64)]
    [TestCase(SecurityAlgorithms.HmacSha384, 48)]
    [TestCase(SecurityAlgorithms.HmacSha256, 32)]
    public void ApplyValidation_SetsValidAlgorithms(string alg, int keyLen)
    {
        var s = new JwtSecurityModeHmacSignature(new byte[keyLen]);

        var tvps = new TokenValidationParameters();
        s.ApplyValidation(tvps);

        Assert.That(tvps.ValidAlgorithms, Is.EquivalentTo(new string[] { alg }));
    }

    [Test]
    public void ApplyValidation_SetsKey()
    {
        var s = new JwtSecurityModeHmacSignature(_key);
        var thumbprint = new SymmetricSecurityKey(_key).ComputeJwkThumbprint();

        var tvps = new TokenValidationParameters();
        s.ApplyValidation(tvps);

        Assert.That(tvps.IssuerSigningKey.ComputeJwkThumbprint(), Is.EqualTo(thumbprint).AsCollection);
    }

    [Test]
    public void ApplyValidation_SetsRequireSignature()
    {
        var s = new JwtSecurityModeHmacSignature(_key);

        var tvps = new TokenValidationParameters() { RequireSignedTokens = false };
        s.ApplyValidation(tvps);

        Assert.That(tvps.RequireSignedTokens);
    }

    [Test]
    public void ApplyValidation_SetsValidateIssuerSigningKey()
    {
        var s = new JwtSecurityModeHmacSignature(_key);

        var tvps = new TokenValidationParameters() { ValidateIssuerSigningKey = false };
        s.ApplyValidation(tvps);

        Assert.That(tvps.ValidateIssuerSigningKey);
    }

}
