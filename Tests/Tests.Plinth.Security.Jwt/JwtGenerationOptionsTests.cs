using NUnit.Framework;
using Plinth.Security.Jwt;

namespace Tests.Plinth.Security.Jwt;

[TestFixture]
public class JwtGenerationOptionsTests
{
    private readonly IJwtSecurityMode _validSecurityMode = new JwtSecurityModeHmacSignature(new byte[64]);

    [Test]
    public void PostValidate_Valid_Succeeds()
    {
        var opts = new JwtGenerationOptions
        {
            SecurityMode = _validSecurityMode,
        };

        opts.Validate();
    }

    [Test]
    public void PostValidate_NoSecurityMode_Fails()
    {
        var opts = new JwtGenerationOptions
        {
        };

        Assert.Throws<InvalidDataException>(() => opts.Validate());
    }

    [Test]
    public void PostValidate_NegativeLifetime_Fails()
    {
        var opts = new JwtGenerationOptions
        {
            SecurityMode = _validSecurityMode,
            TokenLifetime = TimeSpan.FromSeconds(-1),
        };

        Assert.Throws<InvalidDataException>(() => opts.Validate());
    }

    [Test]
    public void PostValidate_ZeroLifetime_Fails()
    {
        var opts = new JwtGenerationOptions
        {
            SecurityMode = _validSecurityMode,
            TokenLifetime = TimeSpan.Zero
        };

        Assert.Throws<InvalidDataException>(() => opts.Validate());
    }

    [Test]
    public void PostValidate_MaxEqualLifetime_Fails()
    {
        var opts = new JwtGenerationOptions
        {
            SecurityMode = _validSecurityMode,
            TokenLifetime = TimeSpan.FromMinutes(10),
            MaxTokenLifetime = TimeSpan.FromMinutes(10)
        };

        Assert.Throws<InvalidDataException>(() => opts.Validate());
    }

    [Test]
    public void PostValidate_MaxLessThanLifetime_Fails()
    {
        var opts = new JwtGenerationOptions
        {
            SecurityMode = _validSecurityMode,
            TokenLifetime = TimeSpan.FromMinutes(10),
            MaxTokenLifetime = TimeSpan.FromMinutes(5)
        };

        Assert.Throws<InvalidDataException>(() => opts.Validate());
    }

    [Test]
    public void PostValidate_NegativeClockSkew_Fails()
    {
        var opts = new JwtGenerationOptions
        {
            SecurityMode = _validSecurityMode,
            ClockSkew = TimeSpan.FromSeconds(-1)
        };

        Assert.Throws<InvalidDataException>(() => opts.Validate());
    }

    [Test]
    public void PostValidate_ZeroClockSkew_Succeeds()
    {
        var opts = new JwtGenerationOptions
        {
            SecurityMode = _validSecurityMode,
            ClockSkew = TimeSpan.Zero
        };

        opts.Validate();
    }

    [Test]
    public void PostValidate_GreaterZeroClockSkew_Succeeds()
    {
        var opts = new JwtGenerationOptions
        {
            SecurityMode = _validSecurityMode,
            ClockSkew = TimeSpan.FromSeconds(1)
        };

        opts.Validate();
    }

    [Test]
    public void PostValidate_NullClockSkew_Succeeds()
    {
        var opts = new JwtGenerationOptions
        {
            SecurityMode = _validSecurityMode,
            ClockSkew = null
        };

        opts.Validate();
    }
}
