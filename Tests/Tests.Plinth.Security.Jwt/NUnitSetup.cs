using Plinth.Logging.Host;
using NUnit.Framework;

namespace Tests.Plinth.Security.Jwt;

[SetUpFixture]
public class NUnitSetup
{
    [OneTimeSetUp]
    public void Setup()
    {
        StaticLogManagerSetup.ConfigureForDebugLogging();
    }
}
