using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using NUnit.Framework;
using Plinth.Security.Jwt;

namespace Tests.Plinth.Security.Jwt;

[TestFixture]
public class JwtSecurityModeAesEncryptionTests
{
    private readonly byte[] _key = Convert.FromHexString("33674aec26664b4838113f7c5185fa51218b816e80302e70749c8968b5d95087");

    [Test]
    [TestCase(0)]
    [TestCase(1)]
    [TestCase(31)]
    [TestCase(33)]
    [TestCase(47)]
    [TestCase(49)]
    [TestCase(63)]
    [TestCase(65)]
    public void Constructor_InvalidKeyLength_Fails(int keyLength)
    {
        Assert.Throws<ArgumentException>(() => new JwtSecurityModeAesEncryption(new byte[keyLength]));
    }

    [Test]
    public void Constructor_NullKey_Fails()
    {
        Assert.Throws<ArgumentNullException>(() => new JwtSecurityModeAesEncryption(null!));
    }

    [Test]
    public void ApplySecurity_SetsEncryptingCredentials()
    {
        var s = new JwtSecurityModeAesEncryption(_key);
        var thumbprint = new SymmetricSecurityKey(_key).ComputeJwkThumbprint();

        var tokenDesc = new SecurityTokenDescriptor();
        s.ApplySecurity(tokenDesc);

        Assert.That(tokenDesc.EncryptingCredentials, Is.Not.Null);
        Assert.That(tokenDesc.EncryptingCredentials.Key.ComputeJwkThumbprint(), Is.EqualTo(thumbprint).AsCollection);
    }

    [Test]
    public void ApplySecurity_ClearsSigningCredentials()
    {
        var s = new JwtSecurityModeAesEncryption(_key);

        var tokenDesc = new SecurityTokenDescriptor()
        {
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(new byte[64]), SecurityAlgorithms.HmacSha256Signature)
        };
        s.ApplySecurity(tokenDesc);

        Assert.That(tokenDesc.SigningCredentials, Is.Null);
    }

    [Test]
    [TestCase(SecurityAlgorithms.Aes256CbcHmacSha512, 64)]
    [TestCase(SecurityAlgorithms.Aes192CbcHmacSha384, 48)]
    [TestCase(SecurityAlgorithms.Aes128CbcHmacSha256, 32)]
    public void ApplyValidation_SetsValidAlgorithms(string alg, int keyLen)
    {
        var s = new JwtSecurityModeAesEncryption(new byte[keyLen]);

        var tvps = new TokenValidationParameters();
        s.ApplyValidation(tvps);

        Assert.That(tvps.ValidAlgorithms, Is.EquivalentTo(new string[] { JwtConstants.DirectKeyUseAlg, alg }));
    }

    [Test]
    public void ApplyValidation_DisablesSignatureValidation()
    {
        var s = new JwtSecurityModeAesEncryption(_key);

        var tvps = new TokenValidationParameters()
        {
            RequireSignedTokens = true
        };
        s.ApplyValidation(tvps);

        Assert.That(tvps.RequireSignedTokens, Is.False);
    }

    [Test]
    public void ApplyValidation_SetsKey()
    {
        var s = new JwtSecurityModeAesEncryption(_key);
        var thumbprint = new SymmetricSecurityKey(_key).ComputeJwkThumbprint();

        var tvps = new TokenValidationParameters();
        s.ApplyValidation(tvps);

        Assert.That(tvps.TokenDecryptionKey.ComputeJwkThumbprint(), Is.EqualTo(thumbprint).AsCollection);
    }
}
