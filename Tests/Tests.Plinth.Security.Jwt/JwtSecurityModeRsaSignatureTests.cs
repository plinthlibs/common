using Microsoft.IdentityModel.Tokens;
using NUnit.Framework;
using Plinth.Security.Jwt;
using System.Security.Cryptography;

namespace Tests.Plinth.Security.Jwt;

[TestFixture]
public class JwtSecurityModeRsaSignatureTests
{

    [Test]
    [TestCase(-1)]
    [TestCase(0)]
    [TestCase(1)]
    [TestCase(255)]
    [TestCase(257)]
    [TestCase(383)]
    [TestCase(385)]
    [TestCase(511)]
    [TestCase(513)]
    public void Constructor_InvalidHashLength_Fails(int hashLength)
    {
        Assert.Throws<ArgumentException>(() => new JwtSecurityModeRsaSignature(_publicKey2048, _privateKey2048, hashLength));
    }

    [Test]
    public void Constructor_NullPublicKey_Fails()
    {
        Assert.Throws<ArgumentNullException>(() => new JwtSecurityModeRsaSignature(null!));
    }

    [Test]
    public void Constructor_InvalidPublicKey_Fails()
    {
        Assert.Throws<ArgumentException>(() => new JwtSecurityModeRsaSignature("not a pem"));
    }

    [Test]
    public void Constructor_InvalidPrivateKey_Fails()
    {
        Assert.Throws<ArgumentException>(() => new JwtSecurityModeRsaSignature(_publicKey2048, "not a pem"));
    }

    [Test]
    public void Constructor_PrivateKeyTooSmall_Fails()
    {
        Assert.Throws<InvalidOperationException>(() => new JwtSecurityModeRsaSignature(_publicKey1024, _privateKey1024));
    }

    [Test]
    public void Constructor_PublicKeyMismatch_Fails()
    {
        Assert.Throws<InvalidOperationException>(() =>
            new JwtSecurityModeRsaSignature(_publicKey2048Alt, _privateKey2048));
    }

    private void Assert_PrivateKey(string privateKey, SecurityKey securityKey)
    {
        var rsaPrivate = RSA.Create();
        rsaPrivate.ImportFromPem(privateKey.AsSpan());

        Assert.That(securityKey, Is.InstanceOf<RsaSecurityKey>());
        var rsaKey = (RsaSecurityKey)securityKey;

        Assert.That(rsaKey.Rsa.ExportRSAPrivateKey(), Is.EqualTo(rsaPrivate.ExportRSAPrivateKey()).AsCollection);
    }

    private void Assert_PublicKey(string publicKey, SecurityKey securityKey)
    {
        var rsaPrivate = RSA.Create();
        rsaPrivate.ImportFromPem(publicKey.AsSpan());

        Assert.That(securityKey, Is.InstanceOf<RsaSecurityKey>());
        var rsaKey = (RsaSecurityKey)securityKey;

        Assert.That(rsaKey.Rsa.ExportRSAPublicKey(), Is.EqualTo(rsaPrivate.ExportRSAPublicKey()).AsCollection);
    }

    [Test]
    public void ApplySecurity_SetsSigningCredentials()
    {
        var s = new JwtSecurityModeRsaSignature(_publicKey2048, _privateKey2048, 256);

        var tokenDesc = new SecurityTokenDescriptor();
        s.ApplySecurity(tokenDesc);

        Assert.That(tokenDesc.EncryptingCredentials, Is.Null);
        Assert.That(tokenDesc.SigningCredentials, Is.Not.Null);
        Assert_PrivateKey(_privateKey2048, tokenDesc.SigningCredentials.Key);
    }

    [Test]
    public void ApplySecurity_ClearsEncryptionCredentials()
    {
        var s = new JwtSecurityModeRsaSignature(_publicKey2048, _privateKey2048, 256);

        var tokenDesc = new SecurityTokenDescriptor()
        {
            EncryptingCredentials = new EncryptingCredentials(new SymmetricSecurityKey(new byte[64]), SecurityAlgorithms.Aes128CbcHmacSha256)
        };
        s.ApplySecurity(tokenDesc);

        Assert.That(tokenDesc.EncryptingCredentials, Is.Null);
    }

    [Test]
    public void ApplySecurity_NoPrivateKey_Fails()
    {
        Assert.Throws<InvalidOperationException>(() =>
            new JwtSecurityModeRsaSignature(_publicKey2048).ApplySecurity(new SecurityTokenDescriptor()));
    }

    [Test]
    [TestCase(SecurityAlgorithms.RsaSha512, 512)]
    [TestCase(SecurityAlgorithms.RsaSha384, 384)]
    [TestCase(SecurityAlgorithms.RsaSha256, 256)]
    public void ApplyValidation_SetsValidAlgorithms(string alg, int hashLen)
    {
        var s = new JwtSecurityModeRsaSignature(_publicKey2048, _privateKey2048, hashLen);

        var tvps = new TokenValidationParameters();
        s.ApplyValidation(tvps);

        Assert.That(tvps.ValidAlgorithms, Is.EquivalentTo(new string[] { alg }));
    }

    [Test]
    public void ApplyValidation_SetsKey()
    {
        var s = new JwtSecurityModeRsaSignature(_publicKey2048, _privateKey2048);

        var tvps = new TokenValidationParameters();
        s.ApplyValidation(tvps);

        Assert_PublicKey(_publicKey2048, tvps.IssuerSigningKey);
    }

    [Test]
    public void ApplyValidation_NoPrivateKey_SetsKey()
    {
        var s = new JwtSecurityModeRsaSignature(_publicKey2048);

        var tvps = new TokenValidationParameters();
        s.ApplyValidation(tvps);

        Assert_PublicKey(_publicKey2048, tvps.IssuerSigningKey);
    }


    [Test]
    public void ApplyValidation_SetsRequireSignature()
    {
        var s = new JwtSecurityModeRsaSignature(_publicKey2048);

        var tvps = new TokenValidationParameters() { RequireSignedTokens = false };
        s.ApplyValidation(tvps);

        Assert.That(tvps.RequireSignedTokens);
    }

    [Test]
    public void ApplyValidation_SetsValidateIssuerSigningKey()
    {
        var s = new JwtSecurityModeRsaSignature(_publicKey2048);

        var tvps = new TokenValidationParameters() { ValidateIssuerSigningKey = false };
        s.ApplyValidation(tvps);

        Assert.That(tvps.ValidateIssuerSigningKey);
    }

    // openssl genrsa -out private.pem 2048
    private readonly string _privateKey2048 = @"-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAulGQ01HdwQ0pB5bP/gL51Mv3SjC/ph/PuL2HWyB5faxFcC4V
LApGAeRl7DqrhWIe+GwczW6cuiOeAKgxEDtyTMdk9S6S0nk2PwUGBNc499PF8dmn
WSXfs0fY0/yQbp0D6JdVCCS/RhQCni5ssDMCo3BdxLdNqGzb5tNVv+YYfkhh8c+p
YrvGeUWgQxftDih5inlBfunzEaZmr3yJ6deeKLpFGl6kz7sSFSaCtzXpBwdLJ2AP
UQHcki7iyfHs+H9/Eiiq+DRr/QRHbNbytW5zbikjQsO4ZORPhEJ/XzA9tcbxegxU
0KtcHK/ZwaqlErxEDTifiTm7cun2kOtStwTaJQIDAQABAoIBAQC5aiBo28iFrJYP
MlsMGmQLC2utjjO6m1+5JguEeR3OHdkAWSvCpvqiOWvgmUZFt67AY76KBbC8YGOF
ndstnysRE0D56f48rFl6f4JM4O2VW09abdJnGhSzMiWqWGZXXbQdDDSXpRIgKBuu
1N5evnS6t0DpWc1TdfmkjN2B2fa3+5c7r2yZ9gvhyu4MNMjgFoiaZCwW25cG6ioW
WGFlS6Hq55fELW3mDKceWI1idPK0k8oBfal+ma43C6g9FJT6TxB2v3glB0FXt7xZ
2NKEN8PrEi89eGBGhbDHq82fuSAOl72CFoYw/cM6Kr9RcXPzLTmgaIO8etNIsoFu
TgN5y2jlAoGBAOQp6srI9yjCiwEFaTc/qJc5hrA9kKPqCSKo+vt/z0R3xTGJb311
0uZ/9ITF6a4HI2AMMq8o88mT5Oqwd5+7vJmxkUb2kfef/zhGkL6EsQxj/G3fxN5f
HodCQSXNMwxCUtXOfbvI5akJiCtjLPj98FuXdMZqZe5d0mXTY92W46SbAoGBANEM
unLI4E0nIf9ooQ7R2Z+/l81hxceWPQ7kM5wnXsZ/JuSU+/kmN5zJX+8Z6TAx/HTo
iP+gQr9nDdNXOPA9twjqyZVEImZsyYFWFjxt5ts8OJPFt3rmFZPDpEsLeeERMqSB
KxOD7XwfLZg021iu5Y1pY2GFvKutNCH8kxS894g/AoGAR8GTEPGBIbWFR+3uuCxy
FhVGcbj6+0IIW3kvbdc1VUAhqUGi6Z8bk9hcC0G/Czc9feB4NDQ574RuswyKopbF
asS4fsve/jS4bHds4C4VDGOJqPh+Dr11eW5x1LSjQFC/gnRtgJhe+fmAdLy9ERWz
+Oo6FIizw/KHz/RabP8eWs8CgYEAikZGCWivXLkWts8pQRkzPoRHT7VtUgAhMPlm
3dH40YkHW8jkBj/uJwnYJ9pWsOdPWrD49US3Ac2Tiog2BEp+CCrs3YusyV0JyD/+
A0iam8bwkWYFYAhSm2OcB3t2yZVLjx5VNNpdum0RBo1nzPgapHfiIX6JdnDhe92K
F/yKKpcCgYAJju2rfx6U2DO9LmFqS9RqkOLG3ZaL2Qgrl3xgwEubwm29jytojczn
XXCAKdW8lBr1RdXS0l/MLnUvdZhE90TW7F+3nOdnPpjpmCJqcJVE82eUj53t/r0b
TUf2U5pT6Xwau/ooHkE56u7bSVgMg3t3ymfM7awjcCAbBhkKRDLpvw==
-----END RSA PRIVATE KEY-----";

    // openssl rsa -in private.pem -RSAPublicKey_out
    private readonly string _publicKey2048 = @"-----BEGIN RSA PUBLIC KEY-----
MIIBCgKCAQEAulGQ01HdwQ0pB5bP/gL51Mv3SjC/ph/PuL2HWyB5faxFcC4VLApG
AeRl7DqrhWIe+GwczW6cuiOeAKgxEDtyTMdk9S6S0nk2PwUGBNc499PF8dmnWSXf
s0fY0/yQbp0D6JdVCCS/RhQCni5ssDMCo3BdxLdNqGzb5tNVv+YYfkhh8c+pYrvG
eUWgQxftDih5inlBfunzEaZmr3yJ6deeKLpFGl6kz7sSFSaCtzXpBwdLJ2APUQHc
ki7iyfHs+H9/Eiiq+DRr/QRHbNbytW5zbikjQsO4ZORPhEJ/XzA9tcbxegxU0Ktc
HK/ZwaqlErxEDTifiTm7cun2kOtStwTaJQIDAQAB
-----END RSA PUBLIC KEY-----";

    private readonly string _publicKey2048Alt = @"-----BEGIN RSA PUBLIC KEY-----
MIIBCgKCAQEAsOJdHcsXV2Nhg0pYoBD85EDljuH6fOB00MNzWYuUi6MbgnyJjrSc
AFvI9uMFS16QnROi3CoXLg42O6AKJgCemlmqjz7xGeij76s/8xMYFy8AIxoRMcMX
B70AkC2hL3ahAiiVCzCBVkI74GhFfCwy03M6LOfGyMtWwwmk4VPMABYM+kZsUqaT
h98X5H9oqAxhYhBu6EU9ynV3oMXeWldRTKV3g8W5A/2idPyHV6u6j1jE+cnqyNkC
L5jsa20OOEMVtuPVGKKedtmD0bdujyWMUZ6tESy835aPO/FD7Ld8HNDFSgScxh+i
SmfSLALO2A99PlFukL6jILUswyfe5+WABwIDAQAB
-----END RSA PUBLIC KEY-----";

    private readonly string _privateKey1024 = @"-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQD58sDCqtIMFeXxhemUDS18dqXjpZJNRHNw0j8ZqhOgQy24Ywnv
dg53FwCgp3VfD0SS+4m4p0tqOqljYRu0Bv61qvbkR18gE6J9CMbKMtpJdiQQ26kQ
QydM89Dke4b7hYdx/a25OuPgWI/ELubFcXSrYx5kEAHyvIlifGtftREh4QIDAQAB
AoGAGohvd/rEM247q6jog8PmWTkLISBZaIRksMxQ0Huie7fF6bXxvw7MOE4jzeoY
5o9rWyMCmqx/r/YsNxV+6Fx5uz+G/+OAePYNAZTNKpwIzVXM+Q9tyQOY0oJKXA6C
6yGQuuAaKANBmAiCixhdZN51xBwF3sXMdlzmshB7bi8c3fECQQD+maIJbxLbtPaw
T9RM53t/muEblNSaXEZvhSBgGa7eh5YTkFR11ewF33eg0kotIRkDFqbVSkiMz/qN
Afdhi/jNAkEA+1KSepEPcaChsXNEAwpEigJBXIwtLxKz+dT4lceKO8bX8lHJ/AHy
oGI6tivW3HXrbslparq/9Qe2WBUUe0vdZQJAK/XzGJXpN66DeeYOzkiZ9mqTu+Ny
4kRUlSTzdiR5IUtgCni9XKtXWZxAV7B3xlsUqofavrFY3Zd7t6VDRcCZcQJAVXth
bST33ssnFCdDg9EzPMnFl4fqOm+fHaPNpYxIPUrF2prtG7VB5EFk/N9uZnAJUxZY
r7+zWNF1uQH6hHFqbQJBANLQdRpsnkodMj26IVc+N6mdjRgjNhynQwEZuoRKcaLa
4gobSAm2Qh5hqqq2jUr9r/ZYy4WNmdxcGuTlSRLlJ98=
-----END RSA PRIVATE KEY-----";

    private readonly string _publicKey1024 = @"-----BEGIN RSA PUBLIC KEY-----
MIGJAoGBAPnywMKq0gwV5fGF6ZQNLXx2peOlkk1Ec3DSPxmqE6BDLbhjCe92DncX
AKCndV8PRJL7ibinS2o6qWNhG7QG/rWq9uRHXyATon0Ixsoy2kl2JBDbqRBDJ0zz
0OR7hvuFh3H9rbk64+BYj8Qu5sVxdKtjHmQQAfK8iWJ8a1+1ESHhAgMBAAE=
-----END RSA PUBLIC KEY-----";
}
