using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using Plinth.Security.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;

using SystemJwt = System.IdentityModel.Tokens.Jwt;

namespace Tests.Plinth.Security.Jwt;

[TestFixture]
public class JwtValidatorTests
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private readonly byte[] _key = Convert.FromHexString("33674aec26664b4838113f7c5185fa51218b816e80302e70749c8968b5d95087");

    [SetUp]
    public void SetUp()
    {
        IdentityModelEventSource.ShowPII = true;
    }

    private JwtGenerationOptions GetOptions(string secMode) => new JwtGenerationOptions
        {
            Issuer = "https://plinth.com/unittests",
            Audience = "https://plinth.com",
            TokenContentLogging = true,
            TokenLifetime = TimeSpan.FromMinutes(10),
            SecurityMode = secMode switch
            {
                "sign" => new JwtSecurityModeHmacSignature(_key),
                "encrypt" => new JwtSecurityModeAesEncryption(_key),
                _ => throw new NotImplementedException()
            }
        };

    [Test]
    public void Construct_NullOptions_Fails()
    {
        Assert.Throws<ArgumentNullException>(() => new JwtValidator(null!));
    }

    [Test]
    [TestCase("sign")]
    [TestCase("encrypt")]
    public void Validate_EverythingOk_Succeeds(string secMode)
    {
        var opts = GetOptions(secMode);

        var token = new JwtBuilder(opts)
            .Subject(Guid.NewGuid(), "name")
            .Build();

        var cp = new JwtValidator(opts).Validate(token.Jwt);
        Assert.That(cp.JwtUserName(), Is.EqualTo("name"));
    }

    [Test]
    [TestCase(false, true, "should be no logs")]
    [TestCase(false, false, "should be no logs")]
    [TestCase(false, null, "should be no logs")]
    [TestCase(true, true, "should be two log lines, create and validate")]
    [TestCase(true, false, "should only be create log")]
    [TestCase(true, null, "should be two log lines, create and validate")]
    public void Validate_LoggingSetup(bool optsLogging, bool? validateLogging, string debug)
    {
        var opts = GetOptions("sign");
        opts.TokenContentLogging = optsLogging;

        log.Debug(debug);

        var token = new JwtBuilder(opts)
            .Subject(Guid.NewGuid(), "name")
            .Build();

        // verify via debug.
        var cp = new JwtValidator(opts).Validate(token.Jwt, loggingEnabled: validateLogging);
        Assert.That(cp.JwtUserName(), Is.EqualTo("name"));
    }

    [Test]
    public void Validate_IgnoreIssuer_Succeeds()
    {
        var optsWithIssuer = GetOptions("sign");
        optsWithIssuer.Issuer = "https://plinth.com/unittests";

        var optsNoIssuer = GetOptions("sign");
        optsNoIssuer.Issuer = null;

        var token = new JwtBuilder(optsWithIssuer)
            .Subject(Guid.NewGuid(), "name")
            .Build();

        var cp = new JwtValidator(optsNoIssuer).Validate(token.Jwt);
        Assert.That(cp.JwtUserName(), Is.EqualTo("name"));
    }

    [Test]
    [TestCase("wrongIssuer")]
    [TestCase(null)]
    public void Validate_WrongIssuer_Fails(string? invalidIssuer)
    {
        var optsWithIssuer = GetOptions("sign");
        optsWithIssuer.Issuer = "https://plinth.com/unittests";

        var optsWrongIssuer = GetOptions("sign");
        optsWrongIssuer.Issuer = invalidIssuer;

        var token = new JwtBuilder(optsWrongIssuer)
            .Subject(Guid.NewGuid(), "name")
            .Build();

        Assert.Throws<SecurityTokenInvalidIssuerException>(
            () => new JwtValidator(optsWithIssuer).Validate(token.Jwt));
    }

    [Test]
    public void Validate_IgnoreAudience_Succeeds()
    {
        var optsWithAudience = GetOptions("sign");
        optsWithAudience.Audience = "https://plinth.com/";

        var optsNoAudience = GetOptions("sign");
        optsNoAudience.Audience = null;

        var token = new JwtBuilder(optsWithAudience)
            .Subject(Guid.NewGuid(), "name")
            .Build();

        var cp = new JwtValidator(optsWithAudience).Validate(token.Jwt);
        Assert.That(cp.JwtUserName(), Is.EqualTo("name"));
    }

    [Test]
    [TestCase("wrongAudience")]
    [TestCase(null)]
    public void Validate_WrongAudience_Fails(string? invalidAudience)
    {
        var optsWithAudience = GetOptions("sign");
        optsWithAudience.Audience = "https://plinth.com/unittests";

        var optsWrongAudience = GetOptions("sign");
        optsWrongAudience.Audience = invalidAudience;

        var token = new JwtBuilder(optsWrongAudience)
            .Subject(Guid.NewGuid(), "name")
            .Build();

        Assert.Throws<SecurityTokenInvalidAudienceException>(
            () => new JwtValidator(optsWithAudience).Validate(token.Jwt));
    }

    [Test]
    [TestCase("sign")]
    [TestCase("encrypt")]
    public void Validate_WrongKey_Fails(string secMode)
    {
        var optsKey1 = GetOptions(secMode);

        var wrongKey = Convert.FromHexString("491fde1d62de348b30242f2d475ec1fbe7ff86fd94726313d6af2f49991b3deaecc3c889c43856aeb9a7d7619312167ff709a233fea5f0b1cacdf675889cea46");
        var optsKey2 = GetOptions(secMode);
        optsKey2.SecurityMode = secMode switch
        {
            "sign" => new JwtSecurityModeHmacSignature(wrongKey),
            "encrypt" => new JwtSecurityModeAesEncryption(wrongKey),
            _ => throw new NotImplementedException()
        };

        var token = new JwtBuilder(optsKey1)
            .Subject(Guid.NewGuid(), "name")
            .Build();

        switch (secMode)
        {
            case "sign":
                Assert.Throws<SecurityTokenInvalidSignatureException>(
                    () => new JwtValidator(optsKey2).Validate(token.Jwt));
                break;
            case "encrypt":
                Assert.Throws<SecurityTokenDecryptionFailedException>(
                    () => new JwtValidator(optsKey2).Validate(token.Jwt));
                break;
            default: throw new NotSupportedException();
        }
    }

    [Test]
    public void Validate_Expired_Fails()
    {
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity([
                new Claim(ClaimTypes.NameIdentifier, "name")
            ]),
            Expires = DateTime.UtcNow - TimeSpan.FromSeconds(5),
            IssuedAt = DateTime.UtcNow - TimeSpan.FromHours(1),
            NotBefore = DateTime.UtcNow - TimeSpan.FromHours(1),
            SigningCredentials =
                new SigningCredentials(new SymmetricSecurityKey(_key), SecurityAlgorithms.HmacSha256Signature)
        };

        var tokenHandler = new SystemJwt.JwtSecurityTokenHandler();
        var token = tokenHandler.CreateToken(tokenDescriptor);
        var jwt = tokenHandler.WriteToken(token);

        var opts = GetOptions("sign");
        opts.Audience = null;
        opts.Issuer = null;
        opts.ClockSkew = TimeSpan.Zero;

        Assert.Throws<SecurityTokenExpiredException>(
            () => new JwtValidator(opts).Validate(jwt));
    }

    [Test]
    public void Validate_ExpiredWithoutClockSkew_Succeed()
    {
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity([
                new Claim("unique_name", "name")
            ]),
            Expires = DateTime.UtcNow - TimeSpan.FromSeconds(5),
            IssuedAt = DateTime.UtcNow - TimeSpan.FromHours(1),
            NotBefore = DateTime.UtcNow - TimeSpan.FromHours(1),
            SigningCredentials =
                new SigningCredentials(new SymmetricSecurityKey(_key), SecurityAlgorithms.HmacSha256Signature)
        };

        var tokenHandler = new SystemJwt.JwtSecurityTokenHandler();
        var token = tokenHandler.CreateToken(tokenDescriptor);
        var jwt = tokenHandler.WriteToken(token);

        var opts = GetOptions("sign");
        opts.Audience = null;
        opts.Issuer = null;
        opts.ClockSkew = null;

        var cp = new JwtValidator(opts).Validate(jwt);
        Assert.That(cp.JwtUserName(), Is.EqualTo("name"));
    }

    [Test]
    public void Validate_WrongHmacAlg_Fails()
    {
        var key = ECDsa.Create(ECCurve.NamedCurves.nistP256);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity([
                new Claim(ClaimTypes.NameIdentifier, "name")
            ]),
            Expires = DateTime.UtcNow + TimeSpan.FromHours(1),
            SigningCredentials =
                new SigningCredentials(new ECDsaSecurityKey(key), "ES256")
        };

        var tokenHandler = new SystemJwt.JwtSecurityTokenHandler();
        var token = tokenHandler.CreateToken(tokenDescriptor);
        var jwt = tokenHandler.WriteToken(token);

        var opts = GetOptions("sign");
        Assert.Throws<SecurityTokenSignatureKeyNotFoundException>(
            () => new JwtValidator(opts).Validate(jwt));
    }

    [Test]
    public void Validate_NoSignature_Fails()
    {
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity([
                new Claim(ClaimTypes.NameIdentifier, "name")
            ]),
            Expires = DateTime.UtcNow + TimeSpan.FromHours(1)
        };

        var tokenHandler = new SystemJwt.JwtSecurityTokenHandler();
        var token = tokenHandler.CreateToken(tokenDescriptor);
        var jwt = tokenHandler.WriteToken(token);

        var opts = GetOptions("sign");
        opts.Audience = null;
        opts.Issuer = null;
        Assert.Throws<SecurityTokenInvalidSignatureException>(
            () => new JwtValidator(opts).Validate(jwt));
    }

    [Test]
    public void Validate_WrongAesAlg_Fails()
    {
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity([
                new Claim(ClaimTypes.NameIdentifier, "name")
            ]),
            Expires = DateTime.UtcNow + TimeSpan.FromHours(1),
            EncryptingCredentials = 
                new EncryptingCredentials(new SymmetricSecurityKey(new byte[48]),
                    Microsoft.IdentityModel.JsonWebTokens.JwtConstants.DirectKeyUseAlg,
                    SecurityAlgorithms.Aes192CbcHmacSha384)
        };

        var tokenHandler = new SystemJwt.JwtSecurityTokenHandler();
        var token = tokenHandler.CreateToken(tokenDescriptor);
        var jwt = tokenHandler.WriteToken(token);

        var opts = GetOptions("encrypt");
        opts.Audience = null;
        opts.Issuer = null;
        Assert.Throws<SecurityTokenDecryptionFailedException>(
            () => new JwtValidator(opts).Validate(jwt));
    }

    [Test]
    public void Validate_WrongAesKeyAlg_Fails()
    {
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity([
                new Claim(ClaimTypes.NameIdentifier, "name")
            ]),
            Expires = DateTime.UtcNow + TimeSpan.FromHours(1),
            EncryptingCredentials = 
                new EncryptingCredentials(new SymmetricSecurityKey(_key[..32]), SecurityAlgorithms.Aes256KW, 
                    SecurityAlgorithms.Aes256CbcHmacSha512)
        };

        var tokenHandler = new SystemJwt.JwtSecurityTokenHandler();
        var token = tokenHandler.CreateToken(tokenDescriptor);
        var jwt = tokenHandler.WriteToken(token);

        var opts = GetOptions("encrypt");
        opts.Audience = null;
        opts.Issuer = null;
        Assert.Throws<SecurityTokenDecryptionFailedException>(
            () => new JwtValidator(opts).Validate(jwt));
    }

    [Test]
    public void Validate_WrongRsaOeapHashLen_Fails()
    {
        int[] lens = [160, 256, 384, 512];

        foreach (int len1 in lens)
        {
            foreach (int len2 in lens)
            {
                if (len1 == len2)
                    continue;

                var opts = GetOptions("encrypt");
                opts.SecurityMode = new JwtSecurityModeRsaEncryption(_privateKey2048,
                    aesKeyLen: 256, rsaOaepHashLen: len1);

                var builder = new JwtBuilder(opts);
                var token = builder.Subject(Guid.NewGuid(), "name").Build();

                var opts2 = GetOptions("encrypt");
                opts.SecurityMode = new JwtSecurityModeRsaEncryption(_privateKey2048,
                    aesKeyLen: 256, rsaOaepHashLen: len2);

                Assert.Throws<SecurityTokenDecryptionFailedException>(
                    () => new JwtValidator(opts2).Validate(token.Jwt));
            }
        }
    }

    [Test]
    public void Validate_WrongRsaAesKeyLen_Fails()
    {
        int[] lens = [128, 192, 256];

        foreach (int len1 in lens)
        {
            foreach (int len2 in lens)
            {
                if (len1 == len2)
                    continue;

                var opts = GetOptions("encrypt");
                opts.SecurityMode = new JwtSecurityModeRsaEncryption(_privateKey2048,
                    aesKeyLen: len1);

                var builder = new JwtBuilder(opts);
                var token = builder.Subject(Guid.NewGuid(), "name").Build();

                var opts2 = GetOptions("encrypt");
                opts.SecurityMode = new JwtSecurityModeRsaEncryption(_privateKey2048,
                    aesKeyLen: len2);

                Assert.Throws<SecurityTokenKeyWrapException>(
                    () => new JwtValidator(opts2).Validate(token.Jwt));
            }
        }
    }

    private readonly string _privateKey2048 = @"-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAulGQ01HdwQ0pB5bP/gL51Mv3SjC/ph/PuL2HWyB5faxFcC4V
LApGAeRl7DqrhWIe+GwczW6cuiOeAKgxEDtyTMdk9S6S0nk2PwUGBNc499PF8dmn
WSXfs0fY0/yQbp0D6JdVCCS/RhQCni5ssDMCo3BdxLdNqGzb5tNVv+YYfkhh8c+p
YrvGeUWgQxftDih5inlBfunzEaZmr3yJ6deeKLpFGl6kz7sSFSaCtzXpBwdLJ2AP
UQHcki7iyfHs+H9/Eiiq+DRr/QRHbNbytW5zbikjQsO4ZORPhEJ/XzA9tcbxegxU
0KtcHK/ZwaqlErxEDTifiTm7cun2kOtStwTaJQIDAQABAoIBAQC5aiBo28iFrJYP
MlsMGmQLC2utjjO6m1+5JguEeR3OHdkAWSvCpvqiOWvgmUZFt67AY76KBbC8YGOF
ndstnysRE0D56f48rFl6f4JM4O2VW09abdJnGhSzMiWqWGZXXbQdDDSXpRIgKBuu
1N5evnS6t0DpWc1TdfmkjN2B2fa3+5c7r2yZ9gvhyu4MNMjgFoiaZCwW25cG6ioW
WGFlS6Hq55fELW3mDKceWI1idPK0k8oBfal+ma43C6g9FJT6TxB2v3glB0FXt7xZ
2NKEN8PrEi89eGBGhbDHq82fuSAOl72CFoYw/cM6Kr9RcXPzLTmgaIO8etNIsoFu
TgN5y2jlAoGBAOQp6srI9yjCiwEFaTc/qJc5hrA9kKPqCSKo+vt/z0R3xTGJb311
0uZ/9ITF6a4HI2AMMq8o88mT5Oqwd5+7vJmxkUb2kfef/zhGkL6EsQxj/G3fxN5f
HodCQSXNMwxCUtXOfbvI5akJiCtjLPj98FuXdMZqZe5d0mXTY92W46SbAoGBANEM
unLI4E0nIf9ooQ7R2Z+/l81hxceWPQ7kM5wnXsZ/JuSU+/kmN5zJX+8Z6TAx/HTo
iP+gQr9nDdNXOPA9twjqyZVEImZsyYFWFjxt5ts8OJPFt3rmFZPDpEsLeeERMqSB
KxOD7XwfLZg021iu5Y1pY2GFvKutNCH8kxS894g/AoGAR8GTEPGBIbWFR+3uuCxy
FhVGcbj6+0IIW3kvbdc1VUAhqUGi6Z8bk9hcC0G/Czc9feB4NDQ574RuswyKopbF
asS4fsve/jS4bHds4C4VDGOJqPh+Dr11eW5x1LSjQFC/gnRtgJhe+fmAdLy9ERWz
+Oo6FIizw/KHz/RabP8eWs8CgYEAikZGCWivXLkWts8pQRkzPoRHT7VtUgAhMPlm
3dH40YkHW8jkBj/uJwnYJ9pWsOdPWrD49US3Ac2Tiog2BEp+CCrs3YusyV0JyD/+
A0iam8bwkWYFYAhSm2OcB3t2yZVLjx5VNNpdum0RBo1nzPgapHfiIX6JdnDhe92K
F/yKKpcCgYAJju2rfx6U2DO9LmFqS9RqkOLG3ZaL2Qgrl3xgwEubwm29jytojczn
XXCAKdW8lBr1RdXS0l/MLnUvdZhE90TW7F+3nOdnPpjpmCJqcJVE82eUj53t/r0b
TUf2U5pT6Xwau/ooHkE56u7bSVgMg3t3ymfM7awjcCAbBhkKRDLpvw==
-----END RSA PRIVATE KEY-----";

}
