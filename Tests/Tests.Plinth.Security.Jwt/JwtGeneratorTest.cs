using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using NUnit.Framework;
using Plinth.Security.Jwt;
using System.Security.Claims;

using SystemJwt = System.IdentityModel.Tokens.Jwt;

namespace Tests.Plinth.Security.Jwt;

[TestFixture]
public class JwtGeneratorTests
{
    private readonly byte[] _key = Convert.FromHexString("33674aec26664b4838113f7c5185fa51218b816e80302e70749c8968b5d95087");

    [SetUp]
    public void SetUp()
    {
        IdentityModelEventSource.ShowPII = true;
    }

    private JwtGenerationOptions GetOptions(string secMode) => new JwtGenerationOptions
        {
            Issuer = "https://plinth.com/unittests",
            Audience = "https://plinth.com",
            TokenContentLogging = true,
            TokenLifetime = TimeSpan.FromMinutes(10),
            SecurityMode = secMode switch
            {
                "sign" => new JwtSecurityModeHmacSignature(_key),
                "encrypt" => new JwtSecurityModeAesEncryption(_key),
                _ => throw new NotImplementedException()
            }
        };

    [Test]
    public void Construct_NullOptions_Fails()
    {
        Assert.Throws<ArgumentNullException>(() => new JwtGenerator(null!, new JwtValidator(GetOptions("sign"))));
        Assert.Throws<ArgumentNullException>(() => new JwtGenerator(null!, null!));
        Assert.Throws<ArgumentNullException>(() => new JwtGenerator(GetOptions("sign"), null!));
    }

    [Test]
    [TestCase("sign")]
    [TestCase("encrypt")]
    public void GetBuilder_IdName_Succeeds(string secMode)
    {
        var opts = GetOptions(secMode);
        var val = new JwtValidator(opts);
        var gen = new JwtGenerator(opts, val);
        var id = Guid.NewGuid();

        var token = gen.GetBuilder(id, "name").Build();

        var cp = val.Validate(token.Jwt);
        Assert.That(cp.JwtUserName(), Is.EqualTo("name"));
        Assert.That(cp.JwtUserId(), Is.EqualTo(id));
    }

    [Test]
    [TestCase("sign")]
    [TestCase("encrypt")]
    public void GetBuilder_SessionId_Matches(string secMode)
    {
        var opts = GetOptions(secMode);
        var val = new JwtValidator(opts);
        var gen = new JwtGenerator(opts, val);
        var id = Guid.NewGuid();

        var g = Guid.NewGuid();
        var token = gen.GetBuilder(id, "name").SessionGuid(g).Build();

        Assert.That(token.SessionGuid, Is.EqualTo(g));

        var cp = val.Validate(token.Jwt);
        Assert.That(cp.JwtUserName(), Is.EqualTo("name"));
        Assert.That(cp.JwtUserId(), Is.EqualTo(id));
    }

    [Test]
    [TestCase("sign")]
    [TestCase("encrypt")]
    public void GetBuilder_IdNameRoleList_Succeeds(string secMode)
    {
        var opts = GetOptions(secMode);
        var val = new JwtValidator(opts);
        var gen = new JwtGenerator(opts, val);
        var id = Guid.NewGuid();

        var token = gen.GetBuilder(id, "name", new List<string> { "admin", "user" }).Build();

        var cp = val.Validate(token.Jwt);
        Assert.That(cp.JwtUserName(), Is.EqualTo("name"));
        Assert.That(cp.JwtUserId(), Is.EqualTo(id));
        Assert.That(cp.IsInRole("admin"));
        Assert.That(cp.IsInRole("user"));
        Assert.That(cp.IsInRole("superAdmin"), Is.False);
    }

    [Test]
    [TestCase("sign")]
    [TestCase("encrypt")]
    public void GetBuilder_IdNameRoleParams_Succeeds(string secMode)
    {
        var opts = GetOptions(secMode);
        var val = new JwtValidator(opts);
        var gen = new JwtGenerator(opts, val);
        var id = Guid.NewGuid();

        var token = gen.GetBuilder(id, "name", "admin", "user").Build();

        var cp = val.Validate(token.Jwt);
        Assert.That(cp.JwtUserName(), Is.EqualTo("name"));
        Assert.That(cp.JwtUserId(), Is.EqualTo(id));
        Assert.That(cp.IsInRole("admin"));
        Assert.That(cp.IsInRole("user"));
        Assert.That(cp.IsInRole("superAdmin"), Is.False);
    }

    [Test]
    public void Refresh_IdNameRole_Succeeds()
    {
        var opts = GetOptions("sign");
        var val = new JwtValidator(opts);
        var gen = new JwtGenerator(opts, val);
        var id = Guid.NewGuid();

        var token = gen.GetBuilder(id, "name", "admin", "user").Build();
        var cp1 = val.Validate(token.Jwt);

        Thread.Sleep(TimeSpan.FromSeconds(1.1));

        var refreshed = gen.Refresh(token.Jwt);

        var cp2 = val.Validate(refreshed.Jwt);
        Assert.That(cp2.JwtUserName(), Is.EqualTo("name"));
        Assert.That(cp2.JwtUserId(), Is.EqualTo(id));
        Assert.That(cp2.IsInRole("admin"));
        Assert.That(cp2.IsInRole("user"));

        Assert.That(cp2.JwtUserId(), Is.EqualTo(cp1.JwtUserId()));
        Assert.That(cp2.JwtUserName(), Is.EqualTo(cp1.JwtUserName()));
        Assert.That(cp2.JwtOriginalIssue(), Is.EqualTo(cp1.JwtOriginalIssue()));
        Assert.That(cp2.JwtSessionGuid(), Is.EqualTo(cp1.JwtSessionGuid()));
    }

    [Test]
    [TestCase("sign")]
    [TestCase("encrypt")]
    public void Refresh_Execute_RoundTrips(string secMode)
    {
        var guid = new Guid("e651d1e2-58af-4aba-bb3e-794c0d699146");
        var sessGuid = new Guid("aaa1d1e2-58af-4aba-bb3e-794c0d699146");
        var ori = DateTimeOffset.UtcNow.AddDays(-1);
        var now = DateTimeOffset.UtcNow;

        var opts = GetOptions(secMode);
        var val = new JwtValidator(opts);
        var gen = new JwtGenerator(opts, val);
        opts.TokenLifetime = TimeSpan.FromHours(1);
        opts.MaxTokenLifetime = TimeSpan.FromDays(10);
        opts.CustomClaims.Add(new Claim("global", "55", ClaimValueTypes.Integer32));

        var token = gen.GetBuilder(guid, "name")
            .OriginalIssue(ori)
            .SessionGuid(sessGuid)
            .Roles("user", "admin")
            .AddClaim("test", "56")
            .AddClaim("test", "57")
            .AddClaim("other", "99")
            .Build();

        Thread.Sleep(TimeSpan.FromSeconds(1));

        var newJwt = gen.Refresh(token.Jwt);

        //Console.WriteLine(newJwt.Jwt);

        var cp = val.Validate(newJwt.Jwt);
        assertPrincipal(cp);

        void assertPrincipal(ClaimsPrincipal pr)
        {
            Assert.That(pr.Claims.Count(), Is.EqualTo(15));
            Assert.That(pr.IsInRole("user"));
            Assert.That(pr.IsInRole("admin"));
            Assert.That(pr.JwtOriginalIssue()?.ToUnixTimeSeconds(), Is.EqualTo(ori.ToUnixTimeSeconds()));

            Assert.That(pr.JwtExpiration().ToUnixTimeSeconds(), 
                Is.EqualTo((now + TimeSpan.FromHours(1)).ToUnixTimeSeconds())
                .Or.EqualTo((now + TimeSpan.FromHours(1) + TimeSpan.FromSeconds(1)).ToUnixTimeSeconds())
                .Or.EqualTo((now + TimeSpan.FromHours(1) + TimeSpan.FromSeconds(2)).ToUnixTimeSeconds()));

            Assert.That(pr.JwtUserName(), Is.EqualTo("name"));
            Assert.That(pr.JwtSessionGuid(), Is.EqualTo(sessGuid));

            Assert.That(pr.FindFirst("test")?.Value, Is.EqualTo("56"));
            Assert.That(pr.FindAll("test").Skip(1).Select(c => c.Value).First(), Is.EqualTo("57"));
            Assert.That(pr.FindFirst("other")?.Value, Is.EqualTo("99"));

            Assert.That(pr.FindFirst("global")?.Value, Is.EqualTo("55"));
        }
    }

    [Test]
    [TestCase("sign")]
    [TestCase("encrypt")]
    public void Refresh_PastMaxLifetime_Fails(string secMode)
    {
        var opts = GetOptions(secMode);
        opts.TokenLifetime = TimeSpan.FromHours(1);
        opts.MaxTokenLifetime = TimeSpan.FromHours(2);
        var val = new JwtValidator(opts);
        var gen = new JwtGenerator(opts, val);

        var token = gen.GetBuilder(Guid.NewGuid(), "name")
            .OriginalIssue(DateTimeOffset.UtcNow.AddDays(-1))
            .Roles("user", "admin")
            .Build();

        Assert.Throws<SecurityTokenExpiredException>(() =>
            gen.Refresh(token.Jwt));
    }

    [Test]
    [TestCase("encrypt")]
    public void Refresh_NoOriginalIssue_Fails(string secMode)
    {
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity([
                new Claim(ClaimTypes.NameIdentifier, "name")
            ]),
            Expires = DateTime.UtcNow + TimeSpan.FromHours(1),
            EncryptingCredentials = 
                new EncryptingCredentials(new SymmetricSecurityKey(_key),
                    Microsoft.IdentityModel.JsonWebTokens.JwtConstants.DirectKeyUseAlg,
                    SecurityAlgorithms.Aes128CbcHmacSha256)
        };

        var tokenHandler = new SystemJwt.JwtSecurityTokenHandler();
        var token = tokenHandler.CreateToken(tokenDescriptor);
        var jwt = tokenHandler.WriteToken(token);

        var opts = GetOptions(secMode);
        var val = new JwtValidator(opts);
        var gen = new JwtGenerator(opts, val);

        opts.Audience = null;
        opts.Issuer = null;
        opts.TokenLifetime = TimeSpan.FromHours(1);
        opts.MaxTokenLifetime = TimeSpan.FromHours(2);

        Assert.Throws<InvalidOperationException>(() =>
            gen.Refresh(jwt));
    }
}
