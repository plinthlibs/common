using NUnit.Framework;
using Plinth.Security.Jwt;
using System.Security.Claims;

namespace Tests.Plinth.Security.Jwt;

[TestFixture]
public class ClaimsPrincipalExtensionsTests
{
    private ClaimsPrincipal GetEmptyPrincipal()
    {
        var identity = new ClaimsIdentity((IEnumerable<Claim>)[]);
        return new ClaimsPrincipal(identity);
    }

    private ClaimsPrincipal GetPrincipal(params Claim[] claims)
    {
        var identity = new ClaimsIdentity(claims);
        return new ClaimsPrincipal(identity);
    }

    [Test]
    public void JwtUserName_Missing_Fails()
    {
        Assert.Throws<InvalidDataException>(() => GetEmptyPrincipal().JwtUserName());
    }

    [Test]
    public void JwtUserName_NoIdentity_Fails()
    {
        Assert.Throws<InvalidDataException>(() => new ClaimsPrincipal().JwtUserName());
    }

    [Test]
    public void JwtUserName_WithUniqueName_Succeeds()
    {
        var cp = GetPrincipal(new Claim(ClaimTypes.Name, "un55"));
        Assert.That(cp.JwtUserName(), Is.EqualTo("un55"));
        Assert.That(cp.Identity?.Name, Is.EqualTo("un55"));
    }

    [Test]
    public void JwtUserId_Missing_Fails()
    {
        Assert.Throws<InvalidDataException>(() => GetEmptyPrincipal().JwtUserId());
    }

    [Test]
    public void JwtUserId_NoIdentity_Fails()
    {
        Assert.Throws<InvalidDataException>(() => new ClaimsPrincipal().JwtUserId());
    }

    [Test]
    public void JwtUserId_WithNameIdentifier_Succeeds()
    {
        var cp = GetPrincipal(new Claim(ClaimTypes.NameIdentifier, "29054dec-f039-4184-8392-6c318adf6714"));
        Assert.That(cp.JwtUserId().ToString(), Is.EqualTo("29054dec-f039-4184-8392-6c318adf6714"));
    }

    [Test]
    public void JwtSessionGuid_Missing_Null()
    {
        Assert.That(GetEmptyPrincipal().JwtSessionGuid(), Is.Null);
    }

    [Test]
    public void JwtSessionGuid_NoIdentity_Null()
    {
        Assert.That(new ClaimsPrincipal().JwtSessionGuid(), Is.Null);
    }

    [Test]
    public void JwtSessionGuid_InvalidSessionGuid_Succeeds()
    {
        var cp = GetPrincipal(new Claim("sid", "not-a-guid"));
        Assert.That(cp.JwtSessionGuid(), Is.Null);
    }

    [Test]
    public void JwtSessionGuid_WithSessionGuid_Succeeds()
    {
        var cp = GetPrincipal(new Claim("sid", "c9d0d4b2-fd8a-43ae-98db-0ea019f6820d"));
        Assert.That(cp.JwtSessionGuid(), Is.EqualTo(Guid.Parse("c9d0d4b2-fd8a-43ae-98db-0ea019f6820d")));
    }

    [Test]
    public void JwtExpiration_Missing_Fails()
    {
        Assert.Throws<InvalidDataException>(() => GetEmptyPrincipal().JwtExpiration());
    }

    [Test]
    public void JwtExpiration_NoIdentity_Fails()
    {
        Assert.Throws<InvalidDataException>(() => new ClaimsPrincipal().JwtExpiration());
    }

    [Test]
    public void JwtExpiration_InvalidExpiration_Fails()
    {
        var cp = GetPrincipal(new Claim("exp", "not-a-long"));
        Assert.Throws<InvalidDataException>(() => cp.JwtExpiration());
    }

    [Test]
    public void JwtExpiration_WithExpiration_Succeeds()
    {
        var now = DateTimeOffset.FromUnixTimeSeconds(DateTimeOffset.UtcNow.ToUnixTimeSeconds());
        var cp = GetPrincipal(new Claim("exp", now.ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64));
        Assert.That(cp.JwtExpiration(), Is.EqualTo(now));
    }

    [Test]
    public void JwtOriginalIssue_Missing_Null()
    {
        Assert.That(GetEmptyPrincipal().JwtOriginalIssue(), Is.Null);
    }

    [Test]
    public void JwtOriginalIssue_NoIdentity_Null()
    {
        Assert.That(new ClaimsPrincipal().JwtOriginalIssue(), Is.Null);
    }

    [Test]
    public void JwtOriginalIssue_InvalidOriginalIssue_Succeeds()
    {
        var cp = GetPrincipal(new Claim("ori", "not-a-long"));
        Assert.That(cp.JwtOriginalIssue(), Is.Null);
    }

    [Test]
    public void JwtOriginalIssue_WithOriginalIssue_Succeeds()
    {
        var now = DateTimeOffset.FromUnixTimeSeconds(DateTimeOffset.UtcNow.ToUnixTimeSeconds());
        var cp = GetPrincipal(new Claim("ori", now.ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64));
        Assert.That(cp.JwtOriginalIssue(), Is.EqualTo(now));
    }

    [Test]
    public void JwtRoles_Missing_Empty()
    {
        Assert.That(GetEmptyPrincipal().JwtRoles(), Is.Empty);
    }

    [Test]
    public void JwtRoles_NoIdentity_Empty()
    {
        Assert.That(new ClaimsPrincipal().JwtRoles(), Is.Empty);
    }

    [Test]
    public void JwtRoles_WithRole_Succeeds()
    {
        var cp = GetPrincipal(new Claim(ClaimTypes.Role, "admin"));
        Assert.That(cp.JwtRoles(), Is.EqualTo(new[] { "admin" }).AsCollection);
        Assert.That(cp.IsInRole("admin"));
        Assert.That(cp.IsInRole("user"), Is.False);
    }

    [Test]
    public void JwtRoles_WithRoles_Succeeds()
    {
        var cp = GetPrincipal(new Claim(ClaimTypes.Role, "admin"), new Claim(ClaimTypes.Role, "user"));
        Assert.That(cp.JwtRoles(), Is.EqualTo(new[] { "admin", "user" }).AsCollection);
        Assert.That(cp.IsInRole("admin"));
        Assert.That(cp.IsInRole("user"));
        Assert.That(cp.IsInRole("superAdmin"), Is.False);
    }
}
