// uncomment #define in DatabaseIntTest to run
using System.Data;
using Microsoft.Data.SqlClient;
using Plinth.Database.MSSql.Extensions;
using NUnit.Framework;
using Plinth.Database.Dapper.MSSql;

#if !NET9_0_OR_GREATER
using Convert = Plinth.Common.Utils.BackPort.PlinthConvert;
#endif

namespace Tests.Plinth.Database.MSSql.Integration;

[TestFixture]
#if !RUN_DB_MSSQL_INT_TESTS
[System.Diagnostics.CodeAnalysis.SuppressMessage("Structure", "NUnit1028:The non-test method is public", Justification = "<Pending>")]
#endif
partial class DatabaseIntTest
{
    class TestRecordDapper
    {
        public long ID { get; set; }
        public DateTime? DT { get; set; }
        public string? String { get; set; }
        public int? IntField { get; set; }
        public decimal? MoneyField { get; set; }
        public byte[]? BlobField { get; set; }
    }

#region ExecuteQueryProc


    [Test]
    public void Dapper_TestExecuteQueryProc()
    {
        IntegrationTestSetup.Exec(
            (c) =>
            {
                Assert.That(c.IsAsync(), Is.False);

                c.Dapper().ExecuteProc(
                    "usp_InsertTestRecord",
                    new { ID = 1, String = "s1", Int = 10 });

                c.Dapper().ExecuteProc(
                    "usp_InsertTestRecord",
                    new { ID = 2, String = "s2", Int = 10 });

                var obj = c.Dapper().ExecuteQueryProcOne<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 86 });
                Assert.That(obj, Is.Null);

                obj = c.Dapper().ExecuteQueryProcOne<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s1"));

                var sr = c.Dapper().ExecuteQueryProcOne<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = -86 });
                Assert.That(sr, Is.Null);

                sr = c.Dapper().ExecuteQueryProcOne<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 1 });
                Assert.That(sr, Is.Not.Null);
                Assert.That(sr?.Id, Is.EqualTo(1));
                Assert.That(sr?.String, Is.EqualTo("s1"));

                var d = c.Dapper().ExecuteQueryProcOne<dynamic>(
                    "usp_EchoBit",
                    new { Bit = true });
                Assert.That((bool)d?.Bit);

                d = c.Dapper().ExecuteQueryProcOne<dynamic>(
                    "usp_EchoBit",
                    new { Bit = true });
                Assert.That((bool)d?.Bit);

                var list = c.Dapper().ExecuteQueryProcList<TestRecord>(
                    "usp_GetTestRecordByInt",
                    new { Int = 10 })
                    .ToList();
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                // raw

                const string GetSql = "SELECT * FROM Test WHERE ID = @ID";
                Assert.That(c.Dapper().ExecuteRawQueryOne<TestRecord>(
                    GetSql,
                    new { ID = -86 }), Is.Null);

                sr = c.Dapper().ExecuteRawQueryOne<TestRecord>(
                    GetSql,
                    new { ID = 1 });
                Assert.That(sr?.String, Is.EqualTo("s1"));
                Assert.That(sr?.Id, Is.EqualTo(1));

                const string GetSqlByID = "SELECT * FROM Test WHERE IntField = @Int";
                list = c.Dapper().ExecuteRawQueryList<TestRecord>(
                    GetSqlByID,
                    new { Int = 10 })
                    .ToList();
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                Assert.That(c.Dapper().ExecuteRaw("UPDATE Test SET IntField = 10"), Is.EqualTo(2));
                Assert.That(c.Dapper().ExecuteRaw("UPDATE Test SET IntField = @NewInt", new { NewInt = 20 }), Is.EqualTo(2));
            });
    }

    [Test]
    public void Dapper_TestExecuteQueryProc_Default()
    {
        IntegrationTestSetup.Exec(
            (c) =>
            {
                Assert.That(c.IsAsync(), Is.False);

                c.Dapper().ExecuteProc(
                    "usp_InsertTestRecord",
                    new { Id = 1, String = "s1", Int = 10 });

                c.Dapper().ExecuteProc(
                    "usp_InsertTestRecord",
                    new { Id = 2, String = "s2", Int = 10 });

                Assert.That(c.Dapper().ExecuteQueryProcOne<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = -86 }), Is.Null);

                var d = c.Dapper().ExecuteQueryProcOne<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 1 });

                Assert.That(d?.String, Is.EqualTo("s1"));
            });
    }

    [Test]
    public async Task Dapper_TestExecuteQueryProcAsync()
    {
        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                Assert.That(c.IsAsync());

                await c.Dapper().ExecuteProcAsync(
                    "usp_InsertTestRecord",
                    new { ID = 1, String = "s1", Int = 10 });

                await c.Dapper().ExecuteProcAsync(
                    "usp_InsertTestRecord",
                    new { ID = 2, String = "s2", Int = 10 });

                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 86 });
                Assert.That(obj, Is.Null);

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s1"));

                var sr = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = -86 });
                Assert.That(sr, Is.Null);

                sr = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 1 });
                Assert.That(sr, Is.Not.Null);
                Assert.That(sr?.Id, Is.EqualTo(1));
                Assert.That(sr?.String, Is.EqualTo("s1"));

                var d = await c.Dapper().ExecuteQueryProcOneAsync<dynamic>(
                    "usp_EchoBit",
                    new { Bit = true });
                Assert.That((bool)d?.Bit);

                d = await c.Dapper().ExecuteQueryProcOneAsync<dynamic>(
                    "usp_EchoBit",
                    new { Bit = true });
                Assert.That((bool)d?.Bit);

                var list = (await c.Dapper().ExecuteQueryProcListAsync<TestRecord>(
                    "usp_GetTestRecordByInt",
                    new { Int = 10 }))
                    .ToList();
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                // raw

                const string GetSql = "SELECT * FROM Test WHERE ID = @ID";
                Assert.That(await c.Dapper().ExecuteRawQueryOneAsync<TestRecord>(
                    GetSql,
                    new { ID = -86 }), Is.Null);

                sr = await c.Dapper().ExecuteRawQueryOneAsync<TestRecord>(
                    GetSql,
                    new { ID = 1 });
                Assert.That(sr?.String, Is.EqualTo("s1"));
                Assert.That(sr?.Id, Is.EqualTo(1));

                const string GetSqlByID = "SELECT * FROM Test WHERE IntField = @Int";
                list = (await c.Dapper().ExecuteRawQueryListAsync<TestRecord>(
                    GetSqlByID,
                    new { Int = 10 }))
                    .ToList();
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                Assert.That(await c.Dapper().ExecuteRawAsync("UPDATE Test SET IntField = 10"), Is.EqualTo(2));
                Assert.That(await c.Dapper().ExecuteRawAsync("UPDATE Test SET IntField = @NewInt", new { NewInt = 20 }), Is.EqualTo(2));
            });
    }

    public enum Test2Type
    {
        Value1 = 2,
        ValueAnother = 7
    }

    class Test2
    {
        public Test2Type? EnumStr { get; set; }
        public Test2Type? EnumInt { get; set; }
    }

    [Test]
    public async Task Dapper_TestEnumsAsync()
    {
        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                await c.Dapper().ExecuteRawAsync("INSERT INTO Test2 (EnumStr, EnumInt) VALUES (@EnumStr, @EnumInt)",
                    new Test2 { EnumInt = Test2Type.Value1, EnumStr = Test2Type.Value1 });

                var r = await c.Dapper().ExecuteRawQueryOneAsync<Test2>("SELECT TOP 1 * FROM Test2");
                Assert.That(r?.EnumInt, Is.EqualTo(Test2Type.Value1));
                Assert.That(r?.EnumStr, Is.EqualTo(Test2Type.Value1));

                dynamic? r2 = await c.Dapper().ExecuteRawQueryOneAsync<dynamic>("SELECT TOP 1 * FROM Test2");
                Assert.That(r2?.EnumInt, Is.EqualTo(2));
                Assert.That(r2?.EnumStr, Is.EqualTo("2"));

                await c.Dapper().ExecuteRawAsync("TRUNCATE TABLE Test2");
                await c.Dapper().ExecuteRawAsync("INSERT INTO Test2 (EnumStr, EnumInt) VALUES (@EnumStr, @EnumInt)",
                    new { EnumInt = Test2Type.Value1, EnumStr = Test2Type.Value1.ToString() });

                r = await c.Dapper().ExecuteRawQueryOneAsync<Test2>("SELECT TOP 1 * FROM Test2");
                Assert.That(r?.EnumInt, Is.EqualTo(Test2Type.Value1));
                Assert.That(r?.EnumStr, Is.EqualTo(Test2Type.Value1));

                r2 = await c.Dapper().ExecuteRawQueryOneAsync<dynamic>("SELECT TOP 1 * FROM Test2");
                Assert.That(r2?.EnumInt, Is.EqualTo(2));
                Assert.That(r2?.EnumStr, Is.EqualTo("Value1"));
            });
    }

    [Test]
    public async Task Dapper_TestExecuteQueryProc_DefaultAsync()
    {
        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                Assert.That(c.IsAsync());

                await c.Dapper().ExecuteProcAsync(
                    "usp_InsertTestRecord",
                    new { Id = 1, String = "s1", Int = 10 });

                await c.Dapper().ExecuteProcAsync(
                    "usp_InsertTestRecord",
                    new { Id = 2, String = "s2", Int = 10 });

                Assert.That(await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = -86 }), Is.Null);

                var d = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 1 });

                Assert.That(d?.String, Is.EqualTo("s1"));
            });
    }
#endregion ExecuteQueryProc

#region ExecuteQueryProc Null Blob

    [Test]
    public void Dapper_TestExecuteQueryProc_NullBlob()
    {
        IntegrationTestSetup.Exec(
            (c) =>
            {
                Assert.That(c.IsAsync(), Is.False);

                c.Dapper().ExecuteProc(
                    "usp_InsertTestRecord",
                    new { Id = 1, String = "s1", Int = 10, Blob = Convert.FromHexString("aabbcc") });

                c.Dapper().ExecuteProc(
                    "usp_InsertTestRecord",
                    new { Id = 2, String = "s2", Int = 10 });

                var d = c.Dapper().ExecuteQueryProcOne<TestRecordDapper>(
                    "usp_GetTestRecordByID",
                    new { ID = 1 });

                Assert.That(Convert.ToHexStringLower(d?.BlobField!), Is.EqualTo("aabbcc"));

                d = c.Dapper().ExecuteQueryProcOne<TestRecordDapper>(
                    "usp_GetTestRecordByID",
                    new { ID = 2 });
                Assert.That(d!.BlobField, Is.Null);
            });
    }
#endregion

#region ExecuteQueryProc Multiple Result Sets

    [Test]
    public void Dapper_TestExecuteQueryProcMultipleResultSets()
    {
        static void process(IDapperMultiResultSet rs, int idx, int p)
        {
            var items = rs.GetList<TestRecordDapper>().ToList();
            Assert.That(items, Has.Count.EqualTo(3));
            for (int i = 0; i < 3; i++)
            {
                Assert.That(items[i].ID, Is.EqualTo(p * 10));
                Assert.That(items[i].String, Is.EqualTo($"s{p}"));
                Assert.That(items[i].IntField, Is.EqualTo(idx * 10));
                p++;
            }
        };

        IntegrationTestSetup.Exec(
            (c) =>
            {
                c.Dapper().ExecuteProc( "usp_InsertTestRecord", new { ID = 10, String = "s1", Int = 10 });
                c.Dapper().ExecuteProc( "usp_InsertTestRecord", new { ID = 20, String = "s2", Int = 10 });
                c.Dapper().ExecuteProc( "usp_InsertTestRecord", new { ID = 30, String = "s3", Int = 10 });

                c.Dapper().ExecuteProc( "usp_InsertTestRecord", new { ID = 40, String = "s4", Int = 20 });
                c.Dapper().ExecuteProc( "usp_InsertTestRecord", new { ID = 50, String = "s5", Int = 20 });
                c.Dapper().ExecuteProc( "usp_InsertTestRecord", new { ID = 60, String = "s6", Int = 20 });

                c.Dapper().ExecuteProc( "usp_InsertTestRecord", new { ID = 70, String = "s7", Int = 30 });
                c.Dapper().ExecuteProc( "usp_InsertTestRecord", new { ID = 80, String = "s8", Int = 30 });
                c.Dapper().ExecuteProc( "usp_InsertTestRecord", new { ID = 90, String = "s9", Int = 30 });

                c.Dapper().ExecuteQueryProcMultiResultSet(
                    "usp_GetTestRecordsByInts",
                    (mrs) =>
                    {
                        process(mrs, 1, 1);

                        process(mrs, 2, 4);

                        process(mrs, 3, 7);
                    },
                    new { Int1 = 10, Int2 = 20, Int3 = 30 });

                c.Dapper().ExecuteQueryProcMultiResultSet(
                    "usp_GetTestRecordsByInts",
                    (mrs) =>
                    {
                        var _ = mrs.GetList<TestRecordDapper>().Take(2).ToList();

                        _ = mrs.GetList<TestRecordDapper>().Take(1).ToList();

                        _ = mrs.GetList<TestRecordDapper>().Take(3).ToList();
                    },
                    new { Int1 = 10, Int2 = 20, Int3 = 30 });
            });
    }

    [Test]
    public async Task Dapper_TestExecuteQueryProcMultipleResultSetsAsync()
    {
        static async Task process(IDapperMultiResultSetAsync rs, int idx, int p)
        {
            var items = (await rs.GetListAsync<TestRecordDapper>()).ToList();
            Assert.That(items, Has.Count.EqualTo(3));
            for (int i = 0; i < 3; i++)
            {
                Assert.That(items[i].ID, Is.EqualTo(p * 10));
                Assert.That(items[i].String, Is.EqualTo($"s{p}"));
                Assert.That(items[i].IntField, Is.EqualTo(idx * 10));
                p++;
            }
        };

        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 10, String = "s1", Int = 10 });
                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 20, String = "s2", Int = 10 });
                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 30, String = "s3", Int = 10 });

                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 40, String = "s4", Int = 20 });
                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 50, String = "s5", Int = 20 });
                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 60, String = "s6", Int = 20 });

                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 70, String = "s7", Int = 30 });
                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 80, String = "s8", Int = 30 });
                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 90, String = "s9", Int = 30 });

                await c.Dapper().ExecuteQueryProcMultiResultSetAsync(
                    "usp_GetTestRecordsByInts",
                    async (mrs) =>
                    {
                        await process(mrs, 1, 1);

                        await process(mrs, 2, 4);

                        await process(mrs, 3, 7);
                    },
                    new { Int1 = 10, Int2 = 20, Int3 = 30 });

                await c.Dapper().ExecuteQueryProcMultiResultSetAsync(
                    "usp_GetTestRecordsByInts",
                    async (mrs) =>
                    {
                        var _ = (await mrs.GetListAsync<TestRecordDapper>()).Take(2).ToList();

                        _ = (await mrs.GetListAsync<TestRecordDapper>()).Take(1).ToList();

                        _ = (await mrs.GetListAsync<TestRecordDapper>()).Take(3).ToList();
                    },
                    new { Int1 = 10, Int2 = 20, Int3 = 30 });
            });
    }

#endregion ExecuteQueryProc Multiple Result Sets

#region ExecuteRaw Multiple Result Sets

    [Test]
    public void Dapper_TestExecuteRawMultipleResultSets()
    {
        static void process(IDapperMultiResultSet rs, int idx, int p)
        {
            var items = rs.GetList<TestRecordDapper>().ToList();
            Assert.That(items, Has.Count.EqualTo(3));
            for (int i = 0; i < 3; i++)
            {
                Assert.That(items[i].ID, Is.EqualTo(p * 10));
                Assert.That(items[i].String, Is.EqualTo($"s{p}"));
                Assert.That(items[i].IntField, Is.EqualTo(idx * 10));
                p++;
            }
        };

        IntegrationTestSetup.Exec(
            (c) =>
            {
                c.Dapper().ExecuteProc( "usp_InsertTestRecord", new { ID = 10, String = "s1", Int = 10 });
                c.Dapper().ExecuteProc( "usp_InsertTestRecord", new { ID = 20, String = "s2", Int = 10 });
                c.Dapper().ExecuteProc( "usp_InsertTestRecord", new { ID = 30, String = "s3", Int = 10 });

                c.Dapper().ExecuteProc( "usp_InsertTestRecord", new { ID = 40, String = "s4", Int = 20 });
                c.Dapper().ExecuteProc( "usp_InsertTestRecord", new { ID = 50, String = "s5", Int = 20 });
                c.Dapper().ExecuteProc( "usp_InsertTestRecord", new { ID = 60, String = "s6", Int = 20 });

                c.Dapper().ExecuteProc( "usp_InsertTestRecord", new { ID = 70, String = "s7", Int = 30 });
                c.Dapper().ExecuteProc( "usp_InsertTestRecord", new { ID = 80, String = "s8", Int = 30 });
                c.Dapper().ExecuteProc( "usp_InsertTestRecord", new { ID = 90, String = "s9", Int = 30 });

                c.Dapper().ExecuteRawQueryMultiResultSet(
                    SqlScripts.Dapper_Usp_GetTestRecordsByInts, 
                    (mrs) =>
                    {
                        process(mrs, 1, 1);

                        process(mrs, 2, 4);

                        process(mrs, 3, 7);
                    },
                    new { Int1 = 10, Int2 = 20, Int3 = 30 });
            });
    }

    [Test]
    public async Task Dapper_TestExecuteRawMultipleResultSetsAsync()
    {
        static async Task process(IDapperMultiResultSetAsync rs, int idx, int p)
        {
            var items = (await rs.GetListAsync<TestRecordDapper>()).ToList();
            Assert.That(items, Has.Count.EqualTo(3));
            for (int i = 0; i < 3; i++)
            {
                Assert.That(items[i].ID, Is.EqualTo(p * 10));
                Assert.That(items[i].String, Is.EqualTo($"s{p}"));
                Assert.That(items[i].IntField, Is.EqualTo(idx * 10));
                p++;
            }
        };

        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 10, String = "s1", Int = 10 });
                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 20, String = "s2", Int = 10 });
                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 30, String = "s3", Int = 10 });

                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 40, String = "s4", Int = 20 });
                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 50, String = "s5", Int = 20 });
                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 60, String = "s6", Int = 20 });

                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 70, String = "s7", Int = 30 });
                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 80, String = "s8", Int = 30 });
                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 90, String = "s9", Int = 30 });

                await c.Dapper().ExecuteRawQueryMultiResultSetAsync(
                    SqlScripts.Dapper_Usp_GetTestRecordsByInts, 
                    async (mrs) =>
                    {
                        await process(mrs, 1, 1);

                        await process(mrs, 2, 4);

                        await process(mrs, 3, 7);
                    },
                    new { Int1 = 10, Int2 = 20, Int3 = 30 });
            });
    }

#endregion ExecuteQueryProc Multiple Result Sets

#region Structured Type

    [Test]
    public void Dapper_TestStructuredType()
    {
        IntegrationTestSetup.Exec(
            (c) =>
            {
                var list = new List<string> { "abc", "123", "def" };
                var ret = c.Dapper()
                    .ExecuteQueryProcList<string>("usp_EchoNVARCHARList",
                    new { Table = list.ToNVarCharList() }).ToList();
                Assert.That(ret, Is.EquivalentTo(list));

                var testList = new List<TestRecord>
                {
                    new TestRecord { Id = 5, String = "hello", Int = 77 },
                    new TestRecord { Id = 6, String = "there", Int = 78 },
                    new TestRecord { Id = 7, String = "table", Int = 79 }
                };

                var table = new DataTable();
                table.Columns.Add("GUID", typeof(Guid));
                table.Columns.Add("ID", typeof(long));
                table.Columns.Add("DT", typeof(DateTime));
                table.Columns.Add("String", typeof(string));
                table.Columns.Add("IntField", typeof(int));
                table.Columns.Add("MoneyField", typeof(decimal));
                foreach (var t in testList)
                {
                    var r = table.Rows.Add();
                    r["GUID"] = Guid.NewGuid();
                    r["ID"] = t.Id;
                    r["String"] = t.String;
                    r["IntField"] = t.Int;
                }

                var testRet = c.Dapper().ExecuteQueryProcList<TestRecordDapper>("usp_EchoTableType", 
                    new { Table = table }).ToList();
                foreach (var tl in testList)
                    Assert.That(testRet.Any(t => t.ID == tl.Id && t.String == tl.String && t.IntField == tl.Int));
            });
    }
    #endregion

#region without txn
    [Test]
    public async Task Dapper_WithoutTxn_ReadOnly()
    {
        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                Assert.That(c.IsAsync());

                await c.Dapper().ExecuteProcAsync(
                    "usp_InsertTestRecord",
                    new { ID = 1, String = "s1", Int = 10 });

                await c.Dapper().ExecuteProcAsync(
                    "usp_InsertTestRecord",
                    new { ID = 2, String = "s2", Int = 10 });
            }
        );

        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 86 });
                Assert.That(obj, Is.Null);
            });

        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 86 });
                Assert.That(obj, Is.Null);

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s1"));

                var sr = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = -86 });
                Assert.That(sr, Is.Null);

                sr = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 1 });
                Assert.That(sr, Is.Not.Null);
                Assert.That(sr?.Id, Is.EqualTo(1));
                Assert.That(sr?.String, Is.EqualTo("s1"));

                var d = await c.Dapper().ExecuteQueryProcOneAsync<dynamic>(
                    "usp_EchoBit",
                    new { Bit = true });
                Assert.That((bool)d?.Bit);

                d = await c.Dapper().ExecuteQueryProcOneAsync<dynamic>(
                    "usp_EchoBit",
                    new { Bit = true });
                Assert.That((bool)d?.Bit);

                var list = (await c.Dapper().ExecuteQueryProcListAsync<TestRecord>(
                    "usp_GetTestRecordByInt",
                    new { Int = 10 }))
                    .ToList();
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                // raw

                const string GetSql = "SELECT * FROM Test WHERE ID = @ID";
                Assert.That(await c.Dapper().ExecuteRawQueryOneAsync<TestRecord>(
                    GetSql,
                    new { ID = -86 }), Is.Null);

                sr = await c.Dapper().ExecuteRawQueryOneAsync<TestRecord>(
                    GetSql,
                    new { ID = 1 });
                Assert.That(sr?.String, Is.EqualTo("s1"));
                Assert.That(sr?.Id, Is.EqualTo(1));

                const string GetSqlByID = "SELECT * FROM Test WHERE IntField = @Int";
                list = (await c.Dapper().ExecuteRawQueryListAsync<TestRecord>(
                    GetSqlByID,
                    new { Int = 10 }))
                    .ToList();
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));
            });
    }

    [Test]
    public async Task Dapper_WithoutTxn_OneWrite_OK()
    {
        // single op, 1 write
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 1, String = "s1", Int = 10 });
            });

        // read, then 1 write
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s1"));

                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 2, String = "s2", Int = 10 });
            });

        // 1 write, then read
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 3, String = "s3", Int = 10 });

                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 2 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s2"));
            });

        // 1 write, then multiple read
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 4, String = "s4", Int = 10 });

                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 3 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s3"));

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 4 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s4"));
            });

        // read, 1 write, then multiple read
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s1"));

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 2 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s2"));

                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 5, String = "s5", Int = 10 });

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 3 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s3"));

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 5 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s5"));
            });
    }

    [Test]
    public async Task Dapper_WithoutTxn_OneWrite_NoRollback()
    {
        // confirm row does not exist
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 1 });
                Assert.That(obj, Is.Null);
            });

        // 1 write, fail
        Assert.ThrowsAsync<NotFiniteNumberException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 1, String = "s1", Int = 10 });

                    throw new NotFiniteNumberException();
                });
        });

        // confirm write succeeded
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s1"));
            });
    }

    [Test]
    public async Task Dapper_WithoutTxn_TwoWrites_NoRollbackAndFail()
    {
        // confirm rows do not exist
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 1 });
                Assert.That(obj, Is.Null);

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 2 });
                Assert.That(obj, Is.Null);
            });

        // 2 write, fail
        Assert.ThrowsAsync<NotSupportedException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 1, String = "s1", Int = 10 });

                    await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 2, String = "s2", Int = 10 });
                });
        });

        // confirm first write succeeded, second didn't happen
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s1"));

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 2 });
                Assert.That(obj, Is.Null);
            });
    }

    [Test]
    public async Task Dapper_WithoutTxn_TwoWritesMixed_NoRollbackAndFail()
    {
        // confirm rows do not exist
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 1 });
                Assert.That(obj, Is.Null);

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 2 });
                Assert.That(obj, Is.Null);
            });

        // 2 write [dapper then native], fail
        Assert.ThrowsAsync<NotSupportedException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 1, String = "s1", Int = 10 });

                    await c.ExecuteProcAsync("usp_InsertTestRecord", new SqlParameter("@ID", 2), new SqlParameter("@String", "s2"), new SqlParameter("@Int", 10));
                });
        });

        // confirm first write succeeded, second didn't happen
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s1"));

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 2 });
                Assert.That(obj, Is.Null);
            });

        // 2 write [native then dapper], fail
        Assert.ThrowsAsync<NotSupportedException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    await c.ExecuteProcAsync("usp_InsertTestRecord", new SqlParameter("@ID", 3), new SqlParameter("@String", "s3"), new SqlParameter("@Int", 10));

                    await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 4, String = "s4", Int = 10 });
                });
        });

        // confirm first write succeeded, second didn't happen
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 3 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s3"));

                obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 4 });
                Assert.That(obj, Is.Null);
            });
    }

    [Test]
    public async Task Dapper_WithoutTxn_OneRead_NoRollback()
    {
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 1, String = "s1", Int = 10 });
            });

        // confirm write succeeded, but fail
        Assert.ThrowsAsync<NotFiniteNumberException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                        "usp_GetTestRecordByID",
                        new { ID = 1 });
                    Assert.That(obj, Is.Not.Null);
                    Assert.That(obj?.String, Is.EqualTo("s1"));

                    throw new NotFiniteNumberException();
                });
        });
    }

    [Test]
    public async Task Dapper_WithoutTxn_OneRead_TransientRetry()
    {
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 1, String = "s1", Int = 10 });
            });

        int retries = 0;
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s1"));

                retries++;
                if (retries < 3)
                    throw new TimeoutException("should retry");
            });

        Assert.That(retries, Is.EqualTo(3));
    }

    [Test]
    public async Task Dapper_WithoutTxn_OneWrite_NoTransientRetry()
    {
        int retries = 0;
        Assert.ThrowsAsync<TimeoutException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    await c.Dapper().ExecuteProcAsync( "usp_InsertTestRecord", new { ID = 1, String = "s1", Int = 10 });

                    retries++;
                    if (retries < 3)
                        throw new TimeoutException("should retry");
                });
        });

        Assert.That(retries, Is.EqualTo(1));

        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                var obj = await c.Dapper().ExecuteQueryProcOneAsync<TestRecord>(
                    "usp_GetTestRecordByID",
                    new { ID = 1 });
                Assert.That(obj, Is.Not.Null);
                Assert.That(obj?.String, Is.EqualTo("s1"));
            });
    }
    #endregion

    [Test]
    public async Task Dapper_TestCancel()
    {
        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                Assert.That(c.IsAsync());

                var cts = new CancellationTokenSource();
                cts.CancelAfter(TimeSpan.FromMilliseconds(200));
                try
                {
                    await c.Dapper().ExecuteRawAsync("WAITFOR DELAY '00:02'", param: null, cts.Token);
                    Assert.Fail();
                }
                catch (SqlException se)
                {
                    Assert.That(se.Message, Does.Contain("Operation cancelled by user."), se.Message);
                }

                cts = new CancellationTokenSource();
                cts.CancelAfter(TimeSpan.FromMilliseconds(200));
                try
                {
                    await c.Dapper().ExecuteRawQueryOneAsync<dynamic>("WAITFOR DELAY '00:02'; SELECT 1 AS a, 2 AS b, 3 AS c", param: null, cts.Token);
                    Assert.Fail();
                }
                catch (SqlException se)
                {
                    Assert.That(se.Message, Does.Contain("Operation cancelled by user."), se.Message);
                }
            });
    }

    /*
#region stream

    [Test]
    public void TestStream_Read()
    {
        Stream stream = null;
        var bigString = string.Join("", Enumerable.Repeat("a2", 1_000_000));

        IntegrationTestSetup.Exec(
            (c) =>
            {
                Assert.IsFalse(c.IsAsync());

                c.ExecuteProc(
                    "usp_InsertTestRecord",
                    new SqlParameter("@ID", 1),
                    new SqlParameter("@String", "s1"),
                    new SqlParameter("@Int", 10),
                    new SqlParameter("@Blob", HexUtil.ToBytes(bigString)));

                stream = c.ExecuteQueryProcOne(
                    "usp_GetTestRecordByID",
                    (r) => r.GetStream("BlobField"),
                    new SqlParameter("@ID", 1)).Value;

            });

        var ms = new MemoryStream();
        stream.CopyTo(ms);
        var data = HexUtil.ToHex(ms.ToArray());
        Assert.AreEqual(bigString, data);
    }

    [Test]
    public async Task TestStream_ReadAsync()
    {
        Stream stream = null;
        var bigString = string.Join("", Enumerable.Repeat("a2", 1_000_000));

        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                Assert.IsTrue(c.IsAsync());

                await c.ExecuteProcAsync(
                    "usp_InsertTestRecord",
                    new SqlParameter("@ID", 1),
                    new SqlParameter("@String", "s1"),
                    new SqlParameter("@Int", 10),
                    new SqlParameter("@Blob", HexUtil.ToBytes(bigString)));

                var res = await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => Task.FromResult(r.GetStream("BlobField")),
                    new SqlParameter("@ID", 1));
                stream = res.Value;
            });

        var ms = new MemoryStream();
        stream.CopyTo(ms);
        var data = HexUtil.ToHex(ms.ToArray());
        Assert.AreEqual(bigString, data);
    }

    [Test]
    public void TestStream_Write()
    {
        Stream stream = null;
        var bigBytes = new MemoryStream(1_000_000);
        for (int i = 0; i < bigBytes.Capacity; i++)
            bigBytes.WriteByte(0xa2);
        bigBytes.Position = 0;

        IntegrationTestSetup.Exec(
            (c) =>
            {
                Assert.IsFalse(c.IsAsync());

                c.ExecuteProc(
                    "usp_InsertTestRecord",
                    new SqlParameter("@ID", 1),
                    new SqlParameter("@String", "s1"),
                    new SqlParameter("@Int", 10),
                    new SqlParameter("@Blob", SqlDbType.VarBinary) { Value = bigBytes });

                stream = c.ExecuteQueryProcOne(
                    "usp_GetTestRecordByID",
                    (r) => r.GetStream("BlobField"),
                    new SqlParameter("@ID", 1)).Value;

                Assert.AreEqual(1_000_000, bigBytes.Position);
                bigBytes.Position = 0;

                var ms = new MemoryStream();
                stream.CopyTo(ms);
                CollectionAssert.AreEqual(bigBytes.ToArray(), ms.ToArray());
            });

        bigBytes.Dispose();
    }

    [Test]
    public async Task TestStream_WriteAsync()
    {
        Stream stream = null;
        var bigBytes = new MemoryStream(1_000_000);
        for (int i = 0; i < bigBytes.Capacity; i++)
            bigBytes.WriteByte(0xa2);
        bigBytes.Position = 0;

        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                Assert.IsTrue(c.IsAsync());

                await c.ExecuteProcAsync(
                    "usp_InsertTestRecord",
                    new SqlParameter("@ID", 1),
                    new SqlParameter("@String", "s1"),
                    new SqlParameter("@Int", 10),
                    new SqlParameter("@Blob", SqlDbType.VarBinary) { Value = bigBytes });

                var res = await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => Task.FromResult(r.GetStream("BlobField")),
                    new SqlParameter("@ID", 1));
                stream = res.Value;

                Assert.AreEqual(1_000_000, bigBytes.Position);
                bigBytes.Position = 0;

                var ms = new MemoryStream();
                stream.CopyTo(ms);
                CollectionAssert.AreEqual(bigBytes.ToArray(), ms.ToArray());
            });

        bigBytes.Dispose();
    }
#endregion
    */
}

file static class SqlScripts
{
    public const string Dapper_Usp_GetTestRecordsByInts = @"
    SELECT
        b.ID,
		b.DT,
		b.String,
		b.IntField,
		b.MoneyField,
		1 AS Num1
        FROM dbo.Test AS b
    WHERE b.IntField = @Int1
	ORDER BY b.ID ASC

    SELECT
        b.ID,
		b.DT,
		b.String,
		b.IntField,
		b.MoneyField,
		2 AS Num2
        FROM dbo.Test AS b
    WHERE b.IntField = @Int2
	ORDER BY b.ID ASC

    SELECT
        b.ID,
		b.DT,
		b.String,
		b.IntField,
		b.MoneyField,
		3 AS Num3
        FROM dbo.Test AS b
    WHERE b.IntField = @Int3
	ORDER BY b.ID ASC";

}
