// uncomment to run
//#define RUN_DB_MSSQL_INT_TESTS
#if RUNNING_IN_CI
#define RUN_DB_MSSQL_INT_TESTS
#endif
using System.Data;
using Microsoft.Data.SqlClient;
using Plinth.Database.MSSql;
using Plinth.Database.MSSql.Extensions;
using Plinth.Serialization;
using NUnit.Framework;

#if !NET9_0_OR_GREATER
using Convert = Plinth.Common.Utils.BackPort.PlinthConvert;
#endif

namespace Tests.Plinth.Database.MSSql.Integration;

#if !RUN_DB_MSSQL_INT_TESTS
[AttributeUsage(AttributeTargets.Method)]
class TestCaseAttribute : Attribute { } // disable tests
[AttributeUsage(AttributeTargets.Method)]
class TestAttribute : Attribute { } // disable tests
#endif

#if !RUN_DB_MSSQL_INT_TESTS
[System.Diagnostics.CodeAnalysis.SuppressMessage("Structure", "NUnit1028:The non-test method is public", Justification = "<Pending>")]
#endif
[TestFixture]
partial class DatabaseIntTest
{
    [OneTimeSetUp]
    public async Task SetUp()
    {
        await IntegrationTestSetup.StartUpDatabase();
    }

    [OneTimeTearDown]
    public async Task TearDown()
    {
        await IntegrationTestSetup.CleanUpDatabase();
    }

    [SetUp]
    public void TestSetUp()
    {
        IntegrationTestSetup.SetUpTest();
    }

    [TearDown]
    public void TestTearDown()
    {
        IntegrationTestSetup.CleanUpTest();
    }

    class TestRecord
    {
        public long Id { get; set; }
        public DateTime? Date { get; set; }
        public string? String { get; set; }
        public int? Int { get; set; }
        public decimal? Money { get; set; }
        public byte[]? Blob { get; set; }

        public Dictionary<string, object>? Others { get; set; }
    }

    private const string Usp_GetTestRecordsByInts = @"
    SELECT
        b.ID,
		b.DT,
		b.String,
		b.IntField,
		b.MoneyField,
		1 AS Num1
        FROM dbo.Test AS b
    WHERE b.IntField = @Int1
	ORDER BY b.ID ASC

    SELECT
        b.ID,
		b.DT,
		b.String,
		b.IntField,
		b.MoneyField,
		2 AS Num2
        FROM dbo.Test AS b
    WHERE b.IntField = @Int2
	ORDER BY b.ID ASC

    SELECT
        b.ID,
		b.DT,
		b.String,
		b.IntField,
		b.MoneyField,
		3 AS Num3
        FROM dbo.Test AS b
    WHERE b.IntField = @Int3
	ORDER BY b.ID ASC";

#region DateTime2

    [Test]
    public void TestDateTime2()
    {
        static Tuple<DateTime, DateTime> GetDateTime(IRawSqlConnection c, int id) =>
             c.ExecuteQueryProcOne("usp_GetDateTimeByID", r => Tuple.Create(r.GetDateTime("DT1"), r.GetDateTime("DT2")), new SqlParameter("@ID", id)).Value!;

        static Tuple<DateTime, DateTime> GetDateTimeRaw(IRawSqlConnection c, int id) =>
             c.ExecuteRawQueryOne("SELECT DT1, DT2 FROM DateTime2Test WHERE ID = @ID", r => Tuple.Create(r.GetDateTime("DT1"), r.GetDateTime("DT2")), new SqlParameter("@ID", id)).Value!;

        IntegrationTestSetup.Exec(
            (c) =>
            {
                var dt = new DateTime(2017, 5, 4, 8, 57, 23, 112, DateTimeKind.Utc);
                var dt2 = new DateTime(2017, 2, 7, 16, 23, 36, 812, DateTimeKind.Utc);
                Tuple<DateTime, DateTime> r;

                // insert without type spec
                c.ExecuteProc(
                    "usp_InsertDateTime",
                    new SqlParameter("@ID", 1),
                    new SqlParameter("@DT1", dt),
                    new SqlParameter("@DT2", dt));
                r = GetDateTime(c, 1);
                Assert.That(r.Item1.Millisecond, Is.EqualTo(113));
                //Assert.AreEqual(113, r.Item2.Millisecond); // fixed by auto conversion to datetime2
                Assert.That(r.Item2.Millisecond, Is.EqualTo(112));

                // insert with type spec
                c.ExecuteProc(
                    "usp_InsertDateTime",
                    new SqlParameter("@ID", 2),
                    new SqlParameter("@DT1", dt),
                    new SqlParameter("@DT2", SqlDbType.DateTime2) { Value = dt });
                r = GetDateTime(c, 2);
                Assert.That(r.Item1.Millisecond, Is.EqualTo(113));
                Assert.That(r.Item2.Millisecond, Is.EqualTo(112));

                // insert both with type spec
                c.ExecuteProc(
                    "usp_InsertDateTime",
                    new SqlParameter("@ID", 3),
                    new SqlParameter("@DT1", SqlDbType.DateTime2) { Value = dt },
                    new SqlParameter("@DT2", SqlDbType.DateTime2) { Value = dt });
                r = GetDateTime(c, 3);
                Assert.That(r.Item1.Millisecond, Is.EqualTo(113));
                Assert.That(r.Item2.Millisecond, Is.EqualTo(112));

                for (int i = 4; i <= 6; i++)
                {
                    // insert without type spec
                    c.ExecuteProc(
                        "usp_InsertDateTime",
                        new SqlParameter("@ID", i),
                        new SqlParameter("@DT1", dt),
                        new SqlParameter("@DT2", dt));
                }

                // update without type spec
                c.ExecuteProc(
                    "usp_UpdateDateTime",
                        new SqlParameter("@ID", 4),
                        new SqlParameter("@DT1", dt2),
                        new SqlParameter("@DT2", dt2));
                r = GetDateTime(c, 4);
                Assert.That(r.Item1.Millisecond, Is.EqualTo(813));
                //Assert.AreEqual(113, r.Item2.Millisecond); // fixed by auto conversion to datetime2
                Assert.That(r.Item2.Millisecond, Is.EqualTo(812));

                // update with type spec
                c.ExecuteProc(
                    "usp_UpdateDateTime",
                    new SqlParameter("@ID", 5),
                    new SqlParameter("@DT1", dt2),
                    new SqlParameter("@DT2", SqlDbType.DateTime2) { Value = dt2 });
                r = GetDateTime(c, 5);
                Assert.That(r.Item1.Millisecond, Is.EqualTo(813));
                Assert.That(r.Item2.Millisecond, Is.EqualTo(812));

                // update both with type spec
                c.ExecuteProc(
                    "usp_UpdateDateTime",
                    new SqlParameter("@ID", 6),
                    new SqlParameter("@DT1", SqlDbType.DateTime2) { Value = dt2 },
                    new SqlParameter("@DT2", SqlDbType.DateTime2) { Value = dt2 });
                r = GetDateTime(c, 6);
                Assert.That(r.Item1.Millisecond, Is.EqualTo(813));
                Assert.That(r.Item2.Millisecond, Is.EqualTo(812));

                // RAW
                c.ExecuteRaw("INSERT INTO DateTime2Test (ID, DT1, DT2) VALUES (@ID, @DT1, @DT2)",
                    new SqlParameter("@ID", 7),
                    new SqlParameter("@DT1", dt), // SqlDbType.DateTime2) { Value = dt2 },
                    new SqlParameter("@DT2", dt)); // SqlDbType.DateTime2) { Value = dt2 });
                r = GetDateTime(c, 7);
                Assert.That(r.Item1.Millisecond, Is.EqualTo(113));
                Assert.That(r.Item2.Millisecond, Is.EqualTo(112));

                c.ExecuteRaw("UPDATE DateTime2Test SET DT1=@DT1, DT2=@DT2 WHERE ID = @ID",
                    new SqlParameter("@ID", 7),
                    new SqlParameter("@DT1", dt2), // SqlDbType.DateTime2) { Value = dt2 },
                    new SqlParameter("@DT2", dt2)); // SqlDbType.DateTime2) { Value = dt2 });
                r = GetDateTime(c, 7);
                Assert.That(r.Item1.Millisecond, Is.EqualTo(813));
                Assert.That(r.Item2.Millisecond, Is.EqualTo(812));

                r = GetDateTimeRaw(c, 7);
                Assert.That(r.Item1.Millisecond, Is.EqualTo(813));
                Assert.That(r.Item2.Millisecond, Is.EqualTo(812));

                var sv = c.ExecuteRawQueryOne("SELECT DT1 FROM DateTime2Test WHERE DT1 <= @DT",
                    rs => rs.GetDateTime("DT1"),
                    new SqlParameter("@DT", dt2)); // SqlDbType.DateTime2) { Value = dt2 });
                Assert.That(sv.RowReturned, Is.False);

                sv = c.ExecuteRawQueryOne("SELECT DT2 FROM DateTime2Test WHERE DT2 <= @DT",
                    rs => rs.GetDateTime("DT2"),
                    new SqlParameter("@DT", dt2)); // SqlDbType.DateTime2) { Value = dt2 });
                Assert.That(sv.RowReturned);
                Assert.That(sv.Value.Millisecond, Is.EqualTo(812));
            });
    }

#endregion DateTime2

#region Rollback Action

    [Test]
    public void TestRollbackAction_Sync_1call()
    {
        bool called = false;
        Assert.Throws<Exception>(() => IntegrationTestSetup.Exec(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddRollbackAction("f", () => { called = true; File.Delete(fn); });

                Assert.That(File.Exists(fn));

                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public void TestRollbackAction_Sync_2calls()
    {
        bool first_called = false;
        bool first_called_second = false;
        bool second_called = false;
        bool second_called_first = false;
        Assert.Throws<Exception>(() => IntegrationTestSetup.Exec(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddRollbackAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2.tmp")), Is.False);
                    first_called = true;
                    if (second_called)
                        first_called_second = true;
                    File.Delete(fn);
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddRollbackAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2.tmp")));
                    second_called = true;
                    if (!first_called)
                        second_called_first = true;
                    File.Delete(fn2);
                });

                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_second);
        Assert.That(second_called_first);
    }

    [Test]
    public void TestRollbackAction_Sync_2calls_throws()
    {
        bool first_called = false;
        bool first_called_second = false;
        bool second_called = false;
        bool second_called_first = false;
        Assert.Throws<Exception>(() => IntegrationTestSetup.Exec(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddRollbackAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws.tmp")), Is.False);
                    first_called = true;
                    if (second_called)
                        first_called_second = true;
                    File.Delete(fn);

                    throw new IOException("failed!");
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddRollbackAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws.tmp")));
                    second_called = true;
                    if (!first_called)
                        second_called_first = true;
                    File.Delete(fn2);

                    throw new IOException("failed!");
                });

                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_second);
        Assert.That(second_called_first);
    }

    [Test]
    public void TestRollbackAction_Sync_2calls_1stThrow()
    {
        bool first_called = false;
        bool first_called_second = false;
        bool second_called = false;
        bool second_called_first = false;
        Assert.Throws<Exception>(() => IntegrationTestSetup.Exec(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws-1stthrow.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddRollbackAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws-1stthrow.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws-1stthrow.tmp")));
                    first_called = true;
                    if (second_called)
                        first_called_second = true;
                    File.Delete(fn);

                    throw new IOException("failed!");
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws-1stthrow.tmp");

                File.WriteAllText(fn2, "this is a file2");
                if (fn2 != null)
                    throw new Exception("failure!");
                c.AddRollbackAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws-1stthrow.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws-1stthrow.tmp")));
                    second_called = true;
                    if (!first_called)
                        second_called_first = true;
                    File.Delete(fn2!);

                    throw new IOException("failed!");
                });
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws-1stthrow.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws-1stthrow.tmp")));
        Assert.That(first_called);
        Assert.That(second_called, Is.False);
        Assert.That(first_called_second, Is.False);
        Assert.That(second_called_first, Is.False);
    }

    [Test]
    public void TestRollbackAction_Async_1call()
    {
        bool called = false;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncRollbackAction("f", async () => { called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));

                await Task.Delay(10);
                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public void TestRollbackAction_Sync_AsyncAction()
    {
        bool called = false;
        Assert.Throws<Exception>(() => IntegrationTestSetup.Exec(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncRollbackAction("f", async () => { called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));

                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public void TestRollbackAction_Async_AsyncActionSyncCall()
    {
        bool called = false;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddRollbackAction("f", () => { called = true; File.Delete(fn); });

                Assert.That(File.Exists(fn));

                await Task.Delay(10);
                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public void TestRollbackAction_Async_AsyncActionAsyncCall_WrongMethod()
    {
        bool called = false;
        Assert.Throws<NotSupportedException>(() => IntegrationTestSetup.Exec(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddRollbackAction("f", async () => { called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));

                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")));
        Assert.That(called, Is.False);
    }

    [Test]
    public void TestRollbackAction_Async_2calls()
    {
        bool first_called = false;
        bool first_called_second = false;
        bool second_called = false;
        bool second_called_first = false;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncRollbackAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")), Is.False);
                    first_called = true;
                    if (second_called)
                        first_called_second = true;
                    File.Delete(fn);
                    await Task.Delay(10);
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddAsyncRollbackAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")));
                    second_called = true;
                    if (!first_called)
                        second_called_first = true;
                    File.Delete(fn2);
                    await Task.Delay(10);
                });

                await Task.Delay(10);
                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_second);
        Assert.That(second_called_first);
    }

    [Test]
    public void TestRollbackAction_Async_2calls_throws()
    {
        bool first_called = false;
        bool first_called_second = false;
        bool second_called = false;
        bool second_called_first = false;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncRollbackAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")), Is.False);
                    first_called = true;
                    if (second_called)
                        first_called_second = true;
                    File.Delete(fn);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddAsyncRollbackAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")));
                    second_called = true;
                    if (!first_called)
                        second_called_first = true;
                    File.Delete(fn2);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                await Task.Delay(10);
                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_second);
        Assert.That(second_called_first);
    }

    [Test]
    public void TestRollbackAction_Async_2calls_throws_1stThrows()
    {
        bool first_called = false;
        bool first_called_second = false;
        bool second_called = false;
        bool second_called_first = false;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws-1stThrows.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncRollbackAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws-1stThrows.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws-1stThrows.tmp")));
                    first_called = true;
                    if (second_called)
                        first_called_second = true;
                    File.Delete(fn);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws-1stThrows.tmp");
                File.WriteAllText(fn2, "this is a file2");

                if (fn2 != null)
                    throw new Exception("failure!");
                c.AddAsyncRollbackAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws-1stThrows.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws-1stThrows.tmp")));
                    second_called = true;
                    if (!first_called)
                        second_called_first = true;
                    File.Delete(fn2!);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                await Task.Delay(10);
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws-1stThrows.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws-1stThrows.tmp")));
        Assert.That(first_called);
        Assert.That(second_called, Is.False);
        Assert.That(first_called_second, Is.False);
        Assert.That(second_called_first, Is.False);
    }

#endregion Rollback Action

#region Post Commit Action

    [Test]
    public void TestPostCommitAction_Sync_1call()
    {
        bool called = false;
        IntegrationTestSetup.Exec(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddPostCommitAction("f", () => { called = true; File.Delete(fn); });

                Assert.That(File.Exists(fn));
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public void TestPostCommitAction_Sync_2calls()
    {
        bool first_called = false;
        bool first_called_first = false;
        bool second_called = false;
        bool second_called_second = false;
        IntegrationTestSetup.Exec(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddPostCommitAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2.tmp")));
                    first_called = true;
                    if (!second_called)
                        first_called_first = true;
                    File.Delete(fn);
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddPostCommitAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls.tmp")), Is.False);
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2.tmp")));
                    second_called = true;
                    if (first_called)
                        second_called_second = true;
                    File.Delete(fn2);
                });
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_first);
        Assert.That(second_called_second);
    }

    [Test]
    public void TestPostCommitAction_Sync_2calls_throws()
    {
        bool first_called = false;
        bool first_called_first = false;
        bool second_called = false;
        bool second_called_second = false;
        IntegrationTestSetup.Exec(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddPostCommitAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws.tmp")));
                    first_called = true;
                    if (!second_called)
                        first_called_first = true;
                    File.Delete(fn);

                    throw new IOException("failed!");
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddPostCommitAction("f", () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws.tmp")), Is.False);
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws.tmp")));
                    second_called = true;
                    if (first_called)
                        second_called_second = true;
                    File.Delete(fn2);

                    throw new IOException("failed!");
                });
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-throws.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-2calls-2-throws.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_first);
        Assert.That(second_called_second);
    }

    [Test]
    public async Task TestPostCommitAction_Async_1call()
    {
        bool called = false;
        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncPostCommitAction("f", async () => { called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));

                await Task.Delay(10);
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public void TestPostCommitAction_Sync_AsyncAction()
    {
        bool called = false;
        IntegrationTestSetup.Exec(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncPostCommitAction("f", async () => { called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public async Task TestPostCommitAction_Async_AsyncActionSyncCall()
    {
        bool called = false;
        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddPostCommitAction("f", () => { called = true; File.Delete(fn); });

                Assert.That(File.Exists(fn));

                await Task.Delay(10);
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public void TestPostCommitAction_Async_AsyncActionAsyncCall_WrongMethod()
    {
        bool called = false;
        Assert.Throws<NotSupportedException>(() => IntegrationTestSetup.Exec(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddPostCommitAction("f", async () => { called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")));
        Assert.That(called, Is.False);
    }

    [Test]
    public async Task TestPostCommitAction_Async_2calls()
    {
        bool first_called = false;
        bool first_called_first = false;
        bool second_called = false;
        bool second_called_second = false;
        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncPostCommitAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")));
                    first_called = true;
                    if (!second_called)
                        first_called_first = true;
                    File.Delete(fn);
                    await Task.Delay(10);
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddAsyncPostCommitAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")), Is.False);
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")));
                    second_called = true;
                    if (first_called)
                        second_called_second = true;
                    File.Delete(fn2);
                    await Task.Delay(10);
                });

                await Task.Delay(10);
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_first);
        Assert.That(second_called_second);
    }

    [Test]
    public async Task TestPostCommitAction_Async_2calls_throws()
    {
        bool first_called = false;
        bool first_called_first = false;
        bool second_called = false;
        bool second_called_second = false;
        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncPostCommitAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")));
                    first_called = true;
                    if (!second_called)
                        first_called_first = true;
                    File.Delete(fn);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddAsyncPostCommitAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")), Is.False);
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")));
                    second_called = true;
                    if (first_called)
                        second_called_second = true;
                    File.Delete(fn2);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                await Task.Delay(10);
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_first);
        Assert.That(second_called_second);
    }

    [Test]
    public void TestPostCommitAction_Sync_Rollback()
    {
        bool called = false;
        IntegrationTestSetup.Exec(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddPostCommitAction("f", () => { called = true; File.Delete(fn); });

                Assert.That(File.Exists(fn));

                c.SetRollback();
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback.tmp")));
        Assert.That(called, Is.False);
    }

    [Test]
    public void TestPostCommitAction_Sync_Throw()
    {
        bool called = false;
        bool caught = false;
        try
        {
            IntegrationTestSetup.Exec(
                (c) =>
                {
                    string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback-throw.tmp");
                    File.WriteAllText(fn, "this is a file");
                    c.AddPostCommitAction("f", () => { called = true; File.Delete(fn); });

                    Assert.That(File.Exists(fn));

                    throw new Exception("failure");
                });
        }
        catch (Exception e)
        {
            caught = true;
            Assert.That(e.Message, Is.EqualTo("failure"));
        }

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback-throw.tmp")));
        Assert.That(called, Is.False);
        Assert.That(caught);
    }

    [Test]
    public async Task TestPostCommitAction_Async_Rollback()
    {
        bool called = false;
        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                await Task.Delay(10);
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncPostCommitAction("f", async () => { await Task.Delay(10); called = true; File.Delete(fn); });

                Assert.That(File.Exists(fn));

                c.SetRollback();
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback.tmp")));
        Assert.That(called, Is.False);
    }

    [Test]
    public async Task TestPostCommitAction_Async_Throw()
    {
        bool called = false;
        bool caught = false;
        try
        {
            await IntegrationTestSetup.ExecAsync(
                async (c) =>
                {
                    await Task.Delay(10);
                    string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback-throw.tmp");
                    File.WriteAllText(fn, "this is a file");
                    c.AddAsyncPostCommitAction("f", async () => { await Task.Delay(10); called = true; File.Delete(fn); });

                    Assert.That(File.Exists(fn));

                    throw new Exception("failure");
                });
        }
        catch (Exception e)
        {
            caught = true;
            Assert.That(e.Message, Is.EqualTo("failure"));
        }

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback-throw.tmp")));
        Assert.That(called, Is.False);
        Assert.That(caught);
    }
#endregion Post Commit Action

#region ExecuteQueryProc

    [Test]
    public void TestExecuteQueryProc()
    {
        static TestRecord load(IResult r)
        {
            return new TestRecord() { Id = r.GetLong("ID"), Date = r.GetDateTimeNull("DT"), Int = r.GetIntNull("IntField"), Money = r.GetDecimalNull("MoneyField"), String = r.GetString("String") };
        };

        IntegrationTestSetup.Exec(
            (c) =>
            {
                Assert.That(c.IsAsync(), Is.False);

                c.ExecuteProc(
                    "usp_InsertTestRecord",
                    new SqlParameter("@ID", 1),
                    new SqlParameter("@String", "s1"),
                    new SqlParameter("@Int", 10));

                c.ExecuteProc(
                    "usp_InsertTestRecord",
                    new SqlParameter("@ID", 2),
                    new SqlParameter("@String", "s2"),
                    new SqlParameter("@Int", 10));

                Assert.That(c.ExecuteQueryProcOne(
                    "usp_GetTestRecordByID",
                    (r) => Assert.Fail("no record"),
                    new SqlParameter("@ID", -86)), Is.False);

                Assert.That(c.ExecuteQueryProcOne(
                    "usp_GetTestRecordByID",
                    (r) => Assert.That(r.GetString("String"), Is.EqualTo("s1")),
                    new SqlParameter("@ID", 1)));

                var sr = c.ExecuteQueryProcOne(
                    "usp_GetTestRecordByID",
                    load,
                    new SqlParameter("@ID", -86));
                Assert.That(sr.RowReturned, Is.False);
                Assert.That(sr.Value, Is.Null);

                sr = c.ExecuteQueryProcOne(
                    "usp_GetTestRecordByID",
                    load,
                    new SqlParameter("@ID", 1));
                Assert.That(sr.RowReturned);
                Assert.That(sr.Value, Is.Not.Null);
                Assert.That(sr.Value?.Id, Is.EqualTo(1));
                Assert.That(sr.Value?.String, Is.EqualTo("s1"));

                Assert.That(c.ExecuteQueryProcOne(
                    "usp_EchoBit",
                    r => r.GetBool("Bit"),
                    new SqlParameter("@Bit", true)).Value);

                Assert.That(c.ExecuteQueryProcOne(
                    "usp_EchoBit",
                    r => r.GetBool("Bit"),
                    new SqlParameter("@Bit", false)).Value, Is.False);

                var list = c.ExecuteQueryProcList(
                    "usp_GetTestRecordByInt",
                    load,
                    new SqlParameter("@Int", 10));
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                const string GetSql = "SELECT * FROM Test WHERE ID = @ID";
                Assert.That(c.ExecuteRawQueryOne(
                    GetSql,
                    (r) => Assert.Fail("no record"),
                    new SqlParameter("@ID", -86)), Is.False);

                Assert.That(c.ExecuteRawQueryOne(
                    GetSql,
                    (r) => Assert.That(r.GetString("String"), Is.EqualTo("s1")),
                    new SqlParameter("@ID", 1)));

                sr = c.ExecuteRawQueryOne(
                    GetSql,
                    load,
                    new SqlParameter("@ID", -86));
                Assert.That(sr.RowReturned, Is.False);
                Assert.That(sr.Value, Is.Null);

                sr = c.ExecuteRawQueryOne(
                    GetSql,
                    load,
                    new SqlParameter("@ID", 1));
                Assert.That(sr.RowReturned);
                Assert.That(sr.Value, Is.Not.Null);
                Assert.That(sr.Value?.Id, Is.EqualTo(1));
                Assert.That(sr.Value?.String, Is.EqualTo("s1"));

                const string GetSqlByID = "SELECT * FROM Test WHERE IntField = @Int"; 
                list = c.ExecuteRawQueryList(
                    GetSqlByID,
                    load,
                    new SqlParameter("@Int", 10));
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                Assert.That(c.ExecuteRaw("UPDATE Test SET IntField = 10"), Is.EqualTo(2));
            });
    }

    [Test]
    public void TestExecuteQueryProc_Default()
    {
        IntegrationTestSetup.Exec(
            (c) =>
            {
                Assert.That(c.IsAsync(), Is.False);

                c.ExecuteProc(
                    "usp_InsertTestRecord",
                    new SqlParameter("@ID", 1),
                    new SqlParameter("@String", "s1"),
                    new SqlParameter("@Int", 10));

                c.ExecuteProc(
                    "usp_InsertTestRecord",
                    new SqlParameter("@ID", 2),
                    new SqlParameter("@String", "s2"),
                    new SqlParameter("@Int", 10));

                Assert.That(c.ExecuteQueryProcOne(
                    "usp_GetTestRecordByID",
                    (r) => Assert.Fail("no record"),
                    new SqlParameter("@ID", -86)), Is.False);

                Assert.That(c.ExecuteQueryProcOne(
                    "usp_GetTestRecordByID",
                    (r) => Assert.That(r.GetString("String"), Is.EqualTo("s1")),
                    new SqlParameter("@ID", 1)));
            });
    }

    [Test]
    public async Task TestExecuteQueryProcAsync()
    {
        static Task<TestRecord> load(IResult r)
        {
            return Task.FromResult(new TestRecord() { Id = r.GetLong("ID"), Date = r.GetDateTimeNull("DT"), Int = r.GetIntNull("IntField"), Money = r.GetDecimalNull("MoneyField"), String = r.GetString("String") });
        };

        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                Assert.That(c.IsAsync());

                c.ExecuteProc( "usp_InsertTestRecord", new SqlParameter("@ID", 1), new SqlParameter("@String", "s1"), new SqlParameter("@Int", 10));
                c.ExecuteProc( "usp_InsertTestRecord", new SqlParameter("@ID", 2), new SqlParameter("@String", "s2"), new SqlParameter("@Int", 10));

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new SqlParameter("@ID", -86)), Is.False);

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new SqlParameter("@ID", 1)));

                var sr = await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    load,
                    new SqlParameter("@ID", -86));
                Assert.That(sr.RowReturned, Is.False);
                Assert.That(sr.Value, Is.Null);

                sr = await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    load,
                    new SqlParameter("@ID", 1));
                Assert.That(sr.RowReturned);
                Assert.That(sr.Value, Is.Not.Null);
                Assert.That(sr.Value?.Id, Is.EqualTo(1));
                Assert.That(sr.Value?.String, Is.EqualTo("s1"));

                var list = await c.ExecuteQueryProcListAsync(
                    "usp_GetTestRecordByInt",
                    load,
                    new SqlParameter("@Int", 10));
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                const string GetSql = "SELECT * FROM Test WHERE ID = @ID";
                Assert.That(await c.ExecuteRawQueryOneAsync(
                    GetSql,
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new SqlParameter("@ID", -86)), Is.False);

                Assert.That(await c.ExecuteRawQueryOneAsync(
                    GetSql,
                    (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new SqlParameter("@ID", 1)));

                sr = await c.ExecuteRawQueryOneAsync(
                    GetSql,
                    load,
                    new SqlParameter("@ID", -86));
                Assert.That(sr.RowReturned, Is.False);
                Assert.That(sr.Value, Is.Null);

                sr = await c.ExecuteRawQueryOneAsync(
                    GetSql,
                    load,
                    new SqlParameter("@ID", 1));
                Assert.That(sr.RowReturned);
                Assert.That(sr.Value, Is.Not.Null);
                Assert.That(sr.Value?.Id, Is.EqualTo(1));
                Assert.That(sr.Value?.String, Is.EqualTo("s1"));

                const string GetSqlByID = "SELECT * FROM Test WHERE IntField = @Int"; 
                list = await c.ExecuteRawQueryListAsync(
                    GetSqlByID,
                    load,
                    new SqlParameter("@Int", 10));
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                Assert.That(await c.ExecuteRawAsync("UPDATE Test SET IntField = 10"), Is.EqualTo(2));
            });
    }

    [Test]
    public async Task TestExecuteQueryProcAsync_Default()
    {
        static Task<TestRecord> load(IResult r)
        {
            return Task.FromResult(new TestRecord() { Id = r.GetLong("ID"), Date = r.GetDateTimeNull("DT"), Int = r.GetIntNull("IntField"), Money = r.GetDecimalNull("MoneyField"), String = r.GetString("String") });
        };

        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                Assert.That(c.IsAsync());

                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 1), new SqlParameter("@String", "s1"), new SqlParameter("@Int", 10));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 2), new SqlParameter("@String", "s2"), new SqlParameter("@Int", 10));

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new SqlParameter("@ID", -86)), Is.False);

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new SqlParameter("@ID", 1)));

                var sr = await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    load,
                    new SqlParameter("@ID", -86));
                Assert.That(sr.RowReturned, Is.False);
                Assert.That(sr.Value, Is.Null);

                sr = await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    load,
                    new SqlParameter("@ID", 1));
                Assert.That(sr.RowReturned);
                Assert.That(sr.Value, Is.Not.Null);
                Assert.That(sr.Value?.Id, Is.EqualTo(1));
                Assert.That(sr.Value?.String, Is.EqualTo("s1"));
            });
    }

    [Test]
    public async Task TestCancel()
    {
        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                Assert.That(c.IsAsync());

                var cts = new CancellationTokenSource();
                cts.CancelAfter(TimeSpan.FromMilliseconds(200));
                try
                {
                    await c.ExecuteRawAsync("WAITFOR DELAY '00:02'", cts.Token);
                    Assert.Fail();
                }
                catch (SqlException se)
                {
                    Assert.That(se.Message, Does.Contain("Operation cancelled by user."), se.Message);
                }

                cts = new CancellationTokenSource();
                cts.CancelAfter(TimeSpan.FromMilliseconds(200));
                try
                {
                    await c.ExecuteRawQueryOneAsync("WAITFOR DELAY '00:02'; SELECT 1 AS a, 2 AS b, 3 AS c", 
                        r =>
                        {
                            return Task.FromResult((r.GetInt("a"), r.GetInt("b"), r.GetInt("c")));
                        },
                        cts.Token);
                    Assert.Fail();
                }
                catch (SqlException se)
                {
                    Assert.That(se.Message, Does.Contain("Operation cancelled by user."), se.Message);
                }
            });
    }

#endregion ExecuteQueryProc

#region ExecuteQueryProc Null Blob

    [Test]
    public void TestExecuteQueryProc_NullBlob()
    {
        IntegrationTestSetup.Exec(
            (c) =>
            {
                Assert.That(c.IsAsync(), Is.False);

                c.ExecuteProc(
                    "usp_InsertTestRecord",
                    new SqlParameter("@ID", 1),
                    new SqlParameter("@String", "s1"),
                    new SqlParameter("@Int", 10),
                    new SqlParameter("@Blob", Convert.FromHexString("aabbcc")));

                c.ExecuteProc(
                    "usp_InsertTestRecord",
                    new SqlParameter("@ID", 2),
                    new SqlParameter("@String", "s2"),
                    new SqlParameter("@Int", 10));

                Assert.That(c.ExecuteQueryProcOne(
                    "usp_GetTestRecordByID",
                    (r) => Convert.ToHexStringLower(r.GetBytes("BlobField")!),
                    new SqlParameter("@ID", 1)).Value, Is.EqualTo("aabbcc"));

                Assert.That(c.ExecuteQueryProcOne(
                    "usp_GetTestRecordByID",
                    (r) => r.GetBytes("BlobField"),
                    new SqlParameter("@ID", 2)).Value, Is.Null);
            });
    }
#endregion

#region ExecuteQueryProc Multiple Result Sets

    [Test]
    public void TestExecuteQueryProcMultipleResultSets()
    {
        static TestRecord load(IResult r)
        {
            return new TestRecord()
            {
                Id = r.GetLong("ID"), Date = r.GetDateTimeNull("DT"), Int = r.GetIntNull("IntField"), Money = r.GetDecimalNull("MoneyField"), String = r.GetString("String"),
                Others = r.GetColumns().Where(c => c.StartsWith("Num")).ToDictionary(c => c, c => (object)r.GetInt(c))
            };
        };

        static void process(IResultSet rs, int idx, int p)
        {
            var items = rs.GetList(load);
            Assert.That(items, Has.Count.EqualTo(3));
            for (int i = 0; i < 3; i++)
            {
                Assert.That(items[i].Id, Is.EqualTo(p * 10));
                Assert.That(items[i].String, Is.EqualTo($"s{p}"));
                Assert.That(items[i].Int, Is.EqualTo(idx * 10));
                p++;

                Assert.That(items[i].Others?[$"Num{idx}"], Is.EqualTo(idx));
            }
        };

        IntegrationTestSetup.Exec(
            (c) =>
            {
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 10), new SqlParameter("@String", "s1"), new SqlParameter("@Int", 10));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 20), new SqlParameter("@String", "s2"), new SqlParameter("@Int", 10));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 30), new SqlParameter("@String", "s3"), new SqlParameter("@Int", 10));

                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 40), new SqlParameter("@String", "s4"), new SqlParameter("@Int", 20));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 50), new SqlParameter("@String", "s5"), new SqlParameter("@Int", 20));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 60), new SqlParameter("@String", "s6"), new SqlParameter("@Int", 20));

                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 70), new SqlParameter("@String", "s7"), new SqlParameter("@Int", 30));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 80), new SqlParameter("@String", "s8"), new SqlParameter("@Int", 30));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 90), new SqlParameter("@String", "s9"), new SqlParameter("@Int", 30));

                c.ExecuteQueryProcMultiResultSet(
                    "usp_GetTestRecordsByInts", 
                    (mrs) =>
                    {
                        IResultSet? rs;
                        rs = mrs.NextResultSet();
                        process(rs!, 1, 1);

                        rs = mrs.NextResultSet();
                        process(rs!, 2, 4);

                        rs = mrs.NextResultSet();
                        process(rs!, 3, 7);

                        Assert.That(mrs.NextResultSet(), Is.Null);
                    },
                    new SqlParameter("@Int1", 10), 
                    new SqlParameter("@Int2", 20), 
                    new SqlParameter("@Int3", 30));

                c.ExecuteQueryProcMultiResultSet(
                    "usp_GetTestRecordsByInts", 
                    (mrs) =>
                    {
                        IResultSet? rs;
                        rs = mrs.NextResultSet();
                        var _ = rs!.Take(2).ToList();

                        rs = mrs.NextResultSet();
                        _ = rs!.Take(1).ToList();

                        rs = mrs.NextResultSet();
                        _ = rs!.Take(3).ToList();

                        Assert.That(mrs.NextResultSet(), Is.Null);
                    },
                    new SqlParameter("@Int1", 10), 
                    new SqlParameter("@Int2", 20), 
                    new SqlParameter("@Int3", 30));

                c.ExecuteQueryProcMultiResultSet(
                    "usp_GetTestRecordsByInts", 
                    (mrs) =>
                    {
                        int p = 1;
                        foreach (var rs in mrs)
                        {
                            process(rs, p, (p-1) * 3 + 1);
                            p++;
                        }

                        Assert.That(mrs.NextResultSet(), Is.Null);
                    },
                    new SqlParameter("@Int1", 10), 
                    new SqlParameter("@Int2", 20), 
                    new SqlParameter("@Int3", 30));
            });
    }

    [Test]
    public async Task TestExecuteQueryProcMultipleResultSetsAsync()
    {
        static Task<TestRecord> load(IResult r)
        {
            return Task.FromResult(new TestRecord()
            {
                Id = r.GetLong("ID"), Date = r.GetDateTimeNull("DT"), Int = r.GetIntNull("IntField"), Money = r.GetDecimalNull("MoneyField"), String = r.GetString("String"),
                Others = r.GetColumns().Where(c => c.StartsWith("Num")).ToDictionary(c => c, c => (object)r.GetInt(c))
            });
        };

        static async Task process(IAsyncResultSet rs, int idx, int p)
        {
            var items = await rs.GetListAsync(load);
            Assert.That(items, Has.Count.EqualTo(3));
            for (int i = 0; i < 3; i++)
            {
                Assert.That(items[i].Id, Is.EqualTo(p * 10));
                Assert.That(items[i].String, Is.EqualTo($"s{p}"));
                Assert.That(items[i].Int, Is.EqualTo(idx * 10));
                p++;

                Assert.That(items[i].Others?[$"Num{idx}"], Is.EqualTo(idx));
            }
        };

        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 10), new SqlParameter("@String", "s1"), new SqlParameter("@Int", 10));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 20), new SqlParameter("@String", "s2"), new SqlParameter("@Int", 10));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 30), new SqlParameter("@String", "s3"), new SqlParameter("@Int", 10));

                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 40), new SqlParameter("@String", "s4"), new SqlParameter("@Int", 20));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 50), new SqlParameter("@String", "s5"), new SqlParameter("@Int", 20));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 60), new SqlParameter("@String", "s6"), new SqlParameter("@Int", 20));

                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 70), new SqlParameter("@String", "s7"), new SqlParameter("@Int", 30));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 80), new SqlParameter("@String", "s8"), new SqlParameter("@Int", 30));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 90), new SqlParameter("@String", "s9"), new SqlParameter("@Int", 30));

                await c.ExecuteQueryProcMultiResultSetAsync(
                    "usp_GetTestRecordsByInts", 
                    async (mrs) =>
                    {
                        IAsyncResultSet? rs;
                        rs = await mrs.NextResultSetAsync();
                        await process(rs!, 1, 1);

                        rs = await mrs.NextResultSetAsync();
                        await process(rs!, 2, 4);

                        rs = await mrs.NextResultSetAsync();
                        await process(rs!, 3, 7);

                        Assert.That(await mrs.NextResultSetAsync(), Is.Null);
                    },
                    new SqlParameter("@Int1", 10), 
                    new SqlParameter("@Int2", 20), 
                    new SqlParameter("@Int3", 30));
            });
    }

#endregion ExecuteQueryProc Multiple Result Sets

#region ExecuteRaw Multiple Result Sets


    [Test]
    public void TestExecuteRawMultipleResultSets()
    {
        static TestRecord load(IResult r)
        {
            return new TestRecord()
            {
                Id = r.GetLong("ID"), Date = r.GetDateTimeNull("DT"), Int = r.GetIntNull("IntField"), Money = r.GetDecimalNull("MoneyField"), String = r.GetString("String"),
                Others = r.GetColumns().Where(c => c.StartsWith("Num")).ToDictionary(c => c, c => (object)r.GetInt(c))
            };
        };

        static void process(IResultSet rs, int idx, int p)
        {
            var items = rs.GetList(load);
            Assert.That(items, Has.Count.EqualTo(3));
            for (int i = 0; i < 3; i++)
            {
                Assert.That(items[i].Id, Is.EqualTo(p * 10));
                Assert.That(items[i].String, Is.EqualTo($"s{p}"));
                Assert.That(items[i].Int, Is.EqualTo(idx * 10));
                p++;

                Assert.That(items[i].Others?[$"Num{idx}"], Is.EqualTo(idx));
            }
        };

        IntegrationTestSetup.Exec(
            (c) =>
            {
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 10), new SqlParameter("@String", "s1"), new SqlParameter("@Int", 10));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 20), new SqlParameter("@String", "s2"), new SqlParameter("@Int", 10));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 30), new SqlParameter("@String", "s3"), new SqlParameter("@Int", 10));

                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 40), new SqlParameter("@String", "s4"), new SqlParameter("@Int", 20));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 50), new SqlParameter("@String", "s5"), new SqlParameter("@Int", 20));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 60), new SqlParameter("@String", "s6"), new SqlParameter("@Int", 20));

                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 70), new SqlParameter("@String", "s7"), new SqlParameter("@Int", 30));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 80), new SqlParameter("@String", "s8"), new SqlParameter("@Int", 30));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 90), new SqlParameter("@String", "s9"), new SqlParameter("@Int", 30));

                c.ExecuteRawQueryMultiResultSet(
                    Usp_GetTestRecordsByInts, 
                    (mrs) =>
                    {
                        IResultSet? rs;
                        rs = mrs.NextResultSet();
                        process(rs!, 1, 1);

                        rs = mrs.NextResultSet();
                        process(rs!, 2, 4);

                        rs = mrs.NextResultSet();
                        process(rs!, 3, 7);

                        Assert.That(mrs.NextResultSet(), Is.Null);
                    },
                    new SqlParameter("@Int1", 10), 
                    new SqlParameter("@Int2", 20), 
                    new SqlParameter("@Int3", 30));

                c.ExecuteRawQueryMultiResultSet(
                    Usp_GetTestRecordsByInts, 
                    (mrs) =>
                    {
                        int p = 1;
                        foreach (var rs in mrs)
                        {
                            process(rs, p, (p-1) * 3 + 1);
                            p++;
                        }

                        Assert.That(mrs.NextResultSet(), Is.Null);
                    },
                    new SqlParameter("@Int1", 10), 
                    new SqlParameter("@Int2", 20), 
                    new SqlParameter("@Int3", 30));
            });
    }

    [Test]
    public async Task TestExecuteRawMultipleResultSetsAsync()
    {
        static Task<TestRecord> load(IResult r)
        {
            return Task.FromResult(new TestRecord()
            {
                Id = r.GetLong("ID"), Date = r.GetDateTimeNull("DT"), Int = r.GetIntNull("IntField"), Money = r.GetDecimalNull("MoneyField"), String = r.GetString("String"),
                Others = r.GetColumns().Where(c => c.StartsWith("Num")).ToDictionary(c => c, c => (object)r.GetInt(c))
            });
        };

        static async Task process(IAsyncResultSet rs, int idx, int p)
        {
            var items = await rs.GetListAsync(load);
            Assert.That(items, Has.Count.EqualTo(3));
            for (int i = 0; i < 3; i++)
            {
                Assert.That(items[i].Id, Is.EqualTo(p * 10));
                Assert.That(items[i].String, Is.EqualTo($"s{p}"));
                Assert.That(items[i].Int, Is.EqualTo(idx * 10));
                p++;

                Assert.That(items[i].Others?[$"Num{idx}"], Is.EqualTo(idx));
            }
        };

        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 10), new SqlParameter("@String", "s1"), new SqlParameter("@Int", 10));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 20), new SqlParameter("@String", "s2"), new SqlParameter("@Int", 10));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 30), new SqlParameter("@String", "s3"), new SqlParameter("@Int", 10));

                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 40), new SqlParameter("@String", "s4"), new SqlParameter("@Int", 20));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 50), new SqlParameter("@String", "s5"), new SqlParameter("@Int", 20));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 60), new SqlParameter("@String", "s6"), new SqlParameter("@Int", 20));

                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 70), new SqlParameter("@String", "s7"), new SqlParameter("@Int", 30));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 80), new SqlParameter("@String", "s8"), new SqlParameter("@Int", 30));
                c.ExecuteProc("usp_InsertTestRecord", new SqlParameter("@ID", 90), new SqlParameter("@String", "s9"), new SqlParameter("@Int", 30));

                await c.ExecuteRawQueryMultiResultSetAsync(
                    Usp_GetTestRecordsByInts, 
                    async (mrs) =>
                    {
                        IAsyncResultSet? rs;
                        rs = await mrs.NextResultSetAsync();
                        await process(rs!, 1, 1);

                        rs = await mrs.NextResultSetAsync();
                        await process(rs!, 2, 4);

                        rs = await mrs.NextResultSetAsync();
                        await process(rs!, 3, 7);

                        Assert.That(await mrs.NextResultSetAsync(), Is.Null);
                    },
                    new SqlParameter("@Int1", 10), 
                    new SqlParameter("@Int2", 20), 
                    new SqlParameter("@Int3", 30));
            });
    }

    #endregion ExecuteQueryProc Multiple Result Sets

#region ExecuteTxn / ExecuteTxnAsync
    /* uncomment to test compiler error
    [Test]
    public void TestExecuteTxn_AsyncLambda_SyncTxn()
    {
        Assert.ThrowsAsync<NotSupportedException>(async () =>
        {
            await IntegrationTestSetup.GetProvider().ExecuteRawTxn(
                async (c) =>
                {
                    await Task.Delay(10);
                    var dt = c.ExecuteRawQueryOne("SELECT SYSUTCDATETIME() AS t", r => r.GetDateTimeNull("t")).Value;
                    Assert.IsNotNull(dt);
                });
        });

        Assert.ThrowsAsync<NotSupportedException>(async () =>
        {
            int x = await IntegrationTestSetup.GetProvider().ExecuteRawTxn(
                async (c) =>
                {
                    await Task.Delay(10);
                    var dt = c.ExecuteRawQueryOne("SELECT SYSUTCDATETIME() AS t", r => r.GetDateTimeNull("t")).Value;
                    Assert.IsNotNull(dt);
                    return 55;
                });

            Assert.Fail();
        });
    }
    */

    [Test]
    public async Task TestExecuteTxn_AsyncLambda_AsyncTxn()
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                await Task.Delay(10);
                var dt = c.ExecuteRawQueryOne("SELECT SYSUTCDATETIME() AS t", r => r.GetDateTimeNull("t")).Value;
                Assert.That(dt, Is.Not.Null);
            });

        var x = await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                await Task.Delay(10);
                var dt = c.ExecuteRawQueryOne("SELECT SYSUTCDATETIME() AS t", r => r.GetDateTimeNull("t")).Value;
                Assert.That(dt, Is.Not.Null);
                return dt;
            });
    }

    [Test]
    public void TestExecuteTxn_SyncLambda_SyncTxn()
    {
        IntegrationTestSetup.GetProvider().ExecuteRawTxn(
            (c) =>
            {
                var dt = c.ExecuteRawQueryOne("SELECT SYSUTCDATETIME() AS t", r => r.GetDateTimeNull("t")).Value;
                Assert.That(dt, Is.Not.Null);
            });

        var x = IntegrationTestSetup.GetProvider().ExecuteRawTxn(
            (c) =>
            {
                var dt = c.ExecuteRawQueryOne("SELECT SYSUTCDATETIME() AS t", r => r.GetDateTimeNull("t")).Value;
                Assert.That(dt, Is.Not.Null);
                return dt;
            });
    }

    [Test]
    public async Task TestExecuteTxn_SyncLambda_AsyncTxn()
    {
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            (c) =>
            {
                var dt = c.ExecuteRawQueryOne("SELECT SYSUTCDATETIME() AS t", r => r.GetDateTimeNull("t")).Value;
                Assert.That(dt, Is.Not.Null);
                return Task.CompletedTask;
            });

        var x = await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            (c) =>
            {
                var dt = c.ExecuteRawQueryOne("SELECT SYSUTCDATETIME() AS t", r => r.GetDateTimeNull("t")).Value;
                Assert.That(dt, Is.Not.Null);
                return Task.FromResult(dt);
            });
    }
    #endregion

#region Structured Type

    [Test]
    public void TestStructuredType()
    {
        IntegrationTestSetup.Exec(
            (c) =>
            {
                var list = new List<string> { "abc", "123", "def" };
                var ret = c.ExecuteQueryProcList("usp_EchoNVARCHARList", r => r.GetString("Item"), new SqlParameter("@Table", SqlDbType.Structured) { Value = list.ToNVarCharList() });
                Assert.That(ret, Is.EquivalentTo(list));

                var testList = new List<TestRecord>
                {
                    new TestRecord { Id = 5, String = "hello", Int = 77 },
                    new TestRecord { Id = 6, String = "there", Int = 78 },
                    new TestRecord { Id = 7, String = "table", Int = 79 }
                };

                var table = new DataTable();
                table.Columns.Add("GUID", typeof(Guid));
                table.Columns.Add("ID", typeof(long));
                table.Columns.Add("DT", typeof(DateTime));
                table.Columns.Add("String", typeof(string));
                table.Columns.Add("IntField", typeof(int));
                table.Columns.Add("MoneyField", typeof(decimal));
                foreach (var t in testList)
                {
                    var r = table.Rows.Add();
                    r["GUID"] = Guid.NewGuid();
                    r["ID"] = t.Id;
                    r["String"] = t.String;
                    r["IntField"] = t.Int;
                }

                var testRet = c.ExecuteQueryProcList("usp_EchoTableType", 
                    r => new TestRecord
                    {
                        Id = r.GetLong("ID"),
                        String = r.GetString("String"),
                        Int = r.GetInt("IntField")
                    },
                    new SqlParameter("@Table", SqlDbType.Structured) { Value = table });
                foreach (var tl in testList)
                    Assert.That(testRet.Any(t => t.Id == tl.Id && t.String == tl.String && t.Int == tl.Int));
            });
    }
    #endregion

#region stream

    [Test]
    public void TestStream_Read()
    {
        Stream? stream = null;
        var bigString = string.Join("", Enumerable.Repeat("a2", 1_000_000));

        IntegrationTestSetup.Exec(
            (c) =>
            {
                Assert.That(c.IsAsync(), Is.False);

                c.ExecuteProc(
                    "usp_InsertTestRecord",
                    new SqlParameter("@ID", 1),
                    new SqlParameter("@String", "s1"),
                    new SqlParameter("@Int", 10),
                    new SqlParameter("@Blob", Convert.FromHexString(bigString)));

                stream = c.ExecuteQueryProcOne(
                    "usp_GetTestRecordByID",
                    (r) => r.GetStream("BlobField"),
                    new SqlParameter("@ID", 1)).Value;

            });

        var ms = new MemoryStream();
        stream?.CopyTo(ms);
        var data = Convert.ToHexStringLower(ms.ToArray());
        Assert.That(data, Is.EqualTo(bigString));
    }

    [Test]
    public async Task TestStream_ReadAsync()
    {
        Stream? stream = null;
        var bigString = string.Join("", Enumerable.Repeat("a2", 1_000_000));

        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                Assert.That(c.IsAsync());

                await c.ExecuteProcAsync(
                    "usp_InsertTestRecord",
                    new SqlParameter("@ID", 1),
                    new SqlParameter("@String", "s1"),
                    new SqlParameter("@Int", 10),
                    new SqlParameter("@Blob", Convert.FromHexString(bigString)));

                var res = await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => Task.FromResult(r.GetStream("BlobField")),
                    new SqlParameter("@ID", 1));
                stream = res.Value;
            });

        var ms = new MemoryStream();
        stream?.CopyTo(ms);
        var data = Convert.ToHexStringLower(ms.ToArray());
        Assert.That(data, Is.EqualTo(bigString));
    }

    [Test]
    public void TestStream_Write()
    {
        Stream? stream = null;
        var bigBytes = new MemoryStream(1_000_000);
        for (int i = 0; i < bigBytes.Capacity; i++)
            bigBytes.WriteByte(0xa2);
        bigBytes.Position = 0;

        IntegrationTestSetup.Exec(
            (c) =>
            {
                Assert.That(c.IsAsync(), Is.False);

                c.ExecuteProc(
                    "usp_InsertTestRecord",
                    new SqlParameter("@ID", 1),
                    new SqlParameter("@String", "s1"),
                    new SqlParameter("@Int", 10),
                    new SqlParameter("@Blob", SqlDbType.VarBinary) { Value = bigBytes });

                stream = c.ExecuteQueryProcOne(
                    "usp_GetTestRecordByID",
                    (r) => r.GetStream("BlobField"),
                    new SqlParameter("@ID", 1)).Value;

                Assert.That(bigBytes.Position, Is.EqualTo(1_000_000));
                bigBytes.Position = 0;

                var ms = new MemoryStream();
                stream?.CopyTo(ms);
                Assert.That(ms.ToArray(), Is.EqualTo(bigBytes.ToArray()).AsCollection);
            });

        bigBytes.Dispose();
    }

    [Test]
    public async Task TestStream_WriteAsync()
    {
        Stream? stream = null;
        var bigBytes = new MemoryStream(1_000_000);
        for (int i = 0; i < bigBytes.Capacity; i++)
            bigBytes.WriteByte(0xa2);
        bigBytes.Position = 0;

        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                Assert.That(c.IsAsync());

                await c.ExecuteProcAsync(
                    "usp_InsertTestRecord",
                    new SqlParameter("@ID", 1),
                    new SqlParameter("@String", "s1"),
                    new SqlParameter("@Int", 10),
                    new SqlParameter("@Blob", SqlDbType.VarBinary) { Value = bigBytes });

                var res = await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => Task.FromResult(r.GetStream("BlobField")),
                    new SqlParameter("@ID", 1));
                stream = res.Value;

                Assert.That(bigBytes.Position, Is.EqualTo(1_000_000));
                bigBytes.Position = 0;

                var ms = new MemoryStream();
                stream?.CopyTo(ms);
                Assert.That(ms.ToArray(), Is.EqualTo(bigBytes.ToArray()).AsCollection);
            });

        bigBytes.Dispose();
    }
#endregion

#region json

    class JsonTestObj
    {
        public int X { get; set; }
        public string? Z { get; set; }
        public string? Y { get; set; }
        public object? N { get; set; }
    }

    [Test]
    public void TestJson()
    {
        IntegrationTestSetup.Exec(
            (c) =>
            {
                // insert without type spec
                c.ExecuteProc(
                    "usp_InsertTestRecord",
                    new SqlParameter("@ID", 1818),
                    new SqlParameter("@String", JsonUtil.SerializeObject(new { x = 5, z = "99a" })));

                var ret1 = c.ExecuteQueryProcOne("usp_GetTestRecordByID",
                    r => r.GetJson("String"),
                    new SqlParameter("@ID", 1818));

                Assert.That(ret1.Value?["x"], Is.EqualTo(5));
                Assert.That(ret1.Value?["z"], Is.EqualTo("99a"));

                ret1.Value!["n"] = 77;
                ret1.Value!["x"] = 8;
                c.ExecuteProc(
                    "usp_UpdateTestRecord",
                    new SqlParameter("@ID", 1818),
                    new SqlParameter("@String", JsonUtil.SerializeObject(ret1.Value)));

                var ret3 = c.ExecuteQueryProcOne("usp_GetTestRecordByID",
                    r => r.GetJson("String"),
                    new SqlParameter("@ID", 1818));

                Assert.That(ret3.Value?["x"], Is.EqualTo(8));
                Assert.That(ret3.Value?["z"], Is.EqualTo("99a"));
                Assert.That(ret3.Value?["n"], Is.EqualTo(77));

                var ret4 = c.ExecuteQueryProcOne("usp_GetTestRecordByID",
                    r => r.GetJsonObject<JsonTestObj>("String"),
                    new SqlParameter("@ID", 1818));

                Assert.That(ret4.Value?.X, Is.EqualTo(8));
                Assert.That(ret4.Value?.Z, Is.EqualTo("99a"));
                Assert.That(ret4.Value?.N, Is.EqualTo(77));
            });
    }

    #endregion json

#region without txn
    [Test]
    public async Task WithoutTxn_ReadOnly()
    {
        static Task<TestRecord> load(IResult r)
        {
            return Task.FromResult(new TestRecord() { Id = r.GetLong("ID"), Date = r.GetDateTimeNull("DT"), Int = r.GetIntNull("IntField"), Money = r.GetDecimalNull("MoneyField"), String = r.GetString("String") });
        };

        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                await c.ExecuteProcAsync("usp_InsertTestRecord", new SqlParameter("@ID", 1), new SqlParameter("@String", "s1"), new SqlParameter("@Int", 10));
                await c.ExecuteProcAsync("usp_InsertTestRecord", new SqlParameter("@ID", 2), new SqlParameter("@String", "s2"), new SqlParameter("@Int", 10));
            }
        );

        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new SqlParameter("@ID", -86)), Is.False);
            });

        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new SqlParameter("@ID", -86)), Is.False);

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new SqlParameter("@ID", 1)));

                var sr = await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    load,
                    new SqlParameter("@ID", -86));
                Assert.That(sr.RowReturned, Is.False);
                Assert.That(sr.Value, Is.Null);

                sr = await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    load,
                    new SqlParameter("@ID", 1));
                Assert.That(sr.RowReturned);
                Assert.That(sr.Value, Is.Not.Null);
                Assert.That(sr.Value?.Id, Is.EqualTo(1));
                Assert.That(sr.Value?.String, Is.EqualTo("s1"));

                var list = await c.ExecuteQueryProcListAsync(
                    "usp_GetTestRecordByInt",
                    load,
                    new SqlParameter("@Int", 10));
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));

                const string GetSql = "SELECT * FROM Test WHERE ID = @ID";
                Assert.That(await c.ExecuteRawQueryOneAsync(
                    GetSql,
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new SqlParameter("@ID", -86)), Is.False);

                Assert.That(await c.ExecuteRawQueryOneAsync(
                    GetSql,
                    (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new SqlParameter("@ID", 1)));

                sr = await c.ExecuteRawQueryOneAsync(
                    GetSql,
                    load,
                    new SqlParameter("@ID", -86));
                Assert.That(sr.RowReturned, Is.False);
                Assert.That(sr.Value, Is.Null);

                sr = await c.ExecuteRawQueryOneAsync(
                    GetSql,
                    load,
                    new SqlParameter("@ID", 1));
                Assert.That(sr.RowReturned);
                Assert.That(sr.Value, Is.Not.Null);
                Assert.That(sr.Value?.Id, Is.EqualTo(1));
                Assert.That(sr.Value?.String, Is.EqualTo("s1"));

                const string GetSqlByID = "SELECT * FROM Test WHERE IntField = @Int";
                list = await c.ExecuteRawQueryListAsync(
                    GetSqlByID,
                    load,
                    new SqlParameter("@Int", 10));
                Assert.That(list, Has.Count.EqualTo(2));
                Assert.That(list[0].Id, Is.EqualTo(1));
                Assert.That(list[0].String, Is.EqualTo("s1"));
                Assert.That(list[1].Id, Is.EqualTo(2));
                Assert.That(list[1].String, Is.EqualTo("s2"));
            });
    }

    [Test]
    public async Task WithoutTxn_OneWrite_OK()
    {
        // single op, 1 write
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.ExecuteProcAsync("usp_InsertTestRecord", new SqlParameter("@ID", 1), new SqlParameter("@String", "s1"), new SqlParameter("@Int", 10));
            });

        // read, then 1 write
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new SqlParameter("@ID", 1)));

                await c.ExecuteProcAsync("usp_InsertTestRecord", new SqlParameter("@ID", 2), new SqlParameter("@String", "s2"), new SqlParameter("@Int", 10));
            });

        // 1 write, then read
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.ExecuteProcAsync("usp_InsertTestRecord", new SqlParameter("@ID", 3), new SqlParameter("@String", "s3"), new SqlParameter("@Int", 10));

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s2")); return Task.CompletedTask; },
                    new SqlParameter("@ID", 2)));
            });

        // 1 write, then multiple read
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.ExecuteProcAsync("usp_InsertTestRecord", new SqlParameter("@ID", 4), new SqlParameter("@String", "s4"), new SqlParameter("@Int", 10));

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s3")); return Task.CompletedTask; },
                    new SqlParameter("@ID", 3)));

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s4")); return Task.CompletedTask; },
                    new SqlParameter("@ID", 4)));
            });

        // read, 1 write, then multiple read
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new SqlParameter("@ID", 1)));

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s2")); return Task.CompletedTask; },
                    new SqlParameter("@ID", 2)));

                await c.ExecuteProcAsync("usp_InsertTestRecord", new SqlParameter("@ID", 5), new SqlParameter("@String", "s5"), new SqlParameter("@Int", 10));

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s3")); return Task.CompletedTask; },
                    new SqlParameter("@ID", 3)));

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s5")); return Task.CompletedTask; },
                    new SqlParameter("@ID", 5)));
            });
    }

    [Test]
    public async Task WithoutTxn_OneWrite_NoRollback()
    {
        // confirm row does not exist
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new SqlParameter("@ID", 1)), Is.False);
            });

        // 1 write, fail
        Assert.ThrowsAsync<NotFiniteNumberException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    await c.ExecuteProcAsync("usp_InsertTestRecord", new SqlParameter("@ID", 1), new SqlParameter("@String", "s1"), new SqlParameter("@Int", 10));

                    throw new NotFiniteNumberException();
                });
        });

        // confirm write succeeded
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new SqlParameter("@ID", 1)));
            });
    }

    [Test]
    public async Task WithoutTxn_TwoWrites_NoRollbackAndFail()
    {
        // confirm rows do not exist
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new SqlParameter("@ID", 1)), Is.False);

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new SqlParameter("@ID", 2)), Is.False);
            });

        // 2 write, fail
        Assert.ThrowsAsync<NotSupportedException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    await c.ExecuteProcAsync("usp_InsertTestRecord", new SqlParameter("@ID", 1), new SqlParameter("@String", "s1"), new SqlParameter("@Int", 10));

                    await c.ExecuteProcAsync("usp_InsertTestRecord", new SqlParameter("@ID", 2), new SqlParameter("@String", "s2"), new SqlParameter("@Int", 10));
                });
        });

        // confirm first write succeeded, second didn't happen
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new SqlParameter("@ID", 1)));

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.Fail("no record"); return Task.CompletedTask; },
                    new SqlParameter("@ID", 2)), Is.False);
            });
    }

    [Test]
    public async Task WithoutTxn_OneRead_NoRollback()
    {
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.ExecuteProcAsync("usp_InsertTestRecord", new SqlParameter("@ID", 1), new SqlParameter("@String", "s1"), new SqlParameter("@Int", 10));
            });

        // confirm write succeeded, but fail
        bool errorCalled = false;
        Assert.ThrowsAsync<NotFiniteNumberException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    c.AddErrorAction("should get called", e => { errorCalled = true; });

                    Assert.That(await c.ExecuteQueryProcOneAsync(
                        "usp_GetTestRecordByID",
                        (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s1")); return Task.CompletedTask; },
                        new SqlParameter("@ID", 1)));

                    throw new NotFiniteNumberException();
                });
        });

        Assert.That(errorCalled);
    }

    [Test]
    public async Task WithoutTxn_OneRead_TransientRetry()
    {
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                await c.ExecuteProcAsync("usp_InsertTestRecord", new SqlParameter("@ID", 1), new SqlParameter("@String", "s1"), new SqlParameter("@Int", 10));
            });

        int retries = 0;
        int errorCalled = 0;
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                c.AddErrorAction("should not get called", e => { errorCalled++; });

                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new SqlParameter("@ID", 1)));

                retries++;
                if (retries < 3)
                    throw new TimeoutException("should retry");
            });

        Assert.That(retries, Is.EqualTo(3));
        Assert.That(errorCalled, Is.EqualTo(2));
    }

    [Test]
    public async Task WithoutTxn_OneWrite_NoTransientRetry()
    {
        int retries = 0;
        Assert.ThrowsAsync<TimeoutException>(async () =>
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    await c.ExecuteProcAsync("usp_InsertTestRecord", new SqlParameter("@ID", 1), new SqlParameter("@String", "s1"), new SqlParameter("@Int", 10));

                    retries++;
                    if (retries < 3)
                        throw new TimeoutException("should retry");
                });
        });

        Assert.That(retries, Is.EqualTo(1));

        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                Assert.That(await c.ExecuteQueryProcOneAsync(
                    "usp_GetTestRecordByID",
                    (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s1")); return Task.CompletedTask; },
                    new SqlParameter("@ID", 1)));
            });
    }
    #endregion

#region without txn - post error
    [Test]
    public void WithoutTxn_PostError_Async_1call()
    {
        bool called = false;
        Exception? sent = null;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncErrorAction("f", async (e) => { sent = e; called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));

                await Task.Delay(10);
                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
        Assert.That(sent?.Message, Is.EqualTo("failure!"));
    }

    [Test]
    public void WithoutTxn_PostError_Sync_AsyncAction()
    {
        bool called = false;
        Exception? sent = null;
        Assert.Throws<Exception>(() => IntegrationTestSetup.ExecWithout(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncErrorAction("f", async (e) => { sent = e; called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));

                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
        Assert.That(sent?.Message, Is.EqualTo("failure!"));
    }

    [Test]
    public void WithoutTxn_PostError_Async_AsyncActionSyncCall()
    {
        bool called = false;
        Exception? sent = null;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddErrorAction("f", (e) => { sent = e; called = true; File.Delete(fn); });

                Assert.That(File.Exists(fn));

                await Task.Delay(10);
                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
        Assert.That(sent?.Message, Is.EqualTo("failure!"));
    }

    [Test]
    public void WithoutTxn_PostError_Async_AsyncActionAsyncCall_WrongMethod()
    {
        bool called = false;
        Exception? sent = null;
        Assert.Throws<NotSupportedException>(() => IntegrationTestSetup.ExecWithout(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddErrorAction("f", async (e) => { sent = e; called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));

                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")));
        Assert.That(called, Is.False);
        Assert.That(sent, Is.Null);
    }

    [Test]
    public void WithoutTxn_PostError_Async_2calls()
    {
        bool first_called = false;
        bool first_called_second = false;
        bool second_called = false;
        bool second_called_first = false;
        Exception? sent_e1 = null, sent_e2 = null;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncErrorAction("f", async (e1) =>
                {
                    sent_e1 = e1;
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")), Is.False);
                    first_called = true;
                    if (second_called)
                        first_called_second = true;
                    File.Delete(fn);
                    await Task.Delay(10);
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddAsyncErrorAction("f", async (e2) =>
                {
                    sent_e2 = e2;
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")));
                    second_called = true;
                    if (!first_called)
                        second_called_first = true;
                    File.Delete(fn2);
                    await Task.Delay(10);
                });

                await Task.Delay(10);
                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_second);
        Assert.That(second_called_first);
        Assert.That(sent_e1?.Message, Is.EqualTo("failure!"));
        Assert.That(sent_e2?.Message, Is.EqualTo("failure!"));
    }

    [Test]
    public void WithoutTxn_PostError_Async_2calls_throws()
    {
        bool first_called = false;
        bool first_called_second = false;
        bool second_called = false;
        bool second_called_first = false;
        Exception? sent_e1 = null, sent_e2 = null;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncErrorAction("f", async (e1) =>
                {
                    sent_e1 = e1;
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")), Is.False);
                    first_called = true;
                    if (second_called)
                        first_called_second = true;
                    File.Delete(fn);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddAsyncErrorAction("f", async (e2) =>
                {
                    sent_e2 = e2;
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")));
                    second_called = true;
                    if (!first_called)
                        second_called_first = true;
                    File.Delete(fn2);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                await Task.Delay(10);
                throw new Exception("failure!");
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_second);
        Assert.That(second_called_first);
        Assert.That(sent_e1?.Message, Is.EqualTo("failure!"));
        Assert.That(sent_e2?.Message, Is.EqualTo("failure!"));
    }

    [Test]
    public void WithoutTxn_PostError_Async_2calls_throws_1stThrows()
    {
        bool first_called = false;
        bool first_called_second = false;
        bool second_called = false;
        bool second_called_first = false;
        Exception? sent_e1 = null, sent_e2 = null;
        Assert.ThrowsAsync<Exception>(async () => await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws-1stThrows.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncErrorAction("f", async (e1) =>
                {
                    sent_e1 = e1;
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws-1stThrows.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws-1stThrows.tmp")));
                    first_called = true;
                    if (second_called)
                        first_called_second = true;
                    File.Delete(fn);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws-1stThrows.tmp");
                File.WriteAllText(fn2, "this is a file2");

                if (fn2 != null)
                    throw new Exception("failure!");
                c.AddAsyncErrorAction("f", async (e2) =>
                {
                    sent_e2 = e2;
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws-1stThrows.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws-1stThrows.tmp")));
                    second_called = true;
                    if (!first_called)
                        second_called_first = true;
                    File.Delete(fn2!);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                await Task.Delay(10);
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws-1stThrows.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws-1stThrows.tmp")));
        Assert.That(first_called);
        Assert.That(second_called, Is.False);
        Assert.That(first_called_second, Is.False);
        Assert.That(second_called_first, Is.False);
        Assert.That(sent_e1?.Message, Is.EqualTo("failure!"));
        Assert.That(sent_e2, Is.Null);
    }
    #endregion

#region without txn - Post Close Action
    [Test]
    public async Task WithoutTxn_PostClose_Async_1call()
    {
        bool called = false;
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncPostCloseAction("f", async () => { called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));

                await Task.Delay(10);
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public void WithoutTxn_PostClose_Sync_AsyncAction()
    {
        bool called = false;
        IntegrationTestSetup.ExecWithout(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncPostCloseAction("f", async () => { called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public async Task WithoutTxn_PostClose_Async_AsyncActionSyncCall()
    {
        bool called = false;
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddPostCloseAction("f", () => { called = true; File.Delete(fn); });

                Assert.That(File.Exists(fn));

                await Task.Delay(10);
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")), Is.False);
        Assert.That(called);
    }

    [Test]
    public void WithoutTxn_PostClose_Async_AsyncActionAsyncCall_WrongMethod()
    {
        bool called = false;
        Assert.Throws<NotSupportedException>(() => IntegrationTestSetup.ExecWithout(
            (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddPostCloseAction("f", async () => { called = true; File.Delete(fn); await Task.Delay(10); });

                Assert.That(File.Exists(fn));
            }));

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-1call.tmp")));
        Assert.That(called, Is.False);
    }

    [Test]
    public async Task WithoutTxn_PostClose_Async_2calls()
    {
        bool first_called = false;
        bool first_called_first = false;
        bool second_called = false;
        bool second_called_second = false;
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncPostCloseAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")));
                    first_called = true;
                    if (!second_called)
                        first_called_first = true;
                    File.Delete(fn);
                    await Task.Delay(10);
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddAsyncPostCloseAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")), Is.False);
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")));
                    second_called = true;
                    if (first_called)
                        second_called_second = true;
                    File.Delete(fn2);
                    await Task.Delay(10);
                });

                await Task.Delay(10);
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_first);
        Assert.That(second_called_second);
    }

    [Test]
    public async Task WithoutTxn_PostClose_Async_2calls_throws()
    {
        bool first_called = false;
        bool first_called_first = false;
        bool second_called = false;
        bool second_called_second = false;
        await IntegrationTestSetup.ExecWithoutTxnAsync(
            async (c) =>
            {
                string fn = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp");
                File.WriteAllText(fn, "this is a file");
                c.AddAsyncPostCloseAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")));
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")));
                    first_called = true;
                    if (!second_called)
                        first_called_first = true;
                    File.Delete(fn);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                Assert.That(File.Exists(fn));

                string fn2 = Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp");
                File.WriteAllText(fn2, "this is a file2");
                c.AddAsyncPostCloseAction("f", async () =>
                {
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")), Is.False);
                    Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")));
                    second_called = true;
                    if (first_called)
                        second_called_second = true;
                    File.Delete(fn2);
                    await Task.Delay(10);

                    throw new IOException("failed!");
                });

                await Task.Delay(10);
            });

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-throws.tmp")), Is.False);
        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rasync-2calls-2-throws.tmp")), Is.False);
        Assert.That(first_called);
        Assert.That(second_called);
        Assert.That(first_called_first);
        Assert.That(second_called_second);
    }

    [Test]
    public async Task WithoutTxn_PostClose_Async_Throw()
    {
        bool called = false;
        bool caught = false;
        try
        {
            await IntegrationTestSetup.ExecWithoutTxnAsync(
                async (c) =>
                {
                    await Task.Delay(10);
                    string fn = Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback-throw.tmp");
                    File.WriteAllText(fn, "this is a file");
                    c.AddAsyncPostCloseAction("f", async () => { await Task.Delay(10); called = true; File.Delete(fn); });

                    Assert.That(File.Exists(fn));

                    throw new Exception("failure");
                });
        }
        catch (Exception e)
        {
            caught = true;
            Assert.That(e.Message, Is.EqualTo("failure"));
        }

        Assert.That(File.Exists(Path.Combine(IntegrationTestSetup.Temp, "rsync-rollback-throw.tmp")));
        Assert.That(called, Is.False);
        Assert.That(caught);
    }
    #endregion without txn - Post Close Action

    #region isolation level
    [Test]
    public async Task IsolationLevel_ReadUncommitted()
    {
        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                await c.ExecuteProcAsync("usp_InsertTestRecord", new SqlParameter("@ID", 1), new SqlParameter("@String", "s1"), new SqlParameter("@Int", 10));

                // confirms uncommitted row above can be read with read uncommitted
                await IntegrationTestSetup.GetProvider().ExecuteTxnAsync(
                    async (c2) =>
                    {
                        Assert.That(await c2.ExecuteQueryProcOneAsync(
                            "usp_GetTestRecordByID",
                            (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s1")); return Task.CompletedTask; },
                            new SqlParameter("@ID", 1)));
                    },
                    commandTimeout: 1,
                    isolationLevel: IsolationLevel.ReadUncommitted);
            });
    }

    [Test]
    public async Task IsolationLevel_ReadCommitted()
    {
        await IntegrationTestSetup.ExecAsync(
            async (c) =>
            {
                await c.ExecuteProcAsync("usp_InsertTestRecord", new SqlParameter("@ID", 1), new SqlParameter("@String", "s1"), new SqlParameter("@Int", 10));

                // timeout because read committed cannot read above uncomitted row
                var s_cts = new CancellationTokenSource();
                s_cts.CancelAfter(100);
                var cancellationToken = s_cts.Token;
                Assert.ThrowsAsync<SqlException>(async () => await IntegrationTestSetup.GetProvider().ExecuteTxnAsync(
                    async (c2) =>
                    {
                        Assert.That(await c2.ExecuteQueryProcOneAsync(
                            "usp_GetTestRecordByID",
                            (r) => { Assert.That(r.GetString("String"), Is.EqualTo("s1")); return Task.CompletedTask; },
                            cancellationToken,
                            new SqlParameter("@ID", 1)));
                    },
                    commandTimeout: 1,
                    isolationLevel: IsolationLevel.ReadCommitted));
            });
    }

    [Test]
    public async Task IsolationLevel_Serializable()
    {
        await IntegrationTestSetup.GetProvider().ExecuteWithoutTxnAsync(
            async (c) =>
            {
                await c.ExecuteProcAsync("usp_InsertTestRecord", new SqlParameter("@ID", 1), new SqlParameter("@String", "s1"), new SqlParameter("@Int", 10));
            });

        bool done = false;
        Task? t = null;
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                Assert.That((await c.ExecuteRawQueryOneAsync(
                    "SELECT COUNT(*) AS C FROM Test",
                    (r) => Task.FromResult(r.GetInt("C")))).Value, Is.EqualTo(1));

                t = Task.Run(async () => 
                {
                    await IntegrationTestSetup.GetProvider().ExecuteTxnAsync(
                        async (c2) =>
                        {
                            await c2.ExecuteProcAsync("usp_InsertTestRecord", new SqlParameter("@ID", 2), new SqlParameter("@String", "s2"), new SqlParameter("@Int", 10));
                        },
                        commandTimeout: 5);
                    done = true;
                });

                // insert txn should be blocked and we still get one row
                await Task.Delay(100);

                Assert.That((await c.ExecuteRawQueryOneAsync(
                    "SELECT COUNT(*) AS C FROM Test",
                    (r) => Task.FromResult(r.GetInt("C")))).Value, Is.EqualTo(1));
            },
            isolationLevel: IsolationLevel.Serializable,
            commandTimeout: 1);

        while (!done)
            await Task.Delay(10);

        // now insert txn is complete, we have 2 rows
        await IntegrationTestSetup.GetProvider().ExecuteRawTxnAsync(
            async (c) =>
            {
                Assert.That((await c.ExecuteRawQueryOneAsync(
                    "SELECT COUNT(*) AS C FROM Test",
                    (r) => Task.FromResult(r.GetInt("C")))).Value, Is.EqualTo(2));
            });
    }
    #endregion isolation level
}
