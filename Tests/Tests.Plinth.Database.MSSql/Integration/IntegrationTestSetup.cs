using Dapper;
using Integration.Utilities.Container;
using Integration.Utilities.Database;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Plinth.Database.MSSql;
using System.Diagnostics;
using System.Net.Sockets;
using System.Reflection;
using System.Text.RegularExpressions;
using NUnit.Framework;

namespace Tests.Plinth.Database.MSSql.Integration;

internal static class IntegrationTestSetup
{
    private static readonly ILogger logger = StaticLogManager.GetLogger();

    private static readonly string DbHost = DockerUtil.ContainerHost;
    private static readonly string DbConnection =
        $@"Data Source=tcp:{DbHost},1434;Initial Catalog={{0}};User ID={{1}};Password={{2}};Persist Security Info=True;MultipleActiveResultSets=False;TrustServerCertificate=True";

    private static SqlTransactionFactory? _txnF = null;
    private static string? _dbName = null;

    public static string Dot = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!, "Integration");
    public static string Temp = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!;

    public const string DatabaseUser = "TestUser";
    public const string DatabasePass = "TestPassword123$";

    private const string DatabaseSaUser = "sa";
    private const string DatabaseSaPass = "sasasa123!";

    public static ISqlTransactionProvider GetProvider() => _txnF!.GetDefault();

    public static void Exec(Action<IRawSqlConnection> action)
    {
        _txnF!.GetDefault().ExecuteRawTxn(action);
    }

    public static void ExecWithout(Action<INoTxnRawSqlConnection> action)
    {
        _txnF!.GetDefault().ExecuteRawWithoutTxn(action);
    }

    public static async Task ExecAsync(Func<IRawSqlConnection, Task> action)
    {
        await _txnF!.GetDefault().ExecuteRawTxnAsync(action);
    }

    public static async Task ExecWithoutTxnAsync(Func<INoTxnRawSqlConnection, Task> action)
    {
        await _txnF!.GetDefault().ExecuteRawWithoutTxnAsync(action);
    }

    public static void SetUpTest()
    {
        _dbName = "PL" + Guid.NewGuid().ToString("N");

        TestContext.Progress.WriteLine("creating db");
        var cstr = string.Format(DbConnection, "master", DatabaseSaUser, DatabaseSaPass);
        SqlServerDatabase.CreateDB(cstr, _dbName, DatabaseUser, DatabasePass);

        _txnF = new SqlTransactionFactory(
                _dbName,
                string.Format(DbConnection, _dbName, DatabaseUser, DatabasePass),
                30, 3, 200, true);

        TestContext.Progress.WriteLine("seeding db");
        using (logger.LogExecTiming("Seeding Database"))
        {
            _txnF.GetDefault().ExecuteRawTxn(
                (c) =>
                {
                    c.ExecuteRaw(File.ReadAllText(Path.Combine(Dot, "Schema/Tables.sql")));

                    var text = File.ReadAllText(Path.Combine(Dot, "Schema/Procedures.sql"));
                    foreach (var s in Regex.Split(text, @"^GO\s*$", RegexOptions.Multiline))
                    {
                        if (string.IsNullOrWhiteSpace(s))
                            continue;
                        c.ExecuteRaw(s);
                    }
                });
        }
        TestContext.Progress.WriteLine("ready for test");
    }

    public static void CleanUpTest()
    {
        _txnF = null;
        GC.Collect();

        var cstr = string.Format(DbConnection, "master", DatabaseSaUser, DatabaseSaPass);
        SqlServerDatabase.DeleteDatabase(cstr, _dbName!);
    }

    public static async Task StartUpDatabase()
    {
        TestContext.Progress.WriteLine("starting db");
        await DockerCompose.Up(1434);
        TestContext.Progress.WriteLine("db is ready");
    }

    public static async Task CleanUpDatabase()
    {
        await SqlServerDatabase.WaitForDeletes();
        await DockerCompose.Down();
    }
}
