GO

CREATE PROCEDURE dbo.usp_InsertDateTime
@ID BIGINT,
@DT1 DATETIME = NULL,
@DT2 DATETIME2(3) = NULL
AS
BEGIN
    SET NOCOUNT OFF;

    INSERT INTO dbo.DateTime2Test (
		ID,
		DT1,
		DT2
    )
    VALUES (
		@ID,
        @DT1,
        @DT2
	)
END;
GO

CREATE PROCEDURE dbo.usp_UpdateDateTime
@ID BIGINT,
@DT1 DATETIME = NULL,
@DT2 DATETIME2(3) = NULL
AS
BEGIN
    SET NOCOUNT OFF;

    UPDATE dbo.DateTime2Test SET
        DT1 = ISNULL(@DT1, b.DT1),
        DT2 = ISNULL(@DT2, b.DT2)
        FROM dbo.DateTime2Test AS b
    WHERE b.ID = @ID;
END;
GO

CREATE PROCEDURE dbo.usp_GetDateTimeByID
@ID BIGINT
AS
BEGIN
    SET NOCOUNT ON;

    SELECT
        b.ID,
        b.DT1,
        b.DT2
        FROM dbo.DateTime2Test AS b
    WHERE b.ID = @ID;
END;
GO

CREATE PROCEDURE dbo.usp_InsertTestRecord
@ID BIGINT,
@Date DATETIME2 = NULL,
@String NVARCHAR(255) = NULL,
@Int INT = NULL,
@Money DECIMAL(5,3) = NULL,
@Blob VARBINARY(MAX) = NULL
AS
BEGIN
    SET NOCOUNT OFF;

    INSERT INTO dbo.Test (
		ID,
		DT,
		String,
		IntField,
		MoneyField,
		BlobField
    )
    VALUES (
		@ID,
        @Date,
        @String,
		@Int,
		@Money,
		@Blob
	)
END;
GO

CREATE PROCEDURE dbo.usp_UpdateTestRecord
@ID BIGINT,
@Date DATETIME2 = NULL,
@String NVARCHAR(255) = NULL,
@Int INT = NULL,
@Money DECIMAL(5,3) = NULL,
@Blob VARBINARY(MAX) = NULL
AS
BEGIN
    SET NOCOUNT OFF;

    UPDATE dbo.Test SET
        DT = ISNULL(@Date, b.DT),
        String = ISNULL(@String, b.String),
        IntField = ISNULL(@Int, b.IntField),
        MoneyField = ISNULL(@Money, b.MoneyField),
        BlobField = ISNULL(@Blob, b.BlobField)
        FROM dbo.Test AS b
    WHERE b.ID = @ID;
END;
GO

CREATE PROCEDURE dbo.usp_GetTestRecordByID
@ID BIGINT
AS
BEGIN
    SET NOCOUNT ON;

    SELECT
        b.ID,
		b.DT,
		b.String,
		b.IntField,
		b.MoneyField,
		b.BlobField
        FROM dbo.Test AS b
    WHERE b.ID = @ID;
END;
GO

CREATE PROCEDURE dbo.usp_GetTestRecordByInt
@Int INT
AS
BEGIN
    SET NOCOUNT ON;

    SELECT
        b.ID,
		b.DT,
		b.String,
		b.IntField,
		b.MoneyField,
		b.BlobField
        FROM dbo.Test AS b
    WHERE b.IntField = @Int
	ORDER BY b.ID ASC
END;
GO

CREATE PROCEDURE dbo.usp_GetTestRecordsByInts
@Int1 INT,
@Int2 INT,
@Int3 INT
AS
BEGIN
    SET NOCOUNT ON;

    SELECT
        b.ID,
		b.DT,
		b.String,
		b.IntField,
		b.MoneyField,
		b.BlobField,
		1 AS Num1
        FROM dbo.Test AS b
    WHERE b.IntField = @Int1
	ORDER BY b.ID ASC

    SELECT
        b.ID,
		b.DT,
		b.String,
		b.IntField,
		b.MoneyField,
		b.BlobField,
		2 AS Num2
        FROM dbo.Test AS b
    WHERE b.IntField = @Int2
	ORDER BY b.ID ASC

    SELECT
        b.ID,
		b.DT,
		b.String,
		b.IntField,
		b.MoneyField,
		b.BlobField,
		3 AS Num3
        FROM dbo.Test AS b
    WHERE b.IntField = @Int3
	ORDER BY b.ID ASC
END;
GO


CREATE PROCEDURE dbo.usp_EchoTableType
@Table TableTypeTest READONLY
AS
BEGIN
    SET NOCOUNT ON;

    SELECT * FROM @Table;
END;
GO

CREATE PROCEDURE dbo.usp_EchoNVARCHARList
@Table NVARCHARList READONLY
AS
BEGIN
    SET NOCOUNT ON;

    SELECT * FROM @Table;
END;
GO

CREATE PROCEDURE dbo.usp_EchoBIT
@Bit BIT
AS
BEGIN
    SET NOCOUNT ON;

    SELECT @Bit AS 'Bit';
END;
GO
