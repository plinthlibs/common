using Plinth.Database.MSSql.Impl;
using Plinth.Database.MSSql;
using NUnit.Framework;
using Microsoft.Data.SqlClient;

namespace Tests.Plinth.Database.MSSql.Impl;

[TestFixture]
public class StoredProcLoggerTest
{
    [Test]
    public void Test_ExecToString()
    {
        int? nullInt = null;
        Assert.That(StoredProcLogger.ExecToString("usp_DoThis", [new SqlParameter("@Param1", 5),
                                                        null,
                                                        new SqlParameter("@Param2", "abc"),
                                                        new SqlParameter("@Param3", nullInt),
                                                        new SqlParameter("@Param4", null)]), Is.EqualTo("EXECUTE usp_DoThis @Param1 = 5, @Param2 = 'abc'"));
    }

    [Test]
    public void Test_ExecToString_Nulls()
    {
        int? nullInt = null;
        Assert.That(StoredProcLogger.ExecToString("usp_DoThis", new[] {new SqlParameter("@Param1", 5),
                                                        new SqlParameter("@Param3", nullInt),
                                                        new SqlParameter("@Param4", null),
                                                        new SqlParameter("@Param5", DBNull.Value),
                                                        new SqlParameter(SqlConstants.NullableHeader + "@Param6", DBNull.Value),
                                                        new SqlParameter(SqlConstants.NullableHeader + "@Param7", null),
                                                        new SqlParameter(SqlConstants.NullableHeader + "@Param8", nullInt)
                                                        }.Select(p => { SqlParameterProcessor.ProcessParam(p); return p; }).ToArray()
                                        ), Is.EqualTo("EXECUTE usp_DoThis @Param1 = 5, @Param5 = NULL, @Param6 = NULL, @Param7 = NULL, @Param8 = NULL"));
    }

    [Test]
    public void Test_ExecToString_Guid()
    {
        Assert.That(StoredProcLogger.ExecToString("usp_DoThis", [new SqlParameter("@Param1", new Guid("f75da73e-fb3f-4867-b27c-183e241a0e1e"))]), Is.EqualTo("EXECUTE usp_DoThis @Param1 = 'f75da73e-fb3f-4867-b27c-183e241a0e1e'"));
    }

    [Test]
    public void Test_RawExecToString()
    {
        int? nullInt = null;
        Assert.That(StoredProcLogger.RawExecToString("UPDATE table SET P1=@Param1, P2=@Param2, P3=@Param3, P4=@Param4",
                [
                    new SqlParameter("@Param1", 5),
                    new SqlParameter("@Param2", "abc"),
                    new SqlParameter("@Param3", nullInt),
                    new SqlParameter("@Param4", null)]), Is.EqualTo("UPDATE table SET P1=@Param1, P2=@Param2, P3=@Param3, P4=@Param4 ==> VALUES (@Param1 = 5, @Param2 = 'abc', @Param3 = NULL, @Param4 = NULL)"));
    }

    [Test]
    public void Test_RawExecToString_Nulls()
    {
        int? nullInt = null;
        Assert.That(StoredProcLogger.RawExecToString("UPDATE table SET P1=@Param1, P3=@Param3, P4=@Param4",
                [
                    new SqlParameter("@Param1", 5),
                    null,
                    new SqlParameter("@Param3", nullInt),
                    new SqlParameter("@Param4", null)]), Is.EqualTo("UPDATE table SET P1=@Param1, P3=@Param3, P4=@Param4 ==> VALUES (@Param1 = 5, @Param3 = NULL, @Param4 = NULL)"));
    }

    [Test]
    public void Test_RawExecToString_NoValue()
    {
        Assert.That(StoredProcLogger.RawExecToString("SELECT * FROM TABLE", []), Is.EqualTo("SELECT * FROM TABLE"));
    }
}
