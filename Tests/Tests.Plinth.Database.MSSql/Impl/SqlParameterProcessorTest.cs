﻿using Plinth.Database.MSSql.Impl;
using NUnit.Framework;
using System.Data;
using Microsoft.Data.SqlClient;
using Plinth.Database.MSSql;

namespace Tests.Plinth.Database.MSSql.Impl;

[TestFixture]
public class SqlParameterProcessorTest
{
    [Test]
    public void Test_null()
    {
        string? s = null;
        var p = new SqlParameter("@Null", s);
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.Value, Is.Null);

        p = new SqlParameter("@Null", DBNull.Value);
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.Value, Is.EqualTo(DBNull.Value));
    }

    [Test]
    public void Test_DateTime_LocalToUtc()
    {
        var dt = new DateTime(2015, 02, 15, 14, 05, 01, DateTimeKind.Local);
        var p = new SqlParameter("@DateTime", dt);
        SqlParameterProcessor.ProcessParam(p);

        Assert.That((DateTime)p.Value, Is.EqualTo(new DateTime(2015, 02, 15, 14, 05, 01, DateTimeKind.Local).ToUniversalTime()));
        Assert.That(p.SqlDbType, Is.EqualTo(SqlDbType.DateTime2));
    }

    [Test]
    public void Test_DateTime_UtcToUtc()
    {
        var dt = new DateTime(2015, 02, 15, 14, 05, 01, DateTimeKind.Utc);
        var p = new SqlParameter("@DateTime", dt);
        SqlParameterProcessor.ProcessParam(p);

        Assert.That((DateTime)p.Value, Is.EqualTo(new DateTime(2015, 02, 15, 14, 05, 01, DateTimeKind.Utc)));
        Assert.That(p.SqlDbType, Is.EqualTo(SqlDbType.DateTime2));
    }

    [Test]
    public void Test_DateTime_Utc_NoUtc()
    {
        var dt = new DateTime(2015, 02, 15, 14, 05, 01, DateTimeKind.Utc);
        var p = new SqlParameter(SqlConstants.NoUtcDateTimeHeader + "@DateTime", dt);
        SqlParameterProcessor.ProcessParam(p);

        Assert.That((DateTime)p.Value, Is.EqualTo(new DateTime(2015, 02, 15, 14, 05, 01, DateTimeKind.Utc)));
        Assert.That(p.SqlDbType, Is.EqualTo(SqlDbType.DateTime2));
    }

    [Test]
    public void Test_DateTime_Local_NoUtc()
    {
        var dt = new DateTime(2015, 02, 15, 14, 05, 01, DateTimeKind.Local);
        var p = new SqlParameter(SqlConstants.NoUtcDateTimeHeader + "@DateTime", dt);
        SqlParameterProcessor.ProcessParam(p);

        Assert.That((DateTime)p.Value, Is.EqualTo(new DateTime(2015, 02, 15, 14, 05, 01, DateTimeKind.Local)));
        Assert.That(p.SqlDbType, Is.EqualTo(SqlDbType.DateTime2));
    }

    private enum TestEnum
    {
        Value1,
        Value2
    }

    [Test]
    public void Test_Enum()
    {
        var p = new SqlParameter("@Enum1", TestEnum.Value1);
        SqlParameterProcessor.ProcessParam(p);

        Assert.That(p.Value, Is.EqualTo("Value1"));

        p = new SqlParameter("@Enum2", TestEnum.Value2.ToString());
        SqlParameterProcessor.ProcessParam(p);

        Assert.That(p.Value, Is.EqualTo("Value2"));
    }

    [Test]
    public void Test_Trim()
    {
        var p = new SqlParameter("@String1", "Test");
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.Value, Is.EqualTo("Test"));

        p = new SqlParameter("@String1", "Test   ");
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.Value, Is.EqualTo("Test"));

        p = new SqlParameter("@String1", "   Test");
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.Value, Is.EqualTo("Test"));

        p = new SqlParameter("@String1", " Test ");
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.Value, Is.EqualTo("Test"));

        p = new SqlParameter("@String1", "\tTest \t");
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.Value, Is.EqualTo("Test"));
    }

    [Test]
    public void Test_NoTrim()
    {
        var p = new SqlParameter(SqlConstants.DoNotTrimHeader + "@String1", "Test");
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.ParameterName, Is.EqualTo("@String1"));
        Assert.That(p.Value, Is.EqualTo("Test"));

        p = new SqlParameter(SqlConstants.DoNotTrimHeader + "@String1", "  Test  ");
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.ParameterName, Is.EqualTo("@String1"));
        Assert.That(p.Value, Is.EqualTo("  Test  "));

        p = new SqlParameter(SqlConstants.DoNotTrimHeader + "@String1", null);
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.ParameterName, Is.EqualTo("@String1"));
        Assert.That(p.Value, Is.Null);
    }

    [Test]
    public void Test_ForceNull()
    {
        var p = new SqlParameter(SqlConstants.NullableHeader + "@String1", "Test");
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.ParameterName, Is.EqualTo("@String1"));
        Assert.That(p.Value, Is.EqualTo("Test"));

        p = new SqlParameter(SqlConstants.NullableHeader + "@String1", null);
        SqlParameterProcessor.ProcessParam(p);
        Assert.That(p.ParameterName, Is.EqualTo("@String1"));
        Assert.That(p.Value, Is.EqualTo(DBNull.Value));
    }
}
