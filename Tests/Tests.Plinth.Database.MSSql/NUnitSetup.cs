using Plinth.Logging.Host;
using NUnit.Framework;

namespace Tests.Plinth.Database.MSSql;

[SetUpFixture]
public class NUnitSetup
{
    [OneTimeSetUp]
    public void Setup()
    {
        StaticLogManagerSetup.ConfigureForDebugLogging();
    }
}
