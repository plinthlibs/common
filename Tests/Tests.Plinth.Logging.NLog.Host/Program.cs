using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Plinth.Logging.NLog;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Plinth.Common.Utils;

[assembly: NUnit.Framework.NonTestAssembly]

namespace Tests.Plinth.Logging.NLog.Host;

class Program
{
    static void Main(string[] args)
    {
        var log = StaticLogManagerSetup.BasicNLogSetup();

        log.Trace("shown?");
        log.Debug("debug info");
        log.Info("Hello NLog! -- dotnet {DotNetVersion} on {Machine}", Environment.Version, Environment.MachineName);
        log.Warn("could be bad");
        log.Error("is bad");
        log.Fatal("really bad");

        using (log.LogExecTiming("process"))
        {
            log.Info("doing stuff");
            Thread.Sleep(100);
        }

        // this simulates logging from asp.net core code, and code that injects loggers
        var config = new ConfigurationBuilder()
           .SetBasePath(RuntimeUtil.FindExecutableHome())
           .AddJsonFile("appsettings.json", optional: true, reloadOnChange: false)
           .Build();

        var host = Microsoft.Extensions.Hosting.Host.CreateDefaultBuilder(args)
            .ConfigureAppConfiguration(c =>
            {
                c.AddConfiguration(config);
            })
            .ConfigureServices(s =>
            {
                s.AddNLog();
            });

        var sp = host.Build().Services;

        var fac = sp.GetRequiredService<ILoggerFactory>();
        var localLogger = fac.CreateLogger<Program>();
        var msLogger = fac.CreateLogger("Microsoft.AspNetCore");

        localLogger.Info("info from local");
        msLogger.Info("info from ms");

        try
        {
            throw new Exception("test");
        }
        catch (Exception e)
        {
            log.Warn(e, "caught");
        }
    }
}
