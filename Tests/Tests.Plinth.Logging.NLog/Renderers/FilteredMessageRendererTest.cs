using Plinth.Logging.NLog.Renderers;
using NLog;
using NUnit.Framework;
using System.Text;

namespace Tests.Plinth.Logging.NLog.Renderers;

[TestFixture]
public class FilteredMessageRendererTest
{
    private class Renderer : FilteredMessageRenderer
    {
        public string RunIt(LogEventInfo e)
        {
            var sb = new StringBuilder();
            base.Append(sb, e);
            return sb.ToString();
        }
    }

    [Test]
    public void Test_NoFilter()
    {
        LogEventInfo lei = LogEventInfo.Create(LogLevel.Debug, "logger",
            "{ \"borrower\": \"buddy\", \"phone\": \"123-435-6678\" }");

        var r = new Renderer();
        Assert.That(r.RunIt(lei), Is.EqualTo("{ \"borrower\": \"buddy\", \"phone\": \"123-435-6678\" }"));
    }

    [Test]
    public void Test_HasSsn()
    {
        LogEventInfo lei = LogEventInfo.Create(LogLevel.Debug, "logger",
            "{ \"borrower\": \"buddy\", \"ssn\": \"123-45-6678\" }");

        var r = new Renderer();
        Assert.That(r.RunIt(lei), Is.EqualTo("{ \"borrower\": \"buddy\", \"ssn\": \"_REDACTED_\" }"));
    }

    [Test]
    public void Test_HasPassword()
    {
        LogEventInfo lei = LogEventInfo.Create(LogLevel.Debug, "logger",
            "{ \"borrower\": \"buddy\", \"password\": \"abc123\" }");

        var r = new Renderer();
        Assert.That(r.RunIt(lei), Is.EqualTo("{ \"borrower\": \"buddy\", \"password\": \"_REDACTED_\" }"));
    }

    [Test]
    public void Test_HasPrefixedPassword()
    {
        LogEventInfo lei = LogEventInfo.Create(LogLevel.Debug, "logger",
            "{ \"borrower\": \"buddy\", \"newPassword\": \"abc123\" }");

        var r = new Renderer();
        Assert.That(r.RunIt(lei), Is.EqualTo("{ \"borrower\": \"buddy\", \"newPassword\": \"_REDACTED_\" }"));
    }

    [Test]
    public void Test_HasSsnCase()
    {
        LogEventInfo lei = LogEventInfo.Create(LogLevel.Debug, "logger",
            "{ \"borrower\": \"buddy\", \"Ssn\":\"123456789\" }");

        var r = new Renderer();
        Assert.That(r.RunIt(lei), Is.EqualTo("{ \"borrower\": \"buddy\", \"Ssn\":\"_REDACTED_\" }"));
    }

    [Test]
    public void Test_BigString()
    {
        string bigStr = "123" + string.Join("", Enumerable.Repeat('A', 100000));

        LogEventInfo lei = LogEventInfo.Create(LogLevel.Debug, "logger", bigStr);

        var r = new Renderer();
        Assert.That(r.RunIt(lei), Is.EqualTo(bigStr[..25000] + "<truncated>"));
    }
}
