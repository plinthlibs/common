using Plinth.Logging.NLog.Renderers;
using NLog;
using NUnit.Framework;

namespace Tests.Plinth.Logging.NLog.Renderers;

[TestFixture]
public class AssemblyVersionRendererTest
{
    [Test]
    public void Test_AssemblyRenderer()
    {
        var lf = new AssemblyVersionRenderer();

        Assert.That(lf.Render(new LogEventInfo()), Does.Match(@"^(\d+\.){1,3}\d+$"));
    }

    [Test]
    public void Test_AssemblyRendererWithAssembly()
    {
        var lf1 = new AssemblyVersionRenderer();
        var myVer = lf1.Render(new LogEventInfo());

        var lf = new AssemblyVersionRenderer
        {
            AssemblyName = "Microsoft.Extensions.Logging"
        };
        var clVer = lf.Render(new LogEventInfo());

        Assert.That(clVer, Does.Match(@"^(\d+\.){1,3}\d+$"));
        Assert.That(myVer, Is.Not.EqualTo(clVer));
    }
}
