using Plinth.Logging.NLog.Renderers;
using NLog;
using NUnit.Framework;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;

namespace Tests.Plinth.Logging.NLog.Renderers;

[TestFixture]
public class ExceptionRendererTest
{
    private static readonly bool IsWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

    private const string AnyFileRegexWin = @"([a-zA-Z]:[^:]+)";
    private const string AnyFileRegexLinux = @"(/[^/].+)";
    private const string ThisFileRegex = @"Tests.Plinth.Logging.NLog.Renderers.ExceptionRendererTest.cs:\d+";

    [SetUp]
    public void SetUp()
    {
        LogManager.Configuration ??= new global::NLog.Config.LoggingConfiguration();
    }

    [TearDown]
    public void TearDown()
    {
        LogManager.Configuration = null;
    }

    [Test]
    public void Test_NoStackTrace()
    {
        var er = new ExceptionRenderer();

        var e = new Exception("My Message");

        var result = er.Render(new LogEventInfo { Exception = e });
        var lines = result.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

        Assert.Multiple(() =>
        {
            Assert.That(lines, Has.Length.EqualTo(2));
            Assert.That(Regex.IsMatch(lines[0], @"^Exception in thread ""[^""]+"" System.Exception: My Message$"), lines[0]);
            Assert.That(Regex.IsMatch(lines[1], @"^\s+\[No stack trace present\]$"), lines[1]);
        });
    }

    [Test]
    [TestCase(null, AnyFileRegexWin, AnyFileRegexLinux)]
    [TestCase("Tests.Plinth", "")]
    [TestCase("Tests.Plinth.", "")]
    [TestCase(" Tests.Plinth. ", "")]
    [TestCase(" Tests.Plinth ", "")]
    [TestCase(" Tests.Plin ", "")]
    [TestCase("NotPlinth", AnyFileRegexWin, AnyFileRegexLinux)]
    [TestCase(" Tests.Plinth , Something.Else.", "")]
    [TestCase("Something.Else., Tests.Plinth ", "")]
    public void Test_Exception(string? prefix, string fnRegex, string? fnRegexLinux = null)
    {
        var er = prefix == null ? new ExceptionRenderer() : new ExceptionRenderer() { NamespacePrefixes = prefix };

        if (!IsWindows && fnRegexLinux != null)
            fnRegex = fnRegexLinux;

        try
        {
            throw new Exception("My Message");
        }
        catch (Exception e)
        {
            var result = er.Render(new LogEventInfo { Exception = e });
            var lines = result.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            Assert.That(lines, Has.Length.EqualTo(2));
            Assert.That(Regex.IsMatch(lines[0], @"^Exception in thread ""[^""]+"" System.Exception: My Message$"));
            Assert.That(Regex.IsMatch(lines[1], @"^\s+at Tests.Plinth.Logging.NLog.Renderers.ExceptionRendererTest.Test_Exception\(" + fnRegex + ThisFileRegex + @"\)$"), lines[1]);
        }
    }

    [Test]
    [TestCase(null, AnyFileRegexWin, AnyFileRegexLinux)]
    [TestCase("Tests.Plinth", "")]
    [TestCase("Tests.Plinth.", "")]
    [TestCase(" Tests.Plinth. ", "")]
    [TestCase(" Tests.Plinth ", "")]
    [TestCase(" Tests.Plin ", "")]
    [TestCase("NotPlinth", AnyFileRegexWin, AnyFileRegexLinux)]
    [TestCase(" Tests.Plinth , Something.Else.", "")]
    [TestCase("Something.Else., Tests.Plinth ", "")]
    public void Test_InnerException(string? prefix, string fnRegex, string? fnRegLinux = null)
    {
        var er = prefix == null ? new ExceptionRenderer() : new ExceptionRenderer() { NamespacePrefixes = prefix };

        if (!IsWindows && fnRegLinux != null)
            fnRegex = fnRegLinux;

        try
        {
            throw new Exception("My Message");
        }
        catch (Exception e)
        {
            try
            {
                throw new Exception("Second Info", e);
            }
            catch (Exception e2)
            {
                try
                {
                    throw new Exception("Third Info", e2);
                }
                catch (Exception e3)
                {
                    try
                    {
                        throw new Exception("Fourth Info", e3);
                    }
                    catch (Exception e4)
                    {
                        var result = er.Render(new LogEventInfo { Exception = e4 });
                        var lines = result.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                        Assert.That(lines, Has.Length.EqualTo(8));
                        Assert.That(Regex.IsMatch(lines[0], @"^Exception in thread ""[^""]+"" System.Exception: Fourth Info$"));
                        Assert.That(Regex.IsMatch(lines[1], @"^\s+at Tests.Plinth.Logging.NLog.Renderers.ExceptionRendererTest.Test_InnerException\(" + fnRegex + ThisFileRegex + @"\)$"), lines[1]);
                        Assert.That(Regex.IsMatch(lines[2], @"^Caused by: System.Exception: Third Info"));
                        Assert.That(Regex.IsMatch(lines[3], @"^\s+at Tests.Plinth.Logging.NLog.Renderers.ExceptionRendererTest.Test_InnerException\(" + fnRegex + ThisFileRegex + @"\)$"));
                        Assert.That(Regex.IsMatch(lines[4], @"^Caused by: System.Exception: Second Info$"));
                        Assert.That(Regex.IsMatch(lines[5], @"^\s+at Tests.Plinth.Logging.NLog.Renderers.ExceptionRendererTest.Test_InnerException\(" + fnRegex + ThisFileRegex + @"\)$"));
                        Assert.That(Regex.IsMatch(lines[6], @"^Caused by: System.Exception: My Message$"));
                        Assert.That(Regex.IsMatch(lines[7], @"^\s+at Tests.Plinth.Logging.NLog.Renderers.ExceptionRendererTest.Test_InnerException\(" + fnRegex + ThisFileRegex + @"\)$"));
                    }
                }
            }
        }
    }

    [Test]
    [TestCase(null, AnyFileRegexWin, AnyFileRegexLinux)]
    [TestCase("Tests.Plinth", "")]
    [TestCase("Tests.Plinth.", "")]
    [TestCase(" Tests.Plinth. ", "")]
    [TestCase(" Tests.Plinth ", "")]
    [TestCase(" Tests.Plin ", "")]
    [TestCase("NotPlinth", AnyFileRegexWin, AnyFileRegexLinux)]
    [TestCase(" Tests.Plinth , Something.Else.", "")]
    [TestCase("Something.Else., Tests.Plinth ", "")]
    public void Test_AggregateException(string? prefix, string fnRegex, string? fnRegexLinux = null)
    {
        var er = prefix == null ? new ExceptionRenderer() : new ExceptionRenderer() { NamespacePrefixes = prefix };

        if (!IsWindows && fnRegexLinux != null)
            fnRegex = fnRegexLinux;

        try
        {
            throw new Exception("My Message");
        }
        catch (Exception e)
        {
            try
            {
                throw new Exception("Second Info");
            }
            catch (Exception e2)
            {
                try
                {
                    throw new AggregateException(e, e2);
                }
                catch (AggregateException ae)
                {
                    var result = er.Render(new LogEventInfo { Exception = ae });
                    var lines = result.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                    Assert.That(lines, Has.Length.EqualTo(6));
                    Assert.That(Regex.IsMatch(lines[0], @"^Exception in thread ""[^""]+"" System.AggregateException: One or more errors occurred. \(My Message\) \(Second Info\)$"));
                    Assert.That(Regex.IsMatch(lines[1], @"^\s+at Tests.Plinth.Logging.NLog.Renderers.ExceptionRendererTest.Test_AggregateException\(" + fnRegex + ThisFileRegex + @"\)$"), () => lines[1]);
                    Assert.That(Regex.IsMatch(lines[2], @"^Aggregated: Exception in thread ""[^""]+"" System.Exception: My Message$"));
                    Assert.That(Regex.IsMatch(lines[3], @"^\s+at Tests.Plinth.Logging.NLog.Renderers.ExceptionRendererTest.Test_AggregateException\(" + fnRegex + ThisFileRegex + @"\)$"));
                    Assert.That(Regex.IsMatch(lines[4], @"^Aggregated: Exception in thread ""[^""]+"" System.Exception: Second Info$"));
                    Assert.That(Regex.IsMatch(lines[5], @"^\s+at Tests.Plinth.Logging.NLog.Renderers.ExceptionRendererTest.Test_AggregateException\(" + fnRegex + ThisFileRegex + @"\)$"));
                }
            }
        }
    }

    [Test]
    public void Test_NoException()
    {
        var er = new ExceptionRenderer();

        var result = er.Render(new LogEventInfo());

        Assert.That(result, Is.EqualTo(""));
    }

    [Test]
    public void Test_MaxFrames()
    {
        var er = new ExceptionRenderer();

        var reg = IsWindows ? "[a-zA-Z]:[^:]+" : "/[^/].+/";

        try
        {
            er.MaxFrames = 3;
            Recurse(5);
        }
        catch (Exception e)
        {
            var result = er.Render(new LogEventInfo { Exception = e });
            var lines = result.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            Assert.That(lines, Has.Length.EqualTo(5));
            Assert.That(Regex.IsMatch(lines[0], @"^Exception in thread ""[^""]+"" System.Exception: My Message$"), lines[0]);
            Assert.That(Regex.IsMatch(lines[1], @"^\s+at Tests.Plinth.Logging.NLog.Renderers.ExceptionRendererTest.Recurse\("+reg+@"ExceptionRendererTest.cs:\d+\)$"), lines[1]);
            Assert.That(Regex.IsMatch(lines[2], @"^\s+at Tests.Plinth.Logging.NLog.Renderers.ExceptionRendererTest.Recurse\("+reg+@"ExceptionRendererTest.cs:\d+\)$"), lines[2]);
            Assert.That(Regex.IsMatch(lines[3], @"^\s+at Tests.Plinth.Logging.NLog.Renderers.ExceptionRendererTest.Recurse\("+reg+@"ExceptionRendererTest.cs:\d+\)$"), lines[3]);
            Assert.That(Regex.IsMatch(lines[4], @"^\s+... 4 more$"), lines[4]);
        }
    }

    private void Recurse(int depth)
    {
        if (depth > 0)
            Recurse(depth - 1);

        throw new Exception("My Message");
    }

    [Test]
    [TestCase(null, AnyFileRegexWin, AnyFileRegexLinux)]
    [TestCase("Tests.Plinth", AnyFileRegexWin, AnyFileRegexLinux)]
    public void Test_Exception_PrefixVariable(string? prefix, string fnRegex, string fnRegLinux)
    {
        if (prefix != null)
            LogManager.Configuration.Variables["exceptionRootDirPrefixes"] = new global::NLog.Layouts.SimpleLayout(prefix);
        var er = new ExceptionRenderer();

        if (!IsWindows && fnRegLinux != null)
            fnRegex = fnRegLinux;

        try
        {
            throw new Exception("My Message");
        }
        catch (Exception e)
        {
            var result = er.Render(new LogEventInfo { Exception = e });
            var lines = result.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            Assert.That(lines, Has.Length.EqualTo(2));
            Assert.That(Regex.IsMatch(lines[0], @"^Exception in thread ""[^""]+"" System.Exception: My Message$"), lines[0]);
            Assert.That(Regex.IsMatch(lines[1], @"^\s+at Tests.Plinth.Logging.NLog.Renderers.ExceptionRendererTest.Test_Exception_PrefixVariable\(" + fnRegex + ThisFileRegex + @"\)$"), lines[1]);
        }
    }
}
