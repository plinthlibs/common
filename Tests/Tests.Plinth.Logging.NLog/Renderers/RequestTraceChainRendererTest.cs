using Plinth.Logging.NLog.Renderers;
using NLog;
using NUnit.Framework;
using System.Text;
using Plinth.Common.Logging;

namespace Tests.Plinth.Logging.NLog.Renderers;

[TestFixture]
public class RequestTraceChainRenderTest
{
    private class Child : RequestTraceChainRenderer
    {
        public void CallAppend(StringBuilder sb, LogEventInfo le)
        {
            base.Append(sb, le);
        }
    }

    [Test]
    public void Test_Render()
    {
        var tw = new Child();

        RequestTraceChain.Instance.Create();
        var sb = new StringBuilder();
        tw.CallAppend(sb, new LogEventInfo());
        Assert.That(sb.ToString(), Is.EqualTo("0"));

        RequestTraceChain.Instance.Set("123");
        sb = new StringBuilder();
        tw.CallAppend(sb, new LogEventInfo());
        Assert.That(sb.ToString(), Is.EqualTo("123"));
    }

    [Test]
    public void Test_Render_Null()
    {
        var sb = new StringBuilder();

        using (ExecutionContext.SuppressFlow())
        {
            var t = new Thread(() =>
            {
                var tw = new Child();
                tw.CallAppend(sb, new LogEventInfo());
            });
            t.Start();
            t.Join();
        }

        Assert.That(sb.ToString(), Is.Empty);
    }

    [Test]
    public void Test_Render_Null_MetadataBackup()
    {
        var sb = new StringBuilder();
        string chain = RequestTraceChain.Instance.Create();
        string? inner = null;

        using (ExecutionContext.SuppressFlow())
        {
            var t = new Thread(() =>
            {
                inner = RequestTraceChain.Instance.Get();
                var tw = new Child();
                var lei = new LogEventInfo();
                lei.Properties[LoggingConstants.Field_RequestTraceChain] = chain;
                tw.CallAppend(sb, lei);
            });
            t.Start();
            t.Join();
        }

        Assert.That(inner, Is.Null);
        Assert.That(sb.ToString(), Is.EqualTo(chain));
    }

    [Test]
    public void Test_Render_MetadataBackupIgnored()
    {
        var sb = new StringBuilder();
        string chain = RequestTraceChain.Instance.Create();

        var tw = new Child();
        var lei = new LogEventInfo();
        lei.Properties[LoggingConstants.Field_RequestTraceChain] = "abcd";
        tw.CallAppend(sb, lei);

        Assert.That(sb.ToString(), Is.EqualTo(chain));
    }
}
