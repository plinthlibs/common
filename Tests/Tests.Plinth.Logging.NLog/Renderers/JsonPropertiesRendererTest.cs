using Plinth.Common.Logging;
using Plinth.Logging.NLog.Renderers;
using NLog;
using NUnit.Framework;
using System.Text;
using Plinth.Serialization;

namespace Tests.Plinth.Logging.NLog.Renderers;

[TestFixture]
public class JsonPropertiesRendererTest
{
    private class Renderer : JsonPropertiesRenderer
    {
        public string RunIt(LogEventInfo e)
        {
            var sb = new StringBuilder();
            base.Append(sb, e);
            return sb.ToString();
        }
    }

    [TearDown]
    public void TearDown()
    {
        ScopeContext.Clear();
        GlobalDiagnosticsContext.Clear();
    }

    [Test]
    public void Test_NoString()
    {
        LogEventInfo lei = LogEventInfo.Create(LogLevel.Debug, "logger",
            "message");

        var r = new Renderer();
        Assert.That(r.RunIt(lei), Is.EqualTo("{}"));
    }

    [Test]
    public void Test_MessageType()
    {
        var r = new Renderer();
        var lei = LogEventInfo.Create(LogLevel.Debug, "logger", "message");
        lei.Properties.Add(LoggingConstants.Field_MessageType, "TestMessage");
        Assert.That(
            r.RunIt(lei), Is.EqualTo("{\"MessageType\":\"TestMessage\"}"));
    }

    [Test]
    public void Test_Scope()
    {
        var r = new Renderer();
        var lei = LogEventInfo.Create(LogLevel.Debug, "logger", "message");
        ScopeContext.Clear();
        ScopeContext.PushProperty(LoggingConstants.Field_MessageType, "TestMessage");
        Assert.That(
            r.RunIt(lei), Is.EqualTo("{\"MessageType\":\"TestMessage\"}"));
    }

    [Test]
    public void Test_Structured()
    {
        var r = new Renderer();
        var lei = LogEventInfo.Create(LogLevel.Debug, "logger", null, "message {MessageType}", ["TestMessage"]);
        Assert.That(
            r.RunIt(lei), Is.EqualTo("{\"MessageType\":\"TestMessage\",\"original_msg\":\"message {MessageType}\"}"));
    }

    [Test]
    public void Test_Structured_AndScope()
    {
        var r = new Renderer();
        var lei = LogEventInfo.Create(LogLevel.Debug, "logger", null, "message {MessageType}", ["TestMessage"]);
        ScopeContext.Clear();
        ScopeContext.PushProperty("MessageType2", "TestMessage2");
        Assert.That(
            r.RunIt(lei), Is.EqualTo("{\"MessageType2\":\"TestMessage2\",\"MessageType\":\"TestMessage\",\"original_msg\":\"message {MessageType}\"}"));
    }

    [Test]
    public void Test_Structured_AndScope_Override()
    {
        var r = new Renderer();
        var lei = LogEventInfo.Create(LogLevel.Debug, "logger", null, "message {MessageType}", ["TestMessage"]);
        ScopeContext.Clear();
        ScopeContext.PushProperty(LoggingConstants.Field_MessageType, "Nope");
        Assert.That(
            r.RunIt(lei), Is.EqualTo("{\"MessageType\":\"TestMessage\",\"original_msg\":\"message {MessageType}\"}"));
    }

    [Test]
    public void Test_Scope_AndProps()
    {
        var r = new Renderer();
        var lei = LogEventInfo.Create(LogLevel.Debug, "logger", "message");
        lei.Properties.Add("MessageType2", "TestMessage2");
        ScopeContext.Clear();
        ScopeContext.PushProperty(LoggingConstants.Field_MessageType, "TestMessage");
        Assert.That(
            r.RunIt(lei), Is.EqualTo("{\"MessageType\":\"TestMessage\",\"MessageType2\":\"TestMessage2\"}"));
    }

    [Test]
    public void Test_MDC_MDLC_Props()
    {
        var r = new Renderer();
        var lei = LogEventInfo.Create(LogLevel.Debug, "logger", "message");
        lei.Properties.Add("MessageType2", "TestMessage2");
        ScopeContext.Clear();
        ScopeContext.PushProperty(LoggingConstants.Field_MessageType, "TestMessage");
        GlobalDiagnosticsContext.Clear();
        GlobalDiagnosticsContext.Set("MDC1", "A");
        var result = JsonUtil.DeserializeObject<Dictionary<string, object>>(r.RunIt(lei)) ?? throw new Exception();
        Assert.That(result["MDC1"], Is.EqualTo("A"));
        Assert.That(result["MessageType"], Is.EqualTo("TestMessage"));
        Assert.That(result["MessageType2"], Is.EqualTo("TestMessage2"));
    }

    [Test]
    public void Test_Properties()
    {
        var r = new Renderer();
        var lei = LogEventInfo.Create(LogLevel.Debug, "logger", "message");
        lei.Properties.Add(LoggingConstants.Field_MessageType, "TestMessage");
        lei.Properties.Add("Foo", "FooString");
        lei.Properties.Add("Bar", "BarString");
        Assert.That(
            r.RunIt(lei), Is.EqualTo("{\"MessageType\":\"TestMessage\",\"Foo\":\"FooString\",\"Bar\":\"BarString\"}"));
    }

    [Test]
    public void Test_TraceProperties()
    {
        RequestTraceProperties.SetRequestTraceUserName("imbanker");
        RequestTraceProperties.SetRequestTraceSessionId("abc123");
        RequestTraceProperties.SetRequestTraceProperty("CustomProperty", "CustomValue");

        {
            var r = new Renderer();
            var lei = LogEventInfo.Create(LogLevel.Debug, "logger", "message");
            lei.Properties.Add(LoggingConstants.Field_MessageType, "TestMessage");
            lei.Properties.Add("Foo", "FooString");
            lei.Properties.Add("Bar", "BarString");
            Assert.That(
                r.RunIt(lei), Is.EqualTo("{\"MessageType\":\"TestMessage\",\"Foo\":\"FooString\",\"Bar\":\"BarString\",\"UserName\":\"imbanker\",\"SessionId\":\"abc123\",\"CustomProperty\":\"CustomValue\"}"));
        }

        RequestTraceProperties.ClearRequestTraceProperties();

        {
            var r = new Renderer();
            var lei = LogEventInfo.Create(LogLevel.Debug, "logger", "message");
            lei.Properties.Add(LoggingConstants.Field_MessageType, "TestMessage");
            lei.Properties.Add("Foo", "FooString");
            lei.Properties.Add("Bar", "BarString");
            Assert.That(
                r.RunIt(lei), Is.EqualTo("{\"MessageType\":\"TestMessage\",\"Foo\":\"FooString\",\"Bar\":\"BarString\"}"));
        }
    }
}
