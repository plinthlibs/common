using Plinth.Logging.NLog.Renderers;
using NUnit.Framework;

namespace Tests.Plinth.Logging.NLog.Renderers;

[TestFixture]
public class TruncateLeftRendererTest
{
    private class Child : TruncateLeftRenderer
    {
        public string CallTransform(string text)
        {
            return base.Transform(text);
        }
    }

    [Test]
    public void Test_Longer()
    {
        var tw = new Child
        {
            Length = 20
        };

        Assert.That(tw.CallTransform("1234567890abcdefghij!@#$%^&*()"), Is.EqualTo("abcdefghij!@#$%^&*()"));
    }

    [Test]
    public void Test_Exact()
    {
        var tw = new Child
        {
            Length = 20
        };

        Assert.That(tw.CallTransform("abcdefghij!@#$%^&*()"), Is.EqualTo("abcdefghij!@#$%^&*()"));
    }

    [Test]
    public void Test_Shorter()
    {
        var tw = new Child
        {
            Length = 20
        };

        Assert.That(tw.CallTransform("abcdefghij!@#$%^&"), Is.EqualTo("   abcdefghij!@#$%^&"));
    }
}
