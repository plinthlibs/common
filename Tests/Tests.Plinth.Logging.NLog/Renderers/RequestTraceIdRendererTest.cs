using Plinth.Common.Logging;
using Plinth.Logging.NLog.Renderers;
using NLog;
using NUnit.Framework;
using System.Text;

namespace Tests.Plinth.Logging.NLog.Renderers;

[TestFixture]
public class RequestTraceIdRenderTest
{
    private class Child : RequestTraceIdRenderer
    {
        public void CallAppend(StringBuilder sb, LogEventInfo le)
        {
            base.Append(sb, le);
        }
    }

    [Test]
    public void Test_Render()
    {
        RequestTraceId.Instance.Create();
        var tw = new Child();
        var sb = new StringBuilder();
        tw.CallAppend(sb, new LogEventInfo());

        Assert.That(sb.ToString(), Does.Match(@"^[a-f0-9]{32}$"));
    }

    [Test]
    public void Test_Render_Null()
    {
        var sb = new StringBuilder();

        using (ExecutionContext.SuppressFlow())
        {
            var t = new Thread(() =>
            {
                var tw = new Child();
                tw.CallAppend(sb, new LogEventInfo());
            });
            t.Start();
            t.Join();
        }

        Assert.That(sb.ToString(), Is.Empty);
    }

    [Test]
    public void Test_Render_Null_MetadataBackup()
    {
        var sb = new StringBuilder();
        string traceId = RequestTraceId.Instance.Create();
        string? inner = null;

        using (ExecutionContext.SuppressFlow())
        {
            var t = new Thread(() =>
            {
                var tw = new Child();
                inner = RequestTraceId.Instance.Get();
                var lei = new LogEventInfo();
                lei.Properties[LoggingConstants.Field_RequestTraceId] = traceId; 
                tw.CallAppend(sb, lei);
            });
            t.Start();
            t.Join();
        }

        Assert.That(inner, Is.Null);
        Assert.That(sb.ToString(), Is.EqualTo(traceId));
    }

    [Test]
    public void Test_Render_MetadataBackupIgnored()
    {
        var sb = new StringBuilder();
        string traceId = RequestTraceId.Instance.Create();

        var tw = new Child();
        var lei = new LogEventInfo();
        lei.Properties[LoggingConstants.Field_RequestTraceId] = "abcd"; 
        tw.CallAppend(sb, lei);

        Assert.That(sb.ToString(), Is.EqualTo(traceId));
    }
}
