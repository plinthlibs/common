using Plinth.Logging.NLog.Renderers;
using NLog;
using NUnit.Framework;

namespace Tests.Plinth.Logging.NLog.Renderers;

[TestFixture]
public class LogFileNameRendererTest
{
    [Test]
    public void Test_LogFileNameAll()
    {
        var lf = new LogFileNameRenderer()
        {
            Component = "test-component",
            Environment = "test",
            IncludeMachine = true,
            Type = "error"
        };

        Assert.That(lf.Render(new LogEventInfo()), Is.EqualTo($"logfile.error.test-component.test.{Environment.MachineName.ToLowerInvariant()}"));
    }

    [Test]
    public void Test_LogFileNameMinimal()
    {
        var lf = new LogFileNameRenderer()
        {
            Component = "test-component",
            IncludeMachine = false
        };

        Assert.That(lf.Render(new LogEventInfo()), Is.EqualTo($"logfile.test-component"));
    }

    [Test]
    public void Test_LogFileNameMixture()
    {
        var lf = new LogFileNameRenderer()
        {
            Component = "test-component",
            IncludeMachine = false,
            Type = "type"
        };

        Assert.That(lf.Render(new LogEventInfo()), Is.EqualTo($"logfile.type.test-component"));
    }
}
