using System.Text;
using Microsoft.Extensions.Logging;
using Plinth.Common.Logging;
using Plinth.Logging.NLog.Renderers;
using NLog;
using NUnit.Framework;

namespace Tests.Plinth.Logging.NLog.Renderers;

[TestFixture]
public class AsyncCtxRendererTest
{
    private class Child : AsyncCtxRenderer
    {
        public void CallAppend(StringBuilder sb, LogEventInfo le)
        {
            base.Append(sb, le);
        }
    }

    [Test]
    public void Test_AsyncContext()
    {
        var lf = new AsyncCtxRenderer();

        Assert.That(lf.Render(new LogEventInfo()), Is.EqualTo("     "));

        StaticLogManager.SetContextId(0x1a3b5);

        Assert.That(lf.Render(new LogEventInfo()), Is.EqualTo("1A3B5"));

        StaticLogManager.SetContextId(0xabcde21c3d5);

        Assert.That(lf.Render(new LogEventInfo()), Is.EqualTo("1C3D5"));
    }

    [Test]
    public void Test_Render_Null()
    {
        var sb = new StringBuilder();
        StaticLogManager.SetContextId(1122344L);

        using (ExecutionContext.SuppressFlow())
        {
            var t = new Thread(() =>
            {
                var tw = new Child();
                tw.CallAppend(sb, new LogEventInfo());
            });
            t.Start();
            t.Join();
        }

        Assert.That(sb.ToString(), Is.EqualTo("     "));
    }

    [Test]
    public void Test_Render_Null_MetadataBackup()
    {
        var sb = new StringBuilder();
        StaticLogManager.SetContextId(1122344L);

        using (ExecutionContext.SuppressFlow())
        {
            var t = new Thread(() =>
            {
                var tw = new Child();
                var lei = new LogEventInfo();
                lei.Properties[LoggingConstants.Field_ContextId] = "AB12FD";
                tw.CallAppend(sb, lei);
            });
            t.Start();
            t.Join();
        }

        Assert.That(sb.ToString(), Is.EqualTo("AB12FD"));
    }

    [Test]
    public void Test_Render_MetadataBackupIgnored()
    {
        var sb = new StringBuilder();
        StaticLogManager.SetContextId(112234455L);

        using (ExecutionContext.SuppressFlow())
        {
            var tw = new Child();
            var lei = new LogEventInfo();
            lei.Properties[LoggingConstants.Field_ContextId] = "AB12FD";
            tw.CallAppend(sb, lei);
        }

        Assert.That(sb.ToString(), Is.EqualTo("08FD7"));
    }
}
