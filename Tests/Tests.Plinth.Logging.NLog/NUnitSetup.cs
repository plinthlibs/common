using Plinth.Logging.Host;
using NUnit.Framework;

namespace Tests.Plinth;

[SetUpFixture]
public class NUnitSetup
{
    [OneTimeSetUp]
    public void Setup()
    {
        StaticLogManagerSetup.ConfigureForConsoleLogging();
    }
}
