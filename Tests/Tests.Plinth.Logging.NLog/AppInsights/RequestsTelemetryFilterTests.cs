using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ApplicationInsights.Channel;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.ApplicationInsights.Extensibility;
using NSubstitute;
using NUnit.Framework;
using Plinth.Logging.NLog.AppInsights;

namespace Tests.Plinth.Logging.NLog.AppInsights;

[TestFixture]
internal class RequestsTelemetryFilterTests
{
    [Test]
    [TestCase(null)]
    [TestCase("http://localhost/api/thing")]
    [TestCase("http://localhost/")]
    [TestCase("http://localhost/some.random.js")]
    [TestCase("http://localhost/assets/some.random.png")]
    [TestCase("http://localhost/version")]
    public void Defaults_Pass(string? url)
    {
        AssertPass(url);
    }

    [Test]
    [TestCase("https://localhost/swagger")]
    [TestCase("https://localhost/swagger/")]
    [TestCase("https://localhost/redoc")]
    [TestCase("https://localhost/redoc/")]
    [TestCase("https://localhost/favicon.ico")]
    [TestCase("https://localhost/_vs")]
    [TestCase("https://localhost/_vs/")]
    [TestCase("https://localhost/_framework")]
    [TestCase("https://localhost/_framework/")]
    public void Defaults_Fail(string? url)
    {
        AssertFail(url);
    }

    [Test]
    public void NoUrl_Pass()
    {
        AssertPass(null);
    }

    [Test]
    [TestCase(null)]
    [TestCase("http://localhost/api/thing")]
    [TestCase("http://localhost/")]
    [TestCase("http://localhost/some.random.js")]
    [TestCase("http://localhost/assets/some.random.png")]
    [TestCase("http://localhost/version")]
    // disabled prefix has trailing slash so these pass
    [TestCase("http://localhost/path3")]
    [TestCase("http://localhost/root/path4")]
    public void DisabledSet_Pass(string? url)
    {
        AssertPass(url, s => s.DisabledPrefixes =
        [
            "/path1",
            "/root/path2",
            "/path3/",
            "/root/path4/"
        ]);
    }

    [Test]
    [TestCase("http://localhost/path1")]
    [TestCase("http://localhost/path1/")]
    [TestCase("http://localhost/path1/some/other/thing")]
    [TestCase("http://localhost/root/path2")]
    [TestCase("http://localhost/root/path2/")]
    [TestCase("http://localhost/root/path2/some/other/thing")]
    [TestCase("http://localhost/path3/")]
    [TestCase("http://localhost/path3/some/other/thing")]
    [TestCase("http://localhost/root/path4/")]
    [TestCase("http://localhost/root/path4/some/other/thing")]
    public void DisabledSet_Fail(string? url)
    {
        AssertFail(url, s => s.DisabledPrefixes =
        [
            "/path1",
            "/root/path2",
            "/path3/",
            "/root/path4/"
        ]);
    }

    [Test]
    [TestCase(null)] // requests without urls pass
    [TestCase("http://localhost/path1")]
    [TestCase("http://localhost/path1/")]
    [TestCase("http://localhost/path1/some/other/thing")]
    [TestCase("http://localhost/root/path2")]
    [TestCase("http://localhost/root/path2/")]
    [TestCase("http://localhost/root/path2/some/other/thing")]
    [TestCase("http://localhost/path3/")]
    [TestCase("http://localhost/path3/some/other/thing")]
    [TestCase("http://localhost/root/path4/")]
    [TestCase("http://localhost/root/path4/some/other/thing")]
    public void EnabledSet_DisabledUnset_Pass(string? url)
    {
        AssertPass(url, s =>
        {
            s.DisabledPrefixes.Clear();
            s.EnabledPrefixes =
            [
                "/path1",
                "/root/path2",
                "/path3/",
                "/root/path4/"
            ];
        });
    }

    [Test]
    [TestCase("http://localhost/api/thing")]
    [TestCase("http://localhost/")]
    [TestCase("http://localhost/some.random.js")]
    [TestCase("http://localhost/assets/some.random.png")]
    [TestCase("http://localhost/version")]
    // disabled prefix has trailing slash so these fail
    [TestCase("http://localhost/path3")]
    [TestCase("http://localhost/root/path4")]
    public void EnabledSet_DisabledUnset_Fail(string? url)
    {
        AssertFail(url, s =>
        {
            s.DisabledPrefixes.Clear();
            s.EnabledPrefixes =
            [
                "/path1",
                "/root/path2",
                "/path3/",
                "/root/path4/"
            ];
        });
    }

    [Test]
    [TestCase("http://localhost/path1")]
    [TestCase("http://localhost/path1/")]
    public void EnabledDisabledConflict_Fail(string? url)
    {
        AssertFail(url, s =>
        {
            s.DisabledPrefixes =
            [
                "/path1"
            ];
            s.EnabledPrefixes =
            [
                "/path1",
            ];
        });
    }

    [Test]
    [TestCase("http://localhost/path1")]
    [TestCase("http://localhost/path1/")]
    public void EnabledDisabled_Pass(string? url)
    {
        AssertPass(url, s =>
        {
            s.DisabledPrefixes =
            [
                "/path2"
            ];
            s.EnabledPrefixes =
            [
                "/path1",
            ];
        });
    }

    [Test]
    public void NotARequest_Pass()
    {
        // arrange
        var proc = Substitute.For<ITelemetryProcessor>();

        var s = new AppInsightsSettings();

        var rf = new RequestsTelemetryFilter(proc, s);

        var tel = new TraceTelemetry();

        // act
        rf.Process(tel);

        // assert
        proc.Received(1).Process(Arg.Any<ITelemetry>());
    }

    [Test]
    public void NoSettings_Pass()
    {
        AssertPass("http://localhost/thing", s =>
        {
            s.DisabledPrefixes.Clear();
            s.EnabledPrefixes = null;
        });
    }

    [Test]
    public void RootEnabled_SubDirDisabled_Fails()
    {
        AssertFail("http://localhost/api/mything", s =>
        {
            s.DisabledPrefixes.Add("/api/mything");
            s.EnabledPrefixes = ["/api"];
        });
    }

    [Test]
    public void RootEnabled_SubDirDisabled_Pass()
    {
        AssertPass("http://localhost/api/notmything", s =>
        {
            s.DisabledPrefixes.Add("/api/mything");
            s.EnabledPrefixes = ["/api"];
        });
    }

    [Test]
    public void RootDisabled_SubDirEnabled_Fails()
    {
        AssertFail("http://localhost/api/mything", s =>
        {
            s.DisabledPrefixes.Add("/api");
            s.EnabledPrefixes = ["/api/mything"];
        });
    }

    [Test]
    public void Custom_Pass()
    {
        bool customCalled = false;
        AssertPass("http://localhost/api/anything", s =>
        {
            s.CustomFilter = r => { customCalled = true; Assert.That(new Uri("http://localhost/api/anything"), Is.EqualTo(r.Url)); return true; };
        });

        Assert.That(customCalled);
    }

    [Test]
    public void Custom_Fail()
    {
        bool customCalled = false;
        AssertFail("http://localhost/api/anything", s =>
        {
            s.CustomFilter = r => { customCalled = true; Assert.That(new Uri("http://localhost/api/anything"), Is.EqualTo(r.Url)); return false; };
        });

        Assert.That(customCalled);
    }

    [Test]
    public void Custom_OverridesDisabled()
    {
        bool customCalled = false;
        AssertPass("http://localhost/api/anything", s =>
        {
            s.CustomFilter = r => { customCalled = true; Assert.That(new Uri("http://localhost/api/anything"), Is.EqualTo(r.Url)); return true; };
            s.DisabledPrefixes.Add("/api");
        });

        Assert.That(customCalled);
    }

    [Test]
    public void Custom_OverridesEnabled()
    {
        bool customCalled = false;
        AssertFail("http://localhost/api/anything", s =>
        {
            s.CustomFilter = r => { customCalled = true; Assert.That(new Uri("http://localhost/api/anything"), Is.EqualTo(r.Url)); return false; };
            s.EnabledPrefixes = ["/api"];
        });

        Assert.That(customCalled);
    }

    private static void AssertFail(string? url, Action<AppInsightsSettings>? settings = null)
        => AssertProcess(url, false, settings);

    private static void AssertPass(string? url, Action<AppInsightsSettings>? settings = null)
        => AssertProcess(url, true, settings);

    private static void AssertProcess(string? url, bool expected, Action<AppInsightsSettings>? settings = null)
    {
        // arrange
        var proc = Substitute.For<ITelemetryProcessor>();

        var s = new AppInsightsSettings();
        settings?.Invoke(s);

        var rf = new RequestsTelemetryFilter(proc, s);

        var tel = new RequestTelemetry
        {
            Url = url == null ? null : new Uri(url)
        };

        // act
        rf.Process(tel);

        // assert
        proc.Received(expected ? 1 : 0).Process(Arg.Any<ITelemetry>());
    }
}
