using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Attributes;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Enums;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Plinth.Common.Constants;
using Plinth.Common.Exceptions;
using Plinth.Common.Logging;
using Plinth.HttpApiClient;
using Plinth.HttpApiClient.Common;
using Plinth.Serialization;
using System.Net;

namespace Tests.Plinth.AzureFunction;

public class Function1
{
    private readonly ILogger _logger;
    private readonly MyFuncClient _myFuncClient;

    public Function1(ILoggerFactory loggerFactory, MyFuncClient myFuncClient)
    {
        _logger = loggerFactory.CreateLogger<Function1>();
        _myFuncClient = myFuncClient;
    }

    public class IdModel
    {
        public int Id { get; set; }
    }

    [Function("Function1")]
    [OpenApiOperation(operationId: "function1", tags: ["Functions"], Summary = "Function 1", Description = "This is the first Function", Visibility = OpenApiVisibilityType.Important)]
    [OpenApiSecurity("function_key", SecuritySchemeType.ApiKey, Name = "code", In = OpenApiSecurityLocationType.Query)]
    [OpenApiResponseWithBody(statusCode: HttpStatusCode.OK, contentType: ContentTypes.ApplicationJsonUtf8, bodyType: typeof(MyFuncClient.DataModel), Summary = "The response", Description = "This returns the response")]
    public async Task<ActionResult> RunFunction1([HttpTrigger(AuthorizationLevel.Function, "get", "post")] HttpRequest req)
    {
        _logger.Info($"C# HTTP trigger function processed a request: {RequestTraceId.Instance.Get()} : Method = {req.Method}");

        bool isLocal = string.IsNullOrEmpty(Environment.GetEnvironmentVariable("WEBSITE_RUN_FROM_PACKAGE"));

        var x = await _myFuncClient.CallFunc2(isLocal);
        _logger.LogInformation("Got this data: {body}", JsonUtil.SerializeObject(x));

        return new OkObjectResult(new MyFuncClient.DataModel { Value = 1 });
    }

    [Function("Function2")]
    [OpenApiOperation(operationId: "function2", tags: ["Functions"], Summary = "Function 2", Description = "This is the second Function", Visibility = OpenApiVisibilityType.Advanced)]
    [OpenApiSecurity("function_key", SecuritySchemeType.ApiKey, Name = "code", In = OpenApiSecurityLocationType.Query)]
    [OpenApiResponseWithBody(statusCode: HttpStatusCode.OK, contentType: ContentTypes.ApplicationJsonUtf8, bodyType: typeof(MyFuncClient.DataModel), Summary = "The response", Description = "This returns the response")]
    public async Task<ActionResult> RunFunction2([HttpTrigger(AuthorizationLevel.Function, "get", "post")] HttpRequest req)
    {
        _logger.Info($"C# HTTP trigger (2) function processed a request: {RequestTraceId.Instance.Get()} : Method = {req.Method}");

        await Task.Delay(100);

        return new OkObjectResult(new MyFuncClient.DataModel { Value = 2 });
    }

    [Function("Function3")]
    [OpenApiOperation(operationId: "function3", tags: ["Error Functions"], Summary = "Function 3", Description = "This is the third Function")]
    [OpenApiSecurity("function_key", SecuritySchemeType.ApiKey, Name = "code", In = OpenApiSecurityLocationType.Query)]
    [OpenApiResponseWithBody(statusCode: HttpStatusCode.OK, contentType: ContentTypes.ApplicationJsonUtf8, bodyType: typeof(MyFuncClient.DataModel), Summary = "The response", Description = "This returns the response")]
    [OpenApiResponseWithBody(statusCode: HttpStatusCode.NotFound, contentType: ContentTypes.ApplicationJsonUtf8, bodyType: typeof(IdModel), Summary = "The response", Description = "This returns the response")]
    public async Task<ActionResult> RunFunction3([HttpTrigger(AuthorizationLevel.Function, "get", "post")] HttpRequest req)
    {
        _logger.Info("C# HTTP trigger (3) function processed a request. {reqMethod}", req.Method);

        await Task.Delay(100);

        throw new LogicalNotFoundException(new IdModel { Id = 3 });
    }

    [Function("Function4")]
    public async Task<ActionResult> RunFunction4([HttpTrigger(AuthorizationLevel.Function, "get", "post")] HttpRequest req)
    {
        _logger.Info("C# HTTP trigger (4) function processed a request. {reqMethod}", req.Method);

        await Task.Delay(100);

        throw new RestException(HttpStatusCode.Gone, "I am gone", "I am gone away");
    }

    [Function("Function5")]
    public async Task<ActionResult> RunFunction5([HttpTrigger(AuthorizationLevel.Function, "get", "post")] HttpRequest req)
    {
        _logger.Info("C# HTTP trigger (5) function processed a request. {reqMethod}", req.Method);

        await Task.Delay(100);

        throw new LogicalPreconditionFailedException(new { id = 5 });
    }

    [Function("Function6")]
    public async Task<ActionResult> RunFunction6([HttpTrigger(AuthorizationLevel.Function, "get", "post")] HttpRequest req)
    {
        _logger.Info("C# HTTP trigger (6) function processed a request. {reqMethod}", req.Method);

        await Task.Delay(100);

        throw new InvalidOperationException("I'm not sure what happened");
    }

    [Function("Function6alt")]
    public async Task<ActionResult> RunFunction6alt([HttpTrigger(AuthorizationLevel.Function, "get", "post")] HttpRequest req)
    {
        _logger.Info("C# HTTP trigger (6) function processed a request. {reqMethod}", req.Method);

        await Task.Delay(100);

        throw new BadException();
    }

    private class BadException : Exception
    {
        public BadException() : base("first exception") { }
        public override string Message => throw new NotSupportedException("second exception");
    }

    [Function("Function7")]
    public async Task<ActionResult> RunFunction7([HttpTrigger(AuthorizationLevel.Function, "get", "post")] HttpRequest req)
    {
        _logger.Info("C# HTTP trigger (6) function processed a request. {reqMethod}", req.Method);

        await Task.Delay(100);

        return new OkObjectResult(new
        {
            value =
            new string(Enumerable.Repeat('x', 100000).ToArray())
        });
    }

    [Function("Function8")]
    public async Task RunFunction8([HttpTrigger(AuthorizationLevel.Function, "get", "post")] HttpRequest req)
    {
        _logger.Info("C# HTTP trigger (6) function processed a request. {reqMethod}", req.Method);

        await Task.Delay(100);

        var resp = req.HttpContext.Response;
        resp.ContentType = "application/json";
        await resp.WriteAsJsonAsync(new MyFuncClient.DataModel { Value = 8 });
    }
}
