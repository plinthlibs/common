- To publish, you need the azure `func` tool, and to set up your azure credentials
- once you've done that, simple run this command to publish the function

func azure functionapp publish plinth-function-test
