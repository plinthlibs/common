using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Plinth.HttpApiClient;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration;
using Plinth.AzureFunction.Logging;
using Plinth.AzureFunction;
using Microsoft.Azure.Functions.Worker.Extensions.OpenApi.Extensions;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Abstractions;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Configurations;
using Microsoft.OpenApi.Models;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Enums;

namespace Tests.Plinth.AzureFunction;

public class MyFuncClient : BaseHttpApiClient
{
    public class DataModel
    {
        public int Value { get; set; }
    }

    private readonly string localFuncUrl, remoteFuncUrl;

    public MyFuncClient(HttpClient httpClient, IConfiguration config, IOptions<BaseHttpApiClientSettings>? settings = null)
        : base("Func", httpClient, settings)
    {
        localFuncUrl = config.GetValue<string>("LocalFuncUrl") ?? string.Empty;
        remoteFuncUrl = config.GetValue<string>("RemoteFuncUrl") ?? string.Empty;
    }

    public async Task<DataModel> CallFunc2(bool isLocal)
    {
        return await HttpGet(isLocal ? localFuncUrl : remoteFuncUrl).ExecuteAsync<DataModel>() ?? throw new Exception();
    }
}

public static class Program
{
    /*
    to run, you need a local.settings.json with this

{
  "IsEncrypted": false,
  "Values": {
    "AzureWebJobsStorage": "",
    "FUNCTIONS_WORKER_RUNTIME": "dotnet-isolated",
    "APPLICATIONINSIGHTS_CONNECTION_STRING": "",
    "WEBSITE_CLOUD_ROLENAME": "my-test-function"
  }
}

    */

    public static async Task Main(string[] _)
    {
        var host = new HostBuilder()
            .ConfigureAppConfiguration((hostingContext, config) =>
            {
                var context = hostingContext.HostingEnvironment;
                config
                    .AddJsonFile(Path.Combine(context.ContentRootPath, "funcsettings.json"), optional: false, reloadOnChange: false)
                    .AddJsonFile(Path.Combine(context.ContentRootPath, $"funcsettings.{context.EnvironmentName}.json"), optional: true, reloadOnChange: false)
                    .AddEnvironmentVariables();
            })
            .ConfigureFunctionsWebApplication(builder =>
            {
                //builder.UseNewtonsoftJson();
                builder.UsePlinthServiceLogging();
            })
            .ConfigureServices((hostingContext, s) =>
            {
                s.AddPlinthAppInsightsTelemetry(hostingContext.Configuration);
                s.Configure<LoggerFilterOptions>(options =>
                {
                    // either do this or set the minimum default/appinsights value in funcsettings
                    // options.FixAppInsightsLogging();
                });

                s.AddHttpClient();

                s.AddHttpApiClient<MyFuncClient>();

                s.AddSingleton<IOpenApiConfigurationOptions>(_ =>
                {
                    var options = new OpenApiConfigurationOptions()
                    {
                        Info = new OpenApiInfo()
                        {
                            Version = "1.0.0",
                            Title = "Swagger Test",
                            Description = "This is a test",
                            TermsOfService = new Uri("https://github.com/Azure/azure-functions-openapi-extension"),
                            Contact = new OpenApiContact()
                            {
                                Name = "Enquiry",
                                Email = "dontask@plinth.com",
                                Url = new Uri("https://github.com/Azure/azure-functions-openapi-extension/issues"),
                            },
                            License = new OpenApiLicense()
                            {
                                Name = "MIT",
                                Url = new Uri("http://opensource.org/licenses/MIT"),
                            }
                        },
                        Servers = [ new OpenApiServer() { Url = "http://localhost:7296/api", Description = "Local Dev" } ],
                        OpenApiVersion = OpenApiVersionType.V3,
                        ExcludeRequestingHost = true,
                        //ForceHttps = true,
                        //ForceHttp = false,
                    };

                    return options;
                });
            })
            .ConfigureLogging((hostingContext, logging) =>
            {
                // Make sure the configuration of the appsettings.json file is picked up.
                logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
            })
            .Build();

        await host
            .AddPlinthLogging()
            .RunAsync();
    }
}
