﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.Plinth.Templating.Templates.Models;

public class TestModel1
{
    public string? Name { get; set; }
    public int Id { get; set; }
    public List<string>? Aliases { get; set; }
}
