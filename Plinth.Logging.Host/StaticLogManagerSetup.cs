using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Configuration;
using Microsoft.Extensions.Logging.Console;
using Microsoft.Extensions.Logging.Debug;
using Microsoft.Extensions.Options;
using Plinth.Logging.Host.FileLogging;
using Plinth.Common.Utils;

namespace Plinth.Logging.Host;

/// <summary>
/// Utility for configuring StaticLogManager for logging hosts
/// </summary>
public class StaticLogManagerSetup
{
    /// <summary>
    /// Load configuration from appsettings.json
    /// </summary>
    /// <remarks>These are simple loggers, strongly prefer NLog for any sophistocated case</remarks>
    /// <param name="configRoot">(optional) override root node in appsettings.json, defaults to "Logging"</param>
    /// <param name="appSettings">(optional) override of default 'appsettings.json' filename</param>
    /// <example>
    /// Sample appsettings.json.  Includde "Console", "Debug", or "File" to enable those providers
    /// <code>
    /// {
    ///   "Logging": {
    ///     "LogLevel": {
    ///       "Default": "Debug",
    ///       "System": "Information",
    ///       "Microsoft": "Information"
    ///     },
    ///     "Console": {
    ///       "IncludeScopes": true
    ///     },
    ///     "Debug": { },
    ///     "File": {
    ///       "Path": "app.log",
    ///       "Append": true,
    ///       "FileSizeLimitBytes": 0,  // use to activate rolling file behaviour
    ///       "MaxRollingFiles": 0  // use to specify max number of log files
    ///     }
    ///   }
    /// }
    /// 
    /// var log = StaticLogManagerSetup.FromAppSettings();
    /// </code>
    /// </example>
    /// <returns>an ILogger for logging in Main()</returns>
    [MethodImpl(MethodImplOptions.NoInlining)] // this is important for the automatic class detection
    public static ILogger FromAppSettings(string configRoot = "Logging", string appSettings = "appsettings.json")
    {
        var config = new ConfigurationBuilder()
           .SetBasePath(RuntimeUtil.FindExecutableHome(appSettings))
           .AddJsonFile(appSettings, optional: true, reloadOnChange: false)
           .Build();

        ConfigureFromAppSettingsImpl(config, configRoot);

        return GetMainLoggerImpl();
    }

    [MethodImpl(MethodImplOptions.NoInlining)] // this is important for the automatic class detection
    private static ILogger GetMainLoggerImpl()
    {
        var frame = new StackFrame(2, false);
        var method = frame.GetMethod();

        var declaringType = method?.DeclaringType ?? throw new InvalidOperationException("calling method could not be determined");

        return StaticLogManager.StaticLoggerFactory.CreateLogger(declaringType);
    }

    /// <summary>
    /// Load configuration from a custom IConfiguration
    /// </summary>
    /// <remarks>These are simple loggers, strongly prefer NLog for any sophistocated case</remarks>
    /// <param name="config">loaded from <c>ConfigurationBuilder</c></param>
    /// <param name="configRoot">root node in appsettings.json</param>
    /// <example>
    /// Sample Code
    /// <code>
    ///  var config = new ConfigurationBuilder()
    ///     .SetBasePath(Directory.GetCurrentDirectory())
    ///     .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
    ///     .Build();
    ///     
    /// var log = StaticLogManagerSetup.FromAppSettings(config);
    /// </code>
    /// Sample appsettings.json.  Includde "Console", "Debug", or "File" to enable those providers
    /// <code>
    /// {
    ///   "Logging": {
    ///     "LogLevel": {
    ///       "Default": "Debug",
    ///       "System": "Information",
    ///       "Microsoft": "Information"
    ///     },
    ///     "Console":
    ///     {
    ///       "IncludeScopes": true
    ///     },
    ///     "Debug": { },
    ///     "File": {
    ///       "Path": "app.log",
    ///       "Append": true,
    ///       "MinLevel": "Information",
    ///       "FileSizeLimitBytes": 0,  // use to activate rolling file behaviour
    ///       "MaxRollingFiles": 0  // use to specify max number of log files
    ///     }
    ///   }
    /// }
    /// </code>
    /// </example>
    /// <returns>an ILogger for logging in Main()</returns>
    [MethodImpl(MethodImplOptions.NoInlining)] // this is important for the automatic class detection
    public static ILogger FromAppSettings(IConfiguration config, string configRoot = "Logging")
    {
        ConfigureFromAppSettingsImpl(config, configRoot);
        return GetMainLoggerImpl();
    }

    private static void ConfigureFromAppSettingsImpl(IConfiguration config, string configRoot = "Logging")
    {
        ArgumentNullException.ThrowIfNull(config);
        ArgumentNullException.ThrowIfNull(configRoot);

        var section = config.GetSection(configRoot)
            ?? throw new ArgumentNullException(nameof(configRoot), $"'{configRoot}' section not found in config");

        var loggerFactory = LoggerFactory.Create(builder =>
        {
            builder.AddConfiguration(section);
            if (section.GetSection("Console") != null)
                builder.AddConsole();
            if (section.GetSection("Debug") != null)
                builder.AddDebug();
            if (section.GetSection("File") != null)
                builder.AddFile(section);
        });

        StaticLogManager.SetLoggerFactory(loggerFactory, "APPSETTINGS");

        // the console provider buffers, and will only flush the final buffer if the provider is disposed
        AppDomain.CurrentDomain.ProcessExit += (_a, _b) => loggerFactory.Dispose();
    }

    /// <summary>
    /// Configure StaticLogManager to log to debug/trace log (for unit tests and debugging)
    /// </summary>
    /// <param name="minLevel">(optional) set minimum logging level, defaults to Debug</param>
    /// <example>
    /// Sample Code
    /// <code>
    /// StaticLogManagerSetup.ConfigureForDebugLogging();
    /// </code>
    /// Similar to
    /// <code>
    /// services.AddLogging(builder => builder
    ///     .AddDebug()
    ///     .SetMinimumLevel(LogLevel.Debug)
    /// );
    /// </code>
    /// </example>
    public static ILogger ConfigureForDebugLogging(LogLevel minLevel = LogLevel.Debug)
    {
        var providers = new ILoggerProvider[] { new DebugLoggerProvider() };
        var fac = new LoggerFactory(providers, new LoggerFilterOptions { MinLevel = minLevel });
        StaticLogManager.SetLoggerFactory(fac, "DEBUG");

        return GetMainLoggerImpl();
    }

    /// <summary>
    /// Configure StaticLogManager to log to debug/trace log (for unit tests and debugging)
    /// </summary>
    /// <param name="minLevel">(optional) set minimum logging level, defaults to Information</param>
    /// <param name="configAction">(optional) action for custom simple console formatting</param>
    /// <example>
    /// Sample Code
    /// <code>
    /// StaticLogManagerSetup.ConfigureForConsoleLogging();
    /// </code>
    /// Similar to
    /// <code>
    /// services.AddLogging(builder => builder
    ///     .AddSimpleConsole()
    ///     .SetMinimumLevel(LogLevel.Information)
    /// );
    /// </code>
    /// </example>
    /// <returns>An ILogger if using this in a Console app without a host</returns>
    public static ILogger ConfigureForConsoleLogging(LogLevel minLevel = LogLevel.Information, Action<SimpleConsoleFormatterOptions>? configAction = null)
    {
        var fac = LoggerFactory.Create(builder =>
        {
            builder.SetMinimumLevel(minLevel);
            if (configAction != null)
                builder.AddSimpleConsole(configAction);
            else
                builder.AddSimpleConsole(c =>
                {
                    // a nice set of defaults
                    c.ColorBehavior = LoggerColorBehavior.Enabled;
                    c.SingleLine = true;
                    c.UseUtcTimestamp = true;
                    c.TimestampFormat = "HH:mm:ss.ffffff ";
                    c.IncludeScopes = false;
                });
        });

        StaticLogManager.SetLoggerFactory(fac, "CONSOLE");

        // the console provider buffers, and will only flush the final buffer if disposed
        AppDomain.CurrentDomain.ProcessExit += (_a, _b) => fac.Dispose();

        return GetMainLoggerImpl();
    }

    /// <summary>
    /// Configure StaticLogManager to log to a file (for simple use cases).
    /// </summary>
    /// <remarks>This is a very simple file logger, strongly prefer NLog for any sophistocated case</remarks>
    /// <param name="minLevel">(optional) set minimum logging level, defaults to Information</param>
    /// <param name="filePath">path and file name for the log file, defaults to "app.log"</param>
    /// <param name="options">(optional) custom file log output options</param>
    /// <example>
    /// Sample Code
    /// <code>
    /// StaticLogManagerSetup.ConfigureForFileLogging();
    /// </code>
    /// </example>
    /// <returns>An ILogger if using this in a Console app without a host</returns>
    public static ILogger ConfigureForFileLogging(LogLevel minLevel = LogLevel.Information, string filePath = "app.log", FileLoggerOptions? options = null)
    {
        var providers = new ILoggerProvider[] { new FileLoggerProvider(filePath, minLevel, options) };
        var fac = new LoggerFactory(providers);
        StaticLogManager.SetLoggerFactory(fac, "FILE");

        // the console provider buffers, and will only flush the final buffer if the provider is disposed
        AppDomain.CurrentDomain.ProcessExit += (_a, _b) => providers[0].Dispose();

        return GetMainLoggerImpl();
    }

    /// <summary>
    /// Use this when you are passed an ILogger and want all of Plinth.Common to use it
    /// in a multithreaded context (one ILogger per async context)
    /// </summary>
    /// <remarks>
    /// <para>
    /// The use case for this is an Azure Function where an ILogger is passed to you
    /// from the runtime.  Since you can't create more loggers, everything must log to this single logger.
    /// </para>
    /// <para>
    /// Also, since Azure Functions are multi-threaded, this allows multiple async contexts to get
    /// different ILogger instances per async context, even when the ILogger is retrieved statically in the class
    /// </para>
    /// </remarks>
    /// <param name="mainLogger">single ILogger which is provided to you</param>
    public static void ConfigureForSingleThread(ILogger mainLogger)
    {
        AsyncLocalLoggerFactory.SetCurrentLogger(mainLogger);

        if (StaticLogManager.StaticLoggerFactoryId != "ASYNC_LOCAL_SINGLE")
        {
            StaticLogManager.SetLoggerFactory(_singleThreadSingleLoggerFactory, "ASYNC_LOCAL_SINGLE");
        }
    }

#region Single Thread ILogger
    internal static readonly AsyncLocalLoggerFactory _singleThreadSingleLoggerFactory = new();

    internal class AsyncLocalLoggerFactory : ILoggerFactory
    {
        private static readonly AsyncLocal<ILogger> _localLogger = new();
        private static readonly AsyncLocalLogger _asyncLogger = new();

        public static void SetCurrentLogger(ILogger logger)
        {
            _localLogger.Value = logger;
        }

        public static ILogger? GetCurrentLogger()
            => _localLogger.Value;

        public void AddProvider(ILoggerProvider provider)
            => throw new NotSupportedException("cannot add new provider in ILogger/mainlogger mode");

        public ILogger CreateLogger(string _)
            => _asyncLogger;

        public void Dispose() { }
    }

    private class AsyncLocalLogger : ILogger
    {
        public AsyncLocalLogger()
        {
        }

        private static ILogger GetLogger()
            => AsyncLocalLoggerFactory.GetCurrentLogger() ?? StaticLogManager.CachedNoopLogger;

        public IDisposable? BeginScope<TState>(TState state) where TState : notnull
            => GetLogger().BeginScope(state);

        public bool IsEnabled(LogLevel logLevel)
            => GetLogger().IsEnabled(logLevel);

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception? exception, Func<TState, Exception?, string> formatter)
            => GetLogger().Log(logLevel, eventId, state, exception, formatter);
    }
#endregion

    /// <summary>
    /// Use this when you are passed an ILoggerFactory and want all of Plinth.Common to use it
    /// in a multithreaded context (one ILoggerFactory per async context)
    /// </summary>
    /// <remarks>
    /// <para>
    /// The use case for this is an Azure Function where you can inject an ILoggerFactory
    /// from the runtime.  Since you can't create configure your own factory, you must use this one
    /// </para>
    /// <para>
    /// Also, since Azure Functions are multi-threaded, this allows multiple async contexts to get
    /// different ILoggerFactory instances per async context, even when the ILogger is retrieved once,
    /// statically in the class
    /// </para>
    /// </remarks>
    /// <param name="loggerFactory">single ILoggerFactory which is provided to you</param>
    public static void ConfigureForSingleThread(ILoggerFactory loggerFactory)
    {
        AsyncLocalLoggerFactoryWrapper.SetCurrentFactory(loggerFactory);

        if (StaticLogManager.StaticLoggerFactoryId != "ASYNC_LOCAL_WRAP")
        {
            StaticLogManager.SetLoggerFactory(_singleThreadFactory, "ASYNC_LOCAL_WRAP");
        }
    }

#region Single Thread ILoggerFactory
    /// <summary>
    /// The way this works is that we install a single static ILoggerFactory as the static logger factory.
    /// Said factory has an AsyncLocal of ILoggerFactory which is set via 'ConfigureForSingleThread'.
    ///
    /// When a class first creates its static logger (via StaticLogManager.GetLogger), it will create
    /// an 'AsyncLocalLogger'
    /// 
    /// An AsyncLocalLogger upon creation will call the current context's factory, and store that ILogger
    /// in an AsyncLocal of ILogger.
    /// 
    /// An invocation of the class in a different async context, using the AsyncLocalLogger from the first context,
    /// will trigger creation of a new ILogger underneath using the new context's factory.
    /// 
    /// A NoopLogger is substituted (but not saved) if no factory has been set for the current context yet.
    /// </summary>
    internal static readonly AsyncLocalLoggerFactoryWrapper _singleThreadFactory = new();

    internal class AsyncLocalLoggerFactoryWrapper : ILoggerFactory
    {
        private static readonly AsyncLocal<ILoggerFactory> _localLoggerFac = new();

        public static void SetCurrentFactory(ILoggerFactory loggerFactory)
        {
            _localLoggerFac.Value = loggerFactory;
        }

        public void AddProvider(ILoggerProvider provider)
            => _localLoggerFac.Value?.AddProvider(provider);

        public ILogger CreateLogger(string categoryName)
            => new AsyncLocalLoggerWrapper(categoryName, CreateLocalLogger(categoryName));

        public static ILogger? CreateLocalLogger(string categoryName)
            => _localLoggerFac.Value?.CreateLogger(categoryName);

        public void Dispose()
            => _localLoggerFac.Value?.Dispose();
    }
    
    private class AsyncLocalLoggerWrapper : ILogger
    {
        private readonly AsyncLocal<ILogger?> _localLogger = new();
        private readonly string _categoryName;

        public AsyncLocalLoggerWrapper(string categoryName, ILogger? logger)
        {
            _categoryName = categoryName;
            _localLogger.Value = logger;
        }

        private ILogger GetLogger()
        {
            var log = _localLogger.Value;
            log ??= _localLogger.Value = AsyncLocalLoggerFactoryWrapper.CreateLocalLogger(_categoryName);
            return log ?? StaticLogManager.CachedNoopLogger;
        }

        public IDisposable? BeginScope<TState>(TState state) where TState : notnull
            => GetLogger().BeginScope(state);

        public bool IsEnabled(LogLevel logLevel)
            => GetLogger().IsEnabled(logLevel);

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception? exception, Func<TState, Exception?, string> formatter)
            => GetLogger().Log(logLevel, eventId, state, exception, formatter);
    }
#endregion
}
