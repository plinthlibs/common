﻿#region License
/*
 * NReco file logging provider (https://github.com/nreco/logging)
 * Copyright 2017 Vitaliy Fedorchenko
 * Distributed under the MIT license
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Borrowed for Plinth
 */
#endregion

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Microsoft.Extensions.Logging;
using Plinth.Common.Extensions;

namespace /*NReco.Logging.File*/ Plinth.Logging.Host.FileLogging;

internal static class FileLoggerExtensions
{
    /// <summary>
    /// Adds a file logger.
    /// </summary>
    public static ILoggingBuilder AddFile(this ILoggingBuilder builder, string fileName, Action<FileLoggerOptions> configure)
    {
        builder.Services.Add(ServiceDescriptor.Singleton<ILoggerProvider, FileLoggerProvider>(
            (srvPrv) =>
            {
                var options = new FileLoggerOptions();
                configure(options);
                return new FileLoggerProvider(fileName, LogLevel.Trace, options);
            }
        ));
        return builder;
    }

    /// <summary>
    /// Adds a file logger by specified configuration.
    /// </summary>
    /// <remarks>File logger is not added if "File" section is not present or it doesn't contain "Path" property.</remarks>
    public static ILoggingBuilder AddFile(this ILoggingBuilder builder, IConfiguration configuration)
    {
        var fileLoggerPrv = CreateFromConfiguration(configuration);
        if (fileLoggerPrv != null)
        {
            builder.Services.AddSingleton<ILoggerProvider, FileLoggerProvider>(
                (srvPrv) =>
                {
                    return fileLoggerPrv;
                }
            );
        }
        return builder;
    }

    /// <summary>
    /// Adds a file logger and configures it with given <see cref="IConfiguration"/> (usually "Logging" section).
    /// </summary>
    /// <param name="factory">The <see cref="ILoggerFactory"/> to use.</param>
    /// <param name="configuration">The <see cref="IConfiguration"/> to use getting <see cref="FileLoggerProvider"/> settings.</param>
    public static ILoggerFactory AddFile(this ILoggerFactory factory, IConfiguration configuration)
    {
        var prvFactory = factory;
        var fileLoggerPrv = CreateFromConfiguration(configuration);
        if (fileLoggerPrv == null)
            return factory;

        prvFactory.AddProvider(fileLoggerPrv);
        return factory;
    }

    private static FileLoggerProvider? CreateFromConfiguration(IConfiguration configuration)
    {
        var fileSection = configuration.GetSection("File");
        if (fileSection == null)
            return null;  // file logger is not configured
        var fileName = fileSection["Path"];
        if (string.IsNullOrWhiteSpace(fileName))
            return null; // file logger is not configured

        var fileLoggerOptions = new FileLoggerOptions();
        var appendVal = fileSection["Append"];
        if (!string.IsNullOrEmpty(appendVal) && bool.TryParse(appendVal, out var append))
            fileLoggerOptions.Append = append;

        var fileLimitVal = fileSection["FileSizeLimitBytes"];
        if (!string.IsNullOrEmpty(fileLimitVal) && long.TryParse(fileLimitVal, out var fileLimit))
            fileLoggerOptions.FileSizeLimitBytes = fileLimit;

        var maxFilesVal = fileSection["MaxRollingFiles"];
        if (!string.IsNullOrEmpty(maxFilesVal) && int.TryParse(maxFilesVal, out var maxFiles))
            fileLoggerOptions.MaxRollingFiles = maxFiles;

        var minLevelVal = fileSection["MinLevel"];
        var minLevel = LogLevel.Information;
        if (!string.IsNullOrEmpty(minLevelVal))
            minLevel = minLevelVal.ToEnumOrDefault(LogLevel.Information);

        return new FileLoggerProvider(fileName, minLevel, fileLoggerOptions);
    }
}

