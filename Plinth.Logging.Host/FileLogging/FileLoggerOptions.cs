﻿#region License
/*
 * NReco file logging provider (https://github.com/nreco/logging)
 * Copyright 2017-2018 Vitaliy Fedorchenko
 * Distributed under the MIT license
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Borrowed for Plinth
 */
#endregion


namespace /*NReco.Logging.File*/ Plinth.Logging.Host;

/// <summary>
/// Generic file logger options.
/// </summary>
public class FileLoggerOptions
{
    /// <summary>
    /// Append to the existing file, or replace it on startup, defaults to true
    /// </summary>
    public bool Append { get; set; } = true;

    /// <summary>
    /// Enables log file rolling when the log reaches the specified size
    /// </summary>
    /// <remarks>If log file limit is specified the logger will create a new file when the limit is reached. 
    /// For example, if log file name is 'test.log', logger will create 'test1.log', 'test2.log' etc.
    /// </remarks>
    public long FileSizeLimitBytes { get; set; } = 0;

    /// <summary>
    /// Max number of log files if <see cref="FileSizeLimitBytes"/> is specified.
    /// </summary>
    /// <remarks>If MaxRollingFiles is specified file logger will re-write previously created log files.
    /// For example, if log file name is 'test.log' and max files = 3, logger will use: 'test.log', then 'test1.log', then 'test2.log' and then 'test.log' again (old content is removed).
    /// </remarks>
    public int MaxRollingFiles { get; set; } = 0;
}
