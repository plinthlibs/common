#region License
/*
 * NReco file logging provider (https://github.com/nreco/logging)
 * Copyright 2017 Vitaliy Fedorchenko
 * Distributed under the MIT license
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Borrowed for Plinth
 */
#endregion

using System.Text;

using Microsoft.Extensions.Logging;

namespace /*NReco.Logging.File*/ Plinth.Logging.Host.FileLogging;

/// <summary>
/// Generic file logger that works in a similar way to standard ConsoleLogger.
/// </summary>
internal class FileLogger(string logName, FileLoggerProvider loggerPrv) : ILogger
{
    public IDisposable? BeginScope<TState>(TState state) where TState : notnull
    {
        return new NoopIDisposable();
    }

    private class NoopIDisposable : IDisposable
    {
        public void Dispose() { }
    }

    public bool IsEnabled(LogLevel logLevel)
    {
        return logLevel >= loggerPrv.MinLevel;
    }

    static string GetShortLogLevel(LogLevel logLevel)
    {
        return logLevel switch
        {
            LogLevel.Trace => "TRCE",
            LogLevel.Debug => "DBUG",
            LogLevel.Information => "INFO",
            LogLevel.Warning => "WARN",
            LogLevel.Error => "FAIL",
            LogLevel.Critical => "CRIT",
            _ => logLevel.ToString().ToUpper(),
        };
    }

    public void Log<TState>(LogLevel logLevel, EventId eventId, TState state,
        Exception? exception, Func<TState, Exception?, string> formatter)
    {
        if (!IsEnabled(logLevel))
        {
            return;
        }

        ArgumentNullException.ThrowIfNull(formatter);

        string message = formatter.Invoke(state, exception) ?? string.Empty;

        var tformLogName = TransformLogName(logName, 35);

        // default formatting logic
        var logBuilder = new StringBuilder(message.Length + 38 + tformLogName.Length);
        if (!string.IsNullOrEmpty(message))
        {
            logBuilder.Append(DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.ffffff"));
            logBuilder.Append(' ');
            logBuilder.Append('[').Append(Environment.CurrentManagedThreadId).Append(']');
            logBuilder.Append(' ');
            logBuilder.Append(GetShortLogLevel(logLevel));
            logBuilder.Append(' ').Append('[');
            logBuilder.Append(tformLogName);
            logBuilder.Append(']').Append(' ');
            logBuilder.Append(message);
        }

        if (exception != null)
        {
            // exception message
            logBuilder.AppendLine(exception.ToString());
        }
        loggerPrv.WriteEntry(logBuilder.ToString());
    }

    private static string TransformLogName(string text, int len)
    {
        if (text.Length > len)
        {
            return text[^len..];
        }
        else if (text.Length < len)
        {
            return text.PadLeft(len);
        }
        else
        {
            return text;
        }
    }
}
