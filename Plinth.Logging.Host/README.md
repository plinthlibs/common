# README #

### Plinth.Logging.Host ###

**For applications to log to standard targets using the Plinth logging ecosystem**

* StaticLogManagerSetup
	* Utility for configuring logging in Program.cs
    * Supports Console, Debug, File logging
    * Supports single threaded logging (using passed in ILogger or ILoggerFactory)
* FileLogging
    * An implementation of ILoggerProvider which supports basic file logging which support append and rolling files based on size
