# README

### Plinth.HttpApiClient.Common

**Common package for use with Plinth Http API Client packages**

:point_right: This package should not be referenced directly, use `Plinth.HttpApiClient` or `Plinth.HttpApiClient.RestSharp`
