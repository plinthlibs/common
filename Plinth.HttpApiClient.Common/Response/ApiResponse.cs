using Plinth.Common.Logging;
using System.ComponentModel.DataAnnotations;

namespace Plinth.HttpApiClient.Response;

/// <summary>
/// ApiResponse Model Object
/// </summary>
public class ApiResponse
{
    /// <summary>
    /// Whether or not the operation was successful
    /// </summary>
    [Required]
    public bool Success { get; set; }

    /// <summary>
    /// Errors that prevented successful completion of the operation. Only present when Success = false
    /// </summary>
    public List<ApiError>? Errors { get; set; }

    /// <summary>
    /// Additional metadata for the response
    /// </summary>
    public Dictionary<string, object>? Meta { get; set; }


    /// <summary>
    /// Constructor
    /// </summary>
    public ApiResponse() : this(true)
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    public ApiResponse(bool success)
    {
        Success = success;

        InitMeta();
    }

    /// <summary>
    /// Constructor
    /// </summary>
    public ApiResponse(IEnumerable<ApiError> errors)
        : this(false)
    {
        Errors = errors.ToList();
    }

    /// <summary>
    /// Constructor
    /// </summary>
    public ApiResponse(params ApiError[] errors)
        : this((IEnumerable<ApiError>)errors)
    {
    }

    private void InitMeta()
    {
        var meta = new Dictionary<string, object>();

        // Set RequestTraceId
        var rId = RequestTraceId.Instance.Get();
        if (!string.IsNullOrEmpty(rId))
            meta["RequestTraceId"] = rId;

        if (meta.Count != 0)
            Meta = meta;
    }
}

/// <summary>
/// ApiResponse model object with an embedded object
/// </summary>
/// <typeparam name="T"></typeparam>
public class ApiResponse<T> : ApiResponse
{
    /// <summary>
    /// Response data
    /// </summary>
    public T? Data { get; set; }

    /// <summary>
    /// Constructor
    /// </summary>
    public ApiResponse() : base(true)
    {
    }


    /// <summary>
    /// Constructor
    /// </summary>
    public ApiResponse(bool success) : base(success)
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    public ApiResponse(IEnumerable<ApiError> errors) : base(errors)
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    public ApiResponse(params ApiError[] errors) : base(errors)
    {
    }
}
