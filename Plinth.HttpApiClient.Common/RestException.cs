using System.Net;

namespace Plinth.HttpApiClient.Common;

/// <summary>
/// Exception if rest call returned an HTTP error code
/// </summary>
/// <param name="statusCode"></param>
/// <param name="message"></param>
/// <param name="responseContent"></param>
public class RestException(HttpStatusCode statusCode, string? message, string? responseContent)
    : Exception(message ?? "Status code = " + statusCode)
{
    /// <summary>
    /// HTTP Status Code returned from call
    /// </summary>
    public HttpStatusCode StatusCode { get; set; } = statusCode;

    /// <summary>
    /// Content of the response
    /// </summary>
    public string? ResponseContent { get; set; } = responseContent;
}
