using System.ComponentModel.DataAnnotations;

namespace Plinth.HttpApiClient.Validation;

/// <summary>
/// Validate at least one of the fields are provided
/// </summary>
public sealed class RequiredAtLeastOneOfAttribute(params string[] propertyNames) : RequiredAttribute
{
    /// <summary>
    /// Override IsValid
    /// </summary>
    /// <param name="value"></param>
    /// <param name="validationContext"></param>
    /// <returns></returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        object instance = validationContext.ObjectInstance;
        Type type = instance.GetType();

        foreach (var propName in propertyNames)
        {
            object? propertyValue = type.GetProperty(propName)?.GetValue(instance, null);

            if (propertyValue != null)
                return ValidationResult.Success;
        }

        return new ValidationResult($"you must supply at least one of [{string.Join(", ", propertyNames)}]");
    }
}
