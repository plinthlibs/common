using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Plinth.HttpApiClient.Validation;

/// <summary>
/// Validates a social security number.
/// </summary>
public sealed partial class SocialSecurityNumberAttribute : ValidationAttribute
{
#if NET8_0_OR_GREATER
    [GeneratedRegex(@"^\d{3}-\d{2}-\d{4}$")]
    private static partial Regex SsnRegex();
#else
    private static readonly Regex _ssnRegex = new(@"^\d{3}-\d{2}-\d{4}$", RegexOptions.Compiled);
    private static Regex SsnRegex() => _ssnRegex;
#endif

    /// <summary>
    /// Initializes a new instance of the SocialSecurityNumberAttribute class with the default error message.
    /// </summary>
    public SocialSecurityNumberAttribute()
        : base("The {0} field must be a social security number.")
    {
    }

    /// <summary>
    /// Initializes a new instance of the SocialSecurityNumberAttribute class with an error message.
    /// </summary>
    public SocialSecurityNumberAttribute(string errorMessage) : base(errorMessage)
    {
    }

    /// <summary>
    /// Override IsValid
    /// </summary>
    /// <param name="value"></param>
    /// <param name="validationContext"></param>
    /// <returns></returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        var ssn = value?.ToString();

        if (ssn == null)
            return ValidationResult.Success;

        var isValid = SsnRegex().IsMatch(ssn);

        return isValid ? ValidationResult.Success : new ValidationResult(FormatErrorMessage(validationContext.MemberName ?? "<unknown>"));
    }
}

