using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Plinth.HttpApiClient.Validation;

/// <summary>
/// Validates a zip code.
/// </summary>
public sealed partial class ZipCodeAttribute : ValidationAttribute
{
#if NET8_0_OR_GREATER
    [GeneratedRegex(@"^\d{5}(-\d{4})?$")]
    private static partial Regex ZipCodeRegex();
#else
    private static readonly Regex _zipCodeRegex = new(@"^\d{5}(-\d{4})?$", RegexOptions.Compiled);
    private static Regex ZipCodeRegex() => _zipCodeRegex;
#endif

    /// <summary>
    /// Initializes a new instance of the ZipCodeAttribute class with the default error message.
    /// </summary>
    public ZipCodeAttribute()
        : base("The {0} field must be a zip code.")
    {
    }

    /// <summary>
    /// Initializes a new instance of the ZipCodeAttribute class with an error message.
    /// </summary>
    public ZipCodeAttribute(string errorMessage) : base(errorMessage)
    {
    }

    /// <summary>
    /// Override IsValid
    /// </summary>
    /// <param name="value"></param>
    /// <param name="validationContext"></param>
    /// <returns></returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        var zip = value?.ToString();

        if (zip == null)
            return ValidationResult.Success;

        var isValid = ZipCodeRegex().IsMatch(zip);

        return isValid ? ValidationResult.Success : new ValidationResult(FormatErrorMessage(validationContext.MemberName ?? "<unknown>"));
    }
}
