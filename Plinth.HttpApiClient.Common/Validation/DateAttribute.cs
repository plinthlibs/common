﻿using System.ComponentModel.DataAnnotations;

namespace Plinth.HttpApiClient.Validation;

/// <summary>
/// Validates a date.
/// </summary>
public sealed class DateAttribute : ValidationAttribute
{
    /// <summary>
    /// Initializes a new instance of the DateAttribute class with the default error message.
    /// </summary>
    public DateAttribute()
        : base("The {0} field must be a date.")
    {
    }

    /// <summary>
    /// Initializes a new instance of the DateAttribute class with an error message.
    /// </summary>
    public DateAttribute(string errorMessage) : base(errorMessage)
    {
    }

    /// <summary>
    /// Override IsValid
    /// </summary>
    /// <param name="value"></param>
    /// <param name="validationContext"></param>
    /// <returns></returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if (value == null)
            return ValidationResult.Success;

        var isValid = DateTime.TryParse(Convert.ToString(value), out DateTime _);

        return isValid ? ValidationResult.Success : new ValidationResult(FormatErrorMessage(validationContext.MemberName ?? "<unknown>"));
    }
}
