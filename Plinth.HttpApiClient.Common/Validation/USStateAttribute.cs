using System.ComponentModel.DataAnnotations;

namespace Plinth.HttpApiClient.Validation;

/// <summary>
/// Validate valid US State
/// </summary>
public sealed class USStateAttribute : ValidationAttribute
{
    private static readonly HashSet<string> ValidStates =
    [
        "AL",
        "AK",
        "AZ",
        "AR",
        "CA",
        "CO",
        "CT",
        "DE",
        "DC",
        "FL",
        "GA",
        "HI",
        "ID",
        "IL",
        "IN",
        "IA",
        "KS",
        "KY",
        "LA",
        "ME",
        "MD",
        "MA",
        "MI",
        "MN",
        "MS",
        "MO",
        "MT",
        "NE",
        "NV",
        "NH",
        "NJ",
        "NM",
        "NY",
        "NC",
        "ND",
        "OH",
        "OK",
        "OR",
        "PA",
        "RI",
        "SC",
        "SD",
        "TN",
        "TX",
        "UT",
        "VT",
        "VA",
        "WA",
        "WV",
        "WI",
        "WY"
    ];

    private const string _ErrorMessage = "Invalid state";

    /// <summary>
    /// Constructor
    /// </summary>
    public USStateAttribute()
    {
        ErrorMessage = _ErrorMessage;
    }

    /// <summary>
    /// Override IsValid
    /// </summary>
    /// <param name="value"></param>
    /// <param name="validationContext"></param>
    /// <returns></returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if (value == null)
            return ValidationResult.Success;

        return (value is string s && ValidStates.Contains(s.ToUpperInvariant())) 
            ? ValidationResult.Success 
            : new ValidationResult(ErrorMessage);
    }
}
