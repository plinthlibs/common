using System.ComponentModel.DataAnnotations;

namespace Plinth.HttpApiClient.Validation;

/// <summary>
/// Validate required field exists
/// </summary>
public sealed class RequiredIfAttribute(string propertyName, object desiredValue) : RequiredAttribute
{
    /// <summary>
    /// Override IsValid
    /// </summary>
    /// <param name="value"></param>
    /// <param name="validationContext"></param>
    /// <returns></returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        object instance = validationContext.ObjectInstance;
        Type type = instance.GetType();

        var propertyValue = type.GetProperty(propertyName)?.GetValue(instance, null);
        if (object.Equals(propertyValue, desiredValue))
            return base.IsValid(value, validationContext);

        return ValidationResult.Success;
    }
}
