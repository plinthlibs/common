using System.ComponentModel.DataAnnotations;

namespace Plinth.HttpApiClient.Validation;

/// <summary>
/// Validate that only one field is provided
/// </summary>
public sealed class RequiredOnlyOneOfAttribute(params string[] propertyNames) : RequiredAttribute
{
    /// <summary>
    /// Override IsValid
    /// </summary>
    /// <param name="value"></param>
    /// <param name="validationContext"></param>
    /// <returns></returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        object instance = validationContext.ObjectInstance;
        Type type = instance.GetType();

        bool oneFound = false;
        foreach (var propName in propertyNames)
        {
            var propertyValue = type.GetProperty(propName)?.GetValue(instance, null);

            if (propertyValue != null)
            {
                if (oneFound)
                    return new ValidationResult($"only supply one of [{string.Join(", ", propertyNames)}]");
                oneFound = true;
            }
        }

        if (!oneFound)
            return new ValidationResult($"you must supply one of [{string.Join(", ", propertyNames)}]");

        return ValidationResult.Success;
    }
}
