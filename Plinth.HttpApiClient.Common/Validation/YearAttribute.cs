using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Plinth.HttpApiClient.Validation;

/// <summary>
/// Validates a year.
/// </summary>
public sealed partial class YearAttribute : ValidationAttribute
{
#if NET8_0_OR_GREATER
    [GeneratedRegex(@"^\d{4}$")]
    private static partial Regex YearRegex();
#else
    private static readonly Regex _yearRegex = new(@"^\d{4}$", RegexOptions.Compiled);
    private static Regex YearRegex() => _yearRegex;
#endif

    /// <summary>
    /// Initializes a new instance of the YearAttribute class with the default error message.
    /// </summary>
    public YearAttribute()
        : base("The {0} field must be a year.")
    {
    }

    /// <summary>
    /// Initializes a new instance of the YearAttribute class with an error message.
    /// </summary>
    public YearAttribute(string errorMessage) : base(errorMessage)
    {
    }

    /// <summary>
    /// Override IsValid
    /// </summary>
    /// <param name="value"></param>
    /// <param name="validationContext"></param>
    /// <returns></returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        var year = value?.ToString();

        if (year == null)
            return ValidationResult.Success;

        var isValid = YearRegex().IsMatch(year);

        return isValid ? ValidationResult.Success : new ValidationResult(FormatErrorMessage(validationContext.MemberName ?? "<unknown>"));
    }
}

