﻿using System.ComponentModel.DataAnnotations;
using System.Net;

namespace Plinth.HttpApiClient.Validation;

/// <summary>
/// Validate IP Address value
/// </summary>
public sealed class IPAddressAttribute : ValidationAttribute
{
    private const string _ErrorMessage = "Invalid IP address";

    /// <summary>
    /// Constructor
    /// </summary>
    public IPAddressAttribute()
    {
        ErrorMessage = _ErrorMessage;
    }

    /// <summary>
    /// Override IsValid
    /// </summary>
    /// <param name="value"></param>
    /// <param name="validationContext"></param>
    /// <returns></returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if (value == null)
            return ValidationResult.Success;

        return (value is string @string && IPAddress.TryParse(@string, out _)) 
            ? ValidationResult.Success 
            : new ValidationResult(ErrorMessage);
    }
}
