using System.ComponentModel.DataAnnotations;

namespace Plinth.HttpApiClient.Validation;

/// <summary>
/// Validates an integer greater than or equal to zero.
/// </summary>
public sealed class NonNegativeAttribute : ValidationAttribute
{
    /// <summary>
    /// Initializes a new instance of <see cref="NonNegativeAttribute"/> with the default error message.
    /// </summary>
    public NonNegativeAttribute()
        : base("The {0} field must be greater than or equal to zero.")
    {
    }

    /// <summary>
    /// Initializes a new instance of <see cref="NonNegativeAttribute"/> with an error message.
    /// </summary>
    public NonNegativeAttribute(string errorMessage) : base(errorMessage)
    {
    }

    /// <summary>
    /// Override IsValid
    /// </summary>
    /// <param name="value"></param>
    /// <param name="validationContext"></param>
    /// <returns></returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if (value == null)
            return ValidationResult.Success;

        var isValid =
            (value is int i && i >= 0) ||
            (value is long l && l >= 0) ||
            (value is float f && f >= 0) ||
            (value is decimal d && d >= 0);

        return isValid ? ValidationResult.Success : new ValidationResult(FormatErrorMessage(validationContext.MemberName ?? "<unknown>"));
    }
}
