﻿using System.ComponentModel.DataAnnotations;

namespace Plinth.HttpApiClient.Validation;

/// <summary>
/// Validate True/False (checkbox)
/// </summary>
public sealed class IsTrueAttribute : ValidationAttribute
{
    /// <summary>
    /// Override IsValid
    /// </summary>
    /// <param name="value"></param>
    /// <param name="validationContext"></param>
    /// <returns></returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if (value == null)
            return ValidationResult.Success;

        return (value is bool b && b) 
            ? ValidationResult.Success 
            : new ValidationResult(ErrorMessage);
    }
}
