using System.ComponentModel.DataAnnotations;

namespace Plinth.HttpApiClient.Common.Validation;

/// <summary>
/// Validate a date represents an age at least a minimum number of years in the past.
/// </summary>
public sealed class AgeValidationAttribute : ValidationAttribute
{
    private readonly int _minimumAge;

    /// <summary>
    /// Initializes a new instance of <see cref="AgeValidationAttribute"/> with a minimum age and the default error message.
    /// </summary>
    /// <param name="minimumAge"></param>
    public AgeValidationAttribute(int minimumAge)
        : base("The {0} field must be a date at least {1} years in the past.")
    {
        _minimumAge = minimumAge;
    }

    /// <summary>
    /// Initializes a new instance of <see cref="AgeValidationAttribute"/> with a minimum age and an error message.
    /// </summary>
    /// <param name="minimumAge"></param>
    /// <param name="errorMessage"></param>
    public AgeValidationAttribute(int minimumAge, string errorMessage)
        : base(errorMessage)
    {
        _minimumAge = minimumAge;
    }

    /// <summary>
    /// Formats the error message to present to the user.
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public override string FormatErrorMessage(string name)
    {
        return string.Format(ErrorMessageString, name, _minimumAge);
    }

    /// <summary>
    /// Override IsValid
    /// </summary>
    /// <param name="value"></param>
    /// <param name="validationContext"></param>
    /// <returns></returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if (value == null)
            return ValidationResult.Success;

        DateTime dateOfBirth = (DateTime)value;

        return dateOfBirth.AddYears(_minimumAge) <= DateTime.Now
            ? ValidationResult.Success
            : new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
    }
}
