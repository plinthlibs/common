﻿
namespace Plinth.HttpApiClient.Models;

/// <summary>
/// Derive a model object from this class to designate the model as an API model and inherit
/// common properties
/// </summary>
public class BaseApiModel
{
    /// <summary>
    /// Version of the model, to be used for backwards compatibility
    /// </summary>
    public int? Version { get; set; }

    /// <summary>
    /// This method will be called by WebApi for every request model to perform whatever
    /// backwards-compatible model upgrades that may be needed.
    /// </summary>
    public virtual void UpgradeModel()
    {
        // implement if upgrades are needed
    }
}
