# dotnet tool install -g dotnet-coverage
# dotnet tool install -g dotnet-reportgenerator-globaltool

# run ./code-coverage.ps1 {csproj files command delim}

mkdir -p Coverage -Force
remove-item ./Coverage/html -Recurse -Force

dotnet-coverage collect -l Coverage -o Coverage\coverage.cobertura.xml -f cobertura "dotnet test $args --framework net8.0"

$nugetDir = (dotnet nuget locals -l global-packages).split(': ')[1]
$env:NUGET_DIR = $nugetDir

reportgenerator `
    "-reports:Coverage/coverage.cobertura.xml" `
    "-targetdir:Coverage/html" `
    "-historydir:Coverage/history" `
    "-title:Plinth Code Coverage" `
    "-classfilters:-Moq.*;-TypeNameFormatter.TypeName;-NUnit.*;-Tests.Plinth.*;Plinth.Common.Conditions.*" `
    -reporttypes:HTML

Start-Process 'C:\Program Files\Mozilla Firefox\firefox.exe' ('file://' + (Resolve-Path -Path .\Coverage\html\index.html).Path -replace '\\', '/')

