﻿using Plinth.Storage.MSSql;
using Plinth.Database.MSSql;

namespace Plinth.Storage;

/// <summary>
/// Extensions for S3
/// </summary>
public static class MSSqlBlobStorageFactoryExtensions
{
    /// <summary>
    /// Get an IBlobStorage
    /// </summary>
    /// <returns>IBlobStorage</returns>
    public static IStorage Get(this StorageFactory fac, ISqlConnection conn, string callingUser)
        => fac.CreateFromIndexDriver(
            new BlobIndexDAO(conn, callingUser, fac.GetIndexDriverParameter(MSSqlBlobDriver.DriverSchemaParam, "dbo")));
}
