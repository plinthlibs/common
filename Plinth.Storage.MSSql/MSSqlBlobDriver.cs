namespace Plinth.Storage.MSSql;

/// <summary>
/// SQL Server Blob Driver constants and utilities
/// </summary>
public static class MSSqlBlobDriver
{
    /// <summary>
    /// Specify this parameter to set the schema to a custom schema
    /// </summary>
    public const string DriverSchemaParam = "schema";
}
