using Plinth.Storage.Models;
using System.Data;
using Microsoft.Data.SqlClient;
using Plinth.Database.MSSql;

namespace Plinth.Storage.MSSql;

/// <summary>
/// Internal DAO for managing blobs
/// When using database storage, the data is stored in the blob record as well
/// </summary>
internal class BlobIndexDAO : IStorageIndexDriver
{
    private readonly ISqlConnection _connection;
    private readonly string _callingUser;
    private readonly string _schema;

    public BlobIndexDAO(ISqlConnection connection, string callingUser, string? schema = "dbo")
    {
        _connection = connection;
        _callingUser = callingUser;
        _schema = schema ?? "dbo";
    }

    /// <summary>
    /// SQL Server can stream blobs out of a column
    /// </summary>
    public bool SupportsStreamRead => true;

    /// <summary>
    /// SQL Server can stream blobs into a column
    /// </summary>
    public bool SupportsStreamWrite => true;

    /// <summary>
    /// Retrieves the blob with the specified guid
    /// </summary>
    /// <param name="guid"></param>
    /// <param name="loadData">if using database storage, true will also load the data (left as encrypted)</param>
    /// <param name="cancellationToken"></param>
    /// <returns>a blob model object or null if the blob with the specified guid does not exist</returns>
    public async Task<Blob?> GetBlobByGUIDAsync(Guid guid, bool loadData = false, CancellationToken cancellationToken = default)
    {
        var b = await _connection.ExecuteQueryProcOneAsync(
            $"{_schema}.usp_GetBLOBByGUID",
            (rs) => Task.FromResult(LoadBlob(rs, loadData)),
            cancellationToken,
            new SqlParameter("@GUID", guid)
        );
        return b.Value;
    }

    /// <summary>
    /// Retrieves the blob with the specified guid and a stream to the data (if non null)
    /// </summary>
    /// <param name="guid"></param>
    /// <param name="loadData">if using database storage, true will also load the data (left as encrypted)</param>
    /// <param name="cancellationToken"></param>
    /// <returns>a blob model object or null if the blob with the specified guid does not exist</returns>
    public async Task<(Blob? blob, Stream? stream)> GetBlobStreamByGUIDAsync(Guid guid, bool loadData = false, CancellationToken cancellationToken = default)
    {
        var b = await _connection.ExecuteQueryProcOneAsync(
            $"{_schema}.usp_GetBLOBByGUID",
            (rs) =>
            {
                var blob = LoadBlob(rs, false);
                return Task.FromResult((blob, loadData ? rs.GetStream("Data") : null));
            },
            cancellationToken,
            new SqlParameter("@GUID", guid)
        );
        return b.Value;
    }

    private static Blob LoadBlob(IResult rs, bool loadData)
        => new()
        {
            Guid = rs.GetGuid("GUID"),
            Name = rs.GetString("Name")!,
            Provider = rs.GetString("Provider")!,
            MimeType = rs.GetString("MIMEType")!,
            BlobSize = rs.GetLong("OrigSize"),
            StoredSize = rs.GetLong("StoredSize"),
            Index = rs.GetString("BLOBIndex"),
            Data = loadData ? rs.GetBytes("Data") : null,
            Features = 
                (rs.GetBool("IsEncrypted") ? BlobFeatures.Encrypted : BlobFeatures.None)
              | (rs.GetBool("IsCompressed") ? BlobFeatures.Compressed : BlobFeatures.None)
        };

    /// <summary>
    /// Async Creates a new blob
    /// </summary>
    /// <param name="provider">provider id</param>
    /// <param name="blob">blob metadata</param>
    /// <param name="blobData">encrypted data for Database Storage, null otherwise</param>
    /// <param name="origSize">size of the original data in bytes</param>
    /// <param name="storedSize">size of the data written to storage</param>
    /// <param name="cancellationToken"></param>
    public async Task CreateBlobAsync(string provider, Blob blob, byte[]? blobData, long origSize, long storedSize, CancellationToken cancellationToken)
    {
        await _connection.ExecuteProcAsync(
            $"{_schema}.usp_InsertBLOB",
            cancellationToken,
            UpsertParams(provider, blob.Guid!.Value, blob, blobData, null, origSize, storedSize)
        );
    }

    /// <summary>
    /// Async Creates a new blob from a stream
    /// </summary>
    /// <param name="provider">provider id</param>
    /// <param name="blob">blob metadata</param>
    /// <param name="stream">data for Database Storage, null otherwise</param>
    /// <param name="origSize">size of the original data in bytes</param>
    /// <param name="storedSize">size of the data written to storage</param>
    /// <param name="cancellationToken"></param>
    public async Task CreateBlobStreamAsync(string provider, Blob blob, Stream? stream, long origSize, long storedSize, CancellationToken cancellationToken)
    {
        await _connection.ExecuteProcAsync(
            $"{_schema}.usp_InsertBLOB",
            cancellationToken,
            UpsertParams(provider, blob.Guid!.Value, blob, null, stream, origSize, storedSize)
        );
    }

    public async Task UpdateBlobAsync(string? provider, Guid guid, Blob blob, byte[]? blobData, bool setDataToNull, long? origSize, long? storedSize, CancellationToken cancellationToken)
    {
        blob.Guid = guid;
        await _connection.ExecuteProcAsync(
            $"{_schema}.usp_UpdateBLOB",
            cancellationToken,
            UpsertParams(provider, blob.Guid.Value, blob, blobData, null, origSize, storedSize)
        );

        if (blobData == null && setDataToNull)
        {
            await _connection.ExecuteProcAsync(
                $"{_schema}.usp_UpdateBLOBData",
                cancellationToken,
                new SqlParameter("@GUID", guid),
                new SqlParameter("@Data", blobData),
                new SqlParameter("@CallingUser", _callingUser)
            );
        }
    }

    /// <summary>
    /// Async Updates a blob with stream data
    /// </summary>
    /// <param name="provider">provider id</param>
    /// <param name="guid">blob id</param>
    /// <param name="blob">blob metadata</param>
    /// <param name="stream">data for blob</param>
    /// <param name="origSize">size of the original data in bytes</param>
    /// <param name="storedSize">size of the data written to storage</param>
    /// <param name="cancellationToken"></param>
    public async Task UpdateBlobAsync(string? provider, Guid guid, Blob blob, Stream? stream, long? origSize, long? storedSize, CancellationToken cancellationToken)
    {
        blob.Guid = guid;
        await _connection.ExecuteProcAsync(
            $"{_schema}.usp_UpdateBLOB",
            cancellationToken,
            UpsertParams(provider, blob.Guid.Value, blob, null, stream, origSize, storedSize)
        );
    }

    /// <summary>
    /// Async Updates a blob index sizes, used for streaming
    /// </summary>
    /// <param name="guid">blob id</param>
    /// <param name="origSize">size of the original data in bytes</param>
    /// <param name="storedSize">size of the data written to storage</param>
    /// <param name="cancellationToken"></param>
    public async Task UpdateBlobSizesAsync(Guid guid, long origSize, long storedSize, CancellationToken cancellationToken)
    {
        await _connection.ExecuteProcAsync(
            $"{_schema}.usp_UpdateBLOBSizes",
            cancellationToken,
            new SqlParameter("@GUID", guid),
            new SqlParameter("@OrigSize", origSize),
            new SqlParameter("@StoredSize", storedSize),
            new SqlParameter("@CallingUser", _callingUser)
        );
    }

    private SqlParameter[] UpsertParams(string? provider, Guid guid, Blob blob, byte[]? blobData, Stream? stream, long? origSize, long? storedSize)
        =>
        [
            new SqlParameter("@GUID", guid),
            new SqlParameter("@Name", blob.Name),
            new SqlParameter("@Provider", provider),
            new SqlParameter("@BLOBIndex", blob.Index),
            new SqlParameter("@MIMEType", blob.MimeType),
            blobData == null 
                ? (stream == null
                    ? new SqlParameter("@Data", null)
                    : new SqlParameter("@Data", SqlDbType.VarBinary) { Value = stream })
                : new SqlParameter("@Data", blobData),
            new SqlParameter("@OrigSize", origSize),
            new SqlParameter("@StoredSize", storedSize),
            new SqlParameter("@IsEncrypted", blob.Features.HasFlag(BlobFeatures.Encrypted)),
            new SqlParameter("@IsCompressed", blob.Features.HasFlag(BlobFeatures.Compressed)),
            new SqlParameter("@CallingUser", _callingUser)
        ];

    public async Task DeleteBlobAsync(Guid origGuid, CancellationToken cancellationToken)
    {
        await _connection.ExecuteProcAsync(
            $"{_schema}.usp_DeleteBLOBByGuid",
            cancellationToken,
            new SqlParameter("@GUID", origGuid),
            new SqlParameter("@CallingUser", _callingUser)
        );
    }

    public void AddPostCommitAction(string desc, Action postCommit)
        => _connection.AddPostCommitAction(desc, postCommit);

    public void AddAsyncPostCommitAction(string desc, Func<Task> postCommitAsync)
        => _connection.AddAsyncPostCommitAction(desc, postCommitAsync);

    public void AddRollbackAction(string desc, Action onRollback)
        => _connection.AddRollbackAction(desc, onRollback);

    public void AddAsyncRollbackAction(string desc, Func<Task> onRollbackAsync)
        => _connection.AddAsyncRollbackAction(desc, onRollbackAsync);
}
