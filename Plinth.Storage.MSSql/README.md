# README

### Plinth.Storage.MSSql

**Storage Index driver for Microsoft SQL Server**

Enables storing the blob index used by _Plinth.Storage_ in Microsoft SQL Server

:point_right: In the package, find the `Procedures.sql` and `Tables.sql` files.  Include those in your database scripts.  It is required by the framework that the procedures and tables be available.

This package adds this extension method to `Plinth.Storage.StorageFactory` to utilize SQL Server for the index.

```c#
var blobStorage = storageFactory.Get(connection, callingUser);
```
If you want to read blob **data** from the database, include this line when constructing the factory in Startup
```c#
    storageFactory.AddDatabaseProvider()
```
If you want to write blob **data** to the database (only recommended for testing), include this line
```c#
    storageFactory.SetDefaultWriteProviderAsDatabase()
```
