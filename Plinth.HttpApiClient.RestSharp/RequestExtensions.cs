﻿using RestSharp;

namespace Plinth.HttpApiClient.RestSharp;

/// <summary>
/// Extensions methods for API requests.
/// </summary>
public static class RequestExtensions
{
    /// <summary>
    /// Adds query parameter, automatically converting its value to string using ToString().
    /// </summary>
    public static RestRequest AddQueryParameter(this RestRequest request, string name, object? value)
    {
        return request.AddParameter(name, value?.ToString()!, ParameterType.QueryString);
    }

    /// <summary>
    /// Adds a list as a query parameter, automatically converting its values to string using ToString().
    /// </summary>
    public static RestRequest AddQueryParameter<T>(this RestRequest request, string name, IEnumerable<T?>? values)
    {
        if (values == null)
            return request;

        foreach (var value in values)
            request.AddParameter(name, value?.ToString()!, ParameterType.QueryString);

        return request;
    }

    /// <summary>
    /// Adds optional query parameter. Will not add if it is null, empty or has a default value for value types.
    /// </summary>
    public static RestRequest AddQueryOptional<T>(this RestRequest request, string name, T? value)
    {
        // to avoid boxing and support structs and Nullable<T>
        if (EqualityComparer<T>.Default.Equals(value, default))
            return request;

        return request.AddParameter(name, value?.ToString()!, ParameterType.QueryString);
    }

    /// <summary>
    /// Adds URL segment, automatically converting its value to string using ToString().
    /// </summary>
    public static RestRequest AddUrlSegment(this RestRequest request, string name, object? value)
    {
        return request.AddParameter(name, value?.ToString()!, ParameterType.UrlSegment);
    }
}
