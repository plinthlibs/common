using Plinth.Common.Constants;
using Newtonsoft.Json;

namespace Plinth.Serialization;

/// <summary>
/// A Serializer that allows RestSharp to serialize json requests using Json.Net
/// </summary>
public class RestSharpJsonNetSerializer(Action<JsonSerializerSettings>? jsonSettings = null) : RestSharp.Serializers.ISerializer
{
    /// <summary>
    /// Handled Content Type
    /// </summary>
    public RestSharp.ContentType ContentType { get; set; } = RestSharp.ContentType.Json;

    /// <summary>
    /// Serialize an object into json format for sending
    /// </summary>
    public string? Serialize(object obj)
    {
        return JsonUtil.SerializeObject(obj, jsonSettings);
    }
}
