using Newtonsoft.Json;
using Plinth.Common.Constants;
using Plinth.Serialization;
using RestSharp;
using RestSharp.Serializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plinth.HttpApiClient.RestSharp.Serialization;

/// <summary>
/// Serializer for RestSharp that uses Json.net via <see cref="Plinth.Serialization.JsonUtil"/>
/// </summary>
/// <example>to use with custom serialization,, do something like this in your derived client constructor
/// <code>RestClient.UseSerializer(() => new RestSharpJsonNetClientSerializer(..., ...));</code>
/// </example>
public class RestSharpJsonNetClientSerializer : IRestSerializer
{
    /// <inheritdoc/>
    public ISerializer Serializer { get; }

    /// <inheritdoc/>
    public IDeserializer Deserializer { get; }

    /// <inheritdoc/>
    public DataFormat DataFormat => DataFormat.Json;

    /// <inheritdoc/>
    public string[] AcceptedContentTypes => ContentType.JsonAccept;

    /// <inheritdoc/>
    public SupportsContentType SupportsContentType => ct => ct.Value.Contains("json");

    /// <summary>
    /// Construct with default JsonUtil settings
    /// </summary>
    public RestSharpJsonNetClientSerializer()
        : this(null)
    {
    }

    /// <summary>
    /// Construct with custom serializer settings
    /// </summary>
    /// <param name="serializerSettings"></param>
    public RestSharpJsonNetClientSerializer(Action<JsonSerializerSettings>? serializerSettings)
    {
        Serializer = new RestSharpJsonNetSerializer(serializerSettings);
        Deserializer = new RestSharpJsonNetDeserializer(serializerSettings);
    }

    /// <summary>
    /// Construct with custom serializer settings for each direction
    /// </summary>
    /// <param name="serializerSettings"></param>
    /// <param name="deserializerSettings"></param>
    public RestSharpJsonNetClientSerializer(Action<JsonSerializerSettings>? serializerSettings, Action<JsonSerializerSettings>? deserializerSettings)
    {
        Serializer = new RestSharpJsonNetSerializer(serializerSettings);
        Deserializer = new RestSharpJsonNetDeserializer(deserializerSettings);
    }

    /// <inheritdoc/>
    public string? Serialize(Parameter parameter)
    {
        return Serializer.Serialize(parameter.Value ?? string.Empty);
    }
}
