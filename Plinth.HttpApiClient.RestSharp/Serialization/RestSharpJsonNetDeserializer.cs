using Newtonsoft.Json;
using Plinth.Serialization;
using RestSharp;
using RestSharp.Serializers;

namespace Plinth.HttpApiClient.RestSharp.Serialization;

/// <summary>
/// A Deserializer that allows RestSharp to deserialize json responses using Json.Net
/// </summary>
public class RestSharpJsonNetDeserializer(Action<JsonSerializerSettings>? jsonSettings = null) : IDeserializer
{
    /// <summary>
    /// Deserialize a rest response into an object via Json.net
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="response"></param>
    /// <returns></returns>
    public T? Deserialize<T>(RestResponse response)
    {
        return JsonUtil.DeserializeObject<T>(response.Content ?? string.Empty, jsonSettings);
    }
}
