
# README

### Plinth.HttpApiClient.RestSharp

**HTTP Api Client framework using RestSharp**

Adds logging and tracing to RestSharp based API clients.  
> :warning: **Recommended**: Prefer `Plinth.HttpApiClient` to using RestSharp
> :warning: **Recommended**: If using RestSharp, prefer injecting an HttpClient via Dependency Injection or HttpClientFactory

#  RestSharp based client

Build a manual api client by deriving from `BaseHttpApiClient` and implementing API methods using `RestSharp.RestRequest` extensions.  Example below:

```c#
public class MyClient : BaseHttpApiClient
{
    public MyClient(string serverUrl, HttpClient client) : base("MyApi", serverUrl, client)
    {
    }

    public async Task<ModelObject?> GetThing(int parameter)
    {
        return await ExecuteAsync<ModelObject>(HttpGet("/api/values/thing")
	        .AddQueryParameter("param", parameter));
    }

    public async Task<ModelObject?> CreateThing(ModelObject modelObject)
    {
        return await ExecuteAsync<ModelObject>(HttpPost("/api/values/thing")
          .AddJsonBody(modelObject));
    }
}
```


