using Microsoft.Extensions.Logging;
using Plinth.Common.Constants;
using Plinth.Common.Extensions;
using Plinth.Common.Logging;
using Plinth.Common.Utils;
using Plinth.HttpApiClient.Common;
using Plinth.HttpApiClient.RestSharp.Serialization;
using Plinth.Serialization;
using RestSharp;
using RestSharp.Serializers;
using System.Diagnostics;
using System.Net;
using static Plinth.Common.Logging.Scopes;

namespace Plinth.HttpApiClient.RestSharp;

/// <summary>
/// Base class for ApiClients using RestSharp
/// </summary>
/// <remarks>
/// It is strongly recommended to use the HttpClient based client, or even better, use generated clients.
/// </remarks>
public class BaseHttpApiClient
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private const int DefaultMaxRequestLogging = 25000;
    private const int DefaultMaxResponseLogging = 25000;

    /// <summary>
    /// Delegate type for sync interceptors.  Should NOT throw.
    /// </summary>
    /// <remarks>
    /// An interceptor receives the rest sharp request and a function which executes the request.  
    /// The interceptor can manipulate the request and response in any way desired.
    /// The intent is to allow subclasses to use this to make fallback request, add retries, modify contents, etc
    /// </remarks>
    /// <param name="req">rest sharp request to be sent</param>
    /// <param name="exec">function which will execute the request</param>
    /// <returns>a rest sharp response</returns>
    protected delegate RestResponse SyncInterceptor(RestRequest req, Func<RestRequest, RestResponse> exec);

    /// <summary>
    /// Delegate type for async interceptors.  Should NOT throw.
    /// </summary>
    /// <remarks>
    /// An interceptor receives the rest sharp request and a function which executes the request.  
    /// The interceptor can manipulate the request and response in any way desired
    /// The intent is to allow subclasses to use this to make fallback request, add retries, modify contents, etc
    /// </remarks>
    /// <param name="req">rest sharp request to be sent</param>
    /// <param name="exec">function which will execute the request</param>
    /// <returns>a rest sharp response</returns>
    protected delegate Task<RestResponse> AsyncInterceptor(RestRequest req, Func<RestRequest, Task<RestResponse>> exec);

    /// <summary>
    /// Identifier for the service
    /// </summary>
    protected string ServiceId { get; }

    /// <summary>
    /// Internal RestSharp client
    /// </summary>
    protected RestClient RestClient { get; }

    /// <summary>
    /// Log level
    /// </summary>
    protected LogLevel LogLevel { get; set; } = LogLevel.Debug;

    /// <summary>
    /// Action called just before executing a request (useful for authorization)
    /// </summary>
    protected Action<RestRequest>? BeforeExec { get; set; } = null;

    /// <summary>
    /// Action called just after executing a request (useful for global response handling)
    /// </summary>
    protected Action<RestRequest, RestResponse>? AfterExec { get; set; } = null;

    /// <summary>
    /// Action called after a request that returned a successful code (2xx)
    /// </summary>
    protected Action<RestRequest, RestResponse>? AfterSuccess { get; set; } = null;

    /// <summary>
    /// Action called after a request that returned an error code (not 2xx)
    /// </summary>
    protected Action<RestRequest, RestResponse>? AfterFailure { get; set; } = null;

    /// <summary>max bytes to log from request</summary>
    protected int MaxLoggingRequestBody { get; set; } = DefaultMaxRequestLogging;

    /// <summary>max bytes to log from response</summary>
    protected int MaxLoggingResponseBody { get; set; } = DefaultMaxResponseLogging;

    /// <summary>
    /// Construct from URL and timeout
    /// </summary>
    /// <param name="serviceId">a string ID which will be logged, to identify which service is being called</param>
    /// <param name="serverUrl">protocol and host:port only</param>
    /// <param name="timeout">(optional) timeout, if not specified will use RestSharp default</param>
    public BaseHttpApiClient(string serviceId, string serverUrl, TimeSpan? timeout)
        : this(serviceId, serverUrl, timeout, null)
    {
    }

    /// <summary>
    /// Construct from URL and timeout
    /// </summary>
    /// <param name="serviceId">a string ID which will be logged, to identify which service is being called</param>
    /// <param name="serverUrl">protocol and host:port only</param>
    /// <param name="timeout">(optional) timeout, if not specified will use RestSharp default</param>
    /// <param name="configureSerialization">(optional) override how the RestSharp client does serialization</param>
    /// <remarks>
    /// By default, serialization will use the Plinth JsonUtil serialization settings.
    /// To change this, use the configurationSerialization override.  Plinth provides RestSharpJsonNetClientSerializer
    /// for easy changes
    /// </remarks>
    public BaseHttpApiClient(string serviceId, string serverUrl, TimeSpan? timeout = null, ConfigureSerialization? configureSerialization = null)
    {
        ServiceId = serviceId;

        var rcOpts = new RestClientOptions
        {
            BaseUrl = new Uri(serverUrl)
        };

        if (timeout.HasValue)
        {
            rcOpts.Timeout = timeout;
        }

        if (configureSerialization != null)
            RestClient = new RestClient(rcOpts, configureSerialization: configureSerialization);
        else
            RestClient = new RestClient(rcOpts, configureSerialization: (c) => c.UseSerializer<RestSharpJsonNetClientSerializer>());
    }

    /// <summary>
    /// Construct from URL, HttpClient and timeout
    /// </summary>
    /// <param name="serviceId">a string ID which will be logged, to identify which service is being called</param>
    /// <param name="serverUrl">protocol and host:port only</param>
    /// <param name="httpClient">HttpClient from IHttpClientFactory</param>
    /// <param name="timeout">(optional) timeout, if not specified will use RestSharp default</param>
    /// <param name="configureSerialization">(optional) override how the RestSharp client does serialization</param>
    /// <remarks>
    /// By default, serialization will use the Plinth JsonUtil serialization settings.
    /// To change this, use the configurationSerialization override.  Plinth provides RestSharpJsonNetClientSerializer
    /// for easy changes
    /// </remarks>
    public BaseHttpApiClient(string serviceId, string serverUrl, HttpClient httpClient, TimeSpan? timeout = null, ConfigureSerialization? configureSerialization = null)
    {
        ServiceId = serviceId;

        var rcOpts = new RestClientOptions
        {
            BaseUrl = new Uri(serverUrl)
        };

        if (timeout.HasValue)
        {
            rcOpts.Timeout = timeout;
        }

        if (configureSerialization != null)
            RestClient = new RestClient(httpClient, rcOpts, configureSerialization: configureSerialization);
        else
            RestClient = new RestClient(httpClient, rcOpts, configureSerialization: (c) => c.UseSerializer<RestSharpJsonNetClientSerializer>());
    }

    /// <summary>
    /// Creates HTTP GET request for JSON API.
    /// </summary>
    protected static RestRequest HttpGet(string resource) => MakeRequest(resource);

    /// <summary>
    /// Creates HTTP POST request for JSON API.
    /// </summary>
    protected static RestRequest HttpPost(string resource) => MakeRequest(resource, Method.Post);

    /// <summary>
    /// Creates HTTP PUT request for JSON API.
    /// </summary>
    protected static RestRequest HttpPut(string resource) => MakeRequest(resource, Method.Put);

    /// <summary>
    /// Creates HTTP DELETE request for JSON API.
    /// </summary>
    protected static RestRequest HttpDelete(string resource) => MakeRequest(resource, Method.Delete);

    /// <summary>
    /// Creates HTTP PATCH request for JSON API.
    /// </summary>
    protected static RestRequest HttpPatch(string resource) => MakeRequest(resource, Method.Patch);

    /// <summary>
    /// Creates HTTP request for JSON API.
    /// </summary>
    protected static RestRequest MakeRequest(string resource, Method method = Method.Get)
    {
        return new RestRequest(resource, method) { RequestFormat = DataFormat.Json };
    }

    private string PreExecute(string? rChain, RestRequest request)
    {
        // add in request trace header for next service level down
        request.AddHeader(RequestTraceId.RequestTraceIdHeader, RequestTraceId.Instance.Get() ?? string.Empty);
        request.AddHeader(RequestTraceChain.RequestTraceChainHeader, rChain ?? string.Empty);

        var rProps = RequestTraceProperties.GetRequestTraceProperties();
        if (rProps?.Count > 0)
            request.AddHeader(RequestTraceProperties.RequestTracePropertiesHeader, JsonUtil.SerializeObject(rProps));

        BeforeExec?.Invoke(request);
        var requestUri = RestClient.BuildUri(request).AbsoluteUri;
        LogRequest(rChain, request, requestUri);

        return requestUri;
    }

    private RestResponse PostExecute(string? rChain, RestRequest request, RestResponse response, HttpStatusCode[]? expectedStatusCodes, string requestUri, Stopwatch sw)
    {
        AfterExec?.Invoke(request, response);
        LogResponse(rChain, response, requestUri, sw);
        CheckForErrors(request, response, expectedStatusCodes);
        AfterSuccess?.Invoke(request, response);
        return response;
    }

    /// <summary>
    /// Execute a Request
    /// </summary>
    /// <param name="request"></param>
    /// <param name="expectedStatusCodes"></param>
    protected RestResponse Execute(RestRequest request, params HttpStatusCode[] expectedStatusCodes)
    {
        return ExecuteImpl(request, null, expectedStatusCodes);
    }

    /// <summary>
    /// Execute a Request with an interceptor
    /// </summary>
    /// <param name="request"></param>
    /// <param name="interceptor">function which will intercept the execution of the request</param>
    protected RestResponse ExecuteIntercept(RestRequest request, SyncInterceptor interceptor)
    {
        return ExecuteImpl(request, interceptor, null);
    }

    /// <summary>
    /// Execute a Request Async
    /// </summary>
    /// <param name="request"></param>
    /// <param name="expectedStatusCodes"></param>
    protected Task<RestResponse> ExecuteAsync(RestRequest request, params HttpStatusCode[] expectedStatusCodes)
        => ExecuteAsync(request, CancellationToken.None, expectedStatusCodes);

    /// <summary>
    /// Execute a Request Async
    /// </summary>
    protected Task<RestResponse> ExecuteAsync(RestRequest request, CancellationToken cancellationToken, params HttpStatusCode[] expectedStatusCodes)
        => ExecuteImplAsync(request, null, cancellationToken, expectedStatusCodes);

    /// <summary>
    /// Execute a Request async with an interceptor
    /// </summary>
    /// <param name="request"></param>
    /// <param name="interceptor">function which will intercept the execution of the request</param>
    /// <param name="cancellationToken"></param>
    protected Task<RestResponse> ExecuteInterceptAsync(RestRequest request, AsyncInterceptor? interceptor, CancellationToken cancellationToken = default)
        => ExecuteImplAsync(request, interceptor, cancellationToken, null);

    private async Task<RestResponse> ExecuteImplAsync(RestRequest request, AsyncInterceptor? interceptor, CancellationToken cancellationToken, params HttpStatusCode[]? expectedStatusCodes)
    {
        var rChain = RequestTraceChain.Instance.GenerateNextChain();

        var requestUri = PreExecute(rChain, request);

        var sw = Stopwatch.StartNew();
        var response = interceptor == null
            ? await RestClient.ExecuteAsync(request, cancellationToken)
            : await interceptor.Invoke(request, r => RestClient.ExecuteAsync(r, cancellationToken));
        sw.Stop();

        return PostExecute(rChain, request, response, expectedStatusCodes, requestUri, sw);
    }

    private RestResponse ExecuteImpl(RestRequest request, SyncInterceptor? interceptor, HttpStatusCode[]? expectedStatusCodes)
    {
        var rChain = RequestTraceChain.Instance.GenerateNextChain();

        var requestUri = PreExecute(rChain, request);

        var sw = Stopwatch.StartNew();
        var response = interceptor == null
            ? AsyncUtil.RunSync(async () => await RestClient.ExecuteAsync(request))!
            : interceptor.Invoke(request, r => AsyncUtil.RunSync(async () => await RestClient.ExecuteAsync(request))!);
        sw.Stop();

        return PostExecute(rChain, request, response, expectedStatusCodes, requestUri, sw);
    }

    /// <summary>
    /// Execute a Request with an expected object return type
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="request"></param>
    /// <param name="expectedStatusCodes"></param>
    /// <returns>an object of type T</returns>
    protected T? Execute<T>(RestRequest request, params HttpStatusCode[] expectedStatusCodes)
        => JsonUtil.DeserializeObject<T>(Execute(request, expectedStatusCodes).Content ?? string.Empty);

    /// <summary>
    /// Execute a Request with an expected object return type and an interceptor
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="request"></param>
    /// <param name="interceptor">function which will intercept the execution of the request</param>
    /// <returns>an object of type T</returns>
    protected T? ExecuteIntercept<T>(RestRequest request, SyncInterceptor interceptor)
        => JsonUtil.DeserializeObject<T>(ExecuteIntercept(request, interceptor).Content ?? string.Empty);

    /// <summary>
    /// Execute a Request Async with an expected object return type
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="request"></param>
    /// <param name="expectedStatusCodes"></param>
    /// <returns></returns>
    protected Task<T?> ExecuteAsync<T>(RestRequest request, params HttpStatusCode[] expectedStatusCodes)
        => ExecuteAsync<T>(request, CancellationToken.None, expectedStatusCodes);

    /// <summary>
    /// Execute a Request Async with an expected object return type
    /// </summary>
    protected async Task<T?> ExecuteAsync<T>(RestRequest request, CancellationToken cancellationToken, params HttpStatusCode[] expectedStatusCodes)
    {
        var response = await ExecuteImplAsync(request, null, cancellationToken, expectedStatusCodes);
        return JsonUtil.DeserializeObject<T>(response.Content ?? string.Empty);
    }

    /// <summary>
    /// Execute a Request Async with an expected object return type
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="request"></param>
    /// <param name="interceptor">function which will intercept the execution of the request</param>
    /// <param name="cancellationToken"></param>
    /// <returns>an object of type T</returns>
    protected async Task<T?> ExecuteInterceptAsync<T>(RestRequest request, AsyncInterceptor? interceptor, CancellationToken cancellationToken = default)
    {
        var response = await ExecuteInterceptAsync(request, interceptor, cancellationToken);
        return JsonUtil.DeserializeObject<T>(response.Content ?? string.Empty);
    }

    private void LogRequest(string? rChain, RestRequest request, string requestUri)
    {
        var rId = RequestTraceId.Instance.Get();
        var parameters = string.Join(",",
            request.Parameters
            .Where(p => p.Type == ParameterType.UrlSegment || p.Type == ParameterType.QueryString)
            .Select(p => $"{p.Name}={p.Value}"));

        var body = request.Parameters.FirstOrDefault(p => p.Type == ParameterType.RequestBody);
        var bodyData = (body?.Value as string);
        var bodyContentType = body?.Name; // name of the param for request body is the content type

        var logMetadata = CreateScope(
            Kvp("CallingChain", rChain),
            Kvp("Parameters", parameters),
            Kvp("ContentType", bodyContentType),
            Kvp("ContentLength", bodyData?.Length),
            Kvp(LoggingConstants.Field_MessageType, "RequestSent")
        );

        using (log.BeginScope(logMetadata))
        {
            if (bodyData != null && ShouldLogContent(bodyContentType, true))
                LogDefines.LogRequestContent(log, LogLevel, $"{rId},{rChain}", ServiceId, request.Method.ToString(), requestUri, bodyData.Truncate(MaxLoggingRequestBody, "<truncated>"));
            else
                LogDefines.LogRequestNoContent(log, LogLevel, $"{rId},{rChain}", ServiceId, request.Method.ToString(), requestUri, bodyData?.Length ?? 0);
        }
    }

    private void LogResponse(string? rChain, RestResponse response, string reqUrl, Stopwatch sw)
    {
        var shouldLog = ShouldLogContent(response.ContentType, false);

        var rId = RequestTraceId.Instance.Get();
        var statusCode = response.StatusCode;
        var statusCodeStr = statusCode.ToString();

        var contentLen = response.ContentLength ?? response.Content?.Length ?? 0;
        if (contentLen < 0)
            contentLen = 0;

        var logMetadata = CreateScope(
            Kvp("CallingChain", rChain),
            Kvp(LoggingConstants.Field_TimeDurationMicros, (long)(sw.Elapsed.TotalMilliseconds * 1000)),
            Kvp("RequestUri", reqUrl),
            Kvp(LoggingConstants.Field_MessageType, "ResponseReceived")
        );

        using (log.BeginScope(logMetadata))
        {
            if (shouldLog)
                LogDefines.LogResponseContent(log, LogLevel, $"{rId},{rChain}", ServiceId, sw.Elapsed, (int)statusCode, statusCodeStr, response.ContentType, contentLen, response.Content?.Truncate(MaxLoggingResponseBody, "<truncated>"));
            else
                LogDefines.LogResponseNoContent(log, LogLevel, $"{rId},{rChain}", ServiceId, sw.Elapsed, (int)statusCode, statusCodeStr, response.ContentType, contentLen);
        }
    }

    private static readonly List<string> _loggableContentTypes =
    [
        ContentTypes.ApplicationJavascript,
        ContentTypes.ApplicationEcmascript,
        ContentTypes.ApplicationJson
    ];

    /// <summary>
    /// Utility function which determines if content is suitable for logging.  
    /// By default logs JSON/JS/ECMA, text/*, and xml, override to change
    /// </summary>
    /// <param name="contentType">Content-Type header, may be null</param>
    /// <param name="isRequest">true for request, false for response</param>
    /// <returns></returns>
    public virtual bool ShouldLogContent(string? contentType, bool isRequest)
    {
        if (contentType == null)
            return false;

        // log any text/ or anything that is xml
        if (contentType.StartsWith("text/") || contentType.Contains("xml"))
            return true;

        // check if content type starts with any of the loggable types
        // starts with is used because some content types come back like "application/json; charset=utf-8"
        foreach (var loggable in _loggableContentTypes)
        {
            if (contentType.StartsWith(loggable))
                return true;
        }

        return false;
    }

    private protected void CheckForErrors(RestRequest request, RestResponse response, HttpStatusCode[]? expectedStatusCodes = null)
    {
        // Check expected status codes, if specified
        if (!expectedStatusCodes.IsNullOrEmpty()
            && response.IsSuccess(expectedStatusCodes!))
        {
            return;
        }

        // Check general status codes
        if (response.IsSuccessful)
        {
            // Check error message
            if (!string.IsNullOrEmpty(response.ErrorMessage))
                throw new WebException($"Successful code {(int)response.StatusCode}, but returned error: {response.ErrorMessage}");

            return;
        }

        AfterFailure?.Invoke(request, response);

        if (response.StatusCode == 0)
            throw new WebException(response.ErrorMessage, response.ErrorException);

        throw new RestException(
            response.StatusCode,
            TryExtractErrorMessage(response),
            response.Content);
    }

    private static string? TryExtractErrorMessage(RestResponse response)
    {
        ErrorResponse? error = null;

        try
        {
            error = JsonUtil.DeserializeObject<ErrorResponse>(response.Content!);
        }
        catch
        {
            // parse errors are ignored
        }

        return error?.ExceptionMessage ?? error?.Message ?? response?.ErrorMessage;
    }

    /// <summary>
    /// Invokes web request, and returns null for the 404 Not Found response instead of throwing an exception.
    /// </summary>
    protected static T? ReadOrNull<T>(Func<T?> action) where T : class
    {
        try
        {
            return action.Invoke();
        }
        catch (RestException e) when (e.StatusCode == HttpStatusCode.NotFound)
        {
            return null;
        }
    }

    /// <summary>
    /// Invokes web request, and returns null for the 404 Not Found response instead of throwing an exception.
    /// </summary>
    protected static async Task<T?> ReadOrNullAsync<T>(Func<Task<T?>> action) where T : class
    {
        try
        {
            return await action.Invoke();
        }
        catch (RestException e) when (e.StatusCode == HttpStatusCode.NotFound)
        {
            return null;
        }
    }

    /// <summary>
    /// Response object returned if error detected
    /// </summary>
    public class ErrorResponse
    {
        /// <summary>
        /// error message
        /// </summary>
        public string? Message { get; set; }

        /// <summary>
        /// exception message
        /// </summary>
        public string? ExceptionMessage { get; set; }
    }

    private static class LogDefines
    {
        public static void LogRequestContent(ILogger logger, LogLevel logLevel, string traceString, string serviceId, string? httpMethod, string? uri, string? body, Exception? e = null)
            => _logRequestContent[logLevel](logger, traceString, serviceId, httpMethod, uri, body, e);

        public static void LogRequestNoContent(ILogger logger, LogLevel logLevel, string traceString, string serviceId, string? httpMethod, string? uri, long contentLength, Exception? e = null)
            => _logRequestNoContent[logLevel](logger, traceString, serviceId, httpMethod, uri, contentLength, e);

        public static void LogResponseContent(ILogger logger, LogLevel logLevel, string traceString, string serviceId, TimeSpan elapsed, int statusCode, string? status, string? contentType, long contentLength, string? content, Exception? e = null)
            => _logResponseContent[logLevel](logger, traceString, serviceId, elapsed, statusCode, status, contentType, contentLength, content, e);

        public static void LogResponseNoContent(ILogger logger, LogLevel logLevel, string traceString, string serviceId, TimeSpan elapsed, int statusCode, string? status, string? contentType, long contentLength, Exception? e = null)
            => _logResponseNoContent[logLevel](logger, traceString, serviceId, elapsed, statusCode, status, contentType, contentLength, e);

        private static readonly Dictionary<LogLevel, Action<ILogger, string?, string?, string?, string?, string?, Exception?>> _logRequestContent = [];
        private static readonly Dictionary<LogLevel, Action<ILogger, string?, string?, string?, string?, long, Exception?>> _logRequestNoContent = [];
        private static readonly Dictionary<LogLevel, Action<ILogger, string?, string?, TimeSpan, int, string?, string?, long, string?, Exception?>> _logResponseContent = [];
        private static readonly Dictionary<LogLevel, Action<ILogger, string?, string?, TimeSpan, int, string?, string?, long, Exception?>> _logResponseNoContent = [];

        static LogDefines()
        {
            foreach (var ll in Enum.GetValues<LogLevel>())
            {
                _logRequestContent[ll] = CreateLogRequestWithContent(ll);
                _logRequestNoContent[ll] = CreateLogRequestNoContent(ll);

                _logResponseContent[ll] = CreateLogResponseWithContent(ll);
                _logResponseNoContent[ll] = CreateLogResponseNoContent(ll);
            }
        }

        // see LoggingMiddleware.cs in Plinth.AspNetCore

        private static Action<ILogger, string?, string?, string?, string?, string?, Exception?> CreateLogRequestWithContent(LogLevel logLevel)
            => LoggerMessage.Define<string?, string?, string?, string?, string?>(logLevel, default,
                "[{TraceString}] ==> Server Request [{ServiceId}]: Method: {HttpMethod}, Uri: {RequestUri}, Body: {RequestBody}");

        private static Action<ILogger, string?, string?, string?, string?, long, Exception?> CreateLogRequestNoContent(LogLevel logLevel)
            => LoggerMessage.Define<string?, string?, string?, string?, long>(logLevel, default,
                "[{TraceString}] ==> Server Request [{ServiceId}]: Method: {HttpMethod}, Uri: {RequestUri}, Body Size = {ContentLength}");

        private static Action<ILogger, string?, string?, TimeSpan, int, string?, string?, long, string?, Exception?> CreateLogResponseWithContent(LogLevel logLevel)
        {
            const string fmtStr = "[{TraceString}] <== Server Response [{ServiceId}]: (Took: {" + LoggingConstants.Field_TimeDuration + "}) Status Code: {HttpStatusCode}/{HttpStatus}, Type: {ContentType}, Content-Length: {ContentLength}, Content {Content}";
#if NET10_0_OR_GREATER
            // someday they will fix this: https://github.com/dotnet/runtime/issues/55525
            return LoggerMessage.Define<string?, string?, TimeSpan, int, string?, string?, long, string?, string?>(logLevel, default, fmtStr);
#else
            return (log, t1, t2, t3, t4, t5, t6, t7, t8, e) => log.Log(logLevel, e, fmtStr, t1, t2, t3, t4, t5, t6, t7, t8);
#endif
        }

        private static Action<ILogger, string?, string?, TimeSpan, int, string?, string?, long, Exception?> CreateLogResponseNoContent(LogLevel logLevel)
        {
            const string fmtStr = "[{TraceString}] <== Server Response [{ServiceId}]: (Took: {" + LoggingConstants.Field_TimeDuration + "}) Status Code: {HttpStatusCode}/{HttpStatus}, Type: {ContentType}, Content-Length: {ContentLength}";
#if NET10_0_OR_GREATER
            return LoggerMessage.Define<string?, string?, TimeSpan, int, string?, string?, long>(logLevel, default, fmtStr);
#else
            return (log, t1, t2, t3, t4, t5, t6, t7, e) => log.Log(logLevel, e, fmtStr, t1, t2, t3, t4, t5, t6, t7);
#endif
        }
    }
}
