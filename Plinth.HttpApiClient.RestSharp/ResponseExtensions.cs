﻿using System.Net;
using RestSharp;

namespace Plinth.HttpApiClient.RestSharp;

/// <summary>
/// Extensions methods for API responses.
/// </summary>
public static class ResponseExtensions
{
    /// <summary>
    /// Determine if this response was successful
    /// </summary>
    /// <param name="response"></param>
    /// <param name="successCodes">list of response codes which define success</param>
    /// <returns></returns>
    public static bool IsSuccess(this RestResponse response, params HttpStatusCode[] successCodes) => successCodes.Contains(response.StatusCode);
    
    /// <summary>
    /// Determine if this response was successful
    /// </summary>
    /// <param name="response"></param>
    /// <param name="successCodes">list of response codes which define success</param>
    /// <returns></returns>
    public static bool IsSuccess<T>(this RestResponse<T> response, params HttpStatusCode[] successCodes) => successCodes.Contains(response.StatusCode);
}
