using Hangfire;
using Hangfire.Dashboard;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Plinth.AspNetCore.DI;
using Plinth.Hangfire.Filters;
using Plinth.Hangfire.Impl;
using Plinth.Hangfire.Impl.Sync;

namespace Plinth.Hangfire;

/// <summary>
/// Extensions for a Plinth standard Hangfire instance
/// </summary>
public static class PlinthServicesExtensions
{
    /// <summary>
    /// Used by database specific projects to install hangfire
    /// </summary>
    internal static IServiceCollection AddPlinthHangfireInternal(this IServiceCollection services, 
        IJobSyncFactory jobSyncFactory,
        JobHandlerRegistrar handlerRegistrar,
        JobStorage jobStorage,
        bool startAsync = true,
        string? dbSyncCron = null,
        Action<BackgroundJobServerOptions>? serverOptsAction = null,
        Action<IGlobalConfiguration>? configAction = null)
    {
        services.AddSingleton(jobSyncFactory);
        services.AddSingleton(handlerRegistrar);
        services.AddSingleton(sp => jobSyncFactory.Create(sp, handlerRegistrar));
        services.AddSingleton<IJobSyncManager>(sp => sp.GetRequiredService<JobSyncLogic>());
        services.AddTransient<JobExecutor>();

        // typical hangfire usage uses the static 'RecurringJob' and 'BackgroundJob', but those use a global config
        // in this lib, we use these instead which are tied to DI
        services.AddSingleton<IRecurringJobManager>(new RecurringJobManager(jobStorage));
        services.AddSingleton<IBackgroundJobClient>(new BackgroundJobClient(jobStorage));

        services.AddHangfire(config =>
        {
            config.UseRecommendedSerializerSettings();
            //config.SetDataCompatibilityLevel(CompatibilityLevel.Version_170);
            config.UseStorage(jobStorage);
            config.UseFilter(new LoggingJobFilterAttribute());
            config.UseFilter(new AutomaticRetryAttribute { Attempts = 0, OnAttemptsExceeded = AttemptsExceededAction.Delete });

            configAction?.Invoke(config);
        });

        services.AddHangfireServer((sp, opts) =>
        {
            serverOptsAction?.Invoke(opts);

            opts.Activator = new ServicesJobActivator(sp);
            if (serverOptsAction == null)
                opts.StopTimeout = TimeSpan.FromSeconds(10);

        }, jobStorage);

        dbSyncCron ??= "*/15 * * * *";

        if (startAsync)
        {
            services.AddHostedService(sp =>
                PlinthActivatorUtil.CreateInstance<PlinthHangfireBootstrapService>(sp, dbSyncCron));
        }

        return services;
    }

    /// <summary>
    /// In ConfigureApp(), enable Plinth hangfire
    /// </summary>
    /// <remarks>an ISqlTransactionProvider must be registered</remarks>
    /// <param name="app"></param>
    /// <param name="dashboardPath">path where the hangfire dashboard goes, null to disable dashboard, "" to host at root</param>
    /// <param name="dashboardOpts">optional custom config for the dashboard</param>
    /// <returns></returns>
    public static IApplicationBuilder UsePlinthHangfire(
        this IApplicationBuilder app,
        string? dashboardPath = "/hangfire",
        DashboardOptions? dashboardOpts = null)
    {
        dashboardOpts ??= new DashboardOptions
        {
            AppPath = null,
            Authorization = [new AnonymousAuthorizationFilter()]
        };

        if (dashboardPath != null)
            app.UseHangfireDashboard(dashboardPath.TrimEnd('/'), dashboardOpts);
        else
        {
            // hangfire requires us to host the dashboard, so we put it at a random location and disable all access to it
            app.UseHangfireDashboard("/" + Guid.NewGuid().ToString(), options: new DashboardOptions { Authorization = [new DisableAuthorizationFilter()] });
        }

        return app;
    }

    /// <summary>
    /// used for testing
    /// </summary>
    internal static void StartPlinthSync(this IApplicationBuilder app, string dbSyncCron, CancellationToken cancellationToken)
    {
        var svc = PlinthActivatorUtil.CreateInstance<PlinthHangfireBootstrapService>(
            app.ApplicationServices, dbSyncCron);
        svc.StartAsync(cancellationToken).GetAwaiter().GetResult();
    }
}

/// <summary>
/// This hosted service sets up the sync job out of band with startup
/// This allows startup to complete even if the DB is down
/// </summary>
file class PlinthHangfireBootstrapService(
    JobSyncLogic jobSyncLogic, IRecurringJobManager recurringJobMgr, IBackgroundJobClient backgroundJob,
    string dbSyncCron) : BackgroundService
{
    private static readonly ILogger logger = StaticLogManager.GetLogger();

    protected override async Task ExecuteAsync(CancellationToken cancellationToken)
    {
        try
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                logger.Info("Setting up plinth hangfire sync job");

                try
                {
                    recurringJobMgr.AddOrUpdate(
                        JobSyncConstants.JobListSyncJobName,
                        () => jobSyncLogic.Sync(),
                        dbSyncCron);

                    backgroundJob.Enqueue(
                        () => jobSyncLogic.Sync());

                    break;
                }
                catch (Exception e)
                {
                    logger.Warn(e, "failed to setup hangfire, will try again");
                    await Task.Delay(TimeSpan.FromSeconds(15), cancellationToken);
                }
            }

            logger.Info("Plinth hangfire setup complete");
        }
        catch (TaskCanceledException)
        {
            logger.Info("Plinth hangfire setup cancelled");
        }
    }
}

file class DisableAuthorizationFilter : IDashboardAuthorizationFilter
{
    /// <summary>
    /// Checks whether login should be allowed.
    /// </summary>
    public bool Authorize(DashboardContext context)
    {
        return false;
    }
}
