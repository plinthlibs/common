﻿using Hangfire.Dashboard;

namespace Plinth.Hangfire.Filters;

/// <summary>
/// Allows anyone to login into Hangfire dahsboard.
/// </summary>
public class AnonymousAuthorizationFilter : IDashboardAuthorizationFilter
{
    /// <summary>
    /// Checks whether login should be allowed.
    /// </summary>
    public bool Authorize(DashboardContext context)
    {
        return true;
    }
}
