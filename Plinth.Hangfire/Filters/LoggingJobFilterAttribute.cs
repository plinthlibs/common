using Microsoft.Extensions.Logging;
using Hangfire;
using Hangfire.Common;
using Hangfire.Server;
using Plinth.Common.Logging;
using Plinth.Serialization;
using System.Diagnostics;
using Plinth.Hangfire.Impl;
using Plinth.Hangfire.Impl.Sync;
using System.Reflection;

using static Plinth.Common.Logging.Scopes;

namespace Plinth.Hangfire.Filters;

/// <summary>
/// Filter for logging job start/end
/// </summary>
internal partial class LoggingJobFilterAttribute : JobFilterAttribute, IServerFilter
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private const string _StopwatchKey = "OperationDurationStopwatch";
    private const string _JobNameKey = "PJobName";
    private const string _JobModelKey = "PJobModel";
    private const string _AITelemetryKey = "PAITelemetry";

    // start the request ids off at a random int32, so it doesn't reset to 0 on app reset
    private static long _requestId = new Random().Next();

    // if you have included the plinth nlog appinsights target, we load it and start an AI telemetry session
    private static readonly MethodInfo? _aiTelemetry = Type
        .GetType("Plinth.Logging.NLog.AppInsights.AppInsightsTarget, Plinth.Logging.NLog.AppInsights")?
        .GetMethod("StartTelemetrySession", BindingFlags.Static|BindingFlags.NonPublic);

    /// <summary>
    /// Called when a job is about to run
    /// </summary>
    public void OnPerforming(PerformingContext filterContext)
    {
        var rId = RequestTraceId.Instance.Create();
        var rChain = RequestTraceChain.Instance.Create();

        LogDefines.LogTraceInfo(log, rId, rChain);

        // do a thread safe increment on the request id and use that for async context
        // only the bottom 20 bits are used, so it will reset every ~1M jobs
        StaticLogManager.SetContextId(Interlocked.Increment(ref _requestId));

        var jobInfo = ExtractJobInfo(filterContext.BackgroundJob);

        filterContext.Items[_StopwatchKey] = Stopwatch.StartNew();
        filterContext.Items[_JobNameKey] = jobInfo.name;
        filterContext.Items[_JobModelKey] = jobInfo.model;

        var meta = CreateScope(
            Kvp(LoggingConstants.Field_MessageType, "JobExecutionStarted")
        );

        // _aiTelemetry will be null if Plinth.Logging.NLog.AppInsights is not referenced
        // if it is but the AppInsightsTarget is not configured in NLog.config, the invocation will return null
        filterContext.Items[_AITelemetryKey] = _aiTelemetry?.Invoke(null, [jobInfo.name]) as IDisposable;

        using (log.BeginScope(meta))
        {
            LogDefines.LogJobStart(log, $"{rId},{rChain}", jobInfo.model?.Code, jobInfo.name, jobInfo.model?.CronExpression);
        }
    }

    /// <summary>
    /// Called when a job has completed
    /// </summary>
    public void OnPerformed(PerformedContext filterContext)
    {
        var rId = RequestTraceId.Instance.Get();
        var rChain = RequestTraceChain.Instance.Get();
        var jobInfo = (name: filterContext.Items[_JobNameKey] as string, model: filterContext.Items[_JobModelKey] as PlinthJob);

        var sw = (Stopwatch)filterContext.Items[_StopwatchKey];
        sw.Stop();

        string stringResult = "";
        if (filterContext.Result != null)
        {
            try
            {
                stringResult = JsonUtil.SerializeObject(filterContext.Result);
            }
            catch (Exception ex)
            {
                log.Trace(ex, "Could not serialize result value");
                stringResult = "Could not serialize";
            }
        }

        var elapsed = sw.Elapsed;
        var meta = CreateScope(
            Kvp(LoggingConstants.Field_TimeDurationMicros, (long)(elapsed.TotalMilliseconds * 1000)),
            Kvp(LoggingConstants.Field_MessageType, "JobExecuted")
        );

        using (log.BeginScope(meta))
        {
            LogDefines.LogJobEnd(log, $"{rId},{rChain}", jobInfo.model?.Code, jobInfo.name, elapsed, filterContext.Exception == null, stringResult);
        }

        // if we started an app insights telemetry session, dispose of it
        (filterContext.Items[_AITelemetryKey] as IDisposable)?.Dispose();
    }

    private static partial class LogDefines
    {
        [LoggerMessage(0, LogLevel.Debug,
                "<== [{TraceString}] Starting to execute job {JobId}: {JobName}, schedule = '{CronExpression}'", EventName = "JobStart")]
        public static partial void LogJobStart(ILogger logger, string traceString, string? jobId, string? jobName, string? cronExpression);

        [LoggerMessage(1, LogLevel.Debug,
                "==> [{TraceString}] Executed job {JobId}: {JobName}, Took: {" + LoggingConstants.Field_TimeDuration + "}, Succeeded: {Succeeded} Result: {Result}", EventName = "JobEnd")]
        public static partial void LogJobEnd(ILogger logger, string traceString, string? jobId, string? jobName, TimeSpan timeDuration, bool succeeded, string? result);

        [LoggerMessage(2, LogLevel.Debug,
               "New trace id: {TraceId}, New trace chain: {TraceChain}", EventName = "TraceInfo")]
        public static partial void LogTraceInfo(ILogger logger, string traceId, string traceChain);
    }

    private static (string? name, PlinthJob? model) ExtractJobInfo(BackgroundJob job)
    {
        var jobName = job.Job.Type.Name;
        PlinthJob? baseJob = null;

        if (job.Job.Type == typeof(JobExecutor) && job.Job.Args.Count > 0)
        {
            jobName = job.Job.Args[0].ToString();

            if (job.Job.Args.Count > 1 && job.Job.Args[1] is PlinthJob j)
                baseJob = j;
        }
        else if (job.Job.Type == typeof(JobSyncLogic))
        {
            jobName = JobSyncConstants.JobListSyncJobName;
        }

        return (jobName, baseJob);
    }
}
