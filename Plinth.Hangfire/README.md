

# README

### Plinth.Hangfire

**Utility framework for using Hangfire as a scheduled jobs engine**

Simplifies usage of Hangfire for operating a persistent scheduled jobs engine which can run predefined tasks on a configurable schedule regardless of number of machines or when they reboot.

### 1.  Project setup

   - Option 1: Windows Service
       - See _Plinth.WindowsService_ to set up an ASP&#46;Net Core project as a Windows Service

   - Option 2: ASP&#46;NET Core project (IIS or kestrel)

### 2.  Startup.cs

:point_right: **NOTE**: by default, it will use the default ISqlTransactionProvider to retrieve the job list from the database.
:point_right: The Hangfire schema/tables will be created using the db connection string passed in `AddPlinthHangfire`

_Startup.cs_
```c#
	public void ConfigureServices(IServiceCollection services)
	{
		// hangfire
		services.AddPlinthHangfire(
			Configuration.GetConnectionString("HangfireDB"),
			reg =>
			{
				// register jobs
				reg.RegisterAsyncHandler<BasicJob>(BasicJob.JobCode);
			});
		// hangfire

		// be sure to register job execution class with services
		services.AddTransient<BasicJob>();
	}

	public void Configure(IApplicationBuilder app, IHostingEnvironment env)
	{
	    // hangfire
	    app.UsePlinthHangfire<BasicJobExec>("/dashboard");  // "" to host dashboard at root
	    // hangfire
	}
```

_BasicJob.cs_

```c#
public class BasicJob : IAsyncJob
{
	public const string JobCode = "BasicJob";
	
	public async Task ExecuteAsync(PlinthJob job)
	{
		await DoJobStuff(job.Code, job.JobData);
	}
}
```

### 3. Database setup

:point_right: You must install either _Plinth.Hangfire.MSSql_ or _Plinth.Hangfire.PgSql_

:point_right: In each package, find the `Hangfire_Procedures.sql` and `Hangfire_Tables.sql` files.  Include those in your database scripts.  It is required by the framework that the procedures and tables be available to the main Plinth database connection.

The connection string that is passed to `AddPlinthHangfire` must have rights to create schemas and tables.  This can be a separate database from the main Plinth database.
See here for more details: https://docs.hangfire.io/en/latest/configuration/using-sql-server.html

### 4. Registering Jobs

To register jobs, you must supply the types and the job code that will invoke that job.

```c#
		reg =>
		{
			// register Job Code "BasicJob" which will invoke IAsyncJob BasicJob
			reg.RegisterAsyncHandler<BasicJob>("BasicJob");

			// register a handler for all Job Codes not registered with RegisterAsyncHandler
			reg.RegisterAsyncGlobalHandler<GlobalJob>();
		}
```

* Registering a Job by _Code_ will attach that particular code to that particular class and invoke it according to the schedule.
* Registering a global Job Handler will invoke that handler if the Job Code is not otherwise registered.

:point_right: In all cases, the Job Handler will be instantiated via DI, so it is important to register your Job Handlers with the DI container.

### 5. Configuring Jobs

`Hangfire_Procedures.sql` ships with a stored procedure called `usp_SubmitJob`.  You can use this to seed the job into the `Job` table.   

* Example for MS SQL Server.
```sql
EXECUTE usp_SubmitJob
	@Code = "BasicJob",
	@Description = "A basic job",
	@JobData = '{ "Meta1": 55 }',
	@CronExpression = '*/15 * * * 1-5',
	@TimeZone = NULL,
	@IsActive = 1,
	@CallingUser = 'System';
```
* Example for PostgreSQL
```sql
SELECT * FROM public.fn_submit_job(
	i_code := 'BasicJob',
	i_description := 'A basic Job',
	i_job_data = '{ "Meta1": 55 }',
	i_cron_expression := '*/15 * * * 1-5',
	i_time_zone := NULL,
	i_is_active := 1,
	i_calling_user := 'System');
```

* _@Code_: A unique identifier for the job.  Will appear in the Hangfire dashboard so something human readable is recommended.
* _@Description_: More descriptive text for the job.  Will also appear in the Hangfire dashboard.
* _@JobData_: Option metadata which is provided to the Job Handler.  Typically JSON but can be any `NVARCHAR` data
* _@CronExpression_: A cron expression which indicates when the job will run.
	* This site is a handy way to craft cron expressions: https://crontab.guru
	* Note that Hangfire supports second level precision for jobs.  To use, place an extra value before the expression.
	* Examples:
		* `* * * * *` => Start of every minute
		* `*/30 * * * * *` => Every 30 seconds at the start of the minute and halfway through the minute
		* `30 22 * * *` => Every night at 11:30 PM
		* `*/30 * */4 * 2,3,4 2-5` => Every 30 seconds at every minute past every 4th hour on every day-of-week from Tuesday through Friday in February, March, and April
* _@TimeZone_: Time zone identifier, see next section
* _@IsActive_: Flag indicating the job should run
* _@CallingUser_: Audit data for who last modified the job record

:point_right: Only the Job Code parameter is required.  This same procedure can be used to update subsets of Job configuration

### 6. Time Zones
When submitting a job, a `NULL` Time Zone will indicate that the Cron expression should be evaluated in UTC.

To use a different Time Zone, supply a zone name in the `@TimeZone` parameter.  This will allow scheduling jobs to execute at a specific time in a specific time zone.  For example, 11:30 PM in `America/Los_Angeles`

:point_right: Either Windows "Pacific Standard Time" or Olson "America/Los_Angeles" will work on either OS, but it is **strongly** recommended to use Olson timezone names.  https://en.wikipedia.org/wiki/List_of_tz_database_time_zones




