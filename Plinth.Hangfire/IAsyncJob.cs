﻿namespace Plinth.Hangfire;

/// <summary>
/// Interface for Async Hangfire jobs
/// </summary>
public interface IAsyncJob
{
    /// <summary>
    /// Will be called on the defined schedule
    /// </summary>
    /// <param name="job"></param>
    Task ExecuteAsync(PlinthJob job);
}
