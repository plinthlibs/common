﻿using Hangfire;

namespace Plinth.Hangfire;

/// <summary>
/// Utility to create cron expressions with seconds.  For minute level, use <see cref="global::Hangfire.Cron"/>
/// </summary>
public static class CronHelper
{
    /// <summary>
    /// Run at N second intervals, example: call with 10 => run at 0,10,20,30...
    /// </summary>
    /// <param name="seconds">how often to run</param>
    /// <remarks>within range 1..59</remarks>
    public static string EveryNSeconds(int seconds)
    {
        if (seconds < 1 || seconds >= 60) throw new ArgumentOutOfRangeException(nameof(seconds), "must be 1..59");

        return $"*/{seconds} {Cron.Minutely()}";
    }

    /// <summary>
    /// Run at 30 second intervals (0,30)
    /// </summary>
    public static string Every30Seconds()
        => EveryNSeconds(30);

    /// <summary>
    /// Run at 15 second intervals (0,15,30,45)
    /// </summary>
    public static string Every15Seconds()
        => EveryNSeconds(15);

    /// <summary>
    /// Run every minute, at a particular second (every minute at 23 seconds past the minute)
    /// </summary>
    /// <param name="seconds">second of the minute to run at</param>
    /// <remarks>within range 0..59</remarks>
    public static string MinutelyAtSeconds(int seconds)
    {
        if (seconds < 0 || seconds >= 60) throw new ArgumentOutOfRangeException(nameof(seconds), "must be 0..59");

        return $"{seconds} {Cron.Minutely()}";
    }

    /// <summary>
    /// Run every minute, at particular seconds (every minute at 23 and 46 seconds past the minute)
    /// </summary>
    /// <param name="seconds">list of second of the minute to run at</param>
    /// <remarks>must contain at least 1, all within range 0..59</remarks>
    public static string MinutelyAtSeconds(IEnumerable<int> seconds)
    {
        if (!seconds.Any() || !seconds.All(s => s >= 0 && s < 60)) throw new ArgumentOutOfRangeException(nameof(seconds), "must be 0..59");

        return $"{string.Join(",", seconds.OrderBy(x => x))} {Cron.Minutely()}";
    }
}
