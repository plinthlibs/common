using Hangfire.Client;
using Hangfire.Common;
using Hangfire.Server;
using Hangfire.Storage;
using Plinth.Serialization;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;

namespace Plinth.Hangfire.Impl;

/// <summary>
/// Disable multiple jobs from being executed with the same parameters. (Default behavior)
/// </summary>
internal class DisableConcurrentJobExecutionAttribute : JobFilterAttribute, IClientFilter, IServerFilter
{
    private static readonly TimeSpan _lockTimeout = TimeSpan.FromSeconds(5);
    private static readonly TimeSpan _fingerprintTimeout = TimeSpan.FromHours(1);

    /// <summary>
    /// Called when the job is being created
    /// </summary>
    /// <param name="filterContext"></param>
    public void OnCreating(CreatingContext filterContext)
    {
        if (filterContext.Job.Method.GetCustomAttributes(typeof(EnableConcurrentJobExecutionAttribute), true).Length != 0)
            return;

        if (!AddFingerprintIfNotExists(filterContext.Connection, filterContext.Job))
        {
            filterContext.Canceled = true;
        }
    }

    /// <summary>
    /// Called when the job is done
    /// </summary>
    /// <param name="filterContext"></param>
    public void OnPerformed(PerformedContext filterContext)
    {
        if (filterContext.BackgroundJob.Job.Method.GetCustomAttributes(typeof(EnableConcurrentJobExecutionAttribute), true).Length != 0)
            return;

        RemoveFingerprint(filterContext.Connection, filterContext.BackgroundJob.Job);
    }

    private static bool AddFingerprintIfNotExists(IStorageConnection connection, Job job)
    {
        using (connection.AcquireDistributedLock(GetFingerprintLockKey(job), _lockTimeout))
        {
            var fingerprint = connection.GetAllEntriesFromHash(GetFingerprintKey(job));

            if (fingerprint != null
                && fingerprint.TryGetValue("Timestamp", out var value)
                && DateTimeOffset.TryParse(value, null, DateTimeStyles.RoundtripKind, out DateTimeOffset timestamp)
                && DateTimeOffset.UtcNow <= timestamp.Add(_fingerprintTimeout))
            {
                // Actual fingerprint found, returning.
                return false;
            }

            // Fingerprint does not exist, it is invalid (no `Timestamp` key),
            // or it is not actual (timeout expired).
            connection.SetRangeInHash(
                GetFingerprintKey(job),
                new Dictionary<string, string>
                {
                    { "Timestamp", DateTimeOffset.UtcNow.ToString("o") }
                });

            return true;
        }
    }

    private static void RemoveFingerprint(IStorageConnection connection, Job job)
    {
        using (connection.AcquireDistributedLock(GetFingerprintLockKey(job), _lockTimeout))
        using (var transaction = connection.CreateWriteTransaction())
        {
            transaction.RemoveHash(GetFingerprintKey(job));
            transaction.Commit();
        }
    }

    private static string GetFingerprintLockKey(Job job)
    {
        return $"{GetFingerprintKey(job)}:lock";
    }

    private static string GetFingerprintKey(Job job)
    {
        return $"fingerprint:{GetFingerprint(job)}";
    }

    private static string GetFingerprint(Job job)
    {
        if (job.Type == null
            || job.Method == null)
            return "";

        var parameterStr = "";
        if (job.Args != null)
            parameterStr = JsonUtil.SerializeObject(job.Args);

        var fingerprintString = $"{job.Type.FullName}.{job.Method.Name}.{parameterStr}";

        var fingerprintHash = SHA256.HashData(Encoding.UTF8.GetBytes(fingerprintString));
        return Convert.ToBase64String(fingerprintHash);
    }

    void IClientFilter.OnCreated(CreatedContext filterContext)
    {
        // nothing to do
    }

    void IServerFilter.OnPerforming(PerformingContext filterContext)
    {
        // nothing to do
    }
}
