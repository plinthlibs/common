using Microsoft.Extensions.Logging;

namespace Plinth.Hangfire.Impl;

/// <summary>
/// Logic for time zone handling for jobs
/// </summary>
internal static class TimeZoneLogic
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    /// <summary>
    /// Convert a TimeZone ID to a TimeZoneInfo
    /// </summary>
    /// <param name="timeZoneId">windows or olson db time zone id (e.g. "Pacific Standard Time" or "America/Los_Angeles"). null for UTC</param>
    /// <returns>TimeZoneInfo, UTC by default</returns>
    public static TimeZoneInfo GetInfoFromTimeZone(string? timeZoneId)
    {
        var tz = TimeZoneInfo.Utc;

        try
        {
            if (timeZoneId != null)
            {
                tz = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
            }
        }
        catch (TimeZoneNotFoundException e)
        {
            log.Error(e, $"time zone not found: {timeZoneId}");
        }
        catch (InvalidTimeZoneException e)
        {
            log.Error(e, $"time zone invalid: {timeZoneId}");
        }

        log.Debug($"using time zone: {timeZoneId}");
        return tz;
    }
}
