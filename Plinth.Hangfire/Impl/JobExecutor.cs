using System.ComponentModel;
using Plinth.AspNetCore.DI;

namespace Plinth.Hangfire.Impl;

internal class JobExecutor(IServiceProvider sp, JobHandlerRegistrar handlers)
{
    [DisplayName("{0}")]
    [DisableConcurrentJobExecution]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "first param is rendered as display name")]
    public void ExecuteSyncNoConcurrent(string jobName, PlinthJob job)
    {
        var handler = handlers.GetHandlerFor(job.Code);
        var action = (ISyncJob)ActivateJob(handler.SyncAction!);
        action.Execute(job);
    }

    [DisplayName("{0}")]
    [EnableConcurrentJobExecution]
    public void ExecuteSyncAllowConcurrent(string jobName, PlinthJob job)
        => ExecuteSyncNoConcurrent(jobName, job);

    [DisplayName("{0}")]
    [DisableConcurrentJobExecution]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "first param is rendered as display name")]
    public Task ExecuteAsyncNoConcurrent(string jobName, PlinthJob job)
    {
        var handler = handlers.GetHandlerFor(job.Code);
        var action = (IAsyncJob)ActivateJob(handler.AsyncAction!);
        return action.ExecuteAsync(job);
    }

    [DisplayName("{0}")]
    [EnableConcurrentJobExecution]
    public Task ExecuteAsyncAllowConcurrent(string jobName, PlinthJob job)
        => ExecuteAsyncNoConcurrent(jobName, job);

    private object ActivateJob(Type type)
    {
        // in the event that a no-arg constructor job class is not registered with the provider
        var j = PlinthActivatorUtil.CreateInstance(sp, type);
        if (j != null)
            return j;

        throw new InvalidOperationException($"could not resolve {type.FullName} from services");
    }
}
