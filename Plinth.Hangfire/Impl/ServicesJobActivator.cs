using Hangfire;
using Microsoft.Extensions.DependencyInjection;

namespace Plinth.Hangfire.Impl;

internal class ServicesJobActivator(IServiceProvider sp) : JobActivator
{
    public override object ActivateJob(Type type)
        => sp.GetRequiredService(type);
}
