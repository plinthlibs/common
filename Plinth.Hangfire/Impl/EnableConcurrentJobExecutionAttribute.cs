﻿namespace Plinth.Hangfire.Impl;

/// <summary>
/// Enable multiple instances of a job to run concurrently (based on parameters)
/// </summary>
[AttributeUsage(AttributeTargets.Method)]
internal class EnableConcurrentJobExecutionAttribute : Attribute
{
}
