using System.ComponentModel;
using Microsoft.Extensions.Logging;
using Hangfire;
using Hangfire.Storage;

namespace Plinth.Hangfire.Impl.Sync;

/// <summary>
/// Logic for syncing known jobs with Hangfire recurring jobs.
/// </summary>
internal class JobSyncLogic(IJobRepository repo, JobHandlerRegistrar handlers, IRecurringJobManager recurringJobManager) : IJobSyncManager
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private readonly object _lock = new();

    /// <summary>
    /// Synchronizes jobs from DB with jobs scheduled in Hangfire.
    /// </summary>
    [DisplayName("Sync jobs from DB")]
    public void Sync()
    {
        try
        {
            lock (_lock)
            {
                SyncImpl();
            }
        }
        catch (Exception e)
        {
            log.Error(e, "failed during job sync");
            throw;
        }
    }

    private void SyncImpl()
    {
        IDictionary<string, RecurringJobDto> currentJobs;

        using (var conn = JobStorage.Current.GetConnection())
        {
            currentJobs = conn.GetRecurringJobs().ToDictionary(r => r.Id, r => r);
        }

        var jobs = repo.GetJobList();

        currentJobs.Remove(JobSyncConstants.JobListSyncJobName);

        log.Debug($"Retrieved {jobs.Count} jobs");

        ScheduleJobs(jobs, currentJobs);

        foreach (var extraJobId in currentJobs)
        {
            log.Debug($"removing job {extraJobId.Key}");
            recurringJobManager.RemoveIfExists(extraJobId.Key);
        }
    }

    private void ScheduleJobs(IEnumerable<PlinthJob> jobs, IDictionary<string, RecurringJobDto> currentJobs)
    {
        foreach (var job in jobs)
        {
            if (!job.IsActive)
            {
                log.Info($"Job {job.Code}: is not active");
                recurringJobManager.RemoveIfExists(job.Code);
                continue;
            }

            currentJobs.TryGetValue(job.Code, out var recurringJob);
            Schedule(job, recurringJob);

            currentJobs.Remove(job.Code);
        }
    }

    private void Schedule(PlinthJob job, RecurringJobDto? recurringJob)
    {
        ArgumentNullException.ThrowIfNull(job);

        if (!JobChanged(job, recurringJob))
            return;

        var cronDescOpts = new CronExpressionDescriptor.Options
        {
            ThrowExceptionOnParseError = false,
            Use24HourTimeFormat = false
        };

        var cron = CronExpressionDescriptor.ExpressionDescriptor.GetDescription(job.CronExpression, cronDescOpts);
        log.Info($"Scheduling job {job.Code}: {cron} ({job.TimeZone})");

        var handler = handlers.GetHandlerFor(job.Code);

        var rjo = new RecurringJobOptions { TimeZone = TimeZoneLogic.GetInfoFromTimeZone(job.TimeZone) };

        if (handler.AllowConcurrent)
        {
            if (handler.SyncAction != null)
            {
                recurringJobManager.AddOrUpdate<JobExecutor>(
                    job.Code,
                    e => e.ExecuteSyncAllowConcurrent(job.Description ?? job.Code, job),
                    job.CronExpression,
                    rjo);
            }
            else
            {
                recurringJobManager.AddOrUpdate<JobExecutor>(
                    job.Code,
                    e => e.ExecuteAsyncAllowConcurrent(job.Description ?? job.Code, job),
                    job.CronExpression,
                    rjo);
            }
        }
        else
        {
            if (handler.SyncAction != null)
            {
                recurringJobManager.AddOrUpdate<JobExecutor>(
                    job.Code,
                    e => e.ExecuteSyncNoConcurrent(job.Description ?? job.Code, job),
                    job.CronExpression,
                    rjo);
            }
            else
            {
                recurringJobManager.AddOrUpdate<JobExecutor>(
                    job.Code,
                    e => e.ExecuteAsyncNoConcurrent(job.Description ?? job.Code, job),
                    job.CronExpression,
                    rjo);
            }
        }
    }

    private static bool JobChanged(PlinthJob job, RecurringJobDto? dto)
    {
        if (dto == null)
        {
            log.Trace($"delta: job {job.Code} not scheduled");
            return true;
        }

        if (dto.Job == null)
        {
            log.Trace($"job {job.Code} needs to be re-synced");
            return true;
        }

        var args = dto.Job.Args;
        if (args.Count != 2)
        {
            log.Trace($"job args for {job.Code}, count is {args.Count}");
            return true;
        }

        if (args[1] is not PlinthJob rbj)
        {
            log.Warn($"job args for {job.Code}, 2nd arg isn't PlinthJob: {args[1]?.GetType().FullName}");
            return true;
        }

        if (job.IsActive != rbj.IsActive
            || job.Description != rbj.Description
            || job.CronExpression != rbj.CronExpression
            || job.TimeZone != rbj.TimeZone)
        {
            log.Info($"delta: job has been updated, rescheduling {job.Code}");
            return true;
        }

        log.Trace($"delta: no change to job {job.Code}");
        return false;
    }
}
