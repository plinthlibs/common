﻿namespace Plinth.Hangfire.Impl.Sync;

/// <summary>
/// Implemented by database specific packages
/// </summary>
internal interface IJobRepository
{
    List<PlinthJob> GetJobList();
}
