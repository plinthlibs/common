﻿namespace Plinth.Hangfire.Impl.Sync;

/// <summary>
/// Implemented by database specific packages
/// </summary>
internal interface IJobSyncFactory
{
    /// <summary>
    /// Given a service provider, build out a JobSyncLogic instance
    /// </summary>
    /// <param name="provider"></param>
    /// <param name="handlers"></param>
    /// <returns></returns>
    JobSyncLogic Create(IServiceProvider provider, JobHandlerRegistrar handlers);
}
