﻿namespace Plinth.Hangfire;

/// <summary>
/// Constants for job sync logic
/// </summary>
public static class JobSyncConstants
{
    /// <summary>
    /// Name of the job list sync job
    /// </summary>
    public const string JobListSyncJobName = "Hangfire.SyncJobList";
}
