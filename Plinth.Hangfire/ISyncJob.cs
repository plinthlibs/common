﻿namespace Plinth.Hangfire;

/// <summary>
/// Interface for Hangfire jobs
/// </summary>
public interface ISyncJob
{
    /// <summary>
    /// Will be called on the defined schedule
    /// </summary>
    /// <param name="job"></param>
    void Execute(PlinthJob job);
}
