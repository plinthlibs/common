﻿namespace Plinth.Hangfire;

/// <summary>
/// Utility to trigger a job sync
/// </summary>
public interface IJobSyncManager
{
    /// <summary>
    /// Synchronize the job table with Hangfire
    /// </summary>
    void Sync();
}
