namespace Plinth.Hangfire;

/// <summary>
/// Class for registering job handlers
/// </summary>
public class JobHandlerRegistrar
{
    internal class Handler
    {
        public Type? SyncAction { get; set; }
        public Type? AsyncAction { get; set; }
        public bool AllowConcurrent { get; set; }
    }

    private readonly Dictionary<string, Handler> _handlers = [];
    private readonly Handler _globalHandler = new();

    internal JobHandlerRegistrar()
    {
    }

    /// <summary>
    /// Register a global job handler (used if no specific handler is registered)
    /// </summary>
    /// <typeparam name="T">handler type, resolved from container</typeparam>
    /// <param name="allowConcurrent">allow concurrent execution (default false)</param>
    public JobHandlerRegistrar RegisterGlobalHandler<T>(bool allowConcurrent = false) where T:ISyncJob
    {
        if (_globalHandler.AsyncAction != null)
            throw new NotSupportedException("Cannot register both async and sync global job handlers");
        if (_globalHandler.SyncAction != null)
            throw new NotSupportedException("A global handler is already registered");
        _globalHandler.SyncAction = typeof(T);
        _globalHandler.AllowConcurrent = allowConcurrent;
        return this;
    }

    /// <summary>
    /// Register a global job handler (async) (used if no specific handler is registered)
    /// </summary>
    /// <typeparam name="T">handler type, resolved from container</typeparam>
    /// <param name="allowConcurrent">allow concurrent execution (default false)</param>
    public JobHandlerRegistrar RegisterAsyncGlobalHandler<T>(bool allowConcurrent = false) where T:IAsyncJob
    {
        if (_globalHandler.SyncAction != null)
            throw new NotSupportedException("Cannot register both async and sync global job handlers");
        if (_globalHandler.AsyncAction != null)
            throw new NotSupportedException("A global handler is already registered");
        _globalHandler.AsyncAction = typeof(T);
        _globalHandler.AllowConcurrent = allowConcurrent;
        return this;
    }

    /// <summary>
    /// Register a handler for a specific job
    /// </summary>
    /// <typeparam name="T">handler type, resolved from container</typeparam>
    /// <param name="jobCode">job code for the job</param>
    /// <param name="allowConcurrent">allow concurrent execution (default false)</param>
    public JobHandlerRegistrar RegisterHandler<T>(string jobCode, bool allowConcurrent = false) where T:ISyncJob
    {
        if (_handlers.ContainsKey(jobCode))
            throw new NotSupportedException($"A handler for '{jobCode}' is already registered");
        _handlers[jobCode] = new Handler
        {
            SyncAction = typeof(T),
            AllowConcurrent = allowConcurrent
        };
        return this;
    }

    /// <summary>
    /// Register a handler (async) for a specific job
    /// </summary>
    /// <typeparam name="T">handler type, resolved from container</typeparam>
    /// <param name="jobCode">job code for the job</param>
    /// <param name="allowConcurrent">allow concurrent execution (default false)</param>
    public JobHandlerRegistrar RegisterAsyncHandler<T>(string jobCode, bool allowConcurrent = false) where T:IAsyncJob
    {
        if (_handlers.ContainsKey(jobCode))
            throw new NotSupportedException($"A handler for '{jobCode}' is already registered");
        _handlers[jobCode] = new Handler
        {
            AsyncAction = typeof(T),
            AllowConcurrent = allowConcurrent
        };
        return this;
    }

    internal Handler GetHandlerFor(string jobCode)
    {
        if (_handlers.TryGetValue(jobCode, out var h))
            return h;
        if (_globalHandler.SyncAction != null || _globalHandler.AsyncAction != null)
            return _globalHandler;
        throw new NotSupportedException($"No handler registered for '{jobCode}'");
    }
}
