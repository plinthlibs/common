﻿namespace Plinth.Hangfire;

/// <summary>
/// job model.
/// </summary>
public class PlinthJob
{
    /// <summary>
    /// Unique code for the job.
    /// </summary>
    public string Code { get; set; } = null!;

    /// <summary>
    /// Job description to display in UI.
    /// </summary>
    public string? Description { get; set; }

    /// <summary>
    /// Whether the job is active.
    /// </summary>
    public bool IsActive { get; set; }

    /// <summary>
    /// CRON expression for scheduling the job.
    /// See <see href="https://en.wikipedia.org/wiki/Cron" /> and <see href="https://crontab.guru" />,
    /// Also supports 6 part cron expressions for sub-minute (seconds)
    /// </summary>
    public string? CronExpression { get; set; }

    /// <summary>
    /// Time zone ID for the schedule (e.g. "Pacific Standard Time" or "UTC").
    /// </summary>
    public string? TimeZone { get; set; }

    /// <summary>
    /// Optional custom data for the job to use (should be JSON).
    /// </summary>
    public string? JobData { get; set; }
}
