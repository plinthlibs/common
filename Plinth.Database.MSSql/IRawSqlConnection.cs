using Microsoft.Data.SqlClient;

namespace Plinth.Database.MSSql;

/// <summary>
/// Interface for executing RAW SQL as well as stored procedures
/// </summary>
public interface IRawSqlConnection : ISqlConnection, IRawSqlConnectionBase
{
}
