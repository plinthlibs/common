﻿using System.Data;

namespace Plinth.Database.MSSql.Extensions;

/// <summary>
/// Extensions for database access
/// </summary>
public static class DatabaseExtensions
{
    /// <summary>
    /// Convert a list of Guids into a structured GUIDList type for sending to a stored proc
    /// </summary>
    /// <example>
    /// Usage:
    ///     <code>
    ///     new SqlParameter("@Param", SqlDbType.Structured)
    ///     {
    ///         Value = list.ToGuidList()
    ///     },
    ///     </code>
    /// DDL:
    ///     <code>
    ///     -- Type GUIDList
    ///     IF TYPE_ID(N'GUIDList') IS NOT NULL
    ///         DROP TYPE dbo.GUIDList;
    ///     CREATE TYPE dbo.GUIDList AS TABLE(
    ///         GUID UNIQUEIDENTIFIER NOT NULL PRIMARY KEY CLUSTERED
    ///     );
    ///     </code>
    /// </example>
    /// <param name="guidList">guids</param>
    /// <returns>DataTable for use in SqlParameter</returns>
    public static DataTable ToGuidList(this IEnumerable<Guid> guidList)
        => ToStructuredType(guidList, "GUID");

    /// <summary>
    /// Convert a list of strings into a structured VARCHARList type for sending to a stored proc
    /// </summary>
    /// <example>
    /// Usage:
    ///     <code>
    ///     new SqlParameter("@Param", SqlDbType.Structured)
    ///     {
    ///         Value = list.ToVarCharList()
    ///     },
    ///     </code>
    /// DDL:
    ///     <code>
    ///     -- Type VARCHARList
    ///     IF TYPE_ID(N'VARCHARList') IS NOT NULL
    ///         DROP TYPE dbo.VARCHARList;
    ///     CREATE TYPE dbo.VARCHARList AS TABLE(
    ///         Item VARCHAR(255) NOT NULL PRIMARY KEY CLUSTERED
    ///     );
    ///     </code>
    /// </example>
    /// <param name="varcharList">strings</param>
    /// <returns>DataTable for use in SqlParameter</returns>
    public static DataTable ToVarCharList(this IEnumerable<string> varcharList)
        => ToStructuredType(varcharList, "Item");

    /// <summary>
    /// Convert a list of strings into a structured NVARCHARList type for sending to a stored proc
    /// </summary>
    /// <example>
    /// Usage:
    ///     <code>
    ///     new SqlParameter("@Param", SqlDbType.Structured)
    ///     {
    ///         Value = list.ToNVarCharList()
    ///     },
    ///     </code>
    /// DDL:
    ///     <code>
    ///     -- Type NVARCHARList
    ///     IF TYPE_ID(N'NVARCHARList') IS NOT NULL
    ///         DROP TYPE dbo.NVARCHARList;
    ///     CREATE TYPE dbo.NVARCHARList AS TABLE(
    ///         Item NVARCHAR(255) NOT NULL PRIMARY KEY CLUSTERED
    ///     );
    ///     </code>
    /// </example>
    /// <param name="nvarcharList">strings</param>
    /// <returns>DataTable for use in SqlParameter</returns>
    public static DataTable ToNVarCharList(this IEnumerable<string> nvarcharList)
        => ToStructuredType(nvarcharList, "Item");

    /// <summary>
    /// create a single column table and add items to it
    /// </summary>
    private static DataTable ToStructuredType<T>(IEnumerable<T> items, string colName)
    {
        var table = new DataTable();
        table.Columns.Add(colName, typeof(T));
        foreach (var item in items)
        {
            var row = table.Rows.Add();
            row[colName] = item;
        }
        return table;
    }
}
