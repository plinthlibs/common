using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plinth.Database.MSSql.TransientErrorDetection;

/// <summary>
/// This is used as a sentinel for retry strategies to signal that a transient retry should not occur
/// </summary>
internal class NoRetryWrapperException(Exception? innerException) : Exception(string.Empty, innerException)
{
}
