#region license
// ==============================================================================
// Microsoft patterns & practices Enterprise Library
// Transient Fault Handling Application Block
// ==============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
// ==============================================================================
#endregion

namespace Plinth.MSSql.Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;

using System;

/// <summary>
/// Contains information that is required for the <see cref="RetryPolicy.Retrying"/> event.
/// </summary>
/// <param name="currentRetryCount">The current retry attempt count.</param>
/// <param name="delay">The delay that indicates how long the current thread will be suspended before the next iteration is invoked.</param>
/// <param name="lastException">The exception that caused the retry conditions to occur.</param>
internal class RetryingEventArgs(int currentRetryCount, TimeSpan delay, Exception? lastException) : EventArgs
{
    /// <summary>
    /// Gets the current retry count.
    /// </summary>
    public int CurrentRetryCount { get; private set; } = currentRetryCount;

    /// <summary>
    /// Gets the delay that indicates how long the current thread will be suspended before the next iteration is invoked.
    /// </summary>
    public TimeSpan Delay { get; private set; } = delay;

    /// <summary>
    /// Gets the exception that caused the retry conditions to occur.
    /// </summary>
    public Exception? LastException { get; private set; } = lastException;
}
