﻿namespace Plinth.Database.MSSql;

/// <summary>
/// A sequence of Result Sets (rows), from a multiple select query
/// </summary>
/// <remarks>Typical use case is a stored procedure that does multiple selects with different columns</remarks>
public interface IMultiResultSet : IEnumerable<IResultSet>, IDisposable
{
    /// <summary>
    /// Get the next Result Set (rows)
    /// </summary>
    IResultSet? NextResultSet();
}

/// <summary>
/// A sequence of Result Sets (rows), from a multiple select query, async
/// </summary>
/// <remarks>Typical use case is a stored procedure that does multiple selects with different columns</remarks>
public interface IAsyncMultiResultSet : IAsyncDisposable
{
    /// <summary>
    /// Get the next Result Set (rows)
    /// </summary>
    Task<IAsyncResultSet?> NextResultSetAsync(CancellationToken cancellationToken = default);
}
