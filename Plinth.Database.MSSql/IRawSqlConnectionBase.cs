using Microsoft.Data.SqlClient;

namespace Plinth.Database.MSSql;

/// <summary>
/// Interface for executing RAW SQL as well as stored procedures
/// </summary>
public interface IRawSqlConnectionBase
{
    #region Execute
    /// <summary>
    /// Execute a raw SQL command
    /// </summary>
    /// <param name="sql">The SQL query text</param>
    /// <param name="parameters"></param>
    /// <returns>the row count of modified rows</returns>
    int ExecuteRaw(string sql, params SqlParameter[] parameters);
    #endregion

    #region Raw Query
    /// <summary>
    /// Execute a raw SQL query and return results as an enumerable
    /// </summary>
    /// <param name="sql">The SQL query text</param>
    /// <param name="parameters"></param>
    /// <returns>An IEnumerable that can be interated on to get each returned row</returns>
    IEnumerable<IResult> ExecuteRawQuery(string sql, params SqlParameter[] parameters);

    /// <summary>
    /// Execute a raw SQL query that can return multiple result sets
    /// </summary>
    /// <param name="sql">The SQL query text</param>
    /// <param name="readerAction">An action that receives a multi-result-set which provides all the result sets</param>
    /// <param name="parameters"></param>
    void ExecuteRawQueryMultiResultSet(string sql, Action<IMultiResultSet> readerAction, params SqlParameter[] parameters);

    /// <summary>
    /// Execute a raw SQL query that returns a list of results
    /// </summary>
    /// <typeparam name="T">type of the objects in the list</typeparam>
    /// <param name="sql">The SQL query text</param>
    /// <param name="readerFunc">A function that converts a row to an object (only called if a row was returned)</param>
    /// <param name="parameters"></param>
    /// <returns>A List of objects, always non-null</returns>
    List<T> ExecuteRawQueryList<T>(string sql, Func<IResult, T> readerFunc, params SqlParameter[] parameters);

    /// <summary>
    /// Execute a raw SQL query that returns a single row
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="readerFunc">An action that will be called with a result (only called if a row was returned)</param>
    /// <param name="parameters"></param>
    /// <returns>true if a row was found, false otherwise</returns>
    bool ExecuteRawQueryOne(string sql, Action<IResult> readerFunc, params SqlParameter[] parameters);

    /// <summary>
    /// Execute a raw SQL query that returns a single row
    /// </summary>
    /// <typeparam name="T">type of the single value to return</typeparam>
    /// <param name="sql"></param>
    /// <param name="readerFunc">A function that extracts a single value from a result (only called if a row was returned).</param>
    /// <param name="parameters"></param>
    /// <returns>A SqlSingleResult containing the return value from readerFunc, or default if no results returned,
    /// and a bool containing whether a row was returned or not</returns>
    SingleResult<T> ExecuteRawQueryOne<T>(string sql, Func<IResult, T> readerFunc, params SqlParameter[] parameters);
    #endregion

    #region Execute Async
    /// <summary>
    /// Execute a raw SQL command, async
    /// </summary>
    /// <param name="sql">The SQL query text</param>
    /// <param name="parameters"></param>
    /// <returns>the row count of modified rows</returns>
    Task<int> ExecuteRawAsync(string sql, params SqlParameter[] parameters);

    /// <inheritdoc cref="ExecuteRawAsync(string, SqlParameter[])"/>
    Task<int> ExecuteRawAsync(string sql, CancellationToken cancellationToken, params SqlParameter[] parameters);
    #endregion

    #region Raw Query Async
    /// <summary>
    /// Execute a raw SQL query, async
    /// </summary>
    /// <param name="sql">The SQL query text</param>
    /// <param name="readerFunc">A function that converts a row to an object (only called if a row was returned), should be async</param>
    /// <param name="parameters"></param>
    /// <returns>An IEnumerable that can be interated on to get each returned row</returns>
    Task<List<T>> ExecuteRawQueryListAsync<T>(string sql, Func<IResult, Task<T>> readerFunc, params SqlParameter[] parameters);

    /// <inheritdoc cref="ExecuteRawQueryList{T}(string, Func{IResult, T}, SqlParameter[])"/>
    Task<List<T>> ExecuteRawQueryListAsync<T>(string sql, Func<IResult, Task<T>> readerFunc, CancellationToken cancellationToken, params SqlParameter[] parameters);

    /// <summary>
    /// Execute a raw SQL query that can return multiple result sets, async
    /// </summary>
    /// <param name="sql">The SQL query text</param>
    /// <param name="readerFunc">An async func that receives a multi-result-set which provides all the result sets</param>
    /// <param name="parameters"></param>
    Task ExecuteRawQueryMultiResultSetAsync(string sql, Func<IAsyncMultiResultSet, Task> readerFunc, params SqlParameter[] parameters);

    /// <inheritdoc cref="ExecuteRawQueryMultiResultSet(string, Action{IMultiResultSet}, SqlParameter[])"/>
    Task ExecuteRawQueryMultiResultSetAsync(string sql, Func<IAsyncMultiResultSet, Task> readerFunc, CancellationToken cancellationToken, params SqlParameter[] parameters);

    /// <summary>
    /// Execute a raw SQL query that returns a single row, async
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="readerFunc">An action that will be called with a result (only called if a row was returned)</param>
    /// <param name="parameters"></param>
    /// <returns>true if a row was found, false otherwise</returns>
    Task<bool> ExecuteRawQueryOneAsync(string sql, Func<IResult, Task> readerFunc, params SqlParameter[] parameters);

    /// <inheritdoc cref="ExecuteRawQueryOneAsync(string, Func{IResult, Task}, SqlParameter[])"/>
    Task<bool> ExecuteRawQueryOneAsync(string sql, Func<IResult, Task> readerFunc, CancellationToken cancellationToken, params SqlParameter[] parameters);

    /// <summary>
    /// Execute a raw SQL query that returns a single row, async
    /// </summary>
    /// <typeparam name="T">type of the single value to return</typeparam>
    /// <param name="sql"></param>
    /// <param name="readerFunc">A function that extracts a single value from a result (only called if a row was returned).</param>
    /// <param name="parameters"></param>
    /// <returns>A SqlSingleResult containing the return value from readerFunc, or default if no results returned,
    /// and a bool containing whether a row was returned or not</returns>
    Task<SingleResult<T>> ExecuteRawQueryOneAsync<T>(string sql, Func<IResult, Task<T>> readerFunc, params SqlParameter[] parameters);

    /// <inheritdoc cref="ExecuteRawQueryOneAsync{T}(string, Func{IResult, Task{T}}, SqlParameter[])"/>
    Task<SingleResult<T>> ExecuteRawQueryOneAsync<T>(string sql, Func<IResult, Task<T>> readerFunc, CancellationToken cancellationToken, params SqlParameter[] parameters);
    #endregion
}
