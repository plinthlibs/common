using Microsoft.Extensions.Logging;
using Plinth.MSSql.Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;

namespace Plinth.Database.MSSql;

using Impl;
using System.Linq;

/// <summary>
/// A factory for getting SQL connections
/// </summary>
public class SqlTransactionFactory
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    /// <summary>
    /// Sql Factory Settings
    /// </summary>
    public class FactorySettings
    {
        /// <summary>Max duration of single command</summary>
        public TimeSpan CommandTimeout { get; internal set; }
        /// <summary>Max retries for transient failure</summary>
        public int RetryCount { get; internal set; }
        /// <summary>Interval between retries</summary>
        public TimeSpan RetryInterval { get; internal set; }
        /// <summary>First retry is immediate or after RetryInterval</summary>
        public bool RetryFastFirst { get; internal set; }
        /// <summary>Disable transient error detection and retry</summary>
        public bool DisableTransientRetry { get; internal set; }
    }

    private class TxnFactory
    {
        public string ConnectionString { get; set; } = null!;
        public int TimeoutSeconds { get; set; }
        public bool DisableRetry { get; set; }
        public FactorySettings FactorySettings { get; set; } = null!;
        public ISqlTransactionProvider TxnProvider { get; internal set; } = null!;
    }

    private readonly IDictionary<string, TxnFactory> _connStrMap = new Dictionary<string, TxnFactory>();
    private readonly RetryPolicy _retryPolicy;
    private readonly RetryPolicy _noopRetryPolicy;

    private string _defaultConnName;

    /// <summary>
    /// Create a SQL Transaction Factory
    /// </summary>
    /// <param name="defaultName"></param>
    /// <param name="defaultConnStr"></param>
    /// <param name="defaultCommandTimeout">default timeout for sql commands in seconds</param>
    /// <param name="retryCount">how many times to retry on transient failures</param>
    /// <param name="retryIntervalMs">how often to retry on transient failures</param>
    /// <param name="retryFastFirst">if true, 1st retry will be immediate</param>
    /// <param name="disableTransientRetry">if true, the transaction will not retry on transient errors</param>
    public SqlTransactionFactory(string defaultName,
        string defaultConnStr,
        int defaultCommandTimeout = 30,
        int retryCount = 3,
        int retryIntervalMs = 200,
        bool retryFastFirst = true,
        bool disableTransientRetry = false)
    {
        var facSettings = new FactorySettings
        {
            CommandTimeout = TimeSpan.FromSeconds(defaultCommandTimeout),
            RetryCount = retryCount,
            RetryInterval = TimeSpan.FromMilliseconds(retryIntervalMs),
            RetryFastFirst = retryFastFirst,
            DisableTransientRetry = disableTransientRetry
        };

        var strategy = new FixedInterval("Plinth", retryCount, TimeSpan.FromMilliseconds(retryIntervalMs), retryFastFirst);
        _retryPolicy = new RetryPolicy<PlinthSqlDatabaseTransientErrorDetectionStrategy>(strategy);
        _noopRetryPolicy = new RetryPolicy<NOOPTransientErrorDetectionStrategy>(new FixedInterval(0));

        MapConnectionString(defaultName, defaultConnStr, facSettings);
        _defaultConnName = defaultName;
    }

    /// <summary>
    /// Create a SQL Transaction factory from an appsettings.json config file
    /// </summary>
    /// <param name="config"></param>
    /// <param name="defaultConnName">name of the default connection string</param>
    /// <param name="defaultConnStr"></param>
    /// <param name="configSection">section name in json file, default is 'PlinthMSSqlSettings'</param>
    /// <example>
    ///     - PlinthMSSqlSettings.
    ///         "SqlCommandTimeout": "00:00:50", or 50
    ///         "SqlRetryCount": 3,
    ///         "SqlRetryInterval": "00:00:00.200", or 200
    ///         "SqlRetryFastFirst": true,
    ///         "DisableTransientRetry": false
    /// </example>
    public SqlTransactionFactory(IConfiguration config, 
        string defaultConnName, 
        string defaultConnStr, 
        string configSection = "PlinthMSSqlSettings")
    {
        var settings = new ConfigSettings();
        config.GetSection(configSection).Bind(settings);

        var facSettings = new FactorySettings
        {
            CommandTimeout = settings.SqlCommandTimeoutTimeSpan,
            RetryCount = settings.SqlRetryCount,
            RetryInterval = settings.SqlRetryIntervalTimeSpan,
            RetryFastFirst = settings.SqlRetryFastFirst,
            DisableTransientRetry = settings.DisableTransientRetry
        };

        var strategy = new FixedInterval("Plinth", settings.SqlRetryCount, facSettings.RetryInterval, settings.SqlRetryFastFirst);
        _retryPolicy = new RetryPolicy<PlinthSqlDatabaseTransientErrorDetectionStrategy>(strategy);
        _noopRetryPolicy = new RetryPolicy<NOOPTransientErrorDetectionStrategy>(new FixedInterval(0));

        MapConnectionString(defaultConnName, defaultConnStr, facSettings);
        _defaultConnName = defaultConnName;
    }

    private class ConfigSettings
    {
        public string SqlCommandTimeout { get; set; } = "00:00:50";
        public int SqlRetryCount { get; set; } = 3;
        public string SqlRetryInterval { get; set; } = "00:00:00.200";
        public bool SqlRetryFastFirst { get; set; } = true;
        public bool DisableTransientRetry { get; set; } = false;

        internal TimeSpan SqlCommandTimeoutTimeSpan => Convert(SqlCommandTimeout, false);
        internal TimeSpan SqlRetryIntervalTimeSpan => Convert(SqlRetryInterval, true);

        private static TimeSpan Convert(string val, bool millis)
        {
            if (val.IndexOf(':') > -1 && TimeSpan.TryParse(val, out var ts))
                return ts;
            else if (double.TryParse(val, out var d))
                return millis ? TimeSpan.FromMilliseconds(d) : TimeSpan.FromSeconds(d);
            else
                throw new ArgumentException($"could not parse time span '{val}'");
        }
    }

    /// <summary>
    /// Map a connection name to a connection string for later retrieval from the factory
    /// </summary>
    /// <param name="name"></param>
    /// <param name="connectionStr"></param>
    /// <param name="commandTimeout"></param>
    /// <param name="disableTransientRetry">if true, the transaction will not retry on transient errors</param>
    public void MapConnectionString(string name, string connectionStr, int commandTimeout = 30, bool disableTransientRetry = false)
    {
        var dfs = GetDefaultFactorySettings();
        MapConnectionString(name, connectionStr, new FactorySettings
        {
            CommandTimeout = TimeSpan.FromSeconds(commandTimeout),
            DisableTransientRetry = disableTransientRetry,
            RetryCount = dfs.RetryCount,
            RetryFastFirst = dfs.RetryFastFirst,
            RetryInterval = dfs.RetryInterval
        });
    }

    private void MapConnectionString(string name, string connectionStr, FactorySettings facSettings)
    {
        var fac = new TxnFactory()
        {
            ConnectionString = connectionStr,
            TimeoutSeconds = (int)facSettings.CommandTimeout.TotalSeconds,
            DisableRetry = facSettings.DisableTransientRetry,
            FactorySettings = facSettings,
        };

        fac.TxnProvider = new PlinthSqlTransactionProvider(fac.ConnectionString, fac.TimeoutSeconds, fac.DisableRetry ? _noopRetryPolicy : _retryPolicy);
        _connStrMap[name] = fac;
    }

    /// <summary>
    /// Map the default unnamed connection string for later retrieval from the factory
    /// </summary>
    /// <param name="name"></param>
    /// <param name="connectionStr"></param>
    /// <param name="commandTimeout"></param>
    public void MapDefaultConnectionString(string name, string connectionStr, int commandTimeout = 30)
    {
        MapConnectionString(name, connectionStr, commandTimeout);
        _defaultConnName = name;
    }

    /// <summary>
    /// Get a transaction provider from the factory
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public ISqlTransactionProvider Get(string name) => _connStrMap[name].TxnProvider;

    /// <summary>
    /// Get the default transaction provider from the factory
    /// </summary>
    /// <returns></returns>
    public ISqlTransactionProvider GetDefault() => Get(_defaultConnName);

    /// <summary>
    /// Get the factory settings for a connection name
    /// </summary>
    public FactorySettings GetFactorySettings(string name) => _connStrMap[name].FactorySettings;

    /// <summary>
    /// Get the factory settings for the default
    /// </summary>
    public FactorySettings GetDefaultFactorySettings() => GetFactorySettings(_defaultConnName);

    private class NOOPTransientErrorDetectionStrategy : ITransientErrorDetectionStrategy
    {
        public bool IsTransient(Exception? ex)
        {
            return false;
        }
    }

    /// <summary>
    /// Build a connectiong string from parts using user/pass security
    /// </summary>
    /// <param name="host">database server hostname or IP</param>
    /// <param name="database">database name</param>
    /// <param name="user">username</param>
    /// <param name="password">password</param>
    /// <param name="port">(default = 1433) optionally set the port</param>
    /// <param name="multipleActiveResults">(default = true) enable multiple active result sets per connection</param>
    /// <returns>a connection string</returns>
    /// <seealso cref="SqlConnectionStringBuilder"/>
    public static string BuildUserPassConnectionString(
        string host,
        string database,
        string user,
        string password,
        int? port = null,
        bool multipleActiveResults = true)
    {
        return new SqlConnectionStringBuilder()
        {
            DataSource = host + (port.HasValue ? $",{port.Value}" : string.Empty),
            InitialCatalog = database,
            UserID = user,
            Password = password,
            MultipleActiveResultSets = multipleActiveResults,
            PersistSecurityInfo = true
        }.ToString();
    }

    /// <summary>
    /// Build a connectiong string from parts using integrated/domain security
    /// </summary>
    /// <param name="host">database server hostname or IP</param>
    /// <param name="database">database name</param>
    /// <param name="port">(default = 1433) optionally set the port</param>
    /// <param name="multipleActiveResults">(default = true) enable multiple active result sets per connection</param>
    /// <returns>a connection string</returns>
    /// <seealso cref="SqlConnectionStringBuilder"/>
    public static string BuildDomainConnectionString(
        string host,
        string database,
        int? port = null,
        bool multipleActiveResults = true)
    {
        return new SqlConnectionStringBuilder()
        {
            DataSource = host + (port.HasValue ? $",{port.Value}" : string.Empty),
            InitialCatalog = database,
            IntegratedSecurity = true,
            MultipleActiveResultSets = multipleActiveResults,
            PersistSecurityInfo = true
        }.ToString();
    }

    private class PlinthSqlDatabaseTransientErrorDetectionStrategy : ITransientErrorDetectionStrategy
    {
        private readonly SqlDatabaseTransientErrorDetectionStrategy _strategy = new();

        public bool IsTransient(Exception? ex)
        {
            bool isTransient = false;

            if (ex == null)
                return isTransient;

            if (ex is SqlException sqex)
            {
                // Enumerate through all errors found in the exception.
                foreach (SqlError err in sqex.Errors.Cast<SqlError>())
                {
                    switch (err.Number)
                    {
                        case -2:   // timeout
                        case 1205: // deadlock
                            isTransient = true;
                            break;

                        default:
                            // Intentionally left blank here.
                            break;
                    }
                }
            }

            isTransient |= _strategy.IsTransient(ex);

            if (isTransient)
                log.Warn($"Found transient error, will retry transaction {ex.GetType().Name}: {ex.Message}");

            return isTransient;
        }
    }
}
