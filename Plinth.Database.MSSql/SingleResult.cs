namespace Plinth.Database.MSSql;

/// <summary>
/// Represents a single result that might or might not be present
/// (essentially Optional(T))
/// </summary>
/// <typeparam name="T"></typeparam>
/// <remarks>
/// Construct a SingleResult
/// </remarks>
public class SingleResult<T>(T? val, bool hasVal)
{
    /// <summary>
    /// The value returned
    /// </summary>
    public T? Value => RowReturned ? val : default;

    /// <summary>
    /// True if the query returned a result
    /// </summary>
    public bool RowReturned => hasVal;
}
