using System.Collections;
using System.Data;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;

namespace Plinth.Database.MSSql.Impl;

internal class MultiResultSet : IMultiResultSet, IAsyncMultiResultSet
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    protected readonly SqlDataReader _reader;
    private readonly ResultSet _rs;
    private readonly ResultSetAsync _rsAsync;
    private bool _first;

    public MultiResultSet(IDataReader reader)
    {
        _reader = (SqlDataReader)reader;
        _rs = new ResultSet(_reader, false);
        _rsAsync = new ResultSetAsync(_reader, false);
        _first = true;
    }

    public IResultSet? NextResultSet()
    {
        if (_first)
            _first = false;
        else if (!_reader.NextResult())
            return null;
        log.Debug("next result set");
        _rs.Reset();
        return _rs;
    }

    public async Task<IAsyncResultSet?> NextResultSetAsync(CancellationToken cancellationToken = default)
    {
        if (_first)
            _first = false;
        else if (!await _reader.NextResultAsync(cancellationToken))
            return null;
        log.Debug("next result set");
        _rsAsync.Reset();
        return _rsAsync;
    }

    public virtual IEnumerator<IResultSet> GetEnumerator()
    {
        _first = false;
        do
        {
            log.Debug("next result set");
            yield return _rs;
            _rs.Reset();
        }
        while (_reader.NextResult());
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Dispose()
    {
        _rs.Dispose();
        _rsAsync.Dispose();
        _reader.Dispose();
    }

    public async ValueTask DisposeAsync()
    {
        _rs.Dispose();
        await _rsAsync.DisposeAsync();
        await _reader.DisposeAsync();
    }
}
