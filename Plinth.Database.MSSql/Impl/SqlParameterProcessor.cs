﻿using System.Data;
using Microsoft.Data.SqlClient;

namespace Plinth.Database.MSSql.Impl;

internal static class SqlParameterProcessor
{
    /// <summary>
    /// Process any changes necessary for SqlParameters
    /// </summary>
    /// <remarks>
    /// <list>
    ///   <item>DateTime => to UTC, and change type to DateTime2, if starting with <c>SqlConstants.NoUtcDateTimeHeader</c> skip UTC conversion</item>
    ///   <item>Enums => to string plus warning</item>
    ///   <item>Strings => If starting with <c>SqlConstants.DoNotTrimHeader</c>, value will not be trimmed, Otherwise, .Trim()</item>
    ///   <item>Objects => By default, null won't be passed to SQL, prepend <c>SqlConstants.NullableHeader</c> to explicitly convert to DBNull.Value</item>
    /// </list>
    /// </remarks>
    /// <param name="param">[in/out] will be modified if needed</param>
    public static void ProcessParam(SqlParameter param)
    {
        // nullable parameters must explicitly set DBNull.Value for null values
        if (param.ParameterName[0] != '@'
            && param.ParameterName.StartsWith(SqlConstants.NullableHeader))
        {
            param.ParameterName = param.ParameterName[SqlConstants.NullableHeader.Length..];
            param.Value ??= DBNull.Value;
        }

        // null values require no processing
        if (param.Value == null || param.Value == DBNull.Value)
        {
            ProcessDoNotTrim(param);
            return;
        }

        // ensure all datetimes are UTC
        // convert to DateTime2 to preserve correct milliseconds
        if (param.DbType == DbType.DateTime)
        {
            // allow override of UTC datetime conversion
            if (param.ParameterName.StartsWith(SqlConstants.NoUtcDateTimeHeader))
                param.ParameterName = param.ParameterName[SqlConstants.NoUtcDateTimeHeader.Length..];
            else
                param.Value = ((DateTime)param.Value).ToUniversalTime();
            param.DbType = DbType.DateTime2;
            return;
        }

        // enums should be .ToString()'d before getting here
        if (param.Value.GetType().IsEnum)
        {
            param.Value = param.Value.ToString();
            return;
        }

        // strings are automatically trimmed, unless the parameter name starts with the DoNotTrimHeader
        if (param.Value is string s)
        {
            if (param.ParameterName[0] == '@')
                param.Value = s.Trim();
            else
                ProcessDoNotTrim(param);
            return;
        }
    }

    private static void ProcessDoNotTrim(SqlParameter param)
    {
        if (param.ParameterName.StartsWith(SqlConstants.DoNotTrimHeader))
            param.ParameterName = param.ParameterName[SqlConstants.DoNotTrimHeader.Length..];
    }
}
