using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Plinth.Common.Logging;
using System.Data;

using static Plinth.Common.Logging.Scopes;

namespace Plinth.Database.MSSql.Impl;

internal static partial class PlinthSqlConnectionCommon
{
    public static SqlCommand PrepareCmd(
        ILogger log,
        string commandText,
        CommandType type,
        PlinthSqlTransaction? txn,
        SqlConnection? conn,
        int commandTimeout,
        SqlParameter[] parameters)
    {
        ArgumentNullException.ThrowIfNull(commandText);

        SqlCommand cmd = txn == null
            ? forConn(conn ?? throw new InvalidOperationException(), commandText, type, commandTimeout)
            : forTxn(commandText, type, txn, commandTimeout);

        foreach (var p in parameters)
        {
            if (p == null)
                continue;
            SqlParameterProcessor.ProcessParam(p);
            cmd.Parameters.Add(p);
        }

        if (log.IsDebugEnabled())
        {
            if (cmd.CommandType == CommandType.StoredProcedure)
                log.Debug(StoredProcLogger.ExecToString(commandText, parameters));
            else
                log.Debug(StoredProcLogger.RawExecToString(commandText, parameters));
        }

        return cmd;

        static SqlCommand forTxn(string commandText, CommandType type, PlinthSqlTransaction txn, int commandTimeout)
        {
            txn.CheckIsOpen();

            return txn.GetCommand(commandText, type, commandTimeout);
        }

        static SqlCommand forConn(SqlConnection conn, string commandText, CommandType type, int commandTimeout)
        {
            return new SqlCommand()
            {
                Connection = conn,
                CommandText = commandText,
                CommandTimeout = commandTimeout,
                CommandType = type
            };
        }
    }

    public static KeyValuePair<string, object?>[] ProcMeta(string procName)
    {
        return CreateScope(
            Kvp( "StoredProc", procName ),
            Kvp( LoggingConstants.Field_TimeOperation, procName )
        );
    }

    #region rowcount logging
    public static int LogRows(ILogger log, int rows)
    {
        LogDefinesCommon.LogRowsAffected(log, rows);
        return rows;
    }
    #endregion

    private static partial class LogDefinesCommon
    {
        [LoggerMessage(99, LogLevel.Debug, "{RowCount} row(s) affected", EventName = "RowsAffected")]
        public static partial void LogRowsAffected(ILogger logger, int rowCount);
    }
}
