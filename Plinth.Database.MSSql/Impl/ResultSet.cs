using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Plinth.Common.Extensions;
using Plinth.Serialization;
using System.Collections;
using System.Data;
using Microsoft.Data.SqlClient;

namespace Plinth.Database.MSSql.Impl;

/// <remarks>
/// All "pos" variables are 0-based
/// These are private until a use-case presents itself where you need position access
/// </remarks>
internal partial class ResultSet : IResultSet, IResult
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    protected readonly IDataReader _reader;
    private readonly bool _allowDispose;
    private int rows = 0;

    public ResultSet(IDataReader reader, bool allowDispose = true)
    {
        _reader = reader;
        _allowDispose = allowDispose;
    }

    public virtual IResult? NextResult()
    {
        if (!_reader.Read())
        {
            LogDefines.LogRows(log, rows);
            return null;
        }

        rows++;
        return this;
    }

    internal virtual void Reset()
    {
        rows = 0;
    }

    /// <summary>
    /// Dispose of the result set
    /// </summary>
    public void Dispose()
    {
        if (_allowDispose)
            _reader.Dispose();
    }

    public virtual IEnumerator<IResult> GetEnumerator()
    {
        while (_reader.Read())
        {
            rows++;
            yield return this;
        }

        LogDefines.LogRows(log, rows);
    }

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

    private object? GetValue(int pos) => IsNull(pos) ? null : _reader.GetValue(pos);
    public object? GetValue(string name) => GetValue(_reader.GetOrdinal(name));

    private string? GetString(int pos) => IsNull(pos) ? null : _reader.GetString(pos);
    public string? GetString(string name) => GetString(_reader.GetOrdinal(name));

    private byte GetByte(int pos) => _reader.GetByte(pos);
    public byte GetByte(string name) => GetByte(_reader.GetOrdinal(name));

    private byte? GetByteNull(int pos) => IsNull(pos) ? null : GetByte(pos);
    public byte? GetByteNull(string name) => GetByteNull(_reader.GetOrdinal(name));

    private short GetShort(int pos) => _reader.GetInt16(pos);
    public short GetShort(string name) => GetShort(_reader.GetOrdinal(name));

    private short? GetShortNull(int pos) => IsNull(pos) ? null : GetShort(pos);
    public short? GetShortNull(string name) => GetShortNull(_reader.GetOrdinal(name));

    private int GetInt(int pos) => _reader.GetInt32(pos);
    public int GetInt(string name) => GetInt(_reader.GetOrdinal(name));

    private int? GetIntNull(int pos) => IsNull(pos) ? null : GetInt(pos);
    public int? GetIntNull(string name) => GetIntNull(_reader.GetOrdinal(name));

    private long GetLong(int pos) => _reader.GetInt64(pos);
    public long GetLong(string name) => GetLong(_reader.GetOrdinal(name));

    private long? GetLongNull(int pos) => IsNull(pos) ? null : GetLong(pos);
    public long? GetLongNull(string name) => GetLongNull(_reader.GetOrdinal(name));

    private decimal GetDecimal(int pos) => _reader.GetDecimal(pos);
    public decimal GetDecimal(string name) => GetDecimal(_reader.GetOrdinal(name));

    private decimal? GetDecimalNull(int pos) => IsNull(pos) ? null : GetDecimal(pos);
    public decimal? GetDecimalNull(string name) => GetDecimalNull(_reader.GetOrdinal(name));

    private double GetDouble(int pos) => _reader.GetDouble(pos);
    public double GetDouble(string name) => GetDouble(_reader.GetOrdinal(name));

    private double? GetDoubleNull(int pos) => IsNull(pos) ? null : GetDouble(pos);
    public double? GetDoubleNull(string name) => GetDoubleNull(_reader.GetOrdinal(name));

    private bool GetBool(int pos) => _reader.GetBoolean(pos);
    public bool GetBool(string name) => GetBool(_reader.GetOrdinal(name));

    private bool? GetBoolNull(int pos) => IsNull(pos) ? null : GetBool(pos);
    public bool? GetBoolNull(string name) => GetBoolNull(_reader.GetOrdinal(name));

    private DateTime GetDateTime(int pos, DateTimeKind kind = DateTimeKind.Utc) => DateTime.SpecifyKind(_reader.GetDateTime(pos), kind);
    public DateTime GetDateTime(string name, DateTimeKind kind = DateTimeKind.Utc) => GetDateTime(_reader.GetOrdinal(name), kind);

    private DateTime? GetDateTimeNull(int pos, DateTimeKind kind = DateTimeKind.Utc) => IsNull(pos) ? null : GetDateTime(pos, kind);
    public DateTime? GetDateTimeNull(string name, DateTimeKind kind = DateTimeKind.Utc) => GetDateTimeNull(_reader.GetOrdinal(name), kind);

    private Guid GetGuid(int pos) => _reader.GetGuid(pos);
    public Guid GetGuid(string name) => GetGuid(_reader.GetOrdinal(name));

    private Guid? GetGuidNull(int pos) => IsNull(pos) ? null : GetGuid(pos);
    public Guid? GetGuidNull(string name) => GetGuidNull(_reader.GetOrdinal(name));

    public T GetEnum<T>(string name) where T : struct, Enum
        => GetString(name)!.ToEnum<T>();

    public T GetEnumDefault<T>(string name, T defaultVal) where T : struct, Enum
        => GetString(name).ToEnumOrDefault(defaultVal);

    public T? GetEnumNull<T>(string name) where T : struct, Enum
        => GetString(name).ToEnumOrNull<T>();

    private byte[]? GetBytes(int pos) => IsNull(pos) ? null : (byte[])_reader.GetValue(pos);
    public byte[]? GetBytes(string name) => GetBytes(_reader.GetOrdinal(name));

    private bool IsNull(int pos) => _reader.IsDBNull(pos);
    public bool IsNull(string name) => IsNull(_reader.GetOrdinal(name));

    public List<string> GetColumns()
    {
        return Enumerable.Range(0, _reader.FieldCount)
                         .Select(i => _reader.GetName(i))
                         .ToList();
    }

    public List<object> GetValues()
    {
        var values = new object[_reader.FieldCount];
        _reader.GetValues(values);

        return values.ToList();
    }

    private TimeSpan GetTimeSpan(int pos) => (TimeSpan)_reader.GetValue(pos);
    public TimeSpan GetTimeSpan(string name) => GetTimeSpan(_reader.GetOrdinal(name));

    private TimeSpan? GetTimeSpanNull(int pos) => IsNull(pos) ? null : GetTimeSpan(pos);
    public TimeSpan? GetTimeSpanNull(string name) => GetTimeSpanNull(_reader.GetOrdinal(name));

    private Stream? GetStream(int pos) => IsNull(pos) ? null : (_reader as SqlDataReader)?.GetStream(pos);
    public Stream? GetStream(string name) => GetStream(_reader.GetOrdinal(name));

    public IDictionary<string, object>? GetJson(string name, Action<JsonSerializerSettings>? m = null) => GetJsonObject<IDictionary<string,object>>(_reader.GetOrdinal(name), m);

    private T? GetJsonObject<T>(int pos, Action<JsonSerializerSettings>? m) => IsNull(pos) ? default : JsonUtil.DeserializeObject<T>(_reader.GetString(pos), m);
    public T? GetJsonObject<T>(string name, Action<JsonSerializerSettings>? m = null) => GetJsonObject<T>(_reader.GetOrdinal(name), m);

    public SingleResult<T> GetOne<T>(Func<IResult, T> readerFunc)
    {
        var r = NextResult();
        if (r != null)
        {
            LogDefines.LogRows(log, 1);
            return new SingleResult<T>(readerFunc.Invoke(r), true);
        }
        return new SingleResult<T>(default, false);
    }

    public bool GetOne(Action<IResult> readerFunc)
    {
        var r = NextResult();
        if (r != null)
        {
            LogDefines.LogRows(log, 1);
            readerFunc.Invoke(r);
            return true;
        }
        return false;
    }

    public List<T> GetList<T>(Func<IResult, T> readerFunc)
    {
        return this.Select(readerFunc).ToList();
    }

    private static partial class LogDefines
    {
        [LoggerMessage(0, LogLevel.Debug, "{RowCount} row(s) returned", EventName = "Rows")]
        public static partial void LogRows(ILogger logger, int rowCount);
    }
}
