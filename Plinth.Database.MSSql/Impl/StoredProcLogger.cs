﻿using System.Data;
using Microsoft.Data.SqlClient;
using System.Globalization;
using System.Text;

namespace Plinth.Database.MSSql.Impl;

internal static class StoredProcLogger
{
    /// <summary>
    /// Converts a stored procedure execution to string so it can be logged
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="parameters"></param>
    /// <returns>a string containing executable sql</returns>
    public static string ExecToString(string procName, IEnumerable<SqlParameter?> parameters)
    {
        var sb = new StringBuilder(128);
        sb.AppendFormat("EXECUTE {0} ", procName);

        sb.Append(string.Join(", ", parameters.Where(p => p?.Value != null).Select(p => ParamToString(p!))));

        return sb.ToString();
    }

    /// <summary>
    /// Converts a raw sql call to a string to it can be logged
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns>a string containing the sql followed by the parameters</returns>
    public static string RawExecToString(string sql, IEnumerable<SqlParameter?> parameters)
    {
        var sb = new StringBuilder(128);
        sb.Append(sql);

        int mark = sb.Length;
        sb.Append(" ==> VALUES (");
        string paramStr = string.Join(", ", parameters.Where(p => p != null).Select(p => ParamToString(p!)));
        sb.Append(paramStr);
        sb.Append(')');

        if (string.IsNullOrEmpty(paramStr))
            sb.Length = mark;

        return sb.ToString();
    }

    private static string ParamToString(SqlParameter p)
    {
        if (p.Direction == ParameterDirection.Input)
            return $"{p.ParameterName} = {ValueToString(p)}";
        else
            return $"{p.ParameterName} = {ValueToString(p)} OUTPUT";
    }
    
    private static string ValueToString(SqlParameter p)
    {
        if (p.Value == null || p.Value == DBNull.Value)
            return "NULL";
        else if (p.Value is string || p.Value is Guid)
            return $"'{p.Value}'";
        else if (p.Value is bool b)
            return $"{(b ? '1' : '0')}";
        else if (p.Value is DateTime dt)
            return $"'{dt.ToString("o", CultureInfo.InvariantCulture)}'";
        else if (p.SqlDbType == SqlDbType.Structured)
            return TableTypeToString(p.Value as DataTable);
        else
            return p.Value.ToString() ?? "<null>";
    }

    private static string TableTypeToString(DataTable? table)
    {
        if (table == null)
            return string.Empty;

        /*
        if (table.Rows.Count < 10 && table.Columns.Count < 10)
        {
            string cols = string.Join(",", table.Columns.OfType<DataColumn>().Select(c => c.ColumnName));
            string rows = string.Join("), (", table.Rows.OfType<DataRow>().Select(r => string.Join(",", r.ItemArray)));
            return $"{param} = [({cols}) VALS ({rows})]";
        }
        */
        
        return $"[{table.Rows.Count} rows, {table.Columns.Count} cols]";
    }
}
