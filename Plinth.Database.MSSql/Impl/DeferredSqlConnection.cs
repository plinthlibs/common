using Microsoft.Extensions.Logging;
using System.Collections.Concurrent;
using Microsoft.Data.SqlClient;
using System.Runtime.CompilerServices;
using static Plinth.Database.MSSql.Impl.PlinthSqlConnection;

namespace Plinth.Database.MSSql.Impl;

internal class DeferredSqlConnection(int defaultTimeout) : IDeferredSqlConnection
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private class DeferredCall
    {
        public string Proc = null!;
        public SqlParameter[] Parameters = null!;
        public int CommandTimeout;
        public bool CheckRows;
        public int? ExpectedRows;
    }

    private readonly ConcurrentQueue<DeferredCall> _deferredCalls = new();
    private readonly Lazy<List<PostAction>> _postCommitActions = new();

    public int CommandTimeout { get; set; } = defaultTimeout;

    private bool _failed;

    public bool IsAsync() => true;

    public void DeferProc(string procName, bool checkRowsReturned, params SqlParameter[] parameters)
    {
        log.Debug("Deferring call to {0}: check = {1}", procName, checkRowsReturned);

        _deferredCalls.Enqueue(new DeferredCall()
        {
            Proc = procName,
            Parameters = parameters,
            CommandTimeout = CommandTimeout,
            CheckRows = checkRowsReturned,
            ExpectedRows = null
        });
    }

    public void DeferProc(string procName, int expectedRows, params SqlParameter[] parameters)
    {
        log.Debug("Deferring call to {0}: expected rows = {1}", procName, expectedRows);

        _deferredCalls.Enqueue(new DeferredCall()
        {
            Proc = procName,
            Parameters = parameters,
            CommandTimeout = CommandTimeout,
            CheckRows = false,
            ExpectedRows = expectedRows
        });
    }

    public void ExecuteAll(ISqlConnection conn)
    {
        if (_failed)
        {
            conn.SetRollback();
            return;
        }

        int origTimeout = conn.CommandTimeout;

        while (_deferredCalls.TryDequeue(out var call))
        {
            conn.CommandTimeout = call.CommandTimeout;
            if (call.CheckRows)
                conn.ExecuteProc(call.Proc, call.Parameters);
            else if (call.ExpectedRows.HasValue)
                conn.ExecuteProc(call.Proc, call.ExpectedRows.Value, call.Parameters);
            else
                conn.ExecuteProcUnchecked(call.Proc, call.Parameters);
        }

        if (_postCommitActions.IsValueCreated)
        {
            foreach (var a in _postCommitActions.Value)
                if (a.action != null)
                    conn.AddPostCommitAction(a.Desc, a.action);
                else if (a.actionAsync != null)
                    conn.AddAsyncPostCommitAction(a.Desc, a.actionAsync);
                else
                    throw new InvalidOperationException("no action defined");
        }

        conn.CommandTimeout = origTimeout;
    }

    public async Task ExecuteAllAsync(ISqlConnection conn, CancellationToken cancellationToken = default)
    {
        if (_failed)
        {
            conn.SetRollback();
            return;
        }

        int origTimeout = conn.CommandTimeout;

        while (_deferredCalls.TryDequeue(out var call))
        {
            conn.CommandTimeout = call.CommandTimeout;
            if (call.CheckRows)
                await conn.ExecuteProcAsync(call.Proc, cancellationToken, call.Parameters);
            else if (call.ExpectedRows.HasValue)
                await conn.ExecuteProcAsync(call.Proc, call.ExpectedRows.Value, cancellationToken, call.Parameters);
            else
                await conn.ExecuteProcUncheckedAsync(call.Proc, cancellationToken, call.Parameters);
        }

        conn.CommandTimeout = origTimeout;
    }

    public void SetRollback()
    {
        _failed = true;
    }

    public bool WillBeRollingBack()
    {
        return _failed;
    }

    public void ExecuteProc(string procName, params SqlParameter[] parameters)
    {
        DeferProc(procName, true, parameters);
    }

    public void ExecuteProc(string procName, int expectedRows, params SqlParameter[] parameters)
    {
        DeferProc(procName, expectedRows, parameters);
    }

    public int ExecuteProcUnchecked(string procName, params SqlParameter[] parameters)
    {
        DeferProc(procName, false, parameters);
        // always return 1, since this is unchecked and we don't know how many will be returned
        return 1;
    }

    public IEnumerable<IResult> ExecuteQueryProc(string procName, params SqlParameter[] parameters)
    {
        throw new NotSupportedException("cannot read on deferred connection");
    }

    public List<T> ExecuteQueryProcList<T>(string procName, Func<IResult, T> readerFunc, params SqlParameter[] parameters)
    {
        throw new NotSupportedException("cannot read on deferred connection");
    }

    public bool ExecuteQueryProcOne(string procName, Action<IResult> readerFunc, params SqlParameter[] parameters)
    {
        throw new NotSupportedException("cannot read on deferred connection");
    }

    public SingleResult<T> ExecuteQueryProcOne<T>(string procName, Func<IResult, T> readerFunc, params SqlParameter[] parameters)
    {
        throw new NotSupportedException("cannot read on deferred connection");
    }

    public Task ExecuteProcAsync(string procName, params SqlParameter[] parameters)
    {
        ExecuteProc(procName, parameters);
        return Task.CompletedTask;
    }

    public Task ExecuteProcAsync(string procName, int expectedRows, params SqlParameter[] parameters)
    {
        ExecuteProc(procName, expectedRows, parameters);
        return Task.CompletedTask;
    }

    public Task<int> ExecuteProcUncheckedAsync(string procName, params SqlParameter[] parameters)
    {
        int ret = ExecuteProcUnchecked(procName, parameters);
        return Task.FromResult(ret);
    }

    public Task ExecuteProcAsync(string procName, CancellationToken cancellationToken, params SqlParameter[] parameters)
    {
        return ExecuteProcAsync(procName, parameters);
    }

    public Task ExecuteProcAsync(string procName, int expectedRows, CancellationToken cancellationToken, params SqlParameter[] parameters)
    {
        return ExecuteProcAsync(procName, expectedRows, parameters);
    }

    public Task<int> ExecuteProcUncheckedAsync(string procName, CancellationToken cancellationToken, params SqlParameter[] parameters)
    {
        return ExecuteProcUncheckedAsync(procName, parameters);
    }

    public Task<List<T>> ExecuteQueryProcListAsync<T>(string procName, Func<IResult, Task<T>> readerFunc, params SqlParameter[] parameters)
        => throw new NotSupportedException("cannot read on deferred connection");

    public Task<bool> ExecuteQueryProcOneAsync(string procName, Func<IResult, Task> readerFunc, params SqlParameter[] parameters)
        => throw new NotSupportedException("cannot read on deferred connection");

    public Task<SingleResult<T>> ExecuteQueryProcOneAsync<T>(string procName, Func<IResult, Task<T>> readerFunc, params SqlParameter[] parameters)
        => throw new NotSupportedException("cannot read on deferred connection");

    public void ExecuteQueryProcMultiResultSet(string procName, Action<IMultiResultSet> readerAction, params SqlParameter[] parameters)
        => throw new NotSupportedException("cannot read on deferred connection");

    public Task ExecuteQueryProcMultiResultSetAsync(string procName, Func<IAsyncMultiResultSet, Task> readerFunc, params SqlParameter[] parameters)
        => throw new NotSupportedException("cannot read on deferred connection");

    public IAsyncEnumerable<IResult> ExecuteQueryProcAsync(string procName, params SqlParameter[] parameters)
        => throw new NotSupportedException("cannot read on deferred connection");

    public IAsyncEnumerable<IResult> ExecuteQueryProcAsync(string procName, CancellationToken cancellationToken, params SqlParameter[] parameters)
        => throw new NotSupportedException("cannot read on deferred connection");

    public Task ExecuteQueryProcMultiResultSetAsync(string procName, Func<IAsyncMultiResultSet, Task> readerFunc, CancellationToken cancellationToken, params SqlParameter[] parameters)
        => throw new NotSupportedException("cannot read on deferred connection");

    public Task<List<T>> ExecuteQueryProcListAsync<T>(string procName, Func<IResult, Task<T>> readerFunc, CancellationToken cancellationToken, params SqlParameter[] parameters)
        => throw new NotSupportedException("cannot read on deferred connection");

    public Task<bool> ExecuteQueryProcOneAsync(string procName, Func<IResult, Task> readerFunc, CancellationToken cancellationToken, params SqlParameter[] parameters)
        => throw new NotSupportedException("cannot read on deferred connection");

    public Task<SingleResult<T>> ExecuteQueryProcOneAsync<T>(string procName, Func<IResult, Task<T>> readerFunc, CancellationToken cancellationToken, params SqlParameter[] parameters)
        => throw new NotSupportedException("cannot read on deferred connection");

    public void AddRollbackAction(string? desc, Action onRollback)
        => throw new NotSupportedException("deferred connection does not support rollback actions");

    public void AddAsyncRollbackAction(string? desc, Func<Task> onRollbackAsync)
        => throw new NotSupportedException("deferred connection does not support rollback actions");

    public void AddPostCommitAction(string? desc, Action postCommit)
    {
        if (postCommit.Method.IsDefined(typeof(AsyncStateMachineAttribute), false))
            throw new NotSupportedException("for async actions, use AddAsyncPostCommitAction");

        _postCommitActions.Value.Add(new PostAction { action = postCommit, Desc = desc });
    }

    public void AddAsyncPostCommitAction(string? desc, Func<Task> postCommitAsync)
    {
        _postCommitActions.Value.Add(new PostAction { actionAsync = postCommitAsync, Desc = desc });
    }
}
