using Microsoft.Extensions.Logging;
using Plinth.Common.Logging;
using Plinth.Common.Utils;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Diagnostics;

using static Plinth.Common.Logging.Scopes;

namespace Plinth.Database.MSSql.Impl;

internal partial class PlinthSqlTransaction : IDisposable, IAsyncDisposable
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    public bool IsOpen { get; private set; }

    public PlinthSqlConnection Connection { get; private set; }

    private readonly SqlTransaction _txn;
    private readonly SqlConnection _conn;
    private readonly Stopwatch _sw;

    internal SqlConnection GetSqlConnection() => _conn;
    internal SqlTransaction GetSqlTransaction() => _txn;
    
    // used by Dapper to store sub object
    internal object? ExtensionContainer { get; set; }

    public PlinthSqlTransaction(SqlConnection conn, SqlTransaction txn, int commandTimeout, bool isAsync)
    {
        _conn = conn;
        _txn = txn;
        IsOpen = true;
        Connection = new PlinthSqlConnection(this, commandTimeout, isAsync);

        LogDefines.LogBeginTransaction(log);
        _sw = Stopwatch.StartNew();
    }

    internal SqlCommand GetCommand(string commandText, CommandType type, int commandTimeout)
    {
        return new SqlCommand()
        {
            Connection = _conn,
            CommandText = commandText,
            CommandTimeout = commandTimeout,
            Transaction = _txn,
            CommandType = type
        };
    }

    public void Dispose()
    {
        if (Connection.WillBeRollingBack())
        {
            LogDefines.LogRollbackTransaction(log);
            _txn.Rollback();

            // this is technically bad practice to run an async task this way
            // but we have no choice as we are inside Dispose() which must be sync
            try
            {
                AsyncUtil.RunSync(Connection.ProcessRollbackActions);
            }
            catch (Exception e)
            {
                LogDefines.LogErrorRollbackActions(log, e);
            }
        }
        else
        {
            LogDefines.LogCommitTransaction(log);
            _txn.Commit();

            // this is technically bad practice to run an async task this way
            // but we have no choice as we are inside Dispose() which must be sync
            try
            {
                AsyncUtil.RunSync(Connection.ProcessPostCommitActions);
            }
            catch (Exception e)
            {
                LogDefines.LogErrorPostCommitActions(log, e);
            }
        }

        _sw.Stop();
        var elapsed = _sw.Elapsed;

        var meta = CreateScope(
            Kvp(LoggingConstants.Field_TimeDurationMicros, (long)(elapsed.TotalMilliseconds * 1000)),
            Kvp(LoggingConstants.Field_MessageType, "SQLTransaction")
        );

        using (log.BeginScope(meta))
        {
            LogDefines.LogEndTransaction(log, elapsed);
        }

        IsOpen = false;
        _txn.Dispose();
    }

    internal void CheckIsOpen()
    {
        if (!IsOpen)
            throw new DatabaseException("Transaction is not open!");
    }

    public async ValueTask DisposeAsync()
    {
        if (Connection.WillBeRollingBack())
        {
            LogDefines.LogRollbackTransaction(log);
            await _txn.RollbackAsync();

            try
            {
                await Connection.ProcessRollbackActions();
            }
            catch (Exception e)
            {
                LogDefines.LogErrorRollbackActions(log, e);
            }
        }
        else
        {
            LogDefines.LogCommitTransaction(log);
            await _txn.CommitAsync();

            try
            {
                await Connection.ProcessPostCommitActions();
            }
            catch (Exception e)
            {
                LogDefines.LogErrorPostCommitActions(log, e);
            }
        }

        _sw.Stop();
        var elapsed = _sw.Elapsed;

        var meta = CreateScope(
            Kvp(LoggingConstants.Field_TimeDurationMicros, (long)(elapsed.TotalMilliseconds * 1000)),
            Kvp(LoggingConstants.Field_MessageType, "SQLTransaction")
        );

        using (log.BeginScope(meta))
        {
            LogDefines.LogEndTransaction(log, elapsed);
        }

        IsOpen = false;
    }

    private static partial class LogDefines
    {
        [LoggerMessage(0, LogLevel.Debug, "END TRANSACTION Took {" + LoggingConstants.Field_TimeDuration + "}", EventName = "EndTransaction")]
        public static partial void LogEndTransaction(ILogger logger, TimeSpan timeDuration);

        [LoggerMessage(1, LogLevel.Debug, "BEGIN TRANSACTION", EventName = "BeginTransaction")]
        public static partial void LogBeginTransaction(ILogger logger);

        [LoggerMessage(2, LogLevel.Debug, "COMMIT TRANSACTION", EventName = "CommmitTransaction")]
        public static partial void LogCommitTransaction(ILogger logger);

        [LoggerMessage(3, LogLevel.Information, "ROLLBACK TRANSACTION", EventName = "RollbackTransaction")]
        public static partial void LogRollbackTransaction(ILogger logger);

        [LoggerMessage(4, LogLevel.Information, "Caught while processing rollback actions", EventName = "ErrorRollbackActions")]
        public static partial void LogErrorRollbackActions(ILogger logger, Exception? e);

        [LoggerMessage(5, LogLevel.Error, "Post Commit Action threw exception", EventName = "ErrorRollbackAsyncActions")]
        public static partial void LogErrorPostCommitActions(ILogger logger, Exception? e);
    }
}
