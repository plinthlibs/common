using Microsoft.Extensions.Logging;
using Plinth.Common.Utils;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using Plinth.Common.Logging;

using static Plinth.Common.Logging.Scopes;
using static Plinth.Database.MSSql.Impl.PlinthSqlConnectionCommon;

namespace Plinth.Database.MSSql.Impl;

internal partial class PlinthNoTxnSqlConnection : INoTxnRawSqlConnection, IDisposable, IAsyncDisposable
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private readonly SqlConnection _conn;
    private readonly Stopwatch _sw;
    private readonly Lazy<List<PostAction>> _postCloseActions = new();
    private readonly Lazy<Stack<PostAction>> _postErrorActions = new();

    private readonly bool _isAsync;
    private bool _isError;
    private Exception? _error;

    public bool HasWritten { get; private set; } = false;

    internal SqlConnection GetSqlConnection() => _conn;

    // used by Dapper to store sub object
    internal object? ExtensionContainer { get; set; }

    /// <summary>
    /// The time in seconds to wait for a command to execute before throwing an error.
    /// </summary>
    public int CommandTimeout { get; set; }

    public PlinthNoTxnSqlConnection(SqlConnection conn, int commandTimeout, bool isAsync)
    {
        _conn = conn;
        _isAsync = isAsync;
        CommandTimeout = commandTimeout;

        LogDefines.LogOpen(log);

        _sw = Stopwatch.StartNew();
    }

    public void SetError(Exception? e)
    {
        _isError = true;
        _error = e;
    }

    public void ClearError()
    {
        _isError = false;
        _error = null;
    }

    public bool IsAsync() => _isAsync;

    internal void SetWritten()
    {
        if (HasWritten)
            throw new NotSupportedException("When running without a transaction, only a single write operation is allowed");
        HasWritten = true; // TODO: can we determine if a write failed and allow retries?
    }

    #region proc

    public int ExecuteProcUnchecked(string procName, params SqlParameter[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, null, _conn, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteProc", log))
        {
            return LogRows(log, cmd.ExecuteNonQuery());
        }
    }

    public void ExecuteProc(string procName, params SqlParameter[] parameters)
    {
        if (ExecuteProcUnchecked(procName, parameters) <= 0)
            throw new DatabaseException("expected #rows > 0", DatabaseExceptionType.ExpectedRowsMismatch);
    }

    public void ExecuteProc(string procName, int expectedRows, params SqlParameter[] parameters)
    {
        int rows = ExecuteProcUnchecked(procName, parameters);
        if (rows != expectedRows)
            throw new DatabaseException($"expected {expectedRows} got {rows}", DatabaseExceptionType.ExpectedRowsMismatch);
    }

    public IEnumerable<IResult> ExecuteQueryProc(string procName, params SqlParameter[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, null, _conn, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProc", log))
        using (var rs = new ResultSet(cmd.ExecuteReader()))
        {
            foreach (var r in rs)
                yield return r;
        }
    }

    public void ExecuteQueryProcMultiResultSet(string procName, Action<IMultiResultSet> readerAction, params SqlParameter[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, null, _conn, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProcMultiResultSet", log))
        using (var mrs = new MultiResultSet(cmd.ExecuteReader()))
        {
            readerAction(mrs);
        }
    }

    public bool ExecuteQueryProcOne(string procName, Action<IResult> readerFunc, params SqlParameter[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, null, _conn, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProcOne", log))
        using (var rs = new ResultSet(cmd.ExecuteReader()))
        {
            return rs.GetOne(readerFunc);
        }
    }

    public SingleResult<T> ExecuteQueryProcOne<T>(string procName, Func<IResult, T> readerFunc, params SqlParameter[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, null, _conn, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProcOne", log))
        using (var rs = new ResultSet(cmd.ExecuteReader()))
        {
            return rs.GetOne(readerFunc);
        }
    }

    public List<T> ExecuteQueryProcList<T>(string procName, Func<IResult, T> readerFunc, params SqlParameter[] parameters)
    {
        return ExecuteQueryProc(procName, parameters).Select(readerFunc).ToList();
    }

    #endregion proc

    #region proc async

    public Task<int> ExecuteProcUncheckedAsync(string procName, params SqlParameter[] parameters)
        => ExecuteProcUncheckedAsync(procName, CancellationToken.None, parameters);

    public async Task<int> ExecuteProcUncheckedAsync(string procName, CancellationToken cancellationToken, params SqlParameter[] parameters)
    {
        SetWritten();
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, null, _conn, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteProc", log))
        {
            return LogRows(log, await cmd.ExecuteNonQueryAsync(cancellationToken));
        }
    }

    public Task ExecuteProcAsync(string procName, params SqlParameter[] parameters)
        => ExecuteProcAsync(procName, CancellationToken.None, parameters);

    public async Task ExecuteProcAsync(string procName, CancellationToken cancellationToken, params SqlParameter[] parameters)
    {
        int rows = await ExecuteProcUncheckedAsync(procName, cancellationToken, parameters);
        if (rows <= 0)
            throw new DatabaseException("expected #rows > 0", DatabaseExceptionType.ExpectedRowsMismatch);
    }

    public Task ExecuteProcAsync(string procName, int expectedRows, params SqlParameter[] parameters)
        => ExecuteProcAsync(procName, expectedRows, CancellationToken.None, parameters);

    public async Task ExecuteProcAsync(string procName, int expectedRows, CancellationToken cancellationToken, params SqlParameter[] parameters)
    {
        int rows = await ExecuteProcUncheckedAsync(procName, cancellationToken, parameters);
        if (rows != expectedRows)
            throw new DatabaseException($"expected {expectedRows} got {rows}", DatabaseExceptionType.ExpectedRowsMismatch);
    }

    public Task<bool> ExecuteQueryProcOneAsync(string procName, Func<IResult, Task> readerFunc, params SqlParameter[] parameters)
        => ExecuteQueryProcOneAsync(procName, readerFunc, CancellationToken.None, parameters);

    public async Task<bool> ExecuteQueryProcOneAsync(string procName, Func<IResult, Task> readerFunc, CancellationToken cancellationToken, params SqlParameter[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, null, _conn, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProcOneAsync", log))
        await using (var rs = new ResultSetAsync(await cmd.ExecuteReaderAsync(cancellationToken)))
        {
            return await rs.GetOneAsync(readerFunc);
        }
    }

    public Task<SingleResult<T>> ExecuteQueryProcOneAsync<T>(string procName, Func<IResult, Task<T>> readerFunc, params SqlParameter[] parameters)
        => ExecuteQueryProcOneAsync(procName, readerFunc, CancellationToken.None, parameters);

    public async Task<SingleResult<T>> ExecuteQueryProcOneAsync<T>(string procName, Func<IResult, Task<T>> readerFunc, CancellationToken cancellationToken, params SqlParameter[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, null, _conn, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProcOneAsync", log))
        await using (var rs = new ResultSetAsync(await cmd.ExecuteReaderAsync(cancellationToken)))
        {
            return await rs.GetOneAsync(readerFunc);
        }
    }

    public Task<List<T>> ExecuteQueryProcListAsync<T>(string procName, Func<IResult, Task<T>> readerFunc, params SqlParameter[] parameters)
        => ExecuteQueryProcListAsync(procName, readerFunc, CancellationToken.None, parameters);

    public async Task<List<T>> ExecuteQueryProcListAsync<T>(string procName, Func<IResult, Task<T>> readerFunc, CancellationToken cancellationToken, params SqlParameter[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, null, _conn, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProcListAsync", log))
        await using (var rs = new ResultSetAsync(await cmd.ExecuteReaderAsync(cancellationToken)))
        {
            return await rs.GetListAsync(readerFunc);
        }
    }

    public Task ExecuteQueryProcMultiResultSetAsync(string procName, Func<IAsyncMultiResultSet, Task> readerFunc, params SqlParameter[] parameters)
        => ExecuteQueryProcMultiResultSetAsync(procName, readerFunc, CancellationToken.None, parameters);

    public async Task ExecuteQueryProcMultiResultSetAsync(string procName, Func<IAsyncMultiResultSet, Task> readerFunc, CancellationToken cancellationToken, params SqlParameter[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, null, _conn, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProcMultiResultSetAsync", log))
        await using (var mrs = new MultiResultSet(await cmd.ExecuteReaderAsync(cancellationToken)))
        {
            await readerFunc(mrs);
        }
    }

    public IAsyncEnumerable<IResult> ExecuteQueryProcAsync(string procName, params SqlParameter[] parameters)
        => ExecuteQueryProcAsync(procName, CancellationToken.None, parameters);

    public async IAsyncEnumerable<IResult> ExecuteQueryProcAsync(string procName, [EnumeratorCancellation] CancellationToken cancellationToken, params SqlParameter[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, null, _conn, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProc", log))
        using (var rs = new ResultSetAsync(await cmd.ExecuteReaderAsync(cancellationToken)))
        {
            await foreach (var r in rs)
                yield return r;
        }
    }

    #endregion proc async

    #region raw

    public int ExecuteRaw(string sql, params SqlParameter[] parameters)
    {
        SetWritten();
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, null, _conn, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRaw", log))
        {
            return LogRows(log, cmd.ExecuteNonQuery());
        }
    }

    public IEnumerable<IResult> ExecuteRawQuery(string sql, params SqlParameter[] parameters)
    {
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, null, _conn, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRawQuery", log))
        using (var rs = new ResultSet(cmd.ExecuteReader()))
        {
            foreach (var r in rs)
                yield return r;
        }
    }

    public List<T> ExecuteRawQueryList<T>(string sql, Func<IResult, T> readerFunc, params SqlParameter[] parameters)
    {
        return ExecuteRawQuery(sql, parameters).Select(readerFunc).ToList();
    }

    public bool ExecuteRawQueryOne(string sql, Action<IResult> readerFunc, params SqlParameter[] parameters)
    {
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, null, _conn, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryOne", log))
        using (var rs = new ResultSet(cmd.ExecuteReader()))
        {
            return rs.GetOne(readerFunc);
        }
    }

    public SingleResult<T> ExecuteRawQueryOne<T>(string sql, Func<IResult, T> readerFunc, params SqlParameter[] parameters)
    {
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, null, _conn, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryOne", log))
        using (var rs = new ResultSet(cmd.ExecuteReader()))
        {
            return rs.GetOne(readerFunc);
        }
    }

    public void ExecuteRawQueryMultiResultSet(string sql, Action<IMultiResultSet> readerAction, params SqlParameter[] parameters)
    {
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, null, _conn, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryMultiResultSet", log))
        using (var mrs = new MultiResultSet(cmd.ExecuteReader()))
        {
            readerAction(mrs);
        }
    }

    #endregion raw

    #region raw async

    public Task<int> ExecuteRawAsync(string sql, params SqlParameter[] parameters)
        => ExecuteRawAsync(sql, CancellationToken.None, parameters);

    public async Task<int> ExecuteRawAsync(string sql, CancellationToken cancellationToken, params SqlParameter[] parameters)
    {
        SetWritten();
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, null, _conn, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRawAsync", log))
        {
            return LogRows(log, await cmd.ExecuteNonQueryAsync(cancellationToken));
        }
    }

    public Task<List<T>> ExecuteRawQueryListAsync<T>(string sql, Func<IResult, Task<T>> readerFunc, params SqlParameter[] parameters)
        => ExecuteRawQueryListAsync(sql, readerFunc, CancellationToken.None, parameters);

    public async Task<List<T>> ExecuteRawQueryListAsync<T>(string sql, Func<IResult, Task<T>> readerFunc, CancellationToken cancellationToken, params SqlParameter[] parameters)
    {
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, null, _conn, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryListAsync", log))
        await using (var rs = new ResultSetAsync(await cmd.ExecuteReaderAsync(cancellationToken)))
        {
            return await rs.GetListAsync(readerFunc);
        }
    }

    public Task<bool> ExecuteRawQueryOneAsync(string sql, Func<IResult, Task> readerFunc, params SqlParameter[] parameters)
        => ExecuteRawQueryOneAsync(sql, readerFunc, CancellationToken.None, parameters);

    public async Task<bool> ExecuteRawQueryOneAsync(string sql, Func<IResult, Task> readerFunc, CancellationToken cancellationToken, params SqlParameter[] parameters)
    {
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, null, _conn, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryOneAsync", log))
        await using (var rs = new ResultSetAsync(await cmd.ExecuteReaderAsync(cancellationToken)))
        {
            return await rs.GetOneAsync(readerFunc);
        }
    }

    public Task<SingleResult<T>> ExecuteRawQueryOneAsync<T>(string sql, Func<IResult, Task<T>> readerFunc, params SqlParameter[] parameters)
        => ExecuteRawQueryOneAsync(sql, readerFunc, CancellationToken.None, parameters);

    public async Task<SingleResult<T>> ExecuteRawQueryOneAsync<T>(string sql, Func<IResult, Task<T>> readerFunc, CancellationToken cancellationToken, params SqlParameter[] parameters)
    {
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, null, _conn, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryOneAsync", log))
        await using (var rs = new ResultSetAsync(await cmd.ExecuteReaderAsync(cancellationToken)))
        {
            return await rs.GetOneAsync(readerFunc);
        }
    }

    public Task ExecuteRawQueryMultiResultSetAsync(string sql, Func<IAsyncMultiResultSet, Task> readerFunc, params SqlParameter[] parameters)
        => ExecuteRawQueryMultiResultSetAsync(sql, readerFunc, CancellationToken.None, parameters);

    public async Task ExecuteRawQueryMultiResultSetAsync(string sql, Func<IAsyncMultiResultSet, Task> readerFunc, CancellationToken cancellationToken, params SqlParameter[] parameters)
    {
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, null, _conn, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryMultiResultSetAsync", log))
        await using (var mrs = new MultiResultSet(await cmd.ExecuteReaderAsync(cancellationToken)))
        {
            await readerFunc(mrs);
        }
    }

    #endregion raw async

    #region error

    internal class PostAction
    {
        public Action? action;
        public Func<Task>? actionAsync;
        public Action<Exception?>? errorAction;
        public Func<Exception?, Task>? errorActionAsync;
        public string? Desc;
    }

    public void AddErrorAction(string? desc, Action<Exception?> onError)
    {
        if (onError.Method.IsDefined(typeof(AsyncStateMachineAttribute), false))
            throw new NotSupportedException("for async actions, use AddAsyncErrorAction");

        _postErrorActions.Value.Push(new PostAction { errorAction = onError, Desc = desc });
    }

    public void AddAsyncErrorAction(string? desc, Func<Exception?, Task> onErrorAsync)
    {
        _postErrorActions.Value.Push(new PostAction { errorActionAsync = onErrorAsync, Desc = desc });
    }

    internal async Task ProcessErrorActions()
    {
        var stack = _postErrorActions.Value;

        while (stack.Count > 0)
        {
            var action = stack.Pop();

            LogDefines.LogErrorAction(log, action.Desc);

            if (action.errorAction != null)
            {
                try
                {
                    action.errorAction(_error);
                }
                catch (Exception e)
                {
                    LogDefines.LogErrorErrorAction(log, e);
                }
            }
            else if (action.errorActionAsync != null)
            {
                try
                {
                    await action.errorActionAsync(_error);
                }
                catch (Exception e)
                {
                    LogDefines.LogErrorErrorAsyncAction(log, e);
                }
            }
            else
                throw new InvalidOperationException("no error action");
        }
    }
    #endregion

    #region Dispose
    public void Dispose()
    {
        if (_isError)
        {
            // this is technically bad practice to run an async task this way
            // but we have no choice as we are inside Dispose() which must be sync
            try
            {
                AsyncUtil.RunSync(ProcessErrorActions);
            }
            catch (Exception e)
            {
                LogDefines.LogErrorErrorAction(log, e);
            }
        }
        else
        {
            // this is technically bad practice to run an async task this way
            // but we have no choice as we are inside Dispose() which must be sync
            try
            {
                AsyncUtil.RunSync(ProcessPostCloseActions);
            }
            catch (Exception e)
            {
                LogDefines.LogErrorPostCloseAction(log, e);
            }
        }

        _sw.Stop();
        var elapsed = _sw.Elapsed;

        var meta = CreateScope(
            Kvp(LoggingConstants.Field_TimeDurationMicros, (long)(elapsed.TotalMilliseconds * 1000)),
            Kvp(LoggingConstants.Field_MessageType, "SQLConnection")
        );

        using (log.BeginScope(meta))
        {
            LogDefines.LogClose(log, elapsed);
        }

        _conn.Close();
    }

    public async ValueTask DisposeAsync()
    {
        if (_isError)
        {
            try
            {
                await ProcessErrorActions();
            }
            catch (Exception e)
            {
                LogDefines.LogErrorErrorAction(log, e);
            }
        }
        else
        {
            try
            {
                await ProcessPostCloseActions();
            }
            catch (Exception e)
            {
                LogDefines.LogErrorPostCloseAction(log, e);
            }
        }

        _sw.Stop();
        var elapsed = _sw.Elapsed;

        var meta = CreateScope(
            Kvp(LoggingConstants.Field_OperationResult, _isError ? "success" : "error"),
            Kvp(LoggingConstants.Field_TimeDurationMicros, (long)(elapsed.TotalMilliseconds * 1000)),
            Kvp(LoggingConstants.Field_MessageType, "SQLConnection")
        );

        using (log.BeginScope(meta))
        {
            LogDefines.LogClose(log, elapsed);
        }

        await _conn.CloseAsync();
    }
    #endregion

    #region post close
    public void AddPostCommitAction(string? desc, Action postCommit)
        => throw new NotSupportedException("A connection without a transaction cannot execute post commit actions, use ExecuteTxnAsync() or AddPostCommitAction() instead");

    public void AddAsyncPostCommitAction(string? desc, Func<Task> postCommitAsync)
        => throw new NotSupportedException("A connection without a transaction cannot execute post commit actions, use ExecuteTxnAsync() or AddAsyncPostCommitAction() instead");

    public void AddPostCloseAction(string? desc, Action postClose)
    {
        if (postClose.Method.IsDefined(typeof(AsyncStateMachineAttribute), false))
            throw new NotSupportedException("for async actions, use AddAsyncPostCloseAction");

        _postCloseActions.Value.Add(new PostAction { action = postClose, Desc = desc });
    }

    public void AddAsyncPostCloseAction(string? desc, Func<Task> postCloseAsync)
    {
        _postCloseActions.Value.Add(new PostAction { actionAsync = postCloseAsync, Desc = desc });
    }

    internal async Task ProcessPostCloseActions()
    {
        var list = _postCloseActions.Value;

        foreach (var action in list)
        {
            LogDefines.LogPostCloseAction(log, action.Desc);

            if (action.action != null)
            {
                try
                {
                    action.action();
                }
                catch (Exception e)
                {
                    LogDefines.LogErrorPostCloseAction(log, e);
                }
            }
            else if (action.actionAsync != null)
            {
                try
                {
                    await action.actionAsync();
                }
                catch (Exception e)
                {
                    LogDefines.LogErrorPostCloseAsyncAction(log, e);
                }
            }
            else
                throw new InvalidOperationException("no Post Close action");
        }
    }

    #endregion

    private static partial class LogDefines
    {
        [LoggerMessage(1, LogLevel.Information, "Processing Error Action: {ActionDesc}", EventName = "ErrorAction")]
        public static partial void LogErrorAction(ILogger logger, string? actionDesc);

        [LoggerMessage(2, LogLevel.Error, "Error Action threw exception", EventName = "ErrorErrorAction")]
        public static partial void LogErrorErrorAction(ILogger logger, Exception? e);

        [LoggerMessage(3, LogLevel.Error, "Error (async) Action threw exception", EventName = "ErrorErrorAsyncAction")]
        public static partial void LogErrorErrorAsyncAction(ILogger logger, Exception? e);

        [LoggerMessage(4, LogLevel.Information, "Processing PostClose Action: {ActionDesc}", EventName = "PostCloseAction")]
        public static partial void LogPostCloseAction(ILogger logger, string? actionDesc);

        [LoggerMessage(5, LogLevel.Error, "PostClose Action threw exception", EventName = "ErrorPostCloseAction")]
        public static partial void LogErrorPostCloseAction(ILogger logger, Exception? e);

        [LoggerMessage(6, LogLevel.Error, "PostClose (async) Action threw exception", EventName = "ErrorPostCloseAsyncAction")]
        public static partial void LogErrorPostCloseAsyncAction(ILogger logger, Exception? e);

        [LoggerMessage(7, LogLevel.Debug, "CLOSE CONNECTION Took {" + LoggingConstants.Field_TimeDuration + "}", EventName = "CloseConnection")]
        public static partial void LogClose(ILogger logger, TimeSpan timeDuration);

        [LoggerMessage(8, LogLevel.Debug, "OPEN CONNECTION", EventName = "OpenConnection")]
        public static partial void LogOpen(ILogger logger);
    }
}
