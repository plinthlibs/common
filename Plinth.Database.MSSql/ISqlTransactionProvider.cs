using System.Data;

namespace Plinth.Database.MSSql;

/// <summary>
/// Provides the boundaries of a transaction
/// </summary>
/// <remarks>
/// NOTE: Any Func or Action that is passed may be recalled if a transient error occurs (such as Deadlock)
///       Be cautious about doing anything that cannot be undone inside a DB transaction.
///       This can be disabled when registering the transaction factory
/// </remarks>
public interface ISqlTransactionProvider
{
    /// <summary>
    /// Get the connection string used by this transaction provider
    /// </summary>
    string ConnectionString { get; }

#region sync
    /// <summary>
    /// Execute the Action within a new transaction
    /// </summary>
    /// <param name="action">action to be called with connection</param>
    /// <param name="commandTimeout">(optional) override the default timeout for sql commands (seconds)</param>
    /// <param name="isolationLevel">(optional) override the default isolation level</param>
    void ExecuteTxn(Action<ISqlConnection> action, int? commandTimeout = null, IsolationLevel? isolationLevel = null);

    /// <summary>
    /// Execute the function within a new transaction
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="func">action to be called with connection</param>
    /// <param name="commandTimeout">(optional) override the default timeout for sql commands (seconds)</param>
    /// <param name="isolationLevel">(optional) override the default isolation level</param>
    /// <returns>the value returned from func</returns>
    T ExecuteTxn<T>(Func<ISqlConnection, T> func, int? commandTimeout = null, IsolationLevel? isolationLevel = null);

    /// <summary>
    /// Execute the Action without a transaction
    /// </summary>
    /// <param name="action">action to be called with connection</param>
    /// <param name="commandTimeout">(optional) override the default timeout for sql commands (seconds)</param>
    void ExecuteWithoutTxn(Action<INoTxnSqlConnection> action, int? commandTimeout = null);

    /// <summary>
    /// Execute the function without a transaction
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="func">action to be called with connection</param>
    /// <param name="commandTimeout">(optional) override the default timeout for sql commands (seconds)</param>
    /// <returns>the value returned from func</returns>
    /// <remarks>
    /// WARNING: This should be used only when operating without a transaction is safe.
    /// This will fail if you attempt to write more than once
    /// </remarks>
    T ExecuteWithoutTxn<T>(Func<INoTxnSqlConnection, T> func, int? commandTimeout = null);

    /// <summary>
    /// Execute the Action within a new transaction, suitable for raw sql queries
    /// </summary>
    /// <param name="action">action to be called with connection</param>
    /// <param name="commandTimeout">(optional) override the default timeout for sql commands (seconds)</param>
    /// <param name="isolationLevel">(optional) override the default isolation level</param>
    /// <remarks>
    /// WARNING: This should be used only when operating without a transaction is safe.
    /// This will fail if you attempt to write more than once
    /// A connection without a transaction does not support commit/rollback actions or retries after writing
    /// </remarks>
    void ExecuteRawTxn(Action<IRawSqlConnection> action, int? commandTimeout = null, IsolationLevel? isolationLevel = null);

    /// <summary>
    /// Execute the function within a new transaction, suitable for raw sql queries
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="func">action to be called with connection</param>
    /// <param name="commandTimeout">(optional) override the default timeout for sql commands (seconds)</param>
    /// <param name="isolationLevel">(optional) override the default isolation level</param>
    /// <returns>the value returned from func</returns>
    T ExecuteRawTxn<T>(Func<IRawSqlConnection, T> func, int? commandTimeout = null, IsolationLevel? isolationLevel = null);

    /// <summary>
    /// Execute the action without a transaction, suitable for raw sql queries
    /// </summary>
    /// <param name="action">action to be called with connection</param>
    /// <param name="commandTimeout">(optional) override the default timeout for sql commands (seconds)</param>
    /// <remarks>
    /// WARNING: This should be used only when operating without a transaction is safe.
    /// This will fail if you attempt to write more than once
    /// A connection without a transaction does not support commit/rollback actions or retries after writing
    /// </remarks>
    void ExecuteRawWithoutTxn(Action<INoTxnRawSqlConnection> action, int? commandTimeout = null);

    /// <summary>
    /// Execute the function without a transaction, suitable for raw sql queries
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="func">action to be called with connection</param>
    /// <param name="commandTimeout">(optional) override the default timeout for sql commands (seconds)</param>
    /// <returns>the value returned from func</returns>
    T ExecuteRawWithoutTxn<T>(Func<INoTxnRawSqlConnection, T> func, int? commandTimeout = null);
#endregion

    /// <summary>
    /// Execute the Action within a new transaction, async
    /// </summary>
    /// <param name="action">action to be called with connection, should be async</param>
    /// <param name="commandTimeout">(optional) override the default timeout for sql commands (seconds)</param>
    /// <param name="isolationLevel">(optional) override the default isolation level</param>
    Task ExecuteTxnAsync(Func<ISqlConnection, Task> action, int? commandTimeout = null, IsolationLevel? isolationLevel = null);

    /// <inheritdoc cref="ExecuteTxnAsync(Func{ISqlConnection, Task}, int?, IsolationLevel?)"/>
    Task ExecuteTxnAsync(Func<ISqlConnection, Task> action, CancellationToken cancellationToken, int? commandTimeout = null, IsolationLevel? isolationLevel = null);

    /// <summary>
    /// Execute the Action without a transaction, async
    /// </summary>
    /// <param name="action">action to be called with connection, should be async</param>
    /// <param name="commandTimeout">(optional) override the default timeout for sql commands (seconds)</param>
    /// <remarks>
    /// WARNING: This should be used only when operating without a transaction is safe.
    /// This will fail if you attempt to write more than once
    /// A connection without a transaction does not support commit/rollback actions or retries after writing
    /// </remarks>
    Task ExecuteWithoutTxnAsync(Func<INoTxnSqlConnection, Task> action, int? commandTimeout = null);

    /// <inheritdoc cref="ExecuteWithoutTxnAsync(Func{INoTxnSqlConnection, Task}, int?)"/>
    Task ExecuteWithoutTxnAsync(Func<INoTxnSqlConnection, Task> action, CancellationToken cancellationToken, int? commandTimeout = null);

    /// <summary>
    /// Execute the function within a new transaction, async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="func">action to be called with connection, should be async</param>
    /// <param name="commandTimeout">(optional) override the default timeout for sql commands (seconds)</param>
    /// <param name="isolationLevel">(optional) override the default isolation level</param>
    /// <returns>the T value returned from func</returns>
    Task<T> ExecuteTxnAsync<T>(Func<ISqlConnection, Task<T>> func, int? commandTimeout = null, IsolationLevel? isolationLevel = null);

    /// <inheritdoc cref="ExecuteTxnAsync{T}(Func{ISqlConnection, Task{T}}, int?, IsolationLevel?)"/>
    Task<T> ExecuteTxnAsync<T>(Func<ISqlConnection, Task<T>> func, CancellationToken cancellationToken, int? commandTimeout = null, IsolationLevel? isolationLevel = null);

    /// <summary>
    /// Execute the function without a transaction, async
    /// </summary>
    /// <param name="action">action to be called with connection, should be async</param>
    /// <param name="commandTimeout">(optional) override the default timeout for sql commands (seconds)</param>
    /// <remarks>
    /// WARNING: This should be used only when operating without a transaction is safe.
    /// This will fail if you attempt to write more than once
    /// A connection without a transaction does not support commit/rollback actions or retries after writing
    /// </remarks>
    /// <returns>the T value returned from func</returns>
    Task<T> ExecuteWithoutTxnAsync<T>(Func<INoTxnSqlConnection, Task<T>> action, int? commandTimeout = null);

    /// <inheritdoc cref="ExecuteWithoutTxnAsync(Func{INoTxnSqlConnection, Task}, int?)"/>
    Task<T> ExecuteWithoutTxnAsync<T>(Func<INoTxnSqlConnection, Task<T>> action, CancellationToken cancellationToken, int? commandTimeout = null);

    /// <summary>
    /// Execute the Action within a new transaction, suitable for raw sql queries, async
    /// </summary>
    /// <param name="action">action to be called with connection, should be async</param>
    /// <param name="commandTimeout">(optional) override the default timeout for sql commands (seconds)</param>
    /// <param name="isolationLevel">(optional) override the default isolation level</param>
    Task ExecuteRawTxnAsync(Func<IRawSqlConnection, Task> action, int? commandTimeout = null, IsolationLevel? isolationLevel = null);

    /// <inheritdoc cref="ExecuteRawTxnAsync(Func{IRawSqlConnection, Task}, int?, IsolationLevel?)"/>
    Task ExecuteRawTxnAsync(Func<IRawSqlConnection, Task> action, CancellationToken cancellationToken, int? commandTimeout = null, IsolationLevel? isolationLevel = null);

    /// <summary>
    /// Execute the Action without a transaction, suitable for raw sql queries, async
    /// </summary>
    /// <param name="action">action to be called with connection, should be async</param>
    /// <param name="commandTimeout">(optional) override the default timeout for sql commands (seconds)</param>
    /// <remarks>
    /// WARNING: This should be used only when operating without a transaction is safe.
    /// This will fail if you attempt to write more than once
    /// A connection without a transaction does not support commit/rollback actions or retries after writing
    /// </remarks>
    Task ExecuteRawWithoutTxnAsync(Func<INoTxnRawSqlConnection, Task> action, int? commandTimeout = null);

    /// <inheritdoc cref="ExecuteWithoutTxnAsync(Func{INoTxnSqlConnection, Task}, int?)"/>
    Task ExecuteRawWithoutTxnAsync(Func<INoTxnRawSqlConnection, Task> action, CancellationToken cancellationToken, int? commandTimeout = null);

    /// <summary>
    /// Execute the function within a new transaction, suitable for raw sql queries, async
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="func">action to be called with connection, should be async</param>
    /// <param name="commandTimeout">(optional) override the default timeout for sql commands (seconds)</param>
    /// <param name="isolationLevel">(optional) override the default isolation level</param>
    /// <returns>the T value returned from func</returns>
    Task<T> ExecuteRawTxnAsync<T>(Func<IRawSqlConnection, Task<T>> func, int? commandTimeout = null, IsolationLevel? isolationLevel = null);

    /// <inheritdoc cref="ExecuteRawTxnAsync{T}(Func{IRawSqlConnection, Task{T}}, int?, IsolationLevel?)"/>
    Task<T> ExecuteRawTxnAsync<T>(Func<IRawSqlConnection, Task<T>> func, CancellationToken cancellationToken, int? commandTimeout = null, IsolationLevel? isolationLevel = null);

    /// <summary>
    /// Execute the function without a transaction, suitable for raw sql queries, async
    /// </summary>
    /// <param name="action">action to be called with connection, should be async</param>
    /// <param name="commandTimeout">(optional) override the default timeout for sql commands (seconds)</param>
    /// <remarks>
    /// WARNING: This should be used only when operating without a transaction is safe.
    /// This will fail if you attempt to write more than once.
    /// A connection without a transaction does not support commit/rollback actions or retries after writing
    /// </remarks>
    /// <returns>the T value returned from func</returns>
    Task<T> ExecuteRawWithoutTxnAsync<T>(Func<INoTxnRawSqlConnection, Task<T>> action, int? commandTimeout = null);

    /// <inheritdoc cref="ExecuteWithoutTxnAsync(Func{INoTxnSqlConnection, Task}, int?)"/>
    Task<T> ExecuteRawWithoutTxnAsync<T>(Func<INoTxnRawSqlConnection, Task<T>> action, CancellationToken cancellationToken, int? commandTimeout = null);

    /// <summary>
    /// Get a deferred sql connection
    /// </summary>
    /// <returns></returns>
    IDeferredSqlConnection GetDeferred();

    /// <summary>
    /// Shortcut method to execute all the deferred calls in a deferred connection
    /// </summary>
    /// <param name="deferredConn"></param>
    /// <param name="commandTimeout">(optional) override the default timeout for sql commands (seconds)</param>
    /// <param name="isolationLevel">(optional) override the default isolation level</param>
    void ExecuteDeferred(IDeferredSqlConnection deferredConn, int? commandTimeout = null, IsolationLevel? isolationLevel = null);

    /// <summary>
    /// Shortcut method to execute all the deferred calls in a deferred connection, async
    /// </summary>
    /// <param name="deferredConn"></param>
    /// <param name="commandTimeout"></param>
    /// <param name="isolationLevel">(optional) override the default isolation level</param>
    Task ExecuteDeferredAsync(IDeferredSqlConnection deferredConn, int? commandTimeout = null, IsolationLevel? isolationLevel = null);

    /// <inheritdoc cref="ExecuteDeferredAsync(IDeferredSqlConnection, int?, IsolationLevel?)"/>
    Task ExecuteDeferredAsync(IDeferredSqlConnection deferredConn, CancellationToken cancellationToken, int? commandTimeout = null, IsolationLevel? isolationLevel = null);

    #region protections for async actions
    /// <summary>
    /// Do Not Use, Sync function with async-like action doesn't work like you think it would
    /// </summary>
    [Obsolete("use ExecuteTxnAsync", true)]
    Task ExecuteTxn(Func<ISqlConnection, Task> action, int? commandTimeout = null, IsolationLevel? isolationLevel = null);

    /// <summary>
    /// Do Not Use, Sync function with async-like action doesn't work like you think it would
    /// </summary>
    [Obsolete("use ExecuteTxnAsync", true)]
    Task<T> ExecuteTxn<T>(Func<ISqlConnection, Task<T>> func, int? commandTimeout = null, IsolationLevel? isolationLevel = null);

    /// <summary>
    /// Do Not Use, Sync function with async-like action doesn't work like you think it would
    /// </summary>
    [Obsolete("use ExecuteWithoutTxnAsync", true)]
    Task ExecuteWithoutTxn(Func<INoTxnSqlConnection, Task> action, int? commandTimeout = null);

    /// <summary>
    /// Do Not Use, Sync function with async-like action doesn't work like you think it would
    /// </summary>
    [Obsolete("use ExecuteWithoutTxnAsync", true)]
    Task<T> ExecuteWithoutTxn<T>(Func<INoTxnSqlConnection, Task<T>> func, int? commandTimeout = null);

    /// <summary>
    /// Do Not Use, Sync function with async-like action doesn't work like you think it would
    /// </summary>
    [Obsolete("use ExecuteRawTxnAsync", true)]
    Task ExecuteRawTxn(Func<IRawSqlConnection, Task> action, int? commandTimeout = null, IsolationLevel? isolationLevel = null);

    /// <summary>
    /// Do Not Use, Sync function with async-like action doesn't work like you think it would
    /// </summary>
    [Obsolete("use ExecuteRawTxnAsync", true)]
    Task<T> ExecuteRawTxn<T>(Func<IRawSqlConnection, Task<T>> func, int? commandTimeout = null, IsolationLevel? isolationLevel = null);
    #endregion
}
