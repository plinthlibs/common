using Dapper;

namespace Plinth.Database.Dapper.PgSql.Impl;

class DapperMultiResultSet(CommandDefinition command, Npgsql.NpgsqlConnection conn, List<string> names) : IDapperMultiResultSetAsync, IDapperMultiResultSet
{
    private int _row = 0;

    public int ResultSetCount => names.Count;

    private void Check()
    {
        if (_row == names.Count)
            throw new InvalidOperationException($"Only {names.Count} result sets available");
    }

    private string BuildFetchSql() => $"FETCH ALL FROM \"{names[_row++]}\";";

    public async Task<IEnumerable<T>> GetListAsync<T>(CancellationToken cancellationToken)
    {
        Check();
        var r = await conn.QueryAsync<T>(BuildFetchSql(), transaction: command.Transaction, commandTimeout: command.CommandTimeout);
        return DapperConnection.LogRows(r, affected: false);
    }

    public async Task<T?> GetOneAsync<T>(CancellationToken cancellationToken)
    {
        Check();
        var r = await conn.QueryFirstAsync<T>(BuildFetchSql(), transaction: command.Transaction, commandTimeout: command.CommandTimeout);
        return new DapperConnection.LogRowsSingle<T>(r, affected: false).Value;
    }

    public IEnumerable<T> GetList<T>()
    {
        Check();
        var r = conn.Query<T>(BuildFetchSql(), transaction: command.Transaction, commandTimeout: command.CommandTimeout);
        return DapperConnection.LogRows(r, affected: false);
    }

    public T? GetOne<T>()
    {
        Check();
        var r = conn.QueryFirst<T>(BuildFetchSql(), transaction: command.Transaction, commandTimeout: command.CommandTimeout);
        return new DapperConnection.LogRowsSingle<T>(r, affected: false).Value;
    }
}
