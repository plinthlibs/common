namespace Plinth.Database.Dapper.PgSql;

/// <summary>
/// Dapper specific sql connection
/// </summary>
public interface IRawSqlDapperConnection : ISqlDapperConnection
{
    #region Execute
    /// <summary>
    /// Execute a raw SQL command
    /// </summary>
    /// <param name="sql">The SQL query text</param>
    /// <param name="param"></param>
    /// <returns>the row count of modified rows</returns>
    int ExecuteRaw(string sql, object? param = null);
    #endregion

    #region Raw Query
    /// <summary>
    /// Execute a raw SQL query that returns a list of results
    /// </summary>
    /// <typeparam name="T">type of the objects in the list</typeparam>
    /// <param name="sql">The SQL query text</param>
    /// <param name="param"></param>
    /// <returns>An IEnumerable of objects, always non-null</returns>
    IEnumerable<T> ExecuteRawQueryList<T>(string sql, object? param = null);

    /// <summary>
    /// Execute a raw SQL query that returns a single row
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="param"></param>
    /// <returns>value if a row was found, default(T) otherwise</returns>
    T? ExecuteRawQueryOne<T>(string sql, object? param = null);
    #endregion

    #region Execute Async
    /// <summary>
    /// Execute a raw SQL command, async
    /// </summary>
    /// <param name="sql">The SQL query text</param>
    /// <param name="param"></param>
    /// <param name="cancellationToken"></param>
    /// <returns>the row count of modified rows</returns>
    Task<int> ExecuteRawAsync(string sql, object? param = null, CancellationToken cancellationToken = default);
    #endregion

    #region Raw Query Async
    /// <summary>
    /// Execute a raw SQL query, async
    /// </summary>
    /// <param name="sql">The SQL query text</param>
    /// <param name="param"></param>
    /// <param name="cancellationToken"></param>
    /// <returns>An IEnumerable that can be interated on to get each returned row</returns>
    Task<IEnumerable<T>> ExecuteRawQueryListAsync<T>(string sql, object? param = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Execute a raw SQL query that returns a single row, async
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="param"></param>
    /// <param name="cancellationToken"></param>
    /// <returns>value if a row was found, default(T) otherwise</returns>
    Task<T?> ExecuteRawQueryOneAsync<T>(string sql, object? param = null, CancellationToken cancellationToken = default);
    #endregion
}
