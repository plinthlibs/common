using Plinth.Database.Dapper.PgSql.Impl;
using Plinth.Database.PgSql;
using Plinth.Database.PgSql.Impl;

namespace Plinth.Database.Dapper.PgSql;

/// <summary>
/// Extensions for Dapper on ISqlConnection
/// </summary>
public static class ISqlDapperExtensions
{
    /// <summary>
    /// Use Dapper
    /// </summary>
    public static ISqlDapperConnection Dapper(this ISqlConnection conn)
    {
        var plinthTxn = ((PlinthSqlConnection)conn).GetPlinthSqlTransaction();

        plinthTxn.ExtensionContainer ??= new DapperConnection(plinthTxn);

        return (DapperConnection)plinthTxn.ExtensionContainer;
    }

    /// <summary>
    /// Use Dapper for Raw SQL
    /// </summary>
    public static IRawSqlDapperConnection Dapper(this IRawSqlConnection conn)
        => (IRawSqlDapperConnection)Dapper((ISqlConnection)conn);

    /// <summary>
    /// Use Dapper without a transaction
    /// </summary>
    public static ISqlDapperConnection Dapper(this INoTxnSqlConnection conn)
    {
        var plinthConn = (PlinthNoTxnSqlConnection)conn;

        plinthConn.ExtensionContainer ??= new DapperConnection(plinthConn);

        return (DapperConnection)plinthConn.ExtensionContainer;
    }

    /// <summary>
    /// Use Dapper for Raw SQL without a transaction
    /// </summary>
    public static IRawSqlDapperConnection Dapper(this INoTxnRawSqlConnection conn)
        => (IRawSqlDapperConnection)Dapper((INoTxnSqlConnection)conn);
}
