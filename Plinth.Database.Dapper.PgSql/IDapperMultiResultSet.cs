namespace Plinth.Database.Dapper.PgSql;

/// <summary>
/// Interface for extracting data from a multi result set
/// </summary>
public interface IDapperMultiResultSetAsync
{
    /// <summary>
    /// Read the next result set as a list
    /// </summary>
    /// <typeparam name="T">Type of the objects in the result</typeparam>
    /// <returns>IEnumerable of objects, be sure to materialize</returns>
    public Task<IEnumerable<T>> GetListAsync<T>(CancellationToken cancellationToken = default);

    /// <summary>
    /// Read the first row of the next result set
    /// </summary>
    /// <typeparam name="T">Type of the objects in the result</typeparam>
    /// <returns>The first entity from the result</returns>
    Task<T?> GetOneAsync<T>(CancellationToken cancellationToken = default);

    /// <summary>
    /// The number of result sets available
    /// </summary>
    public int ResultSetCount { get; }
}

/// <summary>
/// Interface for extracting data from a multi result set
/// </summary>
public interface IDapperMultiResultSet
{
    /// <inheritdoc cref="IDapperMultiResultSetAsync.GetListAsync{T}"/>
    public IEnumerable<T> GetList<T>();

    /// <inheritdoc cref="IDapperMultiResultSetAsync.GetOneAsync{T}"/>
    public T? GetOne<T>();

    /// <summary>
    /// The number of result sets available
    /// </summary>
    public int ResultSetCount { get; }
}
