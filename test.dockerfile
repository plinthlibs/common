FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build-env

RUN dotnet --info

COPY *.sln ./
COPY **/*.csproj ./
COPY Tests/**/*.csproj ./
RUN dotnet sln list | \
      tail -n +3 | \
      xargs -I {} sh -c \
        'target="{}"; dir="${target%/*}"; file="${target##*/}"; mkdir -p -- "$dir"; mv -- "$file" "$target"'
COPY Directory.* ./
COPY Tests/Directory.* ./Tests
RUN dotnet sln remove Tests/Tests.Plinth.WindowsService.Core/Tests.Plinth.WindowsService.Core.csproj
RUN dotnet sln remove Tests/Tests.Plinth.WindowsService.AspNetCore/Tests.Plinth.WindowsService.AspNetCore.csproj
RUN dotnet sln remove Tests/Tests.Plinth.BenchMark/Tests.Plinth.BenchMark.csproj
RUN dotnet restore

COPY . ./

# install older runtimes for running tests
RUN wget https://packages.microsoft.com/config/debian/11/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
RUN dpkg -i packages-microsoft-prod.deb
RUN rm packages-microsoft-prod.deb
RUN apt-get update
RUN apt-get install -y apt-transport-https
RUN apt-get update
RUN apt-get install -y aspnetcore-runtime-6.0 aspnetcore-runtime-7.0
RUN dotnet --info

# cache bust
ADD http://worldclockapi.com/api/json/utc/now /etc/builddate

ARG test_arg1
ARG test_arg2

RUN dotnet test --no-restore $test_arg1 $test_arg2 || true
RUN echo $?

