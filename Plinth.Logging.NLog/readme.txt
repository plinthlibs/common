﻿----------------------------------------------------
| Plinth.Logging.NLog Install instructions |
----------------------------------------------------

===============================================================================================================

1.  - Create a new file called "NLog.config" in your root.  
    - Place the XML below inside this file.
	- Right Click - Properties - Copy to Output Directory - Set to "Copy If Newer"

-- {Project}.csproj
  <ItemGroup>
    <None Update="NLog.config">
      <CopyToOutputDirectory>PreserveNewest</CopyToOutputDirectory>
    </None>
  </ItemGroup>

-- NLog.config
<?xml version="1.0" encoding="utf-8"?>
<nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.nlog-project.org/schemas/NLog.xsd NLog.xsd" autoReload="true" throwExceptions="false" internalLogLevel="Off"
  internalLogFile="Logs/nlog-internal.log">

  <!-- component info -->
  <variable name="product" value="PRODUCT" />
  <variable name="component" value="COMPONENT" />

  <!-- files -->
  <!--
  <variable name="file.logdirectory" value="${basedir}/Logs" />  <!-- if using a drive letter, escape the colon: e.g.  'D\:/logs' -->
  -->

  <!-- Logstash -->
  <!--
  <variable name="logstash.uri" value="https://logstash.url" />
  <variable name="logstash.requireAuth" value="false" />
  <variable name="logstash.username" value="" />
  <variable name="logstash.password" value="" />
  <variable name="logstash.sendBulk" value="true" />
  -->

  <!-- Elasticsearch -->
  <!--
  <variable name="elasticsearch.uri" value="https://elasticsearch.url" />
  <variable name="elasticsearch.requireAuth" value="false" />
  <variable name="elasticsearch.username" value="" />
  <variable name="elasticsearch.password" value="" />
  <variable name="elasticsearch.pipeline" value="" />
  <variable name="elasticsearch.index" value="" />
  <variable name="elasticsearch.headerContentType" value="" />
  <variable name="elasticsearch.sendBulk" value="true" />
  -->

  <!-- Misc -->
  <variable name="exceptionRootDirPrefixes" value="Plinth,YourNamespace" />

  <include file="${basedir}/NLog.Plinth.config" ignoreErrors="true" />

  <rules>
    <!-- create rules here
    https://github.com/nlog/NLog/wiki/Configuration-file#rules

    targets from NLog.Plinth.config
      - elasticsearchAsync : direct elasticsearch streaming
      - logstashAsync      : direct logstash streaming
      - fileAsync          : human readable local file in ~/Logs
      - jsonAsync          : JSON file with all attributes in ~/Logs
      - debuggerAsync      : Debug Output logger
      - consoleAsync       : Console logger
      - colorConsoleAsync  : Console logger with color
    -->

    <!-- text (human readable) and json outputs (goes to Logs directory) -->
    <!-- <logger name="*" minlevel="Debug" writeTo="fileAsync,jsonAsync" /> -->

    <!-- text (human readable) logs to console with colors -->
    <!-- <logger name="*" minlevel="Info" writeTo="colorConsoleAsync" /> -->

    <!-- direct stream to logstash -->
    <!-- <logger name="*" minlevel="Debug" writeTo="logstashAsync" /> -->

    <!-- direct stream to elasticsearch -->
    <!-- <logger name="*" minlevel="Debug" writeTo="elasticsearchAsync" /> -->

  </rules>

</nlog>


===============================================================================================================

2.  NLog.config configurations
    - Uncomment one of the logger lines unside the rules section
    - set environment to something like "local", "dev", "prod", etc
    - set product to your product name
    - set component to the component name (like core, web, reports, etc)
    - set logstash.uri if using logstash
    - set elasticsearch.uri if using elasticsearch directly
    - set exceptionRootDirPrefixes to the beginning of the root path of your component (for filtering filenames in stack traces)

===============================================================================================================

3a.  Use this code in Program.cs

    public static class Program
    {
        public static void Main(string[] args)
        {
            var log = StaticLogManagerSetup.BasicNLogSetup();

            log.Debug("startup!");


3b.  Add this to your host setup

        webBuilder.ConfigureLogging(builder => builder.AddNLog());

        OR

        webBuilder.ConfigureServices(services => services.AddNLog());


3b.  Add this code to your classes

    using Microsoft.Extensions.Logging;

       private static readonly ILogger log = StaticLogManager.GetLogger();


    OR inject via DI

        public MyClass(ILogger<MyClass> logger)

===============================================================================================================


4. Configuration

    By default, "Microsoft.*" and "System.*" will be set to Warn and above only.
    To turn those on, add rules in NLog.config like this, which will enable Info and above

	    <logger name="Microsoft.*" maxLevel="Debug" final="true" />


    NOTE: When using NLog, appsettings.json / "Logging" has no effect, it is all configured through NLog.config

===============================================================================================================


5. Overrides

-- use this to override file archiving parameters in NLog.Plinth.config

    - Create a new file called "NLog.Plinth.Overrides.config" in your root.  
    - Place the XML below inside this file.
	- Right Click - Properties - Copy to Output Directory - Set to "Copy If Newer"

-- {Project}.csproj
  <ItemGroup>
    <None Update="NLog.Plinth.Overrides.config">
      <CopyToOutputDirectory>PreserveNewest</CopyToOutputDirectory>
    </None>
  </ItemGroup>

-- NLog.Plinth.Overrides.config
<?xml version="1.0" encoding="utf-8"?>
<nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.nlog-project.org/schemas/NLog.xsd NLog.xsd" autoReload="true" throwExceptions="false" internalLogLevel="Off"
  internalLogFile="Logs\nlog-internal.log">
  
  <variable name="file.archiveAboveSize" value="10485760"/>
  <variable name="file.maxArchiveFiles" value="10"/>
  <variable name="file.enableCompression" value="true"/>

</nlog> 


-- to override the file name for this config (for different environments, etc, add this above the include for NLog.Plinth.config)

  <variable name="plinthOverrideConfig" value="NLog.Plinth.Overrides.PROD.config" />