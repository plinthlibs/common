using NLog;
using NLog.Common;
using NLog.Config;
using NLog.Layouts;
using NLog.Targets;
using Plinth.Common.Extensions;
using Plinth.Serialization;
using System.Net.Http.Headers;
using System.Text;

namespace Plinth.Logging.NLog.Targets;

/// <summary>
/// NLog target that streams to Elasticsearch directly via http
/// </summary>
[Target("Elasticsearch")]
public class ElasticsearchTarget : TargetWithLayout
{
    /// <summary>
    /// Field used to configure Elasticsearch target
    /// </summary>
    [NLogConfigurationItem()]
    public class Field
    {
        /// <summary>
        /// Name of the field
        /// </summary>
        [RequiredParameter]
        public string Name { get; set; } = null!;

        /// <summary>
        /// Layout to use for the field
        /// </summary>
        [RequiredParameter]
        public Layout Layout { get; set; } = null!;

        /// <summary>
        /// Layout type for the field
        /// </summary>
        public Type LayoutType { get; set; } = typeof(string);

        /// <summary>
        /// Set to true if field is a Json encoded string
        /// </summary>
        public bool DecodeJsonString { get; set; }

        /// <summary>
        /// ToString to represent field configuration
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"Name: {Name}, Layout: {Layout}, LayoutType: {LayoutType}";
        }
    }

    private static readonly ThreadLocal<HttpClient> _client = new(() => GetNewHttpClient());

    private HashSet<string> _excludedProperties =
    [
        "CallerMemberName",
        "CallerFilePath",
        "CallerLineNumber",
        "ThreadId"
    ];

    private const int _AverageEventContentLength = 512;

    /// <summary>
    /// Gets or sets the base Elasticsearch URI
    /// </summary>
    public string? BaseUri { get; set; }

    /// <summary>
    /// The Elasticsearch index
    /// </summary>
    [RequiredParameter]
    public string Index { get; set; } = null!;

    /// <summary>
    /// Elasticsearch ingest pipeline
    /// </summary>
    public string? Pipeline { get; set; }

    /// <summary>
    /// Gets or sets the content type on the header for the request to Elasticsearch
    /// </summary>
    public string? ContentType { get; set; }

    /// <summary>
    /// Set it to true if Elasticsearch uses BasicAuth
    /// </summary>
    public string? RequireAuth { get; set; }
    private bool GetRequireAuth() => RequireAuth.ToBool();

    /// <summary>
    /// Username for basic auth
    /// </summary>
    public string? Username { get; set; }

    /// <summary>
    /// Password for basic auth
    /// </summary>
    public string? Password { get; set; }

    /// <summary>
    /// Gets or sets whether to include all properties of the log event in the document
    /// </summary>
    public string? IncludeAllProperties { get; set; }
    private bool GetIncludeProperties() => IncludeAllProperties.ToBool();

    /// <summary>
    /// Gets or sets whether to include all properties of the log event in the document
    /// </summary>
    public string? SendBulk { get; set; }
    private bool GetSendBulk() => SendBulk.ToBool();

    /// <summary>
    /// Gets or sets a comma separated list of excluded properties
    /// </summary>
    public string? ExcludedProperties { get; set; }

    /// <summary>
    /// Gets or sets a list of additional fields to add to the Elasticsearch document
    /// </summary>
    [ArrayParameter(typeof(Field), "field")]
    public IList<Field> Fields { get; set; } = [];

    /// <summary>
    /// Constructor for Elasticsearch target
    /// </summary>
    public ElasticsearchTarget()
    {
        Name = "Elasticsearch";
    }

    /// <summary>
    /// Implementation of Initialize Target for the Elasticsearch NLog target
    /// </summary>
    protected override void InitializeTarget()
    {
        base.InitializeTarget();

        if (!string.IsNullOrEmpty(ExcludedProperties))
            _excludedProperties = new HashSet<string>(ExcludedProperties.Split(',', StringSplitOptions.RemoveEmptyEntries));
    }

    /// <summary>
    /// Writes log event to the log target
    /// </summary>
    /// <param name="logEvent">event to log</param>
    protected override void Write(LogEventInfo logEvent)
    {
        throw new NotSupportedException("Synchronous write operation is not supported.");
    }

    /// <summary>
    /// Implementation of write single event for async logging
    /// </summary>
    /// <param name="logEvent">async event to log</param>
    protected override void Write(AsyncLogEventInfo logEvent)
    {
        SendBatch(Enumerable.Repeat(logEvent, 1), 1);
    }

    /// <summary>
    /// Implementation of buffered / queued writes for async logging
    /// </summary>
    /// <param name="logEvents">array of async events to log</param>
    protected override void Write(IList<AsyncLogEventInfo> logEvents)
    {
        SendBatch(logEvents, logEvents.Count);
    }

    private void SendBatch(IEnumerable<AsyncLogEventInfo> events, int length)
    {
        if (GetSendBulk())
        {
            Exception? continuation = null;
            try
            {
                string uri = $"{BaseUri}/_bulk";
                InternalLogger.Debug($"Sending {length} messages to Elasticsearch.");

                var sb = new StringBuilder(length * _AverageEventContentLength);
                foreach (var e in events)
                {
                    var metadata = GetBulkActionMetadata();
                    sb.AppendLine(JsonUtil.SerializeObject(metadata));

                    var payload = GetDocumentFromEvent(e.LogEvent);
                    sb.AppendLine(JsonUtil.SerializeObject(payload));
                }
                PostPayload(sb.ToString(), uri);
            }
            catch (Exception ex)
            {
                InternalLogger.Error(ex, "Exception while sending log messages to Elasticsearch");

                if (InternalLogger.IsTraceEnabled)
                    InternalLogger.Trace("Creating new HttpClient (to attempt to connect to different server if load balanced)");
                _client.Value = GetNewHttpClient();

                continuation = ex;
            }
            finally
            {
                foreach (var ev in events)
                {
                    ev.Continuation(continuation);
                }
            }
        }
        else
        {
            string uri = $"{BaseUri}/{Index}";
            if (!string.IsNullOrWhiteSpace(Pipeline))
                uri += $"?pipeline={Pipeline}";

            foreach (var ev in events)
            {
                Exception? continuation = null;
                try
                {
                    InternalLogger.Debug($"Sending a message to Elasticsearch.");
                    var payload = GetDocumentFromEvent(ev.LogEvent);                        
                    var strPayload = JsonUtil.SerializeObject(payload);
                    
                    PostPayload(strPayload, uri);
                }
                catch (Exception ex)
                {
                    InternalLogger.Error(ex, "Exception while sending log messages to Elasticsearch");

                    if (InternalLogger.IsTraceEnabled)
                        InternalLogger.Trace("Creating new HttpClient (to attempt to connect to different server if load balanced)");
                    _client.Value = GetNewHttpClient();

                    continuation = ex;
                }
                finally
                {
                    ev.Continuation(continuation);
                }
            }
        }
    }

    private void PostPayload(string payload, string uri)
    {
        if (InternalLogger.IsTraceEnabled)
            InternalLogger.Trace($"Sending message to Elasticsearch. payload = \"{payload}\"");

        var result = PostAsync(payload, uri).Result;

        if (!result.IsSuccessStatusCode)
        {
            var errorMessage = result.ReasonPhrase ?? $"No error message.{(InternalLogger.IsTraceEnabled ? "" : " Enable Trace logging for more information.")}";
            InternalLogger.Error($"Failed to send log message to Elasticsearch: status = {result.StatusCode}, message = \"{errorMessage}\"");

            if (InternalLogger.IsTraceEnabled)
                InternalLogger.Trace($"Send message error result = {JsonUtil.SerializeObject(result)}");
        }
        else
        {
            InternalLogger.Debug($"Message sent to Elasticsearch successfully.");
        }
    }

    private Dictionary<string, object?> GetDocumentFromEvent(LogEventInfo logEvent)
    {
        var document = new Dictionary<string, object?>
        {
            {"@timestamp", logEvent.TimeStamp},
            {"host", Environment.MachineName },
            {"message", Layout.Render(logEvent)}
        };

        foreach (var field in Fields)
        {
            var renderedField = field.Layout.Render(logEvent);
            if (!string.IsNullOrWhiteSpace(renderedField))
            {
                if (field.DecodeJsonString)
                {
                    document[field.Name] = JsonUtil.DeserializeObject<Dictionary<string, object>>(renderedField);
                }
                else
                {
                    document[field.Name] = Convert.ChangeType(renderedField, field.LayoutType);
                }
            }
        }

        if (GetIncludeProperties())
        {
            foreach (var p in logEvent.Properties)
            {
                var key = p.Key.ToString();
                if (key == null)
                    continue;
                if (!_excludedProperties.Contains(key) && !document.ContainsKey(key))
                {
                    document[key] = p.Value;
                }
            }
        }

        return document;
    }

    private object GetBulkActionMetadata()
    {
        object metadata;
        if (!string.IsNullOrWhiteSpace(Pipeline))
            metadata = new { index = new { _index = Index, pipeline = Pipeline } };
        else
            metadata = new { index = new { _index = Index } };

        return metadata;
    }

    private async Task<HttpResponseMessage> PostAsync(string payload, string uri)
    {
        var request = new HttpRequestMessage()
        {
            RequestUri = new Uri(uri),
            Method = HttpMethod.Post,
            Content = new StringContent(payload)
        };

        if (GetRequireAuth())
            request.Headers.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{Username}:{Password}")));

        if (!string.IsNullOrWhiteSpace(ContentType))
            request.Content.Headers.ContentType = new MediaTypeHeaderValue(ContentType);

        if (!_client.IsValueCreated)
            InternalLogger.Debug("New HttpClient created for thread.");

        return await _client.Value!.SendAsync(request);
    }

    private static HttpClient GetNewHttpClient()
    {
        return new HttpClient() { Timeout = TimeSpan.FromSeconds(10) };
    }
}
