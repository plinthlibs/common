using Microsoft.Extensions.Logging;
using NLog;
using Plinth.Common.Utils;
using System.Diagnostics;
using Microsoft.Extensions.DependencyInjection;
using System.Runtime.CompilerServices;

namespace Plinth.Logging.NLog;

using NL = global::NLog;
using NLCfg = global::NLog.Config;

/// <summary>
/// Helpers for configurating Microsoft.Extensions.Logging to use NLog
/// </summary>
public static class StaticLogManagerSetup
{
    /// <summary>
    /// Default NLog.config filename
    /// </summary>
    public const string DefaultNLogConfigFile = "NLog.config";

    /// <summary>
    /// Configure NLog as the ILoggingProvier for microsoft extensions logging
    /// </summary>
    /// <param name="builder">from DI container</param>
    /// <param name="nLogConfigFileName">(optional) override for nlog config file name (NLog.config)</param>
    /// <example>
    /// From inside Startup.cs / ConfigureServices
    /// <code>
    /// services.AddLogging(b => b.AddNLog());
    /// </code>
    /// </example>
    public static ILoggingBuilder AddNLog(this ILoggingBuilder builder, string nLogConfigFileName = DefaultNLogConfigFile)
    {
        // NLog takes over all logging
        builder.ClearProviders();

        // if nlog was already set up, reuse it
        if (StaticLogManager.StaticLoggerFactoryId == "NLOG")
        {
            builder.AddProvider(StaticLogManager.StaticLoggerMainProvider ?? throw new InvalidOperationException());
        }
        else
        {
            // default to current working dir, but if that isn't found, use exe home
            if (!File.Exists(nLogConfigFileName))
                nLogConfigFileName = Path.Combine(RuntimeUtil.FindExecutableHome(), nLogConfigFileName);

            var config = new NLCfg.XmlLoggingConfiguration(nLogConfigFileName);

            // these will disable System and Microsoft logs Info and below by default, unless custom rules are in NLog.config
            AddDefaultRule(config, NL.LogLevel.Info, "System.");
            AddDefaultRule(config, NL.LogLevel.Info, "Microsoft.");

            NL.Extensions.Logging.ConfigureExtensions.AddNLog(builder, config);

            var provider = builder.Services.BuildServiceProvider().GetRequiredService<ILoggerProvider>();

            var fac = new LoggerFactory();
            fac.AddProvider(provider);
            StaticLogManager.SetLoggerFactory(fac, "NLOG");
            StaticLogManager.StaticLoggerMainProvider = provider;

            // clean nlog shutdown
            AppDomain.CurrentDomain.ProcessExit += (_a, _b) => LogManager.Shutdown();
        }

        return builder;
    }

    private static void AddDefaultRule(NLCfg.LoggingConfiguration config, NL.LogLevel maxLevel, string pattern)
    {
        if (!config.LoggingRules.Any(lr => lr.LoggerNamePattern.StartsWith(pattern)))
        {
            var r = new NLCfg.LoggingRule($"{pattern}*", NL.LogLevel.Trace, maxLevel, new NL.Targets.NullTarget())
            {
                Final = true
            };
            config.LoggingRules.Insert(0, r);
        }
    }

    /// <summary>
    /// Configure NLog as the ILoggingProvier for microsoft extensions logging
    /// </summary>
    /// <param name="services">DI container</param>
    /// <param name="nLogConfigFileName">(optional) override for nlog config file name (NLog.config)</param>
    /// <example>
    /// From inside Startup.cs / ConfigureServices
    /// <code>
    /// services.AddNLog();
    /// </code>
    /// </example>
    public static IServiceCollection AddNLog(this IServiceCollection services, string nLogConfigFileName = DefaultNLogConfigFile)
    {
        return services.AddLogging(builder => builder.AddNLog(nLogConfigFileName));
    }

    /// <summary>
    /// Load configuration using NLog.config
    /// </summary>
    /// <param name="nLogConfigFileName">(optional) override for nlog config file name (NLog.config)</param>
    /// <example>
    /// Sample appsettings.json (these settings override settings in code)
    /// <code>
    /// {
    ///   "Logging": {
    ///     "IncludeScopes": false,
    ///     "LogLevel": {
    ///       "Default": "Trace",
    ///       "Microsoft": "Warning"
    ///       "Microsoft.Hosting.Lifetime": "Information"
    ///     }
    ///   }
    /// }
    /// 
    /// var log = StaticLogManagerSetup.BasicNLogSetup();
    /// </code>
    /// </example>
    /// <returns>an ILogger for logging in Main()</returns>
    [MethodImpl(MethodImplOptions.NoInlining)] // this is important for the automatic class detection
    public static Microsoft.Extensions.Logging.ILogger BasicNLogSetup(string nLogConfigFileName = DefaultNLogConfigFile)
    {
        LoggerFactory.Create(builder => builder.AddNLog(nLogConfigFileName));

        var frame = new StackFrame(1, false);
        var method = frame.GetMethod();

        var declaringType = method?.DeclaringType ?? throw new InvalidOperationException("cannot get calling method");

        return StaticLogManager.StaticLoggerFactory.CreateLogger(declaringType);
    }
}
