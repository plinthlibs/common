using Plinth.Common.Extensions;
using NLog;
using NLog.LayoutRenderers;
using System.Text;
using Plinth.Common.Logging;

namespace Plinth.Logging.NLog.Renderers;

/// <summary>
/// Custom renderer for filtering sensitive data and truncating on max length
/// </summary>
[LayoutRenderer("filteredMessage")]
[AmbientProperty("MaxLength")]
public class FilteredMessageRenderer : MessageLayoutRenderer
{
    /// <summary>
    /// Max line length
    /// </summary>
    [System.ComponentModel.DefaultValue(25000)]
    public int MaxLength { get; set; }

    /// <summary>
    /// Custom sensitive data masker
    /// </summary>
    public ISensitiveDataMasker? SensitiveDataMasker { get; set; }

    /// <summary>
    /// Constructor
    /// </summary>
    public FilteredMessageRenderer(ISensitiveDataMasker sensitiveDataMasker) : this()
    {
        SensitiveDataMasker = sensitiveDataMasker;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    public FilteredMessageRenderer()
    {
        MaxLength = 25000;
    }

    /// <summary>
    /// Called by NLog to append our data
    /// </summary>
    protected override void Append(StringBuilder builder, LogEventInfo logEvent)
    {
        string msg = logEvent.FormattedMessage;

        if (msg.Length > MaxLength)
            msg = msg[..MaxLength] + "<truncated>";

        msg = msg.MaskSensitiveFields();

        if (SensitiveDataMasker != null)
            msg = SensitiveDataMasker.MaskSensitiveFields(msg);

        builder.Append(msg);
    }
}
