﻿using NLog;
using NLog.LayoutRenderers;
using System.Reflection;
using System.Text;

namespace Plinth.Logging.NLog.Renderers;

/// <summary>
/// Custom renderer that will log the version of the specified assembly
/// </summary>
[LayoutRenderer("assemblyVersion")]
[AmbientProperty("AssemblyName")]
public class AssemblyVersionRenderer : LayoutRenderer
{
    /// <summary>
    /// Name of the assembly for which to get the version
    /// </summary>
    public string? AssemblyName { get; set; }

    private readonly Lazy<string> LazyAssemblyVersion;
    private string AssemblyVersion => LazyAssemblyVersion.Value;

    /// <summary>
    /// Constructor
    /// </summary>
    public AssemblyVersionRenderer()
    {
        LazyAssemblyVersion = new Lazy<string>(() =>
        {
            Assembly? assembly = null;

            if (!string.IsNullOrEmpty(AssemblyName))
            {
                assembly = Assembly.Load(new AssemblyName(AssemblyName));
            }
            else
            {
                assembly = Assembly.GetExecutingAssembly() ?? Assembly.GetEntryAssembly();
            }

            var fileVer = assembly?.GetCustomAttribute<AssemblyFileVersionAttribute>();
            return fileVer?.Version ?? assembly?.GetName()?.Version?.ToString() ?? string.Empty;
        });
    }

    /// <summary>
    /// Called by NLog to append our data
    /// </summary>
    protected override void Append(StringBuilder builder, LogEventInfo logEvent)
    {
        if (!string.IsNullOrEmpty(AssemblyVersion))
            builder.Append(AssemblyVersion);
    }
}
