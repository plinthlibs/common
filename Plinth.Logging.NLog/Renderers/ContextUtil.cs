using NLog;
using Plinth.Common.Logging;

namespace Plinth.Logging.NLog.Renderers;

internal static class ContextUtil
{
    private static object? GetProperty(LogEventInfo logEvent, string key)
    {
        if (ScopeContext.TryGetProperty(key, out var value))
            return value;

        if (logEvent.HasProperties)
        {
            foreach (var kv in logEvent.Properties)
                if ((kv.Key as string) == key)
                    return kv.Value;
        }

        return null;
    }

    public static string? GetMessageType(LogEventInfo logEvent)
        => GetProperty(logEvent, LoggingConstants.Field_MessageType) as string;

    public static string? GetContextId(LogEventInfo logEvent)
        => GetProperty(logEvent, LoggingConstants.Field_ContextId) as string;

    public static string? GetRequestTraceId(LogEventInfo logEvent)
        => GetProperty(logEvent, LoggingConstants.Field_RequestTraceId) as string;

    public static string? GetRequestTraceChain(LogEventInfo logEvent)
        => GetProperty(logEvent, LoggingConstants.Field_RequestTraceChain) as string;
}
