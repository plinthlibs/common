﻿using NLog.LayoutRenderers;
using NLog.LayoutRenderers.Wrappers;

namespace Plinth.Logging.NLog.Renderers;

/// <summary>
/// Custom renderer that will truncate text from the left by the specified amount of characters
/// </summary>
[LayoutRenderer("truncateLeft")]
[AmbientProperty("Length")]
public class TruncateLeftRenderer : WrapperLayoutRendererBase
{
    /// <summary>
    /// Length at which to truncate
    /// </summary>
    [System.ComponentModel.DefaultValue(10)]
    public int Length { get; set; }

    /// <summary>
    /// Constructor
    /// </summary>
    public TruncateLeftRenderer()
    {
        Length = 10;
    }

    /// <summary>
    /// Called by NLog to transform our data
    /// </summary>
    protected override string Transform(string text)
    {
        if (text.Length > Length)
        {
            return text[^Length..];
        }
        else if (text.Length < Length)
        {
            return text.PadLeft(Length);
        }
        else
        {
            return text;
        }
    }
}
