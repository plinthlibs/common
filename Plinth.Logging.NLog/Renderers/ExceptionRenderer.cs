using NLog;
using NLog.LayoutRenderers;
using System.Diagnostics;
using System.Text;

namespace Plinth.Logging.NLog.Renderers;

/// <summary>
/// Custom Renderer for nicer exceptions in the log
/// </summary>
[LayoutRenderer("exception")]
[AmbientProperty("MaxFrames")]
[AmbientProperty("NamespacePrefixes")]
public class ExceptionRenderer : StackTraceLayoutRenderer
{
    /// <summary>
    /// Maximum number of stack frames in an exception log
    /// </summary>
    [System.ComponentModel.DefaultValue(20)]
    public int MaxFrames { get; set; }

    /// <summary>
    /// Comma separated namespace prefixes for file name rendering
    /// </summary>
    /// <example>
    /// If file is C:\builds\project\Plinth.Project.Core\Controllers\SomethingController.cs, passing "Plinth." will
    /// render the file as Plinth.Project.Core\Controllers\SomethingController.cs, stopping at the first directory with one of the prefixes
    /// </example>
    /// <remarks>
    /// avoids spamming logs with long directories from build servers
    /// </remarks>
    [System.ComponentModel.DefaultValue("Plinth.")]
    public string NamespacePrefixes
    {
        get => string.Join(",", _namespacePrefixes);
        set => _namespacePrefixes = string.IsNullOrWhiteSpace(value) ? _defaultNamespacePrefixes : value.Split(',').Select(s => s.Trim()).ToArray();
    }
    private string[] _namespacePrefixes = _defaultNamespacePrefixes;
    private static readonly string[] _defaultNamespacePrefixes = ["Plinth."];

    /// <summary>
    /// Constructor
    /// </summary>
    public ExceptionRenderer()
    {
        MaxFrames = 100;
    }

    /// <summary>
    /// Called by NLog to append our data
    /// </summary>
    protected override void Append(StringBuilder builder, LogEventInfo logEvent)
    {
        if (logEvent.Exception == null)
            return;

        WriteException(logEvent.Exception, builder, 0);
    }

    /// <summary>
    /// Write an exception to the log
    /// </summary>
    /// <param name="ex"></param>
    /// <param name="builder"></param>
    /// <param name="level"></param>
    /// <param name="isParentAggregate">true if this exception is an InnerException of an AggregateException</param>
    private void WriteException(Exception ex, StringBuilder builder, int level, bool isParentAggregate = false)
    {
        string threadName = Thread.CurrentThread.Name ?? ("Thread-" + Environment.CurrentManagedThreadId);

        builder.Append(Environment.NewLine);

        if (level == 0 || isParentAggregate)
        {
            builder.AppendFormat("{0}Exception in thread \"{1}\" {2}: {3}{4}",
                isParentAggregate ? "Aggregated: " : string.Empty,
                threadName,
                ex.GetType().FullName,
                ex.Message,
                Environment.NewLine);
        }
        else
        {
            builder.AppendFormat("Caused by: {0}: {1}{2}",
                ex.GetType().FullName,
                ex.Message,
                Environment.NewLine);
        }

        var trace = new StackTrace(ex, true).GetFrames();

        AppendStack(trace, builder);

        foreach (var innerException in GetInnerExceptions(ex))
        {
            WriteException(innerException, builder, level + 1, ex is AggregateException);
        }
    }

    private static IEnumerable<Exception> GetInnerExceptions(Exception ex)
    {
        if (ex is AggregateException agg)
        {
            return agg.Flatten().InnerExceptions;
        }

        return ex.InnerException != null ? [ex.InnerException] : [];
    }

    /// <summary>
    /// Append the stack trace
    /// </summary>
    /// <param name="trace"></param>
    /// <param name="builder"></param>
    private void AppendStack(StackFrame?[]? trace, StringBuilder builder)
    {
        int mark = 0;

        if (trace == null || trace.Length == 0)
        {
            builder.Append("    [No stack trace present]");
            return;
        }

        for (int i = 0; i < MaxFrames && i < trace.Length; i++)
        {
            var frame = trace[i];
            var method = frame?.GetMethod();
            var declType = method?.DeclaringType;

            if (declType == null)
            {
                builder.AppendFormat("    at <unknown type>({0})", method?.Name);
                AppendFileInfo(frame, builder);
            }
            else
            {
                builder.AppendFormat("    at {0}.{1}(", declType.FullName, method!.Name);
                AppendFileInfo(frame, builder);
                builder.Append(')');
            }

            mark = builder.Length;
            builder.Append(Environment.NewLine);
        }

        if (trace.Length > MaxFrames)
        {
            builder.AppendFormat("    ... {0} more", trace.Length - MaxFrames);
            mark = builder.Length;
        }

        builder.Length = mark;
    }

    /// <summary>
    /// Construct the file info (file name and line number)
    /// </summary>
    /// <param name="frame"></param>
    /// <param name="builder"></param>
    private void AppendFileInfo(StackFrame? frame, StringBuilder builder)
    {
        var fileName = frame?.GetFileName();
        if (fileName == null)
        {
            builder.Append("<unknown file>");
            return;
        }

        if (checkPrefixContains(fileName))
        {
            var fi = new FileInfo(fileName);
            FilterDir(fi.Directory, builder);
            builder.Append(Path.DirectorySeparatorChar).AppendFormat("{0}:{1}", fi.Name, frame!.GetFileLineNumber());
        }
        else
            builder.AppendFormat("{0}:{1}", fileName, frame!.GetFileLineNumber());

        bool checkPrefixContains(string val) => _namespacePrefixes.Any(s => val.Contains(s));
    }

    /// <summary>
    /// do a post-order traversal to append the path of the file, stopping at the first namespace prefix match
    /// so it doesn't log the full path that was on the build machine
    /// </summary>
    /// <param name="di"></param>
    /// <param name="sb"></param>
    private void FilterDir(DirectoryInfo? di, StringBuilder sb)
    {
        if (di == null)
            return;

        if (di.Parent == null || checkPrefix(di.Name))
        {
            if (di.Name.FirstOrDefault() != Path.DirectorySeparatorChar)
                sb.Append(di.Name);
            return;
        }

        FilterDir(di.Parent, sb);
        sb.Append(Path.DirectorySeparatorChar).Append(di.Name);

        bool checkPrefix(string val) => _namespacePrefixes.Any(s => val.StartsWith(s));
    }
}
