﻿using NLog;
using NLog.LayoutRenderers;
using Plinth.Common.Logging;
using System.Text;

namespace Plinth.Logging.NLog.Renderers;

/// <summary>
/// Custom renderer that will log the trace id associated with an api request
/// </summary>
[LayoutRenderer("requestTraceId")]
public class RequestTraceIdRenderer : LayoutRenderer
{
    /// <summary>
    /// Called by NLog to append our data
    /// </summary>
    protected override void Append(StringBuilder builder, LogEventInfo logEvent)
    {
        var rId = RequestTraceId.Instance.Get();

        // fall back to log metadata if nothing in async context, handles cases where the context is lost but stored via some other means
        if (string.IsNullOrEmpty(rId))
            rId = ContextUtil.GetRequestTraceId(logEvent);

        if (!string.IsNullOrEmpty(rId))
            builder.Append(rId);
    }
}
