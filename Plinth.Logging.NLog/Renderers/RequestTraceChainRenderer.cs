﻿using NLog;
using NLog.LayoutRenderers;
using Plinth.Common.Logging;
using System.Text;

namespace Plinth.Logging.NLog.Renderers;

/// <summary>
/// Custom renderer that will log the trace chain associated with an api request
/// </summary>
[LayoutRenderer("requestTraceChain")]
public class RequestTraceChainRenderer : LayoutRenderer
{
    /// <summary>
    /// Called by NLog to append our data
    /// </summary>
    protected override void Append(StringBuilder builder, LogEventInfo logEvent)
    {
        var rChain = RequestTraceChain.Instance.Get();

        // fall back to log metadata if nothing in async context, handles cases where the context is lost but stored via some other means
        if (string.IsNullOrEmpty(rChain))
            rChain = ContextUtil.GetRequestTraceChain(logEvent);

        if (!string.IsNullOrEmpty(rChain))
            builder.Append(rChain);
    }
}
