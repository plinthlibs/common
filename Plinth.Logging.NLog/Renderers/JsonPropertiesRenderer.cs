using Plinth.Serialization;
using NLog;
using NLog.LayoutRenderers;
using System.Text;
using Plinth.Common.Logging;

namespace Plinth.Logging.NLog.Renderers;

/// <summary>
/// Custom renderer for rendering LoggableObject properties as JSON
/// </summary>
[LayoutRenderer("jsonProperties")]
public class JsonPropertiesRenderer : LayoutRenderer
{
    /// <summary>
    /// Called by NLog to append our data
    /// </summary>
    protected override void Append(StringBuilder builder, LogEventInfo logEvent)
    {
        var properties = new Dictionary<string, object>(ScopeContext.GetAllProperties());

        // GlobalDiagnosticsContext are global scope items, cannot set via ms ILogger
        foreach (var p in GlobalDiagnosticsContext.GetNames())
            properties[p] = GlobalDiagnosticsContext.GetObject(p);

        // Properties comes from embedded values  "{Value}", 83
        // these override scope variables
        if (logEvent.HasProperties)
        {
            foreach (var kv in logEvent.Properties)
            {
                if (kv.Key is string k)
                    properties[k] = kv.Value;
            }
        }

        // properties persisted from the call chain
        var rProps = RequestTraceProperties.GetRequestTraceProperties();
        if (!rProps.IsNullOrEmpty())
        {
            foreach (var prop in rProps!)
            {
                if (!properties.ContainsKey(prop.Key))
                    properties.Add(prop.Key, prop.Value);
            }
        }

        if (logEvent.Message != logEvent.FormattedMessage)
            properties.Add("original_msg", logEvent.Message);

        properties.Remove(LoggingConstants.Field_ContextId);
        properties.Remove(LoggingConstants.Field_RequestTraceChain);
        properties.Remove(LoggingConstants.Field_RequestTraceId);
        builder.Append(JsonUtil.SerializeObject(properties));
    }

}
