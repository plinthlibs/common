﻿using Microsoft.Extensions.Logging;
using NLog;
using NLog.LayoutRenderers;
using System.Text;

namespace Plinth.Logging.NLog.Renderers;

/// <summary>
/// Custom renderer that will log some value for all items in an async context
/// </summary>
[LayoutRenderer("contextId")]
public class AsyncCtxRenderer : LayoutRenderer
{
    /// <summary>
    /// Renders the specified MDLC item and appends it to the specified <see cref="StringBuilder" />.
    /// </summary>
    /// <param name="builder">The <see cref="StringBuilder"/> to append the rendered data to.</param>
    /// <param name="logEvent">Logging event.</param>
    protected override void Append(StringBuilder builder, LogEventInfo logEvent)
    {
        var id = StaticLogManager.GetContextId();

        // fall back to log metadata if nothing in async context, handles cases where the context is lost but stored via some other means
        if (string.IsNullOrEmpty(id))
            id = ContextUtil.GetContextId(logEvent);

        if (string.IsNullOrEmpty(id))
        {
            builder.Append("     ");
            return;
        }

        builder.AppendFormat("{0,-5}", id);
    }
}
