using Newtonsoft.Json.Serialization;
using System.Text.RegularExpressions;

namespace Plinth.Serialization.JsonNet;

/// <summary>
/// Use in Json Deserialization if the json multi-word keys are delimited by underscores
/// </summary>
/// <remarks>
/// Performs this conversion <code>"api_key" => string ApiKey { get; set; }</code>
/// </remarks>
/// <example>
/// Usage:
///
/// <code>
///     return JsonUtil.DeserializeObject(
///         stringValue,
///         (s) =>
///         {
///             s.ContractResolver = new JsonNetUnderscorePropertyNamesContractResolver();
///         });
/// </code>
/// </example>
public partial class JsonNetUnderscorePropertyNamesContractResolver : DefaultContractResolver
{
#if NET8_0_OR_GREATER
    [GeneratedRegex("(?<=[^A-Z])(?<letter>[A-Z])")]
    private static partial Regex LetterRegex();
#else
    private static readonly Regex _regex = new ("(?<=[^A-Z])(?<letter>[A-Z])");
    private static Regex LetterRegex() => _regex;
#endif

    /// <summary>
    /// Called by Json.Net to resolve the property name
    /// </summary>
    protected override string ResolvePropertyName(string propertyName)
    {
        return LetterRegex().Replace(propertyName, "_${letter}").ToLowerInvariant();
    }
}
