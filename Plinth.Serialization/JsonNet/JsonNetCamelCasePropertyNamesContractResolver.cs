﻿using Newtonsoft.Json.Serialization;

namespace Plinth.Serialization.JsonNet;

/// <summary>
/// Resolves member mappings for a type, camel casing property names (except for Dictionaries).
/// <para>
/// This takes all the behavior of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver,
/// but prevents the names of Dictionary keys from being converted, so that Dicationary&lt;string,object&gt;'s key names won't change
/// </para>
/// </summary>
/// <remarks>
///
/// <code>"apiKey" => string ApiKey { get; set; }</code>
///
/// </remarks>
/// <example>
/// Usage:
///
/// <code>
///     return JsonUtil.DeserializeObject(
///         stringValue,
///         (s) =>
///         {
///             s.ContractResolver = new JsonNetCamelCasePropertyNamesContractResolver();
///         });
/// </code>
/// </example>
public class JsonNetCamelCasePropertyNamesContractResolver : CamelCasePropertyNamesContractResolver
{
    /// <summary>
    /// Called by Json.Net to create the dictionary contract
    /// </summary>
    protected override JsonDictionaryContract CreateDictionaryContract(Type objectType)
    {
        JsonDictionaryContract contract = base.CreateDictionaryContract(objectType);

        contract.DictionaryKeyResolver = propertyName => propertyName;

        return contract;
    }
}
