﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Plinth.Serialization.JsonNet;

/// <summary>
/// Extension to Json.NET's <see cref="StringEnumConverter"/> to not throw when converting unsupported
/// enum values into a nullable enum. Instead, the converted value will be set to null.
/// </summary>
/// <remarks>
/// Converting to non-nullable enums will still throw, just like the default <see cref="StringEnumConverter"/>
/// </remarks>
public class JsonNetStringNullableEnumConverter : StringEnumConverter
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    /// <summary>
    /// Reads the JSON representation of the object.
    /// </summary>
    /// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
    /// <param name="objectType">Type of the object.</param>
    /// <param name="existingValue">The existing value of object being read.</param>
    /// <param name="serializer">The calling serializer.</param>
    /// <returns>The object value.</returns>
    public override object? ReadJson(JsonReader reader, Type objectType, object? existingValue, JsonSerializer serializer)
    {
        try
        {
            return base.ReadJson(reader, objectType, existingValue, serializer);
        }
        catch (JsonSerializationException e)
        {
            if (IsNullableType(objectType))
            {
                log.Warn(e, "Could not serialize nullable value");
                return null;
            }

            throw;
        }
    }

    private static bool IsNullableType(Type t)
    {
        return t.IsValueType && (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>));
    }
}
