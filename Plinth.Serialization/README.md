# README #

### Plinth.Serialization ###

**Serialization utilities for JSON and XML**

* JsonNet
	* Converters and Contract Resolvers for custom JSON serialization using Newtonsoft.Json
* JsonUtil
	* Wrapper over Newtonsoft.Json that applies Plinth standard JSON serialization
* XmlUtil
	* Shortcut methods for serialization of XML files
* UtcDateTimeConverter
	* A System.ComponentModel.DateTimeConverter for ensuring all deserialized dates are converted to UTC
