using System.ComponentModel;
using System.Globalization;

namespace Plinth.Serialization;

/// <summary>
/// Custom UTC converter which will process date/time strings and product proper UTC DateTime
/// </summary>
/// <remarks>
/// This assumes that the value is UTC unless the TZ is specified.
/// </remarks>
/// <example>
/// Use <code>UtcDateTimeConverter .ApplyGlobally()</code> to enable globally
/// </example>
public sealed class UtcDateTimeConverter : DateTimeConverter
{
    /// <summary>
    /// Convert a string value to a proper UTC DateTime
    /// </summary>
    /// <param name="context">ignored</param>
    /// <param name="culture">ignored</param>
    /// <param name="value">date/time string</param>
    /// <returns>a proper UTC DateTime</returns>
    public override object? ConvertFrom(ITypeDescriptorContext? context, CultureInfo? culture, object value)
    {
        if (value is not string s) throw new ArgumentNullException(nameof(value));
        return DateTime.Parse(s, null, DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal);
    }

    /// <summary>
    /// Apply this Type Converter globally to all DateTime conversions
    /// Useful for querystring parameters in WebApi
    /// </summary>
    public static void ApplyGlobally()
    {
        TypeDescriptor.AddAttributes(typeof(DateTime), new TypeConverterAttribute(typeof(UtcDateTimeConverter)));
    }
}
