using Plinth.Serialization.JsonNet;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Logging;
using System.Diagnostics.CodeAnalysis;

namespace Plinth.Serialization;

/// <summary>
/// Utility methods for serialization and deserialization using Json.Net
/// </summary>
public static class JsonUtil
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private static readonly JsonSerializerSettings _settings = new();
    private static readonly JsonSerializer _serializer;

    /// <summary>
    /// Configures the supplied serializer settings
    /// </summary>
    static JsonUtil()
    {
        ApplyDefaultSettings(_settings);
        _serializer = JsonSerializer.Create(_settings);
    }

    /// <summary>
    /// Apply our default settings to the settings object
    /// </summary>
    /// <param name="s"></param>
    public static void ApplyDefaultSettings(JsonSerializerSettings s)
    {
        s.Converters.Add(new JsonNetStringNullableEnumConverter());
        s.FloatParseHandling = FloatParseHandling.Decimal;
        s.NullValueHandling = NullValueHandling.Ignore;
    }

    /// <summary>
    /// Serializes an object to json string using standard serialization settings
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="modSettings">customize the json settings with this action, null for no customization</param>
    /// <returns>json string</returns>
    public static string SerializeObject(object? obj, Action<JsonSerializerSettings>? modSettings = null)
    {
        using var writer = new StringWriter();
        GetSerializer(modSettings).Serialize(writer, obj);
        return writer.ToString();
    }

    /// <summary>
    /// Deserializes json string to object using standard serialization settings
    /// </summary>
    /// <typeparam name="T">any type</typeparam>
    /// <param name="s"></param>
    /// <param name="modSettings">customize the json settings with this action, null for no customization</param>
    /// <returns>object of type T</returns>
    public static T? DeserializeObject<T>(string s, Action<JsonSerializerSettings>? modSettings = null)
    {
        using var reader = new JsonTextReader(new StringReader(s));
        return GetSerializer(modSettings).Deserialize<T>(reader);
    }

    /// <summary>
    /// Tries to deserialize json string to object using standard serialization settings
    /// </summary>
    /// <typeparam name="T">any type</typeparam>
    /// <param name="s"></param>
    /// <param name="result">result of deserialization, default if return is false</param>
    /// <param name="modSettings">customize the json settings with this action, null for no customization</param>
    /// <returns>true if deserialize succeeded, false otherwise</returns>
    public static bool TryDeserializeObject<T>(string s, [MaybeNullWhen(false)] out T result, Action<JsonSerializerSettings>? modSettings = null)
    {
        try
        {
            result = DeserializeObject<T>(s, modSettings);
            if (!typeof(T).IsValueType)
                return !EqualityComparer<T>.Default.Equals(result, default);
            else
            {
#pragma warning disable CS8762 // Parameter must have a non-null value when exiting in some condition.
                return true;
#pragma warning restore CS8762 // Parameter must have a non-null value when exiting in some condition.
            }
        }
        catch (Exception)
        {
            result = default;
            return false;
        }
    }

    /// <summary>
    /// Deserializes contents of a json file to object
    /// </summary>
    /// <typeparam name="T">any type</typeparam>
    /// <param name="fileName"></param>
    /// <param name="modSettings">customize the json settings with this action, null for no customization</param>
    /// <returns>object of type T</returns>
    public static T? DeserializeObjectFromFile<T>(string fileName, Action<JsonSerializerSettings>? modSettings = null)
        => DeserializeObject<T>(File.ReadAllText(fileName), modSettings);

    /// <summary>
    /// Tries to deserialize contents of a json file to object
    /// </summary>
    /// <typeparam name="T">any type</typeparam>
    /// <param name="fileName"></param>
    /// <param name="result">result of deserialization, default if return is false</param>
    /// <param name="modSettings">customize the json settings with this action, null for no customization</param>
    /// <returns>true if deserialize succeeded, false otherwise</returns>
    public static bool TryDeserializeObjectFromFile<T>(string fileName, [MaybeNullWhen(false)] out T result, Action<JsonSerializerSettings>? modSettings = null)
    {
        try
        {
            result = DeserializeObjectFromFile<T>(fileName, modSettings);
            if (!typeof(T).IsValueType)
                return !EqualityComparer<T>.Default.Equals(result, default);
            else
            {
#pragma warning disable CS8762 // Parameter must have a non-null value when exiting in some condition.
                return true;
#pragma warning restore CS8762 // Parameter must have a non-null value when exiting in some condition.
            }
        }
        catch (Exception)
        {
            result = default;
            return false;
        }
    }

    /// <summary>
    /// Uses json serialize/deserialize to create a deep clone of an object
    /// </summary>
    /// <typeparam name="T">any type</typeparam>
    /// <param name="obj">object to clone</param>
    /// <returns>cloned object of type T</returns>
    public static T? DeepClone<T>(T? obj)
        => DeserializeObject<T>(SerializeObject(obj));

    /// <summary>
    /// Convert an object deserialized from json
    /// </summary>
    /// <typeparam name="T">type of the converted value</typeparam>
    /// <param name="o">the object to convert</param>
    /// <param name="defaultVal">default value if not found or can't convert</param>
    /// <returns>converted value or default</returns>
    public static T? ConvertDeserializedObject<T>(object? o, T? defaultVal = default)
        => TryConvertDeserializedObject(o, out T? output) ? output : defaultVal;

    /// <summary>
    /// Try to convert an object deserialized from json
    /// </summary>
    /// <typeparam name="T">type of the converted value</typeparam>
    /// <param name="o">the object to convert</param>
    /// <param name="value">(out) if found, value is converted and written</param>
    /// <returns>true if converted successfully, false otherwise</returns>
    public static bool TryConvertDeserializedObject<T>(object? o, [MaybeNullWhen(false)] out T value)
    {
        if (o == null)
        {
            value = default;
            return false;
        }

        try
        {
            if (o is T t)
            {
                // direct access to object after construction, generally no conversion required
                value = t;
            }
            else if (o is string s && typeof(T).IsEnum)
            {
                value = (T)Enum.Parse(typeof(T), s, true);
            }
            else if (o is IConvertible)
            {
                // after decryption, types are whatever json.net decides
                // if convertable (scalar types), just do conversion
                value = (T)Convert.ChangeType(o, typeof(T));
            }
            else if (o is JObject @object)
            {
                // after decryption, objects will be JObject, and need converting to object
                value = @object.ToObject<T>(GetSerializer());
            }
            else if (o is JArray array)
            {
                // after decryption, lists will be JArray, and need converting to object
                value = array.ToObject<T>(GetSerializer());
            }
            else
            {
                throw new InvalidCastException("object is not T, not IConvertible, and not JObject");
            }

            if (!typeof(T).IsValueType)
                return !EqualityComparer<T>.Default.Equals(value, default);
            else
            {
#pragma warning disable CS8762 // Parameter must have a non-null value when exiting in some condition.
                return true;
#pragma warning restore CS8762 // Parameter must have a non-null value when exiting in some condition.
            }
        }
        catch (Exception e)
        {
            log.Debug($"failed converting field in object: cannot convert {o.GetType().Name} to {typeof(T).Name}: {e.GetType().Name}: {e.Message}");
            value = default;
            return false;
        }
    }

    private static JsonSerializer GetSerializer(Action<JsonSerializerSettings>? modSettings = null)
    {
        JsonSerializer serializer = _serializer;
        if (modSettings != null)
        {
            // make a new copy if customization is desired
            var settings = new JsonSerializerSettings();
            ApplyDefaultSettings(settings);
            modSettings.Invoke(settings);
            serializer = JsonSerializer.Create(settings);
        }
        return serializer;
    }
}
