using Microsoft.Extensions.Logging;
using Plinth.Storage.Exceptions;
using Plinth.Storage.Models;
using Plinth.Storage.Providers;
using Plinth.Security.Crypto;
using Plinth.Common.Exceptions;
using Omu.ValueInjecter;

namespace Plinth.Storage;

/// <summary>
/// Manager for a Blob Storage operations
/// </summary>
public class StorageManager : IStorage
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private readonly IStorageIndexDriver _indexDriver;
    private readonly ISecureData? _secureData;
    private readonly Dictionary<string, Lazy<StorageProviderBase>> _providers;
    private readonly List<Lazy<StorageProviderBase>> _backupProviders;
    private readonly string _defaultWriter;
    private readonly BlobFeatures _defaultFeatures;

    /// <summary>
    /// Construct the blob storage manager
    /// </summary>
    internal StorageManager(
        IStorageIndexDriver indexDriver,
        ISecureData? secureData,
        Dictionary<string, Func<StorageProviderBase>> providers,
        List<Func<StorageProviderBase>> backupProviders,
        string defaultWriter,
        BlobFeatures defaultFeatures)
    {
        _indexDriver = indexDriver;
        _secureData = secureData;
        _providers = providers.ToDictionary(k => k.Key, k => new Lazy<StorageProviderBase>(k.Value));
        _backupProviders = backupProviders.Select(v => new Lazy<StorageProviderBase>(v)).ToList();
        _defaultWriter = defaultWriter;
        _defaultFeatures = defaultFeatures;
    }

    #region Read
    /// <inheritdoc cref="IStorage.ReadBlobAsync(Guid, CancellationToken)"/>
    public async Task<Blob?> ReadBlobAsync(Guid blobGuid, CancellationToken cancellationToken)
    {
        var blob = await _indexDriver.GetBlobByGUIDAsync(blobGuid, loadData: true, cancellationToken);
        if (blob == null)
            return null;

        var data = await GetProvider(blob.Provider).ReadBlobDataAsync(blob, cancellationToken);

        return ProcessReadBlob(blob, data);
    }

    private StorageProviderBase GetProvider(string providerKey)
    {
        if (!_providers.TryGetValue(providerKey, out var provLazy))
            throw new InvalidOperationException($"Provider '{providerKey}' has not been configured");
        return provLazy.Value;
    }

    /// <inheritdoc cref="IStorage.ReadBlobStreamAsync(Guid,CancellationToken)"/>
    public async Task<(Blob?, Stream?)> ReadBlobStreamAsync(Guid blobGuid, CancellationToken cancellationToken)
    {
        (var blob, var stream) = await _indexDriver.GetBlobStreamByGUIDAsync(blobGuid, loadData: true, cancellationToken);
        if (blob == null)
            return (null, null);

        var provider = GetProvider(blob.Provider);
        stream = await provider.ReadBlobDataStreamAsync(blob, stream, cancellationToken);

        return ProcessReadBlobStream(blob, stream, provider);
    }

    private Blob? ProcessReadBlob(Blob blob, byte[]? data)
    {
        if (data == null)
            throw new CorruptedBlobException("blob data not found");

        if (data.Length != blob.StoredSize)
            throw new CorruptedBlobException($"expected {blob.StoredSize} but got {data.Length} from provider");

        // if the blob is encrypted, the HMAC provides validity check
        // if the blob is compressed, the GZIP stream provides validity check
        // otherwise, only check is the length
        log.Debug($"Processing blob data for read: {blob.Features}");

        if (blob.Features.HasFlag(BlobFeatures.Encrypted))
            data = (_secureData ?? throw new InvalidOperationException("blob factory not configured with encryption capability")).Decrypt(data);
        if (data != null && blob.Features.HasFlag(BlobFeatures.Compressed))
        {
            try
            {
                data = CompressionUtil.Decompress(data);
            }
            catch (InvalidDataException e)
            {
                log.Error(e, "compressed data was invalid");
                return null;
            }
        }

        blob.Data = data;

        if (blob.Data!.Length != blob.BlobSize)
            throw new CorruptedBlobException($"expected {blob.BlobSize} but got {blob.Data.Length} after processing");

        return blob;
    }

    private (Blob, Stream) ProcessReadBlobStream(Blob blob, Stream? data, StorageProviderBase provider)
    {
        if (data == null)
            throw new CorruptedBlobException("blob data not found");

        log.Debug($"Processing blob data stream for read: {blob.Features}");

        var okToStream = CanStreamRead(blob.Features, provider);
        if (!okToStream.can)
            throw new NotSupportedException(okToStream.reason);

        if (blob.Features.HasFlag(BlobFeatures.Compressed))
            data = CompressionUtil.DecompressStream(data);

        data = new ValidityCheckReadStream(data, blob.BlobSize);

        return (blob, data);
    }

    private (bool can, string? reason) CanStreamRead(BlobFeatures features, StorageProviderBase provider)
    {
        // for stream read, we can't encrypt but we can decompress
        if (features.HasFlag(BlobFeatures.Encrypted))
            return (false, "encryption not supported for streams");

        // if we're ok to stream but we're reading from the db and the db doesn't support it
        if (provider.WritesDataToIndex && !_indexDriver.SupportsStreamRead)
            return (false, "database driver does not support streaming");

        return (true, null);
    }
    #endregion

    #region Write
    /// <inheritdoc cref="IStorage.CreateNewBlobAsync(Blob, Func{Blob, string}, BlobFeatures?, IDictionary{string, object}, CancellationToken)"/>
    public async Task<Guid> CreateNewBlobAsync(Blob blob, Func<Blob, string?>? indexStrategy = null, BlobFeatures? features = null, IDictionary<string, object>? providerParams = null, CancellationToken cancellationToken = default)
    {
        (var prov, var data) = ProcessWriteBlob(blob, indexStrategy, features);

        await _indexDriver.CreateBlobAsync(
            _defaultWriter,
            blob,
            prov.WritesDataToIndex ? data : null,
            blob.Data!.LongLength,
            data.LongLength,
            cancellationToken);

        await prov.WriteBlobAsync(blob, data, providerParams, cancellationToken);
        _indexDriver.AddAsyncRollbackAction($"deleting blob {blob.Guid} at index '{blob.Index}' due to rollback", async () => await prov.DeleteBlobAsync(blob));

        foreach (var bp in _backupProviders)
        {
            await bp.Value.WriteBlobAsync(blob, data, providerParams, cancellationToken);
            _indexDriver.AddAsyncRollbackAction($"deleting blob {blob.Guid} at index '{blob.Index}' on provider {bp.Value.Name} due to rollback", async () => await bp.Value.DeleteBlobAsync(blob));
        }

        return blob.Guid!.Value;
    }

    /// <inheritdoc cref="IStorage.CreateNewBlobAsync(Blob, Stream, long, Func{Blob, string}, BlobFeatures?, IDictionary{string, object}, CancellationToken)"/>
    public async Task<Guid> CreateNewBlobAsync(Blob blob, Stream stream, long lengthHint, Func<Blob, string?>? indexStrategy = null, BlobFeatures? features = null, IDictionary<string, object>? providerParams = null, CancellationToken cancellationToken = default)
    {
        var prov = ProcessWriteBlobStream(blob, indexStrategy, features);

        using (stream)
        {
            // if we will write to backup providers, we need to buffer the stream
            var copyStream = _backupProviders.Count != 0 ? new StreamReadAndCopy(stream) : null;
            var readCounter = new StreamCounter(copyStream ?? stream);

            var (origLen, storedLen, wrote) = await prov.WriteBlobStreamAsync(blob, readCounter, lengthHint, providerParams, cancellationToken);
            _indexDriver.AddAsyncRollbackAction($"deleting blob {blob.Guid} at index '{blob.Index}' due to rollback", async () => await prov.DeleteBlobAsync(blob));

            foreach (var bp in _backupProviders)
            {
                copyStream!.CopyOfStream.Position = 0;
                await bp.Value.WriteBlobStreamAsync(blob, new StreamCounter(copyStream!.CopyOfStream), lengthHint, providerParams, cancellationToken);
                _indexDriver.AddAsyncRollbackAction($"deleting blob {blob.Guid} at index '{blob.Index}' on provider {bp.Value.Name} due to rollback", async () => await bp.Value.DeleteBlobAsync(blob));
            }

            await _indexDriver.CreateBlobStreamAsync(
                _defaultWriter,
                blob,
                wrote ? null : readCounter,
                origLen,
                storedLen,
                cancellationToken
            );

            if (!wrote)
            {
                // if we didn't write (due to writing data to index instead), we wrote 0s to the size, update it now that we know the sizes
                // note streaming doesn't support any manipulation of the stream, so stored == original size
                await _indexDriver.UpdateBlobSizesAsync(blob.Guid!.Value, readCounter.BytesRead, readCounter.BytesRead, cancellationToken);
            }
        }

        return blob.Guid!.Value;
    }

    private (StorageProviderBase, byte[]) ProcessWriteBlob(Blob blob, Func<Blob, string?>? indexStrategy, BlobFeatures? features)
    {
        ArgumentNullException.ThrowIfNull(blob);
        ArgumentNullException.ThrowIfNull(blob.Data);

        var prov = GetProvider(_defaultWriter);

        blob.Features = features ?? _defaultFeatures;
        log.Debug($"Processing blob data for write: {blob.Features}");

        byte[] data = EncryptAndCompress(blob);

        blob.Guid = Guid.NewGuid();
        blob.Index = prov.ConstructBlobIndex(blob, indexStrategy);

        return (prov, data);
    }

    private StorageProviderBase ProcessWriteBlobStream(Blob blob, Func<Blob, string?>? indexStrategy, BlobFeatures? features)
    {
        ArgumentNullException.ThrowIfNull(blob);

        var prov = GetProvider(_defaultWriter);

        blob.Features = features ?? _defaultFeatures;
        log.Debug($"Processing blob data for write: {blob.Features}");

        var okToStream = CanStreamWrite(blob.Features, prov);
        if (!okToStream.can)
            throw new NotSupportedException(okToStream.reason);

        blob.Guid = Guid.NewGuid();
        blob.Index = prov.ConstructBlobIndex(blob, indexStrategy);

        return prov;
    }

    private (bool can, string? reason) CanStreamWrite(BlobFeatures features, StorageProviderBase provider)
    {
        // for stream write, no funny business
        if (features.HasFlag(BlobFeatures.Compressed))
            return (false, "compression not supported for streams");
        if (features.HasFlag(BlobFeatures.Encrypted))
            return (false, "encryption not supported for streams");

        // if we're ok to stream but we're writing to the db and the db doesn't support it
        if (provider.WritesDataToIndex && !_indexDriver.SupportsStreamWrite)
            return (false, "database driver does not support streaming");

        return (true, null);
    }
    #endregion

    private byte[] EncryptAndCompress(Blob blob)
    {
        byte[] data = blob.Data!;
        if (blob.Features.HasFlag(BlobFeatures.Compressed))
            data = CompressionUtil.Compress(data);
        if (data != null && blob.Features.HasFlag(BlobFeatures.Encrypted))
            data = (_secureData ?? throw new InvalidOperationException("blob factory not configured with encryption capability")).Encrypt(data)!;
        return data!;
    }

    /// <inheritdoc cref="IStorage.DeleteBlobAsync(Guid, CancellationToken)"/>
    public async Task DeleteBlobAsync(Guid blobGuid, CancellationToken cancellationToken)
    {
        var origBlob = await _indexDriver.GetBlobByGUIDAsync(blobGuid, loadData: false, cancellationToken);
        if (origBlob == null)
            return;
        var prov = GetProvider(origBlob.Provider);

        // defer actual deletes to post commit in case this txn rolls back
        _indexDriver.AddAsyncPostCommitAction($"removing data for blob {blobGuid} from provider {origBlob.Provider}", async () => await prov.DeleteBlobAsync(origBlob));

        foreach (var bp in _backupProviders)
            _indexDriver.AddAsyncPostCommitAction($"removing data for blob {blobGuid} from backup provider {bp.Value.Name}", async () => await bp.Value.DeleteBlobAsync(origBlob));

        await _indexDriver.DeleteBlobAsync(blobGuid, cancellationToken);
    }

    /// <inheritdoc cref="IStorage.ReadBlobIndexAsync(Guid, CancellationToken)"/>
    public async Task<Blob?> ReadBlobIndexAsync(Guid blobGuid, CancellationToken cancellationToken)
        => await _indexDriver.GetBlobByGUIDAsync(blobGuid, loadData: false, cancellationToken);

    #region rewrite
    /// <inheritdoc cref="IStorage.RewriteBlobAsync(Blob, Func{Blob, string}, BlobFeatures?, IDictionary{string, object}, CancellationToken)"/>
    public async Task<Guid?> RewriteBlobAsync(Blob blob, Func<Blob, string?>? indexStrategy, BlobFeatures? features = null, IDictionary<string, object>? providerParams = null, CancellationToken cancellationToken = default)
    {
        ArgumentNullException.ThrowIfNull(blob);
        ArgumentNullException.ThrowIfNull(blob.Guid);
        ArgumentNullException.ThrowIfNull(blob.Data);

        var origBlob = await _indexDriver.GetBlobByGUIDAsync(blob.Guid!.Value, loadData: false, cancellationToken);
        if (origBlob == null)
        {
            // if we can't find the orig, fall back to creating a fresh one
            await CreateNewBlobAsync(blob, indexStrategy, features ?? _defaultFeatures, providerParams, cancellationToken);
            return blob.Guid;
        }

        var newProv = GetProvider(_defaultWriter);
        var oldProv = GetProvider(origBlob.Provider);

        blob.Features = features ?? _defaultFeatures;

        // if the provider isn't changing and the provider keeps orignal indexes...
        if (origBlob.Provider == _defaultWriter && newProv.RewriteKeepsOriginalIndex)
            blob.Index = origBlob.Index;
        else
            blob.Index = newProv.ConstructBlobIndex(blob, indexStrategy);

        byte[] data = EncryptAndCompress(blob);

        if (newProv.WritesDataToIndex)
        {
            await _indexDriver.UpdateBlobAsync(
                _defaultWriter,
                blob.Guid.Value,
                blob,
                data,
                false,
                blob.Data!.LongLength,
                data.LongLength,
                cancellationToken);

            await newProv.WriteBlobAsync(blob, data, providerParams, cancellationToken);

            foreach (var bp in _backupProviders)
            {
                await bp.Value.WriteBlobAsync(blob, data, providerParams, cancellationToken);
                _indexDriver.AddAsyncRollbackAction($"deleting blob {blob.Guid} at index '{blob.Index}' in provider {bp.Value.Name} due to rollback", async () => await bp.Value.DeleteBlobAsync(blob));
            }

            return null;
        }
        else
        {
            _indexDriver.AddAsyncPostCommitAction($"due to replacement, removing data for blob {origBlob.Guid} from provider {origBlob.Provider}", async () => await oldProv.DeleteBlobAsync(origBlob));
            await _indexDriver.DeleteBlobAsync(origBlob.Guid!.Value, cancellationToken);

            blob.Guid = Guid.NewGuid();

            await _indexDriver.CreateBlobAsync(
                _defaultWriter,
                blob,
                null,
                blob.Data!.LongLength,
                data.LongLength,
                cancellationToken);

            await newProv.WriteBlobAsync(blob, data, providerParams, cancellationToken);
            _indexDriver.AddAsyncRollbackAction($"deleting blob {blob.Guid} at index '{blob.Index}' due to rollback", async () => await newProv.DeleteBlobAsync(blob));
  
            foreach (var bp in _backupProviders)
            {
                _indexDriver.AddAsyncPostCommitAction($"due to replacement, removing data for blob {origBlob.Guid} from provider {bp.Value.Name}", async () => await bp.Value.DeleteBlobAsync(origBlob));
                await bp.Value.WriteBlobAsync(blob, data, providerParams, cancellationToken);
                _indexDriver.AddAsyncRollbackAction($"deleting blob {blob.Guid} at index '{blob.Index}' in provider {bp.Value.Name} due to rollback", async () => await bp.Value.DeleteBlobAsync(blob));
            }

            return blob.Guid;
        }
    }

    /// <inheritdoc cref="IStorage.RewriteBlobAsync(Blob, Stream, long, Func{Blob, string}, BlobFeatures?, IDictionary{string, object}, CancellationToken)"/>
    public async Task<Guid?> RewriteBlobAsync(Blob blob, Stream stream, long lengthHint, Func<Blob, string?>? indexStrategy, BlobFeatures? features = null, IDictionary<string, object>? providerParams = null, CancellationToken cancellationToken = default)
    {
        ArgumentNullException.ThrowIfNull(blob);
        ArgumentNullException.ThrowIfNull(blob.Guid);
        ArgumentNullException.ThrowIfNull(stream);

        var origBlob = await _indexDriver.GetBlobByGUIDAsync(blob.Guid!.Value, loadData: false, cancellationToken);
        if (origBlob == null)
        {
            // if we can't find the orig, fall back to creating a fresh one
            await CreateNewBlobAsync(blob, stream, lengthHint, indexStrategy, features ?? _defaultFeatures, providerParams, cancellationToken);
            return blob.Guid;
        }

        var newProv = GetProvider(_defaultWriter);
        var oldProv = GetProvider(origBlob.Provider);

        blob.Features = features ?? _defaultFeatures;

        // if the provider isn't changing and the provider keeps orignal indexes...
        if (origBlob.Provider == _defaultWriter && newProv.RewriteKeepsOriginalIndex)
            blob.Index = origBlob.Index;
        else
            blob.Index = newProv.ConstructBlobIndex(blob, indexStrategy);

        var okToStream = CanStreamWrite(blob.Features, newProv);
        if (!okToStream.can)
            throw new NotSupportedException(okToStream.reason);

        // if we will write to backup providers, we need to buffer the stream
        var copyStream = _backupProviders.Count != 0 ? new StreamReadAndCopy(stream) : null;
        var readCounter = new StreamCounter(copyStream ?? stream);

        if (newProv.WritesDataToIndex)
        {
            await _indexDriver.UpdateBlobAsync(
                _defaultWriter,
                blob.Guid.Value,
                blob,
                readCounter,
                0,
                0,
                cancellationToken);

            // write copied stream to backup providers
            foreach (var bp in _backupProviders)
            {
                copyStream!.CopyOfStream.Position = 0;
                await bp.Value.WriteBlobStreamAsync(blob, new StreamCounter(copyStream!.CopyOfStream), lengthHint, providerParams, cancellationToken);
                _indexDriver.AddAsyncRollbackAction($"deleting blob {blob.Guid} at index '{blob.Index}' in provider {bp.Value.Name} due to rollback", async () => await bp.Value.DeleteBlobAsync(blob));
            }

            await _indexDriver.UpdateBlobSizesAsync(blob.Guid.Value, readCounter.BytesRead, readCounter.BytesRead, cancellationToken);
            return null;
        }
        else
        {
            _indexDriver.AddAsyncPostCommitAction($"due to replacement, removing data for blob {origBlob.Guid} from provider {origBlob.Provider}", async () => await oldProv.DeleteBlobAsync(origBlob));
            await _indexDriver.DeleteBlobAsync(origBlob.Guid!.Value, cancellationToken);

            blob.Guid = Guid.NewGuid();

            await _indexDriver.CreateBlobAsync(
                _defaultWriter,
                blob,
                null,
                0,
                0,
                cancellationToken);

            _indexDriver.AddAsyncRollbackAction($"deleting blob {blob.Guid} at index '{blob.Index}' due to rollback", async () => await newProv.DeleteBlobAsync(blob));
            await newProv.WriteBlobStreamAsync(blob, readCounter, lengthHint, providerParams, cancellationToken);

            // write copied stream to backup providers
            foreach (var bp in _backupProviders)
            {
                copyStream!.CopyOfStream.Position = 0;
                _indexDriver.AddAsyncPostCommitAction($"due to replacement, removing data for blob {origBlob.Guid} from provider {bp.Value.Name}", async () => await bp.Value.DeleteBlobAsync(origBlob));
                await bp.Value.WriteBlobStreamAsync(blob, new StreamCounter(copyStream!.CopyOfStream), lengthHint, providerParams, cancellationToken);
                _indexDriver.AddAsyncRollbackAction($"deleting blob {blob.Guid} at index '{blob.Index}' on provider {bp.Value.Name} due to rollback", async () => await bp.Value.DeleteBlobAsync(blob));
            }

            // note streaming doesn't support any manipulation of the stream, so stored == original size
            await _indexDriver.UpdateBlobSizesAsync(blob.Guid.Value, readCounter.BytesRead, readCounter.BytesRead, cancellationToken);

            return blob.Guid;
        }
    }
    #endregion

    /// <inheritdoc cref="IStorage.TransferBlobAsync(Guid, string, bool, Func{Blob, string?}?, BlobFeatures?, IDictionary{string, object}?, CancellationToken)"/>
    public async Task TransferBlobAsync(Guid blobGuid, string targetProviderKey, bool retainOriginal = false, Func<Blob, string?>? indexStrategy = null, BlobFeatures? features = null, IDictionary<string, object>? providerParams = null, CancellationToken cancellationToken = default)
    {
        // if we can't find the orig, we have to fail
        var origBlob = await _indexDriver.GetBlobByGUIDAsync(blobGuid, loadData: false, cancellationToken)
            ?? throw new LogicalNotFoundException($"Could not find original blob {blobGuid}");

        // if we aren't moving, no action necessary
        if (targetProviderKey == origBlob.Provider)
            return;

        var newProv = GetProvider(targetProviderKey);
        var oldProv = GetProvider(origBlob.Provider);

        /*
                source      target
           DB     Y           Y         simple read/write (not possible with current model)
           DB     N           Y         read, defer delete, simple write
           DB     Y           N         simple read, clear data, write, defer failure delete
           DB     N           N         read, defer delete, write, defer failure delete

                    src   tgt
           stream?   Y     Y      read stream, write stream
           stream?   N     Y      read to memory stream, write stream
           stream?   Y     N      read to buffer, write buffer
           stream?   N     N      read to buffer, write buffer

        */

        if (oldProv.WritesDataToIndex && newProv.WritesDataToIndex)
        {
            // move to/from the db, we only support a single index so there is nothing to do
            return;
        }

        var newBlob = Mapper.Map<Blob, Blob>(origBlob);
        newBlob.Provider = targetProviderKey;
        newBlob.Index = newProv.ConstructBlobIndex(newBlob, indexStrategy);
        newBlob.Features = features ?? _defaultFeatures;

        var okToStreamRead = CanStreamRead(origBlob.Features, oldProv);
        var okToStreamWrite = CanStreamWrite(newBlob.Features, newProv);

        if (okToStreamWrite.can)
        {
            Stream? readStream = null;
            if (okToStreamRead.can)
            {
                // both can stream, so stream from reader to writer
                (_, readStream) = await ReadBlobStreamAsync(blobGuid, cancellationToken);
                if (readStream == null)
                    throw new LogicalNotFoundException($"Could not find read original blob {blobGuid} from provider");
            }
            else
            {
                // only writer can stream, read bytes and write stream
                var origBlobData = await ReadBlobAsync(blobGuid, cancellationToken);
                if (origBlobData?.Data == null)
                    throw new LogicalNotFoundException($"Could not find read original blob {blobGuid} from provider");
                readStream = new MemoryStream(origBlobData.Data);
            }
            using (readStream)
            {
                var readCounter = new StreamCounter(readStream);
                (var origLen, var storedLen, var wrote) = await newProv.WriteBlobStreamAsync(newBlob, readCounter, origBlob.BlobSize, providerParams, cancellationToken);

                // streaming doesn't support any manipulation of the stream, so stored == original size
                if (wrote)
                    await _indexDriver.UpdateBlobAsync(targetProviderKey, blobGuid, newBlob, null, !retainOriginal, origLen, storedLen, cancellationToken);
                else
                    await _indexDriver.UpdateBlobAsync(targetProviderKey, blobGuid, newBlob, readCounter, origBlob.BlobSize, origBlob.BlobSize, cancellationToken);
            }
        }
        else
        {
            // writer cannot stream, so no benefit to using a read stream even if we can
            var origBlobData = await ReadBlobAsync(blobGuid, cancellationToken);
            if (origBlobData?.Data == null)
                throw new LogicalNotFoundException($"Could not find read original blob {blobGuid} from provider");

            // writing encryption and compression only support byte arrays
            newBlob.Data = origBlobData.Data;
            byte[] data = EncryptAndCompress(newBlob);
            await newProv.WriteBlobAsync(newBlob, data, providerParams, cancellationToken);

            bool setToNull = false;
            byte[]? indexBytes = null;

            if (oldProv.WritesDataToIndex)
            {
                setToNull = !retainOriginal;
                indexBytes = null;
            }
            else if (newProv.WritesDataToIndex)
            {
                indexBytes = data;
            }

            await _indexDriver.UpdateBlobAsync(targetProviderKey, blobGuid, newBlob, indexBytes, setToNull, newBlob.Data.LongLength, data.LongLength, cancellationToken);
        }

        if (!retainOriginal)
        {
            _indexDriver.AddAsyncPostCommitAction($"due to transfer, removing data for blob {origBlob.Guid} from provider {origBlob.Provider}", async () => await oldProv.DeleteBlobAsync(origBlob));
        }

        _indexDriver.AddAsyncRollbackAction($"deleting blob {blobGuid} in target provider at index '{newBlob.Index}' due to rollback", async () => await newProv.DeleteBlobAsync(newBlob));
    }

    #region url references
    /// <inheritdoc cref="IStorage.GetUrlReferenceAsync(Guid, CancellationToken)"/>
    public async Task<Uri?> GetUrlReferenceAsync(Guid blobGuid, CancellationToken cancellationToken)
    {
        var b = await _indexDriver.GetBlobByGUIDAsync(blobGuid, loadData: false, cancellationToken);
        if (b == null)
            return null;

        return await GetProvider(b.Provider).GetUrlReferenceAsync(b, cancellationToken);
    }

    /// <inheritdoc cref="IStorage.GetTemporaryUrlReferenceAsync(Guid, TimeSpan, CancellationToken)"/>
    public async Task<Uri?> GetTemporaryUrlReferenceAsync(Guid blobGuid, TimeSpan duration, CancellationToken cancellationToken)
    {
        var b = await _indexDriver.GetBlobByGUIDAsync(blobGuid, loadData: false, cancellationToken);
        if (b == null)
            return null;

        return await GetProvider(b.Provider).GetTemporaryUrlReferenceAsync(b, duration, cancellationToken);
    }
    #endregion
}
