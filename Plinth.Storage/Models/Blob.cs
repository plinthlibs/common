﻿namespace Plinth.Storage.Models;

/// <summary>
/// Base model for Blobs
/// </summary>
public class Blob
{
    /// <summary>
    /// Unique ID for the blob
    /// </summary>
    public Guid? Guid { get; set; }

    /// <summary>
    /// Blob name (typically a file name)
    /// </summary>
    public string Name { get; set; } = null!;

    /// <summary>
    /// An alternate key for indexing the blob (e.g. path in file system)
    /// </summary>
    public string? Index { get; set; }

    /// <summary>
    /// Provider ID for where the blob is stored
    /// </summary>
    public string Provider { get; set; } = null!;

    /// <summary>
    /// Optional mime type for the blob content
    /// </summary>
    public string MimeType { get; set; } = null!;

    /// <summary>
    /// Blob content
    /// </summary>
    public byte[]? Data { get; set; }

    /// <summary>
    /// Length of the blob content
    /// </summary>
    public long BlobSize { get; set; }

    /// <summary>
    /// Length of the content stored on the physical provider
    /// </summary>
    public long StoredSize { get; set; }

    /// <summary>
    /// Features of this Blob
    /// </summary>
    public BlobFeatures Features { get; set; }
}
