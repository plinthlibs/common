﻿namespace Plinth.Storage.Models;

/// <summary>
/// Blob Features that can be enabled on a blob
/// </summary>
[Flags]
public enum BlobFeatures
{
    /// <summary>Nothing special</summary>
    None       = 0,
    /// <summary>Encrypt the blob data in the storage provider (last step)</summary>
    Encrypted  = 1 << 0,
    /// <summary>Compress the blob data in the storage provider (first step)</summary>
    Compressed = 1 << 1
}
