using Plinth.Storage.Models;

namespace Plinth.Storage;

/// <summary>
/// Interface for implementing a storage index driver
/// </summary>
public interface IStorageIndexDriver
{
    /// <summary>
    /// Indicates if this storage index driver can stream data to the DB
    /// </summary>
    bool SupportsStreamWrite { get; }

    /// <summary>
    /// Indicates if this storage index driver can stream data from the DB
    /// </summary>
    bool SupportsStreamRead { get; }

    /// <summary>
    /// Retrieves the blob with the specified guid
    /// </summary>
    /// <param name="guid"></param>
    /// <param name="loadData">if using database storage, true will also load the data (left as encrypted)</param>
    /// <param name="cancellationToken"></param>
    /// <returns>a blob model object or null if the blob with the specified guid does not exist</returns>
    Task<Blob?> GetBlobByGUIDAsync(Guid guid, bool loadData = false, CancellationToken cancellationToken = default);

    /// <summary>
    /// Retrieves the blob with the specified guid and a stream to the data (if non null)
    /// </summary>
    /// <param name="guid"></param>
    /// <param name="loadData">if using database storage, true will also load the data (left as encrypted)</param>
    /// <param name="cancellationToken"></param>
    /// <returns>a blob model object or null if the blob with the specified guid does not exist</returns>
    Task<(Blob? blob, Stream? stream)> GetBlobStreamByGUIDAsync(Guid guid, bool loadData = false, CancellationToken cancellationToken = default);

    /// <summary>
    /// Async Creates a new blob
    /// </summary>
    /// <param name="provider">provider id</param>
    /// <param name="blob">blob metadata</param>
    /// <param name="blobData">encrypted data for Database Storage, null otherwise</param>
    /// <param name="origSize">size of the original data in bytes</param>
    /// <param name="storedSize">size of the data written to storage</param>
    /// <param name="cancellationToken"></param>
    Task CreateBlobAsync(string provider, Blob blob, byte[]? blobData, long origSize, long storedSize, CancellationToken cancellationToken = default);

    /// <summary>
    /// Async Creates a new blob from a stream
    /// </summary>
    /// <param name="provider">provider id</param>
    /// <param name="blob">blob metadata</param>
    /// <param name="stream">data for Database Storage, null otherwise</param>
    /// <param name="origSize">size of the original data in bytes</param>
    /// <param name="storedSize">size of the data written to storage</param>
    /// <param name="cancellationToken"></param>
    Task CreateBlobStreamAsync(string provider, Blob blob, Stream? stream, long origSize, long storedSize, CancellationToken cancellationToken = default);

    /// <summary>
    /// Async Updates a blob
    /// </summary>
    /// <param name="provider">provider id</param>
    /// <param name="guid">blob id</param>
    /// <param name="blob">blob metadata</param>
    /// <param name="blobData">encrypted data for Database Storage, null otherwise</param>
    /// <param name="setDataToNull">if true and blobData is null, the column will be set to null</param>
    /// <param name="origSize">size of the original data in bytes</param>
    /// <param name="storedSize">size of the data written to storage</param>
    /// <param name="cancellationToken"></param>
    Task UpdateBlobAsync(string? provider, Guid guid, Blob blob, byte[]? blobData, bool setDataToNull, long? origSize, long? storedSize, CancellationToken cancellationToken = default);

    /// <summary>
    /// Async Updates a blob with stream data
    /// </summary>
    /// <param name="provider">provider id</param>
    /// <param name="guid">blob id</param>
    /// <param name="blob">blob metadata</param>
    /// <param name="stream">data for blob</param>
    /// <param name="origSize">size of the original data in bytes</param>
    /// <param name="storedSize">size of the data written to storage</param>
    /// <param name="cancellationToken"></param>
    Task UpdateBlobAsync(string? provider, Guid guid, Blob blob, Stream? stream, long? origSize, long? storedSize, CancellationToken cancellationToken = default);

    /// <summary>
    /// Async Updates a blob index sizes, used for streaming
    /// </summary>
    /// <param name="guid">blob id</param>
    /// <param name="origSize">size of the original data in bytes</param>
    /// <param name="storedSize">size of the data written to storage</param>
    /// <param name="cancellationToken"></param>
    Task UpdateBlobSizesAsync(Guid guid, long origSize, long storedSize, CancellationToken cancellationToken = default);

    /// <summary>
    /// Async deletes a blob from the index
    /// </summary>
    /// <param name="guid">blob id</param>
    /// <param name="cancellationToken"></param>
    Task DeleteBlobAsync(Guid guid, CancellationToken cancellationToken = default);

    /// <summary>
    /// Record an action for execution after commit
    /// </summary>
    /// <param name="desc"></param>
    /// <param name="postCommit"></param>
    void AddPostCommitAction(string desc, Action postCommit);

    /// <summary>
    /// Record an async action for execution after commit
    /// </summary>
    /// <param name="desc"></param>
    /// <param name="postCommitAsync"></param>
    void AddAsyncPostCommitAction(string desc, Func<Task> postCommitAsync);

    /// <summary>
    /// Record an action for execution after rollback
    /// </summary>
    /// <param name="desc"></param>
    /// <param name="onRollback"></param>
    void AddRollbackAction(string desc, Action onRollback);

    /// <summary>
    /// Record an async action for execution after rollback
    /// </summary>
    /// <param name="desc"></param>
    /// <param name="onRollbackAsync"></param>
    void AddAsyncRollbackAction(string desc, Func<Task> onRollbackAsync);
}
