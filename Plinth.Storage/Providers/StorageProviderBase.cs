using Plinth.Storage.Models;

namespace Plinth.Storage.Providers;

/// <summary>
/// Base class for underlying blob storage providers
/// </summary>
internal abstract class StorageProviderBase
{
    /// <summary>
    /// Base Constructor for Blob Storage Providers
    /// </summary>
    private protected StorageProviderBase()
    {
    }

    /// <summary>
    /// A name that will be logged during certain optations
    /// </summary>
    public abstract string Name { get; }

    /// <summary>
    /// A provider should return true if the data written is part of DB transaction,
    /// false if the write occurs in an external system
    /// </summary>
    public abstract bool WritesDataToIndex { get; }

    /// <summary>
    /// By default, when rewriting a blob, the original index value will be used
    /// </summary>
    public virtual bool RewriteKeepsOriginalIndex { get; } = true;

    /// <summary>
    /// Load the bytes from the physical provider
    /// </summary>
    /// <param name="blob">the data from the index</param>
    /// <param name="cancellationToken"></param>
    /// <returns>the bytes from the physical storage</returns>
    public abstract Task<byte[]?> ReadBlobDataAsync(Blob blob, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get a stream to the physical provider
    /// </summary>
    /// <param name="blob">the data from the index</param>
    /// <param name="dbStream">stream with the data from db, can be null</param>
    /// <param name="cancellationToken"></param>
    /// <returns>a stream to read from the physical storage</returns>
    public abstract Task<Stream?> ReadBlobDataStreamAsync(Blob blob, Stream? dbStream, CancellationToken cancellationToken = default);

    /// <summary>
    /// Construct the index for this blob
    /// </summary>
    /// <param name="blob">the data from the user</param>
    /// <param name="indexStrategy">optional strategy for custom indexes</param>
    /// <returns>the index string</returns>
    public abstract string? ConstructBlobIndex(Blob blob, Func<Blob, string?>? indexStrategy);

    /// <summary>
    /// Write blob data to the physical provider
    /// </summary>
    /// <param name="blob">the data going to the index</param>
    /// <param name="data">the data ready to write</param>
    /// <param name="providerParams">optional custom provider parameters</param>
    /// <param name="cancellationToken"></param>
    public abstract Task WriteBlobAsync(Blob blob, byte[] data, IDictionary<string, object>? providerParams, CancellationToken cancellationToken = default);

    /// <summary>
    /// Async Write blob stream to the physical provider
    /// </summary>
    /// <param name="blob">the data going to the index</param>
    /// <param name="stream">a counter for determining the # bytes in the original stream</param>
    /// <param name="lengthHint">a hint for the length, some providers require the length to be set, -1 for unknown</param>
    /// <param name="providerParams">optional custom provider parameters</param>
    /// <param name="cancellationToken"></param>
    /// <returns>a tuple of the data length and the stored length (can be diff due to compression/encryption), and true if this wrote data</returns>
    public abstract Task<(long origLen, long storedLen, bool wrote)> WriteBlobStreamAsync(Blob blob, StreamCounter stream, long lengthHint, IDictionary<string, object>? providerParams, CancellationToken cancellationToken = default);

    /// <summary>
    /// Delete the blob data on the physical provider
    /// </summary>
    /// <param name="blob">the data from the index</param>
    /// <param name="cancellationToken"></param>
    public abstract Task DeleteBlobAsync(Blob blob, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get a Url reference
    /// </summary>
    /// <remarks>by default, providers do not support this</remarks>
    /// <param name="blob">blob index data</param>
    /// <param name="cancellationToken"></param>
    /// <returns>a Uri which can access the blob data, or null if the provider doesn't support url references</returns>
    public virtual Task<Uri?> GetUrlReferenceAsync(Blob blob, CancellationToken cancellationToken = default) => Task.FromResult((Uri?)null);

    /// <summary>
    /// Get a temporary Url Reference
    /// </summary>
    /// <remarks>by default, providers do not support this</remarks>
    /// <param name="blob">blob index data</param>
    /// <param name="duration">how long the url reference should be valid</param>
    /// <param name="cancellationToken"></param>
    /// <returns>a Uri which can access the blob data, or null if the provider doesn't support url references</returns>
    public virtual Task<Uri?> GetTemporaryUrlReferenceAsync(Blob blob, TimeSpan duration, CancellationToken cancellationToken = default) => Task.FromResult((Uri?)null);
}
