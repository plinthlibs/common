using Plinth.Storage.Exceptions;

namespace Plinth.Storage.Providers;

class ValidityCheckReadStream : Stream
{
    private Stream _inner;
    private readonly long _expected;

    private long _bytesWritten = 0;
    private bool _readCalled = false;

    public ValidityCheckReadStream(Stream innerStream, long expectedLength)
    {
        _inner = innerStream ?? throw new ArgumentNullException(nameof(innerStream));
        _expected = expectedLength;

        if (expectedLength < 0)
            throw new ArgumentOutOfRangeException(nameof(expectedLength), "must be >= 0");
    }

    public override bool CanRead => _inner.CanRead;

    public override bool CanSeek => false;

    public override bool CanWrite => false;

    public override long Length => _inner.Length;

    public override long Position { get => _inner.Position; set => throw new NotSupportedException(); }

    public override void Flush() => _inner.Flush();

    public override Task FlushAsync(CancellationToken cancellationToken) => _inner.FlushAsync(cancellationToken);

    public override int Read(byte[] buffer, int offset, int count)
    {
        try
        {
            _readCalled = true;
            var bytes = _inner.Read(buffer, offset, count);
            if (bytes > 0)
                _bytesWritten += bytes;
            return bytes;
        }
        catch (InvalidDataException e)
        {
            throw new CorruptedBlobException("corrupted blob", e);
        }
    }

    public override async ValueTask<int> ReadAsync(Memory<byte> buffer, CancellationToken cancellationToken = default)
    {
        try
        {
            _readCalled = true;
            var bytes = await _inner.ReadAsync(buffer, cancellationToken);
            if (bytes > 0)
                _bytesWritten += bytes;
            return bytes;
        }
        catch (InvalidDataException e)
        {
            throw new CorruptedBlobException("corrupted blob", e);
        }
    }

    public override long Seek(long offset, SeekOrigin origin) => throw new NotSupportedException();

    public override void SetLength(long value) => throw new NotSupportedException();

    public override void Write(byte[] buffer, int offset, int count) => _inner.Write(buffer, offset, count);

    public override ValueTask WriteAsync(ReadOnlyMemory<byte> buffer, CancellationToken cancellationToken = default) => _inner.WriteAsync(buffer, cancellationToken);

    public override void Close()
    {
        base.Close();

        if (_readCalled && _bytesWritten != _expected)
            throw new CorruptedBlobException($"expected {_expected} but got {_bytesWritten} from provider");
    }

    protected override void Dispose(bool disposing)
    {
        try
        {
            if (disposing)
                _inner?.Close();
            _inner = null!;
        }
        finally
        {
            base.Dispose(disposing);
        }
    }
}
