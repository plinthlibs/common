namespace Plinth.Storage.Providers;

class StreamReadAndCopy(Stream innerStream) : Stream
{
    private Stream _inner = innerStream ?? throw new ArgumentNullException(nameof(innerStream));

    public MemoryStream CopyOfStream { get; } = new MemoryStream();

    public override bool CanRead => _inner.CanRead;

    public override bool CanSeek => _inner.CanSeek;

    public override bool CanWrite => false;

    public override long Length => _inner.Length;

    public override long Position { get => _inner.Position; set => throw new NotSupportedException(); }

    public override void Flush() => _inner.Flush();

    public override Task FlushAsync(CancellationToken cancellationToken) => _inner.FlushAsync(cancellationToken);

    public override int Read(byte[] buffer, int offset, int count)
    {
        var bytes = _inner.Read(buffer, offset, count);
        if (bytes > 0)
            CopyOfStream.Write(buffer, offset, bytes);
        return bytes;
    }

    public override async ValueTask<int> ReadAsync(Memory<byte> buffer, CancellationToken cancellationToken = default)
    {
        var bytes = await _inner.ReadAsync(buffer, cancellationToken);
        if (bytes > 0)
            await CopyOfStream.WriteAsync(buffer[..bytes], cancellationToken);
        return bytes;
    }

    public override long Seek(long offset, SeekOrigin origin) => _inner.Seek(offset, origin);

    public override void SetLength(long value) => throw new NotSupportedException();

    public override void Write(byte[] buffer, int offset, int count) => throw new NotSupportedException();

    public override ValueTask WriteAsync(ReadOnlyMemory<byte> buffer, CancellationToken cancellationToken = default) => throw new NotSupportedException();

    protected override void Dispose(bool disposing)
    {
        try
        {
            if (disposing)
            {
                _inner?.Close();
                CopyOfStream.Position = 0;
            }
            _inner = null!;
        }
        finally
        {
            base.Dispose(disposing);
        }
    }
}
