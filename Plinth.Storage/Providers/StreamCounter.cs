namespace Plinth.Storage.Providers;

class StreamCounter(Stream innerStream) : Stream
{
    private Stream _inner = innerStream ?? throw new ArgumentNullException(nameof(innerStream));

    public long BytesRead { get; private set; } = 0;
    public bool ReadCalled { get; private set; } = false;
    public long BytesWritten { get; private set; } = 0;
    public bool WriteCalled { get; private set; } = false;

    public override bool CanRead => _inner.CanRead;

    public override bool CanSeek => false;

    public override bool CanWrite => _inner.CanWrite;

    public override long Length => _inner.Length;

    public override long Position { get => _inner.Position; set => throw new NotSupportedException(); }

    public override void Flush() => _inner.Flush();

    public override Task FlushAsync(CancellationToken cancellationToken) => _inner.FlushAsync(cancellationToken);

    public override int Read(byte[] buffer, int offset, int count)
    {
        ReadCalled = true;
        var bytes = _inner.Read(buffer, offset, count);
        if (bytes > 0)
            BytesRead += bytes;
        return bytes;
    }

    public override async ValueTask<int> ReadAsync(Memory<byte> buffer, CancellationToken cancellationToken = default)
    {
        ReadCalled = true;
        var bytes = await _inner.ReadAsync(buffer, cancellationToken);
        if (bytes > 0)
            BytesRead += bytes;
        return bytes;
    }

    public override long Seek(long offset, SeekOrigin origin) => _inner.Seek(offset, origin);

    public override void SetLength(long value) => throw new NotSupportedException();

    public override void Write(byte[] buffer, int offset, int count)
    {
        WriteCalled = true;
        _inner.Write(buffer, offset, count);
        BytesWritten += count;
    }

    public override async ValueTask WriteAsync(ReadOnlyMemory<byte> buffer, CancellationToken cancellationToken = default)
    {
        WriteCalled = true;
        BytesWritten += buffer.Length;
        await _inner.WriteAsync(buffer, cancellationToken);
    }

    protected override void Dispose(bool disposing)
    {
        try
        {
            if (disposing)
                _inner?.Close();
            _inner = null!;
        }
        finally
        {
            base.Dispose(disposing);
        }
    }
}
