using Plinth.Storage.Models;

namespace Plinth.Storage.Providers.FileSystem;

/// <summary>
/// Default index strategy for when the index strategy function is null
/// </summary>
public enum DefaultIndexStrategy
{
    /// <summary>The default, which is all in root directory {basePath}/{guid}</summary>
    Flat = 0,

    /// <summary>By Date of blob creation, {basePath}/YYYY/MM/DD/{guid}</summary>
    ByDate,

    /// <summary>By DateTime of blob creation (includes hour), {basePath}/YYYY/MM/DD/HH{guid}</summary>
    ByDateTime
}

/// <summary>
/// Customizations and settings for File System blob storage
/// </summary>
public class FileSystemSettings
{
    /// <summary>
    /// Root directory of where to write blobs
    /// </summary>
    public string BasePath { get; set; } = null!;

    /// <summary>
    /// Index Strategy to use when indexStrategy (parameter) or CustomIndexStrategy is null when creating a blob
    /// </summary>
    public DefaultIndexStrategy DefaultIndexStrategy { get; set; }

    /// <summary>
    /// An optional custom index creation strategy
    /// </summary>
    /// <remarks>Input is a Blob, output is the file sytem path that the blob will be writen {BasePath}/{Index}/{Guid}</remarks>
    public Func<Blob, string?>? CustomIndexStrategy { get; set; }
}
