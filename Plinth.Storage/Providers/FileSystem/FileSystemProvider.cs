using Microsoft.Extensions.Logging;
using Plinth.Storage.Models;
using Plinth.Common.Utils;

namespace Plinth.Storage.Providers.FileSystem;

/// <summary>
/// Provides blob storage using the file system for the data
/// </summary>
internal class FileSystemProvider : StorageProviderBase
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private readonly FileSystemSettings _fsSettings;

    public override string Name => $"FS:{_fsSettings.BasePath}";

    /// <summary>
    /// Construct a FileSystemStorage instance for Blobs stored on a file system (best for prod)
    /// </summary>
    /// <remarks>Uses the 'Index' column to store the relative file path</remarks>
    /// <param name="fsSettings">file system storage settings</param>
    public FileSystemProvider(FileSystemSettings fsSettings)
    {
        if (string.IsNullOrEmpty(fsSettings.BasePath)) throw new ArgumentNullException(nameof(fsSettings), "BasePath is null or empty");

        _fsSettings = fsSettings;

        Directory.CreateDirectory(fsSettings.BasePath);
    }

    /// <summary>
    /// The FS provider writes data to disk, which is not transactional
    /// </summary>
    public override bool WritesDataToIndex => false;

    /// <summary>
    /// Load the bytes from the FS
    /// </summary>
    public override async Task<byte[]?> ReadBlobDataAsync(Blob blob, CancellationToken cancellationToken)
    {
        using (new TimeUtil.TimeLogger("ReadBlobAsync(FileSystem)", log))
        {
            var blobPath = PathForBlobGuid(blob);
            var fileData = await File.ReadAllBytesAsync(blobPath, cancellationToken);
            log.Debug($"read {fileData.Length} bytes from file: {blobPath}");
            return fileData;
        }
    }

    public override Task<Stream?> ReadBlobDataStreamAsync(Blob blob, Stream? dbStream, CancellationToken cancellationToken)
    {
        var blobPath = PathForBlobGuid(blob);
        return Task.FromResult<Stream?>(OpenReadAsync(blobPath));
    }

    private static FileStream OpenReadAsync(string path)
        => new(path, FileMode.Open, FileAccess.Read, FileShare.Read, 0x10000, FileOptions.Asynchronous);

    private string PathForBlobGuid(Blob blob)
    {
        string basePath = _fsSettings.BasePath;
        string fn = blob.Guid!.Value.ToString().ToLowerInvariant();
        if (string.IsNullOrEmpty(blob.Index))
            return Path.Combine(basePath, fn);
        return Path.Combine(basePath, blob.Index, fn);
    }

    /// <summary>
    /// the index field is the file path
    /// </summary>
    public override string? ConstructBlobIndex(Blob blob, Func<Blob, string?>? indexStrategy)
    {
        if (indexStrategy != null)
            return indexStrategy.Invoke(blob);

        if (_fsSettings.CustomIndexStrategy != null)
            return _fsSettings.CustomIndexStrategy.Invoke(blob);

        switch (_fsSettings.DefaultIndexStrategy)
        {
            case DefaultIndexStrategy.Flat:
                return string.Empty;

            case DefaultIndexStrategy.ByDate:
            {
                var d = DateTime.UtcNow;
                return Path.Combine(d.Year.ToString("D2"), d.Month.ToString("D2"), d.Day.ToString("D2"));
            }

            case DefaultIndexStrategy.ByDateTime:
            {
                var d = DateTime.UtcNow;
                return Path.Combine(d.Year.ToString("D2"), d.Month.ToString("D2"), d.Day.ToString("D2"), d.Hour.ToString("D2"));
            }

            default:
                throw new NotSupportedException(_fsSettings.DefaultIndexStrategy.ToString());
        }
    }

    /// <summary>
    /// Write the data to a file
    /// </summary>
    public override async Task WriteBlobAsync(Blob blob, byte[] data, IDictionary<string, object>? providerParams, CancellationToken cancellationToken)
    {
        using (new TimeUtil.TimeLogger("WriteBlobAsync(FileSystem)", log))
        {
            await WriteToFSAsync(blob, data, cancellationToken);
        }
    }

    /// <summary>
    /// Write the data to a file
    /// </summary>
    public override async Task<(long origLen, long storedLen, bool wrote)> WriteBlobStreamAsync(Blob blob, StreamCounter stream, long lengthHint, IDictionary<string, object>? providerParams, CancellationToken cancellationToken)
    {
        using (new TimeUtil.TimeLogger("WriteBlobStreamAsync(FileSystem)", log))
        {
            return await WriteToFSAsync(blob, stream, cancellationToken);
        }
    }

    private async Task WriteToFSAsync(Blob blob, byte[] fileData, CancellationToken cancellationToken)
    {
        string path = PathForBlobGuid(blob);
        new FileInfo(path).Directory?.Create();

        await File.WriteAllBytesAsync(path, fileData, cancellationToken);

        log.Debug($"wrote {fileData.Length} bytes to file: {path}");
    }

    private async Task<(long, long, bool)> WriteToFSAsync(Blob blob, StreamCounter stream, CancellationToken cancellationToken)
    {
        string path = PathForBlobGuid(blob);
        new FileInfo(path).Directory?.Create();

        using var fs = new StreamCounter(File.OpenWrite(path));
        await stream.CopyToAsync(fs, cancellationToken);

        long storedLen = fs.BytesWritten;
        long origLen = stream.BytesRead;
        log.Debug($"wrote {storedLen} bytes to file: {path}");
        return (origLen, storedLen, true);
    }

    /// <summary>
    /// Delete the file
    /// </summary>
    public override async Task DeleteBlobAsync(Blob blob, CancellationToken cancellationToken)
    {
        // this will safely fake an async delete
        await Task.Yield();

        var replacedFile = PathForBlobGuid(blob);
        log.Debug($"removing file {replacedFile}");
        File.Delete(replacedFile);
    }
}
