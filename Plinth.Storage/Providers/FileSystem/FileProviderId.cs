using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plinth.Storage.Providers.FileSystem;

/// <summary>
/// Provider Id Key for the File System provider
/// </summary>
public static class FileSystemProviderId
{
    /// <summary>
    /// Provider Id Key for the File System provider
    /// </summary>
    public static string Key => "FS";
}
