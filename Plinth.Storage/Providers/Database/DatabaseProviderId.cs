using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plinth.Storage.Providers.Database;

/// <summary>
/// Provider Id Key for the Database provider
/// </summary>
public static class DatabaseProviderId
{
    /// <summary>
    /// Provider Id Key for the Database provider
    /// </summary>
    public static string Key => "DB";
}
