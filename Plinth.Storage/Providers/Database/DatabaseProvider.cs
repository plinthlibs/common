using Plinth.Storage.Models;

namespace Plinth.Storage.Providers.Database;

/// <summary>
/// Provides blob storage using the database for the data
/// </summary>
internal class DatabaseProvider : StorageProviderBase
{
    public override string Name => "Database";

    /// <summary>
    /// Construct a Database Provider instance for Blobs in the DB (best for local dev and testing)
    /// </summary>
    public DatabaseProvider()
    {
    }

    /// <summary>
    /// The DB provider writes data to the DB, which is part of the txn
    /// </summary>
    public override bool WritesDataToIndex => true;

    /// <summary>
    /// Simply returns the data from the index
    /// </summary>
    public override Task<byte[]?> ReadBlobDataAsync(Blob blob, CancellationToken cancellationToken)
        => Task.FromResult(blob.Data);

    public override Task<Stream?> ReadBlobDataStreamAsync(Blob blob, Stream? dbStream, CancellationToken cancellationToken)
        => Task.FromResult(dbStream);

    /// <summary>
    /// DB does not use the Index field
    /// </summary>
    public override string? ConstructBlobIndex(Blob blob, Func<Blob, string?>? _) => null;

    /// <summary>
    /// DB provider writes data to blob index
    /// </summary>
    public override Task WriteBlobAsync(Blob blob, byte[] data, IDictionary<string, object>? providerParams, CancellationToken cancellationToken)
        => Task.CompletedTask;

    /// <summary>
    /// DB provider writes data to blob index
    /// </summary>
    public override Task<(long origLen, long storedLen, bool wrote)> WriteBlobStreamAsync(Blob blob, StreamCounter stream, long lengthHint, IDictionary<string, object>? providerParams, CancellationToken cancellationToken)
        => Task.FromResult((0L, 0L, false));

    /// <summary>
    /// No action to delete blob in DB outside of index
    /// </summary>
    public override Task DeleteBlobAsync(Blob blob, CancellationToken cancellationToken) => Task.CompletedTask;
}
