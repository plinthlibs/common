using Plinth.Storage.Exceptions;
using Plinth.Storage.Models;

namespace Plinth.Storage;

/// <summary>
/// Interface for the blob storage service
/// </summary>
public interface IStorage
{
    #region Read Blob
    /// <summary>
    /// Async Read a blob from the blob store, indexed by guid
    /// </summary>
    /// <param name="blobGuid">blob guid</param>
    /// <param name="cancellationToken"></param>
    /// <returns>blob with data, null if not found</returns>
    /// <exception cref="CorruptedBlobException">if the blob data was corrupted</exception>
    Task<Blob?> ReadBlobAsync(Guid blobGuid, CancellationToken cancellationToken = default);

    /// <summary>
    /// Async Read a blob from the blob store, indexed by guid, data coming back as a stream
    /// </summary>
    /// <param name="blobGuid">blob guid</param>
    /// <param name="cancellationToken"></param>
    /// <remarks>will attempt to stream directly from underlying storage</remarks>
    /// <returns>blob and stream, null if not found</returns>
    /// <exception cref="CorruptedBlobException">if the blob data was corrupted</exception>
    Task<(Blob? blob, Stream? stream)> ReadBlobStreamAsync(Guid blobGuid, CancellationToken cancellationToken = default);

    /// <summary>
    /// Async Get the blob index data
    /// </summary>
    /// <param name="blobGuid">blob guid</param>
    /// <param name="cancellationToken"></param>
    /// <returns>blob index, null if not found</returns>
    Task<Blob?> ReadBlobIndexAsync(Guid blobGuid, CancellationToken cancellationToken = default);
    #endregion

    #region Create/Replace
    /// <summary>
    /// Create a new blob in the blob store
    /// </summary>
    /// <param name="blob">blob containing the info and data</param>
    /// <param name="indexStrategy">optional strategy for determining index (not used by all providers)</param>
    /// <param name="features">optional override of provider default</param>
    /// <param name="providerParams">optional custom parameters for the underlying provider</param>
    /// <param name="cancellationToken"></param>
    /// <returns>new blob guid (also set in model)</returns>
    Task<Guid> CreateNewBlobAsync(Blob blob, Func<Blob, string?>? indexStrategy = null, BlobFeatures? features = null, IDictionary<string, object>? providerParams = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Create a new blob in the blob store, data comes from stream
    /// </summary>
    /// <param name="blob">blob containing the info and data</param>
    /// <param name="stream">data will be read from this stream</param>
    /// <param name="lengthHint">a hint for the length, some providers require the length to be set, -1 for unknown</param>
    /// <param name="indexStrategy">optional strategy for determining index (not used by all providers)</param>
    /// <param name="features">optional override of provider default</param>
    /// <param name="providerParams">optional custom parameters for the underlying provider</param>
    /// <param name="cancellationToken"></param>
    /// <remarks>will attempt to stream directly to underlying storage</remarks>
    /// <returns>new blob guid (also set in model)</returns>
    Task<Guid> CreateNewBlobAsync(Blob blob, Stream stream, long lengthHint = -1, Func<Blob, string?>? indexStrategy = null, BlobFeatures? features = null, IDictionary<string, object>? providerParams = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Rewrite a blob
    /// </summary>
    /// <remarks>
    /// <para>Some blob storage providers cannot rewrite a blob safely.</para>
    /// <para>If the providers cannot participate in the same transaction as the blob index, then it cannot 100% safely guarantee that both the index and the blob are
    /// either both written or both rolled back. Therefore, Rewrite blob may choose to create a new blob and return the Guid.</para>
    /// <para>Previous versions can be purged.</para>
    /// </remarks>
    /// <param name="blob">blob containing the info and data</param>
    /// <param name="indexStrategy">optional strategy for determining index (not used by all providers)</param>
    /// <param name="features">optional override of provider default</param>
    /// <param name="providerParams">optional custom parameters for the underlying provider</param>
    /// <param name="cancellationToken"></param>
    /// <returns>An optional guid. This will have a value if a new blob needed to be created, see remarks</returns>
    Task<Guid?> RewriteBlobAsync(Blob blob, Func<Blob, string?>? indexStrategy = null, BlobFeatures? features = null, IDictionary<string, object>? providerParams = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Rewrite a blob
    /// </summary>
    /// <remarks>
    /// <para>Some blob storage providers cannot rewrite a blob safely.</para>
    /// <para>If the providers cannot participate in the same transaction as the blob index, then it cannot 100% safely guarantee that both the index and the blob are
    /// either both written or both rolled back. Therefore, Rewrite blob may choose to create a new blob and return the Guid.</para>
    /// <para>Previous versions can be purged.</para>
    /// </remarks>
    /// <param name="blob">blob containing the info and data</param>
    /// <param name="stream">data will be read from this stream</param>
    /// <param name="lengthHint">a hint for the length, some providers require the length to be set, -1 for unknown</param>
    /// <param name="indexStrategy">optional strategy for determining index (not used by all providers)</param>
    /// <param name="features">optional override of provider default</param>
    /// <param name="providerParams">optional custom parameters for the underlying provider</param>
    /// <param name="cancellationToken"></param>
    /// <remarks>will attempt to stream directly to underlying storage</remarks>
    /// <returns>An optional guid. This will have a value if a new blob needed to be created, see remarks</returns>
    Task<Guid?> RewriteBlobAsync(Blob blob, Stream stream, long lengthHint = -1, Func<Blob, string?>? indexStrategy = null, BlobFeatures? features = null, IDictionary<string, object>? providerParams = null, CancellationToken cancellationToken = default);
    #endregion

    #region Transfer
    /// <summary>
    /// Async Transfer a blob from it's current primary provider to a new primary provider
    /// </summary>
    /// <param name="blobGuid">blob guid</param>
    /// <param name="targetProviderKey">key for the new provider, get from {ProviderName}Id.Key</param>
    /// <param name="retainOriginal">if true, will retain the blob bytes in the original location, false will remove it</param>
    /// <param name="indexStrategy">optional strategy for determining index (not used by all providers)</param>
    /// <param name="features">optional override of provider default</param>
    /// <param name="providerParams">optional custom parameters for the underlying provider</param>
    /// <param name="cancellationToken"></param>
    Task TransferBlobAsync(Guid blobGuid, string targetProviderKey, bool retainOriginal = false, Func<Blob, string?>? indexStrategy = null, BlobFeatures? features = null, IDictionary<string, object>? providerParams = null, CancellationToken cancellationToken = default);
    #endregion

    #region Delete Blob
    /// <summary>
    /// Async Remove a blob from the blob store
    /// </summary>
    /// <param name="blobGuid">blob guid</param>
    /// <param name="cancellationToken"></param>
    Task DeleteBlobAsync(Guid blobGuid, CancellationToken cancellationToken = default);
    #endregion

    #region Url References
    /// <summary>
    /// Get a Url reference
    /// </summary>
    /// <param name="blobGuid">blob guid</param>
    /// <param name="cancellationToken"></param>
    /// <returns>a Uri which can access the blob data, or null if the provider doesn't support url references</returns>
    Task<Uri?> GetUrlReferenceAsync(Guid blobGuid, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get a temporary Url Reference
    /// </summary>
    /// <param name="blobGuid">blob guid</param>
    /// <param name="duration">how long the url reference should be valid</param>
    /// <param name="cancellationToken"></param>
    /// <returns>a Uri which can access the blob data, or null if the provider doesn't support url references</returns>
    Task<Uri?> GetTemporaryUrlReferenceAsync(Guid blobGuid, TimeSpan duration, CancellationToken cancellationToken = default);
    #endregion
}
