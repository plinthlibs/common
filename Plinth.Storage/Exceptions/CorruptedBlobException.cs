namespace Plinth.Storage.Exceptions;

/// <summary>
/// Thrown when blob data is corrupted
/// </summary>
public class CorruptedBlobException : Exception
{
    /// <summary>
    /// Constructor
    /// </summary>
    public CorruptedBlobException()
    {
    }

    /// <summary>
    /// Constructor with message
    /// </summary>
    public CorruptedBlobException(string message)
        : base(message)
    {
    }

    /// <summary>
    /// Constructor with message and cause
    /// </summary>
    public CorruptedBlobException(string message, Exception cause)
        : base(message, cause)
    {
    }
}
