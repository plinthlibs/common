﻿using System.IO.Compression;

namespace Plinth.Storage;

internal static class CompressionUtil
{
    public static byte[] Compress(byte[] data)
    {
        using var ms = new MemoryStream(data.Length);
        // using optimal vs fastest
        // found that it gets another ~2% smaller but the time cost isn't bad
        // tested on 840KB xml file, Fastest = ~6ms, 35KB : Optimial = ~10ms, 20KB
        using (var output = new GZipStream(ms, CompressionLevel.Optimal))
        {
            output.Write(data, 0, data.Length);
        }
        return ms.ToArray();
    }

    public static Stream CompressStream(Stream outputStream)
    {
        // using optimal vs fastest
        // found that it gets another ~2% smaller but the time cost isn't bad
        // tested on 840KB xml file, Fastest = ~6ms, 35KB : Optimial = ~10ms, 20KB
        return new GZipStream(outputStream, CompressionLevel.Optimal);
    }

    public static byte[] Decompress(byte[] data)
    {
        using var ms = new MemoryStream(data.Length * 5);
        using var input = new GZipStream(new MemoryStream(data), CompressionMode.Decompress);
        input.CopyTo(ms);
        return ms.ToArray();
    }

    public static Stream DecompressStream(Stream inputStream)
    {
        return new GZipStream(inputStream, CompressionMode.Decompress);
    }
}
