using Plinth.Storage.Models;
using Plinth.Storage.Providers;
using Plinth.Security.Crypto;
using Plinth.Storage.Providers.Database;
using Plinth.Storage.Providers.FileSystem;

namespace Plinth.Storage;

/// <summary>
/// Factory for creating a BlobStorangeManager
/// </summary>
public class StorageFactory
{
    private readonly ISecureData? _secureData;
    private readonly BlobFeatures _defaultFeatures;

    internal Dictionary<string, Func<StorageProviderBase>> Providers { get; } = [];
    internal string? DefaultWriter { get; set; }
    internal List<Func<StorageProviderBase>> BackupProviders { get; } = [];

    /// <summary>
    /// Used by storage drivers to pass parameters to the factory
    /// </summary>
    private IDictionary<string, object>? DriverParameters { get; }

    /// <summary>
    /// Construct a Storage factory
    /// </summary>
    /// <remarks>this mode does not support encryption</remarks>
    /// <param name="defaultFeatures">defaults for writing blobs</param>
    /// <param name="driverParameters">optional parameters for the index driver</param>
    public StorageFactory(BlobFeatures defaultFeatures = BlobFeatures.None, IDictionary<string, object>? driverParameters = null)
    {
        if (defaultFeatures.HasFlag(BlobFeatures.Encrypted))
            throw new ArgumentException("If using BlobFeatures.Encryption, you must supply an ISecureData using a different constructor");

        _secureData = null;
        _defaultFeatures = defaultFeatures;
        DriverParameters = driverParameters;
    }

    /// <summary>
    /// Construct a Storage factory
    /// </summary>
    /// <remarks>this mode does not support encryption</remarks>
    /// <param name="defaultFeatures">defaults for writing blobs</param>
    /// <param name="driverParameters">optional parameters for the index driver</param>
    public StorageFactory(BlobFeatures defaultFeatures = BlobFeatures.None, params (string k, object v)[] driverParameters)
        : this(defaultFeatures, driverParameters.ToDictionary(kv => kv.k, kv => kv.v))
    {
    }

    /// <summary>
    /// Construct a Storage factory with Encryption support
    /// </summary>
    /// <param name="secureData">for encryption</param>
    /// <param name="defaultFeatures">defaults for writing blobs</param>
    /// <param name="driverParameters">optional parameters for the index driver</param>
    public StorageFactory(ISecureData secureData, BlobFeatures defaultFeatures = BlobFeatures.Encrypted, IDictionary<string, object>? driverParameters = null)
    {
        _secureData = secureData;
        _defaultFeatures = defaultFeatures;
        DriverParameters = driverParameters;
    }

    /// <summary>
    /// Construct a Storage factory with Encryption support
    /// </summary>
    /// <param name="secureData">for encryption</param>
    /// <param name="defaultFeatures">defaults for writing blobs</param>
    /// <param name="driverParameters">optional parameters for the index driver</param>
    public StorageFactory(ISecureData secureData, BlobFeatures defaultFeatures = BlobFeatures.Encrypted, params (string k, object v)[] driverParameters)
        : this(secureData, defaultFeatures, driverParameters.ToDictionary(kv => kv.k, kv => kv.v))
    {
    }

    /// <summary>
    /// Add DatabaseProvider support
    /// </summary>
    public void AddDatabaseProvider()
        => Providers[DatabaseProviderId.Key] = () => new DatabaseProvider();

    /// <summary>
    /// Add file system provider support
    /// </summary>
    /// <param name="settings">configuration</param>
    public void AddFileSystemProvider(FileSystemSettings settings)
        => Providers[FileSystemProviderId.Key] = () => new FileSystemProvider(settings);

    /// <summary>
    /// Set the database as the default write provider
    /// </summary>
    public void SetDefaultWriteProviderAsDatabase()
        => DefaultWriter = DatabaseProviderId.Key;

    /// <summary>
    /// Set the file system as the default write provider
    /// </summary>
    public void SetDefaultWriteProviderAsFileSystem()
        => DefaultWriter = FileSystemProviderId.Key;

    /// <summary>
    /// Add file system as a backup write provider
    /// </summary>
    public void AddBackupFileSystemProvider(FileSystemSettings settings)
        => BackupProviders.Add(() => new FileSystemProvider(settings));

    /// <summary>
    /// Used by storage index drivers to construct a storage manager (do not call directly)
    /// </summary>
    /// <param name="driver"></param>
    /// <returns></returns>
    public StorageManager CreateFromIndexDriver(IStorageIndexDriver driver)
        => new(driver, _secureData, Providers, BackupProviders, DefaultWriter ?? throw new InvalidOperationException("Default Writer not set"), _defaultFeatures);

    /// <summary>
    /// Get a driver parameter (used by storage index drivers)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="paramName"></param>
    /// <param name="defaultValue"></param>
    /// <returns></returns>
    public T? GetIndexDriverParameter<T>(string paramName, T? defaultValue = default)
    {
        var v = defaultValue;
        if (DriverParameters != null && DriverParameters.TryGetValue(paramName, out var o) == true)
            v = (T)o;

        return v;
    }
}
