# README

### Plinth.Storage

**A framework for reliably and securely storing large binary objects (BLOBs)**

Provides a framework for abstracting storage of large objects/files. Supports storing the data in a database, on the file system, AWS S3, or Azure Blobs. An index table is maintained in the database with references to the underlying data storage. Also supports encryption and compression if desired.

-   _Plinth.Storage_ - the main engine, supports database and file system storage
-   _Plinth.Storage.MsSql_ - Install this when using Microsoft SQL Server for the index
-   _Plinth.Storage.PgSql_ - Install this when using PostgreSQL for the index
-   _Plinth.Storage.AWS_ - Install for AWS S3 support
-   _Plinth.Storage.Azure_ - Install for Azure Blob support
    

Example setting up Blob Storage to write to S3 with Encryption (Startup.cs)

```c#
// Startup.cs in ConfigureServices
ISecureData secureData = new SecureData(key);

var factory = new StorageFactory(secureData, BlobFeatures.Encrypted);
factory.AddS3Provider(new S3Settings()
  {
      BucketName = "my-bucket",
      ClientId = "...",
      ClientSecret = "...",
      S3Url = "https://s3-us-west-2.amazonaws.com"
  });
factory.SetDefaultWriteProviderAsS3();
services.AddSingleton(factory);
```
In controller or manager with `ISqlConnection` and `StorageFactory` as injected or created parameters

```c#
var blobStorage = storageFactory.Get(connection, callingUser);

var blob = await blobStorage.ReadBlobAsync(blobGuid);

var newBlob = new Blob() { ... }
var newBlobGuid = await blobStorage.CreateNewBlobAsync(newBlob);
```
