using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.WindowsServices;

namespace Plinth.WindowsService;

internal class PlinthWebHostService(IWebHost host, IDictionary<ServiceState, Action>? callbacks = null) : WebHostService(host)
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private void Invoke(ServiceState state)
    {
        log.Info($"Service => {state}");
        if (callbacks != null && callbacks.TryGetValue(state, out var action))
        {
            action?.Invoke();
        }
    }

    protected override void OnStarting(string[] args)
    {
        Invoke(ServiceState.Starting);
        base.OnStarting(args);
    }

    protected override void OnStarted()
    {
        Invoke(ServiceState.Started);
        base.OnStarted();
    }

    protected override void OnStopping()
    {
        Invoke(ServiceState.Stopping);
        base.OnStopping();
    }

    protected override void OnShutdown()
    {
        Invoke(ServiceState.SystemShuttingDown);
        base.OnShutdown();
    }

    protected override void OnStopped()
    {
        Invoke(ServiceState.Stopped);
        base.OnStopped();
    }

    protected override void OnPause()
    {
        Invoke(ServiceState.Pause);
        base.OnPause();
    }

    protected override void OnContinue()
    {
        Invoke(ServiceState.Continue);
        base.OnContinue();
    }
}
