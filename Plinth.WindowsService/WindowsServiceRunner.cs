using System.ServiceProcess;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Hosting.WindowsServices;
using Plinth.Common.Utils;

namespace Plinth.WindowsService;

/// <summary>
/// Run various types of hosts/actions as a windows service with a console app override (--console)
/// </summary>
public static class WindowsServiceRunner
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private static string[] DetectConsoleFlag(string[] args, out bool isConsole)
    {
        var isSvc = WindowsServiceHelpers.IsWindowsService();

        if (args == null)
        {
            isConsole = !isSvc;
            return [];
        }

        var hostArgs = args.Where(a => a != "--console").ToArray();
        var argPassed = hostArgs.Length == args.Length - 1;

        if (!argPassed)
            isConsole = !isSvc;
        else
        {
            if (isSvc)
                log.Warn("Currently running in windows service context but '--console' specified");
            isConsole = true;
        }

        return hostArgs;
    }

    #region web host
    /// <summary>
    /// Run an asp.net core application as a windows service
    /// </summary>
    /// <param name="webHostFunc">a function which creates an IWebHost, usually BuildWebHost() in Program.cs</param>
    /// <param name="args">command line arguments from Main()</param>
    /// <param name="callbacks">(optional) callbacks for service lifecycle events</param>
    public static void RunWebHost(Func<string[], IWebHost> webHostFunc, string[] args, IDictionary<ServiceState, Action>? callbacks = null)
    {
        var hostArgs = DetectConsoleFlag(args, out bool isConsole);
        RunWebHost(webHostFunc(hostArgs), isConsole, callbacks);
    }

    /// <summary>
    /// Run an asp.net core application as a windows service
    /// </summary>
    /// <param name="webHostFunc">a function which creates an IWebHost, usually BuildWebHost() in Program.cs</param>
    /// <param name="isConsole">true to run as console app, false as windows service</param>
    /// <param name="args">command line arguments from Main()</param>
    /// <param name="callbacks">(optional) callbacks for service lifecycle events</param>
    public static void RunWebHost(Func<string[], IWebHost> webHostFunc, bool isConsole, string[] args, IDictionary<ServiceState, Action>? callbacks = null)
        => RunWebHost(webHostFunc(args), isConsole, callbacks);

    /// <summary>
    /// Run a webhost as a windows service (or a console app)
    /// </summary>
    /// <param name="webHost">a built webhost</param>
    /// <param name="isConsole">true to run as console app, false as windows service</param>
    /// <param name="callbacks">optional callbacks for service lifetime</param>
    public static void RunWebHost(IWebHost webHost, bool isConsole, IDictionary<ServiceState, Action>? callbacks = null)
    {
        void execAction(ServiceState s)
        {
            if (callbacks != null && callbacks.TryGetValue(s, out var action))
                action?.Invoke();
        }

        var addrs = webHost.ServerFeatures.Get<IServerAddressesFeature>()?.Addresses ?? Enumerable.Empty<string>();
        log.Info($"Starting webhost, listening at [{string.Join(", ", addrs)}]");

        if (isConsole)
        {
            log.Info("Running as console app");
            execAction(ServiceState.Starting);

            webHost.Start();
            execAction(ServiceState.Started);

            webHost.WaitForShutdown();
            execAction(ServiceState.Stopping);

            log.Info("Stopping console app");
            execAction(ServiceState.Stopped);
            return;
        }

        ServiceBase.Run(new PlinthWebHostService(webHost, callbacks));
    }
    #endregion

    #region console host
    /// <summary>
    /// Run an console application as a windows service
    /// </summary>
    /// <param name="hostFunc">a function which creates an IHost, usually BuildHost() in Program.cs</param>
    /// <param name="args">command line arguments from Main()</param>
    /// <param name="callbacks">(optional) callbacks for service lifecycle events</param>
    public static void RunHost(Func<string[], IHost> hostFunc, string[] args, IDictionary<ServiceState, Action>? callbacks = null)
    {
        var hostArgs = DetectConsoleFlag(args, out bool isConsole);
        RunHost(hostFunc(hostArgs), isConsole, callbacks);
    }

    /// <summary>
    /// Run a console application as a windows service
    /// </summary>
    /// <param name="hostFunc">a function which creates an IHost, usually BuildHost() in Program.cs</param>
    /// <param name="isConsole">true to run as console app, false as windows service</param>
    /// <param name="args">command line arguments from Main()</param>
    /// <param name="callbacks">(optional) callbacks for service lifecycle events</param>
    public static void RunHost(Func<string[], IHost> hostFunc, bool isConsole, string[] args, IDictionary<ServiceState, Action>? callbacks = null)
        => RunHost(hostFunc(args), isConsole, callbacks);

    /// <summary>
    /// Run a console application as a windows service
    /// </summary>
    /// <param name="host">an IHost to run the application</param>
    /// <param name="isConsole">true to run as console app, false as windows service</param>
    /// <param name="callbacks">(optional) callbacks for service lifecycle events</param>
    public static void RunHost(IHost host, bool isConsole, IDictionary<ServiceState, Action>? callbacks = null)
    {
        void execAction(ServiceState s)
        {
            if (callbacks != null && callbacks.TryGetValue(s, out var action))
                action?.Invoke();
        }

        if (isConsole)
        {
            log.Info("Running as console app");
            execAction(ServiceState.Starting);

            // strangely, in a console app calling .Start()/.WaitForShutdown() leaves threads running after Ctrl-C
            // and even though Main() completes the process doesn't end
            // using .Run() works, so I use that with lifetime events for service events
            var appLife = host.Services.GetRequiredService<IHostApplicationLifetime>();
            appLife.ApplicationStarted.Register(() => execAction(ServiceState.Started));
            appLife.ApplicationStopping.Register(() => { log.Info("Stopping console app"); execAction(ServiceState.Stopping); });

            host.Run();
            
            execAction(ServiceState.Stopped);
            return;
        }

        ServiceBase.Run(new PlinthConsoleHostService(host, callbacks));
    }
    #endregion

    #region console hosted service
    /// <summary>
    /// Run an IHostedService as a windows service
    /// </summary>
    /// <param name="service">an IHostedService</param>
    /// <param name="args">command line arguments from Main()</param>
    /// <param name="callbacks">(optional) callbacks for service lifecycle events</param>
    public static void RunHostedService(IHostedService service, string[] args, IDictionary<ServiceState, Action>? callbacks = null)
    {
        DetectConsoleFlag(args, out bool isConsole);
        RunHost(BuildHost(service), isConsole, callbacks);
    }

    /// <summary>
    /// Run an IHostedService as a windows service
    /// </summary>
    /// <param name="service">an IHostedService</param>
    /// <param name="isConsole">true to run as console app, false as windows service</param>
    /// <param name="callbacks">(optional) callbacks for service lifecycle events</param>
    public static void RunHostedService(IHostedService service, bool isConsole, IDictionary<ServiceState, Action>? callbacks = null)
    {
        RunHost(BuildHost(service), isConsole, callbacks);
    }

    private static IHost BuildHost(IHostedService a) =>
        new HostBuilder()
            .UseContentRoot(FindContentRoot())
            .ConfigureServices((hostContext, services) =>
            {
                // this is what .AddHostedService() does
                services.AddTransient(sp => a);
            })
            .Build();

    private class ActionService : IHostedService
    {
        public Func<CancellationToken, Task>? UserAsyncAction { get; set; }
        public Action<CancellationToken>? UserAction { get; set; }
        private readonly CancellationTokenSource _token = new();
        private Task? _userTask;

        public Task StartAsync(CancellationToken cancellationToken)
        {
            if (UserAction != null)
                _userTask = Task.Run(SyncAction, cancellationToken);
            else
                _userTask = AsyncAction();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _token.Cancel();
            return _userTask ?? Task.CompletedTask;
        }

        private void SyncAction()
        {
            try
            {
                ArgumentNullException.ThrowIfNull(UserAction);
                UserAction(_token.Token);
            }
            catch (Exception e) when (e is TaskCanceledException || e is AggregateException || e is OperationCanceledException)
            {
                if (e is not AggregateException ag || ag.InnerException is TaskCanceledException)
                    return;
                throw;
            }
        }

        private async Task AsyncAction()
        {
            try
            {
                ArgumentNullException.ThrowIfNull(UserAsyncAction);
                await UserAsyncAction(_token.Token);
            }
            catch (Exception e) when (e is TaskCanceledException || e is AggregateException)
            {
                if (e is not AggregateException ag || ag.InnerException is TaskCanceledException)
                    return;
                throw;
            }
        }
    }
    #endregion

    #region console action
    /// <summary>
    /// Run an Action as a windows service
    /// </summary>
    /// <param name="action">an Action that takes a cancellation token (try not to block checking the token)</param>
    /// <param name="args">command line arguments from Main()</param>
    /// <param name="callbacks">(optional) callbacks for service lifecycle events</param>
    public static void RunAction(Action<CancellationToken> action, string[] args, IDictionary<ServiceState, Action>? callbacks = null)
    {
        DetectConsoleFlag(args, out bool isConsole);
        RunAction(action, isConsole, callbacks);
    }

    /// <summary>
    /// Run an async Func as a windows service
    /// </summary>
    /// <param name="action">an async Func that takes a cancellation token (try not to block checking the token)</param>
    /// <param name="args">command line arguments from Main()</param>
    /// <param name="callbacks">(optional) callbacks for service lifecycle events</param>
    public static void RunAsyncAction(Func<CancellationToken, Task> action, string[] args, IDictionary<ServiceState, Action>? callbacks = null)
    {
        DetectConsoleFlag(args, out bool isConsole);
        RunAsyncAction(action, isConsole, callbacks);
    }

    /// <summary>
    /// Run an Action as a windows service
    /// </summary>
    /// <param name="action">an Action that takes a cancellation token (try not to block checking the token)</param>
    /// <param name="isConsole">true to run as console app, false as windows service</param>
    /// <param name="callbacks">(optional) callbacks for service lifecycle events</param>
    public static void RunAction(Action<CancellationToken> action, bool isConsole, IDictionary<ServiceState, Action>? callbacks = null)
        => RunHost(BuildHost(new ActionService() { UserAction = action }), isConsole, callbacks);

    /// <summary>
    /// Run an async Func as a windows service
    /// </summary>
    /// <param name="action">an async Func that takes a cancellation token (try not to block checking the token)</param>
    /// <param name="isConsole">true to run as console app, false as windows service</param>
    /// <param name="callbacks">(optional) callbacks for service lifecycle events</param>
    public static void RunAsyncAction(Func<CancellationToken, Task> action, bool isConsole, IDictionary<ServiceState, Action>? callbacks = null)
        => RunHost(BuildHost(new ActionService() { UserAsyncAction = action }), isConsole, callbacks);
    #endregion

    /// <summary>
    /// Get the path of the service's content root, useful for loading appsettings.json
    /// </summary>
    /// <example>
    /// Put this in your BuildWebHost() function in Program.cs.
    /// It will properly set up your service whether running in VS, a myservice.exe, or dotnet.exe myservice.dll
    /// <code>.UseContentRoot(WindowsServiceRunner.FindContentRoot())</code>
    /// </example>
    public static string FindContentRoot(string appSettingsBase = "appsettings")
        => RuntimeUtil.FindExecutableHome(appSettingsBase);
}
