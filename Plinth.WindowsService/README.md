
# README #

### Plinth.WindowsService ###

**Makes creating a windows service easy**

**1. Install as service**

* Include these in your csproj
```xml
     <OutputType>Exe</OutputType>
     <RuntimeIdentifier>win-x64</RuntimeIdentifier>
```
* be sure to add the --urls {URLS} parameter to your binpath
	- to run multiple ports, use semi-colon ';' between urls   
		- `--urls http://localhost:5050;https://something/`
    - note the usage of `SetContentRoot()` when using your own IWebHost/IHost
    - windows services don't start with the current directory where your .exe is
    
* Install
        > `sc create {ServiceName} displayName= "{DisplayName}" binpath= "D:\full\path\app.exe --urls http://localhost:9090" start= auto`
        > `sc description {ServiceName} "Service for Plinth WebApp"`

* Change path or params after installing
        > `sc config {ServiceName} binPath= "{PathAndParams}"`

* Delete service
        > `sc delete {ServiceName}`

* Start/Stop
        > `net start {ServiceName}`
        > `net stop {ServiceName}`

**2.  Standard ASP&#46;NET Core project using NLog**

* Program.cs [below]
* Add `--urls http://localhost:5050` to your Debug settings as command line arguments
	* to run multiple ports, use semi-colon ';' between urls   
		* `--urls http://localhost:5050;https://something/`

```c#
public class Program
{
	public static void Main(string[] args)
	{
		var log = StaticLogManagerSetup.BasicNLogSetup();
		log.Debug("startup!");

		WindowsServiceRunner.RunWebHost(
		    BuildWebHost,
		    args,
		    callbacks: new Dictionary<ServiceState, Action>()
		    {
		        [ServiceState.Starting] = () => log.Debug("Starting..."),
		        [ServiceState.Started] = () => log.Debug("Started"),
		        [ServiceState.Stopping] = () => log.Debug("Stopping..."),
		        [ServiceState.Stopped] = () => log.Debug("Stopped"),
		    });
	
		log.Debug("exit!");
	}

	public static IWebHost BuildWebHost(string[] args) =>
	    WebHost.CreateDefaultBuilder(args)
	        .UseStartup<Startup>()
	        .UseContentRoot(WindowsServiceRunner.FindContentRoot())   // <====  REALLY IMPORTANT!
	        .ConfigureLogging(builder => builder.AddNLog())
	        .CaptureStartupErrors(false)
	        .Build();
}
```


**3.  Console project (no web interface), without NLog**

* Program.cs [below]
* Add `--urls http://localhost:5050` to your Debug settings as command line arguments
	* to run multiple ports, use semi-colon ';' between urls   
		* `--urls http://localhost:5050;https://something/`

```c#
	static void Main(string[] args)
	{
		var log = StaticLogManagerSetup.ConfigureForConsoleLogging();

		log.Debug("startup!");

		var mycallbacks = new Dictionary<ServiceState, Action>()
		{
		    [ServiceState.Starting] = () => log.Debug("Main: Starting..."),
		    [ServiceState.Started] = () => log.Debug("Main: Started"),
		    [ServiceState.Stopping] = () => log.Debug("Main: Stopping..."),
		    [ServiceState.Stopped] = () => log.Debug("Main: Stopped"),
		};

		try
		{
		    // option 1, make your own host and hosted service
		    // WindowsServiceRunner.RunHost(BuildHost, args, callbacks: mycallbacks);

		    // option 2, make your own hosted service
		    // WindowsServiceRunner.RunHostedService(new LoggerService(), args, callbacks: mycallbacks);

		    // option 3, a cancellable user action
		    // WindowsServiceRunner.RunAction(MyAction, args, callbacks: mycallbacks);

		    // option 4, a cancellable user action (async)
		    WindowsServiceRunner.RunAsyncAction(MyAsyncAction, args, callbacks: mycallbacks);
		}
		catch (Exception e)
		{
		    log.Fatal("failed during startup", e);
		    throw;
		}
	}

	// needed for option 1
	public static IHost BuildHost(string[] args) =>
		new HostBuilder()
		    .UseContentRoot(WindowsServiceRunner.FindContentRoot())   // <====  REALLY IMPORTANT!
		    .ConfigureServices((hostContext, services) =>
		    {
		        services.AddHostedService<LoggerService>();
		    })
		    .Build();

	// needed for option 1 and option 2
	public class LoggerService : IHostedService, IDisposable
	{
		private static readonly ILogger log = StaticLogManager.GetLogger();

		public Task StartAsync(CancellationToken cancellationToken)
		{
		    log.Debug("service start()");
		    return Task.CompletedTask;
		}

		public Task StopAsync(CancellationToken cancellationToken)
		{
		    return Task.CompletedTask;
		}

		public void Dispose()
		{
		    _timer?.Dispose();
		}
	}

	// option 3
	private static void MyAction(CancellationToken token)
	{
		while (!token.IsCancellationRequested)
		{
		    log.Debug("timer");
		    Task.Delay(TimeSpan.FromSeconds(2), token).Wait(); // try not to block on non cancellable calls
		}
	}

	// option 4
	private static async Task MyAsyncAction(CancellationToken token)
	{
		while (!token.IsCancellationRequested)
		{
		    log.Debug("timer");
		    await Task.Delay(TimeSpan.FromSeconds(2), token); // try not to block on non cancellable calls
		}
	}

```
