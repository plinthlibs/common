﻿namespace Plinth.WindowsService;

/// <summary>
/// Service lifecycle events
/// </summary>
public enum ServiceState
{
    /// <summary>called when service begins initialization</summary>
    Starting,
    /// <summary>called when service completes initialization and is ready for business</summary>
    Started,
    /// <summary>called when service is first told to stop</summary>
    Stopping,
    /// <summary>called when service has stopped</summary>
    Stopped,
    /// <summary>called when service is informed of system shutdown</summary>
    SystemShuttingDown,
    /// <summary>not supported</summary>
    Pause,
    /// <summary>not supported</summary>
    Continue
}
