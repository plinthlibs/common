using System.ServiceProcess;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Plinth.WindowsService;

// based on https://github.com/aspnet/AspNetCore/blob/master/src/Hosting/WindowsServices/src/WebHostService.cs
internal class PlinthConsoleHostService(IHost host, IDictionary<ServiceState, Action>? callbacks = null) : ServiceBase
{
    private static readonly ILogger log = StaticLogManager.GetLogger();
    private bool _stopRequestedByWindows;

    private void Invoke(ServiceState state)
    {
        log.Info($"Service => {state}");
        if (callbacks != null && callbacks.TryGetValue(state, out var action))
        {
            action?.Invoke();
        }
    }

    protected override void OnStart(string[] args)
    {
        Invoke(ServiceState.Starting);

        host.Start();

        Invoke(ServiceState.Started);

        // Register callback for application stopping after we've
        // started the service, because otherwise we might introduce unwanted
        // race conditions.
        host
            .Services
            .GetRequiredService<IHostApplicationLifetime>()
            .ApplicationStopping
            .Register(() =>
            {
                if (!_stopRequestedByWindows)
                {
                    Stop();
                }
            });
    }

    protected override void OnStop()
    {
        _stopRequestedByWindows = true;
        Invoke(ServiceState.Stopping);
        try
        {
            host.StopAsync().GetAwaiter().GetResult();
        }
        finally
        {
            host.Dispose();
            Invoke(ServiceState.Stopped);
        }
    }

    protected override void OnShutdown()
    {
        Invoke(ServiceState.SystemShuttingDown);
        base.OnShutdown();
    }

    protected override void OnPause()
    {
        Invoke(ServiceState.Pause);
        base.OnPause();
    }

    protected override void OnContinue()
    {
        Invoke(ServiceState.Continue);
        base.OnContinue();
    }
}
