using Hangfire;
using Microsoft.Extensions.DependencyInjection;
using Plinth.Database.MSSql;
using Plinth.Hangfire.Impl.Sync;

namespace Plinth.Hangfire.MSSql;

internal class JobSyncFactory(ISqlTransactionProvider? txnProvider) : IJobSyncFactory
{
    public JobSyncLogic Create(IServiceProvider provider, JobHandlerRegistrar handlers)
    {
        return new JobSyncLogic(
            new JobRepository(txnProvider ?? provider.GetRequiredService<ISqlTransactionProvider>()),
            handlers,
            provider.GetRequiredService<IRecurringJobManager>()
        );
    }
}
