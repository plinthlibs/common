using Hangfire;
using Hangfire.SqlServer;
using Microsoft.Extensions.DependencyInjection;
using Plinth.Database.MSSql;
using Plinth.Hangfire.MSSql;

namespace Plinth.Hangfire;

/// <summary>
/// Extensions for a Plinth standard Hangfire instance using SQL Server
/// </summary>
public static class PlinthServicesExtensionsMSSql
{
    /// <summary>
    /// In ConfigureServices(), add Plinth hangfire setup (for SQL Server)
    /// </summary>
    /// <param name="services"></param>
    /// <param name="hangfireConnString">database connection string for the hangfire tables</param>
    /// <param name="registrarAction">an action to register job handlers</param>
    /// <param name="jobProcDbProv">optionally provide the transaction provider for accessing the jobs list stored proc.  If not provided, it will be resolved from the service provider</param>
    /// <param name="dbSyncCron">cron frequency for database job table sync (default to every 15 mins)</param>
    /// <param name="serverOptsAction">optional custom config for the background server</param>
    /// <param name="storageOptsAction">optional custom config for the storage options</param>
    /// <param name="configAction">optional custom config for the hangfire global configuration</param>
    public static IServiceCollection AddPlinthHangfire(this IServiceCollection services,
        string hangfireConnString,
        Action<JobHandlerRegistrar> registrarAction,
        ISqlTransactionProvider? jobProcDbProv = null,
        string? dbSyncCron = null,
        Action<BackgroundJobServerOptions>? serverOptsAction = null,
        Action<SqlServerStorageOptions>? storageOptsAction = null,
        Action<IGlobalConfiguration>? configAction = null)
        => AddPlinthHangfireImpl(services, hangfireConnString, registrarAction, true, jobProcDbProv, dbSyncCron, serverOptsAction, storageOptsAction, configAction);

    internal static IServiceCollection AddPlinthHangfireImpl(this IServiceCollection services,
        string hangfireConnString,
        Action<JobHandlerRegistrar> registrarAction,
        bool startAsync = true,
        ISqlTransactionProvider? jobProcDbProv = null,
        string? dbSyncCron = null,
        Action<BackgroundJobServerOptions>? serverOptsAction = null,
        Action<SqlServerStorageOptions>? storageOptsAction = null,
        Action<IGlobalConfiguration>? configAction = null)
    {
        var jobReg = new JobHandlerRegistrar();
        registrarAction.Invoke(jobReg);

        var storageOpts = new SqlServerStorageOptions
        {
            EnableHeavyMigrations = true,

            // these are the recommended options: https://docs.hangfire.io/en/latest/configuration/using-sql-server.html
            // they will become the default in 2.0 of hangfire
            CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
            SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
            QueuePollInterval = TimeSpan.Zero,
            UseRecommendedIsolationLevel = true,
            DisableGlobalLocks = true
        };

        storageOptsAction?.Invoke(storageOpts);

        return services.AddPlinthHangfireInternal(
            new JobSyncFactory(jobProcDbProv),
            jobReg,
            new SqlServerStorage(hangfireConnString, storageOpts),
            startAsync,
            dbSyncCron,
            serverOptsAction,
            configAction);
    }
}
