using Plinth.Database.MSSql;
using Plinth.Hangfire.Impl.Sync;

namespace Plinth.Hangfire.MSSql;

internal class JobRepository(ISqlTransactionProvider txnProvider) : IJobRepository
{
    public List<PlinthJob> GetJobList()
        => txnProvider.ExecuteTxn(c => new JobDAO(c).GetJobList());
}
