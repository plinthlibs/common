using Plinth.Database.MSSql;

namespace Plinth.Hangfire.MSSql;

internal class JobDAO(ISqlConnection connection)
{
    public List<PlinthJob> GetJobList()
        => connection.ExecuteQueryProcList("usp_GetJobList", LoadJob);

    private static PlinthJob LoadJob(IResult rs)
        => new()
        {
            Code = rs.GetString("Code") ?? throw new InvalidOperationException("Job Code cannot be null"),
            Description = rs.GetString("Description"),
            IsActive = rs.GetBool("IsActive"),
            CronExpression = rs.GetString("CronExpression"),
            TimeZone = rs.GetString("TimeZone"),
            JobData = rs.GetString("JobData")
        };
}
