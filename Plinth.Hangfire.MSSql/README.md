# README

### Plinth.Hangfire.MSSql

**Microsoft SQL Server driver for _Plinth.Hangfire_**

When using SQL Server for job storage with _Plinth.Hangfire_

:point_right: See _Plinth.Hangfire_ instructions for using this package.

* Provides `dbo.Job` in `Hangfire_Tables.sql`
* Provides `usp_SubmitJob` and `usp_GetJobList` in `Hangfire_Procedures.sql`
