﻿
-- Plinth Job Procedures
CREATE PROCEDURE dbo.usp_SubmitJob
	@Code VARCHAR(100),
	@Description NVARCHAR(255) = NULL,
	@JobData NVARCHAR(4000) = NULL,
	@CronExpression VARCHAR(50) = NULL,
	@TimeZone VARCHAR(100) = NULL,
	@IsActive BIT = NULL,
	@CallingUser NVARCHAR(255) = NULL
AS
BEGIN
	SET NOCOUNT OFF
	/*
		-- create
		EXECUTE dbo.usp_SubmitJob
			@Code = 'MyTestJob',
			@Description = 'This is my test job',
			@CronExpression = '00 * * * *',
			@TimeZone = 'Pacific Standard Time',
			@IsActive = 1

		-- update
		EXECUTE dbo.usp_SubmitJob
			@Code = 'MyTestJob',
			@CronExpression = '00 6,18 * * *',
			@TimeZone = 'Eastern Standard Time'
	*/

	IF (@CallingUser IS NULL)
	BEGIN
		SET @CallingUser = ORIGINAL_LOGIN()
	END

	MERGE dbo.Job AS A
	USING
	(
		SELECT
			@Code AS Code,
			@Description AS [Description],
			@JobData AS JobData,
			@CronExpression AS CronExpression,
			@TimeZone AS TimeZone,
			@IsActive AS IsActive
	) AS B
	ON A.Code = B.Code
	WHEN MATCHED AND (
		A.[Description] <> ISNULL(B.[Description], A.[Description])
		OR A.JobData <> ISNULL(B.JobData, A.JobData)
		OR A.CronExpression <> ISNULL(B.CronExpression, A.CronExpression)
		OR A.TimeZone <> ISNULL(B.TimeZone, A.TimeZone)
		OR A.IsActive <> ISNULL(B.IsActive, A.IsActive)
	) THEN
		UPDATE SET
			A.[Description] = ISNULL(B.[Description], A.[Description]),
			A.JobData = ISNULL(B.JobData, A.JobData),
			A.CronExpression = ISNULL(B.CronExpression, A.CronExpression),
			A.TimeZone = ISNULL(B.TimeZone, A.TimeZone),
			A.IsActive = ISNULL(B.IsActive, A.IsActive),
			A.DateUpdated = SYSUTCDATETIME(),
			A.UpdatedBy = @CallingUser
	WHEN NOT MATCHED THEN
		INSERT (
			Code,
			[Description],
			JobData,
			CronExpression,
			TimeZone,
			IsActive,
			DateInserted,
			InsertedBy,
			DateUpdated,
			UpdatedBy)
		VALUES (
			@Code,
			@Description,
			ISNULL(@JobData, ''),
			@CronExpression,
			ISNULL(@TimeZone, 'UTC'),
			ISNULL(@IsActive, 1),
			SYSUTCDATETIME(),
			@CallingUser,
			SYSUTCDATETIME(),
			@CallingUser);
END
GO

CREATE PROCEDURE dbo.usp_GetJobList
AS
BEGIN
	SET NOCOUNT ON
	/*
		EXECUTE dbo.usp_GetJobList
	*/

	SELECT
		J.Code,
		J.[Description],
		J.JobData,
		J.CronExpression,
		J.TimeZone,
		J.IsActive,
		J.DateInserted,
		J.InsertedBy,
		J.DateUpdated,
		J.UpdatedBy
	FROM dbo.Job J
END
GO
-- Plinth Job Procedures
