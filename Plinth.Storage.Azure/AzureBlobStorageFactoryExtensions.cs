using Plinth.Storage.Azure;

namespace Plinth.Storage;

/// <summary>
/// Extensions for Azure
/// </summary>
public static class AzureBlobStorageFactoryExtensions
{
    /// <summary>
    /// Add Azue Blob provider support
    /// </summary>
    /// <param name="fac"></param>
    /// <param name="settings">configuration</param>
    public static void AddAzureBlobProvider(this StorageFactory fac, Providers.AzureBlob.AzureBlobSettings settings)
        => fac.Providers[AzureBlobProviderId.Key] = () => new Providers.AzureBlob.AzureBlobProvider(settings);

    /// <summary>
    /// Set Azure as the default write provider
    /// </summary>
    public static void SetDefaultWriteProviderAsAzureBlob(this StorageFactory fac)
        => fac.DefaultWriter = AzureBlobProviderId.Key;

    /// <summary>
    /// Add Azure blobs as a backup write provider
    /// </summary>
    public static void AddBackupAzureBlobProvider(this StorageFactory fac, Providers.AzureBlob.AzureBlobSettings settings)
        => fac.BackupProviders.Add(() => new Providers.AzureBlob.AzureBlobProvider(settings));
}
