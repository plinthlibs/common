using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plinth.Storage.Azure;

/// <summary>
/// Provider Id Key for the Azure Blob provider
/// </summary>
public static class AzureBlobProviderId
{
    /// <summary>
    /// Provider Id Key for the Azure Blob provider
    /// </summary>
    public static string Key => "AZB";
}
