
# README

### Plinth.Storage.Azure

**Storage Provider for Plinth.Storage to write blob data to Azure Blob Storage**

Enables storing the blob data used by _Plinth.Storage_ in Azure Blob Storage

This package adds these extension methods to `Plinth.Storage.StorageFactory` to utilize Azure Blob Storage
```c#
    storageFactory.AddAzureBlobProvider(new AzureBlobSettings()
      {
          ConnectionString = "{from-azure-portal}",
          ContainerName = "MyBlobs"
      });

    storageFactory.SetDefaultWriteProviderAsAzureBlob();
```

### Index Strategies

By default, this will write blobs to the root of the container.  There are 2 other built in options for futher segmenting the blobs.  To use these, set `DefaultIndexStrategy` in `AzureBlobSettings`

**_DefaultIndexStrategy.ByDate_**
This will place the blobs in a sub folder with the current date as `/YY/MM/DD/{blobId}`

**_DefaultIndexStrategy.ByDateTime_**
This will place the blobs in a sub folder with the current date and current hour as `/YY/MM/DD/HH/{blobId}`

Alternatively, a custom indexing strategy can be provided.  For example, the below will place the blobs under a folder named the first character of the blob's guid.
```
      new AzureBlobSettings()
      {
          ...
          CustomIndexStrategy = blob => $"{blob.Guid!.ToString()![0]}"
      }
```      

 
### Backup Container

By default, blobs will be written to the bucket specified.  This library supports writing a backup to other containers.

To enable, use `storageFactory.AddBackupAzureBlobProvider(settings)` with a complete `AzureBlobSettings` object.  Blobs will be written to the container specified (using the primary index).

### File Extensions
By default, the blobs will a file extension in blob storage which come from the blob name field.  This allows for browsers to more easily download those files directly.
To disable this, set `DisableBlobExtensions` to `true` in `AzureBlobSettings` and the blobs will not have a file extension.
