using Microsoft.Extensions.Logging;
using Plinth.Storage.Models;
using Plinth.Common.Utils;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;

namespace Plinth.Storage.Providers.AzureBlob;

/// <summary>
/// Provides blob storage using Azure Blob for the data
/// </summary>
internal class AzureBlobProvider : StorageProviderBase
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private readonly AzureBlobSettings _azureSettings;
    private readonly BlobContainerClient _container;

    public override string Name => $"Azure:{_azureSettings.ContainerName}";

    /// <summary>
    /// Construct a Azure BlobStorage instance for blobs stored in AWS Azure Blob objects
    /// </summary>
    /// <remarks>Uses the 'Index' column to store the Azure Blob object path</remarks>
    /// <param name="azureSettings">Azure Blob storage settings</param>
    public AzureBlobProvider(AzureBlobSettings azureSettings)
    {
#if NET8_0_OR_GREATER
        ArgumentException.ThrowIfNullOrEmpty(azureSettings.ConnectionString);
        ArgumentException.ThrowIfNullOrEmpty(azureSettings.ContainerName);
#else
#pragma warning disable CA2208 // Instantiate argument exceptions correctly
        if (string.IsNullOrEmpty(azureSettings.ConnectionString))
            throw new ArgumentNullException(nameof(azureSettings), "azureSettings.ConnectionString");
        if (string.IsNullOrEmpty(azureSettings.ContainerName))
            throw new ArgumentNullException(nameof(azureSettings), "azureSettings.ContainerName");
#pragma warning restore CA2208 // Instantiate argument exceptions correctly
#endif

        _azureSettings = azureSettings;

        _container = new BlobContainerClient(_azureSettings.ConnectionString, _azureSettings.ContainerName);
    }

    /// <summary>
    /// The Azure Blob provider writes data to Azure Blob Web Services, which is not transactional
    /// </summary>
    public override bool WritesDataToIndex => false;

    /// <summary>
    /// Azure Blob provider uses the index as the path in the bucket to the object
    /// </summary>
    public override string? ConstructBlobIndex(Blob blob, Func<Blob, string?>? indexStrategy)
    {
        if (indexStrategy != null)
            return indexStrategy.Invoke(blob);

        if (_azureSettings.CustomIndexStrategy != null)
            return _azureSettings.CustomIndexStrategy.Invoke(blob);

        switch (_azureSettings.DefaultIndexStrategy)
        {
            case DefaultIndexStrategy.Flat:
                return string.Empty;

            case DefaultIndexStrategy.ByDate:
            {
                var d = DateTime.UtcNow;
                return Path.Combine(d.Year.ToString("D2"), d.Month.ToString("D2"), d.Day.ToString("D2"));
            }

            case DefaultIndexStrategy.ByDateTime:
            {
                var d = DateTime.UtcNow;
                return Path.Combine(d.Year.ToString("D2"), d.Month.ToString("D2"), d.Day.ToString("D2"), d.Hour.ToString("D2"));
            }

            default:
                throw new NotSupportedException(_azureSettings.DefaultIndexStrategy.ToString());
        }
    }

    /// <summary>
    /// Load the bytes from Azure Blob
    /// </summary>
    public override async Task<byte[]?> ReadBlobDataAsync(Blob blob, CancellationToken cancellationToken)
    {
        using (new TimeUtil.TimeLogger("ReadBlobAsync(Azure Blob)", log))
        {
            var path = BlobNameForBlobGuid(blob);
            var blockBlob = _container.GetBlobClient(path);

            var data = new MemoryStream((int)blob.StoredSize);
            var response = await blockBlob.DownloadToAsync(data, cancellationToken);

            log.Debug($"read {response.Headers.ContentLength} bytes from blob storage");
            return data.Capacity == blob.StoredSize ? data.GetBuffer() : data.ToArray();
        }
    }

    private static readonly BlobOpenReadOptions _blobReadOptions = new(false);

    /// <summary>
    /// Get a read stream
    /// </summary>
    public override async Task<Stream?> ReadBlobDataStreamAsync(Blob blob, Stream? dbStream, CancellationToken cancellationToken)
    {
        var path = BlobNameForBlobGuid(blob);
        var blockBlob = _container.GetBlobClient(path);

        return await blockBlob.OpenReadAsync(_blobReadOptions, cancellationToken);
    }

    private static string FixIndex(string origIndex) => origIndex.Replace('\\', '/');

    private string BlobNameForBlobGuid(Blob b)
    {
        string fn = b.Guid!.Value.ToString().ToLowerInvariant();

        if (!string.IsNullOrEmpty(b.Index))
            fn = Path.Combine(b.Index, fn);

        // normally, we add the extension to the blob to support url access. this is optionally disabled
        if (_azureSettings.DisableBlobExtensions != true)
        {
            var extension = Path.GetExtension(b.Name);
            if (!string.IsNullOrEmpty(extension))
                fn += extension;
        }

        if (!string.IsNullOrEmpty(_azureSettings.BlobPrefix))
            fn = _azureSettings.BlobPrefix + fn;

        return FixIndex(fn);
    }

    /// <summary>
    /// Write the data to an Azure Blob Object
    /// </summary>
    public override async Task WriteBlobAsync(Blob blob, byte[] data, IDictionary<string, object>? providerParams, CancellationToken cancellationToken)
    {
        var path = BlobNameForBlobGuid(blob);

        var blockBlob = _container.GetBlobClient(path);
        await blockBlob.UploadAsync(new MemoryStream(data), cancellationToken);

        log.Debug($"wrote {data.Length} bytes to azure: {path}");
    }

    public override async Task<(long origLen, long storedLen, bool wrote)> WriteBlobStreamAsync(Blob blob, StreamCounter stream, long lengthHint, IDictionary<string, object>? providerParams, CancellationToken cancellationToken)
    {
        var path = BlobNameForBlobGuid(blob);

        var blockBlob = _container.GetBlobClient(path);
        long storedLen = 0;
        using (var counter = new StreamCounter(stream))
        {
            await blockBlob.UploadAsync(counter, cancellationToken);
            storedLen = counter.BytesRead;
        }

        long origLen = stream.BytesRead;

        log.Debug($"wrote {storedLen} bytes to azure: {path}");

        return (origLen, storedLen, true);
    }

    /// <summary>
    /// Remove the object from the container
    /// </summary>
    public override async Task DeleteBlobAsync(Blob blob, CancellationToken cancellationToken)
    {
        var replacedName = BlobNameForBlobGuid(blob);
        log.Debug($"removing object {_azureSettings.ContainerName}/{replacedName}");
        await _container.DeleteBlobIfExistsAsync(replacedName, DeleteSnapshotsOption.IncludeSnapshots, cancellationToken: cancellationToken);
    }
}
