using Npgsql;

namespace Plinth.Database.PgSql;

/// <summary>
/// Interface for SQL Connections
/// </summary>
public interface ISqlConnectionBase
{
    /// <summary>
    /// Determine if this transaction supports async methods
    /// </summary>
    bool IsAsync();

    /// <summary>
    /// The time in seconds to wait for a command to execute before throwing an error.
    /// </summary>
    int CommandTimeout { get; set; }

    #region Proc
    /// <summary>
    /// Execute a stored procedure that returns no results
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="parameters"></param>
    /// <exception cref="DatabaseException">thrown with ExpectedRowsMismatch if proc returns 0 rows modified</exception>
    void ExecuteProc(string procName, params NpgsqlParameter?[] parameters);

    /// <summary>
    /// Execute a stored procedure that returns no results, check for a specific # of rows returned
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="expectedRows">number of rows expected</param>
    /// <param name="parameters"></param>
    /// <exception cref="DatabaseException">thrown with ExpectedRowsMismatch if #rows modified does not equal expectedRows</exception>
    void ExecuteProc(string procName, int expectedRows, params NpgsqlParameter?[] parameters);

    /// <summary>
    /// Execute a stored procedure that returns no results, does not check expected rows
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="parameters"></param>
    /// <returns>number of rows affected</returns>
    int ExecuteProcUnchecked(string procName, params NpgsqlParameter?[] parameters);
    #endregion

    #region Query Proc
    /// <summary>
    /// Execute a stored procedure and return results as an enumerable
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="parameters"></param>
    /// <returns>An IEnumerable that can be iterated on to get each returned row</returns>
    IEnumerable<IResult> ExecuteQueryProc(string procName, params NpgsqlParameter?[] parameters);

    /// <summary>
    /// Execute a stored procedure that can return multiple result sets
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="readerAction">An action that receives a multi-result-set which provides all the result sets</param>
    /// <param name="parameters"></param>
    void ExecuteQueryProcMultiResultSet(string procName, Action<IMultiResultSet> readerAction, params NpgsqlParameter?[] parameters);

    /// <summary>
    /// Execute a stored procedure that returns a list of results
    /// </summary>
    /// <typeparam name="T">type of the objects in the list</typeparam>
    /// <param name="procName"></param>
    /// <param name="readerFunc">A function that converts a row to an object (only called if a row was returned)</param>
    /// <param name="parameters"></param>
    /// <returns>A List of objects, always non-null</returns>
    List<T> ExecuteQueryProcList<T>(string procName, Func<IResult, T> readerFunc, params NpgsqlParameter?[] parameters);

    /// <summary>
    /// Execute a stored procedure that returns a single row
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="readerFunc">An action that will be called with a result (only called if a row was returned)</param>
    /// <param name="parameters"></param>
    /// <returns>true if a row was found, false otherwise</returns>
    bool ExecuteQueryProcOne(string procName, Action<IResult> readerFunc, params NpgsqlParameter?[] parameters);

    /// <summary>
    /// Execute a stored procedure that returns a single row
    /// </summary>
    /// <typeparam name="T">type of the single value to return</typeparam>
    /// <param name="procName"></param>
    /// <param name="readerFunc">A function that extracts a single value from a result (only called if a row was returned).</param>
    /// <param name="parameters"></param>
    /// <returns>A SqlSingleResult containing the return value from readerFunc, or default if no results returned,
    /// and a bool containing whether a row was returned or not</returns>
    SingleResult<T> ExecuteQueryProcOne<T>(string procName, Func<IResult, T> readerFunc, params NpgsqlParameter?[] parameters);
    #endregion

    #region Proc Async
    /// <summary>
    /// Execute a stored procedure that returns no results, async
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="parameters"></param>
    /// <exception cref="DatabaseException">thrown with ExpectedRowsMismatch if proc returns 0 rows modified</exception>
    Task ExecuteProcAsync(string procName, params NpgsqlParameter?[] parameters);

    /// <inheritdoc cref="ExecuteProcAsync(string, NpgsqlParameter?[])"/>
    Task ExecuteProcAsync(string procName, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters);

    /// <summary>
    /// Execute a stored procedure that returns no results, check for a specific # of rows returned, async
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="expectedRows"></param>
    /// <param name="parameters"></param>
    /// <exception cref="DatabaseException">thrown with ExpectedRowsMismatch if proc returns 0 rows modified</exception>
    Task ExecuteProcAsync(string procName, int expectedRows, params NpgsqlParameter?[] parameters);

    /// <inheritdoc cref="ExecuteProcAsync(string, int, NpgsqlParameter?[])"/>
    Task ExecuteProcAsync(string procName, int expectedRows, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters);

    /// <summary>
    /// Execute a stored procedure that returns no results, does not check expected rows, async
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="parameters"></param>
    /// <returns>number of rows affected</returns>
    Task<int> ExecuteProcUncheckedAsync(string procName, params NpgsqlParameter?[] parameters);

    /// <inheritdoc cref="ExecuteProcUncheckedAsync(string, NpgsqlParameter?[])"/>
    Task<int> ExecuteProcUncheckedAsync(string procName, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters);
    #endregion

    #region Query Proc Async
    /// <summary>
    /// Execute a stored procedure and return results as an enumerable
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="parameters"></param>
    /// <returns>An IAsyncEnumerable that can be iterated on to get each returned row</returns>
    IAsyncEnumerable<IResult> ExecuteQueryProcAsync(string procName, params NpgsqlParameter?[] parameters);

    /// <inheritdoc cref="ExecuteQueryProcAsync(string, NpgsqlParameter?[])"/>
    IAsyncEnumerable<IResult> ExecuteQueryProcAsync(string procName, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters);

    /// <summary>
    /// Execute a stored procedure that can return multiple result sets, async
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="readerFunc">An async func that receives a multi-result-set which provides all the result sets</param>
    /// <param name="parameters"></param>
    Task ExecuteQueryProcMultiResultSetAsync(string procName, Func<IAsyncMultiResultSet, Task> readerFunc, params NpgsqlParameter?[] parameters);

    /// <inheritdoc cref="ExecuteQueryProcMultiResultSet(string, Action{IMultiResultSet}, NpgsqlParameter?[])"/>
    Task ExecuteQueryProcMultiResultSetAsync(string procName, Func<IAsyncMultiResultSet, Task> readerFunc, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters);

    /// <summary>
    /// Execute a stored procedure that returns a list of results, async
    /// </summary>
    /// <typeparam name="T">type of the objects in the list</typeparam>
    /// <param name="procName"></param>
    /// <param name="readerFunc">A function that converts a row to an object (only called if a row was returned), should be async</param>
    /// <param name="parameters"></param>
    /// <returns>A List of objects, always non-null</returns>
    Task<List<T>> ExecuteQueryProcListAsync<T>(string procName, Func<IResult, Task<T>> readerFunc, params NpgsqlParameter?[] parameters);

    /// <inheritdoc cref="ExecuteQueryProcListAsync{T}(string, Func{IResult, Task{T}}, NpgsqlParameter?[])"/>
    Task<List<T>> ExecuteQueryProcListAsync<T>(string procName, Func<IResult, Task<T>> readerFunc, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters);

    /// <summary>
    /// Execute a stored procedure that returns a single row, async
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="readerFunc">An action that will be called with a result (only called if a row was returned), should be async</param>
    /// <param name="parameters"></param>
    /// <returns>true if a row was found, false otherwise</returns>
    Task<bool> ExecuteQueryProcOneAsync(string procName, Func<IResult, Task> readerFunc, params NpgsqlParameter?[] parameters);

    /// <inheritdoc cref="ExecuteQueryProcOneAsync(string, Func{IResult, Task}, NpgsqlParameter?[])"/>
    Task<bool> ExecuteQueryProcOneAsync(string procName, Func<IResult, Task> readerFunc, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters);

    /// <summary>
    /// Execute a stored procedure that returns a single row, async
    /// </summary>
    /// <typeparam name="T">type of the single value to return</typeparam>
    /// <param name="procName"></param>
    /// <param name="readerFunc">A function that extracts a single value from a result (only called if a row was returned), should be async</param>
    /// <param name="parameters"></param>
    /// <returns>A SqlSingleResult containing the return value from readerFunc, or default if no results returned,
    /// and a bool containing whether a row was returned or not</returns>
    Task<SingleResult<T>> ExecuteQueryProcOneAsync<T>(string procName, Func<IResult, Task<T>> readerFunc, params NpgsqlParameter?[] parameters);

    /// <inheritdoc cref="ExecuteQueryProcOneAsync{T}(string, Func{IResult, Task{T}}, NpgsqlParameter?[])"/>
    Task<SingleResult<T>> ExecuteQueryProcOneAsync<T>(string procName, Func<IResult, Task<T>> readerFunc, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters);
    #endregion
}
