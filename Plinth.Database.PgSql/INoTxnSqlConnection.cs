namespace Plinth.Database.PgSql;

/// <summary>
/// Interface for SQL Connections without a transaction
/// </summary>
/// <remarks>
/// This kind of connection will work like ISqlConnection, but you can only do a single write operation
/// </remarks>
public interface INoTxnSqlConnection : ISqlConnectionBase
{
    /// <summary>
    /// Determine if this connection has been used to write (once)
    /// </summary>
    public bool HasWritten { get; }

    #region Error Actions
    /// <summary>
    /// Add an action to execute if operation threw an exception
    /// </summary>
    /// <remarks>
    /// <para>These actions will be called in reverse order if the action throws an exception</para>
    /// <para>Activities that can be undone can be set up here</para>
    /// <para>Executing these actions is done on a best effort basis</para>
    /// <para>These will be called even if the error is transient and triggers retries</para>
    /// </remarks>
    /// <example>
    /// <code>
    /// Guid result = webServiceClient.AddThing();
    /// conn.AddErrorAction("undo add thing", () => webServiceClient.RemoveThing(result));
    /// </code>
    /// </example>
    /// <param name="desc">description of error event (for logs)</param>
    /// <param name="onError">action to call</param>
    void AddErrorAction(string? desc, Action<Exception?> onError);

    /// <summary>
    /// Add an async action to execute if operation threw an exception
    /// </summary>
    /// <remarks>
    /// <para>These actions will be called in reverse order if the action throws an exception</para>
    /// <para>Activities that can be undone can be set up here</para>
    /// <para>Executing these actions is done on a best effort basis</para>
    /// <para>These will be called even if the error is transient and triggers retries</para>
    /// </remarks>
    /// <example>
    /// <code>
    /// Guid result = await webServiceClient.AddThingAsync();
    /// conn.AddAsyncErrorAction("undo add thing", async () => await webServiceClient.RemoveThingAsync(result));
    /// </code>
    /// </example>
    /// <param name="desc">description of rollback event (for logs)</param>
    /// <param name="onErrorAsync">async action to call</param>
    void AddAsyncErrorAction(string? desc, Func<Exception?, Task> onErrorAsync);
    #endregion
        
    #region Post Close Actions
    /// <summary>
    /// Add an action to execute after the action is completed (without error)
    /// </summary>
    /// <remarks>
    /// <para>These actions will be called in order after action completes successfully</para>
    /// <para>Activities that should only take place if the action completes without exception should be added here</para>
    /// </remarks>
    /// <example>
    /// <code>
    /// WriteNewPathToDB(connection, newPath);
    /// string oldPath = GetFilePath();
    /// conn.AddPostCloseAction("remove old file after close", () => File.Delete(oldPath));
    /// </code>
    /// </example>
    /// <param name="desc">description of post close event (for logs)</param>
    /// <param name="postClose">action to call</param>
    void AddPostCloseAction(string? desc, Action postClose);

    /// <summary>
    /// Add an async action to execute after the action is completed (without error)
    /// </summary>
    /// <remarks>
    /// <para>These actions will be called in order after action completes successfully</para>
    /// <para>Activities that should only take place if the action completes without exception should be added here</para>
    /// </remarks>
    /// <example>
    /// <code>
    /// WriteNewPathToDB(connection, newPath);
    /// string oldPath = GetFilePath();
    /// conn.AddPostCloseAction("remove old file after close", async () => await File.DeleteAsync(oldPath));
    /// </code>
    /// </example>
    /// <param name="desc">description of post commit event (for logs)</param>
    /// <param name="postCloseAsync">async action to call</param>
    void AddAsyncPostCloseAction(string? desc, Func<Task> postCloseAsync);
    #endregion
}
