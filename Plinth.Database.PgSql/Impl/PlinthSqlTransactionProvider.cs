using Plinth.PgSql.Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using Npgsql;
using Plinth.Database.PgSql.TransientErrorDetection;
using System.Data;

namespace Plinth.Database.PgSql.Impl;

internal class PlinthSqlTransactionProvider : ISqlTransactionProvider
{
    private readonly int _defaultCommandTimeout;
    private readonly RetryPolicy _retryPolicy;
    private readonly Action<NpgsqlDataSourceBuilder>? _configureDs;
    private readonly NpgsqlDataSource _dataSource;

    public string ConnectionString { get; }

    public PlinthSqlTransactionProvider(string connectionString, Action<NpgsqlDataSourceBuilder>? configureDs, int defaultCommandTimeout, RetryPolicy retryPolicy)
    {
        _configureDs = configureDs;
        _defaultCommandTimeout = defaultCommandTimeout;
        _retryPolicy = retryPolicy;

        ConnectionString = connectionString;

        var builder = new NpgsqlDataSourceBuilder(ConnectionString);
        _configureDs?.Invoke(builder);
        _dataSource = builder.Build();
    }

    public IDeferredSqlConnection GetDeferred()
        => new DeferredSqlConnection(_defaultCommandTimeout);

    public void ExecuteTxn(Action<ISqlConnection> action, int? commandTimeout = null, IsolationLevel? isolationLevel = null)
        => ExecuteRawTxn(action, commandTimeout, isolationLevel);

    public T ExecuteTxn<T>(Func<ISqlConnection, T> func, int? commandTimeout = null, IsolationLevel? isolationLevel = null)
        => ExecuteRawTxn(func, commandTimeout, isolationLevel);

    public void ExecuteRawTxn(Action<IRawSqlConnection> action, int? commandTimeout = null, IsolationLevel? isolationLevel = null)
    {
        ExecuteRawTxn(c =>
            {
                action.Invoke(c);
                return true;
            },
            commandTimeout,
            isolationLevel);
    }

    #region protect against sync code running async action
    public Task ExecuteTxn(Func<ISqlConnection, Task> action, int? commandTimeout = null, IsolationLevel? isolationLevel = null)
        => throw new NotSupportedException("for async transactions, use async form of ExecuteTxn or ExecuteRawTxn");
    public Task<T> ExecuteTxn<T>(Func<ISqlConnection, Task<T>> func, int? commandTimeout = null, IsolationLevel? isolationLevel = null)
        => throw new NotSupportedException("for async transactions, use async form of ExecuteTxn or ExecuteRawTxn");
    public Task ExecuteWithoutTxn(Func<INoTxnSqlConnection, Task> action, int? commandTimeout = null)
        => throw new NotSupportedException("for async transactions, use async form of ExecuteTxn or ExecuteWithoutTxn");
    public Task<T> ExecuteWithoutTxn<T>(Func<INoTxnSqlConnection, Task<T>> func, int? commandTimeout = null)
        => throw new NotSupportedException("for async transactions, use async form of ExecuteTxn or ExecuteWithoutTxn");
    public Task ExecuteRawTxn(Func<IRawSqlConnection, Task> action, int? commandTimeout = null, IsolationLevel? isolationLevel = null)
        => throw new NotSupportedException("for async transactions, use async form of ExecuteTxn or ExecuteRawTxn");
    public Task<T> ExecuteRawTxn<T>(Func<IRawSqlConnection, Task<T>> func, int? commandTimeout = null, IsolationLevel? isolationLevel = null)
        => throw new NotSupportedException("for async transactions, use async form of ExecuteTxn or ExecuteRawTxn");
    #endregion

    public T ExecuteRawTxn<T>(Func<IRawSqlConnection, T> func, int? commandTimeout = null, IsolationLevel? isolationLevel = null)
    {
        return _retryPolicy.ExecuteAction(() =>
        {
            using var conn = _dataSource.OpenConnection();

            using var txn = isolationLevel.HasValue ? conn.BeginTransaction(isolationLevel.Value) : conn.BeginTransaction();
            using var nTxn = new PlinthSqlTransaction(conn, txn, commandTimeout ?? _defaultCommandTimeout, false);
            try
            {
                return func.Invoke(nTxn.Connection);
            }
            catch (Exception)
            {
                nTxn.Connection.SetRollback();
                throw;
            }
        });
    }

    public Task ExecuteTxnAsync(Func<ISqlConnection, Task> action, int? commandTimeout = null, IsolationLevel? isolationLevel = null)
        => ExecuteRawTxnAsync(action, CancellationToken.None, commandTimeout, isolationLevel);

    public Task<T> ExecuteTxnAsync<T>(Func<ISqlConnection, Task<T>> func, int? commandTimeout = null, IsolationLevel? isolationLevel = null)
        => ExecuteRawTxnAsync(func, CancellationToken.None, commandTimeout, isolationLevel);

    public Task ExecuteTxnAsync(Func<ISqlConnection, Task> action, CancellationToken cancellationToken, int? commandTimeout = null, IsolationLevel? isolationLevel = null)
        => ExecuteRawTxnAsync(action, cancellationToken, commandTimeout, isolationLevel);

    public Task<T> ExecuteTxnAsync<T>(Func<ISqlConnection, Task<T>> func, CancellationToken cancellationToken, int? commandTimeout = null, IsolationLevel? isolationLevel = null)
        => ExecuteRawTxnAsync(func, cancellationToken, commandTimeout, isolationLevel);

    public Task ExecuteRawTxnAsync(Func<IRawSqlConnection, Task> action, int? commandTimeout = null, IsolationLevel? isolationLevel = null)
        => ExecuteRawTxnAsync(action, CancellationToken.None, commandTimeout, isolationLevel);

    public Task ExecuteRawTxnAsync(Func<IRawSqlConnection, Task> action, CancellationToken cancellationToken, int? commandTimeout = null, IsolationLevel? isolationLevel = null)
    {
        return ExecuteRawTxnAsync(async c =>
            {
                await action.Invoke(c);
                return true;
            },
            cancellationToken,
            commandTimeout,
            isolationLevel);
    }

    public Task<T> ExecuteRawTxnAsync<T>(Func<IRawSqlConnection, Task<T>> func, int? commandTimeout = null, IsolationLevel? isolationLevel = null)
        => ExecuteRawTxnAsync(func, CancellationToken.None, commandTimeout, isolationLevel);

    public Task<T> ExecuteRawTxnAsync<T>(Func<IRawSqlConnection, Task<T>> func, CancellationToken cancellationToken, int? commandTimeout = null, IsolationLevel? isolationLevel = null)
    {
        return _retryPolicy.ExecuteAsync(async () =>
        {
            await using var conn = await _dataSource.OpenConnectionAsync(cancellationToken);

            await using var txn = isolationLevel.HasValue ? await conn.BeginTransactionAsync(isolationLevel.Value, cancellationToken) : await conn.BeginTransactionAsync(cancellationToken);
            await using var nTxn = new PlinthSqlTransaction(conn, txn, commandTimeout ?? _defaultCommandTimeout, true);
            try
            {
                return await func.Invoke(nTxn.Connection);
            }
            catch (Exception)
            {
                nTxn.Connection.SetRollback();
                throw;
            }
        });
    }

    public void ExecuteDeferred(IDeferredSqlConnection deferredConn, int? commandTimeout = null, IsolationLevel? isolationLevel = null)
    {
        ExecuteRawTxn(c =>
            {
                deferredConn.ExecuteAll(c);
                return true;
            },
            commandTimeout,
            isolationLevel
        );
    }

    public Task ExecuteDeferredAsync(IDeferredSqlConnection deferredConn, int? commandTimeout = null, IsolationLevel? isolationLevel = null)
        => ExecuteDeferredAsync(deferredConn, CancellationToken.None, commandTimeout, isolationLevel);

    public Task ExecuteDeferredAsync(IDeferredSqlConnection deferredConn, CancellationToken cancellationToken, int? commandTimeout = null, IsolationLevel? isolationLevel = null)
    {
        return ExecuteRawTxnAsync(async c =>
            {
                await deferredConn.ExecuteAllAsync(c);
                return true;
            },
            cancellationToken,
            commandTimeout,
            isolationLevel
        );
    }

    public void ExecuteWithoutTxn(Action<INoTxnSqlConnection> action, int? commandTimeout = null)
    {
        ExecuteWithoutTxn(c =>
            {
                action.Invoke(c);
                return true;
            },
            commandTimeout);
    }

    public T ExecuteWithoutTxn<T>(Func<INoTxnSqlConnection, T> func, int? commandTimeout = null)
        => ExecuteRawWithoutTxn(func, commandTimeout);

    public Task ExecuteWithoutTxnAsync(Func<INoTxnSqlConnection, Task> action, int? commandTimeout = null)
        => ExecuteWithoutTxnAsync(action, CancellationToken.None, commandTimeout);

    public Task ExecuteWithoutTxnAsync(Func<INoTxnSqlConnection, Task> action, CancellationToken cancellationToken, int? commandTimeout = null)
    {
        return ExecuteWithoutTxnAsync(async c =>
            {
                await action(c);
                return true;
            },
            cancellationToken,
            commandTimeout
        );
    }

    public Task<T> ExecuteWithoutTxnAsync<T>(Func<INoTxnSqlConnection, Task<T>> action, int? commandTimeout = null)
        => ExecuteWithoutTxnAsync(action, CancellationToken.None, commandTimeout);

    public Task<T> ExecuteWithoutTxnAsync<T>(Func<INoTxnSqlConnection, Task<T>> func, CancellationToken cancellationToken, int? commandTimeout = null)
        => ExecuteRawWithoutTxnAsync(func, cancellationToken, commandTimeout);

    public void ExecuteRawWithoutTxn(Action<INoTxnRawSqlConnection> action, int? commandTimeout = null)
    {
        ExecuteRawWithoutTxn(c =>
            {
                action.Invoke(c);
                return true;
            },
            commandTimeout);
    }

    public T ExecuteRawWithoutTxn<T>(Func<INoTxnRawSqlConnection, T> func, int? commandTimeout = null)
    {
        return _retryPolicy.ExecuteAction(() =>
        {
            using var conn = _dataSource.OpenConnection();

            using var nConn = new PlinthNoTxnSqlConnection(conn, commandTimeout ?? _defaultCommandTimeout, false);
            try
            {
                var ret = func.Invoke(nConn);
                nConn.ClearError();
                return ret;
            }
            catch (Exception e)
            {
                nConn.SetError(e);
                if (nConn.HasWritten) // once we have written to the db, we cannot safely retry
                    throw new NoRetryWrapperException(e);
                else
                    throw;
            }
        });
    }

    public Task ExecuteRawWithoutTxnAsync(Func<INoTxnRawSqlConnection, Task> action, int? commandTimeout = null)
        => ExecuteRawWithoutTxnAsync(action, CancellationToken.None, commandTimeout);

    public Task ExecuteRawWithoutTxnAsync(Func<INoTxnRawSqlConnection, Task> action, CancellationToken cancellationToken, int? commandTimeout = null)
    {
        return ExecuteRawWithoutTxnAsync(async c =>
            {
                await action(c);
                return true;
            },
            cancellationToken,
            commandTimeout
        );
    }

    public Task<T> ExecuteRawWithoutTxnAsync<T>(Func<INoTxnRawSqlConnection, Task<T>> action, int? commandTimeout = null)
        => ExecuteRawWithoutTxnAsync(action, CancellationToken.None, commandTimeout);

    public Task<T> ExecuteRawWithoutTxnAsync<T>(Func<INoTxnRawSqlConnection, Task<T>> func, CancellationToken cancellationToken, int? commandTimeout = null)
    {
        return _retryPolicy.ExecuteAsync(async () =>
        {
            await using var conn = await _dataSource.OpenConnectionAsync(cancellationToken);

            await using var nConn = new PlinthNoTxnSqlConnection(conn, commandTimeout ?? _defaultCommandTimeout, true);
            try
            {
                var ret = await func.Invoke(nConn);
                nConn.ClearError();
                return ret;
            }
            catch (Exception e)
            {
                nConn.SetError(e);
                if (nConn.HasWritten) // once we have written to the db, we cannot safely retry
                    throw new NoRetryWrapperException(e);
                else
                    throw;
            }
        });

    }
}
