using System.Collections;
using System.Data;
using Microsoft.Extensions.Logging;
using Npgsql;

namespace Plinth.Database.PgSql.Impl;

internal class MultiResultSet : IMultiResultSet, IAsyncMultiResultSet
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    protected readonly IDataReader _reader;
    protected readonly NpgsqlConnection _conn;
    private bool _first;
    private readonly List<string> _rsNames = [];
    private int _rsNameIdx = 0;
    private NpgsqlCommand? _lastCmd = null;
    private ResultSet? _lastRs = null;
    private ResultSetAsync? _lastRsAsync = null;

    public MultiResultSet(IDataReader reader, NpgsqlConnection conn)
    {
        _reader = reader;
        _conn = conn;
        _first = true;
    }

    public IResultSet? NextResultSet()
    {
        if (_first)
        {
            while (_reader.Read())
            {
                _rsNames.Add(_reader.GetString(0));
            }
            _reader.Dispose();
            _first = false;
        }
        else
        {
            _lastRs?.Dispose();
            _lastCmd?.Dispose();
        }

        log.Debug("next result set");

        if (_rsNameIdx >= _rsNames.Count)
            return null;

        _lastCmd = new NpgsqlCommand($"FETCH ALL FROM \"{_rsNames[_rsNameIdx++]}\";", _conn);
        return _lastRs = new ResultSet(_lastCmd.ExecuteReader());
    }

    public async Task<IAsyncResultSet?> NextResultSetAsync(CancellationToken cancellationToken)
    {
        if (_first)
        {
            while (_reader.Read())
            {
                _rsNames.Add(_reader.GetString(0));
            }
            _reader.Dispose();
            _first = false;
        }
        else
        {
            if (_lastRsAsync != null)
                await _lastRsAsync.DisposeAsync();
            if (_lastCmd != null)
                await _lastCmd.DisposeAsync();
        }

        log.Debug("next result set");

        if (_rsNameIdx >= _rsNames.Count)
            return null;

        _lastCmd = new NpgsqlCommand($"FETCH ALL FROM \"{_rsNames[_rsNameIdx++]}\";", _conn);
        return _lastRsAsync = new ResultSetAsync(await _lastCmd.ExecuteReaderAsync(cancellationToken));
    }

    public virtual IEnumerator<IResultSet> GetEnumerator()
    {
        while (true)
        {
            log.Debug("next result set");
            var rs = NextResultSet();
            if (rs == null)
                break;
            yield return rs;
        }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Dispose()
    {
        _lastRs?.Dispose();
        _lastRsAsync?.Dispose();
        _lastCmd?.Dispose();
    }

    public async ValueTask DisposeAsync()
    {
        _lastRs?.Dispose();
        if (_lastRsAsync != null)
            await _lastRsAsync.DisposeAsync();
        if (_lastCmd != null)
            await _lastCmd.DisposeAsync();
    }
}
