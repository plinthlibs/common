using Microsoft.Extensions.Logging;
using System.Data;
using Npgsql;
using System.Runtime.CompilerServices;
using Plinth.Common.Utils;
using static Plinth.Database.PgSql.Impl.PlinthSqlConnectionCommon;

namespace Plinth.Database.PgSql.Impl;

internal partial class PlinthSqlConnection : IRawSqlConnection
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private readonly PlinthSqlTransaction _txn;
    private readonly bool _isAsync;
    private readonly Lazy<Stack<PostAction>> _rollbackActions = new();
    private readonly Lazy<List<PostAction>> _postCommitActions = new();

    private bool _rollback;

    internal PlinthSqlTransaction GetPlinthSqlTransaction() => _txn;

    /// <summary>
    /// The time in seconds to wait for a command to execute before throwing an error.
    /// </summary>
    public int CommandTimeout { get; set; }

    public PlinthSqlConnection(PlinthSqlTransaction txn, int commandTimeout, bool isAsync)
    {
        _txn = txn;
        _isAsync = isAsync;
        CommandTimeout = commandTimeout;

        _txn.CheckIsOpen();
    }

    public void SetRollback()
    {
        _rollback = true;
    }

    public bool WillBeRollingBack() => _rollback;

    public bool IsAsync() => _isAsync;

    internal void ReloadTypes() => _txn.ReloadTypes();

    internal NpgsqlConnection RawConnection => _txn.GetSqlConnection();

    #region proc

    public int ExecuteProcUnchecked(string procName, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, _txn, null, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteProc", log))
        {
            return LogRows(log, (int?)cmd.ExecuteScalar() ?? 0);
        }
    }

    public void ExecuteProc(string procName, params NpgsqlParameter?[] parameters)
    {
        if (ExecuteProcUnchecked(procName, parameters) <= 0)
            throw new DatabaseException("expected #rows > 0", DatabaseExceptionType.ExpectedRowsMismatch);
    }

    public void ExecuteProc(string procName, int expectedRows, params NpgsqlParameter?[] parameters)
    {
        int rows = ExecuteProcUnchecked(procName, parameters);
        if (rows != expectedRows)
            throw new DatabaseException($"expected {expectedRows} got {rows}", DatabaseExceptionType.ExpectedRowsMismatch);
    }

    public IEnumerable<IResult> ExecuteQueryProc(string procName, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, _txn, null, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProc", log))
        using (var rs = new ResultSet(cmd.ExecuteReader()))
        {
            foreach (var r in rs)
                yield return r;
        }
    }

    public void ExecuteQueryProcMultiResultSet(string procName, Action<IMultiResultSet> readerAction, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, _txn, null, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProcMultiResultSet", log))
        using (var mrs = new MultiResultSet(cmd.ExecuteReader(), _txn.GetSqlConnection()))
        {
            readerAction(mrs);
        }
    }

    public bool ExecuteQueryProcOne(string procName, Action<IResult> readerFunc, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, _txn, null, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProcOne", log))
        using (var rs = new ResultSet(cmd.ExecuteReader()))
        {
            return rs.GetOne(readerFunc);
        }
    }

    public SingleResult<T> ExecuteQueryProcOne<T>(string procName, Func<IResult, T> readerFunc, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, _txn, null, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProcOne", log))
        using (var rs = new ResultSet(cmd.ExecuteReader()))
        {
            return rs.GetOne(readerFunc);
        }
    }

    public List<T> ExecuteQueryProcList<T>(string procName, Func<IResult, T> readerFunc, params NpgsqlParameter?[] parameters)
    {
        return ExecuteQueryProc(procName, parameters).Select(readerFunc).ToList();
    }

    #endregion proc

    #region proc async

    public Task<int> ExecuteProcUncheckedAsync(string procName, params NpgsqlParameter?[] parameters)
        => ExecuteProcUncheckedAsync(procName, CancellationToken.None, parameters);

    public async Task<int> ExecuteProcUncheckedAsync(string procName, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, _txn, null, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteProc", log))
        {
            return await LogRowsTask(log, cmd.ExecuteScalarAsync(cancellationToken));
        }
    }

    public Task ExecuteProcAsync(string procName, params NpgsqlParameter?[] parameters)
        => ExecuteProcAsync(procName, CancellationToken.None, parameters);

    public async Task ExecuteProcAsync(string procName, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters)
    {
        int rows = await ExecuteProcUncheckedAsync(procName, cancellationToken, parameters);
        if (rows <= 0)
            throw new DatabaseException("expected #rows > 0", DatabaseExceptionType.ExpectedRowsMismatch);
    }

    public Task ExecuteProcAsync(string procName, int expectedRows, params NpgsqlParameter?[] parameters)
        => ExecuteProcAsync(procName, expectedRows, CancellationToken.None, parameters);

    public async Task ExecuteProcAsync(string procName, int expectedRows, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters)
    {
        int rows = await ExecuteProcUncheckedAsync(procName, cancellationToken, parameters);
        if (rows != expectedRows)
            throw new DatabaseException($"expected {expectedRows} got {rows}", DatabaseExceptionType.ExpectedRowsMismatch);
    }

    public Task<bool> ExecuteQueryProcOneAsync(string procName, Func<IResult, Task> readerFunc, params NpgsqlParameter?[] parameters)
        => ExecuteQueryProcOneAsync(procName, readerFunc, CancellationToken.None, parameters);

    public async Task<bool> ExecuteQueryProcOneAsync(string procName, Func<IResult, Task> readerFunc, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, _txn, null, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProcOneAsync", log))
        await using (var rs = new ResultSetAsync(await cmd.ExecuteReaderAsync(cancellationToken)))
        {
            return await rs.GetOneAsync(readerFunc, cancellationToken);
        }
    }

    public Task<SingleResult<T>> ExecuteQueryProcOneAsync<T>(string procName, Func<IResult, Task<T>> readerFunc, params NpgsqlParameter?[] parameters)
        => ExecuteQueryProcOneAsync(procName, readerFunc, CancellationToken.None, parameters);

    public async Task<SingleResult<T>> ExecuteQueryProcOneAsync<T>(string procName, Func<IResult, Task<T>> readerFunc, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, _txn, null, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProcOneAsync", log))
        await using (var rs = new ResultSetAsync(await cmd.ExecuteReaderAsync(cancellationToken)))
        {
            return await rs.GetOneAsync(readerFunc, cancellationToken);
        }
    }

    public Task<List<T>> ExecuteQueryProcListAsync<T>(string procName, Func<IResult, Task<T>> readerFunc, params NpgsqlParameter?[] parameters)
        => ExecuteQueryProcListAsync(procName, readerFunc, CancellationToken.None, parameters);

    public async Task<List<T>> ExecuteQueryProcListAsync<T>(string procName, Func<IResult, Task<T>> readerFunc, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, _txn, null, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProcListAsync", log))
        await using (var rs = new ResultSetAsync(await cmd.ExecuteReaderAsync(cancellationToken)))
        {
            return await rs.GetListAsync(readerFunc, cancellationToken);
        }
    }

    public Task ExecuteQueryProcMultiResultSetAsync(string procName, Func<IAsyncMultiResultSet, Task> readerFunc, params NpgsqlParameter?[] parameters)
        => ExecuteQueryProcMultiResultSetAsync(procName, readerFunc, CancellationToken.None, parameters);

    public async Task ExecuteQueryProcMultiResultSetAsync(string procName, Func<IAsyncMultiResultSet, Task> readerFunc, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, _txn, null, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProcMultiResultSetAsync", log))
        using (var mrs = new MultiResultSet(await cmd.ExecuteReaderAsync(cancellationToken), _txn.GetSqlConnection()))
        {
            await readerFunc(mrs);
        }
    }

    public IAsyncEnumerable<IResult> ExecuteQueryProcAsync(string procName, params NpgsqlParameter?[] parameters)
        => ExecuteQueryProcAsync(procName, CancellationToken.None, parameters);

    public async IAsyncEnumerable<IResult> ExecuteQueryProcAsync(string procName, [EnumeratorCancellation] CancellationToken cancellationToken, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, procName, CommandType.StoredProcedure, _txn, null, CommandTimeout, parameters))
        using (log.BeginScope(ProcMeta(procName)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProc", log))
        using (var rs = new ResultSetAsync(await cmd.ExecuteReaderAsync(cancellationToken)))
        {
            await foreach (var r in rs)
                yield return r;
        }
    }

    #endregion proc async

    #region raw

    public int ExecuteRaw(string sql, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, _txn, null, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRaw", log))
        {
            return LogRows(log, cmd.ExecuteNonQuery());
        }
    }

    public IEnumerable<IResult> ExecuteRawQuery(string sql, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, _txn, null, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRawQuery", log))
        using (var rs = new ResultSet(cmd.ExecuteReader()))
        {
            foreach (var r in rs)
                yield return r;
        }
    }

    public List<T> ExecuteRawQueryList<T>(string sql, Func<IResult, T> readerFunc, params NpgsqlParameter?[] parameters)
    {
        return ExecuteRawQuery(sql, parameters).Select(readerFunc).ToList();
    }

    public bool ExecuteRawQueryOne(string sql, Action<IResult> readerFunc, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, _txn, null, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryOne", log))
        using (var rs = new ResultSet(cmd.ExecuteReader()))
        {
            return rs.GetOne(readerFunc);
        }
    }

    public SingleResult<T> ExecuteRawQueryOne<T>(string sql, Func<IResult, T> readerFunc, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, _txn, null, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryOne", log))
        using (var rs = new ResultSet(cmd.ExecuteReader()))
        {
            return rs.GetOne(readerFunc);
        }
    }

    public void ExecuteRawQueryMultiResultSet(string sql, Action<IMultiResultSet> readerAction, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, _txn, null, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryMultiResultSet", log))
        using (var mrs = new MultiResultSet(cmd.ExecuteReader(), _txn.GetSqlConnection()))
        {
            readerAction(mrs);
        }
    }

    #endregion raw

    #region raw async

    public Task<int> ExecuteRawAsync(string sql, params NpgsqlParameter?[] parameters)
        => ExecuteRawAsync(sql, CancellationToken.None, parameters);

    public async Task<int> ExecuteRawAsync(string sql, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, _txn, null, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRawAsync", log))
        {
            return LogRows(log, await cmd.ExecuteNonQueryAsync(cancellationToken));
        }
    }

    public Task<List<T>> ExecuteRawQueryListAsync<T>(string sql, Func<IResult, Task<T>> readerFunc, params NpgsqlParameter?[] parameters)
        => ExecuteRawQueryListAsync(sql, readerFunc, CancellationToken.None, parameters);

    public async Task<List<T>> ExecuteRawQueryListAsync<T>(string sql, Func<IResult, Task<T>> readerFunc, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, _txn, null, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryListAsync", log))
        await using (var rs = new ResultSetAsync(await cmd.ExecuteReaderAsync(cancellationToken)))
        {
            return await rs.GetListAsync(readerFunc);
        }
    }

    public Task<bool> ExecuteRawQueryOneAsync(string sql, Func<IResult, Task> readerFunc, params NpgsqlParameter?[] parameters)
        => ExecuteRawQueryOneAsync(sql, readerFunc, CancellationToken.None, parameters);

    public async Task<bool> ExecuteRawQueryOneAsync(string sql, Func<IResult, Task> readerFunc, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, _txn, null, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryOneAsync", log))
        await using (var rs = new ResultSetAsync(await cmd.ExecuteReaderAsync(cancellationToken)))
        {
            return await rs.GetOneAsync(readerFunc);
        }
    }

    public Task<SingleResult<T>> ExecuteRawQueryOneAsync<T>(string sql, Func<IResult, Task<T>> readerFunc, params NpgsqlParameter?[] parameters)
        => ExecuteRawQueryOneAsync(sql, readerFunc, CancellationToken.None, parameters);

    public async Task<SingleResult<T>> ExecuteRawQueryOneAsync<T>(string sql, Func<IResult, Task<T>> readerFunc, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, _txn, null, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryOneAsync", log))
        await using (var rs = new ResultSetAsync(await cmd.ExecuteReaderAsync(cancellationToken)))
        {
            return await rs.GetOneAsync(readerFunc);
        }
    }

    public Task ExecuteRawQueryMultiResultSetAsync(string sql, Func<IAsyncMultiResultSet, Task> readerFunc, params NpgsqlParameter?[] parameters)
        => ExecuteRawQueryMultiResultSetAsync(sql, readerFunc, CancellationToken.None, parameters);

    public async Task ExecuteRawQueryMultiResultSetAsync(string sql, Func<IAsyncMultiResultSet, Task> readerFunc, CancellationToken cancellationToken, params NpgsqlParameter?[] parameters)
    {
        using (var cmd = PrepareCmd(log, sql, CommandType.Text, _txn, null, CommandTimeout, parameters))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryMultiResultSetAsync", log))
        await using (var mrs = new MultiResultSet(await cmd.ExecuteReaderAsync(cancellationToken), RawConnection))
        {
            await readerFunc(mrs);
        }
    }

    #endregion raw async

    #region rollback

    internal class PostAction
    {
        public Action? action;
        public Func<Task>? actionAsync;
        public string? Desc;
    }

    public void AddRollbackAction(string? desc, Action onRollback)
    {
        if (onRollback.Method.IsDefined(typeof(AsyncStateMachineAttribute), false))
            throw new NotSupportedException("for async actions, use AddAsyncRollbackAction");

        _rollbackActions.Value.Push(new PostAction { action = onRollback, Desc = desc });
    }

    public void AddAsyncRollbackAction(string? desc, Func<Task> onRollbackAsync)
    {
        _rollbackActions.Value.Push(new PostAction { actionAsync = onRollbackAsync, Desc = desc });
    }

    internal async Task ProcessRollbackActions()
    {
        var stack = _rollbackActions.Value;

        while (stack.Count > 0)
        {
            var action = stack.Pop();

            LogDefines.LogRollbackAction(log, action.Desc);

            if (action.action != null)
            {
                try
                {
                    action.action();
                }
                catch (Exception e)
                {
                    LogDefines.LogErrorRollbackAction(log, e);
                }
            }
            else if (action.actionAsync != null)
            {
                try
                {
                    await action.actionAsync();
                }
                catch (Exception e)
                {
                    LogDefines.LogErrorRollbackAsyncAction(log, e);
                }
            }
            else
                throw new InvalidOperationException("no rollback action");
        }
    }
    #endregion

    #region post commit
    public void AddPostCommitAction(string? desc, Action postCommit)
    {
        if (postCommit.Method.IsDefined(typeof(AsyncStateMachineAttribute), false))
            throw new NotSupportedException("for async actions, use AddAsyncPostCommitAction");

        _postCommitActions.Value.Add(new PostAction { action = postCommit, Desc = desc });
    }

    public void AddAsyncPostCommitAction(string? desc, Func<Task> postCommitAsync)
    {
        _postCommitActions.Value.Add(new PostAction { actionAsync = postCommitAsync, Desc = desc });
    }

    internal async Task ProcessPostCommitActions()
    {
        var list = _postCommitActions.Value;

        foreach (var action in list)
        {
            LogDefines.LogPostCommitAction(log, action.Desc);

            if (action.action != null)
            {
                try
                {
                    action.action();
                }
                catch (Exception e)
                {
                    LogDefines.LogErrorPostCommitAction(log, e);
                }
            }
            else if (action.actionAsync != null)
            {
                try
                {
                    await action.actionAsync();
                }
                catch (Exception e)
                {
                    LogDefines.LogErrorPostCommitAsyncAction(log, e);
                }
            }
            else
                throw new InvalidOperationException("no Post Commit action");
        }
    }
    #endregion

    private static partial class LogDefines
    {
        [LoggerMessage(1, LogLevel.Information, "Processing Rollback Action: {ActionDesc}", EventName = "RollbackAction")]
        public static partial void LogRollbackAction(ILogger logger, string? actionDesc);

        [LoggerMessage(2, LogLevel.Error, "Rollback Action threw exception", EventName = "ErrorRollbackAction")]
        public static partial void LogErrorRollbackAction(ILogger logger, Exception? e);

        [LoggerMessage(3, LogLevel.Error, "Rollback (async) Action threw exception", EventName = "ErrorRollbackAsyncAction")]
        public static partial void LogErrorRollbackAsyncAction(ILogger logger, Exception? e);

        [LoggerMessage(4, LogLevel.Information, "Processing PostCommit Action: {ActionDesc}", EventName = "PostCommitAction")]
        public static partial void LogPostCommitAction(ILogger logger, string? actionDesc);

        [LoggerMessage(5, LogLevel.Error, "PostCommit Action threw exception", EventName = "ErrorPostCommitAction")]
        public static partial void LogErrorPostCommitAction(ILogger logger, Exception? e);

        [LoggerMessage(6, LogLevel.Error, "PostCommit (async) Action threw exception", EventName = "ErrorPostCommitAsyncAction")]
        public static partial void LogErrorPostCommitAsyncAction(ILogger logger, Exception? e);
    }
}
