using Npgsql;
using Microsoft.Extensions.Logging;
using Plinth.Common.Logging;
using System.Data;

using static Plinth.Common.Logging.Scopes;

namespace Plinth.Database.PgSql.Impl;

internal static partial class PlinthSqlConnectionCommon
{
    public static NpgsqlCommand PrepareCmd(
        ILogger log,
        string commandText,
        CommandType type,
        PlinthSqlTransaction? txn,
        NpgsqlConnection? conn,
        int commandTimeout,
        NpgsqlParameter?[] parameters)
    {
        ArgumentNullException.ThrowIfNull(commandText);

        NpgsqlCommand cmd = txn == null
            ? forConn(conn ?? throw new InvalidOperationException(), commandText, type, commandTimeout)
            : forTxn(commandText, type, txn, commandTimeout);

        foreach (var p in parameters)
        {
            if (p == null)
                continue;
            SqlParameterProcessor.ProcessParam(p);
            cmd.Parameters.Add(p);
        }

        if (log.IsDebugEnabled())
        {
            if (cmd.CommandType == CommandType.StoredProcedure)
                log.Debug(StoredProcLogger.ExecToString(commandText, parameters));
            else
                log.Debug(StoredProcLogger.RawExecToString(commandText, parameters));
        }

        return cmd;

        static NpgsqlCommand forTxn(string commandText, CommandType type, PlinthSqlTransaction txn, int commandTimeout)
        {
            txn.CheckIsOpen();

            return txn.GetCommand(commandText, type, commandTimeout);
        }

        static NpgsqlCommand forConn(NpgsqlConnection conn, string commandText, CommandType type, int commandTimeout)
        {
            return new NpgsqlCommand()
            {
                Connection = conn,
                CommandText = commandText,
                CommandTimeout = commandTimeout,
                CommandType = type
            };
        }

#if FALSE
        // npgsql 7.0.0 changed 'CommandType.StoredProcedure' to execute stored procedures instead of functions
        // the problem is that this framework expects results (from query function) and row counts (from exec functions)
        // stored procedures can't return results except through OUT parameters which doesn't fit the model
        // one option is to set an AppContext switch, the other is this (the switch is global for all uses of npgsql in the process)
        // https://github.com/npgsql/npgsql/issues/3913
        // https://www.npgsql.org/doc/release-notes/7.0.html#a-namecommandtypestoredprocedure-commandtypestoredprocedure-now-invokes-procedures-instead-of-functions
        // the code below converts a func call to a raw sql call by mapping the function parameters to the passed in ones
        // much of this came from npgsql source
        if (type == CommandType.StoredProcedure)
        {
            NpgsqlCommandBuilder.DeriveParameters(cmd);

            var paramDict = new Dictionary<string, int>();
            for (int j = 0; j < cmd.Parameters.Count; j++)
                paramDict[cmd.Parameters[j].ParameterName] = j;
            var specified = new HashSet<int>();

            foreach (var p in parameters)
            {
                var pname = p.ParameterName ?? string.Empty;
                if (pname.Length > 0 && (pname[0] == ':' || pname[0] == '@'))
                    pname = pname[1..];
                if (paramDict.TryGetValue(pname, out var cmdParamIdx))
                {
                    p.ParameterName = pname;
                    cmd.Parameters[cmdParamIdx] = p;
                    specified.Add(cmdParamIdx);
                }
            }

            // https://github.com/npgsql/npgsql/blob/b73f42e43a278d93a514ca09b1118119a614e88a/src/Npgsql/NpgsqlCommand.cs#L903
            var sqlBuilder = new StringBuilder()
                .Append("SELECT * FROM ")
                .Append(commandText)
                .Append('(');

            var inputParameters = new List<NpgNpgsqlParameter?>(cmd.Parameters.Count);

            for (var i = 0; i < cmd.Parameters.Count; i++)
            {
                var parameter = cmd.Parameters[i];

                // With functions, output parameters are never present when calling the function (they only define the schema of the
                // returned table). With stored procedures they must be specified in the CALL argument list (see below).
                if (parameter.Direction == ParameterDirection.Output)
                    continue;

                // unspecified parameters are not included in the call, hopefully the parameter has a default set
                if (!specified.Contains(i))
                    continue;

                sqlBuilder
                    .Append('"')
                    .Append(parameter.ParameterName.Replace("\"", "\"\""))
                    .Append("\" := ");

                if (parameter.Direction == ParameterDirection.Output)
                    sqlBuilder.Append("NULL");
                else
                {
                    NpgsqlParameter?Processor.ProcessParam(parameter);
                    inputParameters.Add(parameter);
                    sqlBuilder.Append('$').Append(inputParameters.Count);
                }

                sqlBuilder.Append(',');
            }

            if (sqlBuilder[^1] == ',')
                sqlBuilder.Length -= 1;
            sqlBuilder.Append(')');

            commandText = sqlBuilder.ToString();
            type = CommandType.Text;

            if (log.IsDebugEnabled())
                log.Debug(StoredProcLogger.RawExecToString(commandText, inputParameters));

            var newCmd = _txn.GetCommand(commandText, type, CommandTimeout);
            foreach (var p in inputParameters)
            {
                p.Collection = null;
                p.ParameterName = null;
                newCmd.Parameters.Add(p);
            }

            return newCmd;
        }
        else
        {
            foreach (var p in parameters)
            {
                if (p == null)
                    continue;
                NpgsqlParameter?Processor.ProcessParam(p);
                cmd.Parameters.Add(p);
            }

            if (log.IsDebugEnabled())
                log.Debug(StoredProcLogger.RawExecToString(commandText, parameters));

            return cmd;
        }
#endif
    }

    public static KeyValuePair<string, object?>[] ProcMeta(string procName)
    {
        return CreateScope(
            Kvp( "StoredProc", procName ),
            Kvp( LoggingConstants.Field_TimeOperation, procName )
        );
    }

    #region rowcount logging
    public static int LogRows(ILogger log, int rows)
    {
        LogDefinesCommon.LogRowsAffected(log, rows);
        return rows;
    }

    public static async Task<int> LogRowsTask(ILogger log, Task<object?> t) => LogRows(log, (int?)await t ?? 0);
    #endregion

    private static partial class LogDefinesCommon
    {
        [LoggerMessage(99, LogLevel.Debug, "{RowCount} row(s) affected", EventName = "RowsAffected")]
        public static partial void LogRowsAffected(ILogger logger, int rowCount);
    }
}
