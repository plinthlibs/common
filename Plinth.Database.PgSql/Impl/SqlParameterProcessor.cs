using System.Data;
using Npgsql;

namespace Plinth.Database.PgSql.Impl;

internal static class SqlParameterProcessor
{
    /// <summary>
    /// Process any changes necessary for SqlParameters
    /// </summary>
    /// <remarks>
    /// <list>
    ///   <item>DateTime => to UTC, if starting with <c>SqlConstants.NoUtcDateTimeHeader</c> skip UTC conversion</item>
    ///   <item>Enums => to string plus warning</item>
    ///   <item>Strings => If starting with <c>SqlConstants.DoNotTrimHeader</c>, value will not be trimmed, Otherwise, .Trim()</item>
    ///   <item>Objects => By default, null won't be passed to SQL, prepend <c>SqlConstants.NullableHeader</c> to explicitly convert to DBNull.Value</item>
    /// </list>
    /// </remarks>
    /// <param name="param">[in/out] will be modified if needed</param>
    public static void ProcessParam(NpgsqlParameter param)
    {
        // nullable parameters must explicitly set DBNull.Value for null values
        param.Value ??= DBNull.Value;

        // null values require no processing
        if (param.Value == DBNull.Value)
        {
            ProcessDoNotTrim(param, null);
            return;
        }

        // ensure all datetimes are UTC
        if (param.DbType == DbType.DateTime || param.DbType == DbType.DateTime2 || param.Value is DateTime)
        {
            // allow override of UTC datetime conversion
            if (param.ParameterName.StartsWith(SqlConstants.NoUtcDateTimeHeader))
                param.ParameterName = param.ParameterName[SqlConstants.NoUtcDateTimeHeader.Length..];
            else
            {
                param.Value = ((DateTime)param.Value).ToUniversalTime();
                param.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.TimestampTz;
            }
            return;
        }

        // enums should be .ToString()'d before getting here
        if (param.Value.GetType().IsEnum)
        {
            param.Value = param.Value.ToString();
            return;
        }

        // strings are automatically trimmed, unless the parameter name starts with the DoNotTrimHeader
        if (param.Value is string s)
        {
            ProcessDoNotTrim(param, s);
            return;
        }
    }

    private static void ProcessDoNotTrim(NpgsqlParameter param, string? s)
    {
        if (param.ParameterName.StartsWith(SqlConstants.DoNotTrimHeader))
            param.ParameterName = param.ParameterName[SqlConstants.DoNotTrimHeader.Length..];
        else
        {
            if (s != null)
                param.Value = s.Trim();
        }
    }
}
