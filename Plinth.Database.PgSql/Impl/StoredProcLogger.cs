using System.Data;
using Npgsql;
using System.Globalization;
using System.Text;

namespace Plinth.Database.PgSql.Impl;

internal static class StoredProcLogger
{
    /// <summary>
    /// Converts a stored procedure execution to string so it can be logged
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="parameters"></param>
    /// <returns>a string containing executable sql</returns>
    public static string ExecToString(string procName, IEnumerable<NpgsqlParameter?> parameters)
    {
        var sb = new StringBuilder(128);
        sb.AppendFormat("SELECT * FROM {0}(", procName);

        sb.Append(string.Join(", ", parameters.Where(p => p != null).Select((p, i) => ParamToString(i, p!))));

        sb.Append(')');

        return sb.ToString();
    }

    /// <summary>
    /// Converts a raw sql call to a string to it can be logged
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns>a string containing the sql followed by the parameters</returns>
    public static string RawExecToString(string sql, IEnumerable<NpgsqlParameter?> parameters)
    {
        var sb = new StringBuilder(128);
        sb.Append(sql);

        int mark = sb.Length;
        sb.Append(" ==> VALUES (");
        string paramStr = ProcessParams(parameters);
        sb.Append(paramStr);
        sb.Append(')');

        if (string.IsNullOrEmpty(paramStr))
            sb.Length = mark;

        return sb.ToString();
    }

    private static string ProcessParams(IEnumerable<NpgsqlParameter?> parameters)
        => string.Join(", ", parameters.Where(p => p != null).Select((p, i) => ParamToString(i, p!)));

    private static string ParamToString(int position, NpgsqlParameter p)
    {
        var paramName = string.IsNullOrEmpty(p.ParameterName)
            ? $"${position + 1}"
            : (p.ParameterName[0] == '@' ? p.ParameterName[1..] : p.ParameterName);
        if (p.Direction == ParameterDirection.Input)
            return $"{paramName} := {ValueToString(p)}";
        else
            return $"{paramName} := {ValueToString(p)} OUTPUT";
    }
    
    private static string ValueToString(NpgsqlParameter p)
    {
        if (p.Value == null || p.Value == DBNull.Value)
            return "NULL";
        else if (p.Value is string || p.Value is Guid)
            return $"'{p.Value}'";
        else if (p.Value is bool b)
            return $"{(b ? '1' : '0')}";
        else if (p.Value is DateTime dt)
            return $"'{dt.ToString("o", CultureInfo.InvariantCulture)}'";
        else
            return p.Value.ToString() ?? "<null>";
    }
}
