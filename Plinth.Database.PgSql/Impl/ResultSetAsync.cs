using System.Data;
using Npgsql;
using Microsoft.Extensions.Logging;

namespace Plinth.Database.PgSql.Impl;

internal partial class ResultSetAsync : ResultSet, IAsyncResultSet, IAsyncDisposable
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private readonly NpgsqlDataReader _sqlReader;
    private int rows = 0;
    private readonly bool _allowDispose;

    public ResultSetAsync(IDataReader reader, bool allowDispose = true) : base(reader, allowDispose)
    {
        _sqlReader = (NpgsqlDataReader)reader;
        _allowDispose = allowDispose;
    }

    public override IResult NextResult()
    {
        throw new NotSupportedException("NextResult not supported for async");
    }

    public override IEnumerator<IResult> GetEnumerator()
    {
        throw new NotSupportedException("GetEnumerator not supported for async");
    }

    public async IAsyncEnumerator<IResult> GetAsyncEnumerator(CancellationToken cancellationToken = default)
    {
        while (!await _sqlReader.ReadAsync(cancellationToken))
        {
            rows++;
            yield return this;
        }

        LogDefines.LogRows(log, rows);
    }

    public async Task<IResult?> NextResultAsync(CancellationToken cancellationToken = default)
    {
        if (!await _sqlReader.ReadAsync(cancellationToken))
        {
            LogDefines.LogRows(log, rows);
            return null;
        }

        rows++;
        return this;
    }

    internal override void Reset()
    {
        rows = 0;
    }

    public Task<SingleResult<T>> GetOneAsync<T>(Func<IResult, Task<T>> readerFunc)
        => GetOneAsync(readerFunc, CancellationToken.None);

    public async Task<SingleResult<T>> GetOneAsync<T>(Func<IResult, Task<T>> readerFunc, CancellationToken cancellationToken)
    {
        var r = await NextResultAsync(cancellationToken);
        if (r != null)
        {
            LogDefines.LogRows(log, 1);
            return new SingleResult<T>(await readerFunc.Invoke(this), true);
        }
        return new SingleResult<T>(default, false);
    }

    public Task<List<T>> GetListAsync<T>(Func<IResult, Task<T>> readerFunc)
        => GetListAsync(readerFunc, CancellationToken.None);

    public async Task<List<T>> GetListAsync<T>(Func<IResult, Task<T>> readerFunc, CancellationToken cancellationToken)
    {
        List<T> result = [];
        while (await NextResultAsync(cancellationToken) != null)
        {
            result.Add(await readerFunc(this));
        }
        return result;
    }

    public Task<bool> GetOneAsync(Func<IResult, Task> readerFunc)
        => GetOneAsync(readerFunc, CancellationToken.None);

    public async Task<bool> GetOneAsync(Func<IResult, Task> readerFunc, CancellationToken cancellationToken)
    {
        var r = await NextResultAsync(cancellationToken);
        if (r != null)
        {
            await readerFunc.Invoke(r);
            LogDefines.LogRows(log, 1);
            return true;
        }
        return false;
    }

    public async ValueTask DisposeAsync()
    {
        if (_allowDispose)
            await _sqlReader.DisposeAsync();
    }

    private static partial class LogDefines
    {
        [LoggerMessage(0, LogLevel.Debug, "{RowCount} row(s) returned", EventName = "Rows")]
        public static partial void LogRows(ILogger logger, int rowCount);
    }
}
