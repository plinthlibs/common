using Newtonsoft.Json;

namespace Plinth.Database.PgSql;

/// <summary>
/// Represents a single row result from a query.
/// </summary>
public interface IResult
{
    /// <summary>get object column</summary>
    object? GetValue(string name);

    /// <summary>get string column</summary>
    string? GetString(string name);

    /// <summary>get byte column</summary>
    byte GetByte(string name);

    /// <summary>get nullable byte column</summary>
    byte? GetByteNull(string name);

    /// <summary>get short column</summary>
    short GetShort(string name);

    /// <summary>get nullable short column</summary>
    short? GetShortNull(string name);

    /// <summary>get int column</summary>
    int GetInt(string name);

    /// <summary>get nullable int column</summary>
    int? GetIntNull(string name);

    /// <summary>get long column</summary>
    long GetLong(string name);

    /// <summary>get nullable long column</summary>
    long? GetLongNull(string name);

    /// <summary>get decimal column</summary>
    decimal GetDecimal(string name);

    /// <summary>get nullable decimal column</summary>
    decimal? GetDecimalNull(string name);

    /// <summary>get double column</summary>
    double GetDouble(string name);

    /// <summary>get nullable double column</summary>
    double? GetDoubleNull(string name);

    /// <summary>get bool column</summary>
    bool GetBool(string name);

    /// <summary>get nullable bool column</summary>
    bool? GetBoolNull(string name);

    /// <summary>get DateTime column</summary>
    DateTime GetDateTime(string name, DateTimeKind kind = DateTimeKind.Utc);

    /// <summary>get nullable DateTime column</summary>
    DateTime? GetDateTimeNull(string name, DateTimeKind kind = DateTimeKind.Utc);

    /// <summary>get TimeSpan column</summary>
    TimeSpan GetTimeSpan(string name);

    /// <summary>get nullable TimeSpan column</summary>
    TimeSpan? GetTimeSpanNull(string name);

    /// <summary>get Guid column</summary>
    Guid GetGuid(string name);

    /// <summary>get nullable Guid column</summary>
    Guid? GetGuidNull(string name);

    /// <summary>get Enum column, value must match Enum value name</summary>
    T GetEnum<T>(string name) where T : struct, Enum;

    /// <summary>get Enum column, or default value if NULL</summary>
    T GetEnumDefault<T>(string name, T defaultVal) where T : struct, Enum;

    /// <summary>get Enum column, null if NULL</summary>
    T? GetEnumNull<T>(string name) where T : struct, Enum;

    /// <summary>Get bytes from varbinary</summary>
    byte[]? GetBytes(string name);

    /// <summary>Get stream from varbinary</summary>
    Stream? GetStream(string name);

    /// <summary>Get Dictionary of string,object from json column</summary>
    IDictionary<string, object>? GetJson(string name, Action<JsonSerializerSettings>? m = null);

    /// <summary>Get object from json column</summary>
    T? GetJsonObject<T>(string name, Action<JsonSerializerSettings>? m = null);

    /// <summary>check if column is NULL</summary>
    bool IsNull(string name);

    /// <summary>Get a list of column names</summary>
    List<string> GetColumns();

    /// <summary>Get a list of column values for current row</summary>
    List<object> GetValues();
}
