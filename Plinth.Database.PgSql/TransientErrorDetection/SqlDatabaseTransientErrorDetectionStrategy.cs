#region license
// ==============================================================================
// Microsoft patterns & practices Enterprise Library
// Transient Fault Handling Application Block
// ==============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
// ==============================================================================
#endregion


namespace Plinth.PgSql.Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;

using Npgsql;

/// <summary>
/// Provides the transient error detection logic for transient faults that are specific to SQL Database.
/// </summary>
internal sealed class SqlDatabaseTransientErrorDetectionStrategy : ITransientErrorDetectionStrategy
{
    /// <summary>
    /// Determines whether the specified exception represents a transient failure that can be compensated by a retry.
    /// </summary>
    /// <param name="ex">The exception object to be verified.</param>
    /// <returns>true if the specified exception is considered as transient; otherwise, false.</returns>
    public bool IsTransient(Exception? ex)
    {
        if (ex is NpgsqlException nex)
        {
            if (nex.IsTransient)
                return true;

            if (nex is PostgresException pex)
                return pex.SqlState == PostgresErrorCodes.DeadlockDetected;
        }
        else if (ex is TimeoutException)
            return true;

        return false;
    }
}
