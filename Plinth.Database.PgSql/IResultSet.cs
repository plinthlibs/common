﻿namespace Plinth.Database.PgSql;

/// <summary>
/// A set of results (rows) from a query
/// </summary>
public interface IResultSet : IDisposable, IEnumerable<IResult>
{
    /// <summary>
    /// Get the next result (row) from the set
    /// </summary>
    IResult? NextResult();

    /// <summary>
    /// Get a single row value
    /// </summary>
    /// <typeparam name="T">type of the objects in the list</typeparam>
    /// <param name="readerFunc">A function that converts a row to an object (only called if a row was returned)</param>
    /// <returns>A SqlSingleResult containing the return value from readerFunc, or default if no results returned,
    /// and a bool containing whether a row was returned or not</returns>
    SingleResult<T> GetOne<T>(Func<IResult, T> readerFunc);

    /// <summary>
    /// Get a single row
    /// </summary>
    /// <param name="readerFunc">An action that will be called with a result (only called if a row was returned)</param>
    /// <returns>true if a row was found, false otherwise</returns>
    bool GetOne(Action<IResult> readerFunc);

    /// <summary>
    /// Get a List of objects mapped from results
    /// </summary>
    /// <typeparam name="T">type of the objects in the list</typeparam>
    /// <param name="readerFunc">A function that converts a row to an object (only called if a row was returned)</param>
    /// <returns>A List of objects, always non-null</returns>
    List<T> GetList<T>(Func<IResult, T> readerFunc);
}

/// <summary>
/// A set of results (rows) from a query
/// </summary>
public interface IAsyncResultSet : IAsyncDisposable, IAsyncEnumerable<IResult>
{
    /// <summary>
    /// Get the next result (row) from the set
    /// </summary>
    Task<IResult?> NextResultAsync(CancellationToken cancellationToken = default);

    /// <summary>
    /// Get a single row value
    /// </summary>
    /// <typeparam name="T">type of the objects in the list</typeparam>
    /// <param name="readerFunc">A function that converts a row to an object (only called if a row was returned)</param>
    /// <param name="cancellationToken"></param>
    /// <returns>A SqlSingleResult containing the return value from readerFunc, or default if no results returned,
    /// and a bool containing whether a row was returned or not</returns>
    Task<SingleResult<T>> GetOneAsync<T>(Func<IResult, Task<T>> readerFunc, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get a single row
    /// </summary>
    /// <param name="readerFunc">An action that will be called with a result (only called if a row was returned)</param>
    /// <param name="cancellationToken"></param>
    /// <returns>true if a row was found, false otherwise</returns>
    Task<bool> GetOneAsync(Func<IResult, Task> readerFunc, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get a List of objects mapped from results
    /// </summary>
    /// <typeparam name="T">type of the objects in the list</typeparam>
    /// <param name="readerFunc">A function that converts a row to an object (only called if a row was returned)</param>
    /// <param name="cancellationToken"></param>
    /// <returns>A List of objects, always non-null</returns>
    Task<List<T>> GetListAsync<T>(Func<IResult, Task<T>> readerFunc, CancellationToken cancellationToken = default);
}
