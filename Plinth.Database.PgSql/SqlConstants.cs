﻿namespace Plinth.Database.PgSql;

/// <summary>
/// Constants for DB Code
/// </summary>
public static class SqlConstants
{
    /// <summary>
    /// Put this in front of the parameter name to prevent auto-trimming
    /// </summary>
    /// <example>
    /// <code>new SqlParameter(SqlConstants.DoNotTrimHeader + "@ParamName", valueStr);</code>
    /// </example>
    public const string DoNotTrimHeader = "NOTRIM-";

    /// <summary>
    /// Put this in front of the parameter name to explicitly disable conversion to UTC
    /// </summary>
    /// <example>
    /// <code>new SqlParameter(SqlConstants.NoUtcDateTimeHeader + "@ParamName", valueDt);</code>
    /// </example>
    public const string NoUtcDateTimeHeader = "NOUTCCONVERT-";
}
