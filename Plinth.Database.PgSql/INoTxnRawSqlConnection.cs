namespace Plinth.Database.PgSql;

/// <summary>
/// Interface for executing RAW SQL as well as stored procedures without a transaction
/// </summary>
/// <remarks>
/// This kind of connection will work like IRawSqlConnection, but you can only do a single write operation
/// </remarks>
public interface INoTxnRawSqlConnection : IRawSqlConnectionBase, INoTxnSqlConnection
{
}
