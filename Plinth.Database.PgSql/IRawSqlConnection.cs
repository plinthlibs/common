using Npgsql;

namespace Plinth.Database.PgSql;

/// <summary>
/// Interface for executing RAW SQL as well as stored procedures
/// </summary>
public interface IRawSqlConnection : ISqlConnection, IRawSqlConnectionBase
{
}
