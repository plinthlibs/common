using Npgsql;

namespace Plinth.Database.PgSql;

/// <summary>
/// Interface for SQL Connections
/// </summary>
public interface ISqlConnection : ISqlConnectionBase
{
    /// <summary>
    /// Set this connection to roll back at the end of a transaction
    /// without having to throw an exception
    /// </summary>
    void SetRollback();

    /// <summary>
    /// Determine if this connection/transaction has been set for rollback
    /// </summary>
    bool WillBeRollingBack();

    #region Rollback Actions
    /// <summary>
    /// Add an action to execute if the transaction is rolled back
    /// </summary>
    /// <remarks>
    /// <para>These actions will be called in reverse order if the transaction rolls back</para>
    /// <para>Activities that are not part of the DB transaction should include a call to this function to undo on rollback</para>
    /// </remarks>
    /// <example>
    /// <code>
    /// Guid result = webServiceClient.AddThing();
    /// conn.AddRollbackAction("undo add thing", () => webServiceClient.RemoveThing(result));
    /// </code>
    /// </example>
    /// <param name="desc">description of rollback event (for logs)</param>
    /// <param name="onRollback">action to call</param>
    void AddRollbackAction(string? desc, Action onRollback);

    /// <summary>
    /// Add an async action to execute if the transaction is rolled back
    /// </summary>
    /// <remarks>
    /// <para>These actions will be called in reverse order if the transaction rolls back</para>
    /// <para>Activities that are not part of the DB transaction should include a call to this function to undo on rollback</para>
    /// </remarks>
    /// <example>
    /// <code>
    /// Guid result = await webServiceClient.AddThingAsync();
    /// conn.AddAsyncRollbackAction("undo add thing", async () => await webServiceClient.RemoveThingAsync(result));
    /// </code>
    /// </example>
    /// <param name="desc">description of rollback event (for logs)</param>
    /// <param name="onRollbackAsync">async action to call</param>
    void AddAsyncRollbackAction(string? desc, Func<Task> onRollbackAsync);
    #endregion

    #region Post Commit Actions
    /// <summary>
    /// Add an action to execute after the transaction is committed
    /// </summary>
    /// <remarks>
    /// <para>These actions will be called in order after the transaction commits</para>
    /// <para>Activities that should only take place if the transaction successfully commits should be added here</para>
    /// </remarks>
    /// <example>
    /// <code>
    /// WriteNewPathToDB(connection, newPath);
    /// string oldPath = GetFilePath();
    /// conn.AddPostCommitAction("remove old file after commit", () => File.Delete(oldPath));
    /// </code>
    /// </example>
    /// <param name="desc">description of post commit event (for logs)</param>
    /// <param name="postCommit">action to call</param>
    void AddPostCommitAction(string? desc, Action postCommit);

    /// <summary>
    /// Add an async action to execute after the transaction is committed
    /// </summary>
    /// <remarks>
    /// <para>These actions will be called in order after the transaction commits</para>
    /// <para>Activities that should only take place if the transaction successfully commits should be added here</para>
    /// </remarks>
    /// <example>
    /// <code>
    /// WriteNewPathToDB(connection, newPath);
    /// string oldPath = GetFilePath();
    /// conn.AddPostCommitAction("remove old file after commit", async () => await File.DeleteAsync(oldPath));
    /// </code>
    /// </example>
    /// <param name="desc">description of post commit event (for logs)</param>
    /// <param name="postCommitAsync">async action to call</param>
    void AddAsyncPostCommitAction(string? desc, Func<Task> postCommitAsync);
    #endregion
}
