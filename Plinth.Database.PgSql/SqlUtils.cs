﻿using Npgsql;
using System.Net.Sockets;

namespace Plinth.Database.PgSql;

/// <summary>
/// Utilities for DB Code
/// </summary>
public static class SqlUtils
{
    /// <summary>
    /// Determine if the given exception was caused by timeout
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static bool IsTimeoutException(NpgsqlException e)
    {
        // this appears to be the only way to detect timeout
        // https://github.com/npgsql/npgsql/issues/2239
        return e.InnerException is SocketException se && se.SocketErrorCode == SocketError.TimedOut;
    }

    /// <summary>
    /// Determine if the given exception was caused by a foreign key constraint violation
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static bool IsForeignKeyConstraintException(NpgsqlException e)
    {
        // http://www.npgsql.org/doc/api/Npgsql.PostgresErrorCodes.html
        return e is PostgresException pe && pe.SqlState == PostgresErrorCodes.ForeignKeyViolation;
    }

    /// <summary>
    /// Convert a nullable timespan to integer milliseconds
    /// </summary>
    /// <param name="timespan"></param>
    /// <returns>int milliseconds or null if timespan is null</returns>
    public static int? TimeSpanToMillis(TimeSpan? timespan)
    {
        return timespan.HasValue ? (int)timespan.Value.TotalMilliseconds : (int?)null;
    }

    /// <summary>
    /// Convert a nullable integer milliseconds to timespan
    /// </summary>
    /// <param name="millis"></param>
    /// <returns>timespan or null if millis is null</returns>
    public static TimeSpan? MillisToTimeSpan(int? millis)
    {
        return millis.HasValue ? TimeSpan.FromMilliseconds(millis.Value) : (TimeSpan?)null;
    }
}
