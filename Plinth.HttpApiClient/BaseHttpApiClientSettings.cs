using Microsoft.Extensions.Logging;
using Plinth.Common.Logging;

namespace Plinth.HttpApiClient;

/// <summary>
/// Default settings for an Http Api Client
/// </summary>
public class BaseHttpApiClientSettings
{
    private const int DefaultMaxRequestLogging = 25000;
    private const int DefaultMaxResponseLogging = 25000;

    /// <summary>
    /// Constructor
    /// </summary>
    public BaseHttpApiClientSettings()
    {
    }

    /// <summary>(optional) Base URI of the service, 'scheme://server[:port]/'</summary>
    public string? BaseAddress { get; set; }

    /// <summary>(optional) custom api call timeout</summary>
    public TimeSpan? Timeout { get; set; }

    /// <summary>max bytes to log from request</summary>
    public int MaxLoggingRequestBody { get; set; } = DefaultMaxRequestLogging;

    /// <summary>max bytes to log from response</summary>
    public int MaxLoggingResponseBody { get; set; } = DefaultMaxResponseLogging;

    /// <summary>custom sensitive data masker</summary>
    public ISensitiveDataMasker? SensitiveDataMasker { get; set; }
}
