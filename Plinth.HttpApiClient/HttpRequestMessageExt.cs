using Newtonsoft.Json;
using Plinth.Common.Constants;
using Plinth.Serialization;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Encodings.Web;

namespace Plinth.HttpApiClient;

/// <summary>
/// <see cref="HttpRequestMessage"/> extension class for simplifying http calls inside Http Api Clients using method chaining
/// </summary>
public class HttpRequestMessageExt : HttpRequestMessage
{
    internal const string BodyProperty = "__webapiclient_body_property";

    private readonly BaseHttpApiClient _apiClient;
    private List<KeyValuePair<string, string?>>? _queryParams;

    internal HttpRequestMessageExt(HttpMethod method, string requestUri, BaseHttpApiClient apiClient) : base(method, requestUri)
    {
        _apiClient = apiClient;
    }

    #region execute
    /// <summary>
    /// Execute the http call and deserialize a response
    /// </summary>
    /// <typeparam name="T">type to deserialize response into</typeparam>
    /// <param name="expectedStatusCodes">(optional) override of status codes that are considered 'successful', defaults to 2xx</param>
    public Task<T?> ExecuteAsync<T>(params HttpStatusCode[] expectedStatusCodes)
        => _apiClient.ExecuteAsync<T>(HandleQueryString(), null, expectedStatusCodes, CancellationToken.None);

    /// <summary>
    /// Execute the http call and deserialize a response with custom deserialization
    /// </summary>
    /// <typeparam name="T">type to deserialize response into</typeparam>
    /// <param name="settingsAction">(optional) action to set custom json serializer settings</param>
    /// <param name="expectedStatusCodes">(optional) override of status codes that are considered 'successful', defaults to 2xx</param>
    public Task<T?> ExecuteAsync<T>(Action<JsonSerializerSettings>? settingsAction, params HttpStatusCode[] expectedStatusCodes)
       => _apiClient.ExecuteAsync<T>(HandleQueryString(), settingsAction, expectedStatusCodes, CancellationToken.None);

    /// <summary>
    /// Execute the http call and deserialize a response with custom deserialization and a cancellation token
    /// </summary>
    /// <typeparam name="T">type to deserialize response into</typeparam>
    /// <param name="cancelToken">cancellation token</param>
    /// <param name="settingsAction">(optional) action to set custom json serializer settings</param>
    /// <param name="expectedStatusCodes">(optional) override of status codes that are considered 'successful', defaults to 2xx</param>
    public Task<T?> ExecuteAsync<T>(CancellationToken cancelToken, Action<JsonSerializerSettings>? settingsAction = null, params HttpStatusCode[] expectedStatusCodes)
        => _apiClient.ExecuteAsync<T>(HandleQueryString(), settingsAction, expectedStatusCodes, cancelToken);

    /// <summary>
    /// Execute the http call and get the response as a string
    /// </summary>
    /// <param name="expectedStatusCodes">(optional) override of status codes that are considered 'successful', defaults to 2xx</param>
    /// <returns>a tuple of raw HttpResponseMessage and the content (if it can be read as a string)</returns>
    public Task<(HttpResponseMessage response, string? content)> ExecuteAsync(params HttpStatusCode[] expectedStatusCodes)
        => _apiClient.ExecuteAsync(HandleQueryString(), true, CancellationToken.None, expectedStatusCodes);

    /// <summary>
    /// Execute the http call and get the response as a string
    /// </summary>
    /// <param name="cancelToken">cancellation token</param>
    /// <param name="expectedStatusCodes">(optional) override of status codes that are considered 'successful', defaults to 2xx</param>
    /// <returns>a tuple of raw HttpResponseMessage and the content (if it can be read as a string)</returns>
    public Task<(HttpResponseMessage response, string? content)> ExecuteAsync(CancellationToken cancelToken, params HttpStatusCode[] expectedStatusCodes)
        => _apiClient.ExecuteAsync(HandleQueryString(), true, cancelToken, expectedStatusCodes);

    /// <summary>
    /// Execute the http call without getting the response content (use response .Content.ReadAs....)
    /// </summary>
    /// <param name="expectedStatusCodes">(optional) override of status codes that are considered 'successful', defaults to 2xx</param>
    /// <returns>raw HttpResponseMessage with content un-read</returns>
    public async Task<HttpResponseMessage> ExecuteRawAsync(params HttpStatusCode[] expectedStatusCodes)
        => (await _apiClient.ExecuteAsync(HandleQueryString(), false, CancellationToken.None, expectedStatusCodes)).response;

    /// <summary>
    /// Execute the http call without getting the response content (use response .Content.ReadAs....)
    /// </summary>
    /// <param name="cancelToken">cancellation token</param>
    /// <param name="expectedStatusCodes">(optional) override of status codes that are considered 'successful', defaults to 2xx</param>
    /// <returns>raw HttpResponseMessage with content un-read</returns>
    public async Task<HttpResponseMessage> ExecuteRawAsync(CancellationToken cancelToken, params HttpStatusCode[] expectedStatusCodes)
        => (await _apiClient.ExecuteAsync(HandleQueryString(), false, cancelToken, expectedStatusCodes)).response;

    /// <summary>
    /// Execute the http call and deserialize a response, returning null on 404/NotFound instead of exception
    /// </summary>
    /// <typeparam name="T">type to deserialize response into</typeparam>
    /// <param name="settingsAction">(optional) action to set custom json serializer settings</param>
    public async Task<T?> ExecuteOrNullAsync<T>(Action<JsonSerializerSettings>? settingsAction = null)
        => await ExecuteOrNullAsync<T>(CancellationToken.None, settingsAction);

    /// <summary>
    /// Execute the http call and deserialize a response, returning null on 404/NotFound instead of exception
    /// </summary>
    /// <typeparam name="T">type to deserialize response into</typeparam>
    /// <param name="cancelToken">cancellation token</param>
    /// <param name="settingsAction">(optional) action to set custom json serializer settings</param>
    public async Task<T?> ExecuteOrNullAsync<T>(CancellationToken cancelToken, Action<JsonSerializerSettings>? settingsAction = null)
    {
        var r = await ExecuteAsync(HttpStatusCode.NotFound);
        if (r.response.StatusCode == HttpStatusCode.NotFound)
            return default;

        r.content = await HttpApiClientLogger.GetContent(r.response, r.content, cancelToken);
        return JsonUtil.DeserializeObject<T>(r.content ?? string.Empty, settingsAction);
    }

    /// <summary>
    /// Execute the http call and get the response as a string, returning null on 404/NotFound instead of exception
    /// </summary>
    public async Task<(HttpResponseMessage response, string? content)> ExecuteOrNullAsync()
    {
        var r = await ExecuteAsync(HttpStatusCode.NotFound);
        if (r.response.StatusCode == HttpStatusCode.NotFound)
            return (r.response, null);
        return r;
    }

    /// <summary>
    /// Execute the http call and get the response as a string, returning null on 404/NotFound instead of exception
    /// </summary>
    /// <param name="cancelToken">cancellation token</param>
    public async Task<(HttpResponseMessage response, string? content)> ExecuteOrNullAsync(CancellationToken cancelToken)
    {
        var r = await ExecuteAsync(cancelToken, HttpStatusCode.NotFound);
        if (r.response.StatusCode == HttpStatusCode.NotFound)
            return (r.response, null);
        return r;
    }
    #endregion

    #region set request params
    /// <summary>
    /// Set the request body to a string
    /// </summary>
    /// <param name="body">body content</param>
    /// <param name="contentType">(optional) content type, default is 'text/plain'</param>
    /// <param name="encoding">(optional) default is <see cref="Encoding.UTF8"/></param>
    public HttpRequestMessageExt SetStringBody(string body, string contentType = ContentTypes.TextPlain, Encoding? encoding = null)
    {
        Content = new StringContent(body, encoding ?? Encoding.UTF8, contentType);
        Options.TryAdd(BodyProperty, body);
        return this;
    }

    /// <summary>
    /// Set the request body to the json representation of <paramref name="body"/>
    /// </summary>
    /// <typeparam name="T">any class</typeparam>
    /// <param name="body">object to be serialized into the request body</param>
    /// <param name="settingsAction">(optional) action to set custom json serializer settings</param>
    public HttpRequestMessageExt SetJsonBody<T>(T body, Action<JsonSerializerSettings>? settingsAction = null)
        => SetStringBody(JsonUtil.SerializeObject(body, settingsAction), ContentTypes.ApplicationJson);

    /// <summary>
    /// Set the authorization header
    /// </summary>
    /// <param name="token">the authorization parameter</param>
    /// <param name="scheme">(optional) the authorization scheme, defaults to 'Bearer'</param>
    /// <returns></returns>
    public HttpRequestMessageExt SetAuthorization(string token, string scheme = "Bearer")
    {
        Headers.Authorization = new AuthenticationHeaderValue(scheme, token);
        return this;
    }

    /// <summary>
    /// Add a query parameter to the request
    /// </summary>
    /// <typeparam name="T">any type</typeparam>
    /// <param name="name">name of the query parameter</param>
    /// <param name="value">value, will be .ToString()'d</param>
    public HttpRequestMessageExt AddQueryParameter<T>(string name, T value)
        => AddQueryParameter(name, value?.ToString());

    /// <summary>
    /// Add a query parameter with multiple values to the request
    /// </summary>
    /// <typeparam name="T">any type</typeparam>
    /// <param name="name">name of the query parameter</param>
    /// <param name="values">enumerable of values, each will be .ToString()'d</param>
    public HttpRequestMessageExt AddQueryParameter<T>(string name, IEnumerable<T> values)
        => AddQueryParameter(name, values.Select(v => v?.ToString()));

    /// <inheritdoc cref="AddQueryParameter{T}(string, IEnumerable{T})"/>
    public HttpRequestMessageExt AddQueryParameter<T>(string name, List<T> values)
        => AddQueryParameter(name, values.Select(v => v?.ToString()));

    /// <summary>
    /// Add a query parameter to the request
    /// </summary>
    /// <param name="name">name of the query parameter</param>
    /// <param name="value">value of the query parameter</param>
    public HttpRequestMessageExt AddQueryParameter(string name, string? value)
    {
        _queryParams ??= [];
        _queryParams.Add(KeyValuePair.Create(name, value));
        return this;
    }

    /// <summary>
    /// Add a query parameter with multiple values to the request
    /// </summary>
    /// <param name="name">name of the query parameter</param>
    /// <param name="values">enumerable of values</param>
    public HttpRequestMessageExt AddQueryParameter(string name, IEnumerable<string?> values)
    {
        _queryParams ??= [];
        _queryParams.AddRange(values.Select(v => KeyValuePair.Create(name, v)));
        return this;
    }

    /// <inheritdoc cref="AddQueryParameter(string, IEnumerable{string})"/>
    public HttpRequestMessageExt AddQueryParameter(string name, List<string?> values)
        => AddQueryParameter(name, (IEnumerable<string?>)values);

    /// <summary>
    /// Adds optional query parameter. Will not add if it is null, empty or has a default value for value types.
    /// </summary>
    /// <param name="name">name of the query parameter</param>
    /// <param name="value">value of the query parameter</param>
    public HttpRequestMessageExt AddQueryOptional<T>(string name, T? value)
    {
        // to avoid boxing and support structs and Nullable<T>
        if (EqualityComparer<T>.Default.Equals(value, default))
            return this;

        return AddQueryParameter(name, value);
    }

    /// <summary>
    /// Add custom header to the request
    /// </summary>
    /// <param name="name">header name/key</param>
    /// <param name="value">header value</param>
    public HttpRequestMessageExt AddHeader(string name, string? value)
    {
        Headers.Add(name, value);
        return this;
    }

    /// <summary>
    /// Add custom headers to the request
    /// </summary>
    /// <param name="headers">header dictionary</param>            
    public HttpRequestMessageExt AddHeaders(IEnumerable<KeyValuePair<string, string?>> headers)
    {
        foreach (var header in headers)
            Headers.Add(header.Key, header.Value);
        return this;
    }

    /// <summary>
    /// Add custom headers to the request
    /// </summary>
    /// <param name="headers">header list of tuples</param>            
    public HttpRequestMessageExt AddHeaders(params (string key, string? value)[] headers)
    {
        foreach (var header in headers)
            Headers.Add(header.key, header.value);
        return this;
    }

    /// <summary>
    /// Set accept header
    /// </summary>
    /// <param name="contentType">(optional) content type, default is 'application/json'</param>
    public HttpRequestMessageExt SetAcceptHeader(string contentType = ContentTypes.ApplicationJson)
    {
        Headers.Accept.Clear();
        Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(contentType));
        return this;
    }

    /// <summary>
    /// Method for performing custom request message initialization using inline builder style
    /// </summary>
    /// <param name="action"></param>
    public HttpRequestMessageExt Configure(Action<HttpRequestMessageExt> action)
    {
        action(this);
        return this;
    }
    #endregion

    private static readonly HttpRequestOptionsKey<string> _bodyKey = new(BodyProperty);
    internal string? GetStoredBody() => Options.TryGetValue(_bodyKey, out var v) ? v : null;

    internal HttpRequestMessageExt HandleQueryString()
    {
        if (_queryParams != null)
        {
            RequestUri = _apiClient.InternalClient.BaseAddress == null
                ? new Uri(AddQueryString(RequestUri!.ToString(), _queryParams))
                : new Uri(_apiClient.InternalClient.BaseAddress, AddQueryString(RequestUri!.ToString(), _queryParams));
            _queryParams = null;
        }
        return this;
    }

    // this comes from an aspnetcore package (copied to avoid reference)
    // https://github.com/dotnet/aspnetcore/blob/59260392fc4f31eba3ed6b97bc1b4375eeb8e0b9/src/Http/WebUtilities/src/QueryHelpers.cs#L76C1-L114C6
    private static string AddQueryString(
        string uri,
        IEnumerable<KeyValuePair<string, string?>> queryString)
    {
        ArgumentNullException.ThrowIfNull(uri);
        ArgumentNullException.ThrowIfNull(queryString);

        var anchorIndex = uri.IndexOf('#');
        var uriToBeAppended = uri.AsSpan();
        var anchorText = ReadOnlySpan<char>.Empty;
        // If there is an anchor, then the query string must be inserted before its first occurrence.
        if (anchorIndex != -1)
        {
            anchorText = uriToBeAppended.Slice(anchorIndex);
            uriToBeAppended = uriToBeAppended.Slice(0, anchorIndex);
        }

        var queryIndex = uriToBeAppended.IndexOf('?');
        var hasQuery = queryIndex != -1;

        var sb = new StringBuilder();
        sb.Append(uriToBeAppended);
        foreach (var parameter in queryString)
        {
            if (parameter.Value == null)
            {
                continue;
            }

            sb.Append(hasQuery ? '&' : '?');
            sb.Append(UrlEncoder.Default.Encode(parameter.Key));
            sb.Append('=');
            sb.Append(UrlEncoder.Default.Encode(parameter.Value));
            hasQuery = true;
        }

        sb.Append(anchorText);
        return sb.ToString();
    }
}
