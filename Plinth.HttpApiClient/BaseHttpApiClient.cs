using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Plinth.Common.Logging;
using Plinth.HttpApiClient.Common;
using Plinth.Serialization;
using System.Diagnostics;
using System.Net;

namespace Plinth.HttpApiClient;

/// <summary>
/// Base class for ApiClients using HttpClient
/// </summary>
public class BaseHttpApiClient
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    /// <summary>
    /// Identifier for the service
    /// </summary>
    protected string ServiceId { get; }

    /// <summary>
    /// Settings
    /// </summary>
    protected BaseHttpApiClientSettings Settings { get; }

    /// <summary>
    /// Internal client
    /// </summary>
    internal protected HttpClient InternalClient { get; }

    /// <summary>
    /// Func called just before executing a request (useful for modifying the requests globally)
    /// </summary>
    protected Func<HttpRequestMessageExt, Task>? BeforeExec { get; set; } = null;

    /// <summary>
    /// Func called just after executing a request (useful for global response handling)
    /// </summary>
    protected Func<HttpRequestMessageExt, HttpResponseMessage, Task>? AfterExec { get; set; } = null;

    /// <summary>
    /// Func called after a request that returned a successful code (2xx)
    /// </summary>
    protected Func<HttpRequestMessageExt, HttpResponseMessage, string?, Task>? AfterSuccess { get; set; } = null;

    /// <summary>
    /// Func called after a request that returned an error code or exception (not 2xx)
    /// </summary>
    protected Func<HttpRequestMessageExt, HttpResponseMessage?, Exception?, Task>? AfterFailure { get; set; } = null;

    /// <summary>
    /// Construct from URL and timeout
    /// </summary>
    /// <param name="serviceId">a string ID which will be logged, to identify which service is being called</param>
    /// <param name="httpClient">HttpClient to be used (from IHttpClientFactory)</param>
    /// <param name="settings">(optional) settings object</param>
    protected BaseHttpApiClient(string serviceId, HttpClient httpClient, IOptions<BaseHttpApiClientSettings>? settings = null)
    {
        ServiceId = serviceId ?? throw new ArgumentNullException(nameof(serviceId));
        Settings = settings?.Value ?? new BaseHttpApiClientSettings();

        InternalClient = httpClient;

        if (settings?.Value.BaseAddress != null)
            InternalClient.BaseAddress = new Uri(settings.Value.BaseAddress);

        if (settings?.Value.Timeout != null)
            InternalClient.Timeout = settings.Value.Timeout.Value;
    }

    /// <summary>
    /// Creates HTTP GET request for JSON API.
    /// </summary>
    protected HttpRequestMessageExt HttpGet(string resource) => MakeRequest(resource, HttpMethod.Get);

    /// <summary>
    /// Creates HTTP POST request for JSON API.
    /// </summary>
    protected HttpRequestMessageExt HttpPost(string resource) => MakeRequest(resource, HttpMethod.Post);

    /// <summary>
    /// Creates HTTP PUT request for JSON API.
    /// </summary>
    protected HttpRequestMessageExt HttpPut(string resource) => MakeRequest(resource, HttpMethod.Put);

    /// <summary>
    /// Creates HTTP DELETE request for JSON API.
    /// </summary>
    protected HttpRequestMessageExt HttpDelete(string resource) => MakeRequest(resource, HttpMethod.Delete);

    /// <summary>
    /// Creates HTTP PATCH request for JSON API.
    /// </summary>
    protected HttpRequestMessageExt HttpPatch(string resource) => MakeRequest(resource, HttpMethod.Patch);

    /// <summary>
    /// Creates HTTP request for JSON API.
    /// </summary>
    protected HttpRequestMessageExt MakeRequest(string resource, HttpMethod method)
        => new(method, resource, this);

    private async Task<string> PreExecuteAsync(string? rChain, HttpRequestMessageExt request, CancellationToken cancellationToken)
    {
        // add in request trace header for next service level down
        request.AddHeader(RequestTraceId.RequestTraceIdHeader, RequestTraceId.Instance.Get());
        request.AddHeader(RequestTraceChain.RequestTraceChainHeader, rChain);

        var rProps = RequestTraceProperties.GetRequestTraceProperties();
        if (rProps?.Count > 0)
            request.AddHeader(RequestTraceProperties.RequestTracePropertiesHeader, JsonUtil.SerializeObject(rProps));

        if (BeforeExec != null)
            await BeforeExec(request);

        var requestUri = request.RequestUri?.ToString() ?? "<unknown uri>";
        var body = await HttpApiClientLogger.GetContent(request, request.GetStoredBody(), cancellationToken);
        log.LogRequest(ServiceId, rChain, request, requestUri, body, Settings.MaxLoggingRequestBody, Settings.SensitiveDataMasker);

        return requestUri;
    }

    private async Task<(HttpResponseMessage response, string? content)> PostExecute(string? rChain, HttpRequestMessageExt request, HttpResponseMessage response, HttpStatusCode[] expectedStatusCodes, string requestUri, Stopwatch sw, bool getContent, CancellationToken cancelToken)
    {
        if (AfterExec != null)
            await AfterExec(request, response);

        var maybeContent = await log.LogResponse(ServiceId, rChain, response, requestUri, sw, getContent, Settings.MaxLoggingResponseBody, Settings.SensitiveDataMasker, cancelToken);

        await CheckForErrors(request, response, maybeContent, expectedStatusCodes, cancelToken);

        if (AfterSuccess != null)
            await AfterSuccess(request, response, maybeContent);

        return (response, maybeContent);
    }

    /// <summary>
    /// Execute a Request Async with an expected object return type (json)
    /// </summary>
    internal async Task<T?> ExecuteAsync<T>(HttpRequestMessageExt request, Action<JsonSerializerSettings>? deserAction, HttpStatusCode[] expectedStatusCodes, CancellationToken cancelToken)
    {
        var r = await ExecuteAsync(request, true, cancelToken, expectedStatusCodes);
        r.content = await HttpApiClientLogger.GetContent(r.response, r.content, cancelToken);
        return JsonUtil.DeserializeObject<T>(r.content ?? string.Empty, deserAction);
    }

    /// <summary>
    /// Execute a Request Async
    /// </summary>
    internal async Task<(HttpResponseMessage response, string? content)> ExecuteAsync(HttpRequestMessageExt request, bool getContent, CancellationToken cancelToken, params HttpStatusCode[] expectedStatusCodes)
    {
        var rChain = RequestTraceChain.Instance.GenerateNextChain();

        var requestUri = await PreExecuteAsync(rChain, request, cancelToken);

        var sw = Stopwatch.StartNew();
        HttpResponseMessage response = null!;
        try
        {
            response = await InternalClient.SendAsync(request, cancelToken);
            sw.Stop();
        }
        catch (Exception e)
        {
            sw.Stop();
            log.LogFailure(ServiceId, rChain, e, requestUri, sw);
            if (AfterFailure != null)
                await AfterFailure(request, null, e);
            throw;
        }

        return await PostExecute(rChain, request, response, expectedStatusCodes, requestUri, sw, getContent, cancelToken);
    }

    /// <summary>
    /// Utility function which determines if content is suitable for logging.  
    /// By default logs JSON/JS/ECMA, text/*, and xml, override to change
    /// </summary>
    /// <param name="contentType">Content-Type header, may be null</param>
    /// <param name="isRequest">true for request, false for response</param>
    /// <returns></returns>
    public virtual bool ShouldLogContent(string? contentType, bool isRequest)
        => HttpApiClientLogger.ShouldLogContent(contentType);

    internal async Task CheckForErrors(HttpRequestMessageExt request, HttpResponseMessage response, string? content, HttpStatusCode[] expectedStatusCodes, CancellationToken cancelToken)
    {
        // Check expected status codes, if specified
        if (response != null)
        {
            if (!expectedStatusCodes.IsNullOrEmpty()
                && expectedStatusCodes.Contains(response.StatusCode))
            {
                return;
            }

            // Check general status codes
            if (response.IsSuccessStatusCode)
            {
                return;
            }
        }
        
        if (AfterFailure != null)
            await AfterFailure(request, response, null);

        content = await HttpApiClientLogger.GetContent(response, content, cancelToken);
        throw new RestException(
            response?.StatusCode ?? 0,
            TryExtractErrorMessage(content),
            content);
    }

    private static string TryExtractErrorMessage(string? content)
    {
        ErrorResponse? errorBody = null;

        try
        {
            if (content != null)
                errorBody = JsonUtil.DeserializeObject<ErrorResponse>(content);
        }
        catch
        {
            // parse errors are ignored
        }

        return errorBody?.ExceptionMessage ?? errorBody?.Message ?? "<unknown error>";
    }

    /// <summary>
    /// Response object returned if error detected
    /// </summary>
    public class ErrorResponse
    {
        /// <summary>
        /// error message
        /// </summary>
        public string? Message { get; set; }

        /// <summary>
        /// exception message
        /// </summary>
        public string? ExceptionMessage { get; set; }
    }
}
