﻿using System.Net;

namespace Plinth.HttpApiClient;

/// <summary>
/// Extensions methods for API responses.
/// </summary>
public static class HttpResponseExtensions
{
    /// <summary>
    /// Determine if this response was successful
    /// </summary>
    /// <param name="response"></param>
    /// <param name="successCodes">list of response codes which define success</param>
    /// <returns></returns>
    public static bool IsSuccess(this HttpResponseMessage response, params HttpStatusCode[] successCodes) => successCodes.Contains(response.StatusCode);
}
