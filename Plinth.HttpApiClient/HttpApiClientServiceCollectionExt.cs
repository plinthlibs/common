using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Plinth.HttpApiClient;
using Plinth.HttpApiClient.Handler;

namespace Microsoft.Extensions.DependencyInjection; // putting this here makes it easier to find

/// <summary>
/// Extension methods for registering HttpApiClients
/// </summary>
public static class HttpApiClientServiceCollectionExt
{
    #region Add with Config Section
    /// <summary>
    /// Register an HTTP API Client for DI using an IConfiguration section for settings
    /// </summary>
    /// <typeparam name="Tinterface">An interface containing all the client methods, for injecting into calling code</typeparam>
    /// <typeparam name="Tclass">A concrete implementation of <typeparamref name="Tinterface"/> which derives from <see cref="BaseHttpApiClient"/></typeparam>
    /// <typeparam name="Tsettings">A <see cref="BaseHttpApiClientSettings"/> or derived</typeparam>
    /// <param name="services"></param>
    /// <param name="section">IConfiguration section to get settings from</param>
    /// <param name="configureClient">(optional) Action to configure the httpclient settings</param>
    /// <returns>IHttpClientBuilder for more customization</returns>
    public static IHttpClientBuilder AddHttpApiClient<Tinterface, Tclass, Tsettings>(this IServiceCollection services, IConfigurationSection section, Action<HttpClient>? configureClient = null)
        where Tinterface : class
        where Tclass : BaseHttpApiClient, Tinterface
        where Tsettings : BaseHttpApiClientSettings
    {
        services.Configure<Tsettings>(section);
        services.PostConfigure<Tsettings>(ValidateHttpApiSettings);
        return configureClient == null ? services.AddHttpClient<Tinterface, Tclass>() : services.AddHttpClient<Tinterface, Tclass>(configureClient);
    }

    /// <inheritdoc cref="AddHttpApiClient{Tinterface, Tclass, Tsettings}(IServiceCollection, Action{Tsettings}, Action{HttpClient})"/>
    public static IHttpClientBuilder AddHttpApiClient<Tinterface, Tclass, Tsettings>(this IServiceCollection services, IConfigurationSection section, Action<IServiceProvider, HttpClient> configureClient)
        where Tinterface : class
        where Tclass : BaseHttpApiClient, Tinterface
        where Tsettings : BaseHttpApiClientSettings
    {
        services.Configure<Tsettings>(section);
        services.PostConfigure<Tsettings>(ValidateHttpApiSettings);
        return services.AddHttpClient<Tinterface, Tclass>(configureClient);
    }

    /// <summary>
    /// Register an HTTP API Client for DI using an IConfiguration section for settings
    /// </summary>
    /// <typeparam name="Tclass">A class which derives from <see cref="BaseHttpApiClient"/></typeparam>
    /// <typeparam name="Tsettings">A <see cref="BaseHttpApiClientSettings"/> or derived</typeparam>
    /// <param name="services"></param>
    /// <param name="section">IConfiguration section to get settings from</param>
    /// <param name="configureClient">(optional) Action to configure the httpclient settings</param>
    /// <returns>IHttpClientBuilder for more customization</returns>
    public static IHttpClientBuilder AddHttpApiClient<Tclass, Tsettings>(this IServiceCollection services, IConfigurationSection section, Action<HttpClient>? configureClient = null)
        where Tclass : BaseHttpApiClient
        where Tsettings : BaseHttpApiClientSettings
    {
        services.Configure<Tsettings>(section);
        services.PostConfigure<Tsettings>(ValidateHttpApiSettings);
        return configureClient == null ? services.AddHttpClient<Tclass>() : services.AddHttpClient<Tclass>(configureClient);
    }

    /// <inheritdoc cref="AddHttpApiClient{Tclass, Tsettings}(IServiceCollection, Action{Tsettings}, Action{HttpClient})"/>
    public static IHttpClientBuilder AddHttpApiClient<Tclass, Tsettings>(this IServiceCollection services, IConfigurationSection section, Action<IServiceProvider, HttpClient> configureClient)
        where Tclass : BaseHttpApiClient
        where Tsettings : BaseHttpApiClientSettings
    {
        services.Configure<Tsettings>(section);
        services.PostConfigure<Tsettings>(ValidateHttpApiSettings);
        return services.AddHttpClient<Tclass>(configureClient);
    }
    #endregion

    #region Add with IConfiguration
    /// <summary>
    /// Register an HTTP API Client for DI using an IConfiguration section for settings
    /// </summary>
    /// <typeparam name="Tinterface">An interface containing all the client methods, for injecting into calling code</typeparam>
    /// <typeparam name="Tclass">A concrete implementation of <typeparamref name="Tinterface"/> which derives from <see cref="BaseHttpApiClient"/></typeparam>
    /// <typeparam name="Tsettings">A <see cref="BaseHttpApiClientSettings"/> or derived</typeparam>
    /// <param name="services"></param>
    /// <param name="configuration">An IConfiguration instance, will load settings from a section named the same as <typeparamref name="Tclass"/></param>
    /// <param name="configureClient">(optional) Action to configure the httpclient settings</param>
    /// <returns>IHttpClientBuilder for more customization</returns>
    public static IHttpClientBuilder AddHttpApiClient<Tinterface, Tclass, Tsettings>(this IServiceCollection services, IConfiguration configuration, Action<HttpClient>? configureClient = null)
        where Tinterface : class
        where Tclass : BaseHttpApiClient, Tinterface
        where Tsettings : BaseHttpApiClientSettings
    {
        services.Configure<Tsettings>(configuration.GetSection(typeof(Tclass).Name));
        services.PostConfigure<Tsettings>(ValidateHttpApiSettings);
        return configureClient == null ? services.AddHttpClient<Tinterface, Tclass>() : services.AddHttpClient<Tinterface, Tclass>(configureClient);
    }

    /// <inheritdoc cref="AddHttpApiClient{Tinterface, Tclass, Tsettings}(IServiceCollection, IConfiguration, Action{HttpClient})"/>
    public static IHttpClientBuilder AddHttpApiClient<Tinterface, Tclass, Tsettings>(this IServiceCollection services, IConfiguration configuration, Action<IServiceProvider, HttpClient> configureClient)
        where Tinterface : class
        where Tclass : BaseHttpApiClient, Tinterface
        where Tsettings : BaseHttpApiClientSettings
    {
        services.Configure<Tsettings>(configuration.GetSection(typeof(Tclass).Name));
        services.PostConfigure<Tsettings>(ValidateHttpApiSettings);
        return services.AddHttpClient<Tinterface, Tclass>(configureClient);
    }

    /// <summary>
    /// Register an HTTP API Client for DI using an IConfiguration section for settings
    /// </summary>
    /// <typeparam name="Tclass">A class which derives from <see cref="BaseHttpApiClient"/></typeparam>
    /// <typeparam name="Tsettings">A <see cref="BaseHttpApiClientSettings"/> or derived</typeparam>
    /// <param name="services"></param>
    /// <param name="configuration">An IConfiguration instance, will load settings from a section named the same as <typeparamref name="Tclass"/></param>
    /// <param name="configureClient">(optional) Action to configure the httpclient settings</param>
    /// <returns>IHttpClientBuilder for more customization</returns>
    public static IHttpClientBuilder AddHttpApiClient<Tclass, Tsettings>(this IServiceCollection services, IConfiguration configuration, Action<HttpClient>? configureClient = null)
        where Tclass : BaseHttpApiClient
        where Tsettings : BaseHttpApiClientSettings
    {
        services.Configure<Tsettings>(configuration.GetSection(typeof(Tclass).Name));
        services.PostConfigure<Tsettings>(ValidateHttpApiSettings);
        return configureClient == null ? services.AddHttpClient<Tclass>() : services.AddHttpClient<Tclass>(configureClient);
    }

    /// <inheritdoc cref="AddHttpApiClient{Tclass, Tsettings}(IServiceCollection, IConfiguration, Action{HttpClient})"/>
    public static IHttpClientBuilder AddHttpApiClient<Tclass, Tsettings>(this IServiceCollection services, IConfiguration configuration, Action<IServiceProvider, HttpClient> configureClient)
        where Tclass : BaseHttpApiClient
        where Tsettings : BaseHttpApiClientSettings
    {
        services.Configure<Tsettings>(configuration.GetSection(typeof(Tclass).Name));
        services.PostConfigure<Tsettings>(ValidateHttpApiSettings);
        return services.AddHttpClient<Tclass>(configureClient);
    }
    #endregion

    #region Add with settings action
    /// <summary>
    /// Register an HTTP API Client for DI using an action to configure settings
    /// </summary>
    /// <typeparam name="Tinterface">An interface containing all the client methods, for injecting into calling code</typeparam>
    /// <typeparam name="Tclass">A concrete implementation of <typeparamref name="Tinterface"/> which derives from <see cref="BaseHttpApiClient"/></typeparam>
    /// <typeparam name="Tsettings">A <see cref="BaseHttpApiClientSettings"/> or derived</typeparam>
    /// <param name="services"></param>
    /// <param name="configure">an Action that receives an unconfigured <typeparamref name="Tsettings"/> instance</param>
    /// <param name="configureClient">(optional) Action to configure the httpclient settings</param>
    /// <returns>IHttpClientBuilder for more customization</returns>
    public static IHttpClientBuilder AddHttpApiClient<Tinterface, Tclass, Tsettings>(this IServiceCollection services, Action<Tsettings> configure, Action<HttpClient>? configureClient = null)
        where Tinterface : class
        where Tclass : BaseHttpApiClient, Tinterface
        where Tsettings : BaseHttpApiClientSettings
    {
        services.Configure(configure);
        services.PostConfigure<Tsettings>(ValidateHttpApiSettings);
        return configureClient == null ? services.AddHttpClient<Tinterface, Tclass>() : services.AddHttpClient<Tinterface, Tclass>(configureClient);
    }

    /// <inheritdoc cref="AddHttpApiClient{Tinterface, Tclass, Tsettings}(IServiceCollection, Action{Tsettings}, Action{HttpClient})"/>
    public static IHttpClientBuilder AddHttpApiClient<Tinterface, Tclass, Tsettings>(this IServiceCollection services, Action<Tsettings> configure, Action<IServiceProvider, HttpClient> configureClient)
        where Tinterface : class
        where Tclass : BaseHttpApiClient, Tinterface
        where Tsettings : BaseHttpApiClientSettings
    {
        services.Configure(configure);
        services.PostConfigure<Tsettings>(ValidateHttpApiSettings);
        return services.AddHttpClient<Tinterface, Tclass>(configureClient);
    }

    /// <summary>
    /// Register an HTTP API Client for DI using an action to configure settings
    /// </summary>
    /// <typeparam name="Tclass">A class which derives from <see cref="BaseHttpApiClient"/></typeparam>
    /// <typeparam name="Tsettings">A <see cref="BaseHttpApiClientSettings"/> or derived</typeparam>
    /// <param name="services"></param>
    /// <param name="configure">an Action that receives an unconfigured <typeparamref name="Tsettings"/> instance</param>
    /// <param name="configureClient">(optional) Action to configure the httpclient settings</param>
    /// <returns>IHttpClientBuilder for more customization</returns>
    public static IHttpClientBuilder AddHttpApiClient<Tclass, Tsettings>(this IServiceCollection services, Action<Tsettings> configure, Action<HttpClient>? configureClient = null)
        where Tclass : BaseHttpApiClient
        where Tsettings : BaseHttpApiClientSettings
    {
        services.Configure(configure);
        services.PostConfigure<Tsettings>(ValidateHttpApiSettings);
        return configureClient == null ? services.AddHttpClient<Tclass>() : services.AddHttpClient<Tclass>(configureClient);
    }

    /// <inheritdoc cref="AddHttpApiClient{Tclass, Tsettings}(IServiceCollection, Action{Tsettings}, Action{HttpClient})"/>
    public static IHttpClientBuilder AddHttpApiClient<Tclass, Tsettings>(this IServiceCollection services, Action<Tsettings> configure, Action<IServiceProvider, HttpClient> configureClient)
        where Tclass : BaseHttpApiClient
        where Tsettings : BaseHttpApiClientSettings
    {
        services.Configure(configure);
        services.PostConfigure<Tsettings>(ValidateHttpApiSettings);
        return services.AddHttpClient<Tclass>(configureClient);
    }
    #endregion

    #region Add without settings
    /// <summary>
    /// Register an HTTP API Client for DI using an action to configure settings
    /// </summary>
    /// <typeparam name="Tinterface">An interface containing all the client methods, for injecting into calling code</typeparam>
    /// <typeparam name="Tclass">A concrete implementation of <typeparamref name="Tinterface"/> which derives from <see cref="BaseHttpApiClient"/></typeparam>
    /// <param name="services"></param>
    /// <param name="configureClient">(optional) Action to configure the httpclient settings</param>
    /// <returns>IHttpClientBuilder for more customization</returns>
    public static IHttpClientBuilder AddHttpApiClient<Tinterface, Tclass>(this IServiceCollection services, Action<HttpClient>? configureClient = null)
        where Tinterface : class
        where Tclass : BaseHttpApiClient, Tinterface
    {
        return configureClient == null ? services.AddHttpClient<Tinterface, Tclass>() : services.AddHttpClient<Tinterface, Tclass>(configureClient);
    }

    /// <inheritdoc cref="AddHttpApiClient{Tinterface, Tclass}(IServiceCollection, Action{HttpClient})"/>
    public static IHttpClientBuilder AddHttpApiClient<Tinterface, Tclass>(this IServiceCollection services, Action<IServiceProvider, HttpClient> configureClient)
        where Tinterface : class
        where Tclass : BaseHttpApiClient, Tinterface
    {
        return services.AddHttpClient<Tinterface, Tclass>(configureClient);
    }

    /// <summary>
    /// Register an HTTP API Client for DI using an action to configure settings
    /// </summary>
    /// <typeparam name="Tclass">A class which derives from <see cref="BaseHttpApiClient"/></typeparam>
    /// <param name="services"></param>
    /// <param name="configureClient">(optional) Action to configure the httpclient settings</param>
    /// <returns>IHttpClientBuilder for more customization</returns>
    public static IHttpClientBuilder AddHttpApiClient<Tclass>(this IServiceCollection services, Action<HttpClient>? configureClient = null)
        where Tclass : BaseHttpApiClient
    {
        return configureClient == null ? services.AddHttpClient<Tclass>() : services.AddHttpClient<Tclass>(configureClient);
    }

    /// <inheritdoc cref="AddHttpApiClient{Tclass}(IServiceCollection, Action{HttpClient})"/>
    public static IHttpClientBuilder AddHttpApiClient<Tclass>(this IServiceCollection services, Action<IServiceProvider, HttpClient> configureClient)
        where Tclass : BaseHttpApiClient
    {
        return services.AddHttpClient<Tclass>(configureClient);
    }
    #endregion

    #region handler mode
    /// <summary>
    /// Register an HTTP API Client for DI using a handler for raw usage with another client framework
    /// </summary>
    /// <remarks>
    /// The intent of this model is if using another client framework such as generated clients from an openapispec.
    /// This way you can register a class that does not depend on BaseHttpApiClient (i.e. any class) that will take an HttpClient and use it.
    /// This will add logging and request tracing to that client without the request builder framework.
    /// </remarks>
    /// <typeparam name="Tinterface">An interface containing all the client methods, for injecting into calling code</typeparam>
    /// <typeparam name="Tclass">A concrete implementation of <typeparamref name="Tinterface"/> which derives from <see cref="BaseHttpApiClient"/></typeparam>
    /// <param name="services"></param>
    /// <param name="serviceId">a string ID which will be logged, to identify which service is being called</param>
    /// <param name="configureClient">(optional) Action to configure the httpclient settings</param>
    /// <param name="configureSettings">(optional) Action to configure the handler settings</param>
    /// <returns>IHttpClientBuilder for more customization</returns>
    public static IHttpClientBuilder AddHttpApiHandlerClient<Tinterface, Tclass>(this IServiceCollection services, string serviceId, Action<HttpClient>? configureClient = null, Action<HttpApiClientHandlerSettings>? configureSettings = null)
        where Tinterface : class
        where Tclass : class, Tinterface
    {
        var builder = configureClient == null
            ? services.AddHttpClient<Tinterface, Tclass>()
            : services.AddHttpClient<Tinterface, Tclass>(configureClient);
        var settings = new HttpApiClientHandlerSettings();
        configureSettings?.Invoke(settings);
        return builder
                .AddHttpMessageHandler(sp =>
                {
                    return new HttpApiClientHandler(
                        sp.GetRequiredService<ILogger<HttpApiClientHandler>>(),
                        serviceId,
                        settings);
                });
    }
    #endregion

    private static void ValidateHttpApiSettings(BaseHttpApiClientSettings s)
    {
        if (!string.IsNullOrEmpty(s.BaseAddress))
        {
            if (!Uri.IsWellFormedUriString(s.BaseAddress, UriKind.Absolute))
                throw new ArgumentException($"{nameof(s.BaseAddress)} is not a valid URI");
            var pathAndQuery = new Uri(s.BaseAddress).PathAndQuery;
            if (pathAndQuery != string.Empty && pathAndQuery != "/")
                throw new ArgumentException($"{nameof(s.BaseAddress)} must be only scheme/host/port, no path");
        }
    }
}
