using Microsoft.Extensions.Logging;
using Plinth.Common.Logging;
using Plinth.Serialization;
using System.Diagnostics;

namespace Plinth.HttpApiClient.Handler;

internal class HttpApiClientHandler : DelegatingHandler
{
    private readonly ILogger _logger;
    private readonly HttpApiClientHandlerSettings _settings;

    private readonly string _serviceId;

    // note that anything injected here is not scoped to the request
    // if you need something scoped, you must resolve it via an IHttpContextAccessor inside SendAsync
    // https://andrewlock.net/understanding-scopes-with-ihttpclientfactory-message-handlers/

    public HttpApiClientHandler(ILogger logger, string serviceId, HttpApiClientHandlerSettings settings)
    {
        if (string.IsNullOrWhiteSpace(serviceId))
            throw new ArgumentException($"'{nameof(serviceId)}' cannot be null or whitespace", nameof(serviceId));

        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        _serviceId = serviceId;
        _settings = settings ?? throw new ArgumentNullException(nameof(settings));
    }

    private async Task<string> PreExecuteAsync(string? rChain, HttpRequestMessage request, CancellationToken cancellationToken)
    {
        // add in request trace header for next service level down
        request.Headers.Add(RequestTraceId.RequestTraceIdHeader, RequestTraceId.Instance.Get());
        request.Headers.Add(RequestTraceChain.RequestTraceChainHeader, rChain);

        var rProps = RequestTraceProperties.GetRequestTraceProperties();
        if (rProps?.Count > 0)
            request.Headers.Add(RequestTraceProperties.RequestTracePropertiesHeader, JsonUtil.SerializeObject(rProps));

        if (_settings.BeforeExec != null)
            await _settings.BeforeExec(request);

        var requestUri = request.RequestUri?.ToString() ?? "<unknown uri>";
        var body = await HttpApiClientLogger.GetContent(request, null, cancellationToken);
        _logger.LogRequest(_serviceId, rChain, request, requestUri, body, _settings.MaxLoggingRequestBody, _settings.SensitiveDataMasker);

        return requestUri;
    }

    private async Task PostExecuteAsync(string? rChain, HttpRequestMessage request, HttpResponseMessage response, string requestUri, Stopwatch sw, CancellationToken cancellationToken)
    {
        if (_settings.AfterExec != null)
            await _settings.AfterExec.Invoke(request, response);

        var maybeContent = await _logger.LogResponse(_serviceId, rChain, response, requestUri, sw, true, _settings.MaxLoggingResponseBody, _settings.SensitiveDataMasker, cancellationToken);
        var ok = await CheckForErrors(request, response, null);

        if (ok && _settings.AfterSuccess != null)
            await _settings.AfterSuccess.Invoke(request, response, maybeContent);
    }

    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
    {
        var rChain = RequestTraceChain.Instance.GenerateNextChain();

        var requestUri = await PreExecuteAsync(rChain, request, cancellationToken);

        HttpResponseMessage response;
        var sw = Stopwatch.StartNew();
        try
        {
            response = await base.SendAsync(request, cancellationToken);
            sw.Stop();
        }
        catch (Exception e)
        {
            sw.Stop();
            _logger.LogFailure(_serviceId, rChain, e, requestUri, sw);
            if (_settings.AfterFailure != null)
                await _settings.AfterFailure.Invoke(request, null, e);
            throw;
        }

        await PostExecuteAsync(rChain, request, response, requestUri, sw, cancellationToken);

        return response;
    }

    private async Task<bool> CheckForErrors(HttpRequestMessage request, HttpResponseMessage response, Exception? e)
    {
        if (_settings.IsSuccess != null)
            return await _settings.IsSuccess(request, response);

        if (response.IsSuccessStatusCode)
            return true;

        if (_settings.AfterFailure != null)
            await _settings.AfterFailure.Invoke(request, response, e);

        return false;
    }
}
