using Plinth.Common.Logging;

namespace Plinth.HttpApiClient.Handler;

/// <summary>
/// Settings for handler based api clients
/// </summary>
public sealed class HttpApiClientHandlerSettings
{
    private const int DefaultMaxRequestLogging = 25000;
    private const int DefaultMaxResponseLogging = 25000;

    /// <summary>
    /// Func called just before executing a request (useful for modifying the requests globally)
    /// </summary>
    public Func<HttpRequestMessage, Task>? BeforeExec { get; set; } = null;

    /// <summary>
    /// Func called just after executing a request (useful for global response handling)
    /// </summary>
    public Func<HttpRequestMessage, HttpResponseMessage, Task>? AfterExec { get; set; } = null;

    /// <summary>
    /// Func called to determine if a response is success.  defaults to response.IsSuccessStatusCode
    /// </summary>
    public Func<HttpRequestMessage, HttpResponseMessage, Task<bool>>? IsSuccess { get; set; } = null;

    /// <summary>
    /// Func called after a request that returned a successful code (or IsSuccess returns true)
    /// </summary>
    public Func<HttpRequestMessage, HttpResponseMessage, string?, Task>? AfterSuccess { get; set; } = null;

    /// <summary>
    /// Func called after a request that returned an error code or exception (or IsSuccess returns false)
    /// </summary>
    public Func<HttpRequestMessage, HttpResponseMessage?, Exception?, Task>? AfterFailure { get; set; } = null;

    /// <summary>max bytes to log from request</summary>
    public int MaxLoggingRequestBody { get; set; } = DefaultMaxRequestLogging;

    /// <summary>max bytes to log from response</summary>
    public int MaxLoggingResponseBody { get; set; } = DefaultMaxResponseLogging;

    /// <summary>custom sensitive data masker</summary>
    public ISensitiveDataMasker? SensitiveDataMasker { get; set; }
}
