using Microsoft.Extensions.Logging;
using Plinth.Common.Constants;
using Plinth.Common.Extensions;
using Plinth.Common.Logging;
using System.Diagnostics;

using static Plinth.Common.Logging.Scopes;

namespace Plinth.HttpApiClient;

internal static partial class HttpApiClientLogger
{
    public static void LogRequest(this ILogger log, 
        string serviceId, string? rChain, HttpRequestMessage request, string requestUri, string? requestBody, int maxLength, ISensitiveDataMasker? sensitiveDataMasker)
    {
        var rId = RequestTraceId.Instance.Get();

        var bodyContentType = request.Content?.Headers.ContentType?.MediaType;

        var logMetadata = CreateScope(
            Kvp("CallingChain", rChain),
            Kvp("ContentType", bodyContentType),
            Kvp("ContentLength", requestBody?.Length),
            Kvp(LoggingConstants.Field_MessageType, "RequestSent")
        );

        using (log.BeginScope(logMetadata))
        {
            if (requestBody != null && ShouldLogContent(bodyContentType))
                LogDefines.LogRequestContent(log, $"{rId},{rChain}", serviceId, request.Method.ToString(), requestUri, MaskSensitiveFields(requestBody, sensitiveDataMasker).Truncate(maxLength, "<truncated>"));
            else
                LogDefines.LogRequestNoContent(log, $"{rId},{rChain}", serviceId, request.Method.ToString(), requestUri, requestBody?.Length ?? 0);
        }
    }

    private static string? MaskSensitiveFields(string? s, ISensitiveDataMasker? sensitiveDataMasker)
    {
        var result = s.MaskSensitiveFields();

        if (sensitiveDataMasker != null)
            result = sensitiveDataMasker.MaskSensitiveFields(result);

        return result;
    }

    public static void LogFailure(this ILogger log, 
        string serviceId, string? rChain, Exception error, string reqUrl, Stopwatch sw)
    {
        var rId = RequestTraceId.Instance.Get();

        var logMetadata = CreateScope(
            Kvp("CallingChain", rChain),
            Kvp("HttpStatusCode", 0),
            Kvp("HttpStatus", "ERROR"),
            Kvp("ErrorType", error.GetType().FullName),
            Kvp("ErrorMessage", error.Message),
            Kvp(LoggingConstants.Field_TimeDurationMicros, (long)(sw.Elapsed.TotalMilliseconds * 1000)),
            Kvp("RequestUri", reqUrl),
            Kvp(LoggingConstants.Field_MessageType, "RequestError")
        );

        using (log.BeginScope(logMetadata))
        {
            LogDefines.LogFailureException(log, $"{rId},{rChain}", serviceId, sw.Elapsed, error);
        }
    }

    public static async Task<string?> LogResponse(this ILogger log, 
        string serviceId, string? rChain, HttpResponseMessage response, string reqUrl, Stopwatch sw, bool getContent, int maxLength, ISensitiveDataMasker? sensitiveDataMasker, CancellationToken cancelToken)
    {
        var contentType = response.Content?.Headers?.ContentType?.MediaType;
        var shouldLog = getContent && ShouldLogContent(contentType);

        var rId = RequestTraceId.Instance.Get();
        var statusCode = response.StatusCode;
        var statusCodeStr = statusCode.ToString();

        var content = await GetContent(response, null, cancelToken);
        var contentLen = response.Content?.Headers?.ContentLength ?? content?.Length ?? 0;

        var logMetadata = CreateScope(
            Kvp("CallingChain", rChain),
            Kvp(LoggingConstants.Field_TimeDurationMicros, (long)(sw.Elapsed.TotalMilliseconds * 1000)),
            Kvp("RequestUri", reqUrl),
            Kvp(LoggingConstants.Field_MessageType, "ResponseReceived")
        );

        using (log.BeginScope(logMetadata))
        {
            if (shouldLog)
                LogDefines.LogResponseContent(log, $"{rId},{rChain}", serviceId, sw.Elapsed, (int)statusCode, statusCodeStr, contentType, contentLen, MaskSensitiveFields(content, sensitiveDataMasker).Truncate(maxLength, "<truncated>"));
            else
                LogDefines.LogResponseNoContent(log, $"{rId},{rChain}", serviceId, sw.Elapsed, (int)statusCode, statusCodeStr, contentType, contentLen);
        }

        return content;
    }

    public static Task<string?> GetContent(HttpRequestMessage? m, string? maybeContent, CancellationToken cancelToken)
    {
        if (maybeContent == null)
        {
            if (m == null || m.Content is not StringContent)
                return Task.FromResult<string?>(null);

            return
                m.Content.ReadAsStringAsync(cancelToken) as Task<string?>;
        }
        else
            return Task.FromResult<string?>(maybeContent);
    }

    public static Task<string?> GetContent(HttpResponseMessage? m, string? maybeContent, CancellationToken cancelToken)
    {
        if (maybeContent == null)
        {
            if (m?.Content is null)
                return Task.FromResult<string?>(null);

            return
                m.Content.ReadAsStringAsync(cancelToken) as Task<string?>;
        }
        else
            return Task.FromResult<string?>(maybeContent);
    }

    private static readonly List<string> _loggableContentTypes =
    [
        ContentTypes.ApplicationJavascript,
        ContentTypes.ApplicationEcmascript,
        ContentTypes.ApplicationJson
    ];

    /// <summary>
    /// Utility function which determines if content is suitable for logging.  
    /// By default logs JSON/JS/ECMA, text/*, and xml, override to change
    /// </summary>
    /// <param name="contentType">Content-Type header, may be null</param>
    /// <returns></returns>
    public static bool ShouldLogContent(string? contentType)
    {
        if (contentType == null)
            return false;

        // log any text/ or anything that is xml
        if (contentType.StartsWith("text/") || contentType.Contains("xml"))
            return true;

        // check if content type starts with any of the loggable types
        // starts with is used because some content types come back like "application/json; charset=utf-8"
        foreach (var loggable in _loggableContentTypes)
        {
            if (contentType.StartsWith(loggable))
                return true;
        }

        return false;
    }

    private static partial class LogDefines
    {
        [LoggerMessage(0, LogLevel.Debug,
            "[{TraceString}] ==> Server Request [{ServiceId}]: Method: {HttpMethod}, Uri: {RequestUri}, Body: {RequestBody}",
            EventName = "RequestWithContent")]
        public static partial void LogRequestContent(ILogger logger, string traceString, string serviceId, string? httpMethod, string? requestUri, string? requestBody);

        [LoggerMessage(1, LogLevel.Debug,
            "[{TraceString}] ==> Server Request [{ServiceId}]: Method: {HttpMethod}, Uri: {RequestUri}, Body Size = {ContentLength}",
            EventName = "RequestNoContent")]
        public static partial void LogRequestNoContent(ILogger logger, string traceString, string serviceId, string? httpMethod, string? requestUri, long contentLength);

        [LoggerMessage(2, LogLevel.Debug,
            "[{TraceString}] <== Server Response [{ServiceId}]: (Took: {" + LoggingConstants.Field_TimeDuration + "}) Status Code: {HttpStatusCode}/{HttpStatus}, Type: {ContentType}, Content-Length: {ContentLength}, Content {Content}",
            EventName = "ResponseWithContent")]
        public static partial void LogResponseContent(ILogger logger, string traceString, string serviceId, TimeSpan timeDuration, int httpStatusCode, string? httpStatus, string? contentType, long contentLength, string? content);

        [LoggerMessage(3, LogLevel.Debug,
            "[{TraceString}] <== Server Response [{ServiceId}]: (Took: {" + LoggingConstants.Field_TimeDuration + "}) Status Code: {HttpStatusCode}/{HttpStatus}, Type: {ContentType}, Content-Length: {ContentLength}",
            EventName = "ResponseNoContent")]
        public static partial void LogResponseNoContent(ILogger logger, string traceString, string serviceId, TimeSpan timeDuration, int httpStatusCode, string? httpStatus, string? contentType, long contentLength);

        [LoggerMessage(4, LogLevel.Error,
            "[{TraceString}] <== Server Failure [{ServiceId}]: (Took: {" + LoggingConstants.Field_TimeDuration + "})",
            EventName = "LogFailure")]
        public static partial void LogFailureException(ILogger logger, string traceString, string serviceId, TimeSpan timeDuration, Exception? e = null);
    }
}
