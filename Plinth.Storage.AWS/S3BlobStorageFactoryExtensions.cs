using Plinth.Storage.AWS;

namespace Plinth.Storage;

/// <summary>
/// Extensions for S3
/// </summary>
public static class S3BlobStorageFactoryExtensions
{
    /// <summary>
    /// Add AWS S3 provider support
    /// </summary>
    /// <param name="fac"></param>
    /// <param name="settings">configuration</param>
    public static void AddS3Provider(this StorageFactory fac, Providers.S3.S3Settings settings)
        => fac.Providers[S3ProviderId.Key] = () => new Providers.S3.S3Provider(settings);

    /// <summary>
    /// Set S3 as the default write provider
    /// </summary>
    public static void SetDefaultWriteProviderAsS3(this StorageFactory fac)
        => fac.DefaultWriter = S3ProviderId.Key;

    /// <summary>
    /// Add S3 as a backup write provider
    /// </summary>
    public static void AddBackupS3Provider(this StorageFactory fac, Providers.S3.S3Settings settings)
        => fac.BackupProviders.Add(() => new Providers.S3.S3Provider(settings));
}
