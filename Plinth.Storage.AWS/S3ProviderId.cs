using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plinth.Storage.AWS;

/// <summary>
/// Provider Id Key for the S3 provider
/// </summary>
public static class S3ProviderId
{
    /// <summary>
    /// Provider Id Key for the S3 provider
    /// </summary>
    public static string Key => "S3";
}
