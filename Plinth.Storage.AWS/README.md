# README

### Plinth.Storage.AWS

**Storage Provider for Plinth.Storage to write blob data to AWS S3**

Enables storing the blob data used by _Plinth.Storage_ in AWS S3

This package adds these extension methods to `Plinth.Storage.StorageFactory` to utilize S3
```c#
    storageFactory.AddS3Provider(new S3Settings()
      {
          BucketName = "my-s3-bucket",
          S3Url = "https://s3.us-west-1.amazonaws.com",
          ClientId = "{aws-client-id}",
          ClientSecret = "{aws-client-secret}"
      });

    storageFactory.SetDefaultWriteProviderAsS3();
```

### Index Strategies

By default, this will write blobs to the root of the container.  There are 2 other built in options for further segmenting the blobs.  To use these, set `DefaultIndexStrategy` in `S3Settings`

**_DefaultIndexStrategy.ByDate_**
This will place the blobs in a sub folder with the current date as `/YY/MM/DD/{blobId}`

**_DefaultIndexStrategy.ByDateTime_**
This will place the blobs in a sub folder with the current date and current hour as `/YY/MM/DD/HH/{blobId}`

Alternatively, a custom indexing strategy can be provided.  For example, the below will place the blobs under a folder named the first character of the blob's guid.
```
      new S3Settings()
      {
          ...
          CustomIndexStrategy = blob => $"{blob.Guid!.ToString()![0]}"
      }
```      
 
### Backup Bucket
By default, blobs will be written to the bucket specified.  This library supports writing a backup to other buckets

To enable, use `storageFactory.AddBackupS3Provider(settings)` with a complete `S3Settings` object.  Blobs will be written to the bucket specified (using the primary index).

### IAM Authentication
If you prefer not to supply the ClientId/ClientSecret but instead use the IAM authentication available on AWS infrastructure, set `UseSystemAuth` to `true` in `S3Settings` instead.

### File Extensions
By default, the blobs will a file extension in blob storage which come from the blob name field.  This allows for browsers to more easily download those files directly.
To disable this, set `DisableBlobExtensions` to `true` in `S3Settings` and the blobs will not have a file extension.

### Public Access
By default, S3 blobs are private and require authentication to access.  If anonymous access to the files is desired, set `AllowPublicRead` to `true` in `S3Settings` to enable Public Read access to the blobs.  You must also enable public access at the bucket level for this to work.
