#if !NET9_0_OR_GREATER
using Convert = Plinth.Common.Utils.BackPort.PlinthConvert;
#endif

using Microsoft.Extensions.Logging;
using Plinth.Storage.Models;
using Plinth.Common.Utils;
using System.Web;
using System.Text.RegularExpressions;

namespace Plinth.Storage.Providers.S3;

/// <summary>
/// Provides blob storage using AWS S3 for the data
/// </summary>
internal partial class S3Provider : StorageProviderBase
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private readonly S3Settings _s3Settings;
    private readonly Amazon.S3.IAmazonS3 _client;

    public override string Name => $"S3:{_s3Settings.BucketName}";

    /// <summary>
    /// Construct a S3Storage instance for blobs stored in AWS S3 objects
    /// </summary>
    /// <remarks>Uses the 'Index' column to store the S3 object path</remarks>
    /// <param name="s3Settings">S3 storage settings</param>
    public S3Provider(S3Settings s3Settings)
    {
        if (string.IsNullOrEmpty(s3Settings.BucketName))
            throw new ArgumentNullException(nameof(s3Settings), "BucketName");

        _s3Settings = s3Settings;

        var config = new Amazon.S3.AmazonS3Config();
        if (s3Settings.S3Url != null)
            config.ServiceURL = s3Settings.S3Url;

        if (_s3Settings.UseSystemAuth)
            _client = new Amazon.S3.AmazonS3Client(config);
        else
            _client = new Amazon.S3.AmazonS3Client(_s3Settings.ClientId, _s3Settings.ClientSecret, config);
    }

    /// <summary>
    /// The S3 provider writes data to S3 Web Services, which is not transactional
    /// </summary>
    public override bool WritesDataToIndex => false;

    /// <summary>
    /// S3 provider uses the index as the path in the bucket to the object
    /// </summary>
    public override string? ConstructBlobIndex(Blob blob, Func<Blob, string?>? indexStrategy)
    {
        if (indexStrategy != null)
            return indexStrategy.Invoke(blob);

        if (_s3Settings.CustomIndexStrategy != null)
            return _s3Settings.CustomIndexStrategy.Invoke(blob);

        switch (_s3Settings.DefaultIndexStrategy)
        {
            case DefaultIndexStrategy.Flat:
                return string.Empty;

            case DefaultIndexStrategy.ByDate:
            {
                var d = DateTime.UtcNow;
                return FixIndex(Path.Combine(d.Year.ToString("D2"), d.Month.ToString("D2"), d.Day.ToString("D2")));
            }

            case DefaultIndexStrategy.ByDateTime:
            {
                var d = DateTime.UtcNow;
                return FixIndex(Path.Combine(d.Year.ToString("D2"), d.Month.ToString("D2"), d.Day.ToString("D2"), d.Hour.ToString("D2")));
            }

            default:
                throw new NotSupportedException(_s3Settings.DefaultIndexStrategy.ToString());
        }
    }
    
    /// <summary>
    /// Load the bytes from S3
    /// </summary>
    public override async Task<byte[]?> ReadBlobDataAsync(Blob blob, CancellationToken cancellationToken)
    {
        using (new TimeUtil.TimeLogger("ReadBlobAsync(S3)", log))
        {
            var r = GetReadRequest(blob);
            using var response = await _client.GetObjectAsync(r, cancellationToken);
            using var ms = new MemoryStream((int)response.ContentLength);
            await response.ResponseStream.CopyToAsync(ms, cancellationToken);
            var s3data = ms.ToArray();
            log.Debug($"read {s3data.Length} bytes from S3: {r.Key}");
            return s3data;
        }
    }

    /// <summary>
    /// Be sure to dispose the returned stream!
    /// </summary>
    public override async Task<Stream?> ReadBlobDataStreamAsync(Blob blob, Stream? dbStream, CancellationToken cancellationToken)
    {
        var r = GetReadRequest(blob);
        var response = await _client.GetObjectAsync(r, cancellationToken);
        return response.ResponseStream;
    }

    private Amazon.S3.Model.GetObjectRequest GetReadRequest(Blob b)
    {
        var path = BlobNameForBlobGuid(b);

        return new Amazon.S3.Model.GetObjectRequest
        {
            BucketName = _s3Settings.BucketName,
            Key = path
        };
    }

    private static string FixIndex(string origIndex) => origIndex.Replace('\\', '/');

    private string BlobNameForBlobGuid(Blob b)
    {
        string fn = b.Guid!.Value.ToString().ToLowerInvariant();

        if (!string.IsNullOrEmpty(b.Index))
            fn = Path.Combine(b.Index, fn);

        // normally, we add the extension to the blob to support url access. this is optionally disabled
        if (_s3Settings.DisableBlobExtensions != true)
        {
            var extension = Path.GetExtension(b.Name);
            if (!string.IsNullOrEmpty(extension))
                fn += extension;
        }

        if (!string.IsNullOrEmpty(_s3Settings.BlobPrefix))
            fn = _s3Settings.BlobPrefix + fn;

        return FixIndex(fn);
    }

    private static string? GetMd5FromParams(IDictionary<string, object>? providerParams)
    {
        if (providerParams == null || !providerParams.TryGetValue(S3Settings.S3ProviderParam_ContentMD5, out var md5))
            return null;

        if (md5 is byte[] b)
            return Convert.ToBase64String(b);
        else if (md5 is string s)
        {
            // base64 md5 is 22 chars or 24 with padding
            if (s.Length == 22 || s.Length == 24)
                return s;
            // probably hex
            else if (s.Length == 16)
                return Convert.ToBase64String(Convert.FromHexString(s));
        }

        return null;
    }

    /// <summary>
    /// Write the data to an S3 Object
    /// </summary>
    public override async Task WriteBlobAsync(Blob blob, byte[] data, IDictionary<string, object>? providerParams, CancellationToken cancellationToken)
    {
        string name = BlobNameForBlobGuid(blob);

        var putReq = new Amazon.S3.Transfer.TransferUtilityUploadRequest()
        {
            BucketName = _s3Settings.BucketName,
            InputStream = new MemoryStream(data),
            Key = name,
            ContentType = blob.MimeType,
            StorageClass = Amazon.S3.S3StorageClass.Standard // start as standard, use lifecycle transitions to go to Standard_IA
        };
        putReq.Headers.ContentDisposition = GetContentDispositionHeader(blob.Name);

        if (_s3Settings.AllowPublicRead == true)
            putReq.CannedACL = Amazon.S3.S3CannedACL.PublicRead;

        using (new TimeUtil.TimeLogger("writing to S3 (bytes)", log))
        using (var xfer = new Amazon.S3.Transfer.TransferUtility(_client))
        {
            await xfer.UploadAsync(putReq, cancellationToken);
        }

        log.Debug($"wrote {data.Length} bytes to s3: {name}");
    }

    public override async Task<(long origLen, long storedLen, bool wrote)> WriteBlobStreamAsync(Blob blob, StreamCounter stream, long lengthHint, IDictionary<string, object>? providerParams, CancellationToken cancellationToken)
    {
        if (lengthHint < 0)
            throw new NotSupportedException("S3 Provider requires a length hint for uploading via stream");

        string name = BlobNameForBlobGuid(blob);

        var putReq = new Amazon.S3.Model.PutObjectRequest
        {
            BucketName = _s3Settings.BucketName,
            InputStream = stream,
            Key = name,
            MD5Digest = GetMd5FromParams(providerParams),
            ContentType = blob.MimeType,
            StorageClass = Amazon.S3.S3StorageClass.Standard // start as standard, use lifecycle transitions to go to Standard_IA
        };
        putReq.Headers.ContentLength = lengthHint;
        putReq.Headers.ContentDisposition = GetContentDispositionHeader(blob.Name);

        if (_s3Settings.AllowPublicRead == true)
            putReq.CannedACL = Amazon.S3.S3CannedACL.PublicRead;

        using (new TimeUtil.TimeLogger("writing to S3 (stream)", log))
        {
            // put object is used vs transfer utility because the latter requires a seekable stream
            await _client.PutObjectAsync(putReq, cancellationToken);
        }

        long storedLen = stream.BytesRead;
        long origLen = stream.BytesRead;

        log.Debug($"wrote {storedLen} bytes to s3: {name}");

        return (origLen, storedLen, true);
    }

    /// <summary>
    /// Remove the object from the buckets
    /// </summary>
    public override async Task DeleteBlobAsync(Blob blob, CancellationToken cancellationToken)
    {
        var delRequest = new Amazon.S3.Model.DeleteObjectRequest
        {
            BucketName = _s3Settings.BucketName,
            Key = BlobNameForBlobGuid(blob)
        };

        log.Debug($"removing object {_s3Settings.BucketName}/{delRequest.Key}");
        await _client.DeleteObjectAsync(delRequest, cancellationToken);
    }

    public override Task<Uri?> GetUrlReferenceAsync(Blob blob, CancellationToken cancellationToken)
        => Task.FromResult<Uri?>(new Uri($"https://{_s3Settings.BucketName}.s3.amazonaws.com/{BlobNameForBlobGuid(blob)}"));

    private Uri? GetTemporaryUrlReference(Blob blob, TimeSpan duration)
    {
        var req = new Amazon.S3.Model.GetPreSignedUrlRequest
        {
            BucketName = _s3Settings.BucketName,
            Key = BlobNameForBlobGuid(blob),
            Expires = DateTime.UtcNow + duration
        };
        return new Uri(_client.GetPreSignedURL(req));
    }

    public override Task<Uri?> GetTemporaryUrlReferenceAsync(Blob blob, TimeSpan duration, CancellationToken cancellationToken)
        => Task.FromResult(GetTemporaryUrlReference(blob, duration));


#if NET8_0_OR_GREATER
    [GeneratedRegex("[^\\u0020-\\u007E]")]
    private static partial Regex UnicodeRegex();

    private static string GetContentDispositionHeader(string fileName)
        => $"attachment; filename={UnicodeRegex().Replace(fileName, "_")}; filename*=UTF-8''{HttpUtility.UrlEncode(fileName)}";
#else
    private static string GetContentDispositionHeader(string fileName)
        => $"attachment; filename={Regex.Replace(fileName, @"[^\u0020-\u007E]", "_")}; filename*=UTF-8''{HttpUtility.UrlEncode(fileName)}";
#endif
}
