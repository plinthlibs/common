using Plinth.Storage.Models;

namespace Plinth.Storage.Providers.S3;

/// <summary>
/// Default index strategy for when the index strategy function is null
/// </summary>
public enum DefaultIndexStrategy
{
    /// <summary>The default, which is all in root directory {basePath}/{guid}</summary>
    Flat = 0,

    /// <summary>By Date of blob creation, {basePath}/YYYY/MM/DD/{guid}</summary>
    ByDate,

    /// <summary>By DateTime of blob creation (includes hour), {basePath}/YYYY/MM/DD/HH{guid}</summary>
    ByDateTime
}

/// <summary>
/// Customizations and settings for File System blob storage
/// </summary>
public class S3Settings
{
    /// <summary>
    /// Custom Provider Parameter for specifying the MD5 hash of the content
    /// </summary>
    /// <remarks>will be sent to S3 during write requests so S3 can validate transfer</remarks>
    /// <example>
    /// Supports byte[], base64 string (with or without padding), and hex encoded
    /// </example>
    public const string S3ProviderParam_ContentMD5 = "S3_ContentMD5";

    /// <summary>
    /// Bucket to write blobs
    /// </summary>
    public string BucketName { get; set; } = null!;

    /// <summary>
    /// (optional) Prefix applied to all blobs
    /// </summary>
    public string? BlobPrefix { get; set; }

    /// <summary>
    /// Set this to true if using a credentials file, App/Web.config, or AWS credential store instead of Client id/secret
    /// </summary>
    public bool UseSystemAuth { get; set; } = false;

    /// <summary>
    /// AWS Client ID
    /// </summary>
    public string? ClientId { get; set; }

    /// <summary>
    /// AWS Client Secret
    /// </summary>
    public string? ClientSecret { get; set; }

    /// <summary>
    /// AWS URL (includes region)
    /// </summary>
    public string? S3Url { get; set; }

    /// <summary>
    /// Allow public read access to BLOBs in S3
    /// </summary>
    public bool? AllowPublicRead { get; set; }

    /// <summary>
    /// By default, extensions will be added to S3 Objects to support url access by browsers.
    /// Set to true to disable that (so it works like the file system provider)
    /// </summary>
    public bool? DisableBlobExtensions { get; set; }

    /// <summary>
    /// Index Strategy to use when indexStrategy (parameter) or CustomIndexStrategy is null when creating a blob
    /// </summary>
    public DefaultIndexStrategy DefaultIndexStrategy { get; set; }

    /// <summary>
    /// An optional custom index creation strategy
    /// </summary>
    /// <remarks>Input is a Blob, output is the file sytem path that the blob will be writen {BasePath}/{Index}/{Guid}</remarks>
    public Func<Blob, string?>? CustomIndexStrategy { get; set; }
}
