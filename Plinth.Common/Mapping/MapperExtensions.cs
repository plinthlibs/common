﻿using Omu.ValueInjecter;
using System.Diagnostics.CodeAnalysis;

namespace Plinth.Common.Mapping;

/// <summary>
/// Plinth Extensions for Mapper
/// </summary>
public static class MapperExt
{
    private static readonly PropertyNameInjection DefaultParallelInjecter = new(8, 8, 8);
    private static readonly PropertyNameInjection DefaultInjecter = new();

    /// <summary>
    /// Map two objects using parallel processing.  This is a convenience method for using
    /// PropertyNameInjection with parallel options
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source"></param>
    /// <param name="listMinimum">minimum size of a list/array to map in parallel, defaults to 8</param>
    /// <param name="dictionaryMinimum">minimum size of a dictionary to map in parallel, defaults to 8</param>
    /// <param name="propsMinimum">minimum count of properties to map in parallel, defaults to 8</param>
    /// <returns></returns>
    [return: NotNullIfNotNull(nameof(source))]
    public static T? MapInParallel<T>(object? source, int listMinimum = 8, int dictionaryMinimum = 8, int propsMinimum = 8)
    {
        var inj = (listMinimum == 8 && dictionaryMinimum == 8 && propsMinimum == 8)
            ? DefaultParallelInjecter
            : new PropertyNameInjection(listMinimum, dictionaryMinimum, propsMinimum);
        return MapObjectImpl<T>(source, inj);
    }

    [return: NotNullIfNotNull(nameof(s))]
    private static Tdest? MapObjectImpl<Tdest>(object? s, PropertyNameInjection injector)
    {
        if (s == null)
            return default;
        Tdest t = Activator.CreateInstance<Tdest>();
        t.InjectFrom(injector, s);
        return t!;
    }

    /// <summary>
    /// Map a List of one object to a list of another
    /// </summary>
    /// <typeparam name="Tsource"></typeparam>
    /// <typeparam name="Tdest"></typeparam>
    /// <param name="source"></param>
    /// <returns></returns>
    [return: NotNullIfNotNull(nameof(source))]
    public static List<Tdest?>? MapList<Tsource, Tdest>(IEnumerable<Tsource?>? source)
    {
        return source?.Select(s => MapObjectImpl<Tdest>(s, DefaultInjecter)).ToList();
    }
    
    /// <summary>
    /// Map a List of one object to a list of another
    /// </summary>
    /// <remarks>This preserves order</remarks>
    /// <typeparam name="Tsource"></typeparam>
    /// <typeparam name="Tdest"></typeparam>
    /// <param name="source"></param>
    /// <param name="listMinimum">minimum size of a list/array to map in parallel, defaults to 8</param>
    /// <param name="dictionaryMinimum">minimum size of a dictionary to map in parallel, defaults to 8</param>
    /// <param name="propsMinimum">minimum count of properties to map in parallel, defaults to 8</param>
    /// <returns></returns>
    [return: NotNullIfNotNull(nameof(source))]
    public static List<Tdest?>? MapListInParallel<Tsource, Tdest>(IEnumerable<Tsource?>? source, int listMinimum = 8, int dictionaryMinimum = 8, int propsMinimum = 8)
    {
        var inj = (listMinimum == 8 && dictionaryMinimum == 8 && propsMinimum == 8)
            ? DefaultParallelInjecter
            : new PropertyNameInjection(listMinimum, dictionaryMinimum, propsMinimum);
        return source?.AsParallel().AsOrdered().Select(s => MapObjectImpl<Tdest>(s, inj)).ToList();
    }

    /// <summary>
    /// Map with null safety
    /// </summary>
    /// <returns></returns>
    [return: NotNullIfNotNull(nameof(source))]
    public static T? MapNullSafe<T>(object? source)
    {
        return source == null ?
            default :
            Mapper.Map<T>(source);
    }
}
