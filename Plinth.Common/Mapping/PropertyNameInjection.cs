using Microsoft.Extensions.Logging;
using Omu.ValueInjecter;
using Omu.ValueInjecter.Injections;
using Omu.ValueInjecter.Utils;
using System.Collections;
using System.Reflection;

namespace Plinth.Common.Mapping;

/// <summary>
/// A custom ValueInjecter injection which will attempt to inject
/// properties with the same name. Will perform string to enum conversion
/// as well as deep injection of complex objects, arrays, and ICollections.
/// </summary>
/// <remarks>
/// <para>
/// Also supports mapping Lists/Arrays/Sub-Properties in parallel.
/// <list type="bullet">
/// <item>This can be faster but must be used with caution.  Sometimes the overhead of parallel operations
/// can be slower than serial operations due to overhead.</item>
/// <item>The parallel minimum for properties is especially sensitive to this as an object with a lot of primitives is faster to map serially.
/// That option is best for deep hierarchies with lots of properties at the top level</item>
/// </list>
/// </para>
/// </remarks>
/// <example>
/// Manual Injection
/// <code>
/// Bar bar = new Bar();
/// bar.InjectFrom&lt;PropertyNameInjection&gt;(foo);
/// </code>
///
/// Setting up the default injection for the Mapper static class
/// <code>
/// StaticValueInjecter.DefaultInjection = new PropertyNameInjection();
/// Foo foo = new Foo() { };
/// Mapper.Map&lt;Bar&gt;(foo);
/// </code>
/// </example>
public class PropertyNameInjection : LoopInjection
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private readonly int _parallelListMinimum = int.MaxValue;
    private readonly int _parallelDictionaryMinimum = int.MaxValue;
    private readonly int _parallelPropertyMinimum = int.MaxValue;

    /// <summary>
    /// Constructor
    /// </summary>
    public PropertyNameInjection()
    {
    }

    /// <summary>
    /// Create an injector that will map arrays, lists, dictionaries, sets, and properties of objects in parallel, if their length is >= to the minimum
    /// </summary>
    /// <param name="parallelMinimum">minimum size for parallel mapping, 8 to 16 are good defaults</param>
    public PropertyNameInjection(int parallelMinimum)
    {
        _parallelListMinimum = parallelMinimum;
        _parallelDictionaryMinimum = parallelMinimum;
        _parallelPropertyMinimum = parallelMinimum;
    }

    /// <summary>
    /// Create an injector that will map arrays, lists, dictionaries, sets, and properties of objects in parallel, if their length is >= to the minimums
    /// </summary>
    /// <param name="listMinimum">minimum size for lists to inject in parallel, 8 to 16 are good defaults</param>
    /// <param name="dictionaryMinimum">minimum size for dictionaries to inject in parallel, 8 to 16 are good defaults</param>
    /// <param name="propertyMinimum">minimum property count to inject in parallel, 8 to 16 are good defaults</param>
    public PropertyNameInjection(int listMinimum, int dictionaryMinimum, int propertyMinimum)
    {
        _parallelListMinimum = listMinimum;
        _parallelDictionaryMinimum = dictionaryMinimum;
        _parallelPropertyMinimum = propertyMinimum;
    }

    /// <summary>
    /// override Inject to support parallel mapping
    /// </summary>
    /// <param name="source"></param>
    /// <param name="target"></param>
    protected override void Inject(object source, object target)
    {
        var sourceProps = source.GetType().GetProps();

        if (sourceProps.Length >= _parallelPropertyMinimum)
        {
            Parallel.ForEach(sourceProps, sourceProp => Execute(sourceProp, source, target));
        }
        else
        {
            foreach (var sourceProp in sourceProps)
            {
                Execute(sourceProp, source, target);
            }
        }
    }

    /// <summary>
    /// Returns whether or not the source and target types are mappable.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="target"></param>
    /// <returns></returns>
    protected override bool MatchTypes(Type source, Type target)
    {
        source = Nullable.GetUnderlyingType(source) ?? source;
        target = Nullable.GetUnderlyingType(target) ?? target;

        if (source == typeof(object)
            || target == typeof(object)
            || CanEnumInject(source, target)
            || CanCollectionInject(source, target)
            || CanDictionaryInject(source, target)
            || CanDeepInject(source, target))
            return true;

        return source == target;
    }

    /// <summary>
    /// Set value of the target property using the source property mapping.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="target"></param>
    /// <param name="sp"></param>
    /// <param name="tp"></param>
    protected override void SetValue(object source, object target, PropertyInfo sp, PropertyInfo tp)
    {
        try
        {
            var sourceVal = sp.GetValue(source, null);
            if (sourceVal == null)
            {
                tp.SetValue(target, null, null);
                return;
            }

            var sourceType = sourceVal.GetType();
            var targetType = tp.PropertyType;

            tp.SetValue(target, MapToType(sourceVal, sourceType, targetType), null);
        }
        catch (Exception ex)
        {
            log.Warn(ex, $"Could not map property {tp.Name} ({tp.PropertyType.Name}) in {target.GetType().FullName} from {sp.Name} ({sp.PropertyType.Name}) in {source.GetType().FullName}");
            throw;
        }
    }

    //
    // Mapping logic
    //

    private enum MappingType
    {
        None,
        TargetIsEnum,
        SourceIsEnum,
        TargetIsArray,
        TargetIsIList,
        TargetIsISet,
        TargetIsIDictionary,
        TargetIsDeepInjectable
    }

    private class MappingDefinition
    {
        public Type SourceType { get; set; } = null!;
        public Type TargetType { get; set; } = null!;

        public MappingType MappingType { get; set; }
    }

    private static MappingDefinition GetMappingDefinition(Type sourceType, Type targetType)
    {
        sourceType = Nullable.GetUnderlyingType(sourceType) ?? sourceType;
        targetType = Nullable.GetUnderlyingType(targetType) ?? targetType;
        if (targetType == typeof(object))
            targetType = sourceType;

        return new MappingDefinition
        {
            SourceType = sourceType,
            TargetType = targetType,
            MappingType = determineMappingType()
        };

        MappingType determineMappingType()
        {
            if (targetType.IsEnum)
                return MappingType.TargetIsEnum;
            else if (sourceType.IsEnum)
                return MappingType.SourceIsEnum;
            else if (IsTypeValueType(targetType))
                return MappingType.None;
            else if (targetType.IsArray)
                return MappingType.TargetIsArray;
            else if (IsTypeIList(targetType))
                return MappingType.TargetIsIList;
            else if (IsTypeISet(targetType))
                return MappingType.TargetIsISet;
            else if (IsTypeIDictionary(targetType))
                return MappingType.TargetIsIDictionary;
            else if (IsTypeDeepInjectable(targetType))
                return MappingType.TargetIsDeepInjectable;
            else
                return MappingType.None;
        }
    }

    private object? MapToType(object? sourceVal, MappingDefinition definition)
    {
        if (sourceVal == null)
            return null;

        return definition.MappingType switch
        {
            MappingType.TargetIsEnum => MapToEnum(sourceVal, definition.TargetType),
            MappingType.SourceIsEnum => sourceVal.ToString(),
            MappingType.TargetIsArray => MapToArray(sourceVal, definition.SourceType, definition.TargetType),
            MappingType.TargetIsIList => MapToIList(sourceVal, definition.SourceType, definition.TargetType),
            MappingType.TargetIsISet => MapToISet(sourceVal, definition.SourceType, definition.TargetType),
            MappingType.TargetIsIDictionary => MapToIDictionary(sourceVal, definition.SourceType, definition.TargetType),
            MappingType.TargetIsDeepInjectable => MapToComplexObject(sourceVal, definition.TargetType),
            _ => sourceVal,
        };
    }

    private object? MapToType(object? sourceVal, Type sourceType, Type targetType)
    {
        if (sourceVal == null)
            return null;

        sourceType = Nullable.GetUnderlyingType(sourceType) ?? sourceType;
        targetType = Nullable.GetUnderlyingType(targetType) ?? targetType;
        if (targetType == typeof(object))
            targetType = sourceType;

        if (targetType.IsEnum)
            return MapToEnum(sourceVal, targetType);
        else if (sourceType.IsEnum)
            return sourceVal.ToString();
        else if (targetType.IsArray)
            return MapToArray(sourceVal, sourceType, targetType);
        else if (IsTypeIList(targetType))
            return MapToIList(sourceVal, sourceType, targetType);
        else if (IsTypeISet(targetType))
            return MapToISet(sourceVal, sourceType, targetType);
        else if (IsTypeIDictionary(targetType))
            return MapToIDictionary(sourceVal, sourceType, targetType);
        else if (IsTypeDeepInjectable(targetType))
            return MapToComplexObject(sourceVal, targetType);
        else
            return sourceVal;
    }

    private static object? MapToEnum(object sourceVal, Type targetType)
    {
        try
        {
            return Enum.Parse(targetType, sourceVal.ToString() ?? string.Empty, true);
        }
        catch (Exception)
        {
            return null;
        }
    }

    private Array MapToArray(object sourceVal, Type sourceType, Type targetType)
    {
        var targetItemType = targetType.GetElementType() ?? throw new InvalidOperationException($"could not get element type of target {targetType.FullName}");

        if (sourceType.IsArray)
        {
            var sourceItemType = sourceType.GetElementType() ?? throw new InvalidOperationException($"could not get element type of source {sourceType.FullName}");
            return MapArrayToArray((Array)sourceVal, sourceItemType, targetItemType);
        }
        else
        {
            var sourceItemType = sourceType.GetGenericArguments()[0];
            return MapIListToArray((IList)sourceVal, sourceItemType, targetItemType);
        }
    }

    private object MapToIList(object sourceVal, Type sourceType, Type targetType)
    {
        var targetItemType = targetType.GetGenericArguments()[0];

        Array targetArray;
        if (sourceType.IsArray)
        {
            var sourceItemType = sourceType.GetElementType() ?? throw new InvalidOperationException($"could not get element type of source {sourceType.FullName}");
            targetArray = MapArrayToArray((Array)sourceVal, sourceItemType, targetItemType);
        }
        else
        {
            var sourceItemType = sourceType.GetGenericArguments()[0];
            targetArray = MapIListToArray((IList)sourceVal, sourceItemType, targetItemType);
        }

        if (targetType.GetGenericTypeDefinition() == typeof(IList<>))
            targetType = typeof(List<>).MakeGenericType(targetItemType);

        var targetCollection = Activator.CreateInstance(targetType, targetArray.Length) ?? throw new InvalidOperationException($"could not create {targetType.FullName}");;
        var addRangeMethod = targetType.GetMethod("AddRange") ?? throw new InvalidOperationException($"AddRange method not found in {targetType.FullName}");
        addRangeMethod.Invoke(targetCollection, [targetArray]);

        return targetCollection;
    }

    private object MapToISet(object sourceVal, Type sourceType, Type targetType)
    {
        var sourceItemType = sourceType.GetGenericArguments()[0];
        var targetItemType = targetType.GetGenericArguments()[0];
        var mappingDefinition = GetMappingDefinition(sourceItemType, targetItemType);

        if (targetType.GetGenericTypeDefinition() == typeof(ISet<>))
            targetType = typeof(HashSet<>).MakeGenericType(targetItemType);

        var targetSet = Activator.CreateInstance(targetType) ?? throw new InvalidOperationException($"unable to create {targetType.FullName}");
        var sourceSet = ((IEnumerable)sourceVal).Cast<object>();

        var addMethod = targetType.GetMethod("Add") ?? throw new InvalidOperationException($"no Add method on {targetType.FullName}");
        var sourceCount = (int)(sourceType.GetProperty("Count") ?? throw new InvalidOperationException($"no Count property on {sourceType.FullName}")).GetValue(sourceVal)!;

        if (sourceCount >= _parallelDictionaryMinimum)
        {
            var targetArray = Array.CreateInstance(targetItemType, sourceCount);

            Parallel.ForEach(sourceSet, (item, pls, index) =>
            {
                var targetItem = MapToType(item, mappingDefinition);
                targetArray.SetValue(targetItem, index);
            });

            for (int index = 0; index < targetArray.Length; index++)
            {
                addMethod.Invoke(targetSet, [targetArray.GetValue(index)]);
            }
        }
        else
        {
            foreach (var item in sourceSet)
            {
                var targetItem = MapToType(item, mappingDefinition);
                addMethod.Invoke(targetSet, [targetItem]);
            }
        }

        return targetSet;
    }

    private Array MapArrayToArray(Array sourceArray, Type sourceItemType, Type targetItemType)
    {
        var targetArray = Array.CreateInstance(targetItemType, sourceArray.Length);
        var mappingDefinition = GetMappingDefinition(sourceItemType, targetItemType);

        if (sourceArray.Length >= _parallelListMinimum)
        {
            Parallel.For(0, sourceArray.Length, (index) =>
            {
                var targetItem = MapToType(sourceArray.GetValue(index), mappingDefinition);
                targetArray.SetValue(targetItem, index);
            });
        }
        else
        {
            for (int index = 0; index < sourceArray.Length; index++)
            {
                var targetItem = MapToType(sourceArray.GetValue(index), mappingDefinition);
                targetArray.SetValue(targetItem, index);
            }
        }

        return targetArray;
    }

    private Array MapIListToArray(IList sourceList, Type sourceItemType, Type targetItemType)
    {
        var targetArray = Array.CreateInstance(targetItemType, sourceList.Count);
        var mappingDefinition = GetMappingDefinition(sourceItemType, targetItemType);

        if (sourceList.Count >= _parallelListMinimum)
        {
            Parallel.For(0, sourceList.Count, (index) =>
            {
                var targetItem = MapToType(sourceList[index], mappingDefinition);
                targetArray.SetValue(targetItem, index);
            });
        }
        else
        {
            for (int index = 0; index < sourceList.Count; index++)
            {
                var targetItem = MapToType(sourceList[index], mappingDefinition);
                targetArray.SetValue(targetItem, index);
            }
        }

        return targetArray;
    }

    private object MapToIDictionary(object sourceVal, Type sourceType, Type targetType)
    {
        var sourceKeyItemType = sourceType.GetGenericArguments()[0];
        var targetKeyItemType = targetType.GetGenericArguments()[0];
        var keyMappingDefinition = GetMappingDefinition(sourceKeyItemType, targetKeyItemType);

        var sourceValueItemType = sourceType.GetGenericArguments()[1];
        var targetValueItemType = targetType.GetGenericArguments()[1];
        var valueMappingDefinition = GetMappingDefinition(sourceValueItemType, targetValueItemType);

        if (targetType.GetGenericTypeDefinition() == typeof(IDictionary<,>))
            targetType = typeof(Dictionary<,>).MakeGenericType(targetKeyItemType, targetValueItemType);

        var targetDictionary = Activator.CreateInstance(targetType) ?? throw new InvalidOperationException($"could not construct IDictionary: {targetType.FullName}");
        var sourceDictionary = (sourceVal as IDictionary) ?? throw new InvalidOperationException($"source type is not IDictionary: {sourceType.FullName}");

        var addMethod = targetType.GetMethod("Add") ?? throw new InvalidOperationException($"no Add method on {targetType}");

        if (sourceDictionary.Count >= _parallelDictionaryMinimum)
        {
            var targetKeyArray = Array.CreateInstance(targetKeyItemType, sourceDictionary.Count);
            var targetValueArray = Array.CreateInstance(targetValueItemType, sourceDictionary.Count);

            Parallel.ForEach(sourceDictionary.Keys.Cast<object>(), (key, pls, index) =>
            {
                var targetKeyItem = MapToType(key, keyMappingDefinition);
                targetKeyArray.SetValue(targetKeyItem, index);

                var targetValueItem = MapToType(sourceDictionary[key], valueMappingDefinition);
                targetValueArray.SetValue(targetValueItem, index);
            });

            for (int index = 0; index < targetKeyArray.Length; index++)
            {
                addMethod.Invoke(targetDictionary, [targetKeyArray.GetValue(index), targetValueArray.GetValue(index)]);
            }
        }
        else
        {
            foreach (var key in sourceDictionary.Keys)
            {
                if (key == null)
                    continue;
                var targetKeyItem = MapToType(key, keyMappingDefinition);
                var targetValueItem = MapToType(sourceDictionary[key], valueMappingDefinition);
                addMethod.Invoke(targetDictionary, [targetKeyItem, targetValueItem]);
            }
        }

        return targetDictionary;
    }

    private object MapToComplexObject(object sourceVal, Type targetType)
    {
        return Activator.CreateInstance(targetType).InjectFrom(this, sourceVal);
    }

    //
    // Mapping type matching
    //

    internal static bool CanEnumInject(Type sourceType, Type targetType)
    {
        return (sourceType.IsEnum || sourceType == typeof(string))
            && (targetType.IsEnum || targetType == typeof(string));
    }

    internal bool CanCollectionInject(Type sourceType, Type targetType)
    {
        var sourceItemType = GetCollectionItemType(sourceType);
        if (sourceItemType == null)
            return false;

        var targetItemType = GetCollectionItemType(targetType);
        if (targetItemType == null)
            return false;

        return MatchTypes(sourceItemType, targetItemType);
    }

    internal bool CanDictionaryInject(Type sourceType, Type targetType)
    {
        if (!IsTypeIDictionary(sourceType)
            || !IsTypeIDictionary(targetType))
            return false;

        var sourceKeyItemType = sourceType.GetGenericArguments()[0];
        var targetKeyItemType = targetType.GetGenericArguments()[0];
        if (!MatchTypes(sourceKeyItemType, targetKeyItemType))
            return false;

        var sourceValueItemType = sourceType.GetGenericArguments()[1];
        var targetValueItemType = targetType.GetGenericArguments()[1];
        if (!MatchTypes(sourceValueItemType, targetValueItemType))
            return false;

        return true;
    }

    internal static bool CanDeepInject(Type sourceType, Type targetType)
    {
        return IsTypeDeepInjectable(sourceType)
            && IsTypeDeepInjectable(targetType);
    }

    //
    // Type inspectors
    //

    internal static bool IsTypeValueType(Type type)
    {
        return type.IsValueType || type == typeof(string);
    }

    internal static bool IsTypeIList(Type type)
    {
        if (!type.IsGenericType)
            return false;

        var gtd = type.GetGenericTypeDefinition();
        return gtd == typeof(IList<>)
            || gtd.GetInterfaces()
                .Any(x =>
                x == typeof(IList)
                || (x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IList<>)));
    }

    internal static bool IsTypeISet(Type type)
    {
        if (!type.IsGenericType)
            return false;

        var gtd = type.GetGenericTypeDefinition();
        return gtd == typeof(ISet<>)
            || gtd.GetInterfaces()
                .Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(ISet<>));
    }

    internal static bool IsTypeIListOrISet(Type type)
    {
        if (!type.IsGenericType)
            return false;

        var gtd = type.GetGenericTypeDefinition();
        return gtd == typeof(IList<>)
            || gtd == typeof(ISet<>)
            || gtd.GetInterfaces()
                .Any(x =>
                x == typeof(IList)
                || (x.IsGenericType && (x.GetGenericTypeDefinition() == typeof(IList<>) || x.GetGenericTypeDefinition() == typeof(ISet<>)))
               );
    }

    internal static bool IsTypeIDictionary(Type type)
    {
        if (!type.IsGenericType)
            return false;

        var gtd = type.GetGenericTypeDefinition();
        return gtd == typeof(IDictionary<,>)
            || gtd.GetInterfaces()
                .Any(x =>
                x == typeof(IDictionary)
                || (x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IDictionary<,>)));
    }

    internal static bool IsTypeDeepInjectable(Type type)
    {
        return !IsTypeValueType(type)
            && !type.IsArray
            && !type.IsGenericType;
    }

    internal static Type? GetCollectionItemType(Type collectionType)
    {
        if (collectionType.IsArray)
            return collectionType.GetElementType();
        else if (IsTypeIListOrISet(collectionType))
            return collectionType.GetGenericArguments()[0];
        else
            return null;
    }
}
