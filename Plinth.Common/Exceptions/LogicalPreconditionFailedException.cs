namespace Plinth.Common.Exceptions;

/// <summary>
/// Logic classes should use this exception to indicate to higher layers (like the API)
/// that a precondition of an operation has not been met
/// </summary>
public class LogicalPreconditionFailedException : LogicalException
{
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="data">Custom exception data to set</param>
    public LogicalPreconditionFailedException(object data) : base(data)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data">Custom exception data to set</param>
    /// <param name="innerException">Parameter for underlying .NET Exception</param>
    public LogicalPreconditionFailedException(object data, Exception innerException) : base(data, "PreconditionFailed", innerException)
    {
    }
}
