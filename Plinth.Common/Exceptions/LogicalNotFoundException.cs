namespace Plinth.Common.Exceptions;

/// <summary>
/// Logic classes should use this exception to indicate to higher layers (like the API)
/// that a not found condition has occurred
/// </summary>
public class LogicalNotFoundException : LogicalException
{
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="data">Custom exception data to set</param>
    public LogicalNotFoundException(object data) : base(data)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data">Custom exception data to set</param>
    /// <param name="innerException">Parameter for underlying .NET Exception</param>
    public LogicalNotFoundException(object data, Exception innerException) : base(data, "NotFound", innerException)
    {
    }
}
