namespace Plinth.Common.Exceptions;

/// <summary>
/// Logic classes should use this exception to indicate to higher layers (like the API)
/// that a security/forbidden condition has occurred
/// </summary>
public class LogicalForbiddenException : LogicalException
{
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="data">Custom exception data to set</param>
    public LogicalForbiddenException(object data) : base(data)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data">Custom exception data to set</param>
    /// <param name="innerException">Parameter for underlying .NET Exception</param>
    public LogicalForbiddenException(object data, Exception innerException) : base(data, "Forbidden", innerException)
    {
    }
}
