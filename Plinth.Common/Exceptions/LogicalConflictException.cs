namespace Plinth.Common.Exceptions;

/// <summary>
/// Logic classes should use this exception to indicate to higher layers (like the API)
/// that some kind of conflicting condition has occurred
/// </summary>
public class LogicalConflictException : LogicalException
{
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="data">Custom exception data to set</param>
    public LogicalConflictException(object data) : base(data)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data">Custom exception data to set</param>
    /// <param name="innerException">Parameter for underlying .NET Exception</param>
    public LogicalConflictException(object data, Exception innerException) : base(data, "Conflict", innerException)
    {
    }
}
