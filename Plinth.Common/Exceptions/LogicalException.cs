﻿using System.Runtime.Serialization;

namespace Plinth.Common.Exceptions;

/// <summary>
/// Logic classes should use this exception to indicate to higher layers (like the API)
/// that some kind of conflicting condition has occurred
/// </summary>
public abstract class LogicalException : Exception
{
    /// <summary>
    /// Custom exception data used for Message
    /// </summary>
    public object? CustomData { get; }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="data">Custom exception data to set</param>
    protected LogicalException(object data) : base()
    {
        CustomData = data;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="data">Custom exception data</param>
    /// <param name="message">Parameter for underlying .NET Exception</param>
    /// <param name="innerException">Parameter for underlying .NET Exception</param>
    protected LogicalException(object data, string message, Exception innerException) : base(message, innerException)
    {
        CustomData = data;
    }

    /// <summary>
    /// Exception message
    /// </summary>
    public override string Message => ToString();

    /// <summary>
    /// Convert exception to string
    /// </summary>
    /// <returns>CustomData if set, otherwise default .NET exception message</returns>
    public override string ToString()
    {
        if (CustomData is string msg)
            return msg;
        return base.Message;
    }
}
