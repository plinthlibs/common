namespace Plinth.Common.Exceptions;

/// <summary>
/// Logic classes should use this exception to indicate to higher layers (like the API)
/// that some kind of bad input has been received
/// </summary>
public class LogicalBadRequestException : LogicalException
{
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="data">Custom exception data to set</param>
    public LogicalBadRequestException(object data) : base(data)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data">Custom exception data to set</param>
    /// <param name="innerException">Parameter for underlying .NET Exception</param>
    public LogicalBadRequestException(object data, Exception innerException) : base(data, "BadRequest", innerException)
    {
    }
}
