using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;

namespace Plinth.Common.Extensions;

/// <summary>
/// Methods to help protect sensitive data from appearing in logs
/// </summary>
public static partial class SensitiveDataExtensions
{
    // Note: the insertion text for redacting should be compatible with all supported document
    // types (e.g. JSON, XML, query string)
    private const string _RedactedMark = "_REDACTED_";

    private const string _JsonReplacement = "$1" + _RedactedMark +"\"";
    private const string _XmlReplacement = "<$1>" + _RedactedMark + "</$1>";
    private const string _QueryReplacement = "$1" + _RedactedMark;

#region SSN
#if NET8_0_OR_GREATER
    [GeneratedRegex("(\"ssn\"\\s*:\\s*\")(\\d{3}[\\-]?\\d{2}[\\-]?\\d{4})\"", RegexOptions.IgnoreCase)]
    private static partial Regex SsnJsonRegex();
    [GeneratedRegex("<(ssn)>(\\d{3}[\\-]?\\d{2}[\\-]?\\d{4})</ssn>", RegexOptions.IgnoreCase)]
    private static partial Regex SsnXmlRegex();
    [GeneratedRegex("([?&]ssn=)(\\d{3}[\\-]?\\d{2}[\\-]?\\d{4})", RegexOptions.IgnoreCase)]
    private static partial Regex SsnQueryRegex();
#else
    private static readonly Regex _ssnJsonRegex = new("(\"ssn\"\\s*:\\s*\")(\\d{3}[\\-]?\\d{2}[\\-]?\\d{4})\"", RegexOptions.Compiled | RegexOptions.IgnoreCase);
    private static readonly Regex _ssnXmlRegex = new("<(ssn)>(\\d{3}[\\-]?\\d{2}[\\-]?\\d{4})</\\1>", RegexOptions.Compiled | RegexOptions.IgnoreCase);
    private static readonly Regex _ssnQueryRegex = new("([?&]ssn=)(\\d{3}[\\-]?\\d{2}[\\-]?\\d{4})", RegexOptions.Compiled | RegexOptions.IgnoreCase);

    private static Regex SsnJsonRegex() => _ssnJsonRegex;
    private static Regex SsnXmlRegex() => _ssnXmlRegex;
    private static Regex SsnQueryRegex() => _ssnQueryRegex;
#endif

    private static string MaskJsonSsn(this string s) => SsnJsonRegex().Replace(s, _JsonReplacement);
    private static string MaskXmlSsn(this string s) => SsnXmlRegex().Replace(s, _XmlReplacement);
    private static string MaskQuerySsn(this string s) => SsnQueryRegex().Replace(s, _QueryReplacement);
#endregion

#region Password
#if NET8_0_OR_GREATER
    [GeneratedRegex("(\"[a-z]*password\"\\s*:\\s*\")(.*?)\"", RegexOptions.IgnoreCase)]
    private static partial Regex PasswordJsonRegex();
    [GeneratedRegex("<([a-z]*password)>([^<]*)</[a-z]*password>", RegexOptions.IgnoreCase)]
    private static partial Regex PasswordXmlRegex();
    [GeneratedRegex("([?&][a-z]*password=)([^&]*)", RegexOptions.IgnoreCase)]
    private static partial Regex PasswordQueryRegex();
#else
    private static readonly Regex _passwordJsonRegex = new("(\"[a-z]*password\"\\s*:\\s*\")(.*?)\"", RegexOptions.Compiled | RegexOptions.IgnoreCase);
    private static readonly Regex _passwordXmlRegex = new("<([a-z]*password)>([^<]*)</\\1>", RegexOptions.Compiled | RegexOptions.IgnoreCase);
    private static readonly Regex _passwordQueryRegex = new("([?&][a-z]*password=)([^&]*)", RegexOptions.Compiled | RegexOptions.IgnoreCase);

    private static Regex PasswordJsonRegex() => _passwordJsonRegex;
    private static Regex PasswordXmlRegex() => _passwordXmlRegex;
    private static Regex PasswordQueryRegex() => _passwordQueryRegex;
#endif

    private static string MaskJsonPassword(this string s) => PasswordJsonRegex().Replace(s, _JsonReplacement);
    private static string MaskXmlPassword(this string s) => PasswordXmlRegex().Replace(s, _XmlReplacement);
    private static string MaskQueryPassword(this string s) => PasswordQueryRegex().Replace(s, _QueryReplacement);
#endregion

#region CardNumber
#if NET8_0_OR_GREATER
    [GeneratedRegex("(\"[a-z]*cardNumber\"\\s*:\\s*\")(.*?)\"", RegexOptions.IgnoreCase)]
    private static partial Regex CardNumberJsonRegex();
    [GeneratedRegex("<([a-z]*cardNumber)>([^<]*)</[a-z]*cardNumber>", RegexOptions.IgnoreCase)]
    private static partial Regex CardNumberXmlRegex();
    [GeneratedRegex("([?&][a-z]*cardNumber=)([^&]*)", RegexOptions.IgnoreCase)]
    private static partial Regex CardNumberQueryRegex();
#else
    private static readonly Regex _cardNumberJsonRegex = new("(\"[a-z]*cardNumber\"\\s*:\\s*\")(.*?)\"", RegexOptions.Compiled | RegexOptions.IgnoreCase);
    private static readonly Regex _cardNumberXmlRegex = new("<([a-z]*cardNumber)>([^<]*)</\\1>", RegexOptions.Compiled | RegexOptions.IgnoreCase);
    private static readonly Regex _cardNumberQueryRegex = new("([?&][a-z]*cardNumber=)([^&]*)", RegexOptions.Compiled | RegexOptions.IgnoreCase);

    private static Regex CardNumberJsonRegex() => _cardNumberJsonRegex;
    private static Regex CardNumberXmlRegex() => _cardNumberXmlRegex;
    private static Regex CardNumberQueryRegex() => _cardNumberQueryRegex;
#endif

    private static string MaskJsonCardNumber(this string s) => CardNumberJsonRegex().Replace(s, _JsonReplacement);
    private static string MaskXmlCardNumber(this string s) => CardNumberXmlRegex().Replace(s, _XmlReplacement);
    private static string MaskQueryCardNumber(this string s) => CardNumberQueryRegex().Replace(s, _QueryReplacement);
#endregion

    /// <summary>
    /// Masks values in the provided JSON string which match the provided key and value regular expression.
    /// </summary>
    /// <param name="s"></param>
    /// <param name="keyRegex"></param>
    /// <param name="valueRegex"></param>
    /// <returns>string with masked specified data</returns>
    public static string MaskJsonValues(this string s, string? keyRegex = null, string? valueRegex = null)
    {
        string regexFormat = string.Format("(\"{0}\"\\s*:\\s*\")({1})\"", keyRegex ?? ".*?", valueRegex ?? ".*?");
        return Regex.Replace(s, regexFormat, _JsonReplacement, RegexOptions.IgnoreCase);
    }

    /// <summary>
    /// Masks values in the provided XML string which match the provided key and value regular expression.
    /// </summary>
    /// <param name="s"></param>
    /// <param name="keyRegex"></param>
    /// <param name="valueRegex"></param>
    /// <returns>string with masked specified data</returns>
    public static string MaskXmlValues(this string s, string? keyRegex = null, string? valueRegex = null)
    {
        string regexFormat = string.Format("<({0})>({1})</\\1>", keyRegex ?? ".*?", valueRegex ?? "[^<]*");
        return Regex.Replace(s, regexFormat, _XmlReplacement, RegexOptions.IgnoreCase);
    }

    /// <summary>
    /// Masks values in the provided URI query string which match the provided key and value regular expression.
    /// </summary>
    /// <param name="s"></param>
    /// <param name="keyRegex"></param>
    /// <param name="valueRegex"></param>
    /// <returns>string with masked specified data</returns>
    public static string MaskQueryStringValues(this string s, string? keyRegex = null, string? valueRegex = null)
    {
        string regexFormat = string.Format("([?&]{0}=)({1})", keyRegex ?? ".*?", valueRegex ?? "[^&]*");
        return Regex.Replace(s, regexFormat, _QueryReplacement, RegexOptions.IgnoreCase);
    }

    /// <summary>
    /// Masks json and xml versions of the ssn field
    /// </summary>
    /// <param name="s"></param>
    /// <returns>string with masked ssn values</returns>
    public static string MaskSsn(this string s)
    {
        int p = s.IndexOf("ssn", 0, StringComparison.OrdinalIgnoreCase);
        if (p == -1)
            return s;

        int jsonSsnIndex = s.IndexOf("\"ssn\"", Math.Max(p - 1, 0), StringComparison.OrdinalIgnoreCase);
        if (jsonSsnIndex >= 0)
            s = s.MaskJsonSsn();

        int xmlSsnIndex = s.IndexOf("<ssn>", Math.Max(p - 1, 0), StringComparison.OrdinalIgnoreCase);
        if (xmlSsnIndex >= 0)
            s = s.MaskXmlSsn();

        int querySsnIndex = s.IndexOf("ssn=", p, StringComparison.OrdinalIgnoreCase);
        if (querySsnIndex >= 0)
            s = s.MaskQuerySsn();

        return s;
    }

    /// <summary>
    /// Masks json and xml versions of the password field
    /// </summary>
    /// <param name="s"></param>
    /// <returns>string with masked password values</returns>
    public static string MaskPassword(this string s)
    {
        int p = s.IndexOf("password", 0, StringComparison.OrdinalIgnoreCase);
        if (p == -1)
            return s;

        int jsonPasswordIndex = s.IndexOf("password\"", p, StringComparison.OrdinalIgnoreCase);
        if (jsonPasswordIndex >= 0)
            s = s.MaskJsonPassword();

        int xmlPasswordIndex = s.IndexOf("password>", p, StringComparison.OrdinalIgnoreCase);
        if (xmlPasswordIndex >= 0)
            s = s.MaskXmlPassword();

        int queryPasswordIndex = s.IndexOf("password=", p, StringComparison.OrdinalIgnoreCase);
        if (queryPasswordIndex >= 0)
            s = s.MaskQueryPassword();

        return s;
    }

    /// <summary>
    /// Masks json and xml versions of the credit card number field
    /// </summary>
    /// <param name="s"></param>
    /// <returns>string with masked credit card number values</returns>
    public static string MaskCreditCardNumbers(this string s)
    {
        int p = s.IndexOf("cardNumber", 0, StringComparison.OrdinalIgnoreCase);
        if (p == -1)
            return s;

        int jsonPasswordIndex = s.IndexOf("cardNumber\"", p, StringComparison.OrdinalIgnoreCase);
        if (jsonPasswordIndex >= 0)
            s = s.MaskJsonCardNumber();

        int xmlPasswordIndex = s.IndexOf("cardNumber>", p, StringComparison.OrdinalIgnoreCase);
        if (xmlPasswordIndex >= 0)
            s = s.MaskXmlCardNumber();

        int queryPasswordIndex = s.IndexOf("cardNumber=", p, StringComparison.OrdinalIgnoreCase);
        if (queryPasswordIndex >= 0)
            s = s.MaskQueryCardNumber();

        return s;
    }

    /// <summary>
    /// Masks json and xml versions of sensitive fields (ssns, passwords, credit card numbers)
    /// </summary>
    /// <param name="s"></param>
    /// <returns>string with masked sensitive field values</returns>
    [return: NotNullIfNotNull(nameof(s))]
    public static string? MaskSensitiveFields(this string? s)
    {
        if (string.IsNullOrWhiteSpace(s))
            return s;

        return s.MaskSsn()
            .MaskPassword()
            .MaskCreditCardNumbers();
    }
}
