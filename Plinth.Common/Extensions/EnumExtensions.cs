using System.ComponentModel;
using System.Text.RegularExpressions;

namespace Plinth.Common.Extensions;

/// <summary>
/// Extensions for Enums
/// </summary>
public static partial class EnumExtension
{
#if NET8_0_OR_GREATER
    [GeneratedRegex(@"(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])")]
    private static partial Regex SplitDelimiterPat();
#else
    private static Regex _SplitDelimiterPat = new Regex(@"(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])", RegexOptions.Compiled);
    private static Regex SplitDelimiterPat() => _SplitDelimiterPat;
#endif

    /// <summary>
    /// Returns enum member as a space-separated string
    /// </summary>
    /// <param name="enumMember"></param>
    /// <returns></returns>
    public static string SplitUppercaseWords(this Enum enumMember)
    {
        var splitString = SplitDelimiterPat().Split(enumMember.ToString());

        return string.Join(" ", splitString);
    }

    /// <summary>
    /// Pulls the description of an Enum from the Description attribute, if it exists
    /// </summary>
    /// <param name="value"></param>
    /// <returns>the description of the Enum value</returns>
    public static string? GetDescription(this Enum value)
    {
        var field = value.GetType().GetField(value.ToString());

        var attribute = field != null ? Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute : null;

        return attribute == null ? value.ToString() : attribute.Description;
    }

    /// <summary>
    /// Converts to another enum by name (not by integral value)
    /// </summary>
    /// <param name="value"></param>
    /// <returns>an enum value of type T that has the same value as the one from value</returns>
    public static T ToEnum<T>(this Enum value) where T : struct, Enum
    {
        return value.ToString().ToEnum<T>();
    }

    /// <summary>
    /// Converts to another enum by name (not by integral value), supports nullable enums and value not matching
    /// </summary>
    /// <param name="value"></param>
    /// <returns>an enum value of type T that has the same value as the one from value, null if value was null or no match</returns>
    public static T? ToEnumOrNull<T>(this Enum value) where T : struct, Enum
    {
        if (value != null && Enum.TryParse(value.ToString(), true, out T outVal))
            return outVal;

        return null;
    }

    /// <summary>
    /// Converts to another enum by name (not by integral value), supports nullable enums and value not matching
    /// </summary>
    /// <param name="value"></param>
    /// <param name="defaultVal"></param>
    /// <returns>an enum value of type T that has the same value as the one from value, defaultVal if there was no match or value was null</returns>
    public static T ToEnumOrDefault<T>(this Enum value, T defaultVal) where T : struct, Enum
    {
        return ToEnumOrNull<T>(value) ?? defaultVal;
    }
}
