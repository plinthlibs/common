// Put these in System.Linq for easy finding
namespace System.Linq;

/// <summary>
/// Extensions for IEnumerable
/// </summary>
public static class EnumerableExtensions
{
    /// <summary>
    /// Returns true if the Enumerable is Null or Empty.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source">can be null</param>
    /// <remarks>Uses .Any(), so this will materialize the enumerable</remarks>
    /// <returns></returns>
    public static bool IsNullOrEmpty<T>(this IEnumerable<T>? source)
    {
        if (source == null)
            return true;

        return !source.Any();
    }

    /// <summary>
    /// Returns true if the Enumerable contains more than one value.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source">can be null</param>
    /// <returns></returns>
    public static bool Many<T>(this IEnumerable<T>? source) => source?.Skip(1).Any() ?? false;

    /// <summary>
    /// Returns true if the Enumerable contains more than one value which matches the predicate.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source">can be null</param>
    /// <param name="predicate">predicate used for value matching</param>
    /// <returns></returns>
    public static bool Many<T>(this IEnumerable<T> source, Func<T, bool> predicate)
    {
        ArgumentNullException.ThrowIfNull(predicate);

        return Many(source?.Where(e => predicate(e)));
    }
}
