using System.Diagnostics.CodeAnalysis;

namespace Plinth.Common.Extensions;

/// <summary>
/// Extensions for <see cref="Stream"/>s
/// </summary>
public static class StreamExtensions
{
    /// <summary>
    /// Converts a stream to a byte array.
    /// </summary>
    /// <param name="stream"></param>
    /// <param name="leaveStreamOpen">true to leave the stream open, false to close (default is false)</param>
    /// <returns>byte array</returns>
    [return: NotNullIfNotNull(nameof(stream))]
    public static byte[]? ToBytes(this Stream? stream, bool leaveStreamOpen = false)
        => ToBytes(stream, -1, leaveStreamOpen);
    
    /// <summary>
    /// Converts a stream to a byte array.
    /// </summary>
    /// <param name="stream"></param>
    /// <param name="lengthHint">guess at the length for better performance</param>
    /// <param name="leaveStreamOpen">true to leave the stream open, false to close (default is false)</param>
    /// <returns>byte array</returns>
    [return: NotNullIfNotNull(nameof(stream))]
    public static byte[]? ToBytes(this Stream? stream, int lengthHint, bool leaveStreamOpen = false)
    {
        if (stream == null)
            return null;

        // check if the incoming stream is a memory stream. If so then just convert the stream
        if (stream is MemoryStream ms)
        {
            if (!leaveStreamOpen)
                stream.Close();
            return ms.ToArray();
        }

        // otherwise copy the stream to a memory stream and then convert
        using var newMs = new MemoryStream(lengthHint > 0 ? lengthHint : 32);
        stream.CopyTo(newMs);
        if (!leaveStreamOpen)
            stream.Close();
        return newMs.ToArray();
    }

    /// <summary>
    /// Converts a stream to a byte array.
    /// </summary>
    /// <param name="stream"></param>
    /// <param name="leaveStreamOpen">true to leave the stream open, false to close (default is false)</param>
    /// <param name="cancellationToken"></param>
    /// <returns>byte array</returns>
    [return: NotNullIfNotNull(nameof(stream))]
    public static Task<byte[]?> ToBytesAsync(this Stream? stream, bool leaveStreamOpen = false, CancellationToken cancellationToken = default)
        => ToBytesAsync(stream, -1, leaveStreamOpen, cancellationToken);

    /// <summary>
    /// Converts a stream to a byte array.
    /// </summary>
    /// <param name="stream"></param>
    /// <param name="lengthHint">guess at the length for better performance</param>
    /// <param name="leaveStreamOpen">true to leave the stream open, false to close (default is false)</param>
    /// <param name="cancellationToken"></param>
    /// <returns>byte array</returns>
    [return: NotNullIfNotNull(nameof(stream))]
    public static async Task<byte[]?> ToBytesAsync(this Stream? stream, int lengthHint, bool leaveStreamOpen = false, CancellationToken cancellationToken = default)
    {
        if (stream == null)
            return null;

        // check if the incoming stream is a memory stream. If so then just convert the stream
        if (stream is MemoryStream ms)
        {
            if (!leaveStreamOpen)
                stream.Close();
            return ms.ToArray();
        }

        // otherwise copy the stream to a memory stream and then convert
        using var newMs = new MemoryStream(lengthHint > 0 ? lengthHint : 32);
        await stream.CopyToAsync(newMs, cancellationToken);
        if (!leaveStreamOpen)
            stream.Close();
        return newMs.ToArray();
    }    
}
