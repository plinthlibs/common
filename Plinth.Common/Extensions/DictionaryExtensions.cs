namespace Plinth.Common.Extensions;

/// <summary>
/// A class containing extension methods for the dictionary class
/// </summary>
public static class DictionaryExtensions
{
    /// <summary>
    /// Returns the value associated with the key in the dictionary or the default value if the dictionary doesn't contain the key
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="dictionary"></param>
    /// <param name="key"></param>
    /// <param name="defaultValue"></param>
    /// <returns>The value associated with the key in the dictionary or the default value if the dictionary doesn't contain the key</returns>
    public static TValue? GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue? defaultValue)
    {
        return dictionary.TryGetValue(key, out var value) ? value : defaultValue;
    }
}
