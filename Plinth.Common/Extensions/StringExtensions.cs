using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.Serialization;

namespace Plinth.Common.Extensions;

/// <summary>
/// Extensions for strings;
/// </summary>
public static class StringExtensions
{
    #region Enum Methods

    /// <summary>
    /// Converts a string to an enumeration of the specified type
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="s"></param>
    /// <returns>an enumeration of the specified type</returns>
    /// <exception cref="ArgumentException">if string is not valid for enum T</exception>
    public static T ToEnum<T>(this string s) where T : struct, Enum
    {
        ArgumentNullException.ThrowIfNull(s);

        var enumVal = ToEnumOrNull<T>(s);
        if (!enumVal.HasValue)
            throw new ArgumentException($"'{s}' is not a valid enumerated value", nameof(s));

        return enumVal.Value;
    }

    /// <summary>
    /// Converts a string to an enumeration of the specified type, or a default value if null, empty, or invalid for the enum
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="s"></param>
    /// <param name="defaultVal"></param>
    /// <returns>an enumeration of the specified type, or the default</returns>
    public static T ToEnumOrDefault<T>(this string? s, T defaultVal) where T : struct, Enum
        => ToEnumOrNull<T>(s) ?? defaultVal;

    /// <summary>
    /// Converts a string to an enumeration of the specified type, or null if null, empty, or invalid for the enum
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="s"></param>
    /// <returns>a nullable enumeration of the specified type, or null</returns>
    public static T? ToEnumOrNull<T>(this string? s) where T : struct, Enum
    {
        if (s == null)
            return null;

        if (Enum.TryParse(s, true, out T outVal))
            return outVal;

        return ToAnnotatedEnumOrNull<T>(s);
    }

    /// <summary>
    /// Converts a string to an enumeration of the specified type, or null if null, empty, or invalid for the enum
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="s"></param>
    /// <returns>a nullable enumeration of the specified type, or null</returns>
    private static T? ToAnnotatedEnumOrNull<T>(string s) where T : struct, Enum
    {
        var enumType = typeof(T);

        var annotatedNamesDict = new Dictionary<string, string>();
        foreach (var name in Enum.GetNames(enumType))
        {
            var enumMemberAttribute =
                ((enumType.GetField(name)?.GetCustomAttributes(typeof(EnumMemberAttribute), true) as EnumMemberAttribute[]) ?? Enumerable.Empty<EnumMemberAttribute>())
                .SingleOrDefault();

            if (enumMemberAttribute?.Value != null)
                annotatedNamesDict.Add(enumMemberAttribute.Value.ToLower(), name);
        }

        var found = false;
        var normalizedString = string.Join(
            ",",
            s.Split(',')
                .Select(v =>
                {
                    var vlower = v.Trim().ToLower();
                    if (annotatedNamesDict.TryGetValue(vlower, out var value))
                    {
                        found = true;
                        return value;
                    }

                    return v;
                }));

        // If no annotations found, fail early since the unchanged string already failed parsing
        if (!found)
            return null;

        if (Enum.TryParse(normalizedString, true, out T outVal))
            return outVal;

        return null;
    }

    #endregion Enum Methods

    /// <summary>
    /// Converts a string to the value of the specified type
    /// </summary>
    /// <param name="value">The string to convert</param>
    /// <typeparam name="T">The type to convert the string to</typeparam>
    /// <returns>The value of the string converted to <typeparamref name="T" /></returns>
    public static T? ChangeType<T>(this string value)
    {
        var type = typeof(T);

        if (Nullable.GetUnderlyingType(type) != null)
        {
            // special case which allows whitespace strings
            // to be used as nulls for nullable types
            if (string.IsNullOrWhiteSpace(value))
                return (T?)(object?)null;
        }
        else
        {
            // for null strings we are returning null
            if (type == typeof(string) && value == null)
                return (T?)(object?)null;

            // special case which doesn't allow whitespace strings
            // to be parsed as default values for some value types
            // (i.e. built-in converter below would do that for DateTime)
            if (type.IsValueType && string.IsNullOrWhiteSpace(value))
                throw new InvalidOperationException("Cannot parse non-nullable value type from null or whitespace string.");
        }

        var converter = TypeDescriptor.GetConverter(type);
        return (T?)converter.ConvertFrom(null, CultureInfo.InvariantCulture, value);
    }

    /// <summary>
    /// Converts a string to a DateTime, or null if null, empty, or invalid
    /// </summary>
    /// <param name="dt">date/time as string</param>
    /// <param name="format">DateTime format</param>
    /// <returns>a nullable DateTime, or null</returns>
    public static DateTime? ToDateTimeOrNull(this string dt, string format)
    {
        if (!string.IsNullOrEmpty(dt) && DateTime.TryParseExact(dt, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out var outVal))
            return outVal;

        return null;
    }

    /// <summary>
    /// Converts a string to a decimal, or null if null, empty, or invalid
    /// </summary>
    /// <param name="s"></param>
    /// <returns>a nullable decimal, or null</returns>
    public static decimal? ToDecimalOrNull(this string s)
    {
        if (!string.IsNullOrEmpty(s) && decimal.TryParse(s, NumberStyles.Number, CultureInfo.InvariantCulture, out var outVal))
            return outVal;

        return null;
    }

    /// <summary>
    /// Converts a string to a integer, or null if null, empty, or invalid
    /// </summary>
    /// <param name="s"></param>
    /// <returns>a nullable integer, or null</returns>
    public static int? ToIntOrNull(this string s)
    {
        if (!string.IsNullOrEmpty(s) && int.TryParse(s, NumberStyles.Number, CultureInfo.InvariantCulture, out var outVal))
            return outVal;

        return null;
    }

    /// <summary>
    /// Converts a string to a float, or null if null, empty, or invalid
    /// </summary>
    /// <param name="s"></param>
    /// <returns>a nullable float, or null</returns>
    public static float? ToFloatOrNull(this string s)
    {
        if (!string.IsNullOrEmpty(s) && float.TryParse(s, NumberStyles.Number | NumberStyles.AllowExponent, CultureInfo.InvariantCulture, out var outVal))
            return outVal;

        return null;
    }

    /// <summary>
    /// Converts a string to a double, or null if null, empty, or invalid
    /// </summary>
    /// <param name="s"></param>
    /// <returns>a nullable double, or null</returns>
    public static double? ToDoubleOrNull(this string s)
    {
        if (!string.IsNullOrEmpty(s) && double.TryParse(s, NumberStyles.Number | NumberStyles.AllowExponent, CultureInfo.InvariantCulture, out var outVal))
            return outVal;

        return null;
    }

    /// <summary>
    /// Converts a string to a long, or null if null, empty, or invalid
    /// </summary>
    /// <param name="s"></param>
    /// <returns>a nullable long, or null</returns>
    public static long? ToLongOrNull(this string s)
    {
        if (!string.IsNullOrEmpty(s) && long.TryParse(s, NumberStyles.Number, CultureInfo.InvariantCulture, out var outVal))
            return outVal;

        return null;
    }

    /// <summary>
    /// Converts a string to a bool, or null if null, empty, or invalid
    /// </summary>
    /// <param name="s">
    /// case insensitive string to convert to bool with "true", "y", "yes" defaults for true,
    /// unless trueValues are specified</param>
    /// <param name="trueValues">list of values (like "Y") that is considered true</param>
    /// <returns>a nullable bool, or null</returns>
    public static bool ToBool(this string? s, params string[] trueValues)
    {
        return s.ToBoolOrNull(trueValues) ?? false;
    }

    private static readonly string[] boolTrueValues = ["true", "y", "yes"];

    /// <summary>
    /// Converts a string to a bool, or null if null, empty, or invalid
    /// </summary>
    /// <param name="s">
    /// case insensitive string to convert to bool with "true", "y", "yes" defaults for true,
    /// unless trueValues are specified</param>
    /// <param name="trueValues">list of values (like "Y") that is considered true</param>
    /// <returns>a nullable bool, or null</returns>
    public static bool? ToBoolOrNull(this string? s, params string[] trueValues)
    {
        if (string.IsNullOrEmpty(s))
            return null;

        if (trueValues.Length == 0)
            return boolTrueValues.Contains(s.ToLower());

        return trueValues.Select(v => v.ToLower()).Contains(s.ToLower());
    }

    /// <summary>
    /// Truncate string extension. Limits text to provided length. Appends "..." by default
    /// </summary>
    /// <example>
    /// var str = "ABCDEFG".Truncate(3);
    /// result: "ABC..."
    /// </example>
    /// <param name="value">string to be truncated</param>
    /// <param name="maxLength">number of characters to return from original string before truncation</param>
    /// <param name="append">string to append if the string was truncated, default "..."</param>
    /// <returns>the truncated string, the original (if short enough), or null if value is null</returns>
    [return: NotNullIfNotNull(nameof(value))]
    public static string? Truncate(this string? value, int maxLength, string? append = "...")
    {
        if (string.IsNullOrEmpty(value))
            return value;

        if (maxLength <= 0)
            return string.Empty;

        return value.Length <= maxLength ? value : string.Concat(value.AsSpan(0, maxLength), append ?? string.Empty);
    }
}
