﻿using Plinth.Common.Utils;

namespace Microsoft.Extensions.Logging;

/// <summary>
/// Handy extensions for ILog
/// </summary>
public static class PlinthILoggerExtensions
{
    /// <summary>
    /// Logs duration of code inside using block
    /// </summary>
    /// <example>
    /// for (int i = 0; i &lt; 2; i++)
    /// {
    ///     using (log.LogExecTiming("some code"))
    ///     {
    ///         log.Debug($"action {i}");
    ///         Action(i);
    ///     }
    /// }
    /// will log:
    /// [CTX-6540F] : Starting some code #52400089
    /// [CTX-6540F] : action 1
    /// [CTX-6540F] : some code #52400089 Took: 00.00.00.274472
    /// [CTX-6540F] : Starting some code #26517107
    /// [CTX-6540F] : action 2
    /// [CTX-6540F] : some code #26517107 Took: 00.00.00.352243
    /// </example>
    /// <param name="log">logger to use</param>
    /// <param name="description">logged before and after scope block</param>
    public static TimeUtil.TimeLogger LogExecTiming(this ILogger log, string description = "Execution")
        => new(description, log);

    /// <summary>
    /// Logs duration of code inside using block with specific log level
    /// </summary>
    /// <example>
    /// for (int i = 0; i &lt; 2; i++)
    /// {
    ///     using (log.LogExecTiming("some code", LogLevel.Information))
    ///     {
    ///         log.Debug($"action {i}");
    ///         Action(i);
    ///     }
    /// }
    /// will log:
    /// [CTX-6540F] : Starting some code #52400089
    /// [CTX-6540F] : action 1
    /// [CTX-6540F] : some code #52400089 Took: 00.00.00.274472
    /// [CTX-6540F] : Starting some code #26517107
    /// [CTX-6540F] : action 2
    /// [CTX-6540F] : some code #26517107 Took: 00.00.00.352243
    /// </example>
    /// <param name="log">logger to use</param>
    /// <param name="logLevel">desired log level</param>
    /// <param name="description">logged before and after scope block</param>
    public static TimeUtil.TimeLogger LogExecTiming(this ILogger log, LogLevel logLevel, string description = "Execution")
        => new(logLevel, description, log);
}
