
# README #

### Plinth.Common ###

**A set of generic utilities, extensions, and enhancements for dotnet applications**

### Cache
Extensions for atomic GetOrCreate on Microsoft.Extensions.Caching.Memory memory caches
Example:
```c#
IMemoryCache memCache = MemoryCacheUtil.Create();
await memCache.GetOrCreateAtomicMaxDurationAsync(
	"key", TimeSpan.FromMinutes(2), async () => await LoadValue());
```
:point_right: _MemoryCacheUtil.Create()_ is a shorthand method for creating a new IMemoryCache.


### Collections
- Case Insensitive Dictionary and HashSet
- Default Dictionary that will supply a default value for a key when requested 
- Examples:
```c#
// values are case insensitive
var set = new CaseInsensitiveHashSet<string>() { "abc", "ABC" };
set.Contains("aBc"); // true

// LINQ extension
var set = items.Where(...).Select(...).ToCaseInsensitiveHashSet();

// keys are case insensitive
var dict = new CaseInsensitiveDictionary<string, int>() { ["abc"] = 5 };
dict["ABC"]; // 5
dict.ContainsKey("aBc"); // true

// LINQ extension
var dict = items.Where(...).Select(...).ToCaseInsensitiveDictionary(v => v.ID);

// default dictionary, easy counting
var dict = new DefaultDictionary<string, int>(0);
dict["a"]++; // dict["a"] now is 1

// default dictionary, easy multi maps
var dict = new DefaultDictionary<string, ISet<int>>(() => new HashSet<int>());
dict["a"].Add(7); // "a" has a set with (7) in it
```
### Constants
Handy constants for Content Types
`ContentTypes.ApplicationJson` => `"application/json; charset=utf-8"`

### Exceptions
Logical exceptions for common API return values.  Allows for separation between logic and api projects
* _LogicalBadRequestException_
* _LogicalConflictException_
* _LogicalForbiddenException_
* _LogicalNotFoundException_
* _LogicalPreconditionFailedException_

### Extensions
A variety of useful extensions to common classes
* For manipulating _DataRow_
* _IDictionary_ `.GetValueOrDefault()`
* _IEnumerable_ `.IsNullOrEmpty()` and `.Many()`
* _Enum_ extensions for converting between enums by name (not by integer value)
* _ILogger_ extensions for logging code durations
```c#
using (log.LogExecTiming("some code"))
{
	... some code ...
	log.Debug("action");
}
// logs
[CTX-6540F] : Starting some code #52400089
[CTX-6540F] : action
[CTX-6540F] : some code #52400089 Took: 00.00.00.274472
```
* Sensitive data extensions for masking secure items like passwords, SSNs, and credit card numbers.
* _Stream_ extensions `.ToBytes()` and `.ToBytesAsync()`
* _String_ extensions for converting strings to various types, `.ChangeType<T>()` as various `.To{Type}OrNull()`

### Logging
* Extensions and enhancements to Microsoft.Extensions.ILogger for better tracing when using the Plinth ecosystem
* Provides `StaticLogManager.GetLogger()` to retrieve a static logger for the current class
* Provides `RequestTraceId` and `RequestTraceProperties` singletons for passing data through the entire call stack (including across HTTP boundaries)
* Adds shorthand calls for shorter log function names.
	* `LogTrace` => `Trace`
	* `LogDebug` => `Debug`
	* `LogInformation` => `Info` 
	* `LogWarning` => `Warn` 
	* `LogError` => `Error`
	* `LogCritical` => `Critical` or `Fatal`

### Mapping
An enhanced version of _Omu.ValueInjecter_ for easy object to object mapping
Allows for deeply nested Field to Field mapping (including Lists, Sets, and Dictionaries)

### Utils
A set of static utilities
* _AsyncUtil_: Some async utilities, mostly obsolete as of net6.0
* _DateTimeUtil_: Utilities for truncating date times, UTC/TZ conversions, and some date comparison functions
* _HexUtil_: Conversion of hex strings to bytes and vice versa. :point_right: *Obsolete as of net9.0*
* _RuntimeUtil_: Provides `FindExecutableHome()` which can find your project root at runtime
* _TimeUtil_: Provides log timing (used by _ILogger_ extension noted above)
* _UriUtil_: Provides a `Join()` method similar to _Path.Combine()_
