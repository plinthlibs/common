using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;

namespace Plinth.Common.Cache;

/// <summary>
/// Extension utilities for IMemoryCache
/// </summary>
public static class MemoryCacheUtil
{
    /// <summary>
    /// Utility method to create a memory cache
    /// </summary>
    /// <param name="options">(optional) settings</param>
    /// <returns>new memory cache</returns>
    public static IMemoryCache Create(MemoryCacheOptions? options = null)
        => new MemoryCache(Options.Create(options ?? new MemoryCacheOptions()));

    /// <summary>
    /// Utility method to create a memory cache
    /// </summary>
    /// <param name="config">action to change settings</param>
    /// <returns>new memory cache</returns>
    public static IMemoryCache Create(Action<MemoryCacheOptions> config)
    {
        var options = new MemoryCacheOptions();
        config(options);

        return Create(options);
    }
}
