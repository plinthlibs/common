using Microsoft.Extensions.Caching.Memory;
using System.Collections.Concurrent;

namespace Plinth.Common.Cache;

/// <summary>
/// Extensions for IMemoryCache
/// </summary>
public static class MemoryCacheExtensions
{
    // GetOrCreate is not atomic, it can call the value factory multiple times, and cache the winner
    // https://github.com/dotnet/runtime/issues/36499
    private static readonly ConcurrentDictionary<int, SemaphoreSlim> _semaphores = new();

    // to avoid creating MemoryCacheEntryOptions on every retrieval
    private static readonly ConcurrentDictionary<TimeSpan, MemoryCacheEntryOptions> _maxDurationCache = new();
    private static readonly ConcurrentDictionary<TimeSpan, MemoryCacheEntryOptions> _slidingCache = new();

    private static MemoryCacheEntryOptions GetMaxDurationOptions(TimeSpan duration)
        => _maxDurationCache.GetOrAdd(duration, static d => new MemoryCacheEntryOptions { AbsoluteExpirationRelativeToNow = d });

    private static MemoryCacheEntryOptions GetRecentlyUsedOptions(TimeSpan duration)
        => _slidingCache.GetOrAdd(duration, static d => new MemoryCacheEntryOptions { SlidingExpiration = d });

    /// <summary>
    /// Supports an atomic GetOrCreateAsync with an async generator and a maximum duration (absolute expiration)
    /// </summary>
    /// <remarks><see cref="GetOrCreateAtomicAsync{T}(IMemoryCache, object, MemoryCacheEntryOptions, Func{Task{T}})"/></remarks>
    /// <typeparam name="T"></typeparam>
    /// <param name="memCache"></param>
    /// <param name="key"></param>
    /// <param name="maxDuration">maximum duration for cache entry (if new)</param>
    /// <param name="valueFactoryAsync">called if key is not cached</param>
    /// <returns>the value from cache (created if necessary)</returns>
    public static Task<T> GetOrCreateAtomicMaxDurationAsync<T>(this IMemoryCache memCache, object key, TimeSpan maxDuration, Func<Task<T>> valueFactoryAsync)
        => memCache.GetOrCreateAtomicAsync(key, GetMaxDurationOptions(maxDuration), valueFactoryAsync);

    /// <summary>
    /// Supports an atomic GetOrCreateAsync with an async generator and a sliding expiration
    /// </summary>
    /// <remarks><see cref="GetOrCreateAtomicAsync{T}(IMemoryCache, object, MemoryCacheEntryOptions, Func{Task{T}})"/></remarks>
    /// <typeparam name="T"></typeparam>
    /// <param name="memCache"></param>
    /// <param name="key"></param>
    /// <param name="slidingDuration">maximum duration for cache entry, renewed upon each access</param>
    /// <param name="valueFactoryAsync">called if key is not cached</param>
    /// <returns>the value from cache (created if necessary)</returns>
    public static Task<T> GetOrCreateAtomicRecentlyUsedAsync<T>(this IMemoryCache memCache, object key, TimeSpan slidingDuration, Func<Task<T>> valueFactoryAsync)
        => memCache.GetOrCreateAtomicAsync(key, GetRecentlyUsedOptions(slidingDuration), valueFactoryAsync);

    /// <summary>
    /// Supports an atomic GetOrCreateAsync with an async generator.
    /// </summary>
    /// <remarks>
    ///   <see cref="CacheExtensions.GetOrCreateAsync{TItem}(IMemoryCache, object, Func{ICacheEntry, Task{TItem}})"/>
    ///   is not atomic (i.e. multiple threads could call the generator for the same key, though only one will be cached).
    ///   This extension provides an atomic GetOrCreateAsync
    /// </remarks>
    /// <typeparam name="T"></typeparam>
    /// <param name="memCache"></param>
    /// <param name="key"></param>
    /// <param name="options">cache entry options for eviction</param>
    /// <param name="valueFactoryAsync">called if key is not cached</param>
    /// <returns>the value from cache (created if necessary)</returns>
    public static async Task<T> GetOrCreateAtomicAsync<T>(this IMemoryCache memCache, object key, MemoryCacheEntryOptions options, Func<Task<T>> valueFactoryAsync)
    {
        if (memCache.TryGetValue(key, out T? value))
            return value!;

        var semaphore = GetSemaphore(memCache);

        await semaphore.WaitAsync();

        try
        {
            // double checked under lock
            if (!memCache.TryGetValue(key, out value))
                memCache.Set(key, value = await valueFactoryAsync(), options);

            return value!;
        }
        catch
        {
            memCache.Remove(key);
            throw;
        }
        finally
        {
            semaphore.Release();
        }
    }

    /// <summary>
    /// Supports an atomic GetOrCreate with generator and a maximum duration (absolute expiration)
    /// </summary>
    /// <remarks><see cref="GetOrCreateAtomic{T}(IMemoryCache, object, MemoryCacheEntryOptions, Func{T})"/></remarks>
    /// <typeparam name="T"></typeparam>
    /// <param name="memCache"></param>
    /// <param name="key"></param>
    /// <param name="maxDuration">maximum duration for cache entry (if new)</param>
    /// <param name="valueFactory">called if key is not cached</param>
    /// <returns>the value from cache (created if necessary)</returns>
    public static T GetOrCreateAtomicMaxDuration<T>(this IMemoryCache memCache, object key, TimeSpan maxDuration, Func<T> valueFactory)
        => memCache.GetOrCreateAtomic(key, GetMaxDurationOptions(maxDuration), valueFactory);

    /// <summary>
    /// Supports an atomic GetOrCreate with an async generator and a sliding expiration
    /// </summary>
    /// <remarks><see cref="GetOrCreateAtomic{T}(IMemoryCache, object, MemoryCacheEntryOptions, Func{T})"/></remarks>
    /// <typeparam name="T"></typeparam>
    /// <param name="memCache"></param>
    /// <param name="key"></param>
    /// <param name="slidingDuration">maximum duration for cache entry, renewed upon each access</param>
    /// <param name="valueFactory">called if key is not cached</param>
    /// <returns>the value from cache (created if necessary)</returns>
    public static T GetOrCreateAtomicRecentlyUsed<T>(this IMemoryCache memCache, object key, TimeSpan slidingDuration, Func<T> valueFactory)
        => memCache.GetOrCreateAtomic(key, GetRecentlyUsedOptions(slidingDuration), valueFactory);

    /// <summary>
    /// Supports an atomic GetOrCreate with an async generator.
    /// </summary>
    /// <remarks>
    ///   <see cref="CacheExtensions.GetOrCreateAsync{TItem}(IMemoryCache, object, Func{ICacheEntry, Task{TItem}})"/>
    ///   is not atomic (i.e. multiple threads could call the generator for the same key, though only one will be cached).
    ///   This extension provides an atomic GetOrCreateAsync
    /// </remarks>
    /// <typeparam name="T"></typeparam>
    /// <param name="memCache"></param>
    /// <param name="key"></param>
    /// <param name="options">cache entry options for eviction</param>
    /// <param name="valueFactory">called if key is not cached</param>
    /// <returns>the value from cache (created if necessary)</returns>
    public static T GetOrCreateAtomic<T>(this IMemoryCache memCache, object key, MemoryCacheEntryOptions options, Func<T> valueFactory)
    {
        if (memCache.TryGetValue(key, out T? value))
            return value!;

        var semaphore = GetSemaphore(memCache);

        semaphore.Wait();

        try
        {
            // double checked under lock
            if (!memCache.TryGetValue(key, out value))
                memCache.Set(key, value = valueFactory(), options);

            return value!;
        }
        catch
        {
            memCache.Remove(key);
            throw;
        }
        finally
        {
            semaphore.Release();
        }
    }

    private static SemaphoreSlim GetSemaphore(IMemoryCache memCache)
    {
        var semaphoreKey = memCache.GetHashCode(); // not a perfect uniqueness, some caches may share a semaphore

        if (!_semaphores.TryGetValue(semaphoreKey, out var semaphore))
        {
            SemaphoreSlim? createdSemaphore = null;

            // Try to add the value, this is not atomic, so multiple semaphores could be created, but just one will be stored!
            semaphore = _semaphores.GetOrAdd(semaphoreKey, k => createdSemaphore = new SemaphoreSlim(1));

            if (createdSemaphore != semaphore)
                createdSemaphore?.Dispose(); // This semaphore was not the one that made it into the dictionary, will not be used!
        }

        return semaphore;
    }
}
