using Microsoft.Extensions.Logging;
using Plinth.Common.Logging;
using static Plinth.Common.Logging.Scopes;
using System.Diagnostics;

namespace Plinth.Common.Utils;

/// <summary>
/// Utilities for logging and handling durations and time
/// </summary>
public static class TimeUtil
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    /// <summary>
    /// Time the duration of an action and log it
    /// </summary>
    /// <param name="action">code to time</param>
    /// <param name="description">(optional) description of action</param>
    /// <param name="theLog">(optional) log variable to use</param>
    public static void LogExecTiming(Action action, string description = "Execution", ILogger? theLog = null)
    {
        using (new TimeLogger(description, theLog))
        {
            action.Invoke();
        }
    }

    /// <summary>
    /// Handy class to log the duration of code inside a using block
    /// </summary>
    /// <example>
    /// for (int i = 0; i &lt; 2; i++)
    /// {
    ///     using(new TimeLogger("some code", log))
    ///     {
    ///         log.Debug($"action {i}");
    ///         Action(i);
    ///     }
    /// }
    /// will log:
    /// [CTX-6540F] : Starting some code #52400089
    /// [CTX-6540F] : action 1
    /// [CTX-6540F] : some code #52400089 Took: 00.00.00.274472
    /// [CTX-6540F] : Starting some code #26517107
    /// [CTX-6540F] : action 2
    /// [CTX-6540F] : some code #26517107 Took: 00.00.00.352243
    /// </example>
    public sealed class TimeLogger : IDisposable
    {
        private readonly string _desc;
        private readonly ILogger _theLog;
        private readonly Stopwatch _sw;
        private readonly string _hash;
        private readonly LogLevel _logLevel;

        /// <summary>
        /// Duration of the stop watch (as of time of call)
        /// </summary>
        public TimeSpan Duration => _sw.Elapsed;

        /// <summary>
        /// Construct a TimeLogger
        /// </summary>
        /// <param name="logLevel">desired log level</param>
        /// <param name="description">logged before and after scope block</param>
        /// <param name="theLog">(optional) log object to use</param>
        public TimeLogger(LogLevel logLevel, string description = "Execution", ILogger? theLog = null)
        {
            _theLog = theLog ?? log;
            _desc = description;
            _sw = Stopwatch.StartNew();
            _hash = _sw.GetHashCode().ToString();
            _logLevel = logLevel;

            _theLog.Log(logLevel, "Starting {description} #{hash}", description, _hash);
        }

        /// <summary>
        /// Construct a TimeLogger (defaults to Debug)
        /// </summary>
        /// <param name="description">logged before and after scope block</param>
        /// <param name="theLog">(optional) log object to use</param>
        public TimeLogger(string description = "Execution", ILogger? theLog = null)
            : this(LogLevel.Debug, description, theLog)
        {
        }

        /// <summary>
        /// Stops the timing and logs the total duration
        /// </summary>
        public void Dispose()
        {
            _sw.Stop();

            var props = CreateScope(
                Kvp(LoggingConstants.Field_TimeDurationMicros, (long)(_sw.Elapsed.TotalMilliseconds * 1000)),
                Kvp(LoggingConstants.Field_MessageType, "OperationDuration")
            );

            using (_theLog.BeginScope(props))
            {
                _theLog.Log(_logLevel, "{"+LoggingConstants.Field_TimeOperation+"} #{hash} Took: {"+LoggingConstants.Field_TimeDuration+"}", _desc, _hash, _sw.Elapsed);
            }
        }
    }
}
