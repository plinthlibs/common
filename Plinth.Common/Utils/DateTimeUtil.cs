using System.Diagnostics.CodeAnalysis;

namespace Plinth.Common.Utils;

/// <summary>
/// Utilities for dealing with DateTimes and time zones
/// </summary>
public static class DateTimeUtil
{
    /// <summary>
    /// Calculates the difference between two dates in decimal years
    /// </summary>
    public static decimal YearDiff(DateTime newer, DateTime older)
        => (decimal)((newer - older).TotalDays / 365.2424177);

    /// <summary>
    /// Determines which of two dates is the newer date
    /// </summary>
    public static DateTime MostRecent(DateTime date1, DateTime date2)
        => date1 > date2 ? date1 : date2;

    /// <summary>
    /// Truncates date/time value to the current second (i.e. removes the milliseconds portion)
    /// </summary>
    public static DateTime TruncateToSecond(DateTime t) => new(t.Year, t.Month, t.Day, t.Hour, t.Minute, t.Second, t.Kind);

    /// <summary>
    /// Truncates date/time value to the current minute (i.e. removes the seconds and milliseconds portion)
    /// </summary>
    public static DateTime TruncateToMinute(DateTime t) => new(t.Year, t.Month, t.Day, t.Hour, t.Minute, 0, t.Kind);

    /// <summary>
    /// Truncates date/time value to the current hour (i.e. removes the minutes, seconds and milliseconds portion)
    /// </summary>
    public static DateTime TruncateToHour(DateTime t) => new(t.Year, t.Month, t.Day, t.Hour, 0, 0, t.Kind);

    /// <summary>
    /// Returns a date/time value at the beginning of the day (the time portion will 00:00:00.000)
    /// </summary>
    public static DateTime BeginningOfDay(DateTime t) => t.Date;

    /// <summary>
    /// Returns a date/time value at the end of the day (the time portion will be 23:59:59.999)
    /// </summary>
    public static DateTime EndOfDay(DateTime t) => BeginningOfDay(t.AddDays(1)).AddMilliseconds(-1);

    /// <summary>
    /// Returns the number of days in the given year
    /// </summary>
    public static int DaysInYear(int year) => (new DateTime(year + 1, 1, 1, 0, 0, 0, DateTimeKind.Utc) - new DateTime(year, 1, 1, 0, 0, 0, DateTimeKind.Utc)).Days;

    /// <summary>
    /// Converts a DateTime? from Pacific time zone to UTC
    /// </summary>
    /// <param name="date">Date/time to convert</param>
    /// <param name="timeZone">Timezone ID corresponding to the current OS runtime</param>
    [return: NotNullIfNotNull(nameof(date))]
    public static DateTime? ToUtc(DateTime? date, string timeZone)
        => date == null ? null : TimeZoneInfo.ConvertTimeToUtc(date.Value, TimeZoneInfo.FindSystemTimeZoneById(timeZone));

    /// <summary>
    /// Converts a DateTime? from UTC to a time zone
    /// </summary>
    /// <param name="date">Date/time to convert</param>
    /// <param name="timeZone">Timezone ID corresponding to the current OS runtime</param>
    [return: NotNullIfNotNull(nameof(date))]
    public static DateTime? FromUtc(DateTime? date, string timeZone)
        => date == null ? null : TimeZoneInfo.ConvertTimeFromUtc(date.Value, TimeZoneInfo.FindSystemTimeZoneById(timeZone));
}
