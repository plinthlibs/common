namespace Plinth.Common.Utils;

/// <summary>
/// Utilities for manipulating URLs
/// </summary>
public static class UrlUtil
{ 
    /// <summary>
    /// Joins URL string and another path string.
    /// Slash ('/') character is trimmed from the end of the URL and the beginning of the path.
    /// </summary>
    public static string Join(string url, string path) => $"{url.TrimEnd('/')}/{path.TrimStart('/')}";

    /// <summary>
    /// Joins URL string and multiple path strings.
    /// Slash ('/') character is trimmed correspondingly in places where strings are concatenated.
    /// </summary>
    public static string Join(string url, params string[] paths) => paths.Aggregate(url, Join);
}
