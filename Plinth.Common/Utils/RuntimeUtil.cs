﻿using System.Diagnostics;
using System.Reflection;

namespace Plinth.Common.Utils;

/// <summary>
/// Runtime/Program utilities
/// </summary>
public static class RuntimeUtil
{
    /// <summary>
    /// Get the path of the program's home directory, useful for loading appsettings.json
    /// </summary>
    /// <example>
    /// Put this in your BuildWebHost() function in Program.cs.
    /// It will properly set up your service whether running in VS, a myservice.exe, or dotnet.exe myservice.dll
    /// <code>.UseContentRoot(WindowsServiceHost.FindContentRoot())</code>
    /// </example>
    public static string FindExecutableHome(string appSettingsBase = "appsettings")
    {
        var appsettingsJson = appSettingsBase.EndsWith(".json") ? appSettingsBase : $"{appSettingsBase}.json";
        foreach (var location in AppSettingsPaths())
        {
            if (location == null)
                continue;

            var ret = Path.Combine(location, appsettingsJson);
            if (File.Exists(ret))
                return location;

            // when running in VS, the root dir is the bin/netcoreapp2.x/ directory which does not contain appsettings.json
            // so this attempts to find it by walking up the tree
            var dir = Directory.GetParent(location);
            while (dir != null)
            {
                if (dir.GetFiles(appsettingsJson).Length == 1)
                {
                    ret = Path.Combine(dir.FullName, appsettingsJson);
                    if (File.Exists(ret))
                        return dir.FullName;
                }
                dir = dir.Parent;
            }
        }

        throw new InvalidOperationException($"unable to locate {appsettingsJson}");
    }

    private static IEnumerable<string?> AppSettingsPaths()
    {
        string? basePath = tryGetPath(() => AppContext.BaseDirectory, "base directory");
        if (basePath != null)
            yield return basePath;

        string? entryAssembly = tryGetPath(() => Assembly.GetEntryAssembly()?.Location, "entry assembly");
        if (entryAssembly != null)
            yield return entryAssembly;

        string? execAssembly = tryGetPath(() => Assembly.GetExecutingAssembly().Location, "exec assembly");
        if (execAssembly != null)
            yield return execAssembly;

        string? mainModule = tryGetPath(() => Process.GetCurrentProcess().MainModule?.FileName, "main exe");
        if (mainModule != null)
            yield return mainModule;

        yield return Directory.GetCurrentDirectory();

        static string? tryGetPath(Func<string?> f, string desc)
        {
            string? loc = null;
            try
            {
                loc = f();
            }
            catch (Exception e)
            {
                // getting assembly locations can fail sometimes
                Debug.WriteLine($"exception {e.GetType().FullName} during load of {desc} {e.Message}");
                return null;
            }
            return Path.GetDirectoryName(loc);
        }
    }
}
