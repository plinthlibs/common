#if !NET9_0_OR_GREATER
using Convert = Plinth.Common.Utils.BackPort.PlinthConvert;
#endif

namespace Plinth.Common.Utils;

/// <summary>
/// Utility class for converting bytes to hex strings and vice versa
/// </summary>
public static class HexUtil
{
#if NET9_0_OR_GREATER
    /// <summary>This method is no longer necessary, use <see cref="Convert.ToHexStringLower(byte[])"/></summary>
    [Obsolete("This method is no longer necessary, use Convert.ToHexStringLower() 2024-09-12")]
#else
    /// <summary>
    /// Convert an array of bytes to a hex string (lower case).
    /// </summary>
    /// <example>[0x12, 0xab, 0x4f] becomes "12ab4f"</example>
    /// <param name="bytes">0 or more bytes</param>
    /// <returns>lower case hex string</returns>
#endif
    public static string ToHex(byte[] bytes)
        => Convert.ToHexStringLower(bytes);

#if NET9_0_OR_GREATER
    /// <summary>This method is no longer necessary, use <see cref="Convert.ToHexStringLower(byte[], int, int)"/></summary>
    [Obsolete("This method is no longer necessary, use Convert.ToHexStringLower() 2024-09-12")]
#else
    /// <summary>
    /// Convert an array of bytes to a hex string (lower case).
    /// </summary>
    /// <example>[0x12, 0xab, 0x4f] becomes "12ab4f"</example>
    /// <param name="bytes">0 or more bytes</param>
    /// <param name="offset">offset in bytes to start conversion (0..N-1)</param>
    /// <param name="count">how many bytes to convert (0..N)</param>
    /// <returns>lower case hex string</returns>
#endif
    public static string ToHex(byte[] bytes, int offset, int count)
        => Convert.ToHexStringLower(bytes, offset, count);


#if NET9_0_OR_GREATER
    /// <summary>This method is no longer necessary, use <see cref="Convert.FromHexString(string)"/></summary>
    [Obsolete("This method is no longer necessary, use Convert.FromHexString() 2024-09-12")]
#else
    /// <summary>
    /// Convert a hex string into an array of bytes
    /// </summary>
    /// <param name="hex">a string containing hex digits [0-9A-Fa-f]</param>
    /// <returns></returns>
#endif
    public static byte[] ToBytes(string hex)
        => Convert.FromHexString(hex);
}
