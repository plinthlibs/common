#if !NET9_0_OR_GREATER

// this is a sort-of backport to allow the rest of the code to use Convert.ToHexStringLower() instead of a million #IFs
// For pre net9.0
// not for general use

namespace Plinth.Common.Utils.BackPort;

/// <summary>
/// 
/// </summary>
public static class PlinthConvert
{
    /// <inheritdoc cref="Convert.ToBase64String(byte[])" />
    public static string ToBase64String(byte[] inArray) => Convert.ToBase64String(inArray);

    /// <summary>
    /// Converts an array of 8-bit unsigned integers to its equivalent string representation that is encoded with lowercase hex characters.
    /// </summary>
    /// <param name="inArray">An array of 8-bit unsigned integers.</param>
    /// <returns>The string representation in hex of the elements in <paramref name="inArray"/>.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="inArray"/> is <code>null</code>.</exception>
    /// <exception cref="ArgumentOutOfRangeException"><paramref name="inArray"/> is too large to be encoded.</exception>
    public static string ToHexStringLower(byte[] inArray)
    {
        return Convert.ToHexString(inArray).ToLowerInvariant();
    }

    /// <summary>
    /// Converts a subset of an array of 8-bit unsigned integers to its equivalent string representation that is encoded with lowercase hex characters.
    /// Parameters specify the subset as an offset in the input array and the number of elements in the array to convert.
    /// </summary>
    /// <param name="inArray">An array of 8-bit unsigned integers.</param>
    /// <param name="offset">An offset in <paramref name="inArray"/>.</param>
    /// <param name="length">The number of elements of <paramref name="inArray"/> to convert.</param>
    /// <returns>The string representation in hex of <paramref name="length"/> elements of <paramref name="inArray"/>, starting at position <paramref name="offset"/>.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="inArray"/> is <code>null</code>.</exception>
    /// <exception cref="ArgumentOutOfRangeException"><paramref name="offset"/> or <paramref name="length"/> is negative.</exception>
    /// <exception cref="ArgumentOutOfRangeException"><paramref name="offset"/> plus <paramref name="length"/> is greater than the length of <paramref name="inArray"/>.</exception>
    /// <exception cref="ArgumentOutOfRangeException"><paramref name="inArray"/> is too large to be encoded.</exception>
    public static string ToHexStringLower(byte[] inArray, int offset, int length)
    {
        return Convert.ToHexString(inArray, offset, length).ToLowerInvariant();
    }

    /// <summary>
    /// Converts the specified string, which encodes binary data as hex characters, to an equivalent 8-bit unsigned integer array.
    /// </summary>
    /// <param name="s">The string to convert.</param>
    /// <returns>An array of 8-bit unsigned integers that is equivalent to <paramref name="s"/>.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="s"/> is <code>null</code>.</exception>
    /// <exception cref="FormatException">The length of <paramref name="s"/>, is not zero or a multiple of 2.</exception>
    /// <exception cref="FormatException">The format of <paramref name="s"/> is invalid. <paramref name="s"/> contains a non-hex character.</exception>
    public static byte[] FromHexString(string s)
    {
        return Convert.FromHexString(s);
    }
}
#endif
