using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;

namespace Plinth.Common.Logging;

/// <inheritdoc/>
public class SensitiveDataMasker : ISensitiveDataMasker
{
    private readonly List<(Regex regex, string replacementString)> _replacementInstructions;

    /// <summary>
    /// Creates a new instance of a <see cref="SensitiveDataMasker"/>.
    /// </summary>
    /// <param name="settings"></param>
    /// <exception cref="ArgumentNullException"></exception>
    public SensitiveDataMasker(SensitiveDataMaskerSettings settings)
    {
        _replacementInstructions = BuildReplacementInstructions(settings);
    }

    /// <inheritdoc/>
    [return: NotNullIfNotNull(nameof(s))]
    public string? MaskSensitiveFields(string? s)
    {
        if (string.IsNullOrWhiteSpace(s))
            return s;

        var result = s;

        foreach (var replacementInstruction in _replacementInstructions)
        {
            result = replacementInstruction.regex.Replace(result, replacementInstruction.replacementString);
        }

        return result;
    }

    private static List<(Regex regex, string replacementString)> BuildReplacementInstructions(SensitiveDataMaskerSettings settings)
    {
        var jsonReplacement = "$1" + settings.RedactedMark + "\"";
        var xmlReplacement = "<$1>" + settings.RedactedMark + "</$1>";
        var queryReplacement = "$1" + settings.RedactedMark;

        var replacementInstructions = new List<(Regex regex, string replacementString)>();

        foreach (var field in settings.FieldsToRedact)
        {
            var jsonRegex = string.Format("(\"{0}\"\\s*:\\s*\")({1})\"", field.keyRegex ?? ".*?", field.valueRegex ?? ".*?");
            replacementInstructions.Add((new Regex(jsonRegex, RegexOptions.Compiled | RegexOptions.IgnoreCase), jsonReplacement));

            var xmlRegex = string.Format("<({0})>({1})</\\1>", field.keyRegex ?? ".*?", field.valueRegex ?? "[^<]*");
            replacementInstructions.Add((new Regex(xmlRegex, RegexOptions.Compiled | RegexOptions.IgnoreCase), xmlReplacement));

            var queryRegex = string.Format("([?&]{0}=)({1})", field.keyRegex ?? ".*?", field.valueRegex ?? "[^&]*");
            replacementInstructions.Add((new Regex(queryRegex, RegexOptions.Compiled | RegexOptions.IgnoreCase), queryReplacement));
        }

        foreach (var customRegex in settings.CustomRegexes)
        {
            replacementInstructions.Add((new Regex(customRegex, RegexOptions.Compiled | RegexOptions.IgnoreCase), settings.RedactedMark));
        }

        return replacementInstructions;
    }
}
