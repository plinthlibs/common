namespace Plinth.Common.Logging;

/// <summary>
/// Settings for configuring the <see cref="ISensitiveDataMasker"/>.
/// </summary>
public class SensitiveDataMaskerSettings
{
    /// <summary>
    /// The replacement value for a redacted field.
    ///
    /// Note: the insertion text for redacting should be compatible with all supported document
    /// types (e.g. JSON, XML, query string)
    /// </summary>
    public string RedactedMark = "_REDACTED_";

    /// <summary>
    /// The key and value regular expressions to use to redact fields from json, xml, and queryParams
    /// </summary>
    public List<(string? keyRegex, string? valueRegex)> FieldsToRedact { get; set; } = new List<(string? keyRegex, string? valueRegex)>();

    /// <summary>
    /// Custom regular expressions for finding sensitive data in non-field-key scenarios.
    /// </summary>
    public List<string> CustomRegexes { get; set; } = new List<string>();
}
