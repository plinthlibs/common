using System.Diagnostics.CodeAnalysis;

namespace Plinth.Common.Logging;

/// <summary>
/// Utility to help protect sensitive data from appearing in logs
/// </summary>
public interface ISensitiveDataMasker
{
    /// <summary>
    /// Masks json and xml versions of configured sensitive fields (ssns, passwords, credit card numbers, etc.)
    /// </summary>
    /// <param name="s"></param>
    /// <returns>string with masked sensitive field values</returns>
    [return: NotNullIfNotNull(nameof(s))]
    string? MaskSensitiveFields(string? s);
}
