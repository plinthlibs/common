namespace Plinth.Common.Logging;

/// <summary>
/// Create and maintain an identifier for a specific request chain within a context of a request trace id
/// To be used to trace request chains from top level services all the way down to lower level services
/// </summary>
public class RequestTraceChain
{
    private sealed class ChainData
    {
        public string? CurrentChain;
        public int NextLinkId;
    }

    /// <summary>
    /// Shared instance for all request chains on a machine
    /// </summary>
    public static RequestTraceChain Instance { get; } = new RequestTraceChain();

    /// <summary>
    /// Header name for sending the trace chain
    /// </summary>
    public const string RequestTraceChainHeader = "X-Plinth-ReqTraceChain";

    private readonly AsyncLocal<ChainData> _traceChain = new();

    /// <summary>
    /// Generate a new request trace chain to be used for next request
    /// </summary>
    /// <returns></returns>
    public string? GenerateNextChain()
    {
        if (_traceChain.Value == null)
            return null;

        var currentChain = _traceChain.Value.CurrentChain;
        var nextLinkId = Interlocked.Increment(ref _traceChain.Value.NextLinkId);
        if (string.IsNullOrEmpty(currentChain))
            return $"{nextLinkId}";

        return $"{currentChain}.{nextLinkId}";
    }

    /// <summary>
    /// Create a new request trace chain
    /// </summary>
    /// <returns></returns>
    public string Create()
    {
        Set("0");

        return Get()!;
    }

    /// <summary>
    /// Get the current request trace chain
    /// </summary>
    /// <returns></returns>
    public string? Get() => _traceChain.Value?.CurrentChain;

    /// <summary>
    /// Set the current request trace chain
    /// </summary>
    /// <param name="chain">current chain</param>
    public void Set(string chain)
    {
        _traceChain.Value = new ChainData()
        {
            CurrentChain = chain,
            NextLinkId = -1 // The first generated incremented value will start at 0
        };
    }
}
