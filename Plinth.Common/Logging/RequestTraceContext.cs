namespace Plinth.Common.Logging;

/// <summary>
/// Provides a context scope for all context properties and makes sure the properties are removed
/// when the context is disposed.
/// </summary>
public class RequestTraceContext : IDisposable
{
    private readonly HashSet<string> _propertiesToRemove = [];

    /// <summary>
    /// Constructor
    /// </summary>
    public RequestTraceContext()
    {
    }

    /// <summary>
    /// Constructor using Dictionary
    /// </summary>
    /// <param name="properties"></param>
    /// <param name="overrideIfSet"></param>
    public RequestTraceContext(Dictionary<string, string?> properties, bool overrideIfSet = true)
    {
        SetRequestTraceProperties(properties, overrideIfSet);
    }

    /// <summary>
    /// Constructor using generic object.
    /// Note: The properties object must have unique property names, with only string values
    /// </summary>
    /// <param name="properties"></param>
    /// <param name="overrideIfSet"></param>
    public RequestTraceContext(object properties, bool overrideIfSet = true)
        : this(ToPropertyDictionary(properties), overrideIfSet)
    {
    }

    /// <summary>
    /// Set a value for a request trace property, tracking it in the context
    /// </summary>
    /// <param name="name"></param>
    /// <param name="value"></param>
    /// <param name="overrideIfSet"></param>
    public void SetRequestTraceProperty(string name, string? value, bool overrideIfSet = true)
    {
        if (RequestTraceProperties.SetRequestTraceProperty(name, value, overrideIfSet))
            _propertiesToRemove.Add(name);
    }

    /// <summary>
    /// Set all request trace properties in the provided dictionary, tracking them in the context
    /// </summary>
    /// <param name="props"></param>
    /// <param name="overrideIfSet"></param>
    public void SetRequestTraceProperties(Dictionary<string, string?> props, bool overrideIfSet = true)
    {
        foreach (var kvp in props)
            SetRequestTraceProperty(kvp.Key, kvp.Value, overrideIfSet);
    }

    /// <summary>
    /// Remove our explicitly set properties once we're done
    /// </summary>
    public void Dispose()
    {
        foreach (var prop in _propertiesToRemove)
        {
            RequestTraceProperties.RemoveRequestTraceProperty(prop);
        }
        GC.SuppressFinalize(this);
    }

    private static Dictionary<string, string?> ToPropertyDictionary(object properties)
    {
        return properties.GetType()
            .GetProperties()
            .ToDictionary(pi => pi.Name, pi => (string?)pi.GetValue(properties, null));
    }
}
