namespace Plinth.Common.Logging;

/// <summary>
/// Stores request-specific properties to be logged alongside each request.
/// To be used to trace requests from top level services all the way down to lower level services
/// </summary>
public static class RequestTraceProperties
{
    /// <summary>
    /// Header name for sending request trace properties
    /// </summary>
    public const string RequestTracePropertiesHeader = "X-Plinth-ReqTraceProps";

    private const string _UserNamePropName = "UserName";
    private const string _SessionIdPropName = "SessionId";
    private const string _OriginatingIpAddressPropName = "OriginatingIpAddress";
    private const string _OriginatingApplicationPropName = "OriginatingApplication";

    private readonly static AsyncLocal<Dictionary<string, string>?> _rProps = new();

    /// <summary>
    /// Get the request trace user name
    /// </summary>
    /// <returns>a request trace user name</returns>
    public static string? GetRequestTraceUserName() =>
        GetRequestTraceProperty(_UserNamePropName);

    /// <summary>
    /// Set the request trace user name
    /// </summary>
    /// <param name="userName"></param>
    /// <param name="overrideIfSet"></param>
    public static bool SetRequestTraceUserName(string userName, bool overrideIfSet = true) =>
        SetRequestTraceProperty(_UserNamePropName, userName, overrideIfSet);

    /// <summary>
    /// Use GetRequestTraceUserName() instead
    /// </summary>
    [Obsolete("Use GetRequestTraceUserName() instead, 2023-08-29, 2024-05-15", error:true)]
    public static string? GetRequestTraceLendingUserName() =>
        throw new NotSupportedException();

    /// <summary>
    /// Use SetRequestTraceUserName() instead
    /// </summary>
    [Obsolete("Use SetRequestTraceUserName() instead, 2023-08-29, 2024-05-15", error: true)]
    public static bool SetRequestTraceLendingUserName(string lendingUserName, bool overrideIfSet = true) =>
        throw new NotSupportedException();

    /// <summary>
    /// Get the request trace session ID
    /// </summary>
    /// <returns>a request trace session ID</returns>
    public static string? GetRequestTraceSessionId() =>
        GetRequestTraceProperty(_SessionIdPropName);

    /// <summary>
    /// Set the request trace session ID
    /// </summary>
    /// <param name="sessionId"></param>
    /// <param name="overrideIfSet"></param>
    public static bool SetRequestTraceSessionId(string sessionId, bool overrideIfSet = true) =>
        SetRequestTraceProperty(_SessionIdPropName, sessionId, overrideIfSet);

    /// <summary>
    /// Get the request trace originating IP address
    /// </summary>
    /// <returns>a request trace IP address</returns>
    public static string? GetRequestTraceOriginatingIpAddress() =>
        GetRequestTraceProperty(_OriginatingIpAddressPropName);

    /// <summary>
    /// Set the request trace originating IP address
    /// </summary>
    /// <param name="ipAddress"></param>
    /// <param name="overrideIfSet"></param>
    public static bool SetRequestTraceOriginatingIpAddress(string ipAddress, bool overrideIfSet = true) =>
        SetRequestTraceProperty(_OriginatingIpAddressPropName, ipAddress, overrideIfSet);

    /// <summary>
    /// Get the request trace originating application
    /// </summary>
    /// <returns>a request trace application</returns>
    public static string? GetRequestTraceOriginatingApplication() =>
        GetRequestTraceProperty(_OriginatingApplicationPropName);

    /// <summary>
    /// Set the request trace originating application
    /// </summary>
    /// <param name="applicationName"></param>
    /// <param name="overrideIfSet"></param>
    public static bool SetRequestTraceOriginatingApplication(string applicationName, bool overrideIfSet = true) =>
        SetRequestTraceProperty(_OriginatingApplicationPropName, applicationName, overrideIfSet);

    /// <summary>
    /// Get a request trace property value
    /// </summary>
    /// <returns>a string value of the request trace property</returns>
    public static string? GetRequestTraceProperty(string name)
    {
        if (_rProps.Value == null
            || !_rProps.Value.TryGetValue(name, out string? value))
            return null;

        return value;
    }

    /// <summary>
    /// Get a dictionary of all request trace properties
    /// </summary>
    /// <returns>a dictionary of all request trace properties</returns>
    public static Dictionary<string, string>? GetRequestTraceProperties()
    {
        return _rProps.Value;
    }

    /// <summary>
    /// Set a value for a request trace property
    /// </summary>
    /// <param name="name"></param>
    /// <param name="value"></param>
    /// <param name="overrideIfSet"></param>
    public static bool SetRequestTraceProperty(string name, string? value, bool overrideIfSet = true)
    {
        if (string.IsNullOrEmpty(value))
            return false;

        _rProps.Value ??= [];

        if (!overrideIfSet
            && _rProps.Value.ContainsKey(name))
            return false;

        _rProps.Value[name] = value;

        return true;
    }

    /// <summary>
    /// Remove a value for a request trace property
    /// </summary>
    /// <param name="name"></param>
    public static bool RemoveRequestTraceProperty(string name)
    {
        if (_rProps.Value == null)
            return false;

        return _rProps.Value.Remove(name);
    }

    /// <summary>
    /// Set all request trace properties in the provided dictionary
    /// </summary>
    /// <param name="props"></param>
    public static void SetRequestTraceProperties(IDictionary<string, string>? props)
    {
        if (props.IsNullOrEmpty())
            ClearRequestTraceProperties();
        else
            _rProps.Value = new Dictionary<string, string>(props!);
    }

    /// <summary>
    /// Clear all request trace properties
    /// </summary>
    public static void ClearRequestTraceProperties()
    {
        _rProps.Value = null;
    }
}
