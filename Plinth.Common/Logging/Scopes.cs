﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plinth.Common.Logging;

/// <summary>
/// Shortcuts for creating scopes
/// </summary>
/// <remarks>
/// Using an array of KeyValuePair is faster and allocates less than a Dictionary
/// </remarks>
/// <example>
/// <code>
/// using static Plinth.Common.Logging.Scopes;
/// 
/// var scope = CreateScope(
///     Kvp("Key1", value1),
///     Kvp("Key2", value2)
/// );
/// 
/// using (var log.BeginScope(scope))
/// {
///     
/// }
/// </code>
/// </example>
public static class Scopes
{
    /// <summary>
    /// Used as a shortcut for creating scopes
    /// </summary>
    /// <param name="properties"></param>
    /// <returns></returns>
    public static KeyValuePair<string, object?>[] CreateScope(params KeyValuePair<string, object?>[] properties)
        => properties;

    /// <summary>
    /// Used as a shortcut for creating key value pairs for scopes
    /// </summary>
    public static KeyValuePair<string, object?> Kvp(string key, object? value) => KeyValuePair.Create(key, value);
}
