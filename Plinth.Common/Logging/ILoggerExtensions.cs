using Plinth.Common.Logging;

namespace /*Plinth.*/Microsoft.Extensions.Logging;

/// <summary>
/// Extensions to make logging to MS ILogger more like NLog
/// </summary>
public static class ILoggerExtensions
{
#pragma warning disable CA2254 // Template should be a static expression
    #region Trace
    /// <summary>Trace log (msg, args)</summary>
    public static void Trace(this ILogger logger, string? message, params object?[] args) => logger.LogTrace(message, args);
    /// <summary>Trace log (exception, msg, args)</summary>
    public static void Trace(this ILogger logger, Exception exception, string? message, params object?[] args) => logger.LogTrace(exception, message, args);
    /// <summary>Trace log (event, msg, args)</summary>
    public static void Trace(this ILogger logger, EventId eventId, string? message, params object?[] args) => logger.LogTrace(eventId, message, args);
    /// <summary>Trace log (event, exception, msg, args)</summary>
    public static void Trace(this ILogger logger, EventId eventId, Exception exception, string? message, params object?[] args) => logger.LogTrace(eventId, exception, message, args);

    /// <summary>alias for .IsEnabled(LogLevel.Trace)</summary>
    public static bool IsTraceEnabled(this ILogger logger) => logger.IsEnabled(LogLevel.Trace);

    /// <summary>don't use</summary> 
    [Obsolete("the Exception should be passed before the message", error:true)]
    public static void Trace(this ILogger logger, string? message, Exception e, params object?[] args) => throw new NotSupportedException();
    #endregion

    #region Debug
    /// <summary>Debug log (msg, args)</summary>
    public static void Debug(this ILogger logger, string? message, params object?[] args) => logger.LogDebug(message, args);
    /// <summary>Debug log (exception, msg, args)</summary>
    public static void Debug(this ILogger logger, Exception exception, string? message, params object?[] args) => logger.LogDebug(exception, message, args);
    /// <summary>Debug log (event, msg, args)</summary>
    public static void Debug(this ILogger logger, EventId eventId, string? message, params object?[] args) => logger.LogDebug(eventId, message, args);
    /// <summary>Debug log (event, exception, msg, args)</summary>
    public static void Debug(this ILogger logger, EventId eventId, Exception exception, string? message, params object?[] args) => logger.LogDebug(eventId, exception, message, args);

    /// <summary>alias for .IsEnabled(LogLevel.Debug)</summary>
    public static bool IsDebugEnabled(this ILogger logger) => logger.IsEnabled(LogLevel.Debug);

    /// <summary>don't use</summary>
    [Obsolete("the Exception should be passed before the message", error:true)]
    public static void Debug(this ILogger logger, string? message, Exception e, params object?[] args) => throw new NotSupportedException();
    #endregion

    #region Info/Information
    /// <summary>Info log (msg, args)</summary>
    public static void Info(this ILogger logger, string? message, params object?[] args) => logger.LogInformation(message, args);
    /// <summary>Info log (exception, msg, args)</summary>
    public static void Info(this ILogger logger, Exception exception, string? message, params object?[] args) => logger.LogInformation(exception, message, args);
    /// <summary>Info log (event, msg, args)</summary>
    public static void Info(this ILogger logger, EventId eventId, string? message, params object?[] args) => logger.LogInformation(eventId, message, args);
    /// <summary>Info log (event, exception, msg, args)</summary>
    public static void Info(this ILogger logger, EventId eventId, Exception exception, string? message, params object?[] args) => logger.LogInformation(eventId, exception, message, args);

    /// <summary>alias for .IsEnabled(LogLevel.Information)</summary>
    public static bool IsInfoEnabled(this ILogger logger) => logger.IsEnabled(LogLevel.Information);

    /// <summary>don't use</summary> 
    [Obsolete("the Exception should be passed before the message", error:true)]
    public static void Info(this ILogger logger, string? message, Exception e, params object?[] args) => throw new NotSupportedException();

    /// <summary>Information log (msg, args)</summary>
    public static void Information(this ILogger logger, string? message, params object?[] args) => logger.LogInformation(message, args);
    /// <summary>Information log (exception, msg, args)</summary>
    public static void Information(this ILogger logger, Exception exception, string? message, params object?[] args) => logger.LogInformation(exception, message, args);
    /// <summary>Information log (event, msg, args)</summary>
    public static void Information(this ILogger logger, EventId eventId, string? message, params object?[] args) => logger.LogInformation(eventId, message, args);
    /// <summary>Information log (event, exception, msg, args)</summary>
    public static void Information(this ILogger logger, EventId eventId, Exception exception, string? message, params object?[] args) => logger.LogInformation(eventId, exception, message, args);

    /// <summary>alias for .IsEnabled(LogLevel.Information)</summary>
    public static bool IsInformationEnabled(this ILogger logger) => logger.IsEnabled(LogLevel.Information);

    /// <summary>don't use</summary> 
    [Obsolete("the Exception should be passed before the message", error:true)]
    public static void Information(this ILogger logger, string? message, Exception e, params object?[] args) => throw new NotSupportedException();
    #endregion

    #region Warn / Warning
    /// <summary>Warn log (msg, args)</summary>
    public static void Warn(this ILogger logger, string? message, params object?[] args) => logger.LogWarning(message, args);
    /// <summary>Warn log (exception, msg, args)</summary>
    public static void Warn(this ILogger logger, Exception exception, string? message, params object?[] args) => logger.LogWarning(exception, message, args);
    /// <summary>Warn log (event, msg, args)</summary>
    public static void Warn(this ILogger logger, EventId eventId, string? message, params object?[] args) => logger.LogWarning(eventId, message, args);
    /// <summary>Warn log (event, exception, msg, args)</summary>
    public static void Warn(this ILogger logger, EventId eventId, Exception exception, string? message, params object?[] args) => logger.LogWarning(eventId, exception, message, args);

    /// <summary>alias for .IsEnabled(LogLevel.Warning)</summary>
    public static bool IsWarnEnabled(this ILogger logger) => logger.IsEnabled(LogLevel.Warning);

    /// <summary>don't use</summary> 
    [Obsolete("the Exception should be passed before the message", error:true)]
    public static void Warn(this ILogger logger, string? message, Exception e, params object?[] args) => throw new NotSupportedException();

    /// <summary>Warning log (msg, args)</summary>
    public static void Warning(this ILogger logger, string? message, params object?[] args) => logger.LogWarning(message, args);
    /// <summary>Warning log (exception, msg, args)</summary>
    public static void Warning(this ILogger logger, Exception exception, string? message, params object?[] args) => logger.LogWarning(exception, message, args);
    /// <summary>Warning log (event, msg, args)</summary>
    public static void Warning(this ILogger logger, EventId eventId, string? message, params object?[] args) => logger.LogWarning(eventId, message, args);
    /// <summary>Warning log (event, exception, msg, args)</summary>
    public static void Warning(this ILogger logger, EventId eventId, Exception exception, string? message, params object?[] args) => logger.LogWarning(eventId, exception, message, args);

    /// <summary>alias for .IsEnabled(LogLevel.Warning)</summary>
    public static bool IsWarningEnabled(this ILogger logger) => logger.IsEnabled(LogLevel.Warning);

    /// <summary>don't use</summary> 
    [Obsolete("the Exception should be passed before the message", error:true)]
    public static void Warning(this ILogger logger, string? message, Exception e, params object?[] args) => throw new NotSupportedException();
    #endregion

    #region Error
    /// <summary>Error log (msg, args)</summary>
    public static void Error(this ILogger logger, string? message, params object?[] args) => logger.LogError(message, args);
    /// <summary>Error log (exception, msg, args)</summary>
    public static void Error(this ILogger logger, Exception exception, string? message, params object?[] args) => logger.LogError(exception, message, args);
    /// <summary>Error log (event, msg, args)</summary>
    public static void Error(this ILogger logger, EventId eventId, string? message, params object?[] args) => logger.LogError(eventId, message, args);
    /// <summary>Error log (event, exception, msg, args)</summary>
    public static void Error(this ILogger logger, EventId eventId, Exception exception, string? message, params object?[] args) => logger.LogError(eventId, exception, message, args);

    /// <summary>alias for .IsEnabled(LogLevel.Error)</summary>
    public static bool IsErrorEnabled(this ILogger logger) => logger.IsEnabled(LogLevel.Error);

    /// <summary>don't use</summary>
    [Obsolete("the Exception should be passed before the message", error:true)]
    public static void Error(this ILogger logger, string? message, Exception e, params object?[] args) => throw new NotSupportedException();
    #endregion

    #region Fatal/Critical
    /// <summary>Fatal log (msg, args)</summary>
    public static void Fatal(this ILogger logger, string? message, params object?[] args) => logger.LogCritical(message, args);
    /// <summary>Fatal log (exception, msg, args)</summary>
    public static void Fatal(this ILogger logger, Exception exception, string? message, params object?[] args) => logger.LogCritical(exception, message, args);
    /// <summary>Fatal log (event, msg, args)</summary>
    public static void Fatal(this ILogger logger, EventId eventId, string? message, params object?[] args) => logger.LogCritical(eventId, message, args);
    /// <summary>Fatal log (event, exception, msg, args)</summary>
    public static void Fatal(this ILogger logger, EventId eventId, Exception exception, string? message, params object?[] args) => logger.LogCritical(eventId, exception, message, args);

    /// <summary>don't use</summary>
    [Obsolete("the Exception should be passed before the message", error:true)]
    public static void Fatal(this ILogger logger, string? message, Exception e, params object?[] args) => throw new NotSupportedException();

    /// <summary>alias for .IsEnabled(LogLevel.Critical)</summary>
    public static bool IsFatalEnabled(this ILogger logger) => logger.IsEnabled(LogLevel.Critical);

    /// <summary>Fatal log (msg, args)</summary>
    public static void Critical(this ILogger logger, string? message, params object?[] args) => logger.LogCritical(message, args);
    /// <summary>Critical log (exception, msg, args)</summary>
    public static void Critical(this ILogger logger, Exception exception, string? message, params object?[] args) => logger.LogCritical(exception, message, args);
    /// <summary>Critical log (event, msg, args)</summary>
    public static void Critical(this ILogger logger, EventId eventId, string? message, params object?[] args) => logger.LogCritical(eventId, message, args);
    /// <summary>Critical log (event, exception, msg, args)</summary>
    public static void Critical(this ILogger logger, EventId eventId, Exception exception, string? message, params object?[] args) => logger.LogCritical(eventId, exception, message, args);

    /// <summary>don't use</summary>
    [Obsolete("the Exception should be passed before the message", error:true)]
    public static void Critical(this ILogger logger, string? message, Exception e, params object?[] args) => throw new NotSupportedException();

    /// <summary>alias for .IsEnabled(LogLevel.Critical)</summary>
    public static bool IsCriticalEnabled(this ILogger logger) => logger.IsEnabled(LogLevel.Critical);
    #endregion

#pragma warning restore CA2254 // Template should be a static expression
}
