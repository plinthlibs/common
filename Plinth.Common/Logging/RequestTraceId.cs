﻿namespace Plinth.Common.Logging;

/// <summary>
/// Generates and stores a unique trace id for a request
/// To be used to trace requests from top level services all the way down to lower level services
/// </summary>
public class RequestTraceId
{
    /// <summary>
    /// Shared instance for all requests on a machine
    /// </summary>
    public static RequestTraceId Instance { get; } = new();

    /// <summary>
    /// Header name for sending the trace id
    /// </summary>
    public const string RequestTraceIdHeader = "X-Plinth-ReqTraceID";

    private readonly AsyncLocal<string> _traceId = new();

    /// <summary>
    /// Create a new request trace id
    /// </summary>
    /// <returns></returns>
    public string Create()
    {
        Set(Guid.NewGuid().ToString("N"));

        return _traceId.Value!;
    }

    /// <summary>
    /// Get the current request id
    /// </summary>
    /// <returns></returns>
    public string? Get() => _traceId.Value;

    /// <summary>
    /// Set the current request trace id
    /// </summary>
    /// <param name="id">new id</param>
    public void Set(string id) => _traceId.Value = id;
}
