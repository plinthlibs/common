using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using Plinth.Common.Logging;

/*
 * This is in the Microsoft.Extensions.Logging namespace to avoid needing to import this
 * namespace to only get this single class, since you must import Microsoft.Extensions.Logging to use ILogger
*/

namespace /*Plinth.*/Microsoft.Extensions.Logging;

/// <summary>
/// Global access to loggers
/// </summary>
public static class StaticLogManager
{
    private static readonly AsyncLocal<string?> _ctxId = new();

    internal static readonly ILogger CachedNoopLogger = new NoopLogger();
    internal static ILoggerFactory StaticLoggerFactory = new NoopLoggerFactory();
    internal static ILoggerProvider? StaticLoggerMainProvider = null;
    internal static string StaticLoggerFactoryId = "NOOP";

    internal static void SetLoggerFactory(ILoggerFactory fac, string facId)
    {
        StaticLoggerFactory = fac;
        StaticLoggerFactoryId = facId;
    }

    private static readonly Type? _mainProgramType = Assembly.GetEntryAssembly()?.EntryPoint?.DeclaringType;

    /// <summary>
    /// Get the logger for the current class.
    /// It must be declared as private static readonly at the top of the class
    /// </summary>
    [MethodImpl(MethodImplOptions.NoInlining)] // this is important for the automatic class detection
    public static ILogger GetLogger()
    {
        var frame = new StackFrame(1, false);
        var method = frame.GetMethod();

        if (method == null)
            return StaticLoggerFactory.CreateLogger("Unknown");

        MethodAttributes required =
            MethodAttributes.Private |
            MethodAttributes.PrivateScope |
            MethodAttributes.Static |
            MethodAttributes.SpecialName;

        if (!method.Attributes.HasFlag(required))
        {
            throw new ArgumentException("log variable must be 'private static readonly' declared at the top of the class");
        }

        var declaringType = method.DeclaringType ?? throw new InvalidOperationException("cannot determine declaring type of caller");

        if (declaringType == _mainProgramType)
        {
            throw new ArgumentException("log variable should be initialized using StaticLogManagerSetup in main program class");
        }

        return StaticLoggerFactory.CreateLogger(declaringType);
    }

    /// <summary>
    /// Set the context ID
    /// </summary>
    /// <param name="contextId">a long, but only the bottom 20 bits are used</param>
    public static void SetContextId(long contextId)
    {
        _ctxId.Value = (contextId & 0x0FFFFF).ToString("X5");
    }

    /// <summary>
    /// Get the context ID
    /// </summary>
    /// <returns>a 5 digit hex number for this async context</returns>
    public static string? GetContextId()
    {
        return _ctxId.Value;
    }

    /// <summary>
    /// Get the current trace information into an object suitable for restoring later
    /// </summary>
    /// <remarks>This is useful when spawning a new thread or Task.Run() where the async context is lost</remarks>
    public static TraceInfo GetTraceInfo()
    {
        return new TraceInfo
        {
            ContextId = GetContextId(),
            RequestTraceId = RequestTraceId.Instance.Get(),
            RequestTraceChain = RequestTraceChain.Instance.Get(),
            RequestTraceProperties = RequestTraceProperties.GetRequestTraceProperties()
        };
    }

    /// <summary>
    /// Load trace info into the current context
    /// </summary>
    /// <param name="traceInfo">from StoreTraceInfo()</param>
    /// <remarks>This is useful when spawning a new thread or Task.Run() where the async context is lost</remarks>
    public static void LoadTraceInfo(TraceInfo traceInfo)
    {
        _ctxId.Value = traceInfo.ContextId;
        if (traceInfo.RequestTraceId != null)
            RequestTraceId.Instance.Set(traceInfo.RequestTraceId);
        if (traceInfo.RequestTraceChain != null)
            RequestTraceChain.Instance.Set(traceInfo.RequestTraceChain);
        RequestTraceProperties.SetRequestTraceProperties(traceInfo.RequestTraceProperties);
    }

    /// <summary>
    /// Object containing trace info
    /// </summary>
    public class TraceInfo
    {
        internal string? ContextId;
        internal string? RequestTraceId;
        internal string? RequestTraceChain;
        internal IDictionary<string,string>? RequestTraceProperties;
    }

    internal class NoopLoggerFactory : ILoggerFactory
    {
        public void AddProvider(ILoggerProvider provider) { }

        public ILogger CreateLogger(string categoryName)
            => CachedNoopLogger;

        public void Dispose() { }
    }

    internal class NoopLogger : ILogger
    {
        private class NoopIDisposable : IDisposable
        {
            public void Dispose() { }
        }

        public IDisposable? BeginScope<TState>(TState state) where TState : notnull
            => new NoopIDisposable();

        public bool IsEnabled(LogLevel logLevel)
            => true;

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception? exception, Func<TState, Exception?, string> formatter)
        {
        }
    }
}
