namespace Plinth.Common.Logging;

/// <summary>
/// Constants for logging
/// </summary>
public static class LoggingConstants
{
    /// <summary>
    /// Type of message, used as a hint for what properties may be available for the log event
    /// </summary>
    public const string Field_MessageType = "MessageType";

    /// <summary>
    /// Name of operation being timed
    /// </summary>
    public const string Field_TimeOperation = "TimeOperation";

    /// <summary>
    /// How long the timed operation took in TimeSpan format [days.]hh.mm.ss.fffffff
    /// </summary>
    public const string Field_TimeDuration = "TimeDuration";

    /// <summary>
    /// Type of message, used as a result indicator for an operation
    /// </summary>
    public const string Field_OperationResult = "OperationResult";

    /// <summary>
    /// How long the timed operation took in microseconds
    /// </summary>
    public const string Field_TimeDurationMicros = "TimeMicros";

    /// <summary>
    /// Special optional property which can be used to log context ID value in case
    /// it is missing in the current async context
    /// </summary>
    internal const string Field_ContextId = "__plinth_ContextId";

    /// <summary>
    /// Special optional property which can be used to log request trace ID value in case
    /// it is missing in the current async context
    /// </summary>
    internal const string Field_RequestTraceId = "__plinth_RequestTraceId";

    /// <summary>
    /// Special optional property which can be used to log request trace chain value in case
    /// it is missing in the current async context
    /// </summary>
    internal const string Field_RequestTraceChain = "__plinth_RequestTraceChain";
}
