/*
 * This is in the System.Collections.Generic namespace to avoid needing to import this
 * namespace to only get this single class
*/

namespace System.Collections.Generic;

/// <summary>
/// Extension of the <see cref="HashSet{T}"/> class to be able to create and specify
/// case insensitive string hash sets more explicitly.
/// </summary>
/// <typeparam name="T">must be string</typeparam>
public class CaseInsensitiveHashSet<T> : HashSet<string>
{
    /// <summary>
    /// Constructor, instantiating the instance as a case insensitive dictionary
    /// </summary>
    public CaseInsensitiveHashSet() : base(StringComparer.OrdinalIgnoreCase)
    {
        CheckType(typeof(T));
    }

    /// <summary>
    /// Constructor, instantiating the instance as a case insensitive dictionary with the data from the given IEnumerable
    /// </summary>
    public CaseInsensitiveHashSet(IEnumerable<string> enumerable) : base(enumerable, StringComparer.OrdinalIgnoreCase)
    {
        CheckType(typeof(T));
    }

    private static void CheckType(Type t)
    {
        // only string values make sense for case insensitivity
        if (t != typeof(string))
            throw new NotSupportedException("T must be string");
    }
}

/// <summary>
/// Extensions for CaseInsensitiveHashSet
/// </summary>
public static class CaseInsensitiveHashSetExt
{
    /// <summary>
    /// Convert an IEnumerable to a Case Insensitive HashSet
    /// </summary>
    /// <param name="ienum">enumerable</param>
    /// <returns>CaseInsensitiveHashSet</returns>
    public static CaseInsensitiveHashSet<string> ToCaseInsensitiveHashSet(this IEnumerable<string> ienum)
        => ToCaseInsensitiveHashSet(ienum, s => s);

    /// <summary>
    /// Convert an IEnumerable to a Case Insensitive HashSet
    /// </summary>
    /// <typeparam name="T">type of object in enumerable</typeparam>
    /// <param name="ienum">enumerable</param>
    /// <param name="valSel">function to convert items to hashset values (string)</param>
    /// <returns>CaseInsensitiveHashSet</returns>
    public static CaseInsensitiveHashSet<string> ToCaseInsensitiveHashSet<T>(this IEnumerable<T> ienum,
        Func<T, string> valSel)
    {
        ArgumentNullException.ThrowIfNull(ienum);
        ArgumentNullException.ThrowIfNull(valSel);

        var set = new CaseInsensitiveHashSet<string>();
        foreach (var item in ienum)
            set.Add(valSel(item));
        return set;
    }
}
