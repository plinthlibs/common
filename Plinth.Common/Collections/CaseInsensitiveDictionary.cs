/*
 * This is in the System.Collections.Generic namespace to avoid needing to import this
 * namespace to only get this single class
*/

namespace System.Collections.Generic;

/// <summary>
/// Extension of the <see cref="Dictionary{TKey, TValue}"/> class to be able to create and specify
/// case insensitive string dictionaries more explicitly.
/// </summary>
/// <typeparam name="TKey">must be string</typeparam>
/// <typeparam name="TValue"></typeparam>
public class CaseInsensitiveDictionary<TKey, TValue> : Dictionary<string, TValue>
{
    /// <summary>
    /// Constructor, instantiating the instance as a case insensitive dictionary
    /// </summary>
    public CaseInsensitiveDictionary() : base(StringComparer.OrdinalIgnoreCase)
    {
        CheckType(typeof(TKey));
    }

    /// <summary>
    /// Constructor, instantiating the instance as a case insensitive dictionary with the data from the given dictionary
    /// </summary>
    public CaseInsensitiveDictionary(IDictionary<string, TValue> dict) : base(dict, StringComparer.OrdinalIgnoreCase)
    {
        CheckType(typeof(TKey));
    }

    /// <summary>
    /// Constructor, instantiating the instance as a case insensitive dictionary with a given capacity
    /// </summary>
    public CaseInsensitiveDictionary(int capacity) : base(capacity, StringComparer.OrdinalIgnoreCase)
    {
        CheckType(typeof(TKey));
    }

    private static void CheckType(Type t)
    {
        // only string values make sense for case insensitivity
        if (t != typeof(string))
            throw new NotSupportedException("TKey must be string");
    }
}

/// <summary>
/// Extensions for CaseInsensitiveDictionary
/// </summary>
public static class CaseInsensitiveDictionaryExt
{
    /// <summary>
    /// Convert an IEnumerable to a Case Insensitive Dictionary
    /// </summary>
    /// <typeparam name="T">type of object in enumerable</typeparam>
    /// <param name="ienum">enumerable</param>
    /// <param name="keySel">function to convert items to keys (string)</param>
    /// <returns>CaseInsensitiveDictionary</returns>
    public static CaseInsensitiveDictionary<string, T> ToCaseInsensitiveDictionary<T>(this IEnumerable<T> ienum, Func<T, string> keySel)
        => ToCaseInsensitiveDictionary(ienum, keySel, t => t);

    /// <summary>
    /// Convert an IEnumerable to a Case Insensitive Dictionary
    /// </summary>
    /// <typeparam name="T">type of object in enumerable</typeparam>
    /// <typeparam name="TValue">type of dictionary values</typeparam>
    /// <param name="ienum">enumerable</param>
    /// <param name="keySel">function to convert items to keys (string)</param>
    /// <param name="valSel">function to convert items to values</param>
    /// <returns>CaseInsensitiveDictionary</returns>
    public static CaseInsensitiveDictionary<string, TValue> ToCaseInsensitiveDictionary<T, TValue>(this IEnumerable<T> ienum,
        Func<T, string> keySel, Func<T, TValue> valSel)
    {
        ArgumentNullException.ThrowIfNull(ienum);
        ArgumentNullException.ThrowIfNull(keySel);
        ArgumentNullException.ThrowIfNull(valSel);

        var dict = new CaseInsensitiveDictionary<string, TValue>();
        foreach (var item in ienum)
            dict[keySel(item)] = valSel(item);
        return dict;
    }
}
