/*
 * This is in the System.Collections.Generic namespace to avoid needing to import this
 * namespace to only get this single class
*/

namespace System.Collections.Generic;

/// <summary>
/// A Dictionary where the value of new keys can be defaulted via value or function
/// </summary>
/// <typeparam name="TKey"></typeparam>
/// <typeparam name="TValue"></typeparam>
/// <example>
/// Example 1: Counting
/// <code>
/// var dict = new DefaultDictionary&lt;string, int&gt;(0);
/// dict["a"]++; // dict["a"] now is 1
/// </code>
/// Example 2: Easy Multi Maps
/// <code>
/// var dict = new DefaultDictionary&lt;string, ISet&lt;string&gt;&gt;(() =&gt; new HashSet&lt;string&gt;() );
/// dict["a"].Add("b"); // "a" has a set with ("b") in it
/// </code>
/// </example>
public class DefaultDictionary<TKey, TValue> : IDictionary<TKey, TValue> where TKey: notnull
{
    private readonly IDictionary<TKey, TValue> _dictionary;
    private readonly Func<TValue> _defaultValueFunc;

    /// <summary>
    /// Create a DefaultDictionary with a fixed default value
    /// </summary>
    /// <param name="defaultValue">fixed default value</param>
    /// <remarks><paramref name="defaultValue"/> is strongly recommended to be immutable or a struct/primitive
    /// because if <paramref name="defaultValue"/> is a reference, all default set values will share the same object</remarks>
    public DefaultDictionary(TValue defaultValue) : this( () => defaultValue )
    {
    }

    /// <summary>
    /// Create a DefaultDictionary with the default value from a function
    /// </summary>
    /// <param name="defaultValueFunc">function to supply the default value</param>
    public DefaultDictionary(Func<TValue> defaultValueFunc) : this(null!, defaultValueFunc)
    {
    }

    /// <summary>
    /// Create a DefaultDictionary from another dictionary with the default value from a function
    /// </summary>
    /// <param name="dictionary">this dictionary will be wrapped</param>
    /// <param name="defaultValueFunc">function to supply the default value</param>
    /// <remarks>This wraps <paramref name="dictionary"/>, it does not copy it</remarks>
    public DefaultDictionary(IDictionary<TKey, TValue> dictionary, Func<TValue> defaultValueFunc)
    {
        _dictionary = dictionary ?? new Dictionary<TKey, TValue>();
        _defaultValueFunc = defaultValueFunc;
    }

    /// <summary>
    /// Create a DefaultDictionary from another dictionary with a fixed default value
    /// </summary>
    /// <param name="dictionary">this dictionary will be wrapped</param>
    /// <param name="defaultValue">fixed default value</param>
    /// <remarks><paramref name="defaultValue"/> is strongly recommended to be immutable or a struct/primitive
    /// because if <paramref name="defaultValue"/> is a reference, all default set values will share the same object</remarks>
    public DefaultDictionary(IDictionary<TKey, TValue> dictionary, TValue defaultValue) : this( dictionary, () => defaultValue )
    {
    }

    /// <inheritdoc cref="IDictionary.GetEnumerator()"/>
    public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => _dictionary.GetEnumerator();
    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

    /// <inheritdoc cref="IDictionary.Add(object, object?)"/>
    public void Add(KeyValuePair<TKey, TValue> item) => _dictionary.Add(item);
    /// <inheritdoc cref="IDictionary.Clear()"/>
    public void Clear() => _dictionary.Clear();
    /// <inheritdoc cref="IDictionary.Contains(object)"/>
    public bool Contains(KeyValuePair<TKey, TValue> item) => _dictionary.Contains(item);
    /// <inheritdoc cref="ICollection.CopyTo(Array, int)"/>
    public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) => _dictionary.CopyTo(array, arrayIndex);
    /// <inheritdoc cref="IDictionary.Remove(object)"/>
    public bool Remove(KeyValuePair<TKey, TValue> item) => _dictionary.Remove(item);
    /// <inheritdoc cref="ICollection{T}.Count"/>
    public int Count => _dictionary.Count;
    /// <inheritdoc cref="IDictionary.IsReadOnly"/>
    public bool IsReadOnly => _dictionary.IsReadOnly;
    /// <inheritdoc cref="IDictionary{TKey, TValue}.ContainsKey(TKey)"/>
    public bool ContainsKey(TKey key) => _dictionary.ContainsKey(key);
    /// <inheritdoc cref="IDictionary{TKey, TValue}.Add(TKey, TValue)"/>
    public void Add(TKey key, TValue value) => _dictionary.Add(key, value);
    /// <inheritdoc cref="IDictionary{TKey, TValue}.Remove(TKey)"/>
    public bool Remove(TKey key) => _dictionary.Remove(key);

    /// <summary>
    /// Gets the value associated with the specified key, will put a default value in for missing key
    /// </summary>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <remarks>this will always return true because a value will either be found or added</remarks>
    /// <returns>always true</returns>
    public bool TryGetValue(TKey key, out TValue value)
    {
        if (!_dictionary.TryGetValue(key, out var found))
        {
            found = _dictionary[key] = _defaultValueFunc();
        }
        value = found;

        return true;
    }

    /// <summary>
    /// Gets or sets the element with the specified key, adding a default if the key is not present.
    /// </summary>
    /// <param name="key"></param>
    /// <remarks>this will always return a value because a value will either be found or added</remarks>
    /// <returns>the value in the dictionary or the default</returns>
    public TValue this[TKey key]
    {
        get
        {
            if (!_dictionary.TryGetValue(key, out var value))
            {
                value = _dictionary[key] = _defaultValueFunc();
            }
            return value;
        }

        set { _dictionary[key] = value; }
    }

    /// <inheritdoc cref="IDictionary{TKey, TValue}.Keys"/>
    public ICollection<TKey> Keys => _dictionary.Keys;

    /// <inheritdoc cref="IDictionary{TKey, TValue}.Values"/>
    public ICollection<TValue> Values => _dictionary.Values;
}
