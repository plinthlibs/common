using Plinth.Database.PgSql;

namespace Plinth.Hangfire.PgSql;

internal class JobDAO(ISqlConnection connection)
{
    public List<PlinthJob> GetJobList()
        => connection.ExecuteQueryProcList("fn_get_job_list", LoadJob);

    private static PlinthJob LoadJob(IResult rs)
        => new()
        {
            Code = rs.GetString("o_code") ?? throw new InvalidOperationException("Job Code cannot be null"),
            Description = rs.GetString("o_description"),
            IsActive = rs.GetBool("o_is_active"),
            CronExpression = rs.GetString("o_cron_expression"),
            TimeZone = rs.GetString("o_time_zone"),
            JobData = rs.GetString("o_job_data")
        };
}
