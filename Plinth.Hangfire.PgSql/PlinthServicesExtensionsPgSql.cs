using Hangfire;
using Hangfire.PostgreSql;
using Hangfire.PostgreSql.Factories;
using Microsoft.Extensions.DependencyInjection;
using Plinth.Database.PgSql;
using Plinth.Hangfire.PgSql;

namespace Plinth.Hangfire;

/// <summary>
/// Extensions for a Plinth standard Hangfire instance using PostgreSQL
/// </summary>
public static class PlinthServicesExtensionsPgSql
{
    /// <summary>
    /// In ConfigureServices(), add Plinth hangfire setup (for PostgreSQL)
    /// </summary>
    /// <param name="services"></param>
    /// <param name="hangfireConnString">database connection string for the hangfire tables</param>
    /// <param name="registrarAction">an action to register job handlers</param>
    /// <param name="jobProcDbProv">optionally provide the transaction provider for accessing the jobs list stored proc.  If not provided, it will be resolved from the service provider</param>
    /// <param name="dbSyncCron">cron frequency for database job table sync (default to every 15 mins)</param>
    /// <param name="serverOptsAction">optional custom config for the background server</param>
    /// <param name="storageOptsAction">optional custom config for the storage options</param>
    /// <param name="configAction">optional custom config for the hangfire global configuration</param>
    /// <remarks>
    /// The PgSQL driver attempts to install schema updates by default, which can fail and throw if the DB is down.
    /// To avoid this, pass an action in <c>storageOptsAction</c> and set <c>PrepareSchemaIfNecessary</c> to false
    /// </remarks>
    public static IServiceCollection AddPlinthHangfire(this IServiceCollection services,
        string hangfireConnString,
        Action<JobHandlerRegistrar> registrarAction,
        ISqlTransactionProvider? jobProcDbProv = null,
        string? dbSyncCron = null,
        Action<BackgroundJobServerOptions>? serverOptsAction = null,
        Action<PostgreSqlStorageOptions>? storageOptsAction = null,
        Action<IGlobalConfiguration>? configAction = null)
        => AddPlinthHangfireImpl(services, hangfireConnString, registrarAction, true, jobProcDbProv, dbSyncCron, serverOptsAction, storageOptsAction, configAction);


    internal static IServiceCollection AddPlinthHangfireImpl(this IServiceCollection services,
        string hangfireConnString,
        Action<JobHandlerRegistrar> registrarAction,
        bool startAsync = true,
        ISqlTransactionProvider? jobProcDbProv = null,
        string? dbSyncCron = null,
        Action<BackgroundJobServerOptions>? serverOptsAction = null,
        Action<PostgreSqlStorageOptions>? storageOptsAction = null,
        Action<IGlobalConfiguration>? configAction = null)
    {
        var jobReg = new JobHandlerRegistrar();
        registrarAction.Invoke(jobReg);

        var storageOpts = new PostgreSqlStorageOptions
        {
            // https://github.com/frankhommers/Hangfire.PostgreSql/issues/31
            DistributedLockTimeout = TimeSpan.FromMinutes(1)
        };

        storageOptsAction?.Invoke(storageOpts);

        return services.AddPlinthHangfireInternal(
            new JobSyncFactory(jobProcDbProv),
            jobReg,
            new PostgreSqlStorage(new NpgsqlConnectionFactory(hangfireConnString, storageOpts), storageOpts),
            startAsync,
            dbSyncCron,
            serverOptsAction,
            configAction);
    }

}
