using Plinth.Database.PgSql;
using Plinth.Hangfire.Impl.Sync;

namespace Plinth.Hangfire.PgSql;

internal class JobRepository(ISqlTransactionProvider txnProvider) : IJobRepository
{
    public List<PlinthJob> GetJobList()
        => txnProvider.ExecuteTxn(c => new JobDAO(c).GetJobList());
}
