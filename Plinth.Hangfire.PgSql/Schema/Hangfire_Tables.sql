﻿
-- Plinth Job table
CREATE TABLE public.job (
    id BIGINT GENERATED ALWAYS AS IDENTITY NOT NULL, -- auto-indentity PK, should not be used directly
    code VARCHAR(100) NOT NULL,                      -- unique textual code for the job (e.g. 'MyStuff.DoSomething')
    description VARCHAR(255) NOT NULL,               -- textual description, short enough to be displayed in UI (e.g. 'My very first job for the demo purposes')
    job_data JSON NOT NULL,                          -- additional metadata for the job (e.g. '{ applyRules: true, maxLimit: 10 }', etc.)
    cron_expression VARCHAR(50) NOT NULL,            -- CRON expression for the schedule (e.g. '0 */4 * * *')
    time_zone VARCHAR(100) NOT NULL,                 -- time zone name for the schedule (e.g. 'Pacific Standard Time', 'UTC', etc.)
    is_active BOOLEAN NOT NULL,                      -- whether the job is currently active
    -- Audit
    date_inserted TIMESTAMP NOT NULL,
    inserted_by VARCHAR(255) NOT NULL,
    date_updated TIMESTAMP NOT NULL,
    updated_by VARCHAR(255) NOT NULL,
    -- PK and unique code
    CONSTRAINT pk_job PRIMARY KEY (id),
    CONSTRAINT uk_job_code UNIQUE (code)
)
-- Plinth Job table
