# README

### Plinth.Hangfire.PgSql

**PostgreSQL driver for _Plinth.Hangfire_**

When using PostgreSQL for job storage with _Plinth.Hangfire_

:point_right: See _Plinth.Hangfire_ instructions for using this package.

* Provides `public.job` in `Hangfire_Tables.sql`
* Provides `fn_submit_job` and `fn_get_job_list` in `Hangfire_Procedures.sql`
