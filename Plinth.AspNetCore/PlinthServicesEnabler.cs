using Microsoft.Extensions.Logging;
using Plinth.AspNetCore.Logging;
using Plinth.Common.Mapping;
using Plinth.Serialization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.HttpOverrides;
using Omu.ValueInjecter;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Plinth.AspNetCore;

/// <summary>
/// Enabler of Services
/// </summary>
public class PlinthServicesEnabler
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    // logging
    private const int DefaultMaxRequestLogging = 25000;
    private const int DefaultMaxResponseLogging = 25000;

    private static readonly ISet<string> DefaultDisabledMethods = new HashSet<string>() { "OPTIONS", "HEAD" };

    private bool _enableLogging = false;
    private List<string>? _loggablePaths = null;
    private int _maxLoggingReqBody = DefaultMaxRequestLogging;
    private int _maxLoggingRespBody = DefaultMaxResponseLogging;
    private ISet<string>? _disabledMethods = DefaultDisabledMethods;

    // exception handling
    private bool _enableExceptionHandling;
    private Func<Exception, ILogger, HandleExceptionResponse>? _customExceptionHandler;
    private bool _useDefaultExceptionHandler;
    private bool _useWebAppResponse;
    private bool _ignoreHandlerResponse;

    // type handling
    private bool _disableDatesAsUTC = false;
    private bool _disableValueInjecter = false;

    // auto controllers
    /// <summary>
    /// Use to enable auto controllers
    /// </summary>
    public AutoEndpointsEnabler AutoEndpoints { get; } = new AutoEndpointsEnabler();

    internal PlinthServicesEnabler()
    {
    }

    internal IServiceCollection Add(IServiceCollection services)
    {
        if (_enableLogging)
        {
            _loggablePaths ??= ["/api"];

            // make sure auto controllers get logged
            AutoEndpoints.AddLoggablePaths(_loggablePaths);

            // enable x-forwarded-for handling so we can get the real client IP
            services.Configure<ForwardedHeadersOptions>(o =>
            {
                o.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });

            services.Configure<LoggingMiddleware.Options>(o =>
            {
                o.MaxRequestBody = _maxLoggingReqBody;
                o.MaxResponseBody = _maxLoggingRespBody;
                o.LoggablePaths = _loggablePaths;
                o.DisabledMethods = _disabledMethods;
                o.AutoCtrlCheck = AutoEndpoints.GetHandlerCheck();
            });
        }

        if (_enableExceptionHandling)
        {
            services.Configure<UnhandledExceptionMiddleware.Options>(o =>
            {
                o.CustomHandler = _customExceptionHandler;
                o.UseDefaultHandler = _useDefaultExceptionHandler;
                o.UseWebAppResponse = _useWebAppResponse;
                o.IgnoreResponse = _ignoreHandlerResponse;
            });
        }

        // save for a later call to .Use
        services.AddSingleton(this);

        return services;
    }

    internal IApplicationBuilder Use(IApplicationBuilder builder)
    {
        // checks that you added UseRouting() at the top of configure, and UseEndpoints(...) instead of UseMvc()
        // https://github.com/aspnet/AspNetCore/blob/master/src/Http/Routing/src/Builder/EndpointRoutingApplicationBuilderExtensions.cs
        if (!builder.Properties.TryGetValue("__EndpointRouteBuilder", out var _))
        {
            log.Warn("For ASP.NET Core 3+, a call to 'app.UseRouting()' should be added first in the pipeline inside Configure()");
        }

        if (_enableLogging)
        {
            builder.UseForwardedHeaders();

            builder.UseMiddleware<LoggingMiddleware>();
        }

        if (_enableExceptionHandling)
        {
            builder.UseMiddleware<UnhandledExceptionMiddleware>();
        }

        AutoEndpoints.UseAutoEndpointMiddleware(builder);

        if (!_disableDatesAsUTC)
        {
            UtcDateTimeConverter.ApplyGlobally();
        }

        if (!_disableValueInjecter)
        {
            StaticValueInjecter.DefaultInjection = new PropertyNameInjection();
        }

        return builder;
    }

    /// <summary>
    /// Enable service logging for application
    /// </summary>
    public void EnableServiceLogging() => EnableServiceLogging(DefaultMaxRequestLogging, DefaultMaxResponseLogging, null);

    /// <summary>
    /// Enable service logging for application with specific loggable paths
    /// </summary>
    /// <param name="loggablePaths">paths that start with slash '/' but do NOT end with slash (e.g. "/logthis")</param>
    public void EnableServiceLogging(params string[] loggablePaths) => EnableServiceLogging(DefaultMaxRequestLogging, DefaultMaxResponseLogging, loggablePaths.ToList());

    /// <summary>
    /// Enable service logging for application with specific loggable paths
    /// </summary>
    /// <param name="loggablePaths">paths that start with slash '/' but do NOT end with slash (e.g. "/logthis")</param>
    /// <param name="disabledMethods">set of http methods to not log (default is no OPTIONS/HEAD)</param>
    public void EnableServiceLogging(List<string> loggablePaths, ISet<string> disabledMethods) => EnableServiceLogging(DefaultMaxRequestLogging, DefaultMaxResponseLogging, loggablePaths, disabledMethods);

    /// <summary>
    /// Enable service logging for application with custom limits on logged request/responses
    /// </summary>
    /// <param name="maxRequestBody">max bytes to log from request</param>
    /// <param name="maxResponseBody">max bytes to log from response</param>
    public void EnableServiceLogging(int maxRequestBody, int maxResponseBody) => EnableServiceLogging(maxRequestBody, maxResponseBody, null);

    /// <summary>
    /// Enable service logging for application with specific loggable paths and custom limits on logged request/responses
    /// </summary>
    /// <param name="maxRequestBody">max bytes to log from request</param>
    /// <param name="maxResponseBody">max bytes to log from response</param>
    /// <param name="loggablePaths">paths that start with slash '/' but do NOT end with slash (e.g. "/logthis")</param>
    /// <param name="disabledMethods">optional set of http methods to not log (default is no OPTIONS/HEAD)</param>
    public void EnableServiceLogging(int maxRequestBody, int maxResponseBody, List<string>? loggablePaths, ISet<string>? disabledMethods = null)
    {
        _enableLogging = true;
        _loggablePaths = loggablePaths;
        _maxLoggingReqBody = maxRequestBody;
        _maxLoggingRespBody = maxResponseBody;
        _disabledMethods = disabledMethods;
    }

    /// <summary>
    /// Enable default API service exception handling (best for Core APIs)
    /// </summary>
    public void EnableServiceExceptionHandling() => EnableServiceExceptionHandling(null);

    /// <summary>
    /// Enable default API service exception handling (best for Core APIs)
    /// <param name="customExceptionHandler"> a custom function for handling an exception, return null to not handle </param>
    /// <param name="useDefaultExceptionHandler"> whether default exception handler should be used, in addition to custom one </param>
    /// </summary>
    public void EnableServiceExceptionHandling(
        Func<Exception, ILogger, HandleExceptionResponse>? customExceptionHandler,
        bool useDefaultExceptionHandler = true)
    {
        EnableExceptionHandling(customExceptionHandler, useDefaultExceptionHandler);
    }

    /// <summary>
    /// Enable default Web service exception handling (best for Public APIs)
    /// </summary>
    public void EnableWebExceptionHandling() => EnableWebExceptionHandling(null);

    /// <summary>
    /// Enable default Web service exception handling (best for Public APIs)
    /// <param name="customExceptionHandler"> a custom function for handling an exception, return null to not handle </param>
    /// <param name="useDefaultExceptionHandler"> whether default exception handler should be used, in addition to custom one </param>
    /// </summary>
    public void EnableWebExceptionHandling(
        Func<Exception, ILogger, HandleExceptionResponse>? customExceptionHandler,
        bool useDefaultExceptionHandler = true)
    {
        EnableExceptionHandling(customExceptionHandler, useDefaultExceptionHandler, true);
    }

    /// <summary>
    /// Enable exception handling where exception will only by logged without affecting the HTTP response (best for MVC websites)
    /// </summary>
    public void EnableLogOnlyExceptionHandling() => EnableLogOnlyExceptionHandling(null);

    /// <summary>
    /// Enable exception handling where exception will only by logged without affecting the HTTP response (best for MVC websites)
    /// <param name="customExceptionHandler"> a custom function for handling an exception, return null to not handle </param>
    /// <param name="useDefaultExceptionHandler"> whether default exception handler should be used, in addition to custom one </param>
    /// </summary>
    public void EnableLogOnlyExceptionHandling(
        Func<Exception, ILogger, HandleExceptionResponse>? customExceptionHandler,
        bool useDefaultExceptionHandler = true)
    {
        EnableExceptionHandling(customExceptionHandler, useDefaultExceptionHandler, false, true);
    }

    /// <summary>
    /// Enable customized exception handling
    /// </summary>
    /// <param name="customExceptionHandler"> a custom function for handling an exception, return null to not handle </param>
    /// <param name="useDefaultExceptionHandler"> whether default exception handler should be used, in addition to custom one </param>
    /// <param name="useWebAppResponse"> whether to wrap response in a special ApiError structure, can be used for Public APIs </param>
    /// <param name="ignoreHandlerResponse"> whether to disregard handler response and just throw at the end </param>
    public void EnableExceptionHandling(
        Func<Exception, ILogger, HandleExceptionResponse>? customExceptionHandler = null,
        bool useDefaultExceptionHandler = true,
        bool useWebAppResponse = false,
        bool ignoreHandlerResponse = false)
    {
        _enableExceptionHandling = true;
        _customExceptionHandler = customExceptionHandler;
        _useDefaultExceptionHandler = useDefaultExceptionHandler;
        _useWebAppResponse = useWebAppResponse;
        _ignoreHandlerResponse = ignoreHandlerResponse;
    }

    /// <summary>
    /// By default, we convert DateTime parameters to UTC, this will disable that (not recommended!)
    /// </summary>
    /// <remarks>this affects all string => DateTime conversions</remarks>
    public void DisableDateTimesAsUTC()
    {
        log.Warn("Disabling DateTime parsing to UTC");
        _disableDatesAsUTC = true;
    }

    /// <summary>
    /// By default, we install our custom property injection mapper, this will disable that (not recommended!)
    /// </summary>
    /// <remarks>this affects all <c>Mapper.Map</c> calls</remarks>
    public void DisableCustomMapper()
    {
        log.Warn("Disabling Property Injection Mapper");
        _disableValueInjecter = true;
    }
}
