using Plinth.AspNetCore;

namespace Microsoft.Extensions.DependencyInjection;

/// <summary>
/// Extensions for IServiceCollection
/// </summary>
public static class IServiceCollectionExtensions
{
    /// <summary>
    /// Configure Plinth Services and Middleware
    /// </summary>
    /// <param name="services"></param>
    /// <param name="action">action which will receive a service enabler</param>
    /// <returns>builder</returns>
    public static IServiceCollection AddPlinthServices(this IServiceCollection services, Action<PlinthServicesEnabler> action)
    {
        var enabler = new PlinthServicesEnabler();
        action(enabler);
        return enabler.Add(services);
    }
}
