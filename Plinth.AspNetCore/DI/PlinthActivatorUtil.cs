// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

// Modified (heavily)

using System.Reflection;

namespace Plinth.AspNetCore.DI;

/// <summary>
/// Helper code for the various activator services.
/// </summary>
public static class PlinthActivatorUtil
{
    /// <summary>
    /// Instantiate a type with constructor arguments provided directly and/or from an <see cref="IServiceProvider"/>.
    /// </summary>
    /// <param name="provider">The service provider used to resolve dependencies</param>
    /// <param name="instanceType">The type to activate</param>
    /// <param name="parameters">Constructor arguments not provided by the <paramref name="provider"/>.</param>
    /// <returns>An activated object of type instanceType</returns>
    public static object CreateInstance(IServiceProvider provider, Type instanceType, params object?[]? parameters)
    {
        int bestLength = -1;
        ConstructorMatcher? bestMatcher = null;
        var serviceCache = new Dictionary<Type, object>(10);

        if (!instanceType.GetTypeInfo().IsAbstract)
        {
            foreach (var constructor in instanceType
                .GetTypeInfo()
                .DeclaredConstructors
                .Where(c => !c.IsStatic && c.IsPublic))
            {
                var matcher = new ConstructorMatcher(constructor);
                var length = matcher.Match(provider, parameters, serviceCache);

                if (bestLength < length)
                {
                    bestLength = length;
                    bestMatcher = matcher;
                }
            }
        }

        if (bestMatcher == null)
        {
            var message = $"A suitable constructor for type '{instanceType}' could not be located. Ensure the type is concrete and services are registered for all parameters of a public constructor.";
            throw new InvalidOperationException(message);
        }

        return bestMatcher.CreateInstance(provider, serviceCache);
    }

    /// <summary>
    /// Instantiate a type with constructor arguments provided directly and/or from an <see cref="IServiceProvider"/>.
    /// </summary>
    /// <typeparam name="T">The type to activate</typeparam>
    /// <param name="provider">The service provider used to resolve dependencies</param>
    /// <param name="parameters">Constructor arguments not provided by the <paramref name="provider"/>.</param>
    /// <returns>An activated object of type T</returns>
    public static T CreateInstance<T>(IServiceProvider provider, params object?[]? parameters)
    {
        return (T)CreateInstance(provider, typeof(T), parameters);
    }

    /// <summary>
    /// Instantiate a type with constructor arguments provided directly and/or from an <see cref="IServiceProvider"/>.
    /// </summary>
    /// <remarks>useful when applying two sets of parameters</remarks>
    /// <typeparam name="T">The type to activate</typeparam>
    /// <param name="provider">The service provider used to resolve dependencies</param>
    /// <param name="parameters">Constructor arguments not provided by the <paramref name="provider"/>.</param>
    /// <param name="otherParameters">more Constructor arguments not provided by the <paramref name="provider"/>.</param>
    /// <returns>An activated object of type T</returns>
    public static T CreateInstance<T>(IServiceProvider provider, object?[]? parameters, params object?[]? otherParameters)
    {
        var ps = parameters ?? ConstructorMatcher.NullArray;
        var ops = otherParameters ?? ConstructorMatcher.NullArray;

        var combined = new object[ps.Length + ops.Length];
        Array.Copy(ps, 0, combined, 0, ps.Length);
        Array.Copy(ops, 0, combined, ps.Length, ops.Length);
        return CreateInstance<T>(provider, combined);
    }
}

file sealed class ConstructorMatcher
{
    private readonly ConstructorInfo _constructor;
    private readonly ParameterInfo[] _parameters;
    private readonly object?[] _parameterValues;
    private readonly bool[] _parameterValuesSet;

    public static readonly object?[] NullArray = [null];

    public ConstructorMatcher(ConstructorInfo constructor)
    {
        _constructor = constructor;
        _parameters = _constructor.GetParameters();
        _parameterValuesSet = new bool[_parameters.Length];
        _parameterValues = new object?[_parameters.Length];
    }

    public int Match(IServiceProvider prov, object?[]? givenParameters, Dictionary<Type, object> serviceCache)
    {
        var gv = givenParameters ?? NullArray;

        int givenIndex = 0;
        int matched = 0;
        object? resolved;
        for (var applyIndex = 0; applyIndex < _parameters.Length; applyIndex++)
        {
            var pType = _parameters[applyIndex].ParameterType;
            if (serviceCache.TryGetValue(pType, out var r))
            {
                _parameterValues[applyIndex] = r;
            }
            else if ((resolved = prov.GetService(pType)) != null)
            {
                serviceCache[pType] = resolved;
                _parameterValues[applyIndex] = resolved;
            }
            else if (
                givenIndex < gv.Length
                    && (gv[givenIndex] == null
                        || pType.GetTypeInfo().IsAssignableFrom(gv[givenIndex]!.GetType().GetTypeInfo())))
            {
                _parameterValues[applyIndex] = gv[givenIndex];
                givenIndex++;
            }
            else
                continue;

            matched++;
            _parameterValuesSet[applyIndex] = true;
        }

        if (givenIndex != gv.Length)
            return -1;

        return matched;
    }

    public object CreateInstance(IServiceProvider provider, Dictionary<Type, object> serviceCache)
    {
        for (var index = 0; index != _parameters.Length; index++)
        {
            if (!_parameterValuesSet[index])
            {
                var pType = _parameters[index].ParameterType;
                var value = serviceCache.TryGetValue(pType, out var resolved) ? resolved : provider.GetService(pType);
                if (value == null)
                {
                    if (!TryGetDefaultValue(_parameters[index], out var defaultValue))
                    {
                        throw new InvalidOperationException($"Unable to resolve service for type '{_parameters[index].ParameterType}' while attempting to activate '{_constructor.DeclaringType}'.");
                    }
                    else
                    {
                        _parameterValues[index] = defaultValue;
                    }
                }
                else
                {
                    _parameterValues[index] = value;
                }
            }
        }

        return _constructor.Invoke(BindingFlags.DoNotWrapExceptions, binder: null, parameters: _parameterValues, culture: null);
    }

    private static bool TryGetDefaultValue(ParameterInfo parameter, out object? defaultValue)
    {
        bool hasDefaultValue = CheckHasDefaultValue(parameter, out bool tryToGetDefaultValue);
        defaultValue = null;

        if (hasDefaultValue)
        {
            if (tryToGetDefaultValue)
            {
                defaultValue = parameter.DefaultValue;
            }

            bool isNullableParameterType = parameter.ParameterType.IsGenericType &&
                parameter.ParameterType.GetGenericTypeDefinition() == typeof(Nullable<>);

            // Handle nullable enums
            if (defaultValue != null && isNullableParameterType)
            {
                Type? underlyingType = Nullable.GetUnderlyingType(parameter.ParameterType);
                if (underlyingType != null && underlyingType.IsEnum)
                {
                    defaultValue = Enum.ToObject(underlyingType, defaultValue);
                }
            }
        }

        return hasDefaultValue;
    }

    private static bool CheckHasDefaultValue(ParameterInfo parameter, out bool tryToGetDefaultValue)
    {
        tryToGetDefaultValue = true;
        return parameter.HasDefaultValue;
    }
}

