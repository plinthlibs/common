using Microsoft.AspNetCore.Builder;
using System.Reflection;

namespace Microsoft.Extensions.DependencyInjection;

/// <summary>
/// Extensions for IApplicationBuilder
/// </summary>
public static class AppBuilderExtensions
{
    /// <summary>
    /// Verify that all Controllers can be constructed by the container
    /// </summary>
    /// <param name="app"></param>
    /// <param name="excludedTypes">Types of controllers that should be excluded from this check</param>
    /// <exception cref="InvalidOperationException">thrown if a controller cannot be constructed</exception>
    /// <remarks>
    /// <para>Run this at the end of ConfigureServices().  It will fail app startup if a controller cannot be resolved</para>
    /// </remarks>
    public static IApplicationBuilder VerifyControllers(this IApplicationBuilder app, params Type[] excludedTypes)
    {
        var controllers = GetAssemblies(Assembly.GetCallingAssembly())
            .Where(a => a != null)
            .SelectMany(a => a!.GetTypes())
            .Where(t => typeof(AspNetCore.Mvc.ControllerBase).IsAssignableFrom(t) && !t.IsAbstract && !excludedTypes.Contains(t))
            .Distinct();

        // creating a scope is necessary to support scoped services
        using var prov = app.ApplicationServices.CreateScope();

        foreach (var type in controllers)
        {
            try
            {
                ActivatorUtilities.CreateInstance(prov.ServiceProvider, type, Type.EmptyTypes);
            }
            catch (InvalidOperationException e)
            {
                throw new InvalidOperationException($"unable to resolve controller {type.FullName}, check container bootstrap", e);
            }
        }

        return app;

        static IEnumerable<Assembly?> GetAssemblies(Assembly callingAssembly)
        {
            yield return callingAssembly;
            yield return Assembly.GetEntryAssembly();
            yield return Assembly.GetExecutingAssembly();
        }
    }
}
