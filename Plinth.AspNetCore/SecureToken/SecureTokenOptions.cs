using Microsoft.Extensions.Options;
using Plinth.AspNetCore.TokenAuth;
using Plinth.Security.Crypto;

namespace Plinth.AspNetCore.SecureToken;

/// <summary>
/// Options for SecureToken authentication handler
/// </summary>
public class SecureTokenOptions : TokenAuthHandlerOptionsBase
{
    /// <summary>
    /// Set the encryption key for tokens as a 32/48/64 hex char string
    /// </summary>
    public string EncryptionKey { set { SecureData = new SecureData(value); } }

    /// <summary>
    /// ISecureData for encryption (vs setting the key directly)
    /// </summary>
    public ISecureData? SecureData { get; set; }

    // protected override void Validate() is called on every request, the post configure is called only once
    private protected override void ValidateOptions()
    {
        if (SecureData == null)
            throw new ArgumentException("EncryptionKey or SecureData must be set");

        base.ValidateOptions();
    }

    // this gets called once after the options are configured
    internal class SecureTokenPostConfigureOptions : IPostConfigureOptions<SecureTokenOptions>
    {
        public void PostConfigure(string? name, SecureTokenOptions options)
        {
            options.ValidateOptions();
        }
    }
}

