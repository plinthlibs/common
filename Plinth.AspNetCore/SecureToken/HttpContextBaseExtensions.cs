using Microsoft.AspNetCore.Http;
using Plinth.AspNetCore.TokenAuth;

namespace Plinth.AspNetCore.SecureToken;

/// <summary>
/// Extensions for HttpContext
/// </summary>
public static class HttpContextBaseExtensions
{
    /// <summary>
    /// Get the AuthenticationToken object that was extracted from the Authorization Header
    /// </summary>
    /// <param name="context"></param>
    /// <exception cref="Security.SecurityException">if user is not authenticated</exception>
    public static Security.Tokens.AuthenticationToken GetAuthenticationToken(this HttpContext context)
        => Check(context, () => (Security.Tokens.AuthenticationToken)context.Items[SecureTokenHandler.ItemKeyAuthenticationToken]!);

    /// <summary>
    /// Try to get the AuthenticationToken object that was extracted from the Authorization Header
    /// </summary>
    /// <param name="context"></param>
    /// <return>null if user is not authenticated</return>
    public static Security.Tokens.AuthenticationToken? TryGetAuthenticationToken(this HttpContext context)
        => context.Items.TryGetValue(SecureTokenHandler.ItemKeyAuthenticationToken, out var o)
            ? o as Security.Tokens.AuthenticationToken
            : null;

    private static T Check<T>(HttpContext context, Func<T> val)
    {
        if (context.User.Identity?.IsAuthenticated ?? false)
            return val();
        throw new Security.SecurityException("user is not authenticated");
    }
}
