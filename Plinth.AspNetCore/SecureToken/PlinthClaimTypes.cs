namespace Plinth.AspNetCore.SecureToken;

/// <summary>
/// Defines constants for the custom claim types that can be assigned to a subject.
/// </summary>
public static class PlinthClaimTypes
{
    /// <summary>
    /// Claim Type for Session ID, http://plinthlibs/claims/SessionID
    /// </summary>
    public const string SessionId = "http://plinthlibs/claims/SessionID";

    /// <summary>
    /// Claim Type for custom data, http://plinthlibs/claims/data
    /// </summary>
    public const string Data = "http://plinthlibs/claims/data";
}
