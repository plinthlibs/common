using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Plinth.AspNetCore.TokenAuth;
using Plinth.Common.Logging;

namespace Plinth.AspNetCore.SecureToken;

/// <summary>
/// in ASP.NET Core, authentication is separate from authorization
/// So every request will come in here (if this is set as the default scheme), and the result will either be 'none', 'fail', or 'success'
/// Regardless, the request will continue until it hits an Authorization check, and that is where it might be stopped
/// </summary>
internal class SecureTokenHandler : TokenAuthHandlerBase<SecureTokenOptions>
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    internal const string ItemKeyAuthenticationToken = "plinthlibs/SecureToken";

    public SecureTokenHandler(IOptionsMonitor<SecureTokenOptions> options, ILoggerFactory logger, UrlEncoder encoder
#if NET8_0_OR_GREATER
        )
        : base(options, logger, encoder)
#else
        ,ISystemClock clock)
        : base(options, logger, encoder, clock)
#endif
    {
    }

    protected override AuthenticateResult ValidateToken(HttpRequest request, string tokenValue, bool requestLoggingEnabled)
    {
        try
        {
            var authToken = Security.Tokens.AuthenticationToken.FromEncryptedToken(
                Options.SecureData!, tokenValue, requestLoggingEnabled, throwOnExpired: false);

            if (authToken.IsExpired)
            {
                if (requestLoggingEnabled) log.Info("token is expired");
                return AuthenticateResult.Fail("token is expired", new AuthenticationProperties { IssuedUtc = authToken.Issued, ExpiresUtc = authToken.Expires });
            }

            RequestTraceProperties.SetRequestTraceUserName(authToken.SubjectName);
            RequestTraceProperties.SetRequestTraceSessionId(authToken.SessionGuid.ToString());

            Request.HttpContext.Items[ItemKeyAuthenticationToken] = authToken;

            return AuthenticateResult.Success(new AuthenticationTicket(authToken.ToClaimsPrincipal(), Scheme.Name));
        }
        catch (Security.SecurityException e)
        {
            log.Warn($"invalid authorization header: {e.Message}");
            return AuthenticateResult.Fail("invalid authorization data");
        }
    }
}
