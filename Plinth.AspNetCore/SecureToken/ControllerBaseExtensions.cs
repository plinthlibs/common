using System.Collections.Immutable;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;

namespace Plinth.AspNetCore.SecureToken;

/// <summary>
/// Extensions for API Controllers
/// </summary>
public static class ControllerBaseExtensions
{
    /// <summary>
    /// Get the AuthenticationToken object that was extracted from the Authorization Header
    /// </summary>
    /// <param name="controller"></param>
    /// <exception cref="Security.SecurityException">if user is not authenticated</exception>
    public static Security.Tokens.AuthenticationToken GetAuthenticationToken(this ControllerBase controller)
        => controller.HttpContext.GetAuthenticationToken();

    /// <summary>
    /// Try to get the AuthenticationToken object that was extracted from the Authorization Header
    /// </summary>
    /// <param name="controller"></param>
    /// <returns>null if user is not authenticated</returns>
    public static Security.Tokens.AuthenticationToken? TryGetAuthenticationToken(this ControllerBase controller)
        => controller.HttpContext.TryGetAuthenticationToken();

    /// <summary>
    /// Get the user name of the authenticated user.
    /// </summary>
    /// <param name="controller"></param>
    /// <remarks>User.Identity.Name also works</remarks>
    /// <exception cref="Security.SecurityException">if user is not authenticated</exception>
    public static string? GetAuthenticatedUserName(this ControllerBase controller)
        => Check(controller, () => controller.User.Identity?.Name);

    /// <summary>
    /// Try to get the user name of the authenticated user.
    /// </summary>
    /// <param name="controller"></param>
    /// <remarks>User.Identity.Name also works</remarks>
    /// <returns>null if user is not authenticated</returns>
    public static string? TryGetAuthenticatedUserName(this ControllerBase controller)
        => controller.User.Identity?.Name;

    /// <summary>
    /// Get the user guid of the authenticated user.
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    /// <remarks>Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value) also works</remarks>
    /// <exception cref="Security.SecurityException">if user is not authenticated</exception>
    public static Guid GetAuthenticatedUserGuid(this ControllerBase controller)
        => Check(controller, () => GetAuthenticationToken(controller).SubjectGuid);

    /// <summary>
    /// Try to get the user guid of the authenticated user.
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    /// <remarks>Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value) also works</remarks>
    /// <returns>null if user is not authenticated</returns>
    public static Guid? TryGetAuthenticatedUserGuid(this ControllerBase controller)
        => TryGetAuthenticationToken(controller)?.SubjectGuid;

    /// <summary>
    /// Get the session guid of the authenticated user.
    /// </summary>
    /// <param name="controller"></param>
    /// <exception cref="Security.SecurityException">if user is not authenticated</exception>
    public static Guid GetAuthenticatedUserSessionGuid(this ControllerBase controller)
        => Check(controller, () => GetAuthenticationToken(controller).SessionGuid);

    /// <summary>
    /// Try to get the session guid of the authenticated user.
    /// </summary>
    /// <param name="controller"></param>
    /// <returns>null if user is not authenticated</returns>
    public static Guid? TryGetAuthenticatedUserSessionGuid(this ControllerBase controller)
        => TryGetAuthenticationToken(controller)?.SessionGuid;

    /// <summary>
    /// Get the roles assigned to the authenticated user.
    /// </summary>
    /// <param name="controller"></param>
    /// <remarks>User.Claims.Where(c => c.Type == ClaimTypes.Role) also works</remarks>
    /// <exception cref="Security.SecurityException">if user is not authenticated</exception>
    public static ISet<string> GetAuthenticatedUserRoles(this ControllerBase controller)
        => Check(controller, () => new HashSet<string>(controller.User.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value)));

    /// <summary>
    /// Try to get the roles assigned to the authenticated user.
    /// </summary>
    /// <param name="controller"></param>
    /// <remarks>User.Claims.Where(c => c.Type == ClaimTypes.Role) also works</remarks>
    /// <returns>null if user is not authenticated</returns>
    public static ISet<string>? TryGetAuthenticatedUserRoles(this ControllerBase controller)
        => (controller.User.Identity?.IsAuthenticated ?? false) ? controller.GetAuthenticatedUserRoles() : null;

    /// <summary>
    /// Get an encapsulation object of the secure token info for the authenticated user
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    public static SecureTokenInfo GetSecureTokenInfo(this ControllerBase controller)
        => Check(controller, () => new SecureTokenInfo(controller));

    /// <summary>
    /// Get an encapsulation object of the secure token info for the authenticated user
    /// </summary>
    /// <param name="controller"></param>
    /// <returns>null if user is not authenticated</returns>
    public static SecureTokenInfo? TryGetSecureTokenInfo(this ControllerBase controller)
        => (controller.User.Identity?.IsAuthenticated ?? false) ? new SecureTokenInfo(controller) : null;

    private static T Check<T>(ControllerBase controller, Func<T> val)
    {
        if (controller.User.Identity?.IsAuthenticated ?? false)
            return val();
        throw new Security.SecurityException("user is not authenticated");
    }
}

/// <summary>
/// Encapsulation of the secure token info for the authenticated user, useful for passing into logic classes.
/// </summary>
/// <remarks>not thread safe</remarks>
public class SecureTokenInfo
{
    private IImmutableSet<string>? _roles;

    /// <summary>
    /// Access to the controller for derived classes
    /// </summary>
    protected ControllerBase Controller { get; }

    /// <summary>
    /// Create from a controller
    /// </summary>
    /// <param name="controller"></param>
    protected internal SecureTokenInfo(ControllerBase controller)
    {
        Controller = controller;
    }

    /// <summary>
    /// The AuthenticationToken object that was extracted from the Authorization Header
    /// </summary>
    public Security.Tokens.AuthenticationToken Token => Controller.GetAuthenticationToken();

    /// <summary>
    /// The user name of the authenticated user.
    /// </summary>
    public string? UserName => Controller.GetAuthenticatedUserName();

    /// <summary>
    /// The user guid of the authenticated user.
    /// </summary>
    public Guid UserGuid => Controller.GetAuthenticatedUserGuid();

    /// <summary>
    /// The session guid of the authenticated user.
    /// </summary>
    public Guid SessionGuid => Controller.GetAuthenticatedUserSessionGuid();

    /// <summary>
    /// The roles assigned to the authenticated user.
    /// </summary>
    public IImmutableSet<string> Roles
    {
        get
        {
            _roles ??= Controller.User.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).ToImmutableHashSet();
            return _roles;
        }
    }
}
