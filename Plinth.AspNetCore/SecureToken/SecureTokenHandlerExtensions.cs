using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using Plinth.AspNetCore.TokenAuth;

namespace Plinth.AspNetCore.SecureToken;

/// <summary>
/// Extensions for utilizing SecureToken auth in Startup.cs
/// </summary>
public static class SecureTokenHandlerExtensions
{
    /// <summary>
    /// Add SecureToken auth during ConfigureServices()
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configureOptions">required configuration method to configure SecureTokenOptions</param>
    public static IServiceCollection AddSecureTokenAuth(this IServiceCollection services, Action<SecureTokenOptions> configureOptions)
    {
        AddOptionsValidate(services);
        services.AddAuthentication(TokenAuthHandlerOptionsBase.DefaultScheme)
            .AddScheme<SecureTokenOptions, SecureTokenHandler>(TokenAuthHandlerOptionsBase.DefaultScheme, null, configureOptions);
        return services;
    }

    /// <summary>
    /// Add SecureToken auth during ConfigureServices()
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configureOptions">required configuration method to configure SecureTokenOptions</param>
    /// <param name="authScheme">Auth Scheme name</param>
    public static IServiceCollection AddCustomSecureTokenAuth(this IServiceCollection services, Action<SecureTokenOptions> configureOptions, string authScheme)
    {
        AddOptionsValidate(services);
        services.AddAuthentication()
            .AddScheme<SecureTokenOptions, SecureTokenHandler>(authScheme, null, configureOptions);
        return services;
    }

    /// <summary>
    /// Add SecureToken auth for SignalR Hubs.
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configureOptions">required configuration method to configure SecureTokenOptions</param>
    /// <param name="authScheme">name of auth scheme, defaults to "SignalR"</param>
    /// <example>
    /// To use, put this on your hub class
    /// <code>[Authorize(AuthenticationSchemes = "SignalR")]</code>
    /// </example>
    public static IServiceCollection AddSecureTokenAuthForSignalR(this IServiceCollection services, Action<SecureTokenOptions> configureOptions, string authScheme = "SignalR")
    {
        if (authScheme == TokenAuthHandlerOptionsBase.DefaultScheme)
            throw new ArgumentException("signalR auth scheme cannot match the default scheme");

        AddOptionsValidate(services);
        services.AddAuthentication()
            .AddScheme<SecureTokenOptions, SecureTokenHandler>(authScheme, null, config);
        return services;

        void config(SecureTokenOptions o)
        {
            configureOptions(o);
            // signalr client code uses 'access_token' as the query param variable
            o.CustomTokenSource ??= r => TokenAuthQueryStringHandler.GetTokenFromQueryString(r, "access_token");
            o.CustomTokenSourceFallsBack = true;
            // signalr auth should only be enabled on the hub, so we will check all requests
            o.UrlPathFilter ??= _ => true;
        }
    }

    private static void AddOptionsValidate(IServiceCollection services)
        => services.TryAddSingleton<IPostConfigureOptions<SecureTokenOptions>, SecureTokenOptions.SecureTokenPostConfigureOptions>();
}
