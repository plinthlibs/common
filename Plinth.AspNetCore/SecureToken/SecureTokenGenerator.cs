using Plinth.Security.Crypto;
using Plinth.Security.Tokens;

namespace Plinth.AspNetCore.SecureToken;

/// <summary>
/// Generator for SecureTokens, useful in Login/Session endpoints
/// </summary>
public class SecureTokenGenerator
{
    private readonly ISecureData _secureData;
    private readonly TimeSpan _lifetime;

    /// <summary>
    /// Create a SecureTokenGenerator
    /// </summary>
    /// <param name="key">key for encrypting tokens.  For Signing: needs to be at least 32 hex chars, for Encrypt: 64 hex chars</param>
    /// <param name="tokenLifetime">how long the token should be valid</param>
    public SecureTokenGenerator(string key, TimeSpan tokenLifetime) : this(new SecureData(key), tokenLifetime)
    {
    }

    /// <summary>
    /// Create a SecureTokenGenerator
    /// </summary>
    /// <param name="secureData">secure data utility for crypto</param>
    /// <param name="tokenLifetime">how long the token should be valid</param>
    public SecureTokenGenerator(ISecureData secureData, TimeSpan tokenLifetime)
    {
        if (tokenLifetime <= TimeSpan.Zero) throw new ArgumentOutOfRangeException(nameof(tokenLifetime), "must be > TimeSpan.Zero");

        _secureData = secureData ?? throw new ArgumentNullException(nameof(secureData));
        _lifetime = tokenLifetime;
    }

    /// <summary>
    /// Generate a SecureToken
    /// </summary>
    /// <param name="userGuid">user's unique guid identifier</param>
    /// <param name="userName">user's unique name/email/etc</param>
    /// <param name="roles">(optional) list of roles</param>
    /// <returns>a fresh AuthenticationToken</returns>
    public AuthenticationToken Generate(Guid userGuid, string userName, IEnumerable<string>? roles = null)
        => GetBuilder(userGuid, userName, roles).Build();

    /// <summary>
    /// Get a builder for custom AuthenticationToken creation
    /// </summary>
    /// <param name="userGuid">user's unique guid identifier</param>
    /// <param name="userName">user's unique name/email/etc</param>
    /// <param name="roles">(optional) list of roles</param>
    /// <returns>A builder that has the user guid/name/roles and key/lifetime already set</returns>
    public AuthenticationTokenBuilder GetBuilder(Guid userGuid, string userName, IEnumerable<string>? roles = null)
    {
        var bld = new AuthenticationTokenBuilder()
            .WithSecureData(_secureData)
            .WithSubject(userGuid, userName)
            .WithLifetime(_lifetime)
            .WithLogging();

        if (!roles.IsNullOrEmpty())
            bld = bld.WithRoles(roles!);

        return bld;
    }

    /// <summary>
    /// Refresh a token by creating a new one with the original data but a new lifetime
    /// </summary>
    /// <param name="origToken"></param>
    /// <remarks>The original token's OriginalIssued is maintained.  You should check it for maximum refresh time (shouldn't be unlimited)</remarks>
    /// <returns>a fresh AuthenticationToken</returns>
    public AuthenticationToken Refresh(AuthenticationToken origToken)
        => GetBuilder(origToken.SubjectGuid, origToken.SubjectName, origToken.Roles)
            .WithOriginalIssue(origToken.OriginalIssued)
            .WithSessionGuid(origToken.SessionGuid)
            .WithData(origToken.Data)
            .WithLogging()
            .Build();
}
