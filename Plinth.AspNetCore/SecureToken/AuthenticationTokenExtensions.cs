using Plinth.Serialization;
using System.Linq;
using System.Security.Claims;

namespace Plinth.AspNetCore.SecureToken;

/// <summary>
/// AuthenticationToken Extensions specific for Secure Tokens
/// </summary>
public static class AuthenticationTokenExtensions
{
    /// <summary>
    /// Convert this Authentication Token into a ClaimsPrincipal for ASP.NET Core (and classic) authorization
    /// </summary>
    /// <returns>a ClaimsPrincipal with Claims set</returns>
    public static ClaimsPrincipal ToClaimsPrincipal(this Security.Tokens.AuthenticationToken token)
    {
        var identity = new ClaimsIdentity(
            [
                new Claim(ClaimTypes.Name, token.SubjectName),
                new Claim(ClaimTypes.NameIdentifier, token.SubjectGuid.ToString()),
                new Claim(ClaimTypes.AuthenticationInstant, token.OriginalIssued.ToString("u"), ClaimValueTypes.DateTime),
                new Claim(ClaimTypes.Expiration, token.Expires.ToString("u"), ClaimValueTypes.DateTime),
                new Claim(PlinthClaimTypes.SessionId, token.SessionGuid.ToString())
            ],
            authenticationType: "token");

        if (!token.Roles.IsNullOrEmpty())
            identity.AddClaims(token.Roles!.Select(r => new Claim(ClaimTypes.Role, r)));

        if (!token.Data.IsNullOrEmpty())
            identity.AddClaims(token.Data!
                .Where(kvp => kvp.Value != null)
                .Select(kvp => new Claim($"{PlinthClaimTypes.Data}/{kvp.Key}", convertValue(kvp.Value)!))
            );

        return new ClaimsPrincipal(identity);

        static string? convertValue(object? value)
            => value switch
            {
                string s => s,
                not null => JsonUtil.SerializeObject(value).Trim('"'),
                _ => throw new NotImplementedException() // shouldn't be possible due to filtering above
            };
    }
}
