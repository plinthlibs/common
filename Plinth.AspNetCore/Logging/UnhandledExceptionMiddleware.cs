using System.Security;
using System.Text;
using Microsoft.Extensions.Logging;
using Plinth.Common.Constants;
using Plinth.Common.Exceptions;
using Plinth.Serialization;
using Plinth.HttpApiClient.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;

namespace Plinth.AspNetCore.Logging;

/// <summary>
/// Middleware for catching any unhandled exceptions and turning them into valid HTTP Responses
/// </summary>
internal class UnhandledExceptionMiddleware
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    public class Options
    {
        public Func<Exception, ILogger, HandleExceptionResponse?>? CustomHandler { get; set; }
        public bool UseDefaultHandler { get; set; }
        public bool UseWebAppResponse { get; set; }
        public bool IgnoreResponse { get; set; }
    }

    private readonly RequestDelegate _next;
    private readonly IOptions<Options> _options;

    /// <summary>
    /// Initializes a new instance.
    /// </summary>
    public UnhandledExceptionMiddleware(RequestDelegate next, IOptions<Options> options)
    {
        _next = next;
        _options = options;
    }

    /// <summary>
    /// Invoke
    /// </summary>
    public async Task Invoke(HttpContext context)
    {
        try
        {
            await _next(context);
        }
        catch (Exception e)
        {
            if (context.Response.HasStarted)
            {
                log.Warn("The response has already started, the http status code middleware will not be executed.");
                throw;
            }
 
            var handled = HandleException(e);

            // exception was not handled at all
            if (handled == null)
            {
                log.Error(e, "Unhandled Exception");
                throw;
            }

            // exception was handled, but it configured not to provide custom response and just throw instead
            if (_options.Value.IgnoreResponse)
            {
                throw;
            }

            // the Response.Clear() line will drop the CORS headers, we save it here and restore later.
            // Otherwise browsers would detect our error responses as a CORS error and not what they actually are
            context.Response.Headers.TryGetValue(CorsConstants.AccessControlAllowOrigin, out var corsAllowOrigin);
            context.Response.Headers.TryGetValue(CorsConstants.AccessControlAllowCredentials, out var corsAllowCreds);
            context.Response.Headers.TryGetValue(CorsConstants.AccessControlExposeHeaders, out var corsExposeHeaders);
            context.Response.Headers.TryGetValue(HeaderNames.Vary, out var corsVary);

            context.Response.Clear();
            context.Response.StatusCode = handled.StatusCode;
            context.Response.ContentType = ContentTypes.ApplicationJsonUtf8;

            if (corsAllowOrigin.Count > 0)
                context.Response.Headers[CorsConstants.AccessControlAllowOrigin] = corsAllowOrigin;
            if (corsAllowCreds.Count > 0)
                context.Response.Headers[CorsConstants.AccessControlAllowCredentials] = corsAllowCreds;
            if (corsExposeHeaders.Count > 0)
                context.Response.Headers[CorsConstants.AccessControlExposeHeaders] = corsExposeHeaders;
            if (corsVary.Count > 0)
                context.Response.Headers[HeaderNames.Vary] = corsVary;

            string responseContent = null!;

            if (handled.Content is string stringContent)
            {
                if (_options.Value.UseWebAppResponse)
                {
                    responseContent = JsonUtil.SerializeObject(
                        new ApiResponse(new ApiError(ApiErrorCode.ServerError, stringContent)));
                }
                else
                {
                    responseContent = JsonUtil.SerializeObject(new { message = stringContent });
                }
            }
            else if (handled.Content != null)
            {
                responseContent = JsonUtil.SerializeObject(handled.Content);
            }

            await context.Response.WriteAsync(responseContent, Encoding.UTF8);
        }
    }

    private HandleExceptionResponse? HandleException(Exception e)
    {
        if (_options.Value.CustomHandler != null)
        {
            var custom = _options.Value.CustomHandler(e, log);
            if (custom != null)
                return custom;
        }

        if (!_options.Value.UseDefaultHandler)
            return null;

        return DefaultExceptionHandler(e);
    }

    private static HandleExceptionResponse DefaultExceptionHandler(Exception e)
    {
        object content = e.Message;
        int statusCode = StatusCodes.Status500InternalServerError;

        switch (e)
        {
            case SecurityException _:
            case Security.SecurityException _:
                log.Warn(e, $"Security Exception: {e.Message}");
                statusCode = StatusCodes.Status403Forbidden;
                break;

            case LogicalForbiddenException lfe:
                log.Warn(e, $"Forbidden Exception: {e.Message}");
                statusCode = StatusCodes.Status403Forbidden;
                content = GetContentFromLogicalException(lfe, content);
                break;

            case LogicalNotFoundException lnfe:
                log.Warn(e, $"Not Found Exception: {e.Message}");
                statusCode = StatusCodes.Status404NotFound;
                content = GetContentFromLogicalException(lnfe, content);
                break;

            case LogicalConflictException lce:
                log.Warn(e, $"Conflict Exception: {e.Message}");
                statusCode = StatusCodes.Status409Conflict;
                content = GetContentFromLogicalException(lce, content);
                break;

            case LogicalPreconditionFailedException lpfe:
                log.Warn(e, $"Precondition Failed Exception: {e.Message}");
                statusCode = StatusCodes.Status412PreconditionFailed;
                content = GetContentFromLogicalException(lpfe, content);
                break;

            case LogicalBadRequestException lbre:
                log.Warn(e, $"Bad Request Exception: {e.Message}");
                statusCode = StatusCodes.Status400BadRequest;
                content = GetContentFromLogicalException(lbre, content);
                break;

            case HttpApiClient.Common.RestException restEx:
                log.Warn(e, $"Rest Exception: {e.Message}");
                statusCode = (int)restEx.StatusCode;
                break;

            default:
                log.Error(e, $"Unhandled Exception: {e.Message}");
                break;
        }

        return new HandleExceptionResponse
        {
            StatusCode = statusCode,
            Content = content
        };
    }

    private static object GetContentFromLogicalException(LogicalException e, object defaultContent)
    {
        return e.CustomData ?? defaultContent;
    }
}

/// <summary>
/// Response for a custom handling of an exception
/// </summary>
public class HandleExceptionResponse
{
    /// <summary>If Handled, custom status code (from <see cref="Microsoft.AspNetCore.Http.StatusCodes"/>)</summary>
    public int StatusCode { get; set; }
    /// <summary>If Handled, custom content (string or serializable object)</summary>
    public object? Content { get; set; }
}
