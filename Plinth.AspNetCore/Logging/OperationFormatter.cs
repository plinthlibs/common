using Microsoft.Extensions.Logging;
using Plinth.AspNetCore.AutoEndpoints;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc.Controllers;

namespace Plinth.AspNetCore.Logging;

internal static class OperationFormatter
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    public static string FormOperationString(string? actionName, string? controllerNamespace, string? controllerName)
    {
        actionName ??= "UnknownAction";

        if (controllerName == null)
            return $"UnknownController::{actionName}";

        var ctrlName = controllerName;
        if (controllerNamespace != null)
        {
            var elements = controllerNamespace.Split('.').SkipWhile(s => s != "Controllers").Skip(1).Concat(Enumerable.Repeat(controllerName, 1));

            ctrlName = string.Join(".", elements);

            if (ctrlName.Length == 0)
                return $"{controllerNamespace}.{controllerName}Controller::{actionName}";
        }

        return $"{ctrlName}Controller::{actionName}";
    }

    private static string FormOperationString(ControllerActionDescriptor? actionDesc)
    {
        var actName = actionDesc?.MethodInfo.Name;
        var ctrlNamespace = actionDesc?.ControllerTypeInfo?.Namespace;
        var ctrlName = actionDesc?.ControllerName;

        return FormOperationString(actName, ctrlNamespace, ctrlName);
    }

    public static string GetOperationFromRequest(HttpContext context, AutoEndpointsHandlerCheck? autoCtrlCheck)
    {
        try
        {
            // netcore3 changed mvc routing, need to figure out how to get controller and action
            // got some of this from here: https://aregcode.com/blog/2019/dotnetcore-understanding-aspnet-endpoint-routing/

            var endpointFeature = context.Features[typeof(IEndpointFeature)] as IEndpointFeature;
            var endpoint = endpointFeature?.Endpoint;

            //note: endpoint will be null, if there was no
            //route match found for the request by the endpoint route resolver middleware
            if (endpoint != null)
            {
                var actionDesc = endpoint.Metadata.OfType<ControllerActionDescriptor>().FirstOrDefault(o => o != null);
                return FormOperationString(actionDesc);
            }
            else if (autoCtrlCheck != null && autoCtrlCheck.WillHandle(context, out var opName))
                return opName;
        }
        catch (Exception e)
        {
            log.Warn(e, "caught while determining route");
        }

        return FormOperationString(null, null, null);
    }
}
