using System.Diagnostics;
using System.Text;
using Microsoft.Extensions.Logging;
using Plinth.Common.Constants;
using Plinth.Common.Extensions;
using Plinth.Common.Logging;
using Plinth.Serialization;
using Plinth.AspNetCore.AutoEndpoints;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Primitives;
using Microsoft.Extensions.Options;
using static Plinth.Common.Logging.Scopes;

namespace Plinth.AspNetCore.Logging;

/// <summary>
/// Middleware that runs at start of request flow to log the incoming request and outgoing response
/// </summary>
public partial class LoggingMiddleware
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private static readonly List<string> _loggableContentTypes =
    [
        ContentTypes.ApplicationJson,
        ContentTypes.ApplicationJavascript,
        ContentTypes.ApplicationEcmascript
    ];

    private readonly RequestDelegate _next;

    // start the request ids off at a random int32, so it doesn't reset to 0 on app reset
    private static long _requestId = new Random().Next();

    /// <summary>
    /// Set the context ID (logged as CTX-ABCDE)
    /// You only need to call this directly if spawning a thread outside the request flow
    /// For example, if using HostingEnvironment.QueueBackgroundWorkItem() or Task.Run()
    /// </summary>
    public static void SetContextId()
    {
        // do a thread safe increment on the request id and use that for async context
        // only the bottom 20 bits are used, so it will reset every ~1M requests
        StaticLogManager.SetContextId(Interlocked.Increment(ref _requestId));
    }

    private readonly IOptions<Options> _opts;
    private readonly List<string> _loggablePaths;
    private readonly ISensitiveDataMasker? _sensitiveDataMasker;
    private readonly ContentPreProcessor _contentPreProcessor = new();

    /// <summary>Options for logging middleware</summary>
    public class Options
    {
        /// <summary>max bytes to log from request</summary>
        public int MaxRequestBody { get; set; }
        /// <summary>max bytes to log from response</summary>
        public int MaxResponseBody { get; set; }
        /// <summary>paths that start with slash '/' but do NOT end with slash (e.g. "/logthis")</summary>
        public List<string>? LoggablePaths { get; set; }
        /// <summary>checker/opName loader for auto controllers</summary>
        public AutoEndpointsHandlerCheck? AutoCtrlCheck { get; set; }
        /// <summary>Which HTTP methods should not be logged</summary>
        public ISet<string>? DisabledMethods { get; set; }
    }

    /// <summary>
    /// Constructor with variable loggable paths
    /// </summary>
    /// <param name="next">next request delegate</param>
    /// <param name="options"></param>
    /// <param name="sensitiveDataMasker"></param>
    // must be public because asp.net core middleware must be public to be loaded
    public LoggingMiddleware(RequestDelegate next, IOptions<Options> options, ISensitiveDataMasker sensitiveDataMasker) : this(next, options)
    {
        _sensitiveDataMasker = sensitiveDataMasker;
    }

    /// <summary>
    /// Constructor with variable loggable paths
    /// </summary>
    /// <param name="next">next request delegate</param>
    /// <param name="options"></param>
    // must be public because asp.net core middleware must be public to be loaded
    public LoggingMiddleware(RequestDelegate next, IOptions<Options> options)
    {
        _next = next;
        _opts = options;
        _loggablePaths = options.Value.LoggablePaths?.Select(lp => lp.TrimEnd('/')).ToList() ?? ["/api"];
    }

    /// <summary>
    /// Preprocess the request content before logging it for a given api path
    /// </summary>
    /// <param name="apiPath">absolute path, not including query string, starts with slash</param>
    /// <param name="preProcessor"></param>
    public void AddRequestPreprocessor(string apiPath, Func<string, string> preProcessor)
        => _contentPreProcessor.AddRequestProcessor(apiPath, preProcessor);

    /// <summary>
    /// Preprocess the response content before logging it for a given api path
    /// </summary>
    /// <param name="apiPath">absolute path, not including query string, starts with slash</param>
    /// <param name="preProcessor"></param>
    public void AddResponsePreprocessor(string apiPath, Func<string, string> preProcessor)
        => _contentPreProcessor.AddResponseProcessor(apiPath, preProcessor);

    /// <summary>
    /// Invoke
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    public async Task Invoke(HttpContext context)
    {
        var request = context.Request;

        var isLoggable = IsLoggable(request);

        RequestTraceProperties.ClearRequestTraceProperties();

        SetContextId();
        bool newTraceId = SetTraceId(request, out string reqTraceId);
        bool newTraceChain = SetTraceChain(request, out string reqChain);
        SetTraceProperties(request);

        Activity.Current?.AddBaggage("RequestTraceId", reqTraceId);
        Activity.Current?.AddBaggage("RequestTraceChain", reqChain);

        if (!isLoggable)
        {
            // useful to potentially skip other log messages
            context.Items["plinth.request-not-logged"] = null;

            await _next.Invoke(context);
            return;
        }

        LogTraceInfo(newTraceId, newTraceChain);

        string? requestContent = null;
        string reqUrl = context.Request.GetDisplayUrl();
        string? requestContentType = context.Request.ContentType;
        bool shouldLogRequestContent = ShouldLogContent(requestContentType);
        bool isFormPost = IsForm(context.Request, requestContentType);
        long requestContentLength = request.ContentLength ?? 0;

        if (!isFormPost && shouldLogRequestContent && request.Body != null && requestContentLength > 0)
        {
            var forkedRequest = new ForkingReadStream(request.Body, _opts.Value.MaxRequestBody, requestContentLength);
            request.Body = forkedRequest;
            requestContent = await forkedRequest.ReadMemoryAsString(context.RequestAborted);
        }

        _contentPreProcessor.PreProcessRequest(request.Path, ref requestContent);

        var opName = OperationFormatter.GetOperationFromRequest(context, _opts.Value.AutoCtrlCheck);

        LogRequest(request, requestContent, requestContentLength, isFormPost, shouldLogRequestContent, requestContentType, opName, reqUrl);

        HttpResponse response = context.Response;
        string? responseContent = null;
        string? responseContentType = null;
        bool shouldLogResponseContent = false;
        var forkedResponse = new ForkingWriteStream(response.Body, _opts.Value.MaxResponseBody);

        var sw = Stopwatch.StartNew();

        response.OnStarting(() =>
        {
            // note: when errors are returned, the async context is not resumed in here, so no access to AsyncLocals (only local captures)
            if (newTraceId)
                response.Headers[RequestTraceId.RequestTraceIdHeader] = new StringValues(reqTraceId);
            return Task.CompletedTask;
        });

        try
        {
            response.Body = forkedResponse;

            await _next.Invoke(context);

            responseContentType = response.ContentType;
            shouldLogResponseContent = ShouldLogContent(responseContentType);

            if (shouldLogResponseContent)
                responseContent = forkedResponse.GetMemoryAsString();

            _contentPreProcessor.PreProcessResponse(request.Path, ref responseContent);
        }
        finally
        {
            sw.Stop();
            LogResponse(response, responseContent, forkedResponse.Length, shouldLogResponseContent, responseContentType, sw.Elapsed, opName, reqUrl);
        }
    }

    private bool IsLoggable(HttpRequest request)
    {
        if (_opts.Value.DisabledMethods?.Contains(request.Method) ?? false)
            return false;

        var path = request.Path;
        foreach (var l in _loggablePaths)
            if (path.StartsWithSegments(l))
                return true;

        return false;
    }

    private static bool IsForm(HttpRequest req, string? contentType)
        => req.Body != null && contentType != null && contentType.StartsWith(ContentTypes.MultipartFormData);

    // using span/stackalloc here is ~35% faster than a string builder
    private static void LogTraceInfo(bool newTraceId, bool newTraceChain)
    {
        const string newTraceIdPreamble = "New trace id: ";
        const string newTraceChainPreamble = "New trace chain: ";

        if (!newTraceId && !newTraceChain)
            return;

        Span<char> sb = stackalloc char[128];

        int pos = 0;
        if (newTraceId)
            appendStack(ref sb, ref pos, newTraceIdPreamble, RequestTraceId.Instance.Get());

        if (newTraceChain)
        {
            if (newTraceId)
            {
                sb[pos] = ',';
                sb[pos + 1] = ' ';
                pos += 2;
            }

            appendStack(ref sb, ref pos, newTraceChainPreamble, RequestTraceChain.Instance.Get());
        }

        log.Debug(new string(sb[..pos]));

        static void appendStack(ref Span<char> sb, ref int pos, string s1, string? s2)
        {
            s1.TryCopyTo(sb[pos..]);
            pos += s1.Length;
            if (s2 != null)
            {
                s2.TryCopyTo(sb[pos..]);
                pos += s2.Length;
            }
        }
    }

    private static bool SetTraceId(HttpRequest request, out string traceId)
    {
        if (!request.Headers.TryGetValue(RequestTraceId.RequestTraceIdHeader, out var vals) ||
            vals.Count == 0 ||
            string.IsNullOrWhiteSpace(vals[0]))
        {
            traceId = RequestTraceId.Instance.Create();
            return true;
        }
        else
        {
            traceId = vals[0]!;
            RequestTraceId.Instance.Set(traceId);
            return false;
        }
    }

    private static bool SetTraceChain(HttpRequest request, out string chain)
    {
        if (!request.Headers.TryGetValue(RequestTraceChain.RequestTraceChainHeader, out var vals) ||
            vals.Count == 0 ||
            string.IsNullOrWhiteSpace(vals[0]))
        {
            chain = RequestTraceChain.Instance.Create();
            return true;
        }
        else
        {
            chain = vals[0]!;
            RequestTraceChain.Instance.Set(chain);
            return false;
        }
    }

    private static void SetTraceProperties(HttpRequest request)
    {
        if (!request.Headers.TryGetValue(RequestTraceProperties.RequestTracePropertiesHeader, out var vals) ||
            vals.Count == 0 ||
            string.IsNullOrWhiteSpace(vals[0]))
        {
            // no trace props or empty
        }
        else
        {
            try
            {
                var rProps = JsonUtil.DeserializeObject<Dictionary<string, string>>(vals[0]!);
                RequestTraceProperties.SetRequestTraceProperties(rProps);
            }
            catch (Exception e)
            {
                log.Error(e, "Error deserializing request trace properties");
            }
        }
    }

    private void LogRequest(HttpRequest r, string? requestContent, long requestContentLength, bool isFormPost, bool shouldLog, string? contentType, string operationName, string reqUrl)
    {
        var rId = RequestTraceId.Instance.Get();
        var rChain = RequestTraceChain.Instance.Get();
        var rIpAddress = GetIPAddress(r);

        // Set request trace originating IP address if one is not already set
        RequestTraceProperties.SetRequestTraceOriginatingIpAddress(rIpAddress, false);

        var logMetadata = CreateScope(
            Kvp("IpAddress", rIpAddress),
            Kvp(LoggingConstants.Field_MessageType, "RequestReceived")
        );

        using (log.BeginScope(logMetadata))
        {
            if (isFormPost)
                LogDefines.LogRequestFormPost(log, $"{rId},{rChain}", rIpAddress, operationName, r.Method, reqUrl, requestContentLength);
            else if (string.IsNullOrEmpty(requestContent))
                LogDefines.LogRequestNoContent(log, $"{rId},{rChain}", rIpAddress, operationName, r.Method, reqUrl, requestContentLength);
            else if (!shouldLog)
                LogDefines.LogRequestNoLog(log, $"{rId},{rChain}", rIpAddress, operationName, r.Method, reqUrl, contentType, requestContentLength);
            else
                LogDefines.LogRequestWithContent(log, $"{rId},{rChain}", rIpAddress, operationName, r.Method, reqUrl, contentType, requestContentLength, MaskSensitiveFields(requestContent));
        }
    }

    private void LogResponse(HttpResponse r, string? responseContent, long contentLength, bool shouldLogContent, string? contentType, TimeSpan duration, string operationName, string reqUrl)
    {
        var rId = RequestTraceId.Instance.Get();
        var rChain = RequestTraceChain.Instance.Get();
        var statusCode = r.StatusCode;
        var statusCodeStr = ((System.Net.HttpStatusCode)statusCode).ToString();
        var content = responseContent ?? string.Empty;

        var logMetadata = CreateScope(
            Kvp("RequestUri", reqUrl),
            Kvp(LoggingConstants.Field_TimeOperation, operationName),
            Kvp(LoggingConstants.Field_TimeDurationMicros, (long)(duration.TotalMilliseconds * 1000)),
            Kvp(LoggingConstants.Field_MessageType, "ResponseSent")
        );

        using (log.BeginScope(logMetadata))
        {
            if (shouldLogContent)
                LogDefines.LogResponseWithContent(log, $"{rId},{rChain}", duration.ToString(), statusCode, statusCodeStr, contentType, contentLength, MaskSensitiveFields(content));
            else
                LogDefines.LogResponseNoContent(log, $"{rId},{rChain}", duration.ToString(), statusCode, statusCodeStr, contentType, contentLength);
        }
    }

    private string? MaskSensitiveFields(string? s)
    {
        var result = s.MaskSensitiveFields();

        if (_sensitiveDataMasker != null)
            result = _sensitiveDataMasker.MaskSensitiveFields(result);

        return result;
    }

    /// <summary>
    /// Utility function which determines if content is suitable for logging
    /// </summary>
    private static bool ShouldLogContent(string? contentType)
    {
        if (contentType == null)
            return false;

        // log any text/ or anything that is xml
        if (contentType.StartsWith("text/") || contentType.Contains("xml"))
            return true;

        // check if content type starts with any of the loggable types
        // starts with is used because some content types come back like "application/json; charset=utf-8"
        foreach (var loggable in _loggableContentTypes)
        {
            if (contentType.StartsWith(loggable))
                return true;
        }

        return false;
    }

    /// <summary>
    /// Tries to get client IP address for logging purposes
    /// </summary>
    private static string GetIPAddress(HttpRequest request)
    {
        var ip = request.HttpContext.Connection?.RemoteIpAddress;
        if (ip != null)
            return ip.ToString();

        // some value representing local connections
        return "::0";
    }

    private static partial class LogDefines
    {
        [LoggerMessage(0, LogLevel.Information,
            "<== [{TraceString}] Received form request from {IpAddress}: {" + LoggingConstants.Field_TimeOperation + "}, {HttpMethod} {RequestUri}, Length: {ContentLength}",
            EventName = "RequestFormPost")]
        public static partial void LogRequestFormPost(ILogger logger, string? TraceString, string? ipAddress, string timeOperation, string httpMethod, string? requestUri, long contentLength);

        [LoggerMessage(1, LogLevel.Information,
            "<== [{TraceString}] Received request from {IpAddress}: {" + LoggingConstants.Field_TimeOperation + "}, {HttpMethod} {RequestUri}, Type: {ContentType}, Length: {ContentLength}",
            EventName = "RequestNoLog")]
        public static partial void LogRequestNoLog(ILogger logger, string traceString, string? ipAddress, string? timeOperation, string? httpMethod, string? requestUri, string? contentType, long contentLength);

        [LoggerMessage(2, LogLevel.Information,
            "<== [{TraceString}] Received request from {IpAddress}: {" + LoggingConstants.Field_TimeOperation + "}, {HttpMethod} {RequestUri}, Length: {ContentLength}, No Content",
            EventName = "RequestNoContent")]
        public static partial void LogRequestNoContent(ILogger logger, string traceString, string? ipAddress, string? timeOperation, string? httpMethod, string? requestUri, long contentLength);

        [LoggerMessage(3, LogLevel.Information,
            "<== [{TraceString}] Received request from {IpAddress}: {" + LoggingConstants.Field_TimeOperation + "}, {HttpMethod} {RequestUri}, Type: {ContentType}, Length: {ContentLength}, Content {Content}",
            EventName = "RequestWithContent")]
        public static partial void LogRequestWithContent(ILogger logger, string traceString, string? ipAddress, string? timeOperation, string? httpMethod, string? requestUri, string? contentType, long contentLEngth, string? content);

        [LoggerMessage(4, LogLevel.Information,
            "==> [{TraceString}] Sending response: Took: {" + LoggingConstants.Field_TimeDuration + "}, Status Code: {HttpStatusCode}/{HttpStatus}, Type: {ContentType}, Length: {ContentLength}",
            EventName = "ResponseNoContent")]
        public static partial void LogResponseNoContent(ILogger logger, string traceString, string timeDuration, int httpStatusCode, string httpStatus, string? contentType, long contentLength);

        [LoggerMessage(5, LogLevel.Information,
            "==> [{TraceString}] Sending response: Took: {" + LoggingConstants.Field_TimeDuration + "}, Status Code: {HttpStatusCode}/{HttpStatus}, Type: {ContentType}, Length: {ContentLength}, Content: {Content}",
            EventName = "ResponseWithContent")]
        public static partial void LogResponseWithContent(ILogger logger, string? TraceString, string? timeDuration, int httpStatusCode, string httpStatus, string? contentType, long contentLength, string? content);
    }
}
