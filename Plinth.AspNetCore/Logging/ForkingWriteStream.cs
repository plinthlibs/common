using System.Text;

namespace Plinth.AspNetCore.Logging;

/// <summary>
/// Allows us to intercept the response stream without having to copy it around
/// </summary>
class ForkingWriteStream : Stream
{
    private readonly Stream _inner;
    private readonly int _max;
    private readonly MemoryStream _mem;

    private long _bytesWritten = 0;

    public ForkingWriteStream(Stream innerStream, int maxMemoryBytes, int sizeGuess = 512)
    {
        _inner = innerStream;
        _max = maxMemoryBytes;
        _mem = new MemoryStream(sizeGuess);
    }

    public override bool CanRead => false;

    public override bool CanSeek => false;

    public override bool CanWrite => _inner.CanWrite;

    public override long Length => _bytesWritten;

    public override long Position { get => _inner.Position; set => throw new NotSupportedException(); }

    public override void Flush() => _inner.Flush();

    public override Task FlushAsync(CancellationToken cancellationToken) => _inner.FlushAsync(cancellationToken);

    public override int Read(byte[] buffer, int offset, int count) => throw new NotSupportedException();

    public override long Seek(long offset, SeekOrigin origin) => throw new NotSupportedException();

    public override void SetLength(long value) => throw new NotSupportedException();

    public override void Write(byte[] buffer, int offset, int count)
    {
        _inner.Write(buffer, offset, count);
        _bytesWritten += count;
        if (_mem.Length < _max)
            _mem.Write(buffer, offset, (int)Math.Min(_max - _mem.Length, count));
    }

    public override async ValueTask WriteAsync(ReadOnlyMemory<byte> buffer, CancellationToken cancellationToken = default)
    {
        await _inner.WriteAsync(buffer, cancellationToken);
        _bytesWritten += buffer.Length;
        if (_mem.Length < _max)
            _mem.Write(buffer[..(int)Math.Min(_max - _mem.Length, buffer.Length)].Span);
    }

    public string GetMemoryAsString()
    {
        _mem.Position = 0;
        using var r = new StreamReader(_mem, Encoding.UTF8, leaveOpen: true);
        var s = r.ReadToEnd();
#if NET8_0_OR_GREATER
        // net7.0 and before had a bug where leftover bytes of an incomplete character would be dropped
        // this code used to depend on this behavior, but they fixed it in net8.0
        // https://github.com/dotnet/runtime/pull/69888
        // this check removes the garbage at the end
        if (s.Length > 0 && s[^1] == '\uFFFD')
            return s.TrimEnd('\uFFFD');
#endif
        return s;
    }
}
