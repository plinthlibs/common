using System.Text;

namespace Plinth.AspNetCore.Logging;

/// <summary>
/// Allows us to intercept the request stream and keep some of it without have to copy all of it
/// </summary>
class ForkingReadStream : Stream
{
    private readonly Stream _inner;
    private readonly MemoryStream _mem;
    private readonly int _memSize;

    public ForkingReadStream(Stream innerStream, int maxMemoryBytes, long size)
    {
        _inner = innerStream;
        _memSize = (int)Math.Min(maxMemoryBytes, size);
        _mem = new MemoryStream(_memSize);
    }

    public override bool CanRead => _inner.CanRead;

    public override bool CanSeek => false;

    public override bool CanWrite => false;

    public override long Length => 0;

    public override long Position { get => _inner.Position; set => throw new NotSupportedException(); }

    public override void Flush() => throw new NotSupportedException();

    public override int Read(byte[] buffer, int offset, int count)
    {
        int memBytes = _mem.Read(buffer, offset, count);
        if (memBytes == count)
            return memBytes;

        return memBytes + _inner.Read(buffer, offset + memBytes, count - memBytes);
    }

    public override async ValueTask<int> ReadAsync(Memory<byte> buffer, CancellationToken cancellationToken = default)
    {
        // when reading, we exhaust the memory stream first
        int memBytes = _mem.Read(buffer.Span);
        if (memBytes == buffer.Length)
            return memBytes;

        // then we read from the inner stream
        return memBytes + await _inner.ReadAsync(buffer[memBytes..], cancellationToken);
    }

    public override long Seek(long offset, SeekOrigin origin) => throw new NotSupportedException();

    public override void SetLength(long value) => throw new NotSupportedException();

    public override void Write(byte[] buffer, int offset, int count) => throw new NotSupportedException();

    public override ValueTask WriteAsync(ReadOnlyMemory<byte> buffer, CancellationToken cancellationToken = default) => throw new NotSupportedException();

    public async Task<string> ReadMemoryAsString(CancellationToken cancellationToken = default)
    {
        // step 1, read up to max into memory stream
        byte[] buffer = new byte[Math.Min(32768, _memSize)];
        do
        {
            int bytesRead = await _inner.ReadAsync(buffer.AsMemory(0, (int)Math.Min(buffer.Length, _memSize - _mem.Length)), cancellationToken);
            if (bytesRead == 0)
                break;
            _mem.Write(buffer, 0, bytesRead);
        }
        while (_mem.Length < _memSize);

        // step 2, read memory stream into string
        _mem.Position = 0;
        using var r = new StreamReader(_mem, Encoding.UTF8, true, 1024, leaveOpen: true);
        var s = r.ReadToEnd();
        _mem.Position = 0;
#if NET8_0_OR_GREATER
        // net7.0 and before had a bug where leftover bytes of an incomplete character would be dropped
        // this code used to depend on this behavior, but they fixed it in net8.0
        // https://github.com/dotnet/runtime/pull/69888
        // this check removes the garbage at the end
        if (s.Length > 0 && s[^1] == '\uFFFD')
            return s.TrimEnd('\uFFFD');
#endif
        return s;
    }
}
