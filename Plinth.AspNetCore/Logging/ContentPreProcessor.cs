namespace Plinth.AspNetCore.Logging;

internal class ContentPreProcessor
{
    private Dictionary<string, Func<string, string>> _requestMap = [];
    private Dictionary<string, Func<string, string>> _responseMap = [];

    public ContentPreProcessor()
    {
    }

    public void AddRequestProcessor(string requestUriPath, Func<string, string> func)
        => AddProcessor(requestUriPath, func, ref _requestMap);

    public void AddResponseProcessor(string requestUriPath, Func<string, string> func)
        => AddProcessor(requestUriPath, func, ref _responseMap);

    private static void AddProcessor(string uri, Func<string, string> func, ref Dictionary<string, Func<string, string>> dict)
    {
        if (string.IsNullOrEmpty(uri)) throw new ArgumentNullException(nameof(uri));
        if (uri[0] != '/') throw new ArgumentException("uri must start with /", nameof(uri));
        ArgumentNullException.ThrowIfNull(func);
        dict ??= [];
        dict.Add(uri, func);
    }

    public bool PreProcessRequest(string requestUriPath, ref string? content)
        => PreProcess(requestUriPath, ref content, _requestMap);

    public bool PreProcessResponse(string requestUriPath, ref string? content)
        => PreProcess(requestUriPath, ref content, _responseMap);

    private static bool PreProcess(string uri, ref string? content, Dictionary<string, Func<string, string>>? dict)
    {
        if (content != null && dict != null && dict.TryGetValue(uri, out var func))
        {
            content = func.Invoke(content);
            return true;
        }
        return false;
    }
}
