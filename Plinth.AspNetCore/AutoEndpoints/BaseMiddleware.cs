using System.Text;
using System.Text.RegularExpressions;
using Plinth.Common.Constants;
using Plinth.Serialization;
using Plinth.HttpApiClient.Response;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Plinth.AspNetCore.AutoEndpoints.Shared;
using System.Diagnostics.CodeAnalysis;

namespace Plinth.AspNetCore.AutoEndpoints;

/// <summary>
/// Used by internal logging classes
/// </summary>
// must be public because it is used by LoggingMiddleware which must be public
public class AutoEndpointsHandlerCheck
{
    private readonly List<Func<HttpContext, string?>> _handlers;

    internal AutoEndpointsHandlerCheck(List<Func<HttpContext, string?>> funcs)
    {
        _handlers = funcs;
    }

    internal bool WillHandle(HttpContext httpContext, [MaybeNullWhen(false)] out string opName)
    {
        foreach (var ac in _handlers)
        {
            opName = ac.Invoke(httpContext);
            if (opName != null)
                return true;
        }

        opName = null;
        return false;
    }
}

/// <summary>
/// Base middleware for Auto Controllers
/// </summary>
internal abstract class BaseMiddleware
{
    protected const string HandleMatchKey = "Plinth-AutoEndpoints-HandleMatchKey";

    internal abstract Formatting GetFormatting();

    private static void ClearResponse(HttpContext context, string? contentType, int statusCode)
    {
        // copy response headers.  Main use case is HSTS
        var headers = context.Response.Headers.ToDictionary(v => v.Key, v => v.Value);

        context.Response.Clear();

        foreach (var h in headers)
            context.Response.Headers.Add(h);

        context.Response.StatusCode = statusCode;
        context.Response.ContentType = contentType ?? ContentTypes.ApplicationJsonUtf8;
    }

    protected async Task WriteResponse(HttpContext context, object? responseModel, int statusCode = StatusCodes.Status200OK)
    {
        ClearResponse(context, null, statusCode);

        string responseContent = responseModel == null ? string.Empty : JsonUtil.SerializeObject(responseModel, m => m.Formatting = GetFormatting());

        await context.Response.WriteAsync(responseContent, Encoding.UTF8);
    }

    protected async Task WriteNotImplemented(HttpContext context, string message, string? propertyName = null)
    {
        ClearResponse(context, null, StatusCodes.Status501NotImplemented);

        string responseContent = JsonUtil.SerializeObject(
                        new ApiResponse(new ApiError(ApiErrorCode.ValidationError, message, propertyName)),
                        m => m.Formatting = GetFormatting());

        await context.Response.WriteAsync(responseContent, Encoding.UTF8);
    }

    protected async Task WriteForbidden(HttpContext context, string message)
    {
        ClearResponse(context, null, StatusCodes.Status403Forbidden);

        string responseContent = JsonUtil.SerializeObject(
                        new ApiResponse(new ApiError(ApiErrorCode.ValidationError, message, null)),
                        m => m.Formatting = GetFormatting());

        await context.Response.WriteAsync(responseContent, Encoding.UTF8);
    }

    protected async Task WriteError(HttpContext context, Exception ex, int statusCode = StatusCodes.Status500InternalServerError)
    {
        ClearResponse(context, null, statusCode);

        string responseContent = JsonUtil.SerializeObject(ex, m => m.Formatting = GetFormatting());

        await context.Response.WriteAsync(responseContent, Encoding.UTF8);
    }

    protected static async Task WriteError(HttpContext context, string message, int statusCode = StatusCodes.Status500InternalServerError)
    {
        ClearResponse(context, null, statusCode);

        await context.Response.WriteAsync(message, Encoding.UTF8);
    }

    protected static bool ShouldHandle(HttpContext httpContext, string key, Regex pathRegex, string method, [MaybeNullWhen(false)] out Match match)
    {
        match = null;
        if (httpContext.Request.Method != method)
            return false;

        var path = httpContext.Request.Path.Value ?? string.Empty;

        match = pathRegex.Match(path);

        if (match.Success)
        {
            httpContext.Items[HandleMatchKey + key] = match;
            return true;
        }

        return false;
    }

    protected static bool GetStoredMatch(HttpContext httpContext, string key, Regex pathRegex, string method, [MaybeNullWhen(false)] out Match match)
    {
        if (httpContext.Items.TryGetValue(HandleMatchKey + key, out var value))
        {
            match = value as Match;
            if (match?.Success ?? false)
                return true;
        }
        else
        {
            if (ShouldHandle(httpContext, key, pathRegex, method, out match) && match.Success)
                return true;
        }

        return false;
    }

    protected static void CheckIsInternal(HttpContext context, BaseConfig config)
    {
        if (config.AllowExternal)
            return;

        var ip = context.Connection.RemoteIpAddress;

        if (ip == null || !IPUtils.IsInternal(ip))
            throw new NotSupportedException($"access denied to non-internal IP: {ip}");
    }
}
