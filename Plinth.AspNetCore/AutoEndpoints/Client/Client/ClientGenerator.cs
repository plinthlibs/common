using System.ComponentModel.DataAnnotations;
using System.Text;
using Microsoft.Extensions.Logging;
using Plinth.AspNetCore.AutoEndpoints.Client.Models;
using Plinth.Common.Exceptions;
using Plinth.Serialization;

namespace Plinth.AspNetCore.AutoEndpoints.Client;

internal class ClientGenerator(ClientBaseConfig config)
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private static void Validate(object o)
    {
        var context = new ValidationContext(o, serviceProvider: null, items: null);
        var results = new List<ValidationResult>();
        if (! Validator.TryValidateObject(o, context, results, validateAllProperties: true))
            throw new LogicalBadRequestException(results);
    }

    public void Log(string body)
    {
        if (JsonUtil.TryDeserializeObject<List<ClientLogRequest>>(body, out var list))
        {
            foreach (var e in list)
                LogEntry(e);
            return;
        }

        if (JsonUtil.TryDeserializeObject<ClientLogRequest>(body, out var entry))
            LogEntry(entry);

        if (entry == null)
            throw new LogicalBadRequestException("could not deserialize body");
    }

    private void LogEntry(ClientLogRequest msg)
    {
        Validate(msg);

        switch (msg.Level)
        {
            case ClientLogRequest.LogLevel.Trace: log.Trace(formMsg(msg)); break;
            case ClientLogRequest.LogLevel.Debug: log.Debug(formMsg(msg)); break;
                
            case ClientLogRequest.LogLevel.Info:
            case ClientLogRequest.LogLevel.Information: log.Info(formMsg(msg)); break;

            case ClientLogRequest.LogLevel.Warn:
            case ClientLogRequest.LogLevel.Warning: log.Warn(formMsg(msg)); break;

            case ClientLogRequest.LogLevel.Error: log.Error(formMsg(msg)); break;

            case ClientLogRequest.LogLevel.Fatal:
            case ClientLogRequest.LogLevel.Critical: log.Fatal(formMsg(msg)); break;
            default:
                throw new LogicalBadRequestException($"Level '{msg.Level}' not supported");
        }

        string formMsg(ClientLogRequest m)
        {
            var sb = new StringBuilder(m.Class?.Length ?? 0 + m.Message?.Length ?? 0 + m.Exception?.Length ?? 0 + 20);
            sb.Append(config.LoggingPrefix).Append(": ");

            if (m.EventTime.HasValue)
                sb.Append(m.EventTime.Value.ToString("O")).Append(": ");

            if (!string.IsNullOrEmpty(m.Class))
                sb.Append("class ").Append(m.Class).Append(": ");

            sb.Append(m.Message);

            if (!string.IsNullOrEmpty(m.Exception))
                sb.Append(Environment.NewLine).Append(m.Exception);

            return sb.ToString();
        }
    }
}
