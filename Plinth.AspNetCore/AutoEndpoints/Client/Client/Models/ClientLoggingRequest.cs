﻿using System.ComponentModel.DataAnnotations;

namespace Plinth.AspNetCore.AutoEndpoints.Client.Models;

/// <summary>
/// Request model for a client/log post
/// </summary>
public class ClientLogRequest
{
    /// <summary>
    /// Log Level
    /// </summary>
    public enum LogLevel
    {
        /// <summary>Info</summary>
        Info,
        /// <summary>Warn, alias for Warning</summary>
        Warn,
        /// <summary>Fatal, alias for Critical</summary>
        Fatal,
        /// <summary>Trace</summary>
        Trace,
        /// <summary>Debug</summary>
        Debug,
        /// <summary>Information</summary>
        Information,
        /// <summary>Warning</summary>
        Warning,
        /// <summary>Error</summary>
        Error,
        /// <summary>Critical</summary>
        Critical,
        /// <summary>None</summary>
        None
    }

    /// <summary>Optional time that the event occurred</summary>
    public DateTime? EventTime { get; set; }

    /// <summary>Only Trace, Debug, Info, Warn, Error, Fatal supported</summary>
    [Required]
    public LogLevel? Level { get; set; }

    /// <summary>Optional class name</summary>
    public string? Class { get; set; }

    /// <summary>Message content</summary>
    [Required] public string Message { get; set; } = null!;

    /// <summary>Optional exception stack trace</summary>
    public string? Exception { get; set; }
}
