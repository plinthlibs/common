﻿using Plinth.AspNetCore.AutoEndpoints.Shared;

namespace Plinth.AspNetCore.AutoEndpoints.Client;

/// <summary>
/// Configuration Class for Client Auto Controller
/// </summary>
public class ClientBaseConfig : BaseConfig
{
    internal string? ComponentName { get; set; }

    /// <summary>
    /// Set to true to enable the Logging client endpoint (on by default)
    /// </summary>
    public bool LoggingEnabled { get; set; } = true;

    /// <summary>
    /// Customize the prefix on client log messages
    /// </summary>
    public string LoggingPrefix { get; set; } = "WEBAPP";

    /// <summary>
    /// Turn on/off request/response logging for client log (off by default)
    /// </summary>
    public bool LoggingLogWebRequests { get; set; } = false;

    /// <summary>
    /// Constructor for Client Config
    /// </summary>
    public ClientBaseConfig()
    {
        ComponentName = null;
    }
}
