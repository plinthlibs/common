using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Plinth.HttpApiClient.Response;
using Plinth.Common.Exceptions;
using Plinth.Serialization;

namespace Plinth.AspNetCore.AutoEndpoints.Client;

/// <summary>
/// Middleware for Version Auto Controller
/// </summary>
internal class ClientMiddleware : BaseMiddleware
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private readonly RequestDelegate _next;
    private readonly ClientConfig _config;
    private readonly Regex _pathRegex;

    private static Regex PathRegex(string urlPrefix) =>
        new(@$"^/client/{urlPrefix}/(log)(?:/([^/]*))?$", RegexOptions.Compiled);

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="next"></param>
    /// <param name="config"></param>
    public ClientMiddleware(RequestDelegate next, ClientConfig config)
    {
        _config = config;
        _next = next;
        _pathRegex = PathRegex(config.UrlPrefix);
    }

    public static string? WillHandle(HttpContext httpContext, ClientConfig config)
    {
        if (ShouldHandle(httpContext, nameof(ClientMiddleware), PathRegex(config.UrlPrefix), HttpMethods.Get, out var match))
            return $"AutoEndpoint::{config.LoggingOpName}";

        return null;
    }

    public async Task Invoke(HttpContext httpContext)
    {
        if (!GetStoredMatch(httpContext, nameof(ClientMiddleware), _pathRegex, HttpMethods.Post, out var match))
        {
            await _next(httpContext);
            return;
        }

        var gen = new ClientGenerator(_config);

        try
        {
            var apiVersion = match.Groups[2].Value;
            switch (apiVersion)
            {
                case null:
                case "":
                case "v1":

                    switch (match.Groups[1].Value)
                    {
                        case "log":
                            using (var reader = new StreamReader(httpContext.Request.Body, Encoding.UTF8, true))
                            {
                                gen.Log(await reader.ReadToEndAsync());
                            }
                            await WriteResponse(httpContext, null, statusCode: StatusCodes.Status202Accepted);
                            break;

                        default:
                            await WriteNotImplemented(httpContext, $"sub-controller '{match.Groups[1].Value}' does not exist");
                            break;
                    }
                    break;

                default:
                    await WriteNotImplemented(httpContext, $"apiversion '{apiVersion}' does not exist");
                    break;
            }
        }
        catch (LogicalBadRequestException e)
        {
            log.Warn(e, "bad request");

            if (e.CustomData is IEnumerable<ValidationResult> list)
            {
                var response = new ApiResponse(
                   list
                       .Select(er => new ApiError(ApiErrorCode.ValidationError, er.ErrorMessage, string.Join(",", er.MemberNames)))
                       .GroupBy(er => new { er.ErrorCode, er.ErrorDescription, er.PropertyName })
                       .Select(g => g.First())
                );

                await WriteError(httpContext, JsonUtil.SerializeObject(response), StatusCodes.Status400BadRequest);
            }
            else
                await WriteError(httpContext, JsonUtil.SerializeObject(new { e.Message }), StatusCodes.Status400BadRequest);
        }
        catch (Exception ex)
        {
            log.Error(ex, "Exception during version handler");
            await WriteError(httpContext, ex, StatusCodes.Status404NotFound);
        }
    }

    internal override Formatting GetFormatting()
    {
        return _config.GetFormatting();
    }
}
