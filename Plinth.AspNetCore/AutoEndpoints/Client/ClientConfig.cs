﻿namespace Plinth.AspNetCore.AutoEndpoints.Client;

/// <summary>
/// Configuration Class for Client Auto Controller
/// </summary>
public class ClientConfig : ClientBaseConfig
{
    private string _urlPrefix = "webapp";

    /// <summary>
    /// Customize the prefix for url path
    /// </summary>
    public string UrlPrefix
    {
        get
        {
            return _urlPrefix;
        }
        set
        {
            _urlPrefix = value.TrimStart('/').TrimEnd('/');
        }
    }

    /// <summary>
    /// Customize the endpoint operation name that appears in the logs
    /// </summary>
    public string LoggingOpName { get; set; } = "WebApp";

    internal string LoggingUrlPrefix => $"/client/{UrlPrefix}";

    /// <summary>
    /// Constructor for Client Config
    /// </summary>
    public ClientConfig()
    {
    }
}
