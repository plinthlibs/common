using System.Reflection;
using Plinth.AspNetCore.AutoEndpoints.Shared;
using Plinth.AspNetCore.AutoEndpoints.Version.Models;

namespace Plinth.AspNetCore.AutoEndpoints.Version;

/// <summary>
/// Configuration Class for Version Auto Controller
/// </summary>
public class VersionBaseConfig : BaseConfig
{
    internal string? ComponentName { get; set; }
    internal Func<Assembly>? AssemblyResolver { get; set; }
    internal Func<string>? DbVersionResolver { get; set; }
    internal VersionField DisabledFields { get; set; }

    internal VersionBaseConfig(Assembly callingAssembly) : this()
    {
        AssemblyResolver = () => callingAssembly;
    }

    /// <summary>
    /// Constructor for Version Config
    /// </summary>
    public VersionBaseConfig()
    {
        AssemblyResolver = null;
        DbVersionResolver = null;
        ComponentName = null;
        DisabledFields = VersionField.None;
    }

    /// <summary>
    /// Function to resolve assembly to pull version from
    /// </summary>
    /// <param name="assemblyResolver"></param>
    public void SetAssemblyResolver(Func<Assembly> assemblyResolver)
    {
        AssemblyResolver = assemblyResolver;
    }

    /// <summary>
    /// Function to resolve the db version
    /// </summary>
    /// <param name="dbVersionResolver"></param>
    public void SetDbVersionResolver(Func<string> dbVersionResolver)
    {
        DbVersionResolver = dbVersionResolver;
    }

    /// <summary>
    /// Function to configure the disabled version fields
    /// </summary>
    /// <param name="disabledFields"></param>
    public void SetDisabledFields(VersionField disabledFields)
    {
        DisabledFields = disabledFields;
    }
}
