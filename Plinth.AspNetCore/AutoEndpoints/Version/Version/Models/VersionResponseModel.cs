using Newtonsoft.Json;

namespace Plinth.AspNetCore.AutoEndpoints.Version.Models;

/// <summary>
/// Response model for version auto controller endpoint
/// </summary>
public class VersionResponseModel
{
    /// <summary>
    /// String value used in case of error.
    /// </summary>
    public const string Error = "*** ERROR ***";

    /// <summary>
    /// String value used in case of error.
    /// </summary>
    public const string NotSet = "*** NOT SET ***";

    /// <summary>
    /// String value used in case of error.
    /// </summary>
    public const string Missing = "*** MISSING ***";

    /// <summary>
    /// The environment
    /// </summary>
    [JsonProperty("environment", Order = -110)]
    public string? Environment { get; set; }

    /// <summary>
    /// Gets or sets component name.
    /// </summary>
    [JsonProperty("component", Order = -110)]
    public string? Component { get; set; }

    /// <summary>
    /// Gets or sets primary assembly name.
    /// </summary>
    [JsonProperty("assembly", Order = -109)]
    public string? Assembly { get; set; }

    /// <summary>
    /// Gets or sets primary assembly version.
    /// </summary>
    [JsonProperty("version", Order = -108)]
    public string? Version { get; set; }

    /// <summary>
    /// Gets or sets primary assembly informational version.
    /// </summary>
    [JsonProperty("release", Order = -107)]
    public string? Release { get; set; }

    /// <summary>
    /// Gets or sets primary assembly build date
    /// </summary>
    [JsonProperty("built", Order = -106)]
    public DateTime? BuildDate { get; set; }

    /// <summary>
    /// Gets or sets machine name.
    /// </summary>
    [JsonProperty("machine", Order = -105)]
    public string? MachineName { get; set; }

    /// <summary>
    /// Gets or sets machine name.
    /// </summary>
    [JsonProperty("machineUpTime", Order = -105)]
    public TimeSpan? MachineUpTime { get; set; }

    /// <summary>
    /// Gets or sets process name.
    /// </summary>
    [JsonProperty("process", Order = -105)]
    public string? ProcessName { get; set; }

    /// <summary>
    /// Gets or sets process name.
    /// </summary>
    [JsonProperty("processUpTime", Order = -105)]
    public TimeSpan? ProcessUpTime { get; set; }

    /// <summary>
    /// The .net runtime for the endpoint
    /// </summary>
    [JsonProperty("runtime", Order = -104)]
    public string? Runtime { get; set; }

    /// <summary>
    /// The .net core runtime version (e.g. 2.2.7)
    /// </summary>
    [JsonProperty("netcore", Order = -103)]
    public string? NetCore { get; set; }

    /// <summary>
    /// Gets or sets port number for the incoming request.
    /// </summary>
    [JsonProperty("host", Order = -102)]
    public string? RequestHost { get; set; }

    /// <summary>
    /// Gets or sets port number for the incoming request.
    /// </summary>
    [JsonProperty("port", Order = -101)]
    public int? RequestPort { get; set; }

    /// <summary>
    /// Gets or sets DB version used by this application.
    /// </summary>
    [JsonProperty("db", Order = -100)]
    public string? DbVer { get; set; }
}
