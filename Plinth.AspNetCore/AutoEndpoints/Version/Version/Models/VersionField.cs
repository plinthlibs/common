namespace Plinth.AspNetCore.AutoEndpoints.Version.Models;

/// <summary>
/// Blacklisting flags for elements returned by version generator
/// </summary>
[Flags]
public enum VersionField
{
    /// <summary>None</summary>
    None = 0,
    /// <summary>Environment</summary>
    Environment = 1 << 0,
    /// <summary>Component</summary>
    Component = 1 << 1,
    /// <summary>Assembly</summary>
    Assembly = 1 << 2,
    /// <summary>Version</summary>
    Version = 1 << 3,
    /// <summary>Release</summary>
    Release = 1 << 4,
    /// <summary>BuildDate</summary>
    BuildDate = 1 << 5,
    /// <summary>MachineName</summary>
    MachineName = 1 << 6,
    /// <summary>MachineUpTime</summary>
    MachineUpTime = 1 << 7,
    /// <summary>ProcessName</summary>
    ProcessName = 1 << 8,
    /// <summary>ProcessUpTime</summary>
    ProcessUpTime = 1 << 9,
    /// <summary>RequestHost</summary>
    RequestHost = 1 << 10,
    /// <summary>RequestPort</summary>
    RequestPort = 1 << 11,
    /// <summary>Runtime</summary>
    Runtime = 1 << 12,
    /// <summary>NetCore</summary>
    NetCore = 1 << 13,
    /// <summary>DbVer</summary>
    DbVer = 1 << 14,
}
