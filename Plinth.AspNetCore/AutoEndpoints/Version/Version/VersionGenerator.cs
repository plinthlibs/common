using System.Diagnostics;
using System.Reflection;
using Microsoft.Extensions.Logging;
using Plinth.AspNetCore.AutoEndpoints.Shared;
using Plinth.AspNetCore.AutoEndpoints.Version.Models;

namespace Plinth.AspNetCore.AutoEndpoints.Version;

internal class VersionGenerator
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private readonly VersionBaseConfig _config;
    private readonly string _version, _release, _assembly;
    private readonly DateTime? _buildDate;
    private readonly string? _processName;

    public VersionGenerator(VersionBaseConfig config)
    {
        _config = config;

        var assembly = config.AssemblyResolver?.Invoke();

        if (assembly != null)
        {
            _assembly = assembly.GetName().Name ?? VersionResponseModel.Missing;

            // could use this too FileVersionInfo.GetVersionInfo(assembly.Location).ProductVersion;

            var ctime = File.GetCreationTimeUtc(assembly.Location);
            var mtime = File.GetLastWriteTimeUtc(assembly.Location);
            _buildDate = ctime > mtime ? ctime : mtime;

            var fileVer = assembly.GetCustomAttribute<AssemblyFileVersionAttribute>();
            _version = fileVer?.Version ?? assembly.GetName().Version?.ToString() ?? VersionResponseModel.Missing;

            var info = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>();
            if (info != null)
            {
                _release = info.InformationalVersion;
            }
            else
            {
                _release = VersionResponseModel.Missing;
                log.Warn("AssemblyInformationalVersion is missing.");
            }

            try
            {
                _processName = Process.GetCurrentProcess().MainModule?.ModuleName ?? VersionResponseModel.Missing;
            }
            catch (Exception e)
            {
                log.Info(e, "Could not get process name");
                _processName = VersionResponseModel.Error;
            }
        }
        else
        {
            _release = VersionResponseModel.NotSet;
            _assembly = VersionResponseModel.NotSet;
            _version = VersionResponseModel.NotSet;
            _buildDate = null;
            _processName = null;
            log.Warn("AssemblyResolver is not set.");
        }
    }

    public VersionResponseModel GetVersionResponse(Uri requestUri)
    {
        TimeSpan? processTime;
        try
        {
            processTime = DateTime.UtcNow - Process.GetCurrentProcess().StartTime.ToUniversalTime();
            processTime = TimeSpan.FromSeconds((long)processTime.Value.TotalSeconds);
        }
        catch (Exception e)
        {
            log.Info(e, "Could not get process time");
            processTime = null;
        }

        var resp = new VersionResponseModel
        {
            Environment = GetValueOrNull(VersionField.Environment, EnvironmentUtility.SafeGetEnvironmentVariable("ASPNETCORE_ENVIRONMENT").Value),
            Component = GetValueOrNull(VersionField.Component, _config.ComponentName),
            MachineName = GetValueOrNull(VersionField.MachineName, Environment.MachineName),
            MachineUpTime = GetValueOrNull(VersionField.MachineUpTime, (TimeSpan?)TimeSpan.FromSeconds(Stopwatch.GetTimestamp() / (double)Stopwatch.Frequency)),
            ProcessName = GetValueOrNull(VersionField.ProcessName, _processName),
            ProcessUpTime = GetValueOrNull(VersionField.ProcessUpTime, processTime),
            RequestHost = GetValueOrNull(VersionField.RequestHost, requestUri.Host),
            RequestPort = GetValueOrNull(VersionField.RequestPort, (int?)requestUri.Port),
            Runtime = GetValueOrNull(VersionField.Runtime, RuntimeUtils.GetNetRuntime()),
            NetCore = GetValueOrNull(VersionField.NetCore, RuntimeUtils.GetNetCoreVersion()),
            Release = GetValueOrNull(VersionField.Release, _release),
            Assembly = GetValueOrNull(VersionField.Assembly, _assembly),
            Version = GetValueOrNull(VersionField.Version, _version),
            BuildDate = GetValueOrNull(VersionField.BuildDate, _buildDate),
        };

        try
        {
            if (_config.DbVersionResolver != null)
                resp.DbVer = _config.DbVersionResolver.Invoke() ?? VersionResponseModel.Missing;
            if (_config.DisabledFields.HasFlag(VersionField.DbVer))
                resp.DbVer = null;
        }
        catch (Exception e)
        {
            resp.DbVer = VersionResponseModel.Error;
            log.Warn(e, "Failed to get DB version.");
        }

        return resp;
    }

    #region // HELPERS
    private T? GetValueOrNull<T>(VersionField flg, T? self) => _config.DisabledFields.HasFlag(flg) ? default : self;
    #endregion
}
