using System.Text.RegularExpressions;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace Plinth.AspNetCore.AutoEndpoints.Version;

/// <summary>
/// Middleware for Version Auto Controller
/// </summary>
internal partial class VersionMiddleware : BaseMiddleware
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

#if NET8_0_OR_GREATER
    [GeneratedRegex("^/version(?:/([^/]*))?$")]
    private static partial Regex PathRegex();
#else
    private static readonly Regex _pathRegex = new(@"^/version(?:/([^/]*))?$", RegexOptions.Compiled);
    private static Regex PathRegex() => _pathRegex;
#endif

    private readonly VersionConfig _config;
    private readonly RequestDelegate _next;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="next"></param>
    /// <param name="config"></param>
    public VersionMiddleware(RequestDelegate next, VersionConfig config)
    {
        _config = config;
        _next = next;
    }

    public static string? WillHandle(HttpContext httpContext)
    {
        if (ShouldHandle(httpContext, nameof(VersionMiddleware), PathRegex(), HttpMethods.Get, out var match))
            return "AutoEndpoint::Version";

        return null;
    }

    public async Task Invoke(HttpContext httpContext, IServiceProvider serviceProvider)
    {
        if (!GetStoredMatch(httpContext, nameof(VersionMiddleware), PathRegex(), HttpMethods.Get, out var match))
        {
            await _next(httpContext);
            return;
        }

        if (_config.ServiceDbVersionResolver != null)
        {
            _config.DbVersionResolver = resolver;
        }

        var gen = new VersionGenerator(_config);

        try
        {
            var apiVersion = match.Groups[1].Value;
            switch (apiVersion)
            {
                case null:
                case "":
                case "v1":
                    var responseModel = gen.GetVersionResponse(new Uri(httpContext.Request.GetDisplayUrl()));
                    await WriteResponse(httpContext, responseModel);
                    break;
                default:
                    await WriteNotImplemented(httpContext, $"apiversion '{apiVersion}' does not exist");
                    break;
            }
        }
        catch (Exception ex)
        {
            log.Error(ex, "Exception during version handler");
            await WriteError(httpContext, ex, StatusCodes.Status404NotFound);
        }

        string resolver()
        {
            var db = serviceProvider.GetRequiredService(_config.ServiceDbVersionResolverType ?? throw new InvalidOperationException("service resolver type is null"));
            return _config.ServiceDbVersionResolver(db);
        }
    }

    internal override Formatting GetFormatting()
    {
        return _config.GetFormatting();
    }

}
