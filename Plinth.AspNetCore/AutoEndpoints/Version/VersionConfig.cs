﻿using System.Reflection;

namespace Plinth.AspNetCore.AutoEndpoints.Version;

/// <summary>
/// Configuration Class for Version Auto Controller
/// </summary>
public class VersionConfig : VersionBaseConfig
{
    internal const string LoggingUrlPrefix = "/version";

    internal Func<object, string>? ServiceDbVersionResolver { get; set; }
    internal Type? ServiceDbVersionResolverType { get; set; }

    internal VersionConfig(Assembly callingAssembly) : base(callingAssembly)
    {
    }

    /// <summary>
    /// Function to resolve the db version with a resolved dependency
    /// </summary>
    /// <param name="dbVersionResolver"></param>
    /// <example>
    /// <code>
    ///     c.SetDbVersionResolver&lt;ISqlTransactionProvider&gt;(t => t.ExecuteTxn(...));
    /// </code>
    /// </example>
    /// <remarks>if set, will override the base SetDbVersionResolver</remarks>
    public void SetDbVersionResolver<T>(Func<T, string> dbVersionResolver)
    {
        ServiceDbVersionResolverType = typeof(T);
        ServiceDbVersionResolver = t => dbVersionResolver((T)t);
    }

    /// <summary>
    /// Constructor for Version Config
    /// </summary>
    public VersionConfig()
    {
    }
}
