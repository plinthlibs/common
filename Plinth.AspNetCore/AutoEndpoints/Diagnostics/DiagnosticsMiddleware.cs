using System.Text.RegularExpressions;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Plinth.AspNetCore.AutoEndpoints.Diagnostics;

/// <summary>
/// Middleware for Diagnostics Auto Controller
/// </summary>
internal partial class DiagnosticsMiddleware : BaseMiddleware
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

#if NET8_0_OR_GREATER
    [GeneratedRegex("^/diagnostics/(ping|connectivity)(?:/([^/]*))?$")]
    private static partial Regex PathRegex();
#else
    private static readonly Regex _pathRegex = new(@"^/diagnostics/(ping|connectivity)(?:/([^/]*))?$", RegexOptions.Compiled);
    private static Regex PathRegex() => _pathRegex;
#endif

    private readonly DiagnosticsConfig _config;

    private readonly RequestDelegate _next;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="next"></param>
    /// <param name="config"></param>
    public DiagnosticsMiddleware(RequestDelegate next, DiagnosticsConfig config)
    {
        _config = config;
        _next = next;
    }

    private static string GetOperationName(string action)
    {
        return action switch
        {
            "ping" => "AutoEndpoint::DiagnosticsPing",
            "connectivity" => "AutoEndpoint::DiagnosticsConnectivity",
            _ => "AutoEndpoint::DiagnosticsUnknown",
        };
    }

    public static string? WillHandle(HttpContext httpContext)
    {
        if (ShouldHandle(httpContext, nameof(DiagnosticsMiddleware), PathRegex(), HttpMethods.Get, out var match))
            return GetOperationName(match.Groups[1].Value);

        return null;
    }

    public async Task Invoke(HttpContext httpContext)
    {
        if (!GetStoredMatch(httpContext, nameof(DiagnosticsMiddleware), PathRegex(), HttpMethods.Get, out var match))
        {
            await _next(httpContext);
            return;
        }

        var action = match.Groups[1].Value;
        var apiVersion = match.Groups[2].Value;

        var gen = new DiagnosticsGenerator(_config);

        // ping only checks IP if honor for ping is set
        if (action != "ping" || _config.HonorForPing)
            CheckIsInternal(httpContext, _config);

        if (!string.IsNullOrEmpty(apiVersion) && apiVersion != "v1")
        {
            await WriteNotImplemented(httpContext, $"apiversion '{apiVersion}' does not exist");
            return;
        }

        try
        {
            switch (action)
            {
                case "ping":
                    if (!CheckAuth(httpContext.Request, DiagnosticsEndpoint.Ping))
                    {
                        await WriteForbidden(httpContext, "forbidden");
                        return;
                    }

                    await WriteResponse(httpContext, null, StatusCodes.Status200OK);
                    break;

                case "connectivity":
                    if (!CheckAuth(httpContext.Request, DiagnosticsEndpoint.Connectivity))
                    {
                        await WriteForbidden(httpContext, "forbidden");
                        return;
                    }

                    var connResponseModel = await gen.GetConnectivityResponseAsync(httpContext.RequestAborted);
                    await WriteResponse(httpContext, connResponseModel);
                    break;

                default:
                    await WriteNotImplemented(httpContext, $"{action} is not supported");
                    break;
            }
        }
        catch (Exception ex)
        {
            log.Error(ex, "Exception during diagnostics handler");
            await WriteError(httpContext, ex, StatusCodes.Status404NotFound);
        }
    }

    private bool CheckAuth(HttpRequest request, DiagnosticsEndpoint ep)
    {
        if (_config.AuthFuncs == null)
            return true;

        bool ret = true;

        try
        {
            string? authHeader = null;
            if (request.Headers.TryGetValue("Authorization", out var vals))
                authHeader = vals.FirstOrDefault();

            if (_config.AuthFuncs.TryGetValue(DiagnosticsEndpoint.All, out var func))
                ret = func.Invoke(request, authHeader);
            else if (_config.AuthFuncs.TryGetValue(ep, out func))
                ret = func.Invoke(request, authHeader);
        }
        catch (Exception e)
        {
            log.Error(e, "Exception during diagnostics handler auth function");
            ret = false;
        }

        if (!ret)
            log.Warn($"authentication for endpoint {ep} failed");

        return ret;
    }

    internal override Formatting GetFormatting()
    {
        return _config.GetFormatting();
    }
}
