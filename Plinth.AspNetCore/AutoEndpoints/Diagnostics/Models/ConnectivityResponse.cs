﻿using Plinth.HttpApiClient.Models;

namespace Plinth.AspNetCore.AutoEndpoints.Diagnostics.Models;

/// <summary>
/// Response to requests for connectivity information
/// </summary>
public class ConnectivityResponse : BaseApiModel
{
    /// <summary>
    /// Dictionary containing the connectivity information for each of the component's dependencies.
    /// The key of each entry is the name of the dependency.
    /// The value of each entry is the connectivity information retrieved for that dependency.
    /// </summary>
    public IDictionary<string, ConnectivityInfo>? DependencyConnectivityInfo { get; set; }
}
