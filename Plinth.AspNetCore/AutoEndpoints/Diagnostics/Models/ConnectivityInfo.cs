﻿using Plinth.HttpApiClient.Models;

namespace Plinth.AspNetCore.AutoEndpoints.Diagnostics.Models;

/// <summary>
/// Component connectivity information
/// </summary>
public class ConnectivityInfo : BaseApiModel
{
    /// <summary>
    /// Indicates whether or not it is possible to connect
    /// </summary>
    public bool Connectivity { get; set; }
}
