﻿using Plinth.AspNetCore.AutoEndpoints.Diagnostics.Connectivity;
using Plinth.AspNetCore.AutoEndpoints.Shared;

namespace Plinth.AspNetCore.AutoEndpoints.Diagnostics;

/// <summary>
/// Configuration Class for Diagnostic Auto Controller
/// </summary>
public class DiagnosticsBaseConfig : BaseConfig
{
    internal List<IConnectivityTester>? ConnectivityTesters { get; set; }
    internal bool HonorForPing { get; set; }

    /// <summary>
    /// Constructor for Diagnostic Config
    /// </summary>
    public DiagnosticsBaseConfig()
    {
        ConnectivityTesters = null;
        HonorForPing = false;
    }

    /// <summary>
    /// Set the Connectivity Resolver
    /// </summary>
    public void SetConnectivityTesters(params IConnectivityTester[] testers)
    {
        ConnectivityTesters = new List<IConnectivityTester>(testers);
    }

    /// <summary>
    /// By default, ping allows external IPs, even if AllowExternalIPs() is not called.
    /// Use this to override and make ping honor the AllowExternalIPs() setting
    /// </summary>
    /// <remarks>
    /// <list type="table">
    /// Use this table to determine the effect of combinations of calls
    ///     <listheader><description>Call AllowExternalIPs()</description>
    ///         <description>Call HonorAllowExternalIPSettingForPing()</description>
    ///         <description>Effect</description>
    ///     </listheader>
    ///     <item><term>No</term><term>No</term><term>default, ping accessible externally, other diagnostics not</term></item>
    ///     <item><term>No</term><term><b>Yes</b></term><term>no diagnostics accessible externally</term></item>
    ///     <item><term><b>Yes</b></term><term>No</term><term>all diagnostics accessible externally</term></item>
    ///     <item><term><b>Yes</b></term><term><b>Yes</b></term><term>all diagnostics accessible externally</term></item>
    /// </list>
    /// </remarks>
    public void HonorAllowExternalIPSettingForPing()
    {
        HonorForPing = true;
    }
}
