﻿using Microsoft.AspNetCore.Http;

namespace Plinth.AspNetCore.AutoEndpoints.Diagnostics;

/// <summary>
/// Configuration Class for Diagnostic Auto Controller
/// </summary>
public class DiagnosticsConfig : DiagnosticsBaseConfig
{
    /// <summary>
    /// Default route template for Diagnostic controller
    /// </summary>
    internal const string LoggingUrlPrefix = "/diagnostics";

    internal Dictionary<DiagnosticsEndpoint, Func<HttpRequest, string?, bool>>? AuthFuncs { get; set; }

    /// <summary>
    /// Constructor for Diagnostic Config
    /// </summary>
    public DiagnosticsConfig()
    {
    }

    /// <summary>
    /// Wire up authentication for diagnostic endpoints with specific auth per endpoint
    /// </summary>
    /// <param name="authFuncs">authentication functions which receive the request and the auth header value</param>
    public void AuthenticateRequests(Dictionary<DiagnosticsEndpoint, Func<HttpRequest, string?, bool>> authFuncs)
    {
        AuthFuncs = authFuncs;
    }

    /// <summary>
    /// Wire up authentication for all diagnostic endpoints
    /// </summary>
    /// <param name="authFunc">authentication function which receives the request and the auth header value</param>
    public void AuthenticateRequests(Func<HttpRequest, string?, bool> authFunc)
    {
        AuthFuncs = new Dictionary<DiagnosticsEndpoint, Func<HttpRequest, string?, bool>>
        {
            [DiagnosticsEndpoint.All] = authFunc
        };
    }
}
