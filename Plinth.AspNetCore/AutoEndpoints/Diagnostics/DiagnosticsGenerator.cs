using Plinth.AspNetCore.AutoEndpoints.Diagnostics.Models;

namespace Plinth.AspNetCore.AutoEndpoints.Diagnostics;

internal class DiagnosticsGenerator(DiagnosticsBaseConfig config)
{
    public async Task<ConnectivityResponse> GetConnectivityResponseAsync(CancellationToken cancellationToken)
    {
        if (config.ConnectivityTesters == null)
            throw new NotImplementedException();

        var tasks = new Task<ConnectivityInfo>[config.ConnectivityTesters.Count];
        var results = new Dictionary<string, Task<ConnectivityInfo>>(config.ConnectivityTesters.Count);

        int p = 0;
        foreach (var tester in config.ConnectivityTesters)
        {
            if (string.IsNullOrEmpty(tester.TestIdentifier))
                throw new ArgumentException("a TestIdentifier must be provided for connectivity");

            results[tester.TestIdentifier] = tasks[p] = tester.TestAsync(cancellationToken);
            p++;
        }

        await Task.WhenAll(tasks);

        return new ConnectivityResponse
        {
            DependencyConnectivityInfo = new SortedDictionary<string, ConnectivityInfo>(
                results.ToDictionary(r => r.Key, r => r.Value.Result))
        };
    }
}
