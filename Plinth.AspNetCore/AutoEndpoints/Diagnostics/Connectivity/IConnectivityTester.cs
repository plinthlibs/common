﻿using Plinth.AspNetCore.AutoEndpoints.Diagnostics.Models;

namespace Plinth.AspNetCore.AutoEndpoints.Diagnostics.Connectivity;

/// <summary>
/// Common interface for all connectivity testers
/// </summary>
public interface IConnectivityTester
{
    /// <summary>
    /// a unique identifier for the thing being tested
    /// </summary>
    string? TestIdentifier { get; }

    /// <summary>
    /// Test for connectivity
    /// </summary>
    Task<ConnectivityInfo> TestAsync(CancellationToken cancellationToken = default);
}
