using System.Diagnostics;
using System.Net.Sockets;
using Microsoft.Extensions.Logging;
using Plinth.Serialization;
using Plinth.Common.Utils;
using Plinth.AspNetCore.AutoEndpoints.Diagnostics.Models;
using Plinth.Common.Logging;

namespace Plinth.AspNetCore.AutoEndpoints.Diagnostics.Connectivity;

/// <summary>
/// A container for built-in connectivity testers
/// </summary>
public static class ConnectivityTesters
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    /// <summary>
    /// Base class for testers
    /// </summary>
    public abstract class BaseTester : IConnectivityTester
    {
        /// <summary>
        /// Thing being tested
        /// </summary>
        public string? TestIdentifier { get; }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="testeeName">unique name of the thing being tested</param>
        protected BaseTester(string testeeName)
        {
            TestIdentifier = testeeName;
        }

        private protected BaseTester()
        {
        }

        /// <summary>
        /// Override to perform test
        /// </summary>
        protected abstract Task<ConnectivityInfo> PerformTestAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Will test connectivity
        /// </summary>
        public async Task<ConnectivityInfo> TestAsync(CancellationToken cancellationToken)
        {
            Stopwatch? sw = null;
            if (log.IsTraceEnabled())
            {
                log.Trace($"Test Begin [{TestIdentifier}]");
                sw = Stopwatch.StartNew();
            }

            var ret = await PerformTestAsync(cancellationToken);

            if (log.IsTraceEnabled())
            {
                sw?.Stop();
                log.Trace($"Test Complete [{TestIdentifier}], Took {sw?.Elapsed}, result = {ret.Connectivity}");
            }

            return ret;
        }

        /// <summary>
        /// Utility to create ConnectivityInfo from bool
        /// </summary>
        protected static ConnectivityInfo CreateFrom(bool connectivity)
        {
            return new ConnectivityInfo { Connectivity = connectivity };
        }
    }

    /// <summary>
    /// A simple tester that does an HTTP HEAD request against a URL
    /// </summary>
    public class HttpHead : BaseTester
    {
        private readonly string _url;
        private readonly TimeSpan? _timeout;

        /// <summary>
        /// constructor
        /// </summary>
        public HttpHead(string testId, string url, TimeSpan? timeout = null) : base(testId)
        {
            if (string.IsNullOrWhiteSpace(testId)) throw new ArgumentNullException(nameof(testId));
            if (string.IsNullOrWhiteSpace(url)) throw new ArgumentNullException(nameof(url));
            _url = url;
            _timeout = timeout;
        }

        /// <summary>
        /// constructor for discovery
        /// </summary>
        public HttpHead(string url, TimeSpan? timeout = null) : base()
        {
            if (string.IsNullOrWhiteSpace(url)) throw new ArgumentNullException(nameof(url));
            _url = url;
            _timeout = timeout;
        }

        /// <summary>
        /// Test
        /// </summary>
        protected override async Task<ConnectivityInfo> PerformTestAsync(CancellationToken cancellationToken)
        {
            using var client = new HttpClient();
            if (_timeout.HasValue)
                client.Timeout = _timeout.Value;
            using var request = new HttpRequestMessage(HttpMethod.Head, new Uri(_url));
            try
            {
                using var response = await client.SendAsync(request, cancellationToken);
                return CreateFrom(response != null);
            }
            catch
            {
                return CreateFrom(false);
            }
        }
    }

    /// <summary>
    /// A simple tester that uses the auto-controller ping method on a component
    /// </summary>
    public class HttpPing : BaseTester
    {
        private readonly Uri _uri;
        private readonly TimeSpan? _timeout;

        private Action<HttpRequestMessage>? _preAction;
        private string? _authHeader;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="testId">Test Identifier, like '{Product}-{Component}'</param>
        /// <param name="baseUrl">Protocol, host, port (https://server:8080/)</param>
        /// <param name="timeout">optional timeout override</param>
        /// <param name="relativePathOverride">optional relative path override (defaults to 'diagnostics/ping')</param>
        public HttpPing(string testId, string baseUrl, TimeSpan? timeout = null, string? relativePathOverride = null) : base(testId)
        {
            if (string.IsNullOrWhiteSpace(testId)) throw new ArgumentNullException(nameof(testId));
            if (string.IsNullOrWhiteSpace(baseUrl)) throw new ArgumentNullException(nameof(baseUrl));
            _uri = new Uri(new Uri(baseUrl), relativePathOverride ?? "diagnostics/ping");
            _timeout = timeout;
        }

        /// <summary>
        /// constructor for discovery
        /// </summary>
        /// <param name="baseUrl">Protocol, host, port (https://server:8080/)</param>
        /// <param name="timeout">optional timeout override</param>
        /// <param name="relativePathOverride">optional relative path override (defaults to 'diagnostics/ping')</param>
        public HttpPing(string baseUrl, TimeSpan? timeout = null, string? relativePathOverride = null) : base()
        {
            if (string.IsNullOrWhiteSpace(baseUrl)) throw new ArgumentNullException(nameof(baseUrl));
            _uri = new Uri(new Uri(baseUrl), relativePathOverride ?? "diagnostics/ping");
            _timeout = timeout;
        }

        /// <summary>
        /// Set authentication header for ping requests
        /// </summary>
        /// <param name="authHeader">value of authorization header</param>
        /// <returns>this</returns>
        public HttpPing SetAuthHeader(string authHeader)
        {
            _authHeader = authHeader;
            return this;
        }

        /// <summary>
        /// Set a function to manipulate the request before sending
        /// </summary>
        /// <param name="action">action called with mutable request</param>
        /// <returns>this</returns>
        public HttpPing SetPreProcess(Action<HttpRequestMessage> action)
        {
            _preAction = action;
            return this;
        }

        /// <summary>
        /// Test
        /// </summary>
        protected override async Task<ConnectivityInfo> PerformTestAsync(CancellationToken cancellationToken)
        {
            using var client = new HttpClient();
            if (_timeout.HasValue)
                client.Timeout = _timeout.Value;

            var request = new HttpRequestMessage(HttpMethod.Get, _uri);

            // plinth headers
            request.Headers.Add(RequestTraceId.RequestTraceIdHeader, RequestTraceId.Instance.Get());
            request.Headers.Add(RequestTraceChain.RequestTraceChainHeader, RequestTraceChain.Instance.GenerateNextChain());

            var rProps = RequestTraceProperties.GetRequestTraceProperties();
            if (rProps?.Count > 0)
                request.Headers.Add(RequestTraceProperties.RequestTracePropertiesHeader, JsonUtil.SerializeObject(rProps));
            // plinth Headers

            try
            {
                if (_authHeader != null)
                    request.Headers.Add("Authorization", _authHeader);
                _preAction?.Invoke(request);

                using var response = await client.SendAsync(request, cancellationToken);
                return CreateFrom(response != null && response.IsSuccessStatusCode);
            }
            catch
            {
                return CreateFrom(false);
            }
        }
    }

    /// <summary>
    /// A tester which confirms connectivity to a tcp port
    /// </summary>
    public class TcpPing : BaseTester
    {
        private readonly string _hostname;
        private readonly int _port;
        private readonly TimeSpan _timeout;

        /// <summary>
        /// constructor
        /// </summary>
        public TcpPing(string testId, string hostname, int port, TimeSpan? timeout = null) : base(testId)
        {
            if (string.IsNullOrWhiteSpace(testId)) throw new ArgumentNullException(nameof(testId));
            if (string.IsNullOrWhiteSpace(hostname)) throw new ArgumentNullException(nameof(hostname));
            if (port < 0 || port > 65535) throw new ArgumentOutOfRangeException(nameof(port), "must be 0..65535");
            _hostname = hostname;
            _port = port;
            _timeout = timeout ?? TimeSpan.FromSeconds(30);
        }

        /// <summary>
        /// constructor for discovery
        /// </summary>
        public TcpPing(string hostname, int port, TimeSpan? timeout = null) : base()
        {
            if (string.IsNullOrWhiteSpace(hostname)) throw new ArgumentNullException(nameof(hostname));
            if (port < 0 || port > 65535) throw new ArgumentOutOfRangeException(nameof(port), "must be 0..65535");
            _hostname = hostname;
            _port = port;
            _timeout = timeout ?? TimeSpan.FromSeconds(30);
        }

        /// <summary>
        /// Test
        /// </summary>
        protected override async Task<ConnectivityInfo> PerformTestAsync(CancellationToken cancellationToken)
        {
            using var tcpClient = new TcpClient();
            try
            {
                using (CreateTimeoutScope(tcpClient, _timeout))
                {
                    await tcpClient.ConnectAsync(_hostname, _port, cancellationToken);
                    tcpClient.Close();
                }
                return CreateFrom(true);
            }
            catch
            {
                return CreateFrom(false);
            }
        }

        // clever hack to apply timeout to tcp connect
        // https://stackoverflow.com/questions/21468137/async-network-operations-never-finish/21468138#21468138
        private static IDisposable CreateTimeoutScope(IDisposable disposable, TimeSpan timeSpan)
        {
            var cancellationTokenSource = new CancellationTokenSource(timeSpan);
            var cancellationTokenRegistration = cancellationTokenSource.Token.Register(disposable.Dispose);
            return new DisposableScope(
                () =>
                {
                    cancellationTokenRegistration.Dispose();
                    cancellationTokenSource.Dispose();
                    disposable.Dispose();
                });
        }

        private sealed class DisposableScope(Action closeScopeAction) : IDisposable
        {
            public void Dispose()
            {
                closeScopeAction();
            }
        }
    }

    /// <summary>
    /// A tester which confirms connectivity to a server via Ping (ICMP)
    /// </summary>
    public class Ping : BaseTester
    {
        private readonly string _hostname;
        private readonly int _timeoutMillis;

        /// <summary>
        /// constructor
        /// </summary>
        public Ping(string testId, string hostname, TimeSpan? timeout = null) : base(testId)
        {
            if (string.IsNullOrWhiteSpace(testId)) throw new ArgumentNullException(nameof(testId));
            if (string.IsNullOrWhiteSpace(hostname)) throw new ArgumentNullException(nameof(hostname));
            _hostname = hostname;
            _timeoutMillis = (int)(timeout ?? TimeSpan.FromSeconds(5)).TotalMilliseconds;
        }

        /// <summary>
        /// constructor for discovery
        /// </summary>
        public Ping(string hostname, TimeSpan? timeout = null) : base()
        {
            if (string.IsNullOrWhiteSpace(hostname)) throw new ArgumentNullException(nameof(hostname));
            _hostname = hostname;
            _timeoutMillis = (int)(timeout ?? TimeSpan.FromSeconds(5)).TotalMilliseconds;
        }

        /// <summary>
        /// Test
        /// </summary>
        protected override async Task<ConnectivityInfo> PerformTestAsync(CancellationToken cancellationToken)
        {
            using var pingSender = new System.Net.NetworkInformation.Ping();

            try
            {
                var reply = await pingSender.SendPingAsync(_hostname, _timeoutMillis);
                return CreateFrom(reply.Status == System.Net.NetworkInformation.IPStatus.Success);
            }
            catch
            {
                return CreateFrom(false);
            }
        }
    }

    /// <summary>
    /// A tester which calls a custom function to determine connectivity
    /// </summary>
    public class Custom : BaseTester
    {
        private readonly Func<CancellationToken, Task<bool>> _func;

        /// <summary>
        /// constructor
        /// </summary>
        public Custom(string testId, Func<CancellationToken, Task<bool>> func) : base(testId)
        {
            if (string.IsNullOrWhiteSpace(testId)) throw new ArgumentNullException(nameof(testId));
            _func = func ?? throw new ArgumentNullException(nameof(func));
        }

        /// <summary>
        /// constructor for discovery
        /// </summary>
        public Custom(Func<CancellationToken, Task<bool>> func) : base()
        {
            _func = func ?? throw new ArgumentNullException(nameof(func));
        }

        /// <summary>
        /// Test
        /// </summary>
        protected override async Task<ConnectivityInfo> PerformTestAsync(CancellationToken cancellationToken)
        {
            try
            {
                // this forces the rest of the function to be in a continuation
                // this assures us that the _func() will not block us if it only executes synchronous code
                await Task.Yield();

                return CreateFrom( await _func(cancellationToken) );
            }
            catch
            {
                return CreateFrom(false);
            }
        }
    }

    /// <summary>
    /// A tester which confirms connectivity to a sql database
    /// </summary>
    public class SqlDatabase : BaseTester
    {
        private readonly string _connStr;

        private static readonly Type[] _openAsyncParamTypes = [typeof(CancellationToken)];

        /// <summary>
        /// constructor
        /// </summary>
        public SqlDatabase(string testId, string connectionString) : base(testId)
        {
            if (string.IsNullOrWhiteSpace(testId)) throw new ArgumentNullException(nameof(testId));
            if (string.IsNullOrWhiteSpace(connectionString)) throw new ArgumentNullException(nameof(connectionString));
            _connStr = connectionString;
        }

        /// <summary>
        /// constructor for discovery
        /// </summary>
        public SqlDatabase(string connectionString) : base()
        {
            if (string.IsNullOrWhiteSpace(connectionString)) throw new ArgumentNullException(nameof(connectionString));
            _connStr = connectionString;
        }

        /// <summary>
        /// Test
        /// </summary>
        protected override async Task<ConnectivityInfo> PerformTestAsync(CancellationToken cancellationToken)
        {
            // try postgresql first, then sql server
            var sqlConnType = Type.GetType("Npgsql.NpgsqlConnection,Npgsql");
            if (sqlConnType == null)
            {
                sqlConnType = Type.GetType("Microsoft.Data.SqlClient.SqlConnection,Microsoft.Data.SqlClient");
                if (sqlConnType == null)
                {
                    sqlConnType = Type.GetType("System.Data.SqlClient.SqlConnection,System.Data.SqlClient");
                    if (sqlConnType == null)
                    {
                        log.Error("no sql client library found (tried Npgsql, Microsoft.Data.SqlClient, System.Data.SqlClient)");
                        return CreateFrom(false);
                    }
                }
            }

            using var connection = (IDisposable)Activator.CreateInstance(sqlConnType, _connStr)!;
            try
            {
                var method = sqlConnType.GetMethod("OpenAsync", _openAsyncParamTypes);
                if (method == null)
                {
                    log.Error("could not find OpenAsync method on {0}", sqlConnType.FullName);
                    return CreateFrom(false);
                }
                await (method.Invoke(connection, [cancellationToken]) as Task)!;
                return CreateFrom(true);
            }
            catch
            {
                return CreateFrom(false);
            }
        }
    }

    /// <summary>
    /// A tester which confirms connectivity to a read only file share
    /// </summary>
    public class ReadOnlyFileShare : BaseTester
    {
        private readonly string _path;

        /// <summary>
        /// constructor
        /// </summary>
        public ReadOnlyFileShare(string testId, string path) : base(testId)
        {
            if (string.IsNullOrWhiteSpace(testId)) throw new ArgumentNullException(nameof(testId));
            if (string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException(nameof(path));
            _path = path;
        }

        /// <summary>
        /// constructor for discovery
        /// </summary>
        public ReadOnlyFileShare(string path) : base()
        {
            if (string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException(nameof(path));
            _path = path;
        }

        /// <summary>
        /// Test
        /// </summary>
        protected override async Task<ConnectivityInfo> PerformTestAsync(CancellationToken cancellationToken)
        {
            // this forces the rest of the function to be in a continuation
            await Task.Yield();

            try
            {
                foreach (var f in Directory.EnumerateFiles(_path, "*.*", SearchOption.AllDirectories))
                    if (new FileInfo(Path.Combine(_path, f)).CreationTimeUtc > DateTime.MinValue)
                        return CreateFrom(true);
            }
            catch
            {
                // fall through
            }

            return CreateFrom(false);
        }
    }

    /// <summary>
    /// A tester which confirms connectivity to a read only file share
    /// </summary>
    public class WriteableFileShare : BaseTester
    {
        private readonly string _path;

        /// <summary>
        /// constructor
        /// </summary>
        public WriteableFileShare(string testId, string path) : base(testId)
        {
            if (string.IsNullOrWhiteSpace(testId)) throw new ArgumentNullException(nameof(testId));
            if (string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException(nameof(path));
            _path = path;
        }

        /// <summary>
        /// constructor for discovery
        /// </summary>
        public WriteableFileShare(string path) : base()
        {
            if (string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException(nameof(path));
            _path = path;
        }

        /// <summary>
        /// Test
        /// </summary>
        protected override async Task<ConnectivityInfo> PerformTestAsync(CancellationToken cancellationToken)
        {
            // this forces the rest of the function to be in a continuation
            await Task.Yield();

            try
            {
                using FileStream fs = File.Create(Path.Combine(_path, Path.GetRandomFileName()), 1, FileOptions.DeleteOnClose);
                return CreateFrom(true);
            }
            catch
            {
                return CreateFrom(false);
            }
        }
    }
}
