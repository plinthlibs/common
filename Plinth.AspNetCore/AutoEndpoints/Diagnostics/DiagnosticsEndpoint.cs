﻿namespace Plinth.AspNetCore.AutoEndpoints.Diagnostics;

/// <summary>
/// Diagnostic Endpoints
/// </summary>
public enum DiagnosticsEndpoint
{
    /// <summary>All Endpoints</summary>
    All = 0,
    /// <summary>Ping Endpoint</summary>
    Ping,
    /// <summary>Connectivity Endpoint</summary>
    Connectivity
}
