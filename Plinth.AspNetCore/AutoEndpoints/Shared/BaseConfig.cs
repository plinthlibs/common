using Newtonsoft.Json;

namespace Plinth.AspNetCore.AutoEndpoints.Shared;

/// <summary>
/// Base config class for the auto controller config classes
/// </summary>
public abstract class BaseConfig
{
    private bool _prettyPrint = true;

    internal bool AllowExternal { get; private set; } = false;

    /// <summary>
    /// Constructor
    /// </summary>
    private protected BaseConfig()
    {
    }

    /// <summary>
    /// Disable pretty printing in json response
    /// </summary>
    public void NoPrettyPrint()
    {
        _prettyPrint = false;
    }

    /// <summary>
    /// Allow non-private networks to access the service
    /// </summary>
    public void AllowExternalIPs()
    {
        AllowExternal = true;
    }

    internal Formatting GetFormatting()
    {
        return _prettyPrint ? Formatting.Indented : Formatting.None;
    }
}
