﻿using System.Net;

namespace Plinth.AspNetCore.AutoEndpoints.Shared;

internal static class IPUtils
{
    /* An IP should be considered as internal when:

       ::1          -   IPv6  loopback
       10.0.0.0     -   10.255.255.255  (10/8 prefix)
       127.0.0.0    -   127.255.255.255  (127/8 prefix)
       172.16.0.0   -   172.31.255.255  (172.16/12 prefix)
       192.168.0.0  -   192.168.255.255 (192.168/16 prefix)
     */
    public static bool IsInternal(IPAddress testIp)
    {
        if (IPAddress.IsLoopback(testIp))
            return true;

        if (testIp.IsIPv4MappedToIPv6)
            testIp = testIp.MapToIPv4();

        byte[] ip = testIp.GetAddressBytes();

        // we will reject IPv6 for now
        if (ip.Length == 6)
            return false;

        return (ip[0]) switch
        {
            10 or 127 => true,
            172 => ip[1] >= 16 && ip[1] < 32,
            192 => ip[1] == 168,
            _ => false,
        };
    }
}
