namespace Plinth.AspNetCore.AutoEndpoints.Shared;

internal static class EnvironmentUtility
{
    public static EnvironmentVariableInfo SafeGetEnvironmentVariable(string variableName)
    {
        try
        {
            return new EnvironmentVariableInfo(true, Environment.GetEnvironmentVariable(variableName));
        }
        catch (Exception)
        {
            return new EnvironmentVariableInfo(false, null);
        }
    }
}
