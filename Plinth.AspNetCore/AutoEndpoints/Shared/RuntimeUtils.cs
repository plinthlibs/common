﻿namespace Plinth.AspNetCore.AutoEndpoints.Shared;

internal static class RuntimeUtils
{
    public static string? GetNetRuntime()
        => AppDomain.CurrentDomain.SetupInformation.TargetFrameworkName;

    public static string GetNetCoreVersion()
        => Environment.Version.ToString();
}
