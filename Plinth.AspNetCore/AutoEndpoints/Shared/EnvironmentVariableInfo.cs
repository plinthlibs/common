namespace Plinth.AspNetCore.AutoEndpoints.Shared;

/// <summary>
/// Information about Environment Variable
/// </summary>
/// <param name="successRead"></param>
/// <param name="value"></param>
internal class EnvironmentVariableInfo(bool successRead, string? value)
{
    /// <summary>
    /// Was the variable successfully read
    /// </summary>
    public bool SuccessfulRead { get; } = successRead;

    /// <summary>
    /// The value of the variable (cannot be null)
    /// </summary>
    public string Value { get; } = value ?? "--Not Present--";

    /// <summary>
    /// The raw value of the variable from the environment (may be null)
    /// </summary>
    public string? RawValue { get; } = value;

    /// <summary>
    /// implicitly convert to string
    /// </summary>
    /// <param name="e"></param>
    public static implicit operator string(EnvironmentVariableInfo e)
    {
        return e.Value;
    }
}
