using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Plinth.AspNetCore.Configuration
{
    /// <summary>
    /// Utilities for enabling CORS in ASP.NET Core projects
    /// </summary>
    public static class CorsUtil
    {
        /// <summary>
        /// Eliminates trailing slashes from a comma separated list of URLs, .UserCorsForOrigins() is recommended
        /// </summary>
        /// <remarks>
        /// NOTE: This will distinct and sort as well
        /// </remarks>
        /// <param name="allowedOrigins">A comma separated list of URLs</param>
        /// <returns>An array of origins, suitable for passing to .WithOrigins()</returns>
        /// <example>
        /// <code>
        ///   var corsOrigins = GetStringFromConfig();
        ///   
        ///   app.UseCors(builder => builder.WithOrigins(CorsUtil.ParseOrigins(corsOrigins));
        /// </code>
        /// </example>
        public static string[] ParseOrigins(string allowedOrigins)
        {
            return allowedOrigins.Split(',', StringSplitOptions.RemoveEmptyEntries)
                                .Select(c => c.Trim().TrimEnd('/'))
                                .Distinct()
                                .OrderBy(k => k)
                                .ToArray();
        }
    }
}

namespace Microsoft.AspNetCore.Builder // putting it here makes it easier to find
{
    /// <summary>
    /// Extensions for Cors Middleware
    /// </summary>
    public static class CorsUtilExtensions
    {
        private static readonly ILogger log = StaticLogManager.GetLogger();

        /// <summary>
        /// This enables CORS and opens it up completely for any domain to do anything, equivalent to services.AddCorsWithOrigins("*")
        /// </summary>
        /// <param name="services">call during ConfigureServices() in startup</param>
        /// <param name="configure">(optional) action to customize the cors policy</param>
        /// <remarks><para>be sure to call <c>app.UseCors()</c> in <c>Configure()</c></para>
        /// <para>this solves the signalr issue with chrome/credentials/wildcard domain</para></remarks>
        public static IServiceCollection AddCorsFullyOpen(this IServiceCollection services, Action<CorsPolicyBuilder>? configure = null)
        {
            // this minor hack is based on a branch in the CorsService code which will return the passed in origin
            // when 1 or more origins are passed in.  The custom IsOriginAllowed func which returns true allows all origins to pass
            // It is essentially simulating AllowAnyOrigin(), but without returning '*' in the preflight response
            // This is because when a browser sets credentials on an XHR request and the server is allowing '*' domains, the browser will reject
            // By making it return the correct origin instead, the browser accepts it
            return services.AddCors(s =>
            {
                var policyBuilder = new CorsPolicyBuilder()
                    .AllowCredentials()
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .WithOrigins("http://any", "https://any")
                    .SetIsOriginAllowed(_ => true);

                configure?.Invoke(policyBuilder);

                s.AddDefaultPolicy(policyBuilder.Build());
            });
        }

        /// <summary>
        /// Enable CORS for app with a comma separated list of allowed origins, supports '*'
        /// </summary>
        /// <param name="services"></param>
        /// <param name="allowedOrigins">A comma separated list of URLs</param>
        /// <param name="configure">(optional) action to customize the cors policy</param>
        /// <remarks><para>be sure to call <c>app.UseCors()</c> in <c>Configure()</c></para>
        /// <para>ASP.NET CORS does not support subdomain wildcards, to use you must manually use SetIsOriginAllowed()</para></remarks>
        public static IServiceCollection AddCorsWithOrigins(this IServiceCollection services, string allowedOrigins, Action<CorsPolicyBuilder>? configure = null)
        {
            if (allowedOrigins == "*")
                return services.AddCorsFullyOpen(configure);

            var origins = Plinth.AspNetCore.Configuration.CorsUtil.ParseOrigins(allowedOrigins);

            return services.AddCors(s =>
            {
                var policyBuilder = new CorsPolicyBuilder()
                     .AllowCredentials()
                     .AllowAnyHeader()
                     .AllowAnyMethod();

                if (origins.Contains("*"))
                {
                    log.Warn("Do not include '*' within an origin list, either pass only '*' or only real origins");
                    policyBuilder.DisallowCredentials();
                    policyBuilder.AllowAnyOrigin();
                }
                else
                    policyBuilder.WithOrigins(origins);

                configure?.Invoke(policyBuilder);

                s.AddDefaultPolicy(policyBuilder.Build());
            });
        }
    }
}
