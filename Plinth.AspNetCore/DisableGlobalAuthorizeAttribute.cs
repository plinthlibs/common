﻿namespace Plinth.AspNetCore;

/// <summary>
/// Add this to your controller to disable the global [Authorize] attribute added via AddGlobalAuthorizedFilter()
/// </summary>
/// <remarks>Since [Authorize] attributes are additive, the global [Authorize] will prevent using other authentication schemes,
/// since the default will always apply first.  This attribute will turn that off so you can specify custom schemes.  Be careful
/// because using without applying your own [Authorize] will leave endpoints exposed</remarks>
[AttributeUsage(AttributeTargets.Class)]
public class DisableGlobalAuthorizeAttribute : Attribute
{
}
