﻿using Plinth.HttpApiClient.Models;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Plinth.AspNetCore.Filters;

/// <summary>
/// Use this attribute to automatically call UpgradeModel on any supported action parameters
/// </summary>
public sealed class UpgradeModelFilterAttribute : ActionFilterAttribute
{
    /// <summary>
    /// Handle Action Executing
    /// </summary>
    /// <param name="actionContext"></param>
    public override void OnActionExecuting(ActionExecutingContext actionContext)
    {
        foreach (var argument in actionContext.ActionArguments)
            (argument.Value as BaseApiModel)?.UpgradeModel();
    }
}
