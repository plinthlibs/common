using Plinth.AspNetCore;
using Plinth.AspNetCore.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Plinth.AspNetCore.Newtonsoft;

namespace Microsoft.AspNetCore.Builder; // putting it here makes it easier to find

/// <summary>
/// Extensions for enabling Plinth Services
/// </summary>
public static class PlinthServicesExtensions
{
    /// <summary>
    /// Configure Plinth Services/Middleware, do this BEFORE calling <c>.UseEndpoints()</c>
    /// </summary>
    /// <param name="builder"></param>
    /// <returns>builder</returns>
    public static IApplicationBuilder UsePlinthServices(this IApplicationBuilder builder)
    {
        var enabler = builder.ApplicationServices.GetService<PlinthServicesEnabler>()
            ?? throw new InvalidOperationException("Make sure to use .AddPlinthServices() in .ConfigureServices()");

        return enabler.Use(builder);
    }

    /// <summary>
    /// Apply Plinth Controller Defaults when calling .AddControllers()
    /// </summary>
    /// <param name="mvc">result of calling <c>services.AddControllers()</c> in <c>ConfigureServices()</c></param>
    /// <remarks>
    /// <list type="bullet">
    /// <item>Calling <c>.Upgrade()</c> on incoming model objects derived from <c>BaseApiModel</c></item>
    /// <item>Applying our standard Json.Net serializer settings</item>
    /// </list>
    /// </remarks>
    /// <example>
    /// Call like this inside <c>ConfigureServices()</c>
    /// <code>
    ///   services.AddControllers()
    ///           .AddPlinthControllerDefaults();
    /// </code>
    /// </example>
    /// <returns>mvc</returns>
    public static IMvcBuilder AddPlinthControllerDefaults(this IMvcBuilder mvc)
    {
        mvc.AddMvcOptions(options =>
        {
            options.Filters.Add<UpgradeModelFilterAttribute>();
        });

        // configure System.Text.Json
        mvc.AddJsonOptions(c => c.JsonSerializerOptions.DefaultIgnoreCondition = System.Text.Json.Serialization.JsonIgnoreCondition.WhenWritingNull);

        // for now, keep using newtonsoft, eventually, make this optional
        mvc.AddPlinthControllerNewtonsoftJson();

        // not really 'controllers' technically, but related enough
        IBackgroundTaskQueueImpl impl = new BackgroundTaskQueue();
        mvc.Services.AddSingleton<IBackgroundTaskQueue>(impl);
        mvc.Services.AddSingleton<IBackgroundTaskQueueImpl>(impl);
        mvc.Services.AddHostedService<QueuedHostedService>();

        return mvc;
    }

    /// <summary>
    /// Add [Authorized] globally to all controllers
    /// </summary>
    /// <param name="mvc">result of calling <c>services.AddMvc()</c> in <c>ConfigureServices()</c></param>
    /// <remarks>
    /// NOTE: if you plan to use custom schemes (e.g.  [Authorize(AuthenticationSchemes = "...")], use
    /// [DisableGlobalAuthorize] on that controller, otherwise the custom scheme won't work
    /// </remarks>
    /// <example>
    /// Call like this inside <c>ConfigureServices()</c>
    /// <code>
    ///   services.AddConntrollers()
    ///           .AddGlobalAuthorizedFilter();
    /// </code>
    /// </example>
    /// <returns>mvc</returns>
    public static IMvcBuilder AddGlobalAuthorizedFilter(this IMvcBuilder mvc)
        => AddGlobalAuthorizedFilter(mvc, new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build());

    /// <summary>
    /// Add custom AuthorizationPolicy globally to all controllers
    /// </summary>
    /// <param name="mvc">result of calling <c>services.AddMvc()</c> in <c>ConfigureServices()</c></param>
    /// <param name="policy">custom policy to apply</param>
    /// <remarks>
    /// NOTE: if you plan to use custom schemes (e.g.  [Authorize(AuthenticationSchemes = "...")], use
    /// [DisableGlobalAuthorize] on that controller, otherwise the custom scheme won't work
    /// </remarks>
    /// <example>
    /// Call like this inside <c>ConfigureServices()</c>
    /// <code>
    ///   var myPolicy = new AuthorizationPolicyBuilder().Require....().Build();
    /// 
    ///   services.AddConntrollers()
    ///           .AddGlobalAuthorizedFilter(myPolicy);
    /// </code>
    /// </example>
    /// <returns>mvc</returns>
    public static IMvcBuilder AddGlobalAuthorizedFilter(this IMvcBuilder mvc, AuthorizationPolicy policy)
        => mvc.AddMvcOptions(options =>
        {
            options.Conventions.Add(new PlinthControllerApplicationModelConvention(policy));
        });

    /* This is used because if you add an IControllerModelConvention, asp.net core will wrap it in its own private IApplicationModelConvention
     * the problem is that in SwaggerExtensions in Plinth.AspNetCore.Swagger, we have to detect that global auth is enabled
     * to properly mark endpoints as authorized.  It can only do that by inspecting MvcOptions.  
     * So we need to make sure our own class is added and not their wrapper.
     * https://github.com/aspnet/Mvc/blob/master/src/Microsoft.AspNetCore.Mvc.Core/DependencyInjection/ApplicationModelConventionExtensions.cs/
    */
    internal class PlinthControllerApplicationModelConvention(AuthorizationPolicy policy) : IApplicationModelConvention
    {
        public void Apply(ApplicationModel application)
        {
            ArgumentNullException.ThrowIfNull(application);

            var controllers = application.Controllers.ToArray();
            foreach (var controller in controllers)
            {
                if (!controller.Attributes.Any(a => a is DisableGlobalAuthorizeAttribute))
                {
                    controller.Filters.Add(new AuthorizeFilter(policy));
                }
            }
        }
    }
}
