using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using Plinth.Common.Utils;

namespace Plinth.AspNetCore;

// This is based on samples from the documentation.  Built-in support has been rejected
// https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/hosted-services?view=aspnetcore-2.2
// https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/hosted-services?view=aspnetcore-3.0
// https://github.com/aspnet/Hosting/issues/1280

/// <summary>
/// Interface for queueing background tasks (non guaranteed, otherwise, use hangfire)
/// </summary>
public interface IBackgroundTaskQueue
{
    /// <summary>
    /// Queue a background task
    /// </summary>
    /// <param name="workItem">an async function which takes a cancellation token</param>
    /// <example>
    /// <code>
    ///     IBackgroundTaskQueue queue;
    ///     queue.QueueBackgroundWorkItem(async token =>
    ///     {
    ///         await SomeCode();
    ///     });
    /// </code>
    /// </example>
    /// <remarks>the logging trace info will be maintained from the queuing async context</remarks>
    void QueueBackgroundWorkItem(Func<CancellationToken, Task> workItem);
}

internal class QueuedTask
{
    public StaticLogManager.TraceInfo TraceInfo { get; set; } = null!;
    public Func<CancellationToken, Task> Func { get; set; } = null!;
}

internal interface IBackgroundTaskQueueImpl : IBackgroundTaskQueue
{
    Task<QueuedTask> DequeueAsync(CancellationToken cancellationToken);

    bool IsEmpty { get; }

    int QueueDepth { get; }
}

internal class BackgroundTaskQueue : IBackgroundTaskQueueImpl
{
    private readonly ConcurrentQueue<QueuedTask> _workItems = new();
    private readonly SemaphoreSlim _signal = new(0);

    public void QueueBackgroundWorkItem(Func<CancellationToken, Task> workItem)
    {
        ArgumentNullException.ThrowIfNull(workItem);

        _workItems.Enqueue(new QueuedTask { Func = workItem, TraceInfo = StaticLogManager.GetTraceInfo() });
        _signal.Release();
    }

    public async Task<QueuedTask> DequeueAsync(CancellationToken cancellationToken)
    {
        await _signal.WaitAsync(cancellationToken);
        _workItems.TryDequeue(out var workItem);
        return workItem!;
    }

    public bool IsEmpty => _workItems.IsEmpty;

    public int QueueDepth => _workItems.Count;
}

internal class QueuedHostedService : BackgroundService
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private readonly ConcurrentDictionary<Guid, Task> _tasks = new();
    private readonly SemaphoreSlim _signal;

    public QueuedHostedService(IBackgroundTaskQueueImpl taskQueue, int maxConcurrent = 8)
    {
        _taskQueue = taskQueue;
        _signal = new SemaphoreSlim(maxConcurrent, maxConcurrent);
    }

    private readonly IBackgroundTaskQueueImpl _taskQueue;

    protected async override Task ExecuteAsync(CancellationToken cancellationToken)
    {
        log.Trace("Queued Hosted Service is starting.");

        try
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                await _signal.WaitAsync(cancellationToken);

                var workItem = await _taskQueue.DequeueAsync(cancellationToken);

                Guid key = Guid.NewGuid();
                var t = Exec(workItem, key, cancellationToken);
                _tasks.TryAdd(key, t);
            }
        }
        catch (OperationCanceledException)
        {
            // fall through
        }

        log.Trace("Queued Hosted Service is stopping.");

        if (!_taskQueue.IsEmpty)
        {
            log.Warn($"background task queue not empty on shutdown: {_taskQueue.QueueDepth} remaining");
        }

        if (!_tasks.IsEmpty)
        {
            // give them as much time as the host will give us
            log.Warn("background tasks still processing (will wait as long as host allows)");
            await Task.WhenAll(_tasks.Values);
            log.Info("background tasks complete");
        }

        log.Trace("Queued Hosted Service is stopped.");
    }

    private async Task Exec(QueuedTask workItem, Guid key, CancellationToken cancellationToken)
    {
        try
        {
            StaticLogManager.LoadTraceInfo(workItem.TraceInfo);
            using (new TimeUtil.TimeLogger("Background Task", log))
            {
                await workItem.Func(cancellationToken);
            }
        }
        catch (Exception e)
        {
            log.Error(e, $"Error occurred executing {nameof(workItem)}.");
        }
        finally
        {
            _signal.Release();
            _tasks.TryRemove(key, out Task? _);
        }
    }
}
