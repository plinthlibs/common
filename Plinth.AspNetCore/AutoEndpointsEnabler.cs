using System.Reflection;
using Plinth.AspNetCore.AutoEndpoints.Version;
using Plinth.AspNetCore.AutoEndpoints.Diagnostics;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Plinth.AspNetCore.AutoEndpoints;
using Plinth.AspNetCore.AutoEndpoints.Client;

namespace Plinth.AspNetCore;

/// <summary>
/// Enabler for auto controllers
/// </summary>
public class AutoEndpointsEnabler
{
    internal AutoEndpointsEnabler() { }

    internal VersionConfig? _versionConfig;
    internal DiagnosticsConfig? _diagnosticsConfig;
    internal List<ClientConfig> _clientLogConfigList = [];

    private string? _componentName = null;

    /// <summary>
    /// Set this component's name for all auto controllers
    /// </summary>
    /// <param name="componentName">Product-Component, like 'MyApp-Core'</param>
    public AutoEndpointsEnabler SetComponentName(string componentName)
    {
        if (string.IsNullOrWhiteSpace(componentName)) throw new ArgumentNullException(nameof(componentName));
        _componentName = componentName;
        return this;
    }

    /// <summary>
    /// Enables the Version Auto Controller
    /// </summary>
    /// <param name="configure"></param>
    public AutoEndpointsEnabler EnableVersion(Action<VersionConfig>? configure = null)
    {
        _versionConfig = new VersionConfig(Assembly.GetCallingAssembly()) { ComponentName = _componentName };
        configure?.Invoke(_versionConfig);
        return this;
    }

    /// <summary>
    /// Enables the Diagnostics Auto Controller
    /// </summary>
    /// <param name="configure"></param>
    public AutoEndpointsEnabler EnableDiagnostics(Action<DiagnosticsConfig>? configure = null)
    {
        _diagnosticsConfig = new DiagnosticsConfig();
        configure?.Invoke(_diagnosticsConfig);
        return this;
    }

    /// <summary>
    /// Enables the Client Logging Auto Controller
    /// </summary>
    /// <param name="configure"></param>
    public AutoEndpointsEnabler EnableClient(Action<ClientConfig>? configure = null)
    {
        var clientLogConfig = new ClientConfig() { ComponentName = _componentName };
        configure?.Invoke(clientLogConfig);
        // don't enable middleware if no subcontrollers are enabled
        if (clientLogConfig.LoggingEnabled)
            _clientLogConfigList.Add(clientLogConfig);
        return this;
    }

    internal void AddLoggablePaths(List<string> loggablePaths)
    {
        if (_versionConfig != null)
            loggablePaths.Add(VersionConfig.LoggingUrlPrefix);
        if (_diagnosticsConfig != null)
            loggablePaths.Add(DiagnosticsConfig.LoggingUrlPrefix);

        foreach (var clientLogConfig in _clientLogConfigList)
        {
            if (clientLogConfig.LoggingLogWebRequests)
                loggablePaths.Add(clientLogConfig.LoggingUrlPrefix);
        }
    }

    internal void UseAutoEndpointMiddleware(IApplicationBuilder builder)
    {
        if (_versionConfig != null)
            builder.UseMiddleware<VersionMiddleware>(_versionConfig);
        if (_diagnosticsConfig != null)
            builder.UseMiddleware<DiagnosticsMiddleware>(_diagnosticsConfig);

        foreach (var clientLogConfig in _clientLogConfigList)
        {
            builder.UseMiddleware<ClientMiddleware>(clientLogConfig);
        }
    }

    internal AutoEndpointsHandlerCheck GetHandlerCheck()
    {
        var funcs = new List<Func<HttpContext, string?>>();

        if (_versionConfig != null)
            funcs.Add(VersionMiddleware.WillHandle);
        if (_diagnosticsConfig != null)
            funcs.Add(DiagnosticsMiddleware.WillHandle);

        foreach (var clientLogConfig in _clientLogConfigList)
        {
            funcs.Add((context) => ClientMiddleware.WillHandle(context, clientLogConfig));
        }

        return new AutoEndpointsHandlerCheck(funcs);
    }
}
