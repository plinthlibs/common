using Microsoft.Extensions.Options;
using Plinth.Security.Jwt;

namespace Plinth.AspNetCore.Jwt;

internal class JwtGenerationOptionsPostCreation : IPostConfigureOptions<JwtGenerationOptions>
{
    /// <summary>
    /// validate JwtGenerationOptions
    /// </summary>
    public void PostConfigure(string? name, JwtGenerationOptions options)
    {
        options.Validate();
    }
}
