using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using Plinth.AspNetCore.TokenAuth;
using Plinth.Security.Jwt;

namespace Plinth.AspNetCore.Jwt;

/// <summary>
/// Extensions for utilizing JWT auth in Startup.cs
/// </summary>
public static class JwtAuthHandlerExtensions
{
    /// <summary>
    /// Add Jwt auth during ConfigureServices() as the default scheme
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configureJwt">(required) action to configure jwt options</param>
    /// <param name="configureHandler">(optional) configuration method to configure JwtAuthHandlerOptions</param>
    public static IServiceCollection AddPlinthJwtAuth(this IServiceCollection services, Action<JwtGenerationOptions> configureJwt, Action<JwtAuthHandlerOptions>? configureHandler = null)
    {
        AddOptions(services, TokenAuthHandlerOptionsBase.DefaultScheme, configureJwt);
        services.AddAuthentication(TokenAuthHandlerOptionsBase.DefaultScheme)
            .AddScheme<JwtAuthHandlerOptions, JwtAuthHandler>(TokenAuthHandlerOptionsBase.DefaultScheme, null, configureHandler);
        return services;
    }

    /// <summary>
    /// Add Jwt auth during ConfigureServices() as a custom scheme
    /// </summary>
    /// <param name="services"></param>
    /// <param name="authScheme">Auth Scheme name</param>
    /// <param name="configureJwt">(required) action to configure jwt options</param>
    /// <param name="configureHandler">(optional) configuration method to configure JwtAuthHandlerOptions</param>
    public static IServiceCollection AddPlinthJwtAuth(this IServiceCollection services, string authScheme, Action<JwtGenerationOptions> configureJwt, Action<JwtAuthHandlerOptions>? configureHandler = null)
    {
        AddOptions(services, authScheme, configureJwt);
        services.AddAuthentication()
            .AddScheme<JwtAuthHandlerOptions, JwtAuthHandler>(authScheme, null, configureHandler);
        return services;
    }

    /// <summary>
    /// Add Jwt auth for SignalR Hubs.
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configureJwt">(required) action to configure jwt options</param>
    /// <param name="authScheme">name of auth scheme, defaults to "SignalR"</param>
    /// <param name="configureHandler">(optional) configuration method to configure JwtAuthHandlerOptions</param>
    /// <example>
    /// To use, put this on your hub class
    /// <code>[Authorize(AuthenticationSchemes = "SignalR")]</code>
    /// </example>
    public static IServiceCollection AddPlinthJwtAuthForSignalR(this IServiceCollection services, Action<JwtGenerationOptions> configureJwt, string authScheme = "SignalR", Action<JwtAuthHandlerOptions>? configureHandler = null)
    {
        if (authScheme == TokenAuthHandlerOptionsBase.DefaultScheme)
            throw new ArgumentException("signalR auth scheme cannot be the default scheme");

        AddOptions(services, authScheme, configureJwt);
        services.AddAuthentication()
            .AddScheme<JwtAuthHandlerOptions, JwtAuthHandler>(authScheme, null, config);
        return services;

        void config(JwtAuthHandlerOptions o)
        {
            configureHandler?.Invoke(o);
            // signalr client code uses 'access_token' as the query param variable
            o.CustomTokenSource ??= r => TokenAuthQueryStringHandler.GetTokenFromQueryString(r, "access_token");
            o.CustomTokenSourceFallsBack = true;
            // signalr auth should only be enabled on the hub, so we will check all requests
            o.UrlPathFilter ??= _ => true;
        }
    }

    private static void AddOptions(IServiceCollection services, string scheme, Action<JwtGenerationOptions> configTokenOptions)
    {
        services.TryAddSingleton<IPostConfigureOptions<JwtAuthHandlerOptions>, JwtAuthHandlerOptions.PostConfigureOptions>();
        services.TryAddSingleton<IPostConfigureOptions<JwtGenerationOptions>, JwtGenerationOptionsPostCreation>();
        services.TryAddSingleton(new JwtManager());

        var options = new JwtGenerationOptions();
        configTokenOptions(options);

        // unfortunately, this is the way to get the singleton instance possibly added above
        services
            .Where(sd => sd.ServiceType == typeof(JwtManager))
            .Select(sd => sd.ImplementationInstance)
            .OfType<JwtManager>()
            .First()
            .AddScheme(scheme, options);
    }
}
