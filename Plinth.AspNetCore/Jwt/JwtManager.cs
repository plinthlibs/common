using Plinth.AspNetCore.TokenAuth;
using Plinth.Security.Jwt;
using System.Collections.Concurrent;

namespace Plinth.AspNetCore.Jwt;

/// <summary>
/// Injectable manager for creating tokens
/// </summary>
public class JwtManager
{
    private readonly ConcurrentDictionary<string, Data> _managers = new();

    private sealed class Data
    {
        public JwtGenerator Generator { get; set; } = null!;
        public JwtValidator Validator { get; set; } = null!;
    }

    internal JwtManager()
    {
    }

    internal void AddScheme(string scheme, JwtGenerationOptions options)
    {
        ArgumentNullException.ThrowIfNull(scheme);
        ArgumentNullException.ThrowIfNull(options);

        var validator = new JwtValidator(options);
        var generator = new JwtGenerator(options, validator);

        _managers[scheme] = new Data { Generator = generator, Validator = validator };
    }

    private Data GetManagerForScheme(string scheme)
    {
        if (_managers.TryGetValue(scheme, out var manager))
            return manager;

        // if using the Generator/Validator property and only one non-default scheme is added, use it
        if (_managers.Count == 1 && scheme == TokenAuthHandlerOptionsBase.DefaultScheme)
            return _managers.Values.First();

        throw new ArgumentException($"scheme '{scheme}' is not registered", nameof(scheme));
    }

    /// <summary>
    /// Get a generator for the default scheme
    /// </summary>
    public JwtGenerator Generator
        => GeneratorForScheme(TokenAuthHandlerOptionsBase.DefaultScheme);

    /// <summary>
    /// Get a generator for a custom scheme
    /// </summary>
    public JwtGenerator GeneratorForScheme(string scheme)
        => GetManagerForScheme(scheme).Generator;

    /// <summary>
    /// Get a generator for the default scheme
    /// </summary>
    internal JwtValidator Validator
        => ValidatorForScheme(TokenAuthHandlerOptionsBase.DefaultScheme);

    /// <summary>
    /// Get a generator for a custom scheme
    /// </summary>
    internal JwtValidator ValidatorForScheme(string scheme)
        => GetManagerForScheme(scheme).Validator;

    internal void ClearForTests()
    {
        _managers.Clear();
    }
}
