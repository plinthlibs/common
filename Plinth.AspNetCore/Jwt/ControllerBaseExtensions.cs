using System.Collections.Immutable;
using Microsoft.AspNetCore.Mvc;
using Plinth.Security.Jwt;

namespace Plinth.AspNetCore.Jwt;

/// <summary>
/// Extensions for API Controllers
/// </summary>
public static class ControllerBaseExtensions
{
    /// <summary>
    /// Get the jwt that was extracted from the Authorization Header
    /// </summary>
    /// <param name="controller"></param>
    /// <exception cref="Security.SecurityException">if user is not authenticated</exception>
    public static string GetJwt(this ControllerBase controller)
        => controller.HttpContext.GetJwt();

    /// <summary>
    /// Try to get the jwt that was extracted from the Authorization Header
    /// </summary>
    /// <param name="controller"></param>
    /// <returns>null if user is not authenticated</returns>
    public static string? TryGetJwt(this ControllerBase controller)
        => controller.HttpContext.TryGetJwt();

    /// <summary>
    /// Get the user name of the authenticated user.
    /// </summary>
    /// <param name="controller"></param>
    /// <remarks>User.Identity.Name also works</remarks>
    /// <exception cref="Security.SecurityException">if user is not authenticated</exception>
    public static string? GetAuthenticatedUserName(this ControllerBase controller)
        => Check(controller, () => controller.User.Identity!.Name) ?? throw new InvalidDataException("authenticated user name not set");

    /// <summary>
    /// Try to get the user name of the authenticated user.
    /// </summary>
    /// <param name="controller"></param>
    /// <remarks>User.Identity.Name also works</remarks>
    /// <returns>null if user is not authenticated</returns>
    public static string? TryGetAuthenticatedUserName(this ControllerBase controller)
        => SafeCheck(controller, () => controller.User.Identity!.Name);

    /// <summary>
    /// Get the user guid of the authenticated user.
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    /// <remarks>Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value) also works</remarks>
    /// <exception cref="Security.SecurityException">if user is not authenticated</exception>
    public static Guid GetAuthenticatedUserId(this ControllerBase controller)
        => Check(controller, () => controller.User.JwtUserId());

    /// <summary>
    /// Try to get the user guid of the authenticated user.
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    /// <remarks>Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value) also works</remarks>
    /// <returns>null if user is not authenticated</returns>
    public static Guid? TryGetAuthenticatedUserId(this ControllerBase controller)
        => SafeCheck<Guid?>(controller, () => controller.User.JwtUserId());

    /// <summary>
    /// Get the session guid of the authenticated user.
    /// </summary>
    /// <param name="controller"></param>
    /// <exception cref="Security.SecurityException">if user is not authenticated</exception>
    /// <exception cref="InvalidDataException">if token does not have a session guid</exception>
    public static Guid GetAuthenticatedUserSessionGuid(this ControllerBase controller)
        => Check(controller, () => controller.User.JwtSessionGuid() ?? throw new InvalidDataException("user token does not have a session id"));

    /// <summary>
    /// Try to get the session guid of the authenticated user.
    /// </summary>
    /// <param name="controller"></param>
    /// <returns>null if user is not authenticated</returns>
    public static Guid? TryGetAuthenticatedUserSessionGuid(this ControllerBase controller)
        => SafeCheck(controller, () => controller.User.JwtSessionGuid());

    /// <summary>
    /// Get the roles assigned to the authenticated user.
    /// </summary>
    /// <param name="controller"></param>
    /// <remarks>User.Claims.Where(c => c.Type == ClaimTypes.Role) also works</remarks>
    /// <exception cref="Security.SecurityException">if user is not authenticated</exception>
    public static ISet<string> GetAuthenticatedUserRoles(this ControllerBase controller)
        => Check(controller, () => controller.User.JwtRoles().ToImmutableHashSet());

    /// <summary>
    /// Try to get the roles assigned to the authenticated user.
    /// </summary>
    /// <param name="controller"></param>
    /// <remarks>User.Claims.Where(c => c.Type == ClaimTypes.Role) also works</remarks>
    /// <returns>empty if user is not authenticated</returns>
    public static ISet<string> TryGetAuthenticatedUserRoles(this ControllerBase controller)
        => (controller.User.Identity?.IsAuthenticated ?? false) ? controller.GetAuthenticatedUserRoles() : ImmutableHashSet<string>.Empty;

    /// <summary>
    /// Get an encapsulation object of the secure token info for the authenticated user
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    public static AuthTokenInfo GetAuthTokenInfo(this ControllerBase controller)
        => Check(controller, () => new AuthTokenInfo(controller));

    /// <summary>
    /// Get an encapsulation object of the secure token info for the authenticated user
    /// </summary>
    /// <param name="controller"></param>
    /// <returns>null if user is not authenticated</returns>
    public static AuthTokenInfo? TryGetAuthTokenInfo(this ControllerBase controller)
        => (controller.User.Identity?.IsAuthenticated ?? false) ? new AuthTokenInfo(controller) : null;

    private static T Check<T>(ControllerBase controller, Func<T> val)
    {
        if (controller.User.Identity?.IsAuthenticated ?? false)
            return val();
        throw new Security.SecurityException("user is not authenticated");
    }

    private static T SafeCheck<T>(ControllerBase controller, Func<T> val)
    {
        try
        {
            if (controller.User.Identity?.IsAuthenticated ?? false)
                return val();
        }
        catch (InvalidDataException)
        {
            // ignore, return default
        }
        return default!;
    }
}

/// <summary>
/// Encapsulation of the secure token info for the authenticated user, useful for passing into logic classes.
/// </summary>
/// <remarks>not thread safe</remarks>
public class AuthTokenInfo
{
    private ISet<string>? _roles;

    /// <summary>
    /// Access to the controller for derived classes
    /// </summary>
    protected ControllerBase Controller { get; }

    /// <summary>
    /// Create from a controller
    /// </summary>
    /// <param name="controller"></param>
    protected internal AuthTokenInfo(ControllerBase controller)
    {
        Controller = controller;
    }

    /// <summary>
    /// The user name of the authenticated user.
    /// </summary>
    public string? UserName => Controller.GetAuthenticatedUserName();

    /// <summary>
    /// The user id of the authenticated user.
    /// </summary>
    public Guid UserId => Controller.GetAuthenticatedUserId();

    /// <summary>
    /// The session guid of the authenticated user.
    /// </summary>
    public Guid SessionGuid => Controller.GetAuthenticatedUserSessionGuid();

    /// <summary>
    /// The roles assigned to the authenticated user.
    /// </summary>
    public ISet<string> Roles
    {
        get
        {
            _roles ??= Controller.User.JwtRoles().ToImmutableHashSet();
            return _roles;
        }
    }
}
