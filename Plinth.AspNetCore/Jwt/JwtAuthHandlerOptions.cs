using Microsoft.Extensions.Options;
using Plinth.AspNetCore.TokenAuth;

namespace Plinth.AspNetCore.Jwt;

/// <summary>
/// Options for jwt token authentication handler
/// </summary>
public class JwtAuthHandlerOptions : TokenAuthHandlerOptionsBase
{
    // protected override void Validate() is called on every request, the post configure is called only once

    // this gets called once after the options are configured
    internal class PostConfigureOptions : IPostConfigureOptions<JwtAuthHandlerOptions>
    {
        public void PostConfigure(string? name, JwtAuthHandlerOptions options)
        {
            options.ValidateOptions();
        }
    }
}

