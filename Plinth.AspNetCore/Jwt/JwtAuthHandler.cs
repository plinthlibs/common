using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Plinth.AspNetCore.TokenAuth;
using Plinth.Security.Jwt;
using Plinth.Common.Logging;

namespace Plinth.AspNetCore.Jwt;

/// <summary>
/// Authentication Handler for using plinth jwt tokens for authentication and authorization
/// </summary>
internal class JwtAuthHandler : TokenAuthHandlerBase<JwtAuthHandlerOptions>
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    internal const string ItemKeyAuthenticationToken = "plinthlibs/Jwt";

    private readonly JwtManager _tokenManager;

    public JwtAuthHandler(
        JwtManager tokenManager,
        IOptionsMonitor<JwtAuthHandlerOptions> options,
        ILoggerFactory logger,
        UrlEncoder encoder
#if NET8_0_OR_GREATER
        )
        : base(options, logger, encoder)
#else
        ,ISystemClock clock)
        : base(options, logger, encoder, clock)
#endif
    {
        _tokenManager = tokenManager ?? throw new ArgumentNullException(nameof(tokenManager));
    }

    protected override AuthenticateResult ValidateToken(HttpRequest request, string tokenValue, bool requestLoggingEnabled)
    {
        try
        {
            var cp = _tokenManager.ValidatorForScheme(Scheme.Name).Validate(tokenValue, requestLoggingEnabled);

            RequestTraceProperties.SetRequestTraceUserName(cp.JwtUserName());
            if (cp.JwtSessionGuid() != null)
                RequestTraceProperties.SetRequestTraceSessionId(cp.JwtSessionGuid().ToString()!);

            Request.HttpContext.Items[ItemKeyAuthenticationToken] = tokenValue;

            return AuthenticateResult.Success(new AuthenticationTicket(cp, Scheme.Name));
        }
        catch (SecurityTokenExpiredException e)
        {
            if (requestLoggingEnabled)
                log.Info($"token expired at {e.Expires}");
            return AuthenticateResult.Fail($"token expired at {e.Expires}");
        }
    }
}
