using Microsoft.AspNetCore.Http;
using Plinth.AspNetCore.TokenAuth;

namespace Plinth.AspNetCore.Jwt;

/// <summary>
/// Extensions for HttpContext
/// </summary>
public static class HttpContextBaseExtensions
{
    /// <summary>
    /// Get the original JWT that was extracted from the Authorization Header
    /// </summary>
    /// <param name="context"></param>
    /// <exception cref="Security.SecurityException">if user is not authenticated</exception>
    public static string GetJwt(this HttpContext context)
        => Check(context, () => (string?)context.Items[JwtAuthHandler.ItemKeyAuthenticationToken]) ?? throw new InvalidDataException("authentication token not set in context");

    /// <summary>
    /// Try to get the original JWT that was extracted from the Authorization Header
    /// </summary>
    /// <param name="context"></param>
    /// <return>null if user is not authenticated</return>
    public static string? TryGetJwt(this HttpContext context)
        => context.Items.TryGetValue(JwtAuthHandler.ItemKeyAuthenticationToken, out var o)
            ? o as string
            : null;

    private static T Check<T>(HttpContext context, Func<T> val)
    {
        if (context.User.Identity?.IsAuthenticated ?? false)
            return val();
        throw new Security.SecurityException("user is not authenticated");
    }
}
