using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Plinth.Security.Crypto;

namespace Plinth.AspNetCore.TokenAuth;

/// <summary>
/// Options for SecureToken authentication handler
/// </summary>
public class TokenAuthHandlerOptionsBase : AuthenticationSchemeOptions
{
    internal const string DefaultScheme = "Bearer";

    /// <summary>
    /// A list of url prefixes for which the Authorization header will be checked.  All must start with slash '/'.  Defaults to '/api'
    /// </summary>
    /// <remarks>
    /// If left null (and UrlPathFilter is null), the default is '/api'
    /// NOTE: this only controls whether a value Authorization header + token is checked for Authentication
    ///       this has nothing to do with Authorization.  If an endpoint specifies [Authorize] but you filter out this check, you'll get 401
    /// </remarks>
    public IList<string>? UrlPathPrefixes { get; set; }

    /// <summary>
    /// A function for determining if the Authorization header should be checked
    /// </summary>
    /// <remarks>
    /// If left null (and UrlPathPrefixes is null), the default is '/api'
    /// NOTE: this only controls whether a value Authorization header + token is checked for Authentication
    ///       this has nothing to do with Authorization.  If an endpoint specifies [Authorize] but you filter out this check, you'll get 401
    /// </remarks>
    public Func<HttpRequest, bool>? UrlPathFilter { get; set; }

    /// <summary>
    /// A function for custom handling to get the token from the request
    /// </summary>
    /// <remarks>Default is from 'Authorization: Bearer {token}'.  Return null for none</remarks>
    public Func<HttpRequest, string?>? CustomTokenSource { get; set; }

    /// <summary>
    /// If CustomTokenSource is provided and cannot find a token, this flag determines if the handler falls back to default bearer token behavior
    /// </summary>
    public bool CustomTokenSourceFallsBack { get; set; } = false;

    /// <summary>
    /// The realm specified in the 401/Unauthorized authentication challenge, defaults to 'secure api'
    /// </summary>
    public string WWWAuthenticateRealm { get; set; } = "secure api";

    private protected virtual void ValidateOptions()
    {
        if (UrlPathPrefixes != null && UrlPathFilter != null)
            throw new ArgumentException("UrlPathPrefixes and UrlPathFilter cannot both be specified");

        if (UrlPathPrefixes != null)
        {
            if (UrlPathPrefixes.Count == 0)
                throw new ArgumentException("UrlPathPrefixes is specified but empty");
            foreach (var prefix in UrlPathPrefixes)
                if (!prefix.StartsWith('/'))
                    throw new ArgumentException($"UrlPathPrefixes '{prefix}' does not start with slash '/'");
        }
    }
}
