using Microsoft.Extensions.Logging;
using System.Diagnostics.CodeAnalysis;
using System.Net.Http.Headers;

namespace Plinth.AspNetCore.TokenAuth;

internal static class TokenHandlerUtil
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    internal const string ItemKeyAuthenticationToken = "plinthlibs/AuthToken";
    internal const string PlinthAuthorizationHeader = "X-Plinth-Auth";

    public static bool GetTokenFromAuthHeader(string? authorization, string scheme, string? plinthHeader, bool loggingEnabled, [MaybeNullWhen(false)] out string token)
    {
        token = null;

        // If no authorization header found, try getting custom plinth-specific header which may be used for debugging purposes
        if (string.IsNullOrEmpty(authorization) && !string.IsNullOrWhiteSpace(plinthHeader))
        {
            plinthHeader = plinthHeader.Trim();
            if (AuthenticationHeaderValue.TryParse(plinthHeader, out var plinthHeaderVal))
                plinthHeader = plinthHeaderVal.Parameter ?? plinthHeaderVal.Scheme;

            token = plinthHeader;
            return true;
        }

        // If no authorization header found, nothing to process further
        if (string.IsNullOrEmpty(authorization))
        {
            if (loggingEnabled) log.Trace("missing authorization header");
            return false;
        }

        if (!AuthenticationHeaderValue.TryParse(authorization, out var authHeaderVal))
        {
            if (loggingEnabled) log.Debug($"unparseable authorization header: {authorization}");
            return false;
        }

        if (authHeaderVal.Scheme.Equals(scheme, StringComparison.OrdinalIgnoreCase))
        {
            token = authHeaderVal.Parameter;
        }
        else
        {
            if (loggingEnabled) log.Debug($"authorization header does not contain scheme {scheme}, will try {TokenAuthHandlerOptionsBase.DefaultScheme}");

            // allow default scheme to work as a backup
            if (authHeaderVal.Scheme.Equals(TokenAuthHandlerOptionsBase.DefaultScheme, StringComparison.OrdinalIgnoreCase))
            {
                token = authHeaderVal.Parameter;
            }
        }

        // If no token found, no further work possible
        if (string.IsNullOrEmpty(token))
        {
            if (loggingEnabled) log.Debug($"unrecognized authorization header: {authorization}");
            return false;
        }

        return true;
    }
}
