using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Plinth.AspNetCore.TokenAuth;

/// <summary>
/// utility for checking query string parameter if auth header isn't provided
/// mostly useful for signalr/websockets which can't send headers
/// </summary>
internal static class TokenAuthQueryStringHandler
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    public static string? GetTokenFromQueryString(HttpRequest request, string queryParam)
    {
        log.Trace($"checking authorization query param '{queryParam}'");

        if (request.Query.TryGetValue(queryParam, out var sv) && sv.Count == 1)
        {
            return sv[0];
        }

        log.Trace($"missing authorization query param '{queryParam}'");

        return null;
    }
}
