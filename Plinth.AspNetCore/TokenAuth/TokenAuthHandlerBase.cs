using System.Diagnostics.CodeAnalysis;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;

namespace Plinth.AspNetCore.TokenAuth;

/// <summary>
/// A new concept with ASP.NET Core is that authentication is separate from authorization
/// So every request will come in here (because this is set as the default scheme), and the result will either be 'none', 'fail', or 'success'
/// Regardless, the request will continue until it hits an Authorization check, and that is where it might be stopped
/// </summary>
internal abstract class TokenAuthHandlerBase<TOptions> : AuthenticationHandler<TOptions> where TOptions : TokenAuthHandlerOptionsBase, new()
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    protected TokenAuthHandlerBase(IOptionsMonitor<TOptions> options, ILoggerFactory logger, UrlEncoder encoder
#if NET8_0_OR_GREATER
        )
        : base(options, logger, encoder)
#else
        ,ISystemClock clock)
        : base(options, logger, encoder, clock)
#endif
    {
    }

    protected virtual bool AuthShouldBeChecked()
    {
        if (Options.UrlPathFilter != null)
            return Options.UrlPathFilter(Request);

        if (Options.UrlPathPrefixes != null)
        {
            foreach (var prefix in Options.UrlPathPrefixes)
            {
                if (Request.Path.StartsWithSegments(prefix))
                    return true;
            }

            return false;
        }

        // default is /api only
        return Request.Path.StartsWithSegments("/api");
    }

    /// <summary>
    /// Return an AuthenticationResult with either fail, success, or no result
    /// </summary>
    protected abstract AuthenticateResult ValidateToken(HttpRequest request, string tokenValue, bool requestLoggingEnabled);

    // this is called on every request
    protected override Task<AuthenticateResult> HandleAuthenticateAsync()
    {
        if (!AuthShouldBeChecked())
            return Task.FromResult(AuthenticateResult.NoResult());

        var loggingEnabled = !Context.Items.ContainsKey("plinth.request-not-logged");

        log.Trace("Authenticating request");

        try
        {
            if (!GetTokenFromHeader(out var token, loggingEnabled))
                return Task.FromResult(AuthenticateResult.NoResult());

            return Task.FromResult(ValidateToken(Request, token, loggingEnabled));
        }
        catch (Exception e)
        {
            log.Warn(e, "caught while processing auth header");
            return Task.FromResult(AuthenticateResult.Fail("invalid authorization data"));
        }
    }

    protected override async Task HandleChallengeAsync(AuthenticationProperties properties)
    {
        await HandleAuthenticateOnceSafeAsync();

        Response.StatusCode = 401;
        Response.Headers.Append(HeaderNames.WWWAuthenticate, $"WWW-Authenticate: Bearer realm=\"{Options.WWWAuthenticateRealm}\"");
    }

    private bool GetTokenFromHeader([MaybeNullWhen(false)] out string token, bool loggingEnabled)
    {
        token = null;

        if (Options.CustomTokenSource != null)
        {
            token = Options.CustomTokenSource.Invoke(Request);
            if (string.IsNullOrEmpty(token))
            {
                if (!Options.CustomTokenSourceFallsBack)
                {
                    if (loggingEnabled)
                        log.Debug($"custom token source did not find token");
                    return false;
                }
                // falling back to bearer token check
            }
            else
                return true;
        }

        string? authorization = Request.Headers.Authorization;
        string? plinthAuth = Request.Headers[TokenHandlerUtil.PlinthAuthorizationHeader];

        return TokenHandlerUtil.GetTokenFromAuthHeader(authorization, Scheme.Name, plinthAuth, loggingEnabled, out token);
    }
}
