#!/bin/sh

# run with no arguments to run all test
# run with 1 argument to pass '--filter {arg}' to 'dotnet test'

if [ $# -eq 1 ]; then
    docker build -f ./test.dockerfile --build-arg test_arg1=--filter --build-arg test_arg2=$1 --add-host=host.docker.internal:host-gateway --progress=plain .
else
    docker build -f ./test.dockerfile --build-arg test_arg1="" --build-arg test_arg2="" --add-host=host.docker.internal:host-gateway --progress=plain .
fi
