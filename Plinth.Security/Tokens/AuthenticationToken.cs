using Plinth.Security.Crypto;
using Plinth.Serialization;
using Newtonsoft.Json;
using System.Text;
using Microsoft.Extensions.Logging;
using System.Diagnostics.CodeAnalysis;

namespace Plinth.Security.Tokens;

/// <summary>
/// Represents an authentication token that is securely encrypted,
/// and contains a subject, start/end times, and arbitrary data fields
///
/// Loosely based on JWT
/// </summary>
public partial class AuthenticationToken
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    /// <summary>Encrypted token</summary>
    [JsonIgnore]
    public string Token { get; }

    /// <summary>When the token expires</summary>
    public DateTimeOffset Expires { get; }

    /// <summary>When the token was issued</summary>
    public DateTimeOffset Issued { get; }

    /// <summary>When the token was originally issued</summary>
    public DateTimeOffset OriginalIssued => _origIssue ?? Issued;
    private readonly DateTimeOffset? _origIssue;

    /// <summary>guid key for the token</summary>
    public Guid SubjectGuid { get; }

    /// <summary>name key for the token</summary>
    public string SubjectName { get; }

    /// <summary>session itentifier</summary>
    public Guid SessionGuid { get; }

    /// <summary>subject roles</summary>
    public IReadOnlyList<string>? Roles { get; }

    /// <summary>custom data values, may be null, must be serializable to JSON</summary>
    public IReadOnlyDictionary<string, object?>? Data { get; }

    /// <summary>
    /// Determine if the token is expired
    /// </summary>
    public bool IsExpired => Expires < DateTimeOffset.UtcNow;

    /// <summary>
    /// Construct a new token
    /// </summary>
    /// <param name="masterKey">key used to derive other keys (this or secureData)</param>
    /// <param name="secureData">encryption tool (this or masterKey)</param>
    /// <param name="subjectGuid">the guid for the subject (user or app)</param>
    /// <param name="subjectName">key name for the subject (like username)</param>
    /// <param name="lifetime">how long the token is valid</param>
    /// <param name="loggingEnabled">log the contents of the token</param>
    /// <param name="originalIssue">(optional) original issue date</param>
    /// <param name="sessionGuid">(optional) session identifier</param>
    /// <param name="roles">(optional) list of roles</param>
    /// <param name="data">optional custom data values, must be serializable to JSON</param>
    internal AuthenticationToken(
        byte[]? masterKey,
        ISecureData? secureData,
        Guid subjectGuid, 
        string subjectName, 
        TimeSpan lifetime,
        bool loggingEnabled,
        DateTimeOffset? originalIssue = null, 
        Guid? sessionGuid = null,
        List<string>? roles = null,
        IDictionary<string, object?>? data = null)
    {
        // get issued and expires, but remove sub-second values
        DateTimeOffset issued = DateTimeOffset.UtcNow;
        DateTimeOffset expires = issued.Add(lifetime);
        Guid session = sessionGuid ?? Guid.NewGuid();

        var token = new TokenContents()
        {
            sub = ConvertGuidToBase64(subjectGuid),
            snm = subjectName,
            exp = expires.ToUnixTimeSeconds(),
            iat = issued.ToUnixTimeSeconds(),
            ori = originalIssue?.ToUnixTimeSeconds(),
            ses = ConvertGuidToBase64(session),
            rls = roles,
            data = data
        };

        string tokenContents = JsonUtil.SerializeObject(token);

        if (loggingEnabled)
            LogDefines.LogTokenCreate(log, tokenContents);

        Token = (masterKey == null
            ? secureData!.EncryptToBase64(Encoding.UTF8.GetBytes(tokenContents))
            : DataEncryption.EncryptToUrlSafeToken(masterKey, 0, Encoding.UTF8.GetBytes(tokenContents)))
            ?? throw new InvalidOperationException("encryption returned null, possible configuration error");
        Expires = expires;
        Issued = issued;
        _origIssue = originalIssue;
        SubjectGuid = subjectGuid;
        SubjectName = subjectName;
        SessionGuid = session;
        Roles = roles;
        Data = data == null ? null : new ReadOnlyDictionaryWrapper<string,object?>(data);
    }

    /// <summary>
    /// Construct a token from an existing encrypted token
    /// </summary>
    /// <param name="masterKey">key used to derive other keys</param>
    /// <param name="encryptedToken">token from user</param>
    /// <param name="loggingEnabled">true to log token contents, false for silence</param>
    /// <param name="throwOnExpired">true will throw <see cref="ExpiredTokenException"/> if token is expired, false will set IsExpired property</param>
    /// <exception cref="InvalidTokenException">if token has been tampered with</exception>
    /// <exception cref="ExpiredTokenException">if token is expired and <paramref name="throwOnExpired"/> is true</exception>
    public static AuthenticationToken FromEncryptedToken(byte[] masterKey, string encryptedToken, bool loggingEnabled = true, bool throwOnExpired = true)
        => new(masterKey, null, encryptedToken, loggingEnabled, throwOnExpired);

    /// <summary>
    /// Construct a token from an existing encrypted token
    /// </summary>
    /// <param name="secureData">ISecureData for decryption</param>
    /// <param name="encryptedToken">token from user</param>
    /// <param name="loggingEnabled">true to log token contents, false for silence</param>
    /// <param name="throwOnExpired">true will throw <see cref="ExpiredTokenException"/> if token is expired, false will set IsExpired property</param>
    /// <exception cref="InvalidTokenException">if token has been tampered with</exception>
    /// <exception cref="ExpiredTokenException">if token is expired and <paramref name="throwOnExpired"/> is true</exception>
    public static AuthenticationToken FromEncryptedToken(ISecureData secureData, string encryptedToken, bool loggingEnabled = true, bool throwOnExpired = true)
        => new(null, secureData, encryptedToken, loggingEnabled, throwOnExpired);

    private AuthenticationToken(byte[]? masterKey, ISecureData? secureData, string encryptedToken, bool loggingEnabled, bool throwOnExpired)
    {
        Token = encryptedToken;

        byte[] decryptedToken = (masterKey == null 
            ? secureData!.DecryptBase64(encryptedToken)
            : DataEncryption.DecryptUrlSafeToken(masterKey, encryptedToken))
            ?? throw new InvalidTokenException("token could not be decrypted");

        var tokenContents = Encoding.UTF8.GetString(decryptedToken);
        if (loggingEnabled) LogDefines.LogTokenDecrypt(log, tokenContents);

        var authToken = JsonUtil.DeserializeObject<TokenContents>(tokenContents)
            ?? throw new InvalidTokenException("token could not be parsed: " + tokenContents);

        Issued = DateTimeOffset.FromUnixTimeSeconds(authToken.iat);
        _origIssue = authToken.ori.HasValue ? DateTimeOffset.FromUnixTimeSeconds(authToken.ori.Value) : null;
        Expires = DateTimeOffset.FromUnixTimeSeconds(authToken.exp);

        var tokenAge = DateTimeOffset.UtcNow - Expires;

        if (loggingEnabled)
        {
            if (tokenAge > TimeSpan.Zero)
                LogDefines.LogTokenExpired(log, tokenAge);
            else
                LogDefines.LogTokenValid(log, tokenAge.Negate());
        }

        if (throwOnExpired && tokenAge > TimeSpan.Zero)
            throw new ExpiredTokenException($"token expired {tokenAge} ago");

        SubjectGuid = new Guid(Convert.FromBase64String(authToken.sub));
        SubjectName = authToken.snm;
        SessionGuid = new Guid(Convert.FromBase64String(authToken.ses));
        Roles = authToken.rls;
        Data = authToken.data == null ? null : new ReadOnlyDictionaryWrapper<string,object?>(authToken.data);
    }

    /// <summary>
    /// Utility method to convert a Guid to a Base64 string.
    /// These are smaller than the hex variety
    /// TWzXS6m4tUuW/l2y52fo3Q==
    /// 96dd5f74-2c02-4f55-9873-b13504123222
    /// </summary>
    /// <param name="g"></param>
    /// <returns></returns>
    public static string ConvertGuidToBase64(Guid g)
    {
        return Convert.ToBase64String(g.ToByteArray());
    }

    /// <summary>
    /// Get a Base64 encoded Guid from a token data field
    /// Base64 encoded guids are smaller than the hex encoding
    /// </summary>
    /// <param name="keyName"></param>
    /// <returns>Guid or null</returns>
    public Guid? GetBase64Guid(string keyName)
    {
        var base64str = GetDataField<string>(keyName);
        if (base64str == null)
            return null;

        try
        {
            return new Guid(Convert.FromBase64String(base64str));
        }
        catch (FormatException e)
        {
            log.Warn($"invalid base64 string or invalid guid bytes in token param {keyName}: {e.Message}");
            return null;
        }
    }

    /// <summary>
    /// Get a field from a token's Data dictionary
    /// </summary>
    /// <typeparam name="T">type of the field value</typeparam>
    /// <param name="keyName">key name in the Data dictionary</param>
    /// <param name="defaultVal">(optional) value if key not found</param>
    /// <returns></returns>
    public T? GetDataField<T>(string keyName, T? defaultVal = default)
    {
        if (Data == null) return defaultVal;
        return TokenUtil.GetDeserializedValue(Data, keyName, defaultVal);
    }

    /// <summary>
    /// Try to get a field from a token's Data dictionary
    /// </summary>
    /// <typeparam name="T">type of the field value</typeparam>
    /// <param name="keyName">key name in the Data dictionary</param>
    /// <param name="value">(out) if found, field value is converted and written</param>
    /// <returns>true if found and converted successfully, false otherwise</returns>
    public bool TryGetDataField<T>(string keyName, [MaybeNullWhen(false)] out T value)
    {
        if (Data == null)
        {
            value = default;
            return false;
        }
        return TokenUtil.TryGetDeserializedValue(Data, keyName, out value);
    }

    /// <summary>
    /// The internal values within a token, modeled after a JSON Web Token
    /// </summary>
    private class TokenContents
    {
        /// <summary>
        /// The subject, for us: the user guid, base64 encoded
        /// </summary>
#pragma warning disable IDE1006 // Naming Styles
        public string sub { get; set; } = null!;

        /// <summary>
        /// 
        /// The subject name
        /// </summary>
        public string snm { get; set; } = null!;

        /// <summary>
        /// Expiration (seconds since unix epoch)
        /// </summary>
        public long exp { get; set; }

        /// <summary>
        /// Issued At (seconds since unix epoch)
        /// </summary>
        public long iat { get; set; }

        /// <summary>
        /// Original Issued At (seconds since unix epoch)
        /// </summary>
        public long? ori { get; set; }

        /// <summary>
        /// The session identifier
        /// </summary>
        public string ses { get; set; } = null!;

        /// <summary>
        /// Roles
        /// </summary>
        public List<string>? rls { get; set; }

        /// <summary>
        /// custom data values, may be null
        /// </summary>
        public IDictionary<string, object?>? data { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }

    internal class ReadOnlyDictionaryWrapper<TKey, TValue> : IReadOnlyDictionary<TKey, TValue>
    {
        private readonly IDictionary<TKey, TValue> _dictionary;

        public ReadOnlyDictionaryWrapper(IDictionary<TKey, TValue> dictionary)
        {
            _dictionary = dictionary ?? throw new ArgumentNullException(nameof(dictionary));
        }

        public bool ContainsKey(TKey key) => _dictionary.ContainsKey(key);

        public IEnumerable<TKey> Keys => _dictionary.Keys;

        public bool TryGetValue(TKey key, [MaybeNullWhen(false)] out TValue value)
        {
            var result = _dictionary.TryGetValue(key, out TValue? v);
            value = v;
            return result;
        }

        public IEnumerable<TValue> Values => _dictionary.Values;

        public TValue this[TKey key] => _dictionary[key];

        public int Count => _dictionary.Count;

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
            => _dictionary.Select(x => new KeyValuePair<TKey, TValue>(x.Key, x.Value)).GetEnumerator();

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() => GetEnumerator();
    }

    private static partial class LogDefines
    {
        [LoggerMessage(0, LogLevel.Debug, "create: token contents: {TokenContents}", EventName = "TokenCreate")]
        public static partial void LogTokenCreate(ILogger logger, string? tokenContents);

        [LoggerMessage(1, LogLevel.Debug, "decrypt: token contents: {TokenContents}", EventName = "TokenDecrypt")]
        public static partial void LogTokenDecrypt(ILogger logger, string? tokenContents);

        [LoggerMessage(2, LogLevel.Debug, "token is expired, it expired {Age} ago", EventName = "TokenExpired")]
        public static partial void LogTokenExpired(ILogger logger, TimeSpan age);

        [LoggerMessage(3, LogLevel.Debug, "token is valid, good for {Age}", EventName = "TokenValid")]
        public static partial void LogTokenValid(ILogger logger, TimeSpan age);
    }
}
