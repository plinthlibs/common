﻿using Plinth.Serialization;

namespace Plinth.Security.Tokens;

/// <summary>
/// Utilities for Tokens
/// </summary>
public static class TokenUtil
{
    /// <summary>
    /// Get a field from an IReadOnlyDictionary deserialized from json
    /// </summary>
    /// <typeparam name="T">type of the converted value</typeparam>
    /// <param name="jsonDict">a deserialized json dictionary</param>
    /// <param name="keyName">key name in the dictionary</param>
    /// <param name="defaultVal">(optional) value if key not found</param>
    /// <returns></returns>
    public static T? GetDeserializedValue<T>(IReadOnlyDictionary<string, object?> jsonDict, string keyName, T? defaultVal = default)
        => GetDeserializedValue(jsonDict.TryGetValue, keyName, defaultVal);

    /// <summary>
    /// Get a value from a Dictionary deserialized from json
    /// </summary>
    /// <typeparam name="T">type of the converted value</typeparam>
    /// <param name="jsonDict">a deserialized json dictionary</param>
    /// <param name="keyName">key name in the dictionary</param>
    /// <param name="defaultVal">(optional) value if key not found</param>
    /// <returns></returns>
    public static T? GetDeserializedValue<T>(Dictionary<string, object?> jsonDict, string keyName, T? defaultVal = default)
        => GetDeserializedValue(jsonDict.TryGetValue, keyName, defaultVal);

    /// <summary>
    /// Get a value from an IDictionary deserialized from json
    /// </summary>
    /// <typeparam name="T">type of the converted value</typeparam>
    /// <param name="jsonDict">a deserialized json dictionary</param>
    /// <param name="keyName">key name in the dictionary</param>
    /// <param name="defaultVal">(optional) value if key not found</param>
    /// <returns></returns>
    public static T? GetDeserializedValue<T>(IDictionary<string, object?> jsonDict, string keyName, T? defaultVal = default)
        => GetDeserializedValue(jsonDict.TryGetValue, keyName, defaultVal);

    /// <summary>
    /// Try to get a field from an IReadOnlyDictionary deserialized from json
    /// </summary>
    /// <typeparam name="T">type of the converted value</typeparam>
    /// <param name="jsonDict">a deserialized json dictionary</param>
    /// <param name="keyName">key name in the dictionary</param>
    /// <param name="value">(out) if found, dictionary value is converted and written</param>
    /// <returns>true if found and converted successfully, false otherwise</returns>
    public static bool TryGetDeserializedValue<T>(IReadOnlyDictionary<string, object?> jsonDict, string keyName, out T? value)
        => TryGetDeserializedValue(jsonDict.TryGetValue, keyName, out value);

    /// <summary>
    /// Try to get a value from a Dictionary deserialized from json
    /// </summary>
    /// <typeparam name="T">type of the converted value</typeparam>
    /// <param name="jsonDict">a deserialized json dictionary</param>
    /// <param name="keyName">key name in the dictionary</param>
    /// <param name="value">(out) if found, dictionary value is converted and written</param>
    /// <returns>true if found and converted successfully, false otherwise</returns>
    public static bool TryGetDeserializedValue<T>(Dictionary<string, object?> jsonDict, string keyName, out T? value)
        => TryGetDeserializedValue(jsonDict.TryGetValue, keyName, out value);

    /// <summary>
    /// Try to get a value from an IDictionary deserialized from json
    /// </summary>
    /// <typeparam name="T">type of the converted value</typeparam>
    /// <param name="jsonDict">a deserialized json dictionary</param>
    /// <param name="keyName">key name in the dictionary</param>
    /// <param name="value">(out) if found, dictionary value is converted and written</param>
    /// <returns>true if found and converted successfully, false otherwise</returns>
    public static bool TryGetDeserializedValue<T>(IDictionary<string, object?> jsonDict, string keyName, out T? value)
        => TryGetDeserializedValue(jsonDict.TryGetValue, keyName, out value);

    /// <summary>
    /// Delegate which matches a dictionary TryGetValue for sharing code
    /// </summary>
    private delegate bool KeyValueGetter(string keyName, out object? o);

    /// <summary>
    /// private shared method to extract a deserialized method from the delegate
    /// </summary>
    private static T? GetDeserializedValue<T>(KeyValueGetter func, string keyName, T? defaultVal = default)
    {
        if (!TryGetDeserializedValue(func, keyName, out T? output))
            return defaultVal;

        return output;
    }

    /// <summary>
    /// private shared method to try to extract a deserialized method from the delegate
    /// </summary>
    private static bool TryGetDeserializedValue<T>(KeyValueGetter func, string keyName, out T? value)
    {
        // going to treat a null value equivalent to a key not found
        if (!func(keyName, out object? o) || o == null)
        {
            value = default;
            return false;
        }

        return JsonUtil.TryConvertDeserializedObject(o, out value);
    }
}
