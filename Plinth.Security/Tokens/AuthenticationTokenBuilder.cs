#if !NET9_0_OR_GREATER
using Convert = Plinth.Common.Utils.BackPort.PlinthConvert;
#endif

using Plinth.Security.Crypto;

namespace Plinth.Security.Tokens;

/// <summary>
/// Builder for <see cref="AuthenticationToken"/>
/// </summary>
public class AuthenticationTokenBuilder
{
    private byte[]? _masterKey;
    private ISecureData? _secureData;
    private Guid? _subjectGuid;
    private string? _subjectName;
    private TimeSpan? _lifetime;
    private DateTimeOffset? _originalIssue;
    private Guid? _sessionGuid;
    private List<string>? _roles;
    private IDictionary<string, object?>? _data;
    private bool _logging = false;

    /// <summary>
    /// Construct with the required elements
    /// </summary>
    /// <param name="masterKey">key used to derive other keys</param>
    /// <param name="subjectGuid">the guid for the subject (user or app)</param>
    /// <param name="subjectName">key name for the subject (like username)</param>
    /// <param name="lifetime">how long the token is valid</param>
    public AuthenticationTokenBuilder(byte[] masterKey,
        Guid subjectGuid, 
        string subjectName, 
        TimeSpan lifetime)
    {
        WithMasterKey(masterKey);
        WithSubject(subjectGuid, subjectName);
        WithLifetime(lifetime);
    }

    /// <summary>
    /// Construct with the required elements
    /// </summary>
    /// <param name="masterKey">key used to derive other keys</param>
    /// <param name="subjectGuid">the guid for the subject (user or app)</param>
    /// <param name="subjectName">key name for the subject (like username)</param>
    /// <param name="lifetime">how long the token is valid</param>
    public AuthenticationTokenBuilder(string masterKey,
        Guid subjectGuid, 
        string subjectName, 
        TimeSpan lifetime)
    {
        WithMasterKey(masterKey);
        WithSubject(subjectGuid, subjectName);
        WithLifetime(lifetime);
    }

    /// <summary>
    /// Construct with the required elements
    /// </summary>
    /// <param name="secureData">ISecureData for encryption</param>
    /// <param name="subjectGuid">the guid for the subject (user or app)</param>
    /// <param name="subjectName">key name for the subject (like username)</param>
    /// <param name="lifetime">how long the token is valid</param>
    public AuthenticationTokenBuilder(ISecureData secureData,
        Guid subjectGuid, 
        string subjectName, 
        TimeSpan lifetime)
    {
        WithSecureData(secureData);
        WithSubject(subjectGuid, subjectName);
        WithLifetime(lifetime);
    }

    /// <summary>
    /// Construct with just the key (must set SubjectGuid, SubjectName, and Lifetime)
    /// </summary>
    /// <param name="masterKey">key used to derive other keys</param>
    public AuthenticationTokenBuilder(byte[] masterKey)
        => WithMasterKey(masterKey);

    /// <summary>
    /// Construct with just the key (must set SubjectGuid, SubjectName, and Lifetime)
    /// </summary>
    /// <param name="masterKey">key used to derive other keys</param>
    public AuthenticationTokenBuilder(string masterKey)
        => WithMasterKey(masterKey);

    /// <summary>
    /// Construct with just an ISecureData (must set SubjectGuid, SubjectName, and Lifetime)
    /// </summary>
    /// <param name="secureData">ISecureData for encryption</param>
    public AuthenticationTokenBuilder(ISecureData secureData)
        => WithSecureData(secureData);

    /// <summary>
    /// Construct with nothing (must set MasterKey/SecureData, SubjectGuid, SubjectName, and Lifetime)
    /// </summary>
    public AuthenticationTokenBuilder()
    {
    }

    /// <summary>
    /// Set key used to derive other keys
    /// </summary>
    public AuthenticationTokenBuilder WithMasterKey(byte[] key)
    {
        if (_secureData != null) throw new ArgumentException("cannot supply both master key and secure data");
        _masterKey = key ?? throw new ArgumentNullException(nameof(key));
        return this;
    }

    /// <summary>
    /// Set key used to derive other keys
    /// </summary>
    public AuthenticationTokenBuilder WithMasterKey(string key)
    {
        ArgumentNullException.ThrowIfNull(key);
        if (_secureData != null) throw new ArgumentException("cannot supply both master key and secure data");
        _masterKey = Convert.FromHexString(key);
        return this;
    }

    /// <summary>
    /// Set key used to derive other keys
    /// </summary>
    public AuthenticationTokenBuilder WithSecureData(ISecureData secureData)
    {
        if (_masterKey != null) throw new ArgumentException("cannot supply both master key and secure data");
        _secureData = secureData ?? throw new ArgumentNullException(nameof(secureData));
        return this;
    }

    /// <summary>
    /// Set subject details
    /// </summary>
    /// <param name="subjectGuid">the guid for the subject (user or app)</param>
    /// <param name="subjectName">key name for the subject (like username)</param>
    public AuthenticationTokenBuilder WithSubject(Guid subjectGuid, string subjectName)
        => WithSubjectGuid(subjectGuid).WithSubjectName(subjectName);

    /// <summary>
    /// Set subject guid
    /// </summary>
    /// <param name="subjectGuid">the guid for the subject (user or app)</param>
    public AuthenticationTokenBuilder WithSubjectGuid(Guid subjectGuid)
    {
        _subjectGuid = subjectGuid;
        return this;
    }

    /// <summary>
    /// Set subject name
    /// </summary>
    /// <param name="subjectName">key name for the subject (like username)</param>
    public AuthenticationTokenBuilder WithSubjectName(string subjectName)
    {
        _subjectName = subjectName ?? throw new ArgumentNullException(nameof(subjectName));
        return this;
    }

    /// <summary>
    /// Set token lifetime
    /// </summary>
    /// <param name="lifetime">how long the token is valid</param>
    public AuthenticationTokenBuilder WithLifetime(TimeSpan lifetime)
    {
#if NET8_0_OR_GREATER
        ArgumentOutOfRangeException.ThrowIfLessThanOrEqual(lifetime, TimeSpan.Zero);
#else
        if (lifetime <= TimeSpan.Zero) throw new ArgumentOutOfRangeException(nameof(lifetime));
#endif
        _lifetime = lifetime;
        return this;
    }

    /// <summary>
    /// Set token original issue date
    /// </summary>
    /// <param name="originalIssue">(optional) original issue date</param>
    /// <returns></returns>
    public AuthenticationTokenBuilder WithOriginalIssue(DateTimeOffset originalIssue)
    {
#if NET8_0_OR_GREATER
        ArgumentOutOfRangeException.ThrowIfGreaterThanOrEqual(originalIssue, DateTimeOffset.UtcNow);
#else
        if (originalIssue >= DateTimeOffset.UtcNow) throw new ArgumentOutOfRangeException(nameof(originalIssue));
#endif
        _originalIssue = originalIssue;
        return this;
    }

    /// <summary>
    /// Set session guid
    /// </summary>
    /// <param name="sessionGuid">(optional) session identifier</param>
    public AuthenticationTokenBuilder WithSessionGuid(Guid sessionGuid)
    {
        _sessionGuid = sessionGuid;
        return this;
    }

    /// <summary>
    /// Set user roles
    /// </summary>
    /// <param name="roles">(optional) list of roles</param>
    public AuthenticationTokenBuilder WithRoles(List<string> roles)
    {
        _roles = roles;
        return this;
    }

    /// <summary>
    /// Set user roles
    /// </summary>
    /// <param name="roles">(optional) list of roles</param>
    public AuthenticationTokenBuilder WithRoles(IEnumerable<string> roles)
    {
        _roles = roles?.ToList();
        return this;
    }

    /// <summary>
    /// Set user roles
    /// </summary>
    /// <param name="roles">(optional) list of roles</param>
    public AuthenticationTokenBuilder WithRoles(params string[] roles)
        => WithRoles((IEnumerable<string>)roles);

    /// <summary>
    /// Set custom data
    /// </summary>
    /// <param name="data">optional custom data values, must be serializable to JSON</param>
    public AuthenticationTokenBuilder WithData(IDictionary<string, object?>? data)
    {
        _data = data;
        return this;
    }

    /// <summary>
    /// Set custom data
    /// </summary>
    /// <param name="data">optional custom data values, must be serializable to JSON</param>
    public AuthenticationTokenBuilder WithData(IEnumerable<KeyValuePair<string,object?>>? data)
    {
        _data = data?.ToDictionary(kv => kv.Key, kv => kv.Value);
        return this;
    }

    /// <summary>
    /// Set custom data
    /// </summary>
    /// <param name="data">optional custom data values, must be serializable to JSON</param>
    public AuthenticationTokenBuilder WithData(IEnumerable<Tuple<string,object?>>? data)
    {
        _data = data?.ToDictionary(kv => kv.Item1, kv => kv.Item2);
        return this;
    }

    /// <summary>
    /// Set custom data
    /// </summary>
    /// <param name="data">optional custom data values, must be serializable to JSON</param>
    public AuthenticationTokenBuilder WithData(params KeyValuePair<string,object?>[]? data)
        => WithData((IEnumerable<KeyValuePair<string,object?>>?)data);

    /// <summary>
    /// Set custom data
    /// </summary>
    /// <param name="data">optional custom data values, must be serializable to JSON</param>
    public AuthenticationTokenBuilder WithData(params Tuple<string,object?>[]? data)
        => WithData((IEnumerable<Tuple<string,object?>>?)data);

    /// <summary>
    /// Enable logging of the built token contents
    /// </summary>
    public AuthenticationTokenBuilder WithLogging()
    {
        _logging = true;
        return this;
    }

    /// <summary>
    /// Disable logging of the built token contents
    /// </summary>
    public AuthenticationTokenBuilder WithoutLogging()
    {
        _logging = false;
        return this;
    }

    /// <summary>
    /// Build the token
    /// </summary>
    /// <returns>an AuthenticationToken</returns>
    public AuthenticationToken Build()
    {
        if (_secureData == null && _masterKey == null)
            throw new ArgumentNullException(nameof(_masterKey));

        if (_subjectGuid == null) throw new ArgumentNullException(nameof(_subjectGuid));
        if (_subjectName == null) throw new ArgumentNullException(nameof(_subjectName));
        if (_lifetime == null) throw new ArgumentNullException(nameof(_lifetime));

        return new AuthenticationToken(
            _masterKey,
            _secureData,
            _subjectGuid.Value,
            _subjectName,
            _lifetime.Value,
            _logging,
            _originalIssue,
            _sessionGuid,
            _roles,
            _data);
    }
}
