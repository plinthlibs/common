# README

### Plinth.Security

**Security, Cryptography, and Token Utilities**

* Crypto
	* _ISecureData:_ Symmetric encryption utility for encrypting data in transit and at rest
	* _IPasswordHash:_ Secure storage and validation of passwords
	* _IPredictableHasher:_ Mechanism for producing a secure one way hash of sensitive data that is repeatable for uniqueness checks
* Tokens
	* A library for generating secure, plinth specific authentication tokens, loosely based on JWE
