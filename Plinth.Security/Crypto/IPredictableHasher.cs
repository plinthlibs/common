﻿namespace Plinth.Security.Crypto;

/// <summary>
/// Interface for predictable hashing (for storing obscurred values that are not passwords) that will still be searchable by the original value
/// </summary>
public interface IPredictableHasher
{
    /// <summary>
    /// Perform a predictable hash on a value.
    /// Useful for something that must be encrypted, but also searched for duplicates (like ssn)
    /// </summary>
    /// <param name="value">data (like ssn)</param>
    string PredictableHash(string value);
}
