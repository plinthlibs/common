﻿namespace Plinth.Security.Crypto;

/// <summary>
/// Interface for hashing capabilities
/// </summary>
public interface IPasswordHasher
{
    /// <summary>
    /// Securly Hash a password into a one-way hashed value
    /// </summary>
    /// <param name="password">password from user</param>
    string HashPassword(string password);

    /// <summary>
    /// Verify a password against the hash
    /// </summary>
    /// <param name="password">password from user</param>
    /// <param name="hashedPassword">hash from storage</param>
    /// <returns>true if password matches, false otherwise</returns>
    bool VerifyPasswordHash(string password, string? hashedPassword);
}
