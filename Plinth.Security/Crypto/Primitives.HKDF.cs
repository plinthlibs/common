using System.Security.Cryptography;

namespace Plinth.Security.Crypto;

internal static partial class Primitives
{
    public static byte[] HKDFSha256(byte[] ikm, byte[]? salt, int desiredLength, byte[]? info = null)
        => HKDF(32, ikm, salt, desiredLength, info);

    public static byte[] HKDFSha384(byte[] ikm, byte[]? salt, int desiredLength, byte[]? info = null)
        => HKDF(48, ikm, salt, desiredLength, info);

    public static byte[] HKDFSha512(byte[] ikm, byte[]? salt, int desiredLength, byte[]? info = null)
        => HKDF(64, ikm, salt, desiredLength, info);

    // https://en.wikipedia.org/wiki/HKDF
    // https://tools.ietf.org/html/rfc5869
    private static byte[] HKDF(int hashSize, byte[] ikm, byte[]? salt, int desiredLength, byte[]? info = null)
    {
        info ??= [];
        salt ??= new byte[hashSize];

        var algo = hashSize switch
        {
            32 => HashAlgorithmName.SHA256,
            48 => HashAlgorithmName.SHA384,
            64 => HashAlgorithmName.SHA512,
            _ => throw new NotSupportedException($"hash size: {hashSize}")
        };
        return System.Security.Cryptography.HKDF.DeriveKey(algo, ikm, desiredLength, salt, info);
    }
}
