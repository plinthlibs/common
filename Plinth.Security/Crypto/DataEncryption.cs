using Microsoft.Extensions.Logging;
using System.Buffers.Text;
using System.Security.Cryptography;

namespace Plinth.Security.Crypto;

internal static class DataEncryption
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    // Version tag as the first byte in an encrypted token

    /// <summary>Legacy version using PBKDF2 [Obsolete("switched to v2 2018-10")]</summary>
    private const byte VER_HMACSHA256_AES128CBC = 0x01;
    /// <summary>legacy version using HKDF and supporting 128/192/256 bit AES [Obsolete("switched to v3 2020-04")]</summary>
    private const byte VER_HMACSHA256_AESCBC_V2 = 0x02;
    /// <summary>Current version using HKDF and supporting 128/192/256 bit AES, supporing multi-key</summary>
    private const byte VER_HMACSHA256_AESCBC_V3 = 0x03;

    /// <summary>
    /// Encrypt bytes into a url-safe token string, suitable for sending out into the world
    /// </summary>
    /// <param name="masterKey">A 128-bit master key for encryption and HMAC</param>
    /// <param name="keyId">identifer for the key being used</param>
    /// <param name="plainText">the content</param>
    /// <returns>the token as a string</returns>
    public static string EncryptToUrlSafeToken(byte[] masterKey, byte keyId, byte[] plainText)
    {
        return HttpEncoder.UrlTokenEncode(Encrypt(masterKey, keyId, plainText)); // url safe modified base64
    }

    private static bool IsValidKey(this byte[] v)
        => (v.Length == Primitives.Aes128KeySizeBytes
                || v.Length == Primitives.Aes192KeySizeBytes
                || v.Length == Primitives.Aes256KeySizeBytes);

    private class NoDisposeMemoryStream : MemoryStream
    {
        public NoDisposeMemoryStream(int capacity) : base(capacity)
        {
        }

        public NoDisposeMemoryStream(byte[] array) : base(array)
        {
        }

        protected override void Dispose(bool disposing)
        {
            // do nothing!
        }
    }

    /// <summary>
    /// Encrypt bytes, suitable for sending out into the world
    /// </summary>
    /// <param name="masterKey">A 128/192/256-bit master key for encryption and HMAC</param>
    /// <param name="keyId">identifer for the key being used</param>
    /// <param name="plainText">the content</param>
    /// <returns>the token as a string</returns>
    public static byte[] Encrypt(byte[] masterKey, byte keyId, byte[] plainText)
    {
        ArgumentNullException.ThrowIfNull(masterKey);
        ArgumentNullException.ThrowIfNull(plainText);
        if (!masterKey.IsValidKey()) throw new ArgumentException("key is invalid", nameof(masterKey));

        var size = 2 + Primitives.AesCbcIvSizeBytes + Primitives.Sha256SizeBytes +
            plainText.Length + (Primitives.AesCbcBlockSizeBytes - (plainText.Length % Primitives.AesCbcBlockSizeBytes));

        byte[] iv = Primitives.GetSecureRandomBytes(Primitives.AesCbcIvSizeBytes);

        // derive encryption and hmac keys from the master key, using iv as salt
        // key1 is encryption, key2 is hmac
        (var key1, var key2) = DeriveKeys(masterKey, iv);

        byte[] output = new byte[size];
        using var buffer = new NoDisposeMemoryStream(output);

        buffer.WriteByte(VER_HMACSHA256_AESCBC_V3);
        buffer.WriteByte(keyId);

        buffer.Write(iv, 0, iv.Length);

        Primitives.EncryptAES_CBC(key1, plainText, iv, buffer);

        byte[] hmac = Primitives.HmacSHA256(output.AsSpan()[..(int)buffer.Position], key2);
        buffer.Write(hmac, 0, hmac.Length);

        return output;
    }

    private static byte[]? GetMasterKey(Dictionary<byte, byte[]> keyRing, byte[] data)
    {
        if (keyRing == null || keyRing.Count == 0) throw new ArgumentNullException(nameof(keyRing));

        if (data == null || data.Length == 0)
        {
            log.Error("no data provided");
            return null;
        }

        switch (data[0])
        {
            case VER_HMACSHA256_AES128CBC:
            case VER_HMACSHA256_AESCBC_V2:
                if (keyRing.TryGetValue(0, out var legacyKey))
                    return legacyKey;
                log.Error($"legacy key not found in key ring");
                return null;

            case VER_HMACSHA256_AESCBC_V3:
                if (data.Length == 1)
                {
                    log.Error("insufficient data provided");
                    return null;
                }
                if (keyRing.TryGetValue(data[1], out var key))
                    return key;
                log.Error($"key {data[1]} not found in key ring");
                return null;

            default:
                log.Error("invalid token version: {0}", data[0]);
                return null;
        }
    }

    /// <summary>
    /// Decrypt bytes that were previously encrypted
    /// </summary>
    /// <param name="keyRing">a keyring with alternate keys</param>
    /// <param name="data">the token</param>
    /// <returns>the decrypted bytes, or null if token was not valid</returns>
    public static byte[]? Decrypt(Dictionary<byte, byte[]> keyRing, byte[] data)
    {
        var key = GetMasterKey(keyRing, data);
        if (key == null)
            return null;

        return Decrypt(key, data);
    }

    /// <summary>
    /// Decrypt bytes that were previously encrypted
    /// </summary>
    /// <param name="masterKey">A 128/192/256-bit master key for HMAC and decryption</param>
    /// <param name="data">the token</param>
    /// <returns>the decrypted bytes, or null if token was not valid</returns>
    public static byte[]? Decrypt(byte[] masterKey, byte[]? data)
    {
        ArgumentNullException.ThrowIfNull(masterKey);
        if (!masterKey.IsValidKey()) throw new ArgumentException("key is invalid", nameof(masterKey));

        if (data == null)
        {
            log.Error("invalid token, null data");
            return null;
        }

        if (data[0] != VER_HMACSHA256_AESCBC_V3 
            && data[0] != VER_HMACSHA256_AESCBC_V2 
            && data[0] != VER_HMACSHA256_AES128CBC)
        {
            log.Error("invalid token version: {0}", data[0]);
            return null;
        }

        var dataStart = data[0] == VER_HMACSHA256_AESCBC_V3 ? 2 : 1;

        if (data.Length < (dataStart + Primitives.AesCbcIvSizeBytes + Primitives.AesCbcBlockSizeBytes + Primitives.Sha256SizeBytes))
        {
            log.Error("invalid token, data not long enough to be valid, possible tampering attempt");
            return null;
        }

        byte[] iv = new byte[Primitives.AesCbcIvSizeBytes];
        Array.Copy(data, dataStart, iv, 0, iv.Length);

        // derive encryption and hmac keys from the master key, using iv as salt
        // key1 is encryption, key2 is hmac
        (var key1, var key2) = data[0] == VER_HMACSHA256_AES128CBC ? DeriveKeysV1(masterKey, iv) : DeriveKeys(masterKey, iv);

        int hmacOffset = data.Length - Primitives.Sha256SizeBytes;
        int cipherLength = hmacOffset;
        if (!Primitives.VerifyHmacSHA256(data, hmacOffset, 0, cipherLength, key2))
        {
            log.Warn("token failed hmac validation!");
            return null;
        }

        return Primitives.DecryptAES_CBC(key1, data, dataStart + iv.Length, cipherLength - dataStart - iv.Length, iv);
    }

    /// <summary>
    /// Decrypt bytes that were previously encrypted
    /// </summary>
    /// <param name="keyRing">a keyring with alternate keys</param>
    /// <param name="token">the token</param>
    /// <returns>the decrypted bytes, or null if token was not valid</returns>
    public static byte[]? DecryptUrlSafeToken(Dictionary<byte, byte[]> keyRing, string token)
    {
        try
        {
            byte[] data = HttpEncoder.UrlTokenDecode(token); // url safe modified base64
            var key = GetMasterKey(keyRing, data);
            if (key == null)
                return null;

            return Decrypt(key, data);
        }
        catch (FormatException e)
        {
            log.Debug("token is not valid url-base64: {0}", e.Message);
            return null;
        }
    }

    /// <summary>
    /// Decrypt a url-safe token that was previously encrypted
    /// </summary>
    /// <param name="masterKey">A 128/192/256-bit master key for HMAC and decryption</param>
    /// <param name="token">the token</param>
    /// <returns>the decrypted bytes, or null if token was not valid</returns>
    public static byte[]? DecryptUrlSafeToken(byte[] masterKey, string token)
    {
        try
        {
            return Decrypt(masterKey, HttpEncoder.UrlTokenDecode(token)); // url safe modified base64
        }
        catch (FormatException e)
        {
            log.Debug("token is not valid url-base64: {0}", e.Message);
            return null;
        }
    }

    private static (byte[], byte[]) DeriveKeys(byte[] masterKey, byte[] salt)
    {
        var okm = Primitives.HKDFSha512(masterKey, salt, masterKey.Length * 2);

        byte[] key1 = new byte[masterKey.Length];
        byte[] key2 = new byte[masterKey.Length];
        Array.Copy(okm, 0, key1, 0, masterKey.Length);
        Array.Copy(okm, masterKey.Length, key2, 0, masterKey.Length);

        return (key1, key2);
    }


    /// <summary>
    /// Legacy version which uses PBKDF2 for key stretching.  
    /// This version takes around 33x longer than HKDF
    /// </summary>
    private static (byte[], byte[]) DeriveKeysV1(byte[] masterKey, byte[] salt)
    {
        using var hasher = new Rfc2898DeriveBytes(masterKey, salt, 200, HashAlgorithmName.SHA1);
        byte[] keyPrime = hasher.GetBytes(Primitives.AesCbcBlockSizeBytes);

        using var hasher2 = new Rfc2898DeriveBytes(keyPrime, salt, 200, HashAlgorithmName.SHA1);
        byte[] keys = hasher2.GetBytes(Primitives.AesCbcBlockSizeBytes * 2);

        byte[] key1 = new byte[Primitives.AesCbcBlockSizeBytes];
        byte[] key2 = new byte[Primitives.AesCbcBlockSizeBytes];
        Array.Copy(keys, 0, key1, 0, Primitives.AesCbcBlockSizeBytes);
        Array.Copy(keys, 16, key2, 0, Primitives.AesCbcBlockSizeBytes);

        return (key1, key2);
    }
}
