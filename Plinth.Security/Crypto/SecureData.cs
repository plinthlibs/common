#if !NET9_0_OR_GREATER
using Convert = Plinth.Common.Utils.BackPort.PlinthConvert;
#endif

using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Plinth.Security.Crypto;

/// <summary>
/// Implementation of ISecureData
/// </summary>
public class SecureData : ISecureData
{
    private readonly Dictionary<byte, byte[]> _keys;
    private readonly byte _defaultKey;

    /// <summary>
    /// Constant for key id zero
    /// </summary>
    public const byte KEY_0 = 0;

    /// <summary>
    /// Constant for the default encryption key
    /// </summary>
    public const byte KEY_DEFAULT = KEY_0;

    /// <inheritdoc cref="ISecureData.WithNewDefaultKey(byte)"/>
    public ISecureData WithNewDefaultKey(byte newDefaultKeyId)
        => new DefaultWrapper(this, newDefaultKeyId);

    /// <summary>
    /// Securly encrypt data with a set of keys with a default key
    /// </summary>
    /// <param name="defaultKeyId">default key used for encryption</param>
    /// <param name="keyRing">key ring of keys which can be used, 16,24, or 32 hex bytes</param>
    public SecureData(byte defaultKeyId, params (byte keyId, string key)[] keyRing)
    {
        foreach (var k in keyRing)
        {
            if (k.key == null)
                throw new ArgumentNullException(nameof(keyRing), $"key {k.keyId} is not valid.");
            if (!IsValidKey(k.key))
                throw new ArgumentException($"key {k.keyId} is not valid.");
        }

        _keys = keyRing.ToDictionary(x => x.keyId, x => Convert.FromHexString(x.key));
        _defaultKey = defaultKeyId;
        if (!_keys.ContainsKey(_defaultKey))
            throw new NotSupportedException("must specify default key id in keyring");
    }

    /// <summary>
    /// Securly encrypt data with a set of keys, default is KEY_DEFAULT
    /// </summary>
    /// <param name="keyRing">key ring of keys which can be used, 16,24, or 32 hex bytes</param>
    public SecureData(params (byte keyId, string key)[] keyRing) : this(KEY_DEFAULT, keyRing)
    {
    }

    /// <summary>
    /// (Legacy) Securly encrypt data
    /// </summary>
    /// <param name="legacyKey">legacy encryption key, 16,24, or 32 hex bytes, will be key id 0 in keyring</param>
    public SecureData(string legacyKey) : this((KEY_DEFAULT, legacyKey))
    {
    }

    /// <summary>
    /// (Legacy) Securly encrypt data with alternate keys
    /// </summary>
    /// <param name="legacyKey">legacy encryption key, 16,24, or 32 hex bytes, will be key id 0 in keyring</param>
    /// <param name="keyRing">other keys which can be used</param>
    public SecureData(string legacyKey, params (byte keyId, string key)[] keyRing) : this(keyRing)
    {
        ArgumentNullException.ThrowIfNull(legacyKey);
        if (!IsValidKey(legacyKey)) throw new ArgumentException($"key is not valid.", nameof(legacyKey));

        if (_keys.ContainsKey(KEY_DEFAULT))
            throw new NotSupportedException("cannot specify both default (legacy) and key 0");
        _keys[KEY_DEFAULT] = Convert.FromHexString(legacyKey);
    }

    private byte[] KeyRing(byte keyId)
    {
        if (_keys.TryGetValue(keyId, out var v))
            return v;
        throw new KeyNotFoundException($"key {keyId} not supplied");
    }

    /// <inheritdoc cref="ISecureData.Encrypt(byte[])"/>
    [return: NotNullIfNotNull(nameof(data))]
    public byte[]? Encrypt(byte[]? data)
        => data == null ? null : DataEncryption.Encrypt(KeyRing(_defaultKey), _defaultKey, data);

    /// <inheritdoc cref="ISecureData.Encrypt(string)"/>
    [return: NotNullIfNotNull(nameof(data))]
    public byte[]? Encrypt(string? data)
        => data == null ? null : Encrypt(Encoding.UTF8.GetBytes(data));

    /// <inheritdoc cref="ISecureData.Encrypt(byte[], byte)"/>
    [return: NotNullIfNotNull(nameof(data))]
    public byte[]? Encrypt(byte[]? data, byte keyId)
        => data == null ? null : DataEncryption.Encrypt(KeyRing(keyId), keyId, data);

    /// <inheritdoc cref="ISecureData.Encrypt(string, byte)"/>
    [return: NotNullIfNotNull(nameof(data))]
    public byte[]? Encrypt(string? data, byte keyId)
        => data == null ? null : Encrypt(Encoding.UTF8.GetBytes(data), keyId);

    /// <inheritdoc cref="ISecureData.EncryptToBase64(byte[])"/>
    [return: NotNullIfNotNull(nameof(data))]
    public string? EncryptToBase64(byte[]? data)
        => data == null ? null : DataEncryption.EncryptToUrlSafeToken(KeyRing(_defaultKey), _defaultKey, data);

    /// <inheritdoc cref="ISecureData.EncryptToBase64(string)"/>
    [return: NotNullIfNotNull(nameof(data))]
    public string? EncryptToBase64(string? data)
        => data == null ? null : EncryptToBase64(Encoding.UTF8.GetBytes(data));

    /// <inheritdoc cref="ISecureData.EncryptToHex(byte[])"/>
    [return: NotNullIfNotNull(nameof(data))]
    public string? EncryptToHex(byte[]? data)
        => data == null ? null : Convert.ToHexStringLower(DataEncryption.Encrypt(KeyRing(_defaultKey), _defaultKey, data));

    /// <inheritdoc cref="ISecureData.EncryptToHex(string)"/>
    [return: NotNullIfNotNull(nameof(data))]
    public string? EncryptToHex(string? data)
        => data == null ? null : Convert.ToHexStringLower(DataEncryption.Encrypt(KeyRing(_defaultKey), _defaultKey, Encoding.UTF8.GetBytes(data)));

    /// <inheritdoc cref="ISecureData.EncryptToBase64(byte[], byte)"/>
    [return: NotNullIfNotNull(nameof(data))]
    public string? EncryptToBase64(byte[]? data, byte keyId)
        => data == null ? null : DataEncryption.EncryptToUrlSafeToken(KeyRing(keyId), keyId, data);

    /// <inheritdoc cref="ISecureData.EncryptToBase64(string, byte)"/>
    [return: NotNullIfNotNull(nameof(data))]
    public string? EncryptToBase64(string? data, byte keyId)
        => data == null ? null : EncryptToBase64(Encoding.UTF8.GetBytes(data), keyId);

    /// <inheritdoc cref="ISecureData.EncryptToHex(byte[], byte)"/>
    [return: NotNullIfNotNull(nameof(data))]
    public string? EncryptToHex(byte[]? data, byte keyId)
        => data == null ? null : Convert.ToHexStringLower(DataEncryption.Encrypt(KeyRing(keyId), keyId, data));

    /// <inheritdoc cref="ISecureData.EncryptToHex(string, byte)"/>
    [return: NotNullIfNotNull(nameof(data))]
    public string? EncryptToHex(string? data, byte keyId)
        => data == null ? null : Convert.ToHexStringLower(DataEncryption.Encrypt(KeyRing(keyId), keyId, Encoding.UTF8.GetBytes(data)));

    /// <inheritdoc cref="ISecureData.Decrypt(byte[])"/>
    public byte[]? Decrypt(byte[]? encrypted)
        => encrypted == null ? null : DataEncryption.Decrypt(_keys, encrypted);

    /// <inheritdoc cref="ISecureData.DecryptBase64(string)"/>
    public byte[]? DecryptBase64(string? encrypted)
        => encrypted == null ? null : DataEncryption.DecryptUrlSafeToken(_keys, encrypted);

    /// <inheritdoc cref="ISecureData.DecryptToString(byte[])"/>
    public string? DecryptToString(byte[]? encrypted)
        => encrypted == null ? null : GetNullSafeString(DataEncryption.Decrypt(_keys, encrypted));

    /// <inheritdoc cref="ISecureData.DecryptBase64ToString(string)"/>
    public string? DecryptBase64ToString(string? encrypted)
        => encrypted == null ? null : GetNullSafeString(DataEncryption.DecryptUrlSafeToken(_keys, encrypted));

    /// <inheritdoc cref="ISecureData.DecryptHex(string)"/>
    public byte[]? DecryptHex(string? encrypted)
        => encrypted == null ? null : DataEncryption.Decrypt(_keys, Convert.FromHexString(encrypted));

    /// <inheritdoc cref="ISecureData.DecryptHexToString(string)"/>
    public string? DecryptHexToString(string? encrypted)
        => encrypted == null ? null : GetNullSafeString(DataEncryption.Decrypt(_keys, Convert.FromHexString(encrypted)));

    private static string? GetNullSafeString(byte[]? data) => data == null ? null : Encoding.UTF8.GetString(data);

    /// <summary>
    /// Wraps an existing ISecureData and changes the default key
    /// </summary>
    /// <remarks>The use case for this is to use a single ISecureData via DI, but to use different 
    /// default keys to underlying services like blob storage</remarks>
    internal class DefaultWrapper(ISecureData sd, byte defaultKeyId) : ISecureData
    {
        public byte[]? Decrypt(byte[]? encrypted) => sd.Decrypt(encrypted);
        public byte[]? DecryptBase64(string? encrypted) => sd.DecryptBase64(encrypted);
        public string? DecryptBase64ToString(string? encrypted) => sd.DecryptBase64ToString(encrypted);
        public byte[]? DecryptHex(string? encrypted) => sd.DecryptHex(encrypted);
        public string? DecryptHexToString(string? encrypted) => sd.DecryptHexToString(encrypted);
        public string? DecryptToString(byte[]? encrypted) => sd.DecryptToString(encrypted);

        public byte[]? Encrypt(byte[]? data) => sd.Encrypt(data, defaultKeyId);
        public byte[]? Encrypt(string? data) => sd.Encrypt(data, defaultKeyId);
        public byte[]? Encrypt(byte[]? data, byte keyId) => sd.Encrypt(data, keyId);
        public byte[]? Encrypt(string? data, byte keyId) => sd.Encrypt(data, keyId);

        public string? EncryptToBase64(byte[]? data) => sd.EncryptToBase64(data, defaultKeyId);
        public string? EncryptToBase64(string? data) => sd.EncryptToBase64(data, defaultKeyId);
        public string? EncryptToBase64(byte[]? data, byte keyId) => sd.EncryptToBase64(data, keyId);
        public string? EncryptToBase64(string? data, byte keyId) => sd.EncryptToBase64(data, keyId);

        public string? EncryptToHex(byte[]? data) => sd.EncryptToHex(data, defaultKeyId);
        public string? EncryptToHex(string? data) => sd.EncryptToHex(data, defaultKeyId);
        public string? EncryptToHex(byte[]? data, byte keyId) => sd.EncryptToHex(data, keyId);
        public string? EncryptToHex(string? data, byte keyId) => sd.EncryptToHex(data, keyId);

        public ISecureData WithNewDefaultKey(byte newDefaultKeyId) => sd.WithNewDefaultKey(newDefaultKeyId);
    }

    private static bool IsValidKey(string? v)
        => v != null &&
            (v.Length == 32
                || v.Length == 48
                || v.Length == 64);
}
