using System.Security.Cryptography;

namespace Plinth.Security.Crypto;

internal static partial class Primitives
{
    static HashAlgorithmName GetHash(int hashSize)
        => hashSize switch
        {
            Sha1SizeBytes => HashAlgorithmName.SHA1, // legacy
            32 => HashAlgorithmName.SHA256,
            48 => HashAlgorithmName.SHA384,
            64 => HashAlgorithmName.SHA512,
            _ => throw new NotSupportedException(hashSize.ToString())
        };

    /// <summary>
    /// Securely hash some plain text using PBKDF2/HMAC-SHA [1, 256, 384, 512]
    /// </summary>
    /// <remarks>
    /// http://en.wikipedia.org/wiki/PBKDF2
    /// 20 => LEgacy SHA1
    /// 32 => SHA256
    /// 48 => SHA384
    /// 64 => SHA512
    /// </remarks>
    /// <param name="password">the secret value, used to derive the key</param>
    /// <param name="salt">the message for the HMAC calculation, should be at least 128-bits</param>
    /// <param name="iterations">how 'slow' you want the hash.  More is more secure.  310K for Passwords (as of 2021)</param>
    /// <param name="hashSize">the size of the output hash.  Determines which hash algorithm will be used. (must be 20 in netcore2)</param>
    public static byte[] HmacPBKDF2(string password, byte[] salt, int iterations, int hashSize)
    {
        ArgumentNullException.ThrowIfNull(password);
        ArgumentNullException.ThrowIfNull(salt);
#if NET8_0_OR_GREATER
        ArgumentOutOfRangeException.ThrowIfLessThan(iterations, 1);
#else
        if (iterations < 1) throw new ArgumentOutOfRangeException(nameof(iterations), "must be >= 1");
#endif

        using var hasher = new Rfc2898DeriveBytes(password, salt, iterations, GetHash(hashSize));
        return hasher.GetBytes(hashSize);
    }

    /// <summary>
    /// Don't use this, use HKDF, faster and more secure
    /// </summary>
    [Obsolete("SHA1 based PBKDF2 is deprecated, use HashPBKDF2 with a hashsize of at least 32, 2023-08-29, 2024-09-07", error:true)]
    public static byte[] HmacPBKDF2_LegacySHA1(byte[] key, byte[] salt, int outputBytes, int iterations)
    {
        throw new NotSupportedException();
    }
}
