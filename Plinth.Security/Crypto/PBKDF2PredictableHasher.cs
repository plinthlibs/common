#if !NET9_0_OR_GREATER
using Convert = Plinth.Common.Utils.BackPort.PlinthConvert;
#endif

namespace Plinth.Security.Crypto;

/// <summary>
/// Implementation of IPredictableHasher
/// </summary>
public class PBKDF2PredictableHasher : IPredictableHasher
{
    /// <summary>
    /// The version of the password hash
    /// </summary>
    public const byte CurrentVersion = 3;

    private readonly byte[] _key;
    private readonly int _hashLen;
    private readonly int _iter;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="hashKey">key for predictable hashes, at least 16 bytes</param>
    /// <param name="hashLength">length of predictable hash, 32 bytes by default.  Must be [20, 32, 48, 64]</param>
    /// <param name="iterations"># of iteration rounds for predictable hashes, 310k reccomended, 100k is the default for legacy support</param>
    public PBKDF2PredictableHasher(string hashKey, int hashLength = 32, int iterations = 100000)
    {
        ArgumentNullException.ThrowIfNull(hashKey);
        if (hashKey.Length < 32) throw new ArgumentException("length must be >= 32", nameof(hashKey));
        if (hashLength != 20 && hashLength != 32 && hashLength != 48 && hashLength != 64) throw new ArgumentException("invalid hash length", nameof(hashLength));
        if (iterations <= 0) throw new ArgumentOutOfRangeException(nameof(iterations), "must be > 0");

        _key = Convert.FromHexString(hashKey);
        _hashLen = hashLength;
        _iter = iterations;
    }

    /// <inheritdoc cref="IPredictableHasher.PredictableHash(string)"/>
    public string PredictableHash(string value)
    {
        var bytes = Primitives.HmacPBKDF2(value, _key, _iter, _hashLen);
        return Convert.ToHexStringLower(bytes);
    }
}
