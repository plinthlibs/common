using System.Security.Cryptography;

namespace Plinth.Security.Crypto;

/// <summary>
/// Utility class containing cryptography primitives for use by other projects
/// </summary>
internal static partial class Primitives
{
    public const int Aes128KeySizeBytes = 16;
    public const int Aes192KeySizeBytes = 24;
    public const int Aes256KeySizeBytes = 32;

    public const int Sha256SizeBytes = 32;
    public const int Sha384SizeBytes = 48;
    public const int Sha512SizeBytes = 64;
    public const int AesCbcIvSizeBytes = 16;
    public const int AesCbcBlockSizeBytes = 16;
    public const int Sha1SizeBytes = 20;

    /// <summary>
    /// Generate secure random bytes suitable for cryptography
    /// </summary>
    /// <param name="nBytes"></param>
    /// <returns>array of bytes</returns>
    public static byte[] GetSecureRandomBytes(int nBytes)
    {
        if (nBytes < 0) throw new ArgumentOutOfRangeException(nameof(nBytes), "must be >= 0");

        if (nBytes == 0)
            return [];

        return RandomNumberGenerator.GetBytes(nBytes);
    }

    /// <summary>
    /// Encrypt some plaintext using AES-128/192/256-CBC
    /// </summary>
    /// <param name="key">a 128/192/256 bit key</param>
    /// <param name="plaintext">byte array</param>
    /// <param name="iv">a 128-bit initialization vector</param>
    /// <returns>an encrypted ciphertext</returns>
    public static byte[] EncryptAES_CBC(byte[] key, byte[] plaintext, byte[] iv)
    {
        if (plaintext == null || plaintext.Length == 0) throw new ArgumentNullException(nameof(plaintext));
        if (key.Length != 16 && key.Length != 24 && key.Length != 32) throw new ArgumentException("128/192/256 bits required", nameof(key));
        if (iv == null || iv.Length != Aes128KeySizeBytes) throw new ArgumentException("128-bits required", nameof(iv));

        using var aes = CreateAes(key);

        return aes.EncryptCbc(plaintext, iv, PaddingMode.PKCS7);
    }

    private static Aes CreateAes(byte[] key)
    {
        var aes = Aes.Create();
        aes.Key = key;
        return aes;
    }

    public static void EncryptAES_CBC(byte[] key, byte[] plaintext, byte[] iv, Stream output)
    {
        ArgumentNullException.ThrowIfNull(plaintext);
        if (key.Length != 16 && key.Length != 24 && key.Length != 32) throw new ArgumentException("128/192/256 bits required", nameof(key));
        if (iv == null || iv.Length != Aes128KeySizeBytes) throw new ArgumentException("128-bits required", nameof(iv));

        using var aes = CreateAes(key);

        ICryptoTransform encryptor = aes.CreateEncryptor(key, iv);

        using var csEncrypt = new CryptoStream(output, encryptor, CryptoStreamMode.Write);
        csEncrypt.Write(plaintext, 0, plaintext.Length);
    }

    /// <summary>
    /// Decrypt a ciphertext using AES-128/192/256-CBC
    /// </summary>
    /// <param name="key">a 128/192/256 bit key</param>
    /// <param name="cipherText">byte array</param>
    /// <param name="iv">a 128-bit initialization vector</param>
    /// <returns>the decrypted plaintext</returns>
    public static byte[] DecryptAES_CBC(byte[] key, byte[] cipherText, byte[] iv)
    {
        if (cipherText == null || cipherText.Length == 0) throw new ArgumentNullException(nameof(cipherText));

        return DecryptAES_CBC(key, cipherText, 0, cipherText.Length, iv);
    }

    /// <summary>
    /// Decrypt a ciphertext using AES-128/192/256-CBC from a buffer
    /// </summary>
    /// <param name="key">a 128/192/256 bit key</param>
    /// <param name="buffer">byte array</param>
    /// <param name="cipherOffset">where the cipher is in buffer</param>
    /// <param name="cipherLength">how long the cipher is</param>
    /// <param name="iv">a 128-bit initialization vector</param>
    /// <returns>the decrypted plaintext</returns>
    public static byte[] DecryptAES_CBC(byte[] key, byte[] buffer, int cipherOffset, int cipherLength, byte[] iv)
    {
        ArgumentNullException.ThrowIfNull(buffer);
        if (buffer.Length < cipherLength) throw new ArgumentException("buffer length must be >= cipherLength", nameof(buffer));
        if (key.Length != 16 && key.Length != 24 && key.Length != 32) throw new ArgumentException("128/192/256 bits required", nameof(key));
        if (iv == null || iv.Length != Aes128KeySizeBytes) throw new ArgumentException("128-bits required", nameof(iv));

        using var aes = CreateAes(key);

        return aes.DecryptCbc(buffer.AsSpan(cipherOffset, cipherLength), iv, PaddingMode.PKCS7);
    }
}
