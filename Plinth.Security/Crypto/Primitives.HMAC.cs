using System;
using System.Security.Cryptography;

namespace Plinth.Security.Crypto;

internal static partial class Primitives
{
    #region HMAC (data, key)

#if NET6_0
    private static byte[] Hmac(HMAC hmac, Stream cipherStream, byte[] key)
    {
        if (key == null || key.Length == 0) throw new ArgumentNullException(nameof(key));
        ArgumentNullException.ThrowIfNull(cipherStream);

        using (hmac)
        {
            return hmac.ComputeHash(cipherStream);
        }
    }
#endif

    /// <summary>
    /// Compute an HMAC-SHA256 hash for a ciphertext and a key
    /// </summary>
    /// <param name="cipherText">the encrypted message (for CBC, should include version,IV,cipherText)</param>
    /// <param name="key">key used to compute HMAC</param>
    /// <remarks>
    /// When using HMAC for message authentication, be sure to
    /// a) encrypt then HMAC
    /// b) include all data in the HMAC (version, IV, cipherText)
    /// </remarks>
    /// <returns>the hash (32 bytes)</returns>
    public static byte[] HmacSHA256(ReadOnlySpan<byte> cipherText, ReadOnlySpan<byte> key)
        => HMACSHA256.HashData(key, cipherText);

    /// <summary>
    /// Compute an HMAC-SHA256 hash for a stream and a key.  This will advance the stream to the end.
    /// </summary>
    /// <param name="cipherStream">the encrypted message (for CBC, should include version,IV,cipherText)</param>
    /// <param name="key">key used to compute HMAC</param>
    /// <remarks>
    /// When using HMAC for message authentication, be sure to
    /// a) encrypt then HMAC
    /// b) include all data in the HMAC (version, IV, cipherText)
    /// </remarks>
    /// <returns>the hash (32 bytes)</returns>
    public static byte[] HmacSHA256(Stream cipherStream, byte[] key)
#if NET8_0_OR_GREATER
        => HMACSHA256.HashData(key, cipherStream);
#else
        => Hmac(new HMACSHA256(key), cipherStream, key);
#endif

    /// <summary>
    /// Compute an HMAC-SHA384 hash for a ciphertext and a key
    /// </summary>
    /// <param name="cipherText">the encrypted message (for CBC, should include version,IV,cipherText)</param>
    /// <param name="key">key used to compute HMAC</param>
    /// <remarks>
    /// When using HMAC for message authentication, be sure to
    /// a) encrypt then HMAC
    /// b) include all data in the HMAC (version, IV, cipherText)
    /// </remarks>
    /// <returns>the hash (48 bytes)</returns>
    public static byte[] HmacSHA384(ReadOnlySpan<byte> cipherText, ReadOnlySpan<byte> key)
        => HMACSHA384.HashData(key, cipherText);

    /// <summary>
    /// Compute an HMAC-SHA384 hash for a stream and a key.  This will advance the stream to the end.
    /// </summary>
    /// <param name="cipherStream">the encrypted message (for CBC, should include version,IV,cipherText)</param>
    /// <param name="key">key used to compute HMAC</param>
    /// <remarks>
    /// When using HMAC for message authentication, be sure to
    /// a) encrypt then HMAC
    /// b) include all data in the HMAC (version, IV, cipherText)
    /// </remarks>
    /// <returns>the hash (48 bytes)</returns>
    public static byte[] HmacSHA384(Stream cipherStream, byte[] key)
#if NET8_0_OR_GREATER
        => HMACSHA384.HashData(key, cipherStream);
#else
        => Hmac(new HMACSHA384(key), cipherStream, key);
#endif

    /// <summary>
    /// Compute an HMAC-SHA512 hash for a ciphertext and a key
    /// </summary>
    /// <param name="cipherText">the encrypted message (for CBC, should include version,IV,cipherText)</param>
    /// <param name="key">key used to compute HMAC</param>
    /// <remarks>
    /// When using HMAC for message authentication, be sure to
    /// a) encrypt then HMAC
    /// b) include all data in the HMAC (version, IV, cipherText)
    /// </remarks>
    /// <returns>the hash (64 bytes)</returns>
    public static byte[] HmacSHA512(ReadOnlySpan<byte> cipherText, ReadOnlySpan<byte> key)
        => HMACSHA512.HashData(key, cipherText);

    /// <summary>
    /// Compute an HMAC-SHA512 hash for a stream and a key.  This will advance the stream to the end.
    /// </summary>
    /// <param name="cipherStream">the encrypted message (for CBC, should include version,IV,cipherText)</param>
    /// <param name="key">key used to compute HMAC</param>
    /// <remarks>
    /// When using HMAC for message authentication, be sure to
    /// a) encrypt then HMAC
    /// b) include all data in the HMAC (version, IV, cipherText)
    /// </remarks>
    /// <returns>the hash (64 bytes)</returns>
    public static byte[] HmacSHA512(Stream cipherStream, byte[] key)
#if NET8_0_OR_GREATER
        => HMACSHA512.HashData(key, cipherStream);
#else
        => Hmac(new HMACSHA512(key), cipherStream, key);
#endif
#endregion

#region Verify HMAC
    /// <summary>
    /// Confirm that a given hmac is valid for the ciphertext
    /// </summary>
    /// <param name="hmac">the hmac sent in the message</param>
    /// <param name="cipherText">the encrypted message</param>
    /// <param name="key">key used to compute HMAC</param>
    /// <returns>true if valid, false if not</returns>
    public static bool VerifyHmacSHA256(byte[] hmac, byte[] cipherText, byte[] key)
        => SecureEquals(HmacSHA256(cipherText, key), hmac);

    /// <inheritdoc cref="VerifyHmacSHA256(byte[], byte[], byte[])"/>
    public static bool VerifyHmacSHA384(byte[] hmac, byte[] cipherText, byte[] key)
        => SecureEquals(HmacSHA384(cipherText, key), hmac);

    /// <inheritdoc cref="VerifyHmacSHA256(byte[], byte[], byte[])"/>
    public static bool VerifyHmacSHA512(byte[] hmac, byte[] cipherText, byte[] key)
        => SecureEquals(HmacSHA512(cipherText, key), hmac);

    private static bool VerifyHmac(int hashSize, byte[] buffer, int cipherLength, byte[] key)
    {
        if (key == null || key.Length == 0) throw new ArgumentNullException(nameof(key));
        ArgumentNullException.ThrowIfNull(buffer);
        if (buffer.Length < cipherLength + hashSize / 8) throw new ArgumentException("must be greater than cipherLength + hash size", nameof(buffer));
        return true;
    }

    /// <summary>
    /// Confirm that a given hmac is valid for the ciphertext
    /// </summary>
    /// <param name="buffer">buffer containing cipher and hmac</param>
    /// <param name="hmacOffset">where the hmac is in the buffer</param>
    /// <param name="cipherOffset">where the cipher is in the buffer</param>
    /// <param name="cipherLength">how long the cipher is</param>
    /// <param name="key">key used to compute HMAC</param>
    /// <returns>true if valid, false if not</returns>
    public static bool VerifyHmacSHA256(byte[] buffer, int hmacOffset, int cipherOffset, int cipherLength, byte[] key)
#if NET8_0_OR_GREATER
        => VerifyHmac(HMACSHA256.HashSizeInBytes, buffer, cipherLength, key)
             && SecureEquals(HMACSHA256.HashData(key.AsSpan(), buffer.AsSpan(cipherOffset, cipherLength)), buffer.AsSpan(hmacOffset, HMACSHA256.HashSizeInBytes)); 
#else
        => VerifyHmac(256 / 8, buffer, cipherLength, key)
             && SecureEquals(HMACSHA256.HashData(key.AsSpan(), buffer.AsSpan(cipherOffset, cipherLength)), buffer.AsSpan(hmacOffset, 256 / 8)); 
#endif

    /// <inheritdoc cref="VerifyHmacSHA256(byte[], int, int, int, byte[])"/>
    public static bool VerifyHmacSHA384(byte[] buffer, int hmacOffset, int cipherOffset, int cipherLength, byte[] key)
#if NET8_0_OR_GREATER
        => VerifyHmac(HMACSHA384.HashSizeInBytes, buffer, cipherLength, key)
             && SecureEquals(HMACSHA384.HashData(key.AsSpan(), buffer.AsSpan(cipherOffset, cipherLength)), buffer.AsSpan(hmacOffset, HMACSHA384.HashSizeInBytes)); 
#else
        => VerifyHmac(384 / 8, buffer, cipherLength, key)
             && SecureEquals(HMACSHA384.HashData(key.AsSpan(), buffer.AsSpan(cipherOffset, cipherLength)), buffer.AsSpan(hmacOffset, 384 / 8)); 
#endif

    /// <inheritdoc cref="VerifyHmacSHA256(byte[], int, int, int, byte[])"/>
    public static bool VerifyHmacSHA512(byte[] buffer, int hmacOffset, int cipherOffset, int cipherLength, byte[] key)
#if NET8_0_OR_GREATER
        => VerifyHmac(HMACSHA512.HashSizeInBytes, buffer, cipherLength, key)
             && SecureEquals(HMACSHA512.HashData(key.AsSpan(), buffer.AsSpan(cipherOffset, cipherLength)), buffer.AsSpan(hmacOffset, HMACSHA512.HashSizeInBytes)); 
#else
        => VerifyHmac(512 / 8, buffer, cipherLength, key)
             && SecureEquals(HMACSHA512.HashData(key.AsSpan(), buffer.AsSpan(cipherOffset, cipherLength)), buffer.AsSpan(hmacOffset, 512 / 8)); 
#endif
#endregion

#region SecureEquals
    /// <summary>
    /// Do a length constant comparison of two byte arrays
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <remarks>read about length constant comparison here
    /// https://crackstation.net/hashing-security.htm
    /// Essentially, the fact that if the hash doesn't match, normal '==' will return faster
    /// This makes every comparison take the same time regardless of equality
    /// </remarks>
    /// <returns></returns>
    public static bool SecureEquals(ReadOnlySpan<byte> a, ReadOnlySpan<byte> b)
    {
        // could use CryptographicOperations.FixedTimeEquals() though that one short circuits if the lengths are different
        uint diff = (uint)a.Length ^ (uint)b.Length;
        for (int i = 0; i < a.Length && i < b.Length; i++)
            diff |= (uint)(a[i] ^ b[i]);
        return diff == 0;
    }

    /// <summary>
    /// Do a length constant comparison of two byte arrays
    /// </summary>
    /// <seealso cref="SecureEquals(ReadOnlySpan{byte}, ReadOnlySpan{byte})"/>
    /// <param name="a"></param>
    /// <param name="a_offset"></param>
    /// <param name="a_length"></param>
    /// <param name="b"></param>
    /// <param name="b_offset"></param>
    /// <param name="b_length"></param>
    /// <returns></returns>
    public static bool SecureEquals(byte[] a, int a_offset, int a_length, byte[] b, int b_offset, int b_length)
    {
        ArgumentNullException.ThrowIfNull(a);
        ArgumentNullException.ThrowIfNull(b);
        if (a_offset < 0 || a_offset > a.Length - a_length) throw new ArgumentOutOfRangeException(nameof(a_offset));
#if NET8_0_OR_GREATER
        ArgumentOutOfRangeException.ThrowIfNegative(a_length);
        ArgumentOutOfRangeException.ThrowIfNegative(b_length);
#else
        if (a_length < 0) throw new ArgumentOutOfRangeException(nameof(a_length));
        if (b_length < 0) throw new ArgumentOutOfRangeException(nameof(b_length));
#endif
        if (b_offset < 0 || b_offset > b.Length - b_length) throw new ArgumentOutOfRangeException(nameof(b_offset));

        return SecureEquals(a.AsSpan(a_offset, a_length), b.AsSpan(b_offset, b_length));
    }
#endregion
}
