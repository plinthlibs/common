#if NET9_0_OR_GREATER
using System.Buffers.Text;
#endif

namespace Plinth.Security.Crypto;

// borrowered from System.Web.Util.HttpEncoder to avoid reference to System.Web
// NOTE: net9.0 provided a new 'Base64Url' class which does this better/faster
internal static class HttpEncoder
{
    internal static byte[] UrlTokenDecode(string input)
    {
        ArgumentNullException.ThrowIfNull(input);

        int len = input.Length;
        if (len < 1)
            return [];

        // Calculate the number of padding chars to append to this string.
        // The number of padding chars to append is stored in the last char of the string.
        int numPadChars = input[len - 1] - '0';
        if (numPadChars < 0 || numPadChars > 2)
            throw new FormatException($"Invalid padding on url token: {input[len - 1]}");

#if NET9_0_OR_GREATER
        // for net9, we just ignore the padding char since Base64Url doesn't need it
        return Base64Url.DecodeFromChars(input.AsSpan(0, input.Length - 1));
#else

        ///////////////////////////////////////////////////////////////////
        // Step 2: Create array to store the chars (not including the last char)
        //          and the padding chars
        char[] base64Chars = new char[len - 1 + numPadChars];


        ////////////////////////////////////////////////////////
        // Step 3: Copy in the chars. Transform the "-" to "+", and "*" to "/"
        for (int iter = 0; iter < len - 1; iter++)
        {
            char c = input[iter];

            base64Chars[iter] = c switch
            {
                '-' => '+',
                '_' => '/',
                _ => c,
            };
        }

        ////////////////////////////////////////////////////////
        // Step 4: Add padding chars
        for (int iter = len - 1; iter < base64Chars.Length; iter++)
        {
            base64Chars[iter] = '=';
        }

        // Do the actual conversion
        return Convert.FromBase64CharArray(base64Chars, 0, base64Chars.Length);
#endif
    }

    internal static string UrlTokenEncode(byte[] input)
    {
#if NET9_0_OR_GREATER
        // net9 we use Base64Url.  we append the padding indicator to keep it compatible with the old HttpEncoder
        const int MaxStackLimit = 1024;

        int targetLen = Base64Url.GetEncodedLength(input.Length);
        Span<char> encoded = targetLen > MaxStackLimit ? new char[targetLen + 1] : stackalloc char[targetLen + 1];
        Base64Url.EncodeToChars(input, encoded);
        var padding = (3 - (input.Length % 3)) % 3;
        encoded[^1] = (char)('0' + padding);

        return new string(encoded);
#else
        ArgumentNullException.ThrowIfNull(input);
        if (input.Length < 1)
            return string.Empty;

        string base64Str;
        int endPos;
        char[] base64Chars;

        ////////////////////////////////////////////////////////
        // Step 1: Do a Base64 encoding
        base64Str = Convert.ToBase64String(input);

        ////////////////////////////////////////////////////////
        // Step 2: Find how many padding chars are present in the end
        for (endPos = base64Str.Length; endPos > 0; endPos--)
        {
            if (base64Str[endPos - 1] != '=') // Found a non-padding char!
            {
                break; // Stop here
            }
        }

        ////////////////////////////////////////////////////////
        // Step 3: Create char array to store all non-padding chars,
        //      plus a char to indicate how many padding chars are needed
        base64Chars = new char[endPos + 1];
        base64Chars[endPos] = (char)((int)'0' + base64Str.Length - endPos); // Store a char at the end, to indicate how many padding chars are needed

        ////////////////////////////////////////////////////////
        // Step 3: Copy in the other chars. Transform the "+" to "-", and "/" to "_"
        for (int iter = 0; iter < endPos; iter++)
        {
            char c = base64Str[iter];

            base64Chars[iter] = c switch
            {
                '+' => '-',
                '/' => '_',
                '=' => c,
                _ => c,
            };
        }
        return new string(base64Chars);
#endif
    }
}
