﻿using System.Diagnostics.CodeAnalysis;

namespace Plinth.Security.Crypto;

/// <summary>
/// Securly Encrypt and Decrypt Data
/// </summary>
public interface ISecureData
{
    /// <summary>
    /// Wraps an existing ISecureData and changes the default key
    /// </summary>
    /// <param name="newDefaultKeyId">the new default key id</param>
    ISecureData WithNewDefaultKey(byte newDefaultKeyId);

    /// <summary>
    /// Encrypt bytes to bytes using key 0
    /// </summary>
    [return: NotNullIfNotNull(nameof(data))]
    byte[]? Encrypt(byte[]? data);

    /// <summary>
    /// Encrypt string to bytes using key 0
    /// </summary>
    [return: NotNullIfNotNull(nameof(data))]
    byte[]? Encrypt(string? data);

    /// <summary>
    /// Encrypt bytes to bytes using an alternate key
    /// </summary>
    [return: NotNullIfNotNull(nameof(data))]
    byte[]? Encrypt(byte[]? data, byte keyId);

    /// <summary>
    /// Encrypt string to bytes using an alternate key
    /// </summary>
    [return: NotNullIfNotNull(nameof(data))]
    byte[]? Encrypt(string? data, byte keyId);

    /// <summary>
    /// Encrypt bytes to Url-Safe Base64 Encoded string using key 0
    /// </summary>
    [return: NotNullIfNotNull(nameof(data))]
    string? EncryptToBase64(byte[]? data);

    /// <summary>
    /// Encrypt string to Url-Safe Base64 Encoded string using key 0
    /// </summary>
    [return: NotNullIfNotNull(nameof(data))]
    string? EncryptToBase64(string? data);

    /// <summary>
    /// Encrypt bytes to a Hex String  [0x03, 0xa4] => "03a4" using key 0
    /// </summary>
    [return: NotNullIfNotNull(nameof(data))]
    string? EncryptToHex(byte[]? data);

    /// <summary>
    /// Encrypt bytes to a Hex String  [0x03, 0xa4] => "03a4" using key 0
    /// </summary>
    [return: NotNullIfNotNull(nameof(data))]
    string? EncryptToHex(string? data);

    /// <summary>
    /// Encrypt bytes to Url-Safe Base64 Encoded string using an alternate key
    /// </summary>
    [return: NotNullIfNotNull(nameof(data))]
    string? EncryptToBase64(byte[]? data, byte keyId);

    /// <summary>
    /// Encrypt string to Url-Safe Base64 Encoded string using an alternate key
    /// </summary>
    [return: NotNullIfNotNull(nameof(data))]
    string? EncryptToBase64(string? data, byte keyId);

    /// <summary>
    /// Encrypt bytes to a Hex String  [0x03, 0xa4] => "03a4" using an alternate key
    /// </summary>
    [return: NotNullIfNotNull(nameof(data))]
    string? EncryptToHex(byte[]? data, byte keyId);

    /// <summary>
    /// Encrypt bytes to a Hex String  [0x03, 0xa4] => "03a4" using an alternate key
    /// </summary>
    [return: NotNullIfNotNull(nameof(data))]
    string? EncryptToHex(string? data, byte keyId);

    /// <summary>
    /// Decrypt bytes to bytes
    /// </summary>
    byte[]? Decrypt(byte[]? encrypted);

    /// <summary>
    /// Decrypt Url-Safe Base64 Encoded string to bytes
    /// </summary>
    byte[]? DecryptBase64(string? encrypted);

    /// <summary>
    /// Decrypt Hex string to bytes
    /// </summary>
    byte[]? DecryptHex(string? encrypted);

    /// <summary>
    /// Decrypt bytes to string
    /// </summary>
    string? DecryptToString(byte[]? encrypted);

    /// <summary>
    /// Decrypt Url-Safe Base64 Encoded string to string
    /// </summary>
    string? DecryptBase64ToString(string? encrypted);

    /// <summary>
    /// Decrypt Hex string to string
    /// </summary>
    string? DecryptHexToString(string? encrypted);
}
