#if !NET9_0_OR_GREATER
using Convert = Plinth.Common.Utils.BackPort.PlinthConvert;
#endif

namespace Plinth.Security.Crypto;

/// <summary>
/// Implementation of IPasswordHasher
/// </summary>
/// <remarks>
/// <list type="bullet">
/// <item>Version 1) 82 chars</item>
/// <item>Version 2) 82 chars</item>
/// <item>Version 3) 130 chars</item>
/// <item>Version 4) 130 chars</item>
/// </list>
/// </remarks>
/// <see href="https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html#pbkdf2"/>
public class PBKDF2PasswordHasher : IPasswordHasher
{
    /// <summary>
    /// The version of the password hash
    /// </summary>
    public const byte CurrentVersion = 4;

    private readonly byte _version;

    private readonly static Dictionary<byte, (int iterations, int hashLen)> _versionMap;

    static PBKDF2PasswordHasher()
    {
        // the actual iterations are mapped to a version here.
        // the version gets encoded into the password hash as the first 1 byte
        // this allows us to keep the iterations obscurred, but also be able to change them later
        _versionMap = new Dictionary<byte, (int, int)>()
        {
            {0xff, (500, 20) },     // for unit tests
            {   1, (50000, 20) },   // legacy SHA1, original (doubtful any in the wild)
            {   2, (100000, 20) },  // legacy SHA1, max supported by netcore2
            {   3, (100000, 32) },  // SHA256
            {   4, (310000, 32) },  // SHA256, 2021 recommendation from OWASP
            {   5, (600000, 32) }   // SHA256, 2023 recommendation from OWASP
        };
    }

    /// <summary>
    /// Securly encrypt data
    /// </summary>
    /// <param name="version">version indicating the number of iterations and hash length</param>
    /// <remarks>
    /// <list type="bullet">
    /// <item>1 = 50k rounds, SHA1 (don't use)</item>
    /// <item>2 = 100k rounds, SHA1 (pre 2016, only use for netcore2 compatibility)</item>
    /// <item>3 = 100k rounds, SHA256 (2016-2021)</item>
    /// <item>4 = 310k rounds, SHA256 (2021-2022)</item>
    /// <item>5 = 600k rounds, SHA256 (2023-current)</item>
    /// </list>
    /// </remarks>
    public PBKDF2PasswordHasher(byte version = CurrentVersion)
    {
        if (!_versionMap.ContainsKey(version)) throw new ArgumentException("version not supported", nameof(version));

        _version = version;
    }

    /// <inheritdoc cref="IPasswordHasher.HashPassword(string)"/>
    public string HashPassword(string password)
    {
        (var iter, var hashLen) = _versionMap[_version];

        var salt = Primitives.GetSecureRandomBytes(hashLen);

        using var buffer = new MemoryStream(1 + hashLen * 2);

        buffer.WriteByte(_version);
        buffer.Write(salt, 0, salt.Length);

        var hash = Primitives.HmacPBKDF2(password, salt, iter, hashLen);
        buffer.Write(hash, 0, hash.Length);

        return Convert.ToHexStringLower(buffer.ToArray());
    }

    /// <inheritdoc cref="IPasswordHasher.VerifyPasswordHash(string, string?)"/>
    public bool VerifyPasswordHash(string password, string? hashedPassword)
    {
        if (hashedPassword == null || hashedPassword.Length < 20*4+2)
            return false;

        var bytes = Convert.FromHexString(hashedPassword);

        if (!_versionMap.TryGetValue(bytes[0], out var verInfo))
            return false;

        if (bytes.Length != verInfo.hashLen*2 + 1)
            return false;

        var salt = new byte[verInfo.hashLen];
        Array.Copy(bytes, 1, salt, 0, verInfo.hashLen);

        var testHash = Primitives.HmacPBKDF2(password, salt, verInfo.iterations, verInfo.hashLen);

        return Primitives.SecureEquals(bytes, 1 + verInfo.hashLen, verInfo.hashLen, testHash, 0, verInfo.hashLen);
    }
}
