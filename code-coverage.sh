#!/bin/sh

# dotnet tool install -g dotnet-coverage
# dotnet tool install -g dotnet-reportgenerator-globaltool

# run ./code-coverage.sh {csproj files}

mkdir -p ./Coverage
rm -f ./Coverage/html/*

dotnet-coverage collect -l Coverage -o Coverage/coverage.cobertura.xml -f cobertura \
    "dotnet test $* --framework net8.0"

NUGET_DIR=$(dotnet nuget locals -l global-packages | awk '{ print $2 }' | tr -d '\r\n' )

reportgenerator \
    "-reports:Coverage/coverage.cobertura.xml" \
    "-targetdir:Coverage/html" \
    "-historydir:Coverage/history" \
    "-title:Plinth Code Coverage" \
    "-classfilters:-Moq.*;-TypeNameFormatter.TypeName;-NUnit.*;-Tests.Plinth.*;Plinth.Common.Conditions.*" \
    -reporttypes:HTML

firefox Coverage/html/index.html

