using System.Diagnostics;
using System.Reflection;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Plinth.AspNetCore;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.ReDoc;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace Microsoft.AspNetCore.Builder; // putting it here makes it easier to find

/// <summary>
/// Options for configuring Plinth Swagger
/// </summary>
public class PlinthSwaggerOptions
{
    /// <summary>API Name/Title/Header</summary>
    public string ApiTitle { get; set; } = "Web API";
    /// <summary>API Description</summary>
    public string ApiDescription { get; set; } = "Web API Docs";
    /// <summary>API Version, defaults to "v1"</summary>
    public string ApiVersion { get; set; } = "v1";

    /// <summary>true (default) to use project generated xml docs</summary>
    /// <example>
    /// Put this in your csproj for your API project and any projects which export types used by controllers
    /// <code>
    ///   <PropertyGroup>
    ///     <DocumentationFile>bin/$(Configuration)/$(TargetFramework)/$(MSBuildProjectName).xml</DocumentationFile>
    ///   </PropertyGroup>
    /// </code>
    /// </example>
    public bool EnableXmlDocs { get; set; } = true;

    /// <summary>true (default) to add bearer token auth for any [Authorize] services</summary>
    /// <remarks>
    /// will apply to any service that
    /// a) has [Authorize] attribute
    /// b) in controller with [Authorize] attribute
    /// c) globally if AuthorizeFilter is added to MvcOptions filters
    /// will not authorize if [AllowAnonymous] is defined for method or controller
    /// </remarks>
    public bool AddBearerTokenAuth { get; set; } = true;

    /// <summary>
    /// Default authorization token for debugging in non-production environment.
    /// Will not do anything if set to null, empty or whitespace string.
    /// DO NOT USE IN PRODUCTION!
    /// </summary>
    public string? DebugAuthToken { get; set; }

    /// <summary>optional action to customize the swagger info section</summary>
    public Action<OpenApiInfo>? ConfigureSwaggerInfo { get; set; } = null;

    /// <summary>optional action to add custom swagger configuration</summary>
    public Action<SwaggerGenOptions>? ConfigureSwaggerGen { get; set; } = null;

    /// <summary>show a servers drop down on the swagger UI (hidden by default)</summary>
    /// <remarks>does not affect the openapi document, only the UI</remarks>
    public bool ShowServersDropdown { get; set; } = false;

    /// <summary>a function which can provide a set of custom openapi servers to call</summary>
    /// <remarks>the host that the openapi document was loaded from is always included first</remarks>
    public Func<Http.HttpRequest, IEnumerable<OpenApiServer>>? CustomServers { get; set; } = null;

    /// <summary>a function which can be set to customize generation of client ids</summary>
    /// <remarks>the second parameter can be called to invoke the normal plinth behavior</remarks>
    /// <example>
    /// <code>
    /// CustomOperationId = (apiDesc, baseBehavior) =>
    /// {
    ///     if (custom) { return customOpId(apiDesc); }
    ///     return baseBehavior(apiDesc);
    /// };
    /// </code>
    /// </example>
    public Func<Mvc.ApiExplorer.ApiDescription, Func<Mvc.ApiExplorer.ApiDescription, string>, string>? CustomOperationId { get; set; } = null;

    /// <summary>
    /// Optionally disable the duplicate operation ids check
    /// </summary>
    /// <remarks>OpenAPI says operation ids should be unique, but it really only matters if generating clients.
    /// Use this flag to disable the exception if the IDs aren't unique</remarks>
    public bool DisableDuplicateOperationCheck { get; set; } = false;

    // private stuff
    internal bool GlobalAuthEnabled = false;
}

/// <summary>
/// Extensions for enabling Plinth Standard Swagger
/// </summary>
public static class SwaggerExtensions
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    /// <summary>
    /// Configure Plinth Standard Swagger Generation, do this in <c>ConfigureServices()</c>
    /// </summary>
    /// <param name="services"></param>
    /// <param name="opts">configuration options</param>
    public static void AddPlinthSwaggerGen(this IServiceCollection services, PlinthSwaggerOptions opts)
    {
        services.AddSingleton(opts);
        services.AddSingleton<IConfigureOptions<MvcOptions>, ConfigureMvcOptions>();

        services.AddSwaggerGen(c =>
        {
            var info = new OpenApiInfo
            {
                Version = opts.ApiVersion,
                Title = opts.ApiTitle,
                Description = opts.ApiDescription
            };
            opts.ConfigureSwaggerInfo?.Invoke(info);

            c.SwaggerDoc(opts.ApiVersion, info);

            // swagger-codegen does not like schemas without a 'type', which swashbuckle does not generate for object by default
            c.MapType<object>(() => new OpenApiSchema { Type = "object" });

            // use camel case for parameters (asp.net core defaults to this as well)
            c.DescribeAllParametersInCamelCase();
            // set up operation ids for client generation
            if (opts.CustomOperationId != null)
                c.CustomOperationIds(a => opts.CustomOperationId(a, CustomOperationIdGenerator));
            else
                c.CustomOperationIds(CustomOperationIdGenerator);

            if (!opts.DisableDuplicateOperationCheck)
            {
                // Add a custom filter to ensure all operations are globally unique
                c.DocumentFilter<SwaggerUniqueOperationId>();
            }

            // show controllers by "{namespace} - {namespace} - {controller}", also makes them searchable
            c.TagActionsBy(a =>
            {
                if (a.ActionDescriptor is ControllerActionDescriptor cd)
                {
                    // take all the namespaces after 'Controllers' and append the controller name (without 'Controller')
                    var elements = (cd.ControllerTypeInfo.Namespace ?? string.Empty).Split('.').SkipWhile(s => s != "Controllers").Skip(1);
                    return [string.Join(" - ", elements.Concat(Enumerable.Repeat(cd.ControllerName, 1)))];
                }
                return [];
            });

            if (opts.EnableXmlDocs)
            {
                foreach (var file in GetXmlDocFiles())
                {
                    c.IncludeXmlComments(file);
                }
            }

            if (opts.AddBearerTokenAuth)
            {
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.Http,
                    In = ParameterLocation.Header,
                    Scheme = "bearer",
                    Description = "Enter bearer token",
                    Name = "Authorization",
                    BearerFormat = "Encrypted"
                });

                /* this adds the auth globally, prefer an operations filter which adds to [Authorize]d services
                   c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                    ["Bearer"] = Enumerable.Empty<string>()
                });*/

                c.OperationFilter<SecurityRequirementsOperationFilter>(opts);
                c.OperationFilter<DebugAuthTokenFilter>(opts);
            }

            opts.ConfigureSwaggerGen?.Invoke(c);
        });

        services.AddSwaggerGenNewtonsoftSupport();
    }

    private static string CustomOperationIdGenerator(Mvc.ApiExplorer.ApiDescription apiDesc)
    {
        var ctrl = (ControllerActionDescriptor)apiDesc.ActionDescriptor;

        // default to the name specified in the route info, via [Route("...", Name="{useThis}"]
        if (!string.IsNullOrEmpty(ctrl.AttributeRouteInfo?.Name))
            return ctrl.AttributeRouteInfo.Name;

        // try swagger operation next
        var swaggerOp = apiDesc.ActionDescriptor.EndpointMetadata
            .OfType<SwaggerOperationAttribute>().FirstOrDefault();

        if (!string.IsNullOrWhiteSpace(swaggerOp?.OperationId))
            return swaggerOp.OperationId;

        // this makes (hopefully) unique openapi operation ids, which is necessary for client code generation
        // it creates operation ids like "User_GetUser_GET" for [HttpGet] UserController::GetUser()
        var elements = (ctrl.ControllerTypeInfo.Namespace ?? string.Empty).Split('.').SkipWhile(s => s != "Controllers").Skip(1);
        var ctrlName = string.Join("_", elements.Concat(Enumerable.Repeat(ctrl.ControllerName, 1)));

        return $"{ctrlName}_{ctrl.ActionName}_{apiDesc.HttpMethod}";
    }

    private static IEnumerable<string> GetXmlDocFiles()
    {
        var files = new HashSet<string>();

        // this catches dotnet provided DLLs mostly
        foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies().Where(a => !a.IsDynamic))
            checkAndAdd(assembly.Location);

        // these catch assemblies not yet loaded
        var paths = new[]
        {
            Directory.GetCurrentDirectory(),
            Assembly.GetEntryAssembly()?.Location,
            Assembly.GetExecutingAssembly().Location,
            Process.GetCurrentProcess().MainModule?.FileName
        };

        // these catch assemblies not yet loaded
        foreach (var p in paths)
        {
            foreach (var file in filesForPath(p).Where(isDLL))
                checkAndAdd(file);
        }

        return files;

        static IEnumerable<string> filesForPath(string? file)
        {
            if (file == null)
                return [];
            var dir = Path.GetDirectoryName(file);
            if (string.IsNullOrEmpty(dir))
                return [];
            return Directory.EnumerateFiles(dir);
        }

        static bool isDLL(string file) => string.Compare(Path.GetExtension(file), ".DLL", StringComparison.OrdinalIgnoreCase) == 0;

        void checkAndAdd(string file)
        {
            var dir = Path.GetDirectoryName(file) ?? "";
            var xml = Path.Combine(dir, Path.GetFileNameWithoutExtension(file) + ".xml");
            if (File.Exists(xml))
                files.Add(xml);
        }
    }

    private sealed class SwaggerUniqueOperationId : IDocumentFilter
    {
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            var opIds = new Dictionary<string, string>();
            foreach (var path in swaggerDoc.Paths)
            {
                foreach (var operationId in path.Value.Operations.Select(o => o.Value.OperationId))
                {
                    if (opIds.TryGetValue(operationId, out var value))
                        throw new InvalidOperationException($"Detected multiple operations with ID: {operationId} on paths '{path.Key}' and '{value}'");
                    opIds[operationId] = path.Key;
                }
            }
        }
    }

    // this odd thing is how you get access to the MvcOptions
    private sealed class ConfigureMvcOptions(PlinthSwaggerOptions opts) : IConfigureOptions<MvcOptions>
    {
        public void Configure(MvcOptions options)
        {
            opts.GlobalAuthEnabled = options.Conventions.OfType<PlinthServicesExtensions.PlinthControllerApplicationModelConvention>().Any();
        }
    }

    /// <summary>
    /// Returns true if current operation requires some kind of authorization.
    /// </summary>
    private static bool IsAuthorizedOperation(this OperationFilterContext context, PlinthSwaggerOptions opts)
    {
        var allAttrs = context.MethodInfo
            .GetCustomAttributes(true)
            .Concat(context.MethodInfo.DeclaringType?.GetCustomAttributes(true) ?? Enumerable.Empty<object>())
            .ToList();

        // there is a 'GlobalAuthEnabled' option, and it can be disabled for some methods by attribute
        var globalEnable = opts.GlobalAuthEnabled &&
            !allAttrs.OfType<DisableGlobalAuthorizeAttribute>().Any();

        // auth can be manually applied to the method using 'Authorize' attribute
        var authEnable = allAttrs.OfType<AuthorizeAttribute>().Any();

        // some methods can be marked with 'AllowAnonymous' attribute
        var allowAnonymous = allAttrs.OfType<AllowAnonymousAttribute>().Any();

        return (globalEnable || authEnable) && !allowAnonymous;
    }

    private sealed class SecurityRequirementsOperationFilter(object opts) : IOperationFilter
    {
        private readonly PlinthSwaggerOptions _opts = (PlinthSwaggerOptions)opts;

        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            // only applies to methods which require authorization (when 'lock' icon is displayed)
            if (!context.IsAuthorizedOperation(_opts))
                return;

            if (!operation.Responses.ContainsKey("401"))
                operation.Responses["401"] = new OpenApiResponse { Description = "Unauthorized, token missing, expired, or invalid" };
            if (!operation.Responses.ContainsKey("403"))
                operation.Responses["403"] = new OpenApiResponse { Description = "Forbidden, access denied by policy" };

            operation.Security =
            [
                new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Bearer" }
                        },
                        Array.Empty<string>()
                    }
                }
            ];
        }
    }

    private sealed class DebugAuthTokenFilter(object opts) : IOperationFilter
    {
        //
        // we have to duplicate header name here and in Plinth.AspNetCore::TokenAuth/TokenHandlerUtil.cs
        // to avoid extra project dependency
        //
        internal const string DebugAuthHeader = "X-Plinth-Auth";

        private readonly PlinthSwaggerOptions _opts = (PlinthSwaggerOptions)opts;

        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            // not applied when 'DebugAuthToken' option is not set
            if (string.IsNullOrWhiteSpace(_opts.DebugAuthToken))
                return;

            // only applies to methods which require authorization (when 'lock' icon is displayed)
            if (!context.IsAuthorizedOperation(_opts))
                return;

            operation.Parameters ??= [];

            operation.Parameters.Add(new OpenApiParameter
            {
                In = ParameterLocation.Header,
                Name = DebugAuthHeader,
                Description = "Default authorization token for debugging purposes",
                Required = false,
                Schema = new OpenApiSchema
                {
                    Type = "string",
                    Default = new OpenApiString($"Bearer {_opts.DebugAuthToken}")
                }
            });
        }
    }

    /// <summary>
    /// Configure Plinth Standard Swagger UI, do this BEFORE calling <c>.UseEndpoints()</c>
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="enableSwaggerAtRoot">true to have root page display swagger</param>
    /// <param name="enableReDoc">true to enable redoc api</param>
    /// <param name="customizeSwaggerUI">(optional) action to customize the swagger ui</param>
    /// <param name="customizeReDocUI">(optional) action to customize the redoc ui (if enabled)</param>
    /// <param name="customizeSwagger">(optional) action to customize swagger output</param>
    /// <returns>builder</returns>
    public static IApplicationBuilder UsePlinthSwaggerUI(this IApplicationBuilder builder,
        bool enableSwaggerAtRoot = true,
        bool enableReDoc = true,
        Action<SwaggerUIOptions>? customizeSwaggerUI = null,
        Action<ReDocOptions>? customizeReDocUI = null,
        Action<SwaggerOptions>? customizeSwagger = null)
    {
        var swaggerOpts = builder.ApplicationServices.GetService<IOptions<SwaggerGenOptions>>();
        var plinthOpts = builder.ApplicationServices.GetService<PlinthSwaggerOptions>();
        if (swaggerOpts == null || plinthOpts == null)
            throw new InvalidOperationException("You must configure SwaggerGen in ConfigureServices().  Use services.AddPlinthSwaggerGen()");

        // redirect / to /swagger
        if (enableSwaggerAtRoot)
            builder.UseRewriter(new RewriteOptions().AddRedirect("^[/]?$", "/swagger"));

        builder.UseSwagger(c =>
        {
            // this fixes the host value when the internal server host is not the same as the external (load balancing)
            // we also append optional custom servers from the options
            c.PreSerializeFilters.Add((swagger, httpReq) =>
            {
                swagger.Servers = Enumerable.Repeat(new OpenApiServer { Url = $"{httpReq.Scheme}://{httpReq.Host.Value}" }, 1)
                    .Concat(plinthOpts.CustomServers?.Invoke(httpReq) ?? [])
                    .ToList();
                if (swagger.Servers.Count > 1 && !plinthOpts.ShowServersDropdown)
                    log.Warn("Custom openapi servers provided, but server dropdown is hidden. Set ShowServersDropdown to true");
            });

            customizeSwagger?.Invoke(c);
        });

        builder.UseSwaggerUI(c =>
        {
            c.RoutePrefix = "swagger"; // serve the UI at /swagger
            c.EnableTryItOutByDefault(); // user does not need to click "Try It Out" first
            ConfigureSwaggerUI(c, plinthOpts);
            customizeSwaggerUI?.Invoke(c);
        });

        if (enableReDoc)
        {
            builder.UseReDoc(c =>
            {
                c.RoutePrefix = "redoc"; // serve ReDoc at /redoc
                ConfigureReDocUI(c, plinthOpts);
                customizeReDocUI?.Invoke(c);
            });
        }

        return builder;
    }

    private static void ConfigureSwaggerUI(SwaggerUIOptions c, PlinthSwaggerOptions plinthOpts)
    {
        c.SwaggerEndpoint($"../swagger/{plinthOpts.ApiVersion}/swagger.json", plinthOpts.ApiVersion);

        c.DefaultModelExpandDepth(2);
        c.DefaultModelRendering(ModelRendering.Example);
        c.DefaultModelsExpandDepth(1);

        c.EnableDeepLinking();

        c.DocExpansion(DocExpansion.None);
        c.EnableFilter();
        c.DisplayRequestDuration();

        c.DocumentTitle = plinthOpts.ApiTitle;

        // enable curl bash/powershell examples
        c.ConfigObject.AdditionalItems.Add("requestSnippetsEnabled", true);

        if (!string.IsNullOrWhiteSpace(plinthOpts.DebugAuthToken))
        {
            // customize Swagger UI styles for better displaying debug auth tokens
            c.HeadContent = $@"{c.HeadContent}
<style>
  tr[data-param-name='{DebugAuthTokenFilter.DebugAuthHeader}'] div.parameter__name {{ display: none; }}
  tr[data-param-name='{DebugAuthTokenFilter.DebugAuthHeader}'] div.parameter__default {{ display: none; }}
  tr[data-param-name='{DebugAuthTokenFilter.DebugAuthHeader}'] div.parameter__type {{ display: none; }}
  tr[data-param-name='{DebugAuthTokenFilter.DebugAuthHeader}'] td.parameters-col_description {{ font-size: 12px; }}
  tr[data-param-name='{DebugAuthTokenFilter.DebugAuthHeader}'] td.parameters-col_description p {{ margin: 0; }}
</style>";
        }

        if (!plinthOpts.ShowServersDropdown)
            c.HeadContent += "<style>.servers, .servers-title { display: none }</style>";
    }

    private static void ConfigureReDocUI(ReDocOptions c, PlinthSwaggerOptions plinthOpts)
    {
        c.SpecUrl = $"../swagger/{plinthOpts.ApiVersion}/swagger.json";
        c.DocumentTitle = plinthOpts.ApiTitle;

        if (!string.IsNullOrWhiteSpace(plinthOpts.DebugAuthToken))
        {
            // customize ReDoc UI styles for better displaying debug auth tokens
            c.HeadContent = $@"{c.HeadContent}
<style>
  td[title='{DebugAuthTokenFilter.DebugAuthHeader}'] + td > div > div:nth-child(2) {{ display: none; }}
  td[title='{DebugAuthTokenFilter.DebugAuthHeader}'] + td p {{ font-size: 12px; }}
</style>";
        }
    }
}
