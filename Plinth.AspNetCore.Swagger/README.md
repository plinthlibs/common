# README

### Plinth.AspNetCore.Swagger

**Swagger support for Plinth.AspNetCore**

Example code for adding a swagger page to an ASP&#46;NET Core application.
In `ConfigureServices()`
```c#
        services.AddPlinthSwaggerGen(
            new PlinthSwaggerOptions
            {
                ApiTitle = "AspNetCore Test API",
                ApiDescription = "This is an API for Demonstrating an AspNetCore Web API",
                ApiVersion = "v1",
                ConfigureSwaggerInfo = i =>
                {
                    i.TermsOfService = new Uri("https://myapp.com/tos.html");
                }
            }
        );
```
In `Configure()`
```c#
        app.UsePlinthSwaggerUI();
```
