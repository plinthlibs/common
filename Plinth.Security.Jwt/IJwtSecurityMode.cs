using Microsoft.IdentityModel.Tokens;

namespace Plinth.Security.Jwt;

/// <summary>
/// Jwt Security Mode interface for custom jwt security
/// </summary>
public interface IJwtSecurityMode
{
    /// <summary>
    /// Apply changes to the token descriptor to add security during token creation
    /// </summary>
    public void ApplySecurity(SecurityTokenDescriptor descriptor);

    /// <summary>
    /// Apply changes to the validation paramaters to validate a token
    /// </summary>
    public void ApplyValidation(TokenValidationParameters parameters);
}
