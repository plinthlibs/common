using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;

namespace Plinth.Security.Jwt;

/// <summary>
/// An <see cref="IJwtSecurityMode"/> implementation for creating JWEs with AES-CBC-HMAC encryption
/// </summary>
public class JwtSecurityModeAesEncryption : IJwtSecurityMode
{
    private readonly EncryptingCredentials _encryptingCredentials;
    private readonly string[] _algorithms;

    /// <summary>
    /// Construct with a key
    /// </summary>
    /// <param name="key">Must be 32, 48, or 64 bytes long</param>
    public JwtSecurityModeAesEncryption(byte[] key)
    {
        ArgumentNullException.ThrowIfNull(key);

        var alg = (key.Length * 8) switch
        {
            256 => SecurityAlgorithms.Aes128CbcHmacSha256,
            384 => SecurityAlgorithms.Aes192CbcHmacSha384,
            512 => SecurityAlgorithms.Aes256CbcHmacSha512,
            _ => throw new ArgumentException($"key length must be one of [256, 384, 512], was {key.Length * 8}", nameof(key))
        };

        _encryptingCredentials = new EncryptingCredentials(new SymmetricSecurityKey(key) { KeyId = "config" }, JwtConstants.DirectKeyUseAlg, alg);
        _algorithms = [JwtConstants.DirectKeyUseAlg, alg];
    }

    /// <inheritdoc cref="IJwtSecurityMode.ApplySecurity(SecurityTokenDescriptor)"/>
    public void ApplySecurity(SecurityTokenDescriptor descriptor)
    {
        descriptor.EncryptingCredentials = _encryptingCredentials;
        descriptor.SigningCredentials = null;
    }

    /// <inheritdoc cref="IJwtSecurityMode.ApplyValidation(TokenValidationParameters)"/>
    public void ApplyValidation(TokenValidationParameters parameters)
    {
        parameters.ValidAlgorithms = _algorithms;
        parameters.RequireSignedTokens = false;
        parameters.TokenDecryptionKey = _encryptingCredentials.Key;
    }
}
