using System.Security.Claims;

namespace Plinth.Security.Jwt;

/// <summary>
/// Options for JWT construction and validation
/// </summary>
public class JwtGenerationOptions
{
    /// <summary>
    /// Security mode for protecting the tokens
    /// </summary>
    public IJwtSecurityMode SecurityMode { get; set; } = null!;

    /// <summary>
    /// Ths Issuer of the token, who created it
    /// </summary>
    public string? Issuer { get; set; }

    /// <summary>
    /// The audience for the token, who can receive it
    /// </summary>
    public string? Audience { get; set; }

    /// <summary>
    /// Lifetime of generated tokens
    /// </summary>
    public TimeSpan TokenLifetime { get; set; } = TimeSpan.FromMinutes(15);

    /// <summary>
    /// Maximum lifetime for a token to live throughout multiple refreshes
    /// </summary>
    public TimeSpan MaxTokenLifetime { get; set; } = TimeSpan.FromHours(24);

    /// <summary>
    /// Optionally override the allowed clock skew when validating token expiration
    /// </summary>
    public TimeSpan? ClockSkew { get; set; }

    /// <summary>
    /// Custom claims common to all tokens created
    /// </summary>
    public List<Claim> CustomClaims { get; private set; } = [];

    /// <summary>
    /// Enable or Disable logging the token contents
    /// </summary>
    public bool TokenContentLogging { get; set; } = true;

    /// <summary>
    /// Construct 
    /// </summary>
    public JwtGenerationOptions()
    {
    }

    /// <summary>
    /// Validate the state of this object
    /// </summary>
    /// <exception cref="InvalidDataException"></exception>
    public void Validate()
    {
        if (SecurityMode == null)
            throw new InvalidDataException("JwtGenerationOptions.SecurityMode must be set");

        if (TokenLifetime <= TimeSpan.Zero)
            throw new InvalidDataException("JwtGenerationOptions.TokenLifetime must be greater than zero");

        if (MaxTokenLifetime <= TokenLifetime)
            throw new InvalidDataException("JwtGenerationOptions.MaxTokenLifetime must be greater than TokenLifetime");

        if (ClockSkew.HasValue && ClockSkew.Value < TimeSpan.Zero)
            throw new InvalidDataException("JwtGenerationOptions.ClockSkew must be greater than or equal to zero");
    }
}
