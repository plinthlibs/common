using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

using SystemJwt = System.IdentityModel.Tokens.Jwt;

namespace Plinth.Security.Jwt;

/// <summary>
/// Builder for a JWT
/// </summary>
public partial class JwtBuilder
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private readonly JwtGenerationOptions _options;

    private Guid? _subjectId;
    private string? _subjectName;
    private DateTimeOffset? _originalIssue;
    private Guid? _sessionGuid;
    private List<string>? _roles;
    private List<Claim>? _claims;

    // used internally to refresh a token
    private SystemJwt.JwtSecurityToken? _jwt;

    // these are not allowed to be set except by this builder
    private readonly HashSet<string> reservedClaims =
    [
        "aud",
        "iss",
        "unique_name",
        "sub",
        "sid",
        "iat",
        "nbf",
        "exp",
        "ori",
        "role",
        ClaimTypes.Role
    ];

    /// <summary>
    /// Construct
    /// </summary>
    public JwtBuilder(JwtGenerationOptions options)
    {
        _options = options ?? throw new ArgumentNullException(nameof(options));
    }

    /// <summary>
    /// Set subject details
    /// </summary>
    /// <param name="subjectId">the unique identifier for the subject (user or app)</param>
    /// <param name="subjectName">key name for the subject (like username)</param>
    public JwtBuilder Subject(Guid subjectId, string subjectName)
        => SubjectId(subjectId).SubjectName(subjectName);

    /// <summary>
    /// Set subject id
    /// </summary>
    /// <param name="subjectId">the unique identifier for the subject (user or app)</param>
    public JwtBuilder SubjectId(Guid subjectId)
    {
        if (subjectId == Guid.Empty) throw new ArgumentNullException(nameof(subjectId), "subject id cannot be an empty guid");
        _subjectId = subjectId;
        return this;
    }

    /// <summary>
    /// Set subject name
    /// </summary>
    /// <param name="subjectName">key name for the subject (like username)</param>
    public JwtBuilder SubjectName(string subjectName)
    {
        _subjectName = subjectName ?? throw new ArgumentNullException(nameof(subjectName));
        return this;
    }

    /// <summary>
    /// Set token original issue date
    /// </summary>
    /// <param name="originalIssue">(optional) original issue date</param>
    /// <returns></returns>
    public JwtBuilder OriginalIssue(DateTimeOffset? originalIssue)
    {
        if (originalIssue.HasValue && originalIssue.Value > DateTimeOffset.UtcNow)
            throw new ArgumentOutOfRangeException(nameof(originalIssue));

        _originalIssue = originalIssue;
        return this;
    }

    /// <summary>
    /// Set session guid
    /// </summary>
    /// <param name="sessionGuid">(optional) session identifier</param>
    public JwtBuilder SessionGuid(Guid? sessionGuid)
    {
        _sessionGuid = sessionGuid;
        return this;
    }

    /// <summary>
    /// Set user roles
    /// </summary>
    /// <param name="roles">(optional) list of roles</param>
    public JwtBuilder Roles(List<string>? roles)
    {
        _roles = roles;
        return this;
    }

    /// <summary>
    /// Set user roles
    /// </summary>
    /// <param name="roles">(optional) list of roles</param>
    public JwtBuilder Roles(IEnumerable<string>? roles)
    {
        _roles = roles?.ToList();
        return this;
    }

    /// <summary>
    /// Set user roles
    /// </summary>
    /// <param name="roles">(optional) list of roles</param>
    public JwtBuilder Roles(params string[] roles)
        => Roles((IEnumerable<string>)roles);

    /// <summary>
    /// Custom claims to add to the JWT
    /// </summary>
    public JwtBuilder AddClaim(Claim claim)
    {
        _claims ??= [];
        if (reservedClaims.Contains(claim.Type))
        {
            if (claim.Type == "role" || claim.Type == ClaimTypes.Role)
                throw new InvalidOperationException("Use .Roles() to set roles for the token");
            throw new InvalidOperationException($"{claim.Type} is a reserved claim");
        }
        _claims.Add(claim);
        return this;
    }

    /// <summary>
    /// Custom claims to add to the JWT
    /// </summary>
    public JwtBuilder AddClaim(string type, string value)
    {
        ArgumentNullException.ThrowIfNull(type);
        ArgumentNullException.ThrowIfNull(value);

        return AddClaim(new Claim(type, value));
    }

    /// <summary>
    /// Custom claims to add to the JWT from an existing token
    /// </summary>
    internal JwtBuilder AddClaimsFromToken(SystemJwt.JwtSecurityToken securityToken)
    {
        _jwt = securityToken;
        return this;
    }

    /// <summary>
    /// Build the token
    /// </summary>
    /// <returns>an AuthenticationToken</returns>
    public JwtData Build()
    {
        if (_subjectId == null) throw new InvalidOperationException("A subject id must be provided");
        if (_subjectName == null) throw new InvalidOperationException("A subject name must be provided");

        _sessionGuid ??= Guid.NewGuid();

        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(GenClaims()),
            Expires = DateTime.UtcNow + _options.TokenLifetime,
            Issuer = _options.Issuer,
            Audience = _options.Audience,
            IssuedAt = DateTime.UtcNow
        };

        _options.SecurityMode.ApplySecurity(tokenDescriptor);

        var tokenHandler = new SystemJwt.JwtSecurityTokenHandler();
        var token = tokenHandler.CreateToken(tokenDescriptor);
        var jwt = tokenHandler.WriteToken(token);

        if (_options.TokenContentLogging)
            LogDefines.LogTokenCreate(log, token.ToString());

        return new JwtData(jwt, token, _sessionGuid.Value);
    }

    private IEnumerable<Claim> GenClaims()
    {
        yield return new Claim("unique_name", _subjectName!);
        yield return new Claim("sub", _subjectId.ToString()!);

        yield return new Claim("sid", _sessionGuid.ToString()!);

        yield return new Claim("ori",
            _originalIssue?.ToUnixTimeSeconds().ToString() ?? DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString(),
            ClaimValueTypes.Integer64);

        if (_roles != null)
        {
            foreach (var role in _roles)
                yield return new Claim(ClaimTypes.Role, role);
        }

        // only add global claims if this is a fresh token, not a token copied from another
        if (_jwt == null)
        {
            foreach (var claim in _options.CustomClaims)
                if (!reservedClaims.Contains(claim.Type))
                    yield return claim;
        }

        if (_claims != null)
        {
            foreach (var claim in _claims)
                yield return claim;
        }

        if (_jwt != null)
        {
            foreach (var claim in _jwt.Claims)
                if (!reservedClaims.Contains(claim.Type))
                    yield return claim;
        }
    }

    private static partial class LogDefines
    {
        [LoggerMessage(0, LogLevel.Debug, "create: token contents: {TokenContents}", EventName = "TokenCreate")]
        public static partial void LogTokenCreate(ILogger logger, string? tokenContents);
    }
}
