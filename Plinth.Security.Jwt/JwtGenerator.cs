using Microsoft.IdentityModel.Tokens;

namespace Plinth.Security.Jwt;

/// <summary>
/// Generator for JWTs, useful in Login/Session endpoints
/// </summary>
public class JwtGenerator
{
    private readonly JwtValidator _JwtValidator;
    private readonly JwtGenerationOptions _options;

    /// <summary>
    /// Create a SecureTokenGenerator
    /// </summary>
    public JwtGenerator(JwtGenerationOptions options, JwtValidator validator)
    {
        _options = options ?? throw new ArgumentNullException(nameof(options));
        _JwtValidator = validator ?? throw new ArgumentNullException(nameof(validator));
    }

    /// <summary>
    /// Get a builder for custom Jwt creation
    /// </summary>
    /// <param name="userId">user's unique identifier</param>
    /// <param name="userName">user's unique name/email/etc</param>
    /// <param name="roles">(optional) list of roles</param>
    /// <returns>A builder that has the user guid/name/roles already set</returns>
    public JwtBuilder GetBuilder(Guid userId, string userName, IEnumerable<string>? roles = null)
    {
        return new JwtBuilder(_options)
            .Subject(userId, userName)
            .Roles(roles);
    }

    /// <summary>
    /// Get a builder for custom Jwt creation
    /// </summary>
    /// <param name="userId">user's unique identifier</param>
    /// <param name="userName">user's unique name/email/etc</param>
    /// <param name="roles">(optional) list of roles</param>
    /// <returns>A builder that has the user guid/name/roles already set</returns>
    public JwtBuilder GetBuilder(Guid userId, string userName, params string[] roles)
    {
        return new JwtBuilder(_options)
            .Subject(userId, userName)
            .Roles(roles);
    }


    /// <summary>
    /// Refresh a token by creating a new one with the original data but a new lifetime
    /// </summary>
    /// <param name="origToken"></param>
    /// <remarks>The original token's OriginalIssued is maintained and checked</remarks>
    /// <returns>a fresh jwt</returns>
    /// <exception cref="SecurityTokenExpiredException">if refreshing this token will exceed the maximum lifetime</exception>
    public JwtData Refresh(string origToken)
    {
        var cp = _JwtValidator.Validate(origToken, out var securityToken);

        var ori = cp.JwtOriginalIssue()
            ?? throw new InvalidOperationException("cannot refresh a token that does not have an original issue date");

        if (DateTime.UtcNow - ori > _options.MaxTokenLifetime)
            throw new SecurityTokenExpiredException("token has reached its maximum lifetime, cannot refresh");

        return GetBuilder(
                cp.JwtUserId(),
                cp.JwtUserName(),
                cp.JwtRoles())
            .OriginalIssue(cp.JwtOriginalIssue())
            .SessionGuid(cp.JwtSessionGuid())
            .AddClaimsFromToken(securityToken)
            .Build();
    }
}
