using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

using SystemJwt = System.IdentityModel.Tokens.Jwt;

namespace Plinth.Security.Jwt;

/// <summary>
/// Validated JWT tokens
/// </summary>
public partial class JwtValidator
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private readonly JwtGenerationOptions _options;

    /// <summary>
    /// Constructor
    /// </summary>
    public JwtValidator(JwtGenerationOptions options)
    {
        _options = options ?? throw new ArgumentNullException(nameof(options));
    }

    /// <summary>
    /// Validate a token
    /// </summary>
    /// <param name="token">the token</param>
    /// <param name="loggingEnabled">true to log contents of token</param>
    /// <returns>A ClaimsPrincipal and a JwtSecurityToken</returns>
    public ClaimsPrincipal Validate(string token, bool? loggingEnabled = null)
        => Validate(token, out var _, loggingEnabled);

    /// <summary>
    /// Validate a token
    /// </summary>
    /// <param name="token">the token</param>
    /// <param name="decoded">(out) the decoded security token</param>
    /// <param name="loggingEnabled">true to log contents of token</param>
    /// <returns>A ClaimsPrincipal and a JwtSecurityToken</returns>
    internal ClaimsPrincipal Validate(string token, out SystemJwt.JwtSecurityToken decoded, bool? loggingEnabled = null)
    {
        var tokenHandler = new SystemJwt.JwtSecurityTokenHandler();

        var tvps = new TokenValidationParameters
        {
            ValidateLifetime = true,
            ValidateIssuer = _options.Issuer != null,
            ValidateAudience = _options.Audience != null,
            ValidIssuer = _options.Issuer,
            ValidAudience = _options.Audience
        };

        if (_options.ClockSkew.HasValue)
            tvps.ClockSkew = _options.ClockSkew.Value;

        _options.SecurityMode.ApplyValidation(tvps);

        var cp = tokenHandler.ValidateToken(token, tvps, out SecurityToken s);

        if (_options.TokenContentLogging && loggingEnabled != false)
            LogDefines.LogTokenValidate(log, s.ToString());

        decoded = (SystemJwt.JwtSecurityToken)s;

        return cp;
    }

    private static partial class LogDefines
    {
        [LoggerMessage(0, LogLevel.Debug, "validate: token contents: {TokenContents}", EventName = "TokenValidate")]
        public static partial void LogTokenValidate(ILogger logger, string? tokenContents);
    }
}
