using Microsoft.IdentityModel.Tokens;
using System.Diagnostics;
using System.Security.Cryptography;

namespace Plinth.Security.Jwt;

/// <summary>
/// An <see cref="IJwtSecurityMode"/> implementation for creating JWEs with RSA/OAEP/XXX + AES-CBC-HMAC encryption
/// </summary>
/// <remarks>
/// <para>This works by using RSA to encrypt a random symmetric key and then uses that key
/// and AES encryption to encrypt the body</para>
/// <para>This supports SHA1 and SHA2 (256,384,512) modes for padding</para>
/// </remarks>
public class JwtSecurityModeRsaEncryption : IJwtSecurityMode
{
    private readonly EncryptingCredentials _encryptingCredentials;
    private readonly string[] _algorithms;

    /// <summary>
    /// Constant for RSA-OAEP-256 algorithm name (replaced by SecurityAlgorithms someday)
    /// </summary>
    public const string RsaOAEP256 = SecurityAlgorithms.RsaOAEP + "-256";

    /// <summary>
    /// Constant for RSA-OAEP-384 algorithm name (replaced by SecurityAlgorithms someday)
    /// </summary>
    public const string RsaOAEP384 = SecurityAlgorithms.RsaOAEP + "-384";

    /// <summary>
    /// Constant for RSA-OAEP-512 algorithm name (replaced by SecurityAlgorithms someday)
    /// </summary>
    public const string RsaOAEP512 = SecurityAlgorithms.RsaOAEP + "-512";

    /// <summary>
    /// Construct with a key
    /// </summary>
    /// <param name="privateKeyPem">private key in pem format</param>
    /// <param name="aesKeyLen">Symmetric encryption key length, must be 128, 192, or 256</param>
    /// <param name="rsaOaepHashLen">Length of the RSA OAEP hash length, must be 160 (sha1), 256, 384, or 512</param>
    public JwtSecurityModeRsaEncryption(string privateKeyPem, int aesKeyLen = 256, int rsaOaepHashLen = 256)
    {
        ArgumentNullException.ThrowIfNull(privateKeyPem);

        var rsaPrivate = RSA.Create();
        rsaPrivate.ImportFromPem(privateKeyPem.AsSpan());

        if (rsaPrivate.KeySize < 2048)
            throw new InvalidOperationException("rsa key size is too small, use at least 2048 bits");

        var enc = aesKeyLen switch
        {
            256 => SecurityAlgorithms.Aes256CbcHmacSha512,
            192 => SecurityAlgorithms.Aes192CbcHmacSha384,
            128 => SecurityAlgorithms.Aes128CbcHmacSha256,
            _ => throw new ArgumentException($"AES key length must be one of [256, 384, 512], was {aesKeyLen}", nameof(aesKeyLen))
        };

        var alg = rsaOaepHashLen switch
        {
            160 => SecurityAlgorithms.RsaOAEP,
            256 => RsaOAEP256,
            384 => RsaOAEP384,
            512 => RsaOAEP512,
            _ => throw new ArgumentException($"RSA OAEP hash length must be one of [160, 256, 384, 512], was {aesKeyLen}", nameof(rsaOaepHashLen))
        };

        var rsaKey = new RsaSecurityKey(rsaPrivate) { KeyId = "config" };
        rsaKey.CryptoProviderFactory.CustomCryptoProvider = new RsaOaepCryptoProvider(alg);

        _encryptingCredentials = new EncryptingCredentials(rsaKey, alg, enc);

        _algorithms = [alg, enc];
    }

    /// <inheritdoc cref="IJwtSecurityMode.ApplySecurity(SecurityTokenDescriptor)"/>
    public void ApplySecurity(SecurityTokenDescriptor descriptor)
    {
        descriptor.EncryptingCredentials = _encryptingCredentials;
        descriptor.SigningCredentials = null;
    }

    /// <inheritdoc cref="IJwtSecurityMode.ApplyValidation(TokenValidationParameters)"/>
    public void ApplyValidation(TokenValidationParameters parameters)
    {
        parameters.ValidAlgorithms = _algorithms;
        parameters.RequireSignedTokens = false;
        parameters.TokenDecryptionKey = _encryptingCredentials.Key;
    }

#if NET10_0_OR_GREATER
#error check to see if full RSA-OAEP is supported in latest version of microsoft.identitymodel.tokens (see github issue below)
#endif

    // RSA-OAEP is supported, but uses SHA-1
    // Following the example linked in this github issue I have added support for RSA-OAEP-XXX
    // TODO: replace with built-in support when it becomes available
    // https://github.com/AzureAD/azure-activedirectory-identitymodel-extensions-for-dotnet/issues/1293
    #region RSA OAEP 256/384/512 support
    private sealed class RsaOaepCryptoProvider(string algorithmName) : ICryptoProvider
    {
        public bool IsSupportedAlgorithm(string algorithm, params object[] args)
            => algorithm == algorithmName;

        public object Create(string algorithm, params object[] args)
        {
            Debug.Assert(algorithm == algorithmName);

            // technically, args[1] is a bool which is false for encrypt, true for decrypt, but we don't really care
            return new RsaOaepKeyWrapProvider((RsaSecurityKey)args[0], algorithm);
        }

        // this is never called because we don't dispose our RSA instances
        public void Release(object cryptoInstance)
        {
            ((RsaOaepKeyWrapProvider)cryptoInstance).Dispose();
        }
    }

    private sealed class RsaOaepKeyWrapProvider(RsaSecurityKey key, string algorithm) : KeyWrapProvider
    {
        protected override void Dispose(bool disposing)
        {
        }

        public override byte[] UnwrapKey(byte[] keyBytes)
        {
            var rsa = (RsaSecurityKey)Key;
            return rsa.Rsa.Decrypt(keyBytes, GetPadding());
        }

        public override byte[] WrapKey(byte[] keyBytes)
        {
            var rsa = (RsaSecurityKey)Key;
            return rsa.Rsa.Encrypt(keyBytes, GetPadding());
        }

        private RSAEncryptionPadding GetPadding()
            => Algorithm switch
            {
                SecurityAlgorithms.RsaOAEP => RSAEncryptionPadding.OaepSHA1,
                RsaOAEP256 => RSAEncryptionPadding.OaepSHA256,
                RsaOAEP384 => RSAEncryptionPadding.OaepSHA384,
                RsaOAEP512 => RSAEncryptionPadding.OaepSHA512,
                _ => throw new NotImplementedException(), // this isn't possible
            };

        public override string Algorithm { get; } = algorithm;
        public override string? Context { get; set; } = null;
        public override SecurityKey Key { get; } = key;
    }
    #endregion RSA OAEP 256/384/512 support}
}
