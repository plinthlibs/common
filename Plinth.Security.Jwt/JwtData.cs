using Microsoft.IdentityModel.Tokens;

namespace Plinth.Security.Jwt;

/// <summary>
/// Container for a generated JWT
/// </summary>
public class JwtData
{
    /// <summary>
    /// The encoded JWT token
    /// </summary>
    public string Jwt { get; private set; }

    /// <summary>
    /// When this token becomes valid
    /// </summary>
    public DateTime ValidFrom => _token.ValidFrom;

    /// <summary>
    /// When this token becomes invalid
    /// </summary>
    public DateTime ValidTo => _token.ValidTo;

    /// <summary>
    /// Session ID for token
    /// </summary>
    public Guid SessionGuid { get; private set; }

    private readonly SecurityToken _token;

    internal JwtData(string jwt, SecurityToken token, Guid sessionGuid)
    {
        Jwt = jwt;
        _token = token;
        SessionGuid = sessionGuid;
    }
}
