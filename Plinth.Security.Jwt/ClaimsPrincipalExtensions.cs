using System.Security.Claims;

namespace Plinth.Security.Jwt;

/// <summary>
/// Extensions for getting JWT data out of a claims principal
/// </summary>
public static class ClaimsPrincipalExtensions
{
    /// <summary>
    /// The unique user identifier, often a guid
    /// </summary>
    /// <exception cref="InvalidDataException">if user id claim is missing</exception>
    public static Guid JwtUserId(this ClaimsPrincipal cp)
        => Guid.Parse(cp.FindFirst(ClaimTypes.NameIdentifier)?.Value ?? throw new InvalidDataException("claims principal missing name identifier"));

    /// <summary>
    /// The unique user name, often an email or username
    /// </summary>
    /// <exception cref="InvalidDataException">if user name claim is missing</exception>
    public static string JwtUserName(this ClaimsPrincipal cp)
        => cp.Identity?.Name ?? throw new InvalidDataException("claims principal missing user name");

    /// <summary>
    /// The session id guid, can be null
    /// </summary>
    public static Guid? JwtSessionGuid(this ClaimsPrincipal cp)
    {
        var str = cp.FindFirst("sid")?.Value;
        if (string.IsNullOrEmpty(str) || !Guid.TryParse(str, out Guid g))
            return null;

        return g;
    }

    /// <summary>
    /// The moment of expiration
    /// </summary>
    /// <exception cref="InvalidDataException">if the expiration timestamp is missing</exception>
    public static DateTimeOffset JwtExpiration(this ClaimsPrincipal cp)
    {
        var str = cp.FindFirst("exp")?.Value;
        if (string.IsNullOrEmpty(str) || !long.TryParse(str, out long s))
            throw new InvalidDataException("claims principal missing expiration");
        return DateTimeOffset.FromUnixTimeSeconds(s);
    }

    /// <summary>
    /// The original issue timestamp, may be null
    /// </summary>
    public static DateTimeOffset? JwtOriginalIssue(this ClaimsPrincipal cp)
    {
        var str = cp.FindFirst("ori")?.Value;
        if (string.IsNullOrEmpty(str) || !long.TryParse(str, out long s))
            return null;
        return DateTimeOffset.FromUnixTimeSeconds(s);
    }

    /// <summary>
    /// Enumeration of roles, may be empty
    /// </summary>
    public static IEnumerable<string> JwtRoles(this ClaimsPrincipal cp)
    {
        return cp.FindAll(c => c.Type == ClaimTypes.Role).Select(c => c.Value);
    }
}
