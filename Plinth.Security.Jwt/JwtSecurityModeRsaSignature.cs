using Microsoft.IdentityModel.Tokens;
using System.Security.Cryptography;

namespace Plinth.Security.Jwt;

/// <summary>
/// An <see cref="IJwtSecurityMode"/> implementation for creating JWSs with HMAC signatures
/// </summary>
public class JwtSecurityModeRsaSignature : IJwtSecurityMode
{
    private readonly SigningCredentials? _signingCredentials;
    private readonly RsaSecurityKey _publicKey;
    private readonly string[] _algorithms;

    /// <summary>
    /// Construct with a key
    /// </summary>
    /// <param name="publicKeyPem">public key in pem format</param>
    /// <param name="privateKeyPem">(optional) private key in pem format</param>
    /// <param name="hashSize">Must be 256, 384, or 512</param>
    /// <remarks>if not supplying a private key, this can only be used to validate tokens, not create them</remarks>
    public JwtSecurityModeRsaSignature(string publicKeyPem, string? privateKeyPem = null, int hashSize = 256)
    {
        ArgumentNullException.ThrowIfNull(publicKeyPem);

        string alg;
        string valdAlg;
        switch (hashSize)
        {
            case 256:
                alg = SecurityAlgorithms.RsaSha256Signature;
                valdAlg = SecurityAlgorithms.RsaSha256;
                break;
            case 384:
                alg = SecurityAlgorithms.RsaSha384Signature;
                valdAlg = SecurityAlgorithms.RsaSha384;
                break;
            case 512:
                alg = SecurityAlgorithms.RsaSha512Signature;
                valdAlg = SecurityAlgorithms.RsaSha512;
                break;
            default:
                throw new ArgumentException($"key length must be one of [256, 384, 512], was {hashSize}", nameof(hashSize));
        }

        var rsaPublic = RSA.Create();
        rsaPublic.ImportFromPem(publicKeyPem.AsSpan());
        _publicKey = new RsaSecurityKey(rsaPublic);

        if (privateKeyPem != null)
        {
            var rsaPrivate = RSA.Create();
            rsaPrivate.ImportFromPem(privateKeyPem.AsSpan());

            if (rsaPrivate.KeySize < 2048)
                throw new InvalidOperationException("rsa key size is too small, use at least 2048 bits");

            if (!Enumerable.SequenceEqual(rsaPublic.ExportRSAPublicKey(), rsaPrivate.ExportRSAPublicKey()))
                throw new InvalidOperationException("public key given does not match private key");

            _signingCredentials = new SigningCredentials(new RsaSecurityKey(rsaPrivate) { KeyId = "config" }, alg);
        }

        _algorithms = [valdAlg];
    }

    /// <inheritdoc cref="IJwtSecurityMode.ApplySecurity(SecurityTokenDescriptor)"/>
    public void ApplySecurity(SecurityTokenDescriptor descriptor)
    {
        if (_signingCredentials == null)
            throw new InvalidOperationException("private key was not set, cannot sign tokens");

        descriptor.EncryptingCredentials = null;
        descriptor.SigningCredentials = _signingCredentials;
    }

    /// <inheritdoc cref="IJwtSecurityMode.ApplyValidation(TokenValidationParameters)"/>
    public void ApplyValidation(TokenValidationParameters parameters)
    {
        parameters.ValidateIssuerSigningKey = true;
        parameters.ValidAlgorithms = _algorithms;
        parameters.RequireSignedTokens = true;
        parameters.IssuerSigningKey = _publicKey;
    }
}
