# README

### Plinth.Security.Jwt

**Jwt signing and encryption utilities, add on to Plinth.Security**

* Supports Signed JWT (JWS) via
	* HMAC with SHA-{256, 384, 512}
	* RSA with SHA-{256, 384, 512}
* Supports Encrypted JWT (JWE) via
	* AES-{128, 192, 256}-CBC-HMAC-{256, 384, 512}
	* RSA-OAEP-{160, 256, 384, 512} with AES-{128, 192, 256}-CBC-HMAC-{256, 384, 512}
