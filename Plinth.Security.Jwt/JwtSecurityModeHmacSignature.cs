using Microsoft.IdentityModel.Tokens;

namespace Plinth.Security.Jwt;

/// <summary>
/// An <see cref="IJwtSecurityMode"/> implementation for creating JWSs with HMAC signatures
/// </summary>
public class JwtSecurityModeHmacSignature : IJwtSecurityMode
{
    private readonly SigningCredentials _signingCredentials;
    private readonly string[] _algorithms;

    /// <summary>
    /// Construct with a key
    /// </summary>
    /// <param name="key">Must be 32, 48, or 64 bytes long</param>
    public JwtSecurityModeHmacSignature(byte[] key)
    {
        ArgumentNullException.ThrowIfNull(key);

        string alg;
        string valdAlg;
        switch (key.Length * 8)
        {
            case 256:
                alg = SecurityAlgorithms.HmacSha256Signature;
                valdAlg = SecurityAlgorithms.HmacSha256;
                break;
            case 384:
                alg = SecurityAlgorithms.HmacSha384Signature;
                valdAlg = SecurityAlgorithms.HmacSha384;
                break;
            case 512:
                alg = SecurityAlgorithms.HmacSha512Signature;
                valdAlg = SecurityAlgorithms.HmacSha512;
                break;
            default:
                throw new ArgumentException($"key length must be one of [256, 384, 512], was {key.Length * 8}", nameof(key));
        }

        _signingCredentials = new SigningCredentials(new SymmetricSecurityKey(key) { KeyId = "config" }, alg);
        _algorithms = [valdAlg];
    }

    /// <inheritdoc cref="IJwtSecurityMode.ApplySecurity(SecurityTokenDescriptor)"/>
    public void ApplySecurity(SecurityTokenDescriptor descriptor)
    {
        descriptor.EncryptingCredentials = null;
        descriptor.SigningCredentials = _signingCredentials;
    }

    /// <inheritdoc cref="IJwtSecurityMode.ApplyValidation(TokenValidationParameters)"/>
    public void ApplyValidation(TokenValidationParameters parameters)
    {
        parameters.ValidateIssuerSigningKey = true;
        parameters.ValidAlgorithms = _algorithms;
        parameters.RequireSignedTokens = true;
        parameters.IssuerSigningKey = _signingCredentials.Key;
    }
}
