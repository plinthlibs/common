﻿namespace Plinth.Templating;

/// <summary>
/// Base class for templates (provides extra features like IncludeAsync)
/// </summary>
public abstract class Template<T> : RazorLight.TemplatePage<T>
{
    /// <summary>
    /// constructor
    /// </summary>
    protected Template()
    {
    }
}

/// <summary>
/// Base class for templates without a model (provides extra features like IncludeAsync)
/// </summary>
/// <remarks>when rendering, pass null for the model</remarks>
public abstract class Template : RazorLight.TemplatePage<object>
{
    /// <summary>
    /// constructor
    /// </summary>
    protected Template()
    {
    }
}
