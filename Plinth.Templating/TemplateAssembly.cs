﻿using System.Reflection;

namespace Plinth.Templating;

internal class TemplateAssembly
{
    public Assembly Assembly { get; set; } = null!;
    public string Namespace { get; set; } = null!;
    public ISet<string> Folders { get; set; } = null!;
}
