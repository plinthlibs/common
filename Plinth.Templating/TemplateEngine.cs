using RazorLight;
using RazorLight.Razor;
using System.Dynamic;
using System.Reflection;
using System.Resources;
using System.Data;
using Microsoft.Extensions.Logging;
using Plinth.Common.Utils;

namespace Plinth.Templating;

/// <summary>
/// Engine for compiling and rendering razor based templates
/// </summary>
public class TemplateEngine
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private readonly RazorLightEngine _engine;
    private readonly MyResourcesProject _proj;

    internal TemplateEngine(List<TemplateAssembly> assemblies)
    {
        _proj = new MyResourcesProject(assemblies);
        _engine = new RazorLightEngineBuilder()
            .UseProject(_proj)
            .UseMemoryCachingProvider()
            .Build();
    }

    /// <summary>
    /// Optionally precompile the templates, since compilation is somewhat slow
    /// </summary>
    public Task PrecompileTemplatesAsync()
        => _proj.PrecompileAll(_engine);

    /// <summary>
    /// Optionally precompile the templates in the background, since compilation is somewhat slow
    /// </summary>
    public void PrecompileTemplatesNoWait()
        => Task.Run( PrecompileTemplatesAsync );

    #region Render (template)
    /// <summary>
    /// Render an embedded template (.cshtml)
    /// </summary>
    /// <typeparam name="T">the @Model type</typeparam>
    /// <param name="templateId">the path for the template</param>
    /// <param name="model">the @Model object</param>
    /// <returns>the rendered template</returns>
    public Task<string> RenderAsync<T>(string templateId, T model)
        => RenderWithViewBagAsync(templateId, model, null);

    /// <summary>
    /// Render an embedded template (.cshtml)
    /// </summary>
    /// <param name="templateId">the path for the template</param>
    /// <returns>the rendered template</returns>
    public Task<string> RenderAsync(string templateId)
        => RenderWithViewBagAsync<object>(templateId, null, null);

    /// <summary>
    /// Render an embedded template (.cshtml) with a view bag
    /// </summary>
    /// <typeparam name="T">the @Model type</typeparam>
    /// <param name="templateId">the path for the template</param>
    /// <param name="model">the @Model object</param>
    /// <param name="viewBag">ViewBag contents</param>
    /// <returns>the rendered template</returns>
    public Task<string> RenderWithViewBagAsync<T>(string templateId, T? model, ExpandoObject? viewBag)
        => _engine.CompileRenderAsync(templateId, model, viewBag);

    /// <summary>
    /// Render an embedded template (.cshtml) with a view bag
    /// </summary>
    /// <param name="templateId">the path for the template</param>
    /// <param name="viewBag">ViewBag contents</param>
    /// <returns>the rendered template</returns>
    public Task<string> RenderWithViewBagAsync(string templateId, ExpandoObject viewBag)
        => RenderWithViewBagAsync<object>(templateId, null, viewBag);
    #endregion

    #region Render (raw)
    /// <summary>
    /// Render a raw string template, and cache for future use
    /// </summary>
    /// <typeparam name="T">the @Model type</typeparam>
    /// <param name="templateUid">a unique id for referencing this template for caching purposes</param>
    /// <param name="templateContent">the razor template content</param>
    /// <param name="model">the @Model object</param>
    /// <remarks>note that subsequent calls with the same <paramref name="templateUid"/> will ignore the <paramref name="templateContent"/></remarks>
    /// <returns>the rendered template</returns>
    public Task<string> RenderRawAsync<T>(string templateUid, string templateContent, T model)
        => RenderRawWithViewBagAsync(templateUid, templateContent, model, null);

    /// <summary>
    /// Render a raw string template, and cache for future use
    /// </summary>
    /// <param name="templateUid">a unique id for referencing this template for caching purposes</param>
    /// <param name="templateContent">the razor template content</param>
    /// <remarks>note that subsequent calls with the same <paramref name="templateUid"/> will ignore the <paramref name="templateContent"/></remarks>
    /// <returns>the rendered template</returns>
    public Task<string> RenderRawAsync(string templateUid, string templateContent)
        => RenderRawWithViewBagAsync<object>(templateUid, templateContent, null, null);

    /// <summary>
    /// Render a raw string template with a view bag, and cache for future use
    /// </summary>
    /// <typeparam name="T">the @Model type</typeparam>
    /// <param name="templateUid">a unique id for referencing this template for caching purposes</param>
    /// <param name="templateContent">the razor template content</param>
    /// <param name="model">the @Model object</param>
    /// <param name="viewBag">ViewBag contents</param>
    /// <remarks>note that subsequent calls with the same <paramref name="templateUid"/> will ignore the <paramref name="templateContent"/></remarks>
    /// <returns>the rendered template</returns>
    public Task<string> RenderRawWithViewBagAsync<T>(string templateUid, string templateContent, T? model, ExpandoObject? viewBag)
    {
        if (string.IsNullOrEmpty(templateUid)) throw new ArgumentNullException(nameof(templateUid));
        return _engine.CompileRenderStringAsync("--RT--" + templateUid, templateContent, model, viewBag);
    }

    /// <summary>
    /// Render a raw string template with a view bag, and cache for future use
    /// </summary>
    /// <param name="templateUid">a unique id for referencing this template for caching purposes</param>
    /// <param name="templateContent">the razor template content</param>
    /// <param name="viewBag">ViewBag contents</param>
    /// <remarks>note that subsequent calls with the same <paramref name="templateUid"/> will ignore the <paramref name="templateContent"/></remarks>
    /// <returns>the rendered template</returns>
    public Task<string> RenderRawWithViewBagAsync(string templateUid, string templateContent, ExpandoObject viewBag)
        => RenderRawWithViewBagAsync<object>(templateUid, templateContent, null, viewBag);
    #endregion

    #region Render (raw, once)
    /// <summary>
    /// Render a raw string template for one-off use
    /// </summary>
    /// <typeparam name="T">the @Model type</typeparam>
    /// <param name="templateContent">the razor template content</param>
    /// <param name="model">the @Model object</param>
    /// <returns>the rendered template</returns>
    public Task<string> RenderRawOnceAsync<T>(string templateContent, T model)
        => RenderRawOnceWithViewBagAsync(templateContent, model, null);

    /// <summary>
    /// Render a raw string template for one-off use
    /// </summary>
    /// <param name="templateContent">the razor template content</param>
    /// <returns>the rendered template</returns>
    public Task<string> RenderRawOnceAsync(string templateContent)
        => RenderRawOnceWithViewBagAsync<object>(templateContent, null, null);

    /// <summary>
    /// Render a raw string template for one-off use, with a view bag
    /// </summary>
    /// <typeparam name="T">the @Model type</typeparam>
    /// <param name="templateContent">the razor template content</param>
    /// <param name="model">the @Model object</param>
    /// <param name="viewBag">ViewBag contents</param>
    /// <returns>the rendered template</returns>
    public Task<string> RenderRawOnceWithViewBagAsync<T>(string templateContent, T? model, ExpandoObject? viewBag)
    {
        var k = Guid.NewGuid().ToString();
        var ret = _engine.CompileRenderStringAsync(k, templateContent, model, viewBag);
        _engine.Handler.Cache.Remove(k);
        return ret;
    }

    /// <summary>
    /// Render a raw string template for one-off use, with a view bag
    /// </summary>
    /// <param name="templateContent">the razor template content</param>
    /// <param name="viewBag">ViewBag contents</param>
    /// <returns>the rendered template</returns>
    public Task<string> RenderRawOnceWithViewBagAsync(string templateContent, ExpandoObject viewBag)
        => RenderRawOnceWithViewBagAsync<object>(templateContent, null, viewBag);
    #endregion

    /// <summary>
    /// finds all cshtml files embedded within an assembly that match the given folders
    /// </summary>
    private static IEnumerable<string> GetCshtmlFromFolder(Assembly assembly, string assemblyName, IEnumerable<string> folders, ISet<string> uniqueNames)
    {
        var unique = new Dictionary<string, ISet<string>>();

        foreach (var folder in folders.Concat(Enumerable.Repeat("", 1)))
        {
            var prefix = assemblyName + '.' + folder;

            var resources = assembly
                .GetManifestResourceNames()
                .Where(n => n.StartsWith(prefix) 
                    && n.EndsWith(".cshtml") 
                    && !n.Substring(prefix.Length + 2, n.Length - prefix.Length - 9).Contains('.'));

            foreach (var resource in resources)
            {
                var code = resource[(assemblyName.Length + 1)..]
                    .Replace(".cshtml", string.Empty);

                var fileName = code[(code.LastIndexOf('.') + 1)..];
                if (!unique.ContainsKey(fileName))
                    unique.Add(fileName, new HashSet<string>());
                unique[fileName].Add(code);

                yield return code;
            }
        }

        foreach (var kv in unique)
        {
            if (kv.Value.Count > 1)
                throw new NotSupportedException($"Found two templates in namespace '{assemblyName}' which have the same name '{kv.Key}.cshtml' [{string.Join(",",kv.Value)}]");
            if (!uniqueNames.Add(kv.Key))
                throw new NotSupportedException($"Found two templates in different assemblies which have the same name '{kv.Key}.cshtml'");
        }
    }

    /// <summary>
    /// custom override of embedded project for finding templates
    /// </summary>
    private class MyResourcesProject : RazorLightProject
    {
        private readonly List<MyTemplateAssembly> _assemblies;

        private class MyTemplateAssembly
        {
            public TemplateAssembly Assembly { get; set; } = null!;
            public EmbeddedRazorProject Project { get; set; } = null!;
            public HashSet<string> PotentialTemplates { get; set; } = null!;
        }

        public MyResourcesProject(List<TemplateAssembly> assemblies)
        {
            var uniqueNames = new HashSet<string>();

            _assemblies = assemblies.Select(a => new MyTemplateAssembly()
            {
                Assembly = a,
                Project = new EmbeddedRazorProject(a.Assembly, a.Namespace),
                PotentialTemplates = new HashSet<string>(GetCshtmlFromFolder(a.Assembly, a.Namespace, a.Folders, uniqueNames))
            }).ToList();
        }

        public Task PrecompileAll(RazorLightEngine engine)
        {
            var cshtmls = _assemblies.SelectMany(a => a.PotentialTemplates);
            return Parallel.ForEachAsync(cshtmls, async (cshtml, _) => await engine.CompileTemplateAsync(cshtml));
        }

        /// <summary>
        /// this gets called during compile to load a template, both at the root _engine.Compile, and 'Layout = ', and 'await IncludeAsync'
        /// </summary>
        public override async Task<RazorLightProjectItem> GetItemAsync(string templateKey)
        {
            log.Debug($"Loading Template '{templateKey}'");

            var templateId = ProcessKey(templateKey);

            RazorLightProjectItem? item = null;
            // first check for known folders
            foreach (var a in _assemblies)
            {
                if (a.PotentialTemplates.Contains(templateId))
                    item = setItem(await a.Project.GetItemAsync(templateId), templateId, a.Assembly.Namespace);

                foreach (var folder in a.Assembly.Folders)
                {
                    var tId = folder + '.' + templateId;
                    if (a.PotentialTemplates.Contains(tId))
                        item = setItem(await a.Project.GetItemAsync(tId), tId, a.Assembly.Namespace);
                }
            }

            // fall back to fully qualified names
            if (item == null)
            {
                foreach (var a in _assemblies)
                    item = setItem(await a.Project.GetItemAsync(templateId), templateId, a.Assembly.Namespace);
            }

            if (item != null && item.Exists)
                return item;

            throw new MissingManifestResourceException($"Could not find '{templateKey}' in any assembly, make sure it is an embedded resource");

            RazorLightProjectItem? setItem(RazorLightProjectItem rlpi, string id, string ns)
            {
                if (!rlpi.Exists)
                    return item;
                log.Debug($"Found Template '{id}' in {ns}");
                if (item != null)
                    throw new NotSupportedException($"Found two templates which match {item.Key}");
                return rlpi;
            }
        }

        // support "Page1/Home" style
        private static string ProcessKey(string tk) => tk.Replace('/', '.');

        public override Task<IEnumerable<RazorLightProjectItem>> GetImportsAsync(string templateKey)
            => Task.FromResult(Enumerable.Empty<RazorLightProjectItem>());
    }
}
