using System.Reflection;

namespace Plinth.Templating;

/// <summary>
/// Builder for <see cref="TemplateEngine"/>
/// </summary>
public class TemplateEngineBuilder
{
    private readonly List<TemplateAssembly> _assemblies = [];
    private bool _precompile = false;

    /// <summary>
    /// Create a template engine builder
    /// </summary>
    public TemplateEngineBuilder()
    {
    }

    /// <summary>
    /// Add a template assembly to the engine
    /// </summary>
    /// <param name="assembly">assembly containing the templates (*.cshtml)</param>
    /// <param name="rootNamespace">root namespace of the templates</param>
    /// <param name="cshtmlFolders">(optional) Which folders in <paramref name="assembly"/> contain template files, root is always included</param>
    public TemplateEngineBuilder AddTemplateAssembly(Assembly assembly, string rootNamespace, IEnumerable<string>? cshtmlFolders = null)
    {
        ArgumentNullException.ThrowIfNull(assembly);
#if NET8_0_OR_GREATER
        ArgumentException.ThrowIfNullOrWhiteSpace(rootNamespace);
#else
        if (string.IsNullOrWhiteSpace(rootNamespace)) throw new ArgumentNullException(nameof(rootNamespace));
#endif

        DupeCheck(assembly);
        _assemblies.Add(new TemplateAssembly
        {
            Assembly = assembly,
            Namespace = rootNamespace,
            Folders = new HashSet<string>(cshtmlFolders?.Select(f => f.Replace('/', '.')) ?? [])
        });
        return this;
    }

    /// <summary>
    /// Add a template assembly to the engine
    /// </summary>
    /// <param name="assembly">assembly containing the templates (*.cshtml)</param>
    /// <param name="cshtmlFolders">(optional) Which folders in <paramref name="assembly"/> contain template files, root is always included</param>
    /// <remarks>assembly name is used for root namespace</remarks>
    public TemplateEngineBuilder AddTemplateAssembly(Assembly assembly, IEnumerable<string>? cshtmlFolders = null)
        => AddTemplateAssembly(assembly, assembly?.GetName().Name ?? string.Empty, cshtmlFolders);

    /// <summary>
    /// Set this to precompile all found templates in the background
    /// </summary>
    public TemplateEngineBuilder PrecompileInBackground()
    {
        _precompile = true;
        return this;
    }

    /// <summary>
    /// Build a <see cref="TemplateEngine"/>
    /// </summary>
    public TemplateEngine Build()
    {
        var engine = new TemplateEngine(_assemblies);
        if (_precompile)
            engine.PrecompileTemplatesNoWait();
        return engine;
    }

    private void DupeCheck(Assembly assembly)
    {
        foreach (var a in _assemblies)
        {
            if (a.Assembly == assembly)
                throw new NotSupportedException($"The Assembly '{assembly.GetName().Name}' has been provided twice");
        }
    }
}
