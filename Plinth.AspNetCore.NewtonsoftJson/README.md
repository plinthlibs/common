# README

### Plinth.AspNetCore.NewtonsoftJson

**Newtonsoft.Json support for Plinth.AspNetCore**

Adds support for [Newtonsoft.Json](https://www.newtonsoft.com/json) as the serialization library for ASP&#46;NET Core applications.

Currently this is automatically used when calling `.AddPlinthControllerDefault()` in startup

```c#
        services.AddControllers()
                .AddPlinthControllerDefaults();
```
