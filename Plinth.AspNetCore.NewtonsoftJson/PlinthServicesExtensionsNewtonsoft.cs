﻿using Microsoft.Extensions.DependencyInjection;
using Plinth.Serialization;

namespace Plinth.AspNetCore.Newtonsoft;

/// <summary>
/// Extensions for enabling Newtonsoft.Json in aspnetcore projects
/// </summary>
public static class PlinthServicesExtensionsNewtonsoft
{
    /// <summary>
    /// Enable Newtonsoft.Json in an aspnetcore project with default Plinth.Serialization settings
    /// </summary>
    /// <param name="mvc"></param>
    /// <returns></returns>
    public static IMvcBuilder AddPlinthControllerNewtonsoftJson(this IMvcBuilder mvc)
    {
        mvc.AddNewtonsoftJson(options =>
        {
            JsonUtil.ApplyDefaultSettings(options.SerializerSettings);
        });
        return mvc;
    }
}
