using Plinth.HttpApiClient.Common;
using Polly;
using RestSharp;
using System.Net;

namespace Plinth.HttpApiClient.RestSharp;

/// <summary>
/// An ApiClient which adds Polly policies for use by Api client authors
/// </summary>
/// <remarks>
/// Policies should be constructed either using <c>PolicyExt.HandleFailedRequest</c> or <c>Policy.HandleResult&gt;RestResponse&lt;(r => ...)</c>.
/// The concept is that a policy should define when it kicks in (e.g. on failed request, or StatusCode == 409),
/// and what to do if it does (e.g. Retry, FallBack, CircuitBreaker, etc).
/// </remarks>
/// <example>
/// Derive your client from WebApiClientPolly.
/// Use ExecuteWithRetries() instead of Execute()
/// Use ExecutePolly() instead of Execute()
/// It is best to create policies statically and reuse them, especially if using Circuit Breakers and the like which use context across requests
/// </example>
/// <see href="https://github.com/App-vNext/Polly/wiki">Read this in depth if you plan to use Polly</see>
public class BaseHttpApiClientPolly : BaseHttpApiClient
{
    /// <inheritdoc cref="BaseHttpApiClient(string, string, TimeSpan?)"/>
    public BaseHttpApiClientPolly(string serviceId, string serverUrl, TimeSpan? timeout)
        : base(serviceId, serverUrl, timeout)
    {
    }

    /// <inheritdoc cref="BaseHttpApiClient(string, string, TimeSpan?, ConfigureSerialization?)"/>
    public BaseHttpApiClientPolly(string serviceId, string serverUrl, TimeSpan? timeout = null, ConfigureSerialization? configureSerialization = null)
        : base(serviceId, serverUrl, timeout, configureSerialization)
    {
    }

    /// <summary>
    /// Execute a request with a policy
    /// </summary>
    /// <param name="request"></param>
    /// <param name="policy"></param>
    protected RestResponse ExecutePolly(RestRequest request, ISyncPolicy<RestResponse> policy)
    {
        return ExecuteIntercept(request, (r, f) => HandleResult(request, policy.ExecuteAndCapture(() => f(r))));
    }

    /// <summary>
    /// Execute a Request with a policy
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="request"></param>
    /// <param name="policy"></param>
    /// <returns>an object of type T</returns>
    protected T? ExecutePolly<T>(RestRequest request, ISyncPolicy<RestResponse> policy)
    {
        return ExecuteIntercept<T>(request, (r, f) => HandleResult(request, policy.ExecuteAndCapture(() => f(r))));
    }

    /// <summary>
    /// Execute an async request with a policy
    /// </summary>
    /// <param name="request"></param>
    /// <param name="policy"></param>
    /// <param name="cancellationToken"></param>
    protected async Task<RestResponse> ExecutePollyAsync(RestRequest request, IAsyncPolicy<RestResponse> policy, CancellationToken cancellationToken = default)
    {
        return await ExecuteInterceptAsync(request, async (r, f) => 
            HandleResult(request, await policy.ExecuteAndCaptureAsync(() => f(r))),
            cancellationToken
        );
    }

    /// <summary>
    /// Execute an async Request with a policy
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="request"></param>
    /// <param name="policy"></param>
    /// <param name="cancellationToken"></param>
    /// <returns>an object of type T</returns>
    protected async Task<T?> ExecutePollyAsync<T>(RestRequest request, IAsyncPolicy<RestResponse> policy, CancellationToken cancellationToken = default)
    {
        return await ExecuteInterceptAsync<T>(request, async (r, f) => 
            HandleResult(request, await policy.ExecuteAndCaptureAsync(() => f(r))),
            cancellationToken
        );
    }

    private RestResponse HandleResult(RestRequest request, PolicyResult<RestResponse> pr)
    {
        if (pr.Outcome == OutcomeType.Successful)
            return pr.Result;

        if (pr.FinalException != null)
            throw pr.FinalException;

        if (pr.FinalHandledResult != null)
        {
            base.CheckForErrors(request, pr.FinalHandledResult, null);
            return pr.FinalHandledResult;
        }

        throw new RestException(HttpStatusCode.InternalServerError, "unhandled policy fault, no result", string.Empty);
    }

    /// <summary>
    /// Execute an async request with a default policy for safely retrying
    /// </summary>
    /// <remarks><see cref="PolicyExt.HandleFailedRetryableRequest()"/></remarks>
    /// <param name="request"></param>
    /// <param name="retries">how many retries (additional beyond initial try)</param>
    /// <param name="retryDelay">how long to delay between retries, TimeSpan.Zero for none</param>
    protected RestResponse ExecuteWithRetries(RestRequest request, int retries, TimeSpan retryDelay)
    {
        var policy = PolicyExt.HandleFailedRetryableRequest().WaitAndRetry(retries, _ => retryDelay);

        return ExecutePolly(request, policy);
    }

    /// <summary>
    /// Execute an async request with a default policy for safely retrying
    /// </summary>
    /// <remarks><see cref="PolicyExt.HandleFailedRetryableRequest()"/></remarks>
    /// <param name="request"></param>
    /// <param name="retries">how many retries (additional beyond initial try)</param>
    /// <param name="retryDelay">how long to delay between retries, TimeSpan.Zero for none</param>
    protected T? ExecuteWithRetries<T>(RestRequest request, int retries, TimeSpan retryDelay)
    {
        var policy = PolicyExt.HandleFailedRetryableRequest().WaitAndRetry(retries, _ => retryDelay);

        return ExecutePolly<T>(request, policy);
    }

    /// <summary>
    /// Execute an async request with a default policy for safely retrying
    /// </summary>
    /// <remarks><see cref="PolicyExt.HandleFailedRetryableRequest()"/></remarks>
    /// <param name="request"></param>
    /// <param name="retries">how many retries (additional beyond initial try)</param>
    /// <param name="retryDelay">how long to delay between retries, TimeSpan.Zero for none</param>
    protected Task<RestResponse> ExecuteWithRetriesAsync(RestRequest request, int retries, TimeSpan retryDelay)
    {
        var policy = PolicyExt.HandleFailedRetryableRequest().WaitAndRetryAsync(retries, _ => retryDelay);

        return ExecutePollyAsync(request, policy);
    }

    /// <summary>
    /// Execute an async request with a default policy for safely retrying
    /// </summary>
    /// <remarks><see cref="PolicyExt.HandleFailedRetryableRequest()"/></remarks>
    /// <param name="request"></param>
    /// <param name="retries">how many retries (additional beyond initial try)</param>
    /// <param name="retryDelay">how long to delay between retries, TimeSpan.Zero for none</param>
    protected Task<T?> ExecuteWithRetriesAsync<T>(RestRequest request, int retries, TimeSpan retryDelay)
    {
        var policy = PolicyExt.HandleFailedRetryableRequest().WaitAndRetryAsync(retries, _ => retryDelay);

        return ExecutePollyAsync<T>(request, policy);
    }
}

/// <summary>
/// Extentions for the Policy static class
/// </summary>
public static class PolicyExt
{
    /// <summary>
    /// Handy extention for building policies which handle RestResponses which have failed (RestResponse::IsSuccessful)
    /// </summary>
    /// <remarks>Ensure that the server you are calling can handle retrying on 400s/500s</remarks>
    public static PolicyBuilder<RestResponse> HandleFailedRequest() => Policy.HandleResult<RestResponse>(r => !r.IsSuccessful);

    /// <summary>
    /// Handy extention for building policies which handle RestResponses which can be safely retried such as 502/503 and connection failure
    /// </summary>
    public static PolicyBuilder<RestResponse> HandleFailedRetryableRequest()
        => Policy.HandleResult<RestResponse>(r =>
            r.StatusCode == 0
            || r.StatusCode == HttpStatusCode.BadGateway
            || r.StatusCode == HttpStatusCode.ServiceUnavailable);
}
