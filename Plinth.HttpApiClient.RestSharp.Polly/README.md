# README

### Plinth.HttpApiClient.RestSharp.Polly

**Extensions for using Polly with Plinth.HttpApiClient.RestSharp**

Example usage:
```c#
public Task<GetResponse?> GetThingRetryAsync(int x, int y)
{
    var r = MakeRequest($"api/v1/thing/{x}/get", Method.Get);
    r.AddQueryParameter("y", y.ToString());
    return ExecuteWithRetriesAsync<GetResponse>(r, 10, TimeSpan.Zero);
}

public async Task<GetResponse?> GetThingAsync(int x, int y, IAsyncPolicy<RestResponse> policy)
{
    var r = MakeRequest($"api/v1/thing/{x}/get", Method.Get);
    r.AddQueryParameter("y", y.ToString());
    return await ExecutePollyAsync<GetResponse>(r, policy);
}
```
