using Microsoft.ApplicationInsights.Channel;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.ApplicationInsights.Extensibility;

namespace Plinth.Logging.NLog.AppInsights;

internal class RequestsTelemetryFilter(ITelemetryProcessor next, AppInsightsSettings settings) : ITelemetryProcessor
{
    public void Process(ITelemetry item)
    {
        if (!Allow(item))
            return;

        next.Process(item);
    }

    private bool Allow(ITelemetry item)
    {
        // no settings, no filter
        if (settings.DisabledPrefixes.IsNullOrEmpty() && settings.EnabledPrefixes.IsNullOrEmpty() && settings.CustomFilter is null)
            return true;

        // only filter requests for urls
        if (item is not RequestTelemetry req)
            return true;

        // use custom filter exclusively if requested
        if (settings.CustomFilter is not null)
            return settings.CustomFilter(req);

        // only filter urls
        if (req.Url is null)
            return true;

        // explicitly disabled prefixes
        foreach (var prefix in settings.DisabledPrefixes)
            if (req.Url.AbsolutePath.StartsWith(prefix))
                return false;

        // explicitly enabled prefixes
        if (settings.EnabledPrefixes is not null)
        {
            foreach (var prefix in settings.EnabledPrefixes)
                if (req.Url.AbsolutePath.StartsWith(prefix))
                    return true;

            // if you specify a whitelist, we filter out anything that doesn't match
            return false;
        }
        else
        {
            // otherwise, let everything else through
            return true;
        }
    }
}
