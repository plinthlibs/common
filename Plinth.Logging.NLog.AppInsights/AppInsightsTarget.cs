// -----------------------------------------------------------------------
// <copyright file="ApplicationInsightsTarget.cs" company="Microsoft">
// Copyright (c) Microsoft Corporation. 
// All rights reserved.  2013
// </copyright>
// -----------------------------------------------------------------------

using System.Globalization;
using Microsoft.ApplicationInsights.Channel;
using Microsoft.ApplicationInsights.DataContracts;

using NLog;
using NLog.Common;
using NLog.Targets;
using Microsoft.ApplicationInsights;
using NLog.LayoutRenderers;
using Plinth.Logging.NLog.Renderers;
using Plinth.Common.Logging;
using System.Diagnostics;
using Microsoft.ApplicationInsights.Extensibility;

namespace Plinth.Logging.NLog.AppInsights;

/// <summary>
/// NLog Target that routes all logging output to the Application Insights logging framework.
/// The messages will be uploaded to the Application Insights cloud service.
/// </summary>
/// <remarks>Modified for Plinth</remarks>
[Target("AppInsightsTarget")]
public sealed class AppInsightsTarget : TargetWithContext
{
    private TelemetryClient? telemetryClient;
    private DateTime lastLogEventTime;
    private global::NLog.Layouts.Layout instrumentationKeyLayout = string.Empty;
    private global::NLog.Layouts.Layout connectionStringLayout = string.Empty;
    private global::NLog.Layouts.Layout roleNameLayout = string.Empty;
    private global::NLog.Layouts.Layout roleInstanceLayout = string.Empty;

    private readonly FilteredMessageRenderer _msgRender = new();

    private static readonly string _assembly, _product, _component, _environment;
    private static readonly LayoutRenderer _reqTraceId = new RequestTraceIdRenderer();
    private static readonly LayoutRenderer _reqChain = new RequestTraceChainRenderer();
    private static readonly LayoutRenderer _threadId = new ThreadIdLayoutRenderer();
    private static readonly LayoutRenderer _asyncCtx = new AsyncCtxRenderer();

    static AppInsightsTarget()
    {
        _assembly = new AssemblyVersionRenderer().Render(null);
        _product = LogManager.Configuration.Variables.TryGetValue("product", out var product) ? product.Render(new LogEventInfo()) : string.Empty;
        _component = LogManager.Configuration.Variables.TryGetValue("component", out var component) ? component.Render(new LogEventInfo()) : string.Empty;
        _environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? string.Empty;
    }

    private static AppInsightsTarget? _instance;
    /// <summary>
    /// Used by plinth internals to start an app insights telemetry session, for non asp.net core flows (like hangfire)
    /// </summary>
    /// <param name="opName">operation name</param>
    /// <returns>an object that should be .Dispose()d</returns>
    internal static IDisposable StartTelemetrySession(string opName)
    {
        return _instance?.telemetryClient?.StartOperation<RequestTelemetry>(opName)!;
    }

    /// <summary>
    /// Initializers a new instance of ApplicationInsightsTarget type.
    /// </summary>
    public AppInsightsTarget()
    {
        this.Layout = @"${message}";

        _instance = this;
    }

    /// <summary>
    /// (Obsolete) Gets or sets the Application Insights instrumentationKey for your application. 
    /// </summary>
    [Obsolete("Instrumentation Keys are obsolete, prefer Connection Strings, 2022-07-12")]
    public string? InstrumentationKey
    {
        get => (this.instrumentationKeyLayout as global::NLog.Layouts.SimpleLayout)?.Text;
        set => this.instrumentationKeyLayout = value ?? string.Empty;
    }

    /// <summary>
    /// Gets or sets the Application Insights connection string for your application. 
    /// </summary>
    public string? ConnectionString
    {
        get => (this.connectionStringLayout as global::NLog.Layouts.SimpleLayout)?.Text;
        set => this.connectionStringLayout = value ?? string.Empty;
    }

    /// <summary>
    /// Gets or sets the Application Insights cloud role name for your application. 
    /// </summary>
    public string? CloudRoleName
    {
        get => (this.roleNameLayout as global::NLog.Layouts.SimpleLayout)?.Text;
        set => this.roleNameLayout = value ?? string.Empty;
    }

    /// <summary>
    /// Gets or sets the Application Insights cloud role instance for your application. 
    /// </summary>
    public string? CloudRoleInstance
    {
        get => (this.roleInstanceLayout as global::NLog.Layouts.SimpleLayout)?.Text;
        set => this.roleInstanceLayout = value ?? string.Empty;
    }

    private int _maxLen = 25000;
    /// <summary>
    /// Max message length
    /// </summary>
    [System.ComponentModel.DefaultValue(25000)]
    public int MaxLength
    {
        get => _maxLen;
        set
        {
            _maxLen = value;
            _msgRender.MaxLength = _maxLen;
        }
    }

    /// <summary>
    /// Custom sensitive data masker
    /// </summary>
    public ISensitiveDataMasker? SensitiveDataMasker
    {
        get => _msgRender.SensitiveDataMasker;
        set
        {
            _msgRender.SensitiveDataMasker = value;
        }
    }

    /*
    /// <summary>
    /// Gets the array of custom attributes to be passed into the logevent context.
    /// </summary>
    [ArrayParameter(typeof(TargetPropertyWithContext), "contextproperty")]
    public IList<TargetPropertyWithContext> ContextProperties { get; } = new List<TargetPropertyWithContext>();

    /// <summary>
    /// Gets the logging controller we will be using.
    /// </summary>
    internal TelemetryClient TelemetryClient
    {
        get { return this.telemetryClient; }
    }
    */

    internal static void BuildPropertyBag(LogEventInfo logEvent, ITelemetry trace)
    {
        trace.Timestamp = logEvent.TimeStamp;
        trace.Sequence = logEvent.SequenceID.ToString(CultureInfo.InvariantCulture);

        IDictionary<string, string> propertyBag;

        if (trace is ExceptionTelemetry et)
            propertyBag = et.Properties;
        else if (trace is TraceTelemetry tt)
            propertyBag = tt.Properties;
        else
            throw new NotSupportedException($"{trace.GetType().FullName}");

        if (!string.IsNullOrEmpty(logEvent.LoggerName))
        {
            propertyBag.Add("LoggerName", logEvent.LoggerName);
        }

        /*
        if (logEvent.UserStackFrame != null)
        {
            propertyBag.Add("UserStackFrame", logEvent.UserStackFrame.ToString());
            propertyBag.Add("UserStackFrameNumber", logEvent.UserStackFrameNumber.ToString(CultureInfo.InvariantCulture));
        }
        */

        /*
        for (int i = 0; i < this.ContextProperties.Count; ++i)
        {
            var contextProperty = this.ContextProperties[i];
            if (!string.IsNullOrEmpty(contextProperty.Name) && contextProperty.Layout != null)
            {
                string propertyValue = this.RenderLogEvent(contextProperty.Layout, logEvent);
                PopulatePropertyBag(propertyBag, contextProperty.Name, propertyValue);
            }
        }
        */

        LoadLogEventProperties(logEvent, propertyBag);
    }

    /// <summary>
    /// Initializes the Target and perform instrumentationKey validation.
    /// </summary>
    protected override void InitializeTarget()
    {
        base.InitializeTarget();

        var telemetryConfig = TelemetryConfiguration.CreateDefault();

        var settings = new AppInsightsSettings();
        SettingsProcessor.LoadConnectionString(settings, this);
        SettingsProcessor.LoadCloudRoleName(settings, this);
        SettingsProcessor.LoadCloudRoleInstance(settings, this);

        if (string.IsNullOrWhiteSpace(settings.ConnectionString))
        {
            Debug.WriteLine("Disabling Application Insights due to missing connection string environment var 'APPLICATIONINSIGHTS_CONNECTION_STRING'");
            this.telemetryClient = new TelemetryClient(new TelemetryConfiguration());
            return;
        }

        // (obsolete) if still using a legacy instrumentation key (which is a guid), set it here
        if (Guid.TryParse(settings.ConnectionString, out Guid _))
        {
            Debug.WriteLine("Using an InstrumentationKey with Application Insights is deprecated, please use a connection string");
#pragma warning disable CS0618 // Type or member is obsolete
            telemetryConfig.InstrumentationKey = settings.ConnectionString;
#pragma warning restore CS0618 // Type or member is obsolete
        }
        else
        {
            try
            {
                telemetryConfig.ConnectionString = settings.ConnectionString;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Disabling Application Insights due to invalid connection string: " + e.Message);
            }
        }

        telemetryConfig.TelemetryInitializers.Add(new CustomFieldsTelemetryInitializer(settings.CloudRoleName, settings.CloudRoleInstance));

        this.telemetryClient = new TelemetryClient(telemetryConfig);
    }

    /// <summary>
    /// Send the log message to Application Insights.
    /// </summary>
    /// <exception cref="ArgumentNullException">If <paramref name="logEvent"/> is null.</exception>
    protected override void Write(LogEventInfo logEvent)
    {
        ArgumentNullException.ThrowIfNull(logEvent);

        this.lastLogEventTime = DateTime.UtcNow;

        if (logEvent.Exception != null)
        {
            this.SendException(logEvent);
        }
        else
        {
            this.SendTrace(logEvent);
        }
    }

    /// <summary>
    /// Flush any pending log messages.
    /// </summary>
    /// <param name="asyncContinuation">The asynchronous continuation.</param>
    protected override void FlushAsync(AsyncContinuation asyncContinuation)
    {
        ArgumentNullException.ThrowIfNull(asyncContinuation);

        try
        {
            this.telemetryClient?.Flush();
            if (DateTime.UtcNow.AddSeconds(-30) > this.lastLogEventTime)
            {
                // Nothing has been written, so nothing to wait for
                asyncContinuation(null);
            }
            else
            {
                // Documentation says it is important to wait after flush, else nothing will happen
                // https://docs.microsoft.com/azure/application-insights/app-insights-api-custom-events-metrics#flushing-data
                Task.Delay(TimeSpan.FromMilliseconds(500)).ContinueWith((task) => asyncContinuation(null));
            }
        }
        catch (Exception ex)
        {
            asyncContinuation(ex);
        }
    }

    private static void LoadLogEventProperties(LogEventInfo logEvent, IDictionary<string, string> propertyBag)
    {
        // GlobalDiagnosticsContext are global scope items, cannot set via ms ILogger
        foreach (var p in GlobalDiagnosticsContext.GetNames())
            PopulatePropertyBag(propertyBag, p, GlobalDiagnosticsContext.GetObject(p));

        // MappedDiagnosticsLogicalContext comes from .BeginScope()
        // these override global variables
        foreach (var p in ScopeContext.GetAllProperties())
            PopulatePropertyBag(propertyBag, p.Key, p.Value);

        // Properties comes from embedded values  "{Value}", 83
        // these override scope variables
        if (logEvent.HasProperties)
        {
            foreach (var kv in logEvent.Properties)
            {
                if (kv.Key is string k)
                    PopulatePropertyBag(propertyBag, k, kv.Value);
            }
        }

        PopulatePropertyBag(propertyBag, "AssemblyVersion", _assembly);
        PopulatePropertyBag(propertyBag, "Product", _product);
        PopulatePropertyBag(propertyBag, "Component", _component);
        PopulatePropertyBag(propertyBag, "Environment", _environment);

        PopulatePropertyBag(propertyBag, "RequestTraceId", _reqTraceId.Render(logEvent));
        PopulatePropertyBag(propertyBag, "RequestTraceChain", _reqChain.Render(logEvent));
        PopulatePropertyBag(propertyBag, "ThreadId", _threadId.Render(logEvent));
        PopulatePropertyBag(propertyBag, "ContextId", _asyncCtx.Render(logEvent));

        // properties persisted from the call chain
        var rProps = RequestTraceProperties.GetRequestTraceProperties();
        if (!rProps.IsNullOrEmpty())
        {
            foreach (var prop in rProps!)
            {
                if (!propertyBag.ContainsKey(prop.Key))
                    PopulatePropertyBag(propertyBag, prop.Key, prop.Value);
            }
        }

        if (logEvent.Message != logEvent.FormattedMessage)
            PopulatePropertyBag(propertyBag, "MessageTemplate", logEvent.Message);

        propertyBag.Remove(LoggingConstants.Field_ContextId);
        propertyBag.Remove(LoggingConstants.Field_RequestTraceChain);
        propertyBag.Remove(LoggingConstants.Field_RequestTraceId);
    }

    private static void PopulatePropertyBag(IDictionary<string, string> propertyBag, string key, object valueObj)
    {
        if (valueObj == null)
        {
            return;
        }

        string value = Convert.ToString(valueObj, CultureInfo.InvariantCulture) ?? string.Empty;
        if (propertyBag.TryGetValue(key, out string? bagValue))
        {
            if (string.Equals(value, bagValue, StringComparison.Ordinal))
            {
                return;
            }

            key += "_1";
        }

        propertyBag.Add(key, value);
    }

    private static SeverityLevel? GetSeverityLevel(LogLevel logEventLevel)
    {
        if (logEventLevel == null)
        {
            return null;
        }

        if (logEventLevel.Ordinal == LogLevel.Trace.Ordinal ||
            logEventLevel.Ordinal == LogLevel.Debug.Ordinal)
        {
            return SeverityLevel.Verbose;
        }

        if (logEventLevel.Ordinal == LogLevel.Info.Ordinal)
        {
            return SeverityLevel.Information;
        }

        if (logEventLevel.Ordinal == LogLevel.Warn.Ordinal)
        {
            return SeverityLevel.Warning;
        }

        if (logEventLevel.Ordinal == LogLevel.Error.Ordinal)
        {
            return SeverityLevel.Error;
        }

        if (logEventLevel.Ordinal == LogLevel.Fatal.Ordinal)
        {
            return SeverityLevel.Critical;
        }

        // The only possible value left if OFF but we should never get here in this case
        return null;
    }

    private void SendException(LogEventInfo logEvent)
    {
        var exceptionTelemetry = new ExceptionTelemetry(logEvent.Exception)
        {
            SeverityLevel = GetSeverityLevel(logEvent.Level),
            Message = $"{logEvent.Exception.GetType()}: {logEvent.Exception.Message}",
        };

        string logMessage = _msgRender.Render(logEvent);
        if (!string.IsNullOrEmpty(logMessage))
        {
            exceptionTelemetry.Properties.Add("Message", logMessage);
        }

        BuildPropertyBag(logEvent, exceptionTelemetry);
        this.telemetryClient?.Track(exceptionTelemetry);
    }

    private void SendTrace(LogEventInfo logEvent)
    {
        string logMessage = _msgRender.Render(logEvent);
        var trace = new TraceTelemetry(logMessage)
        {
            SeverityLevel = GetSeverityLevel(logEvent.Level),
        };

        BuildPropertyBag(logEvent, trace);
        this.telemetryClient?.Track(trace);
    }
}
