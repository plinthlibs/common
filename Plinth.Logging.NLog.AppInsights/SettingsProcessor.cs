using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plinth.Logging.NLog.AppInsights;

internal static class SettingsProcessor
{
    public static void LoadConnectionString(AppInsightsSettings s, AppInsightsTarget? nlogTarget)
    {
        // 1st: passed in parameter
        if (!string.IsNullOrWhiteSpace(s.ConnectionString))
            return;

        // 2nd: nlog
        if (!string.IsNullOrWhiteSpace(nlogTarget?.ConnectionString))
        {
            s.ConnectionString = nlogTarget.ConnectionString;
            return;
        }

        // 3rd: ENV
        var connStrEnv = Environment.GetEnvironmentVariable("APPLICATIONINSIGHTS_CONNECTION_STRING");
        if (!string.IsNullOrWhiteSpace(connStrEnv))
        {
            s.ConnectionString = connStrEnv;
            return;
        }

        // (Obsolete) 4th: instrumentation key from nlog
#pragma warning disable CS0618 // Type or member is obsolete
        if (!string.IsNullOrWhiteSpace(nlogTarget?.InstrumentationKey))
        {
            s.ConnectionString = nlogTarget.InstrumentationKey;
            return;
        }
#pragma warning restore CS0618 // Type or member is obsolete

        // (Obsolete) 5th: instrumentation key from env
        var iKeyEnv = Environment.GetEnvironmentVariable("APPINSIGHTS_INSTRUMENTATIONKEY");
        if (!string.IsNullOrWhiteSpace(iKeyEnv))
        {
            s.ConnectionString = iKeyEnv;
            return;
        }
    }

    public static void LoadCloudRoleName(AppInsightsSettings s, AppInsightsTarget? nlogTarget)
    {
        // 1st: passed in parameter
        if (!string.IsNullOrWhiteSpace(s.CloudRoleName))
            return;

        // 2nd: nlog
        if (!string.IsNullOrWhiteSpace(nlogTarget?.CloudRoleName))
        {
            s.CloudRoleName = nlogTarget?.CloudRoleName;
            return;
        }

        // 3rd: ENV
        var env = Environment.GetEnvironmentVariable("APPLICATIONINSIGHTS_CLOUD_ROLENAME");
        if (!string.IsNullOrWhiteSpace(env))
        {
            s.CloudRoleName = env;
            return;
        }
    }

    public static void LoadCloudRoleInstance(AppInsightsSettings s, AppInsightsTarget? nlogTarget)
    {
        // 1st: passed in parameter
        if (!string.IsNullOrWhiteSpace(s.CloudRoleInstance))
            return;

        // 2nd: nlog
        if (!string.IsNullOrWhiteSpace(nlogTarget?.CloudRoleInstance))
        {
            s.CloudRoleInstance = nlogTarget?.CloudRoleInstance;
            return;
        }

        // 3rd: ENV
        var env = Environment.GetEnvironmentVariable("APPLICATIONINSIGHTS_CLOUD_ROLEINSTANCE");
        if (!string.IsNullOrWhiteSpace(env))
        {
            s.CloudRoleInstance = env;
            return;
        }
    }
}
