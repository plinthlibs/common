using Microsoft.ApplicationInsights.Channel;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.ApplicationInsights.Extensibility;
using Plinth.Common.Logging;
using System.Diagnostics;

namespace Plinth.Logging.NLog.AppInsights;

internal class CustomFieldsTelemetryInitializer(string? roleName, string? roleInstance) : ITelemetryInitializer
{
    public void Initialize(ITelemetry telemetry)
    {
        if (roleName is not null)
            telemetry.Context.Cloud.RoleName = roleName;
        if (roleInstance is not null)
            telemetry.Context.Cloud.RoleInstance = roleInstance;

        // apply any activity baggage to non trace telemetry so the traceid is everywhere.
        // these fields will be applied to traces via nlog
        if (telemetry is not TraceTelemetry && telemetry is ISupportProperties isp)
        {
            foreach (var kv in Activity.Current?.Baggage ?? [])
            {
                isp.Properties.TryAdd(kv.Key, kv.Value);
            }
        }
    }
}
