
# README

### Plinth.Logging.NLog.AppInsights

**Integration allowing Plinth and NLog to record telemetry and logs to Azure Application Insights**

## 1. Add this to your NLog.config under the `<nlog>` tag
```xml
  <extensions>
    <add assembly="Plinth.Logging.NLog.AppInsights" prefix="plinthAI" />
  </extensions>
```

## 2. Then create the target

```xml
  <targets>
    <target xsi:type="plinthAI.AppInsightsTarget" name="aiTarget">
      <connectionString>{from-Azure-Portal}</connectionString>
      <cloudRoleName>testRole1</cloudRoleName>
      <cloudRoleInstance>serverA</cloudRoleInstance>
      <maxLength>25000</maxLength>
    </target>
  </targets>
```
- :point_right: **Do not** wrap this in an async wrapper.  It will break the context and metadata.
- `cloudRoleName` and `cloudRoleInstance` are optional, and will have reasonable defaults if not specified 
- Cloud Role Name is typically the application name
- Cloud Role Instance is typically the machine name
- The `maxLength` field is optional, and defaults to 25000 characters
- :point_right: Using a connection string is preferred.  `<instrumentationKey>` is supported but deprecated and will stop working in March 2025

## 3.  Reference the target in a logger rule

```xml
  <rules>
    <logger name="*" minlevel="Debug" writeTo="aiTarget" />
  </rules>
```

## 4.  Initialize telemetry in Startup

```c#
  services.AddPlinthAppInsightsTelemetry(cfg => 
  {
      cfg.CloudRoleName = "MyApp";
      cfg.DisabledPrefixes.Add("/health");
  });
```

:point_right: The lambda is optional and only necessary if setting hardcoded configuration

Configurations Options are
* `ConnectionString`: If not setting via environment variable or NLog.config
* `DisabledPrefixes`: A blacklist of url prefixes to disable Request Telemetry.  Example: `"/favicon.ico"`
* `EnabledPrefixes`: A whitelist of url prefixes to enable Request Telemetry.  This is _null_ by default.  If a list of prefixes is specified, only requests which match will have request telemetry.  If there is a conflict between this and `DisabledPrefixes`, the preference is to disable telemetry.
* `CaptureSqlCommandText`: Set to true to have SQL Dependency Telemetry capture the SQL command text sent to the database.  Defaults to _false_.
* `CloudRoleName`: See next [section](#application-insights-configuration-settings)
* `CloudRoleInstance`: See next [section](#application-insights-configuration-settings)

## 5. Application Insights Configuration Settings
Values are applied in this order of precedence

* Connection String (get this from the Azure Portal)
	1. Hardcoded parameter passed in Startup.cs _(does not apply to NLog logs)_
	2. NLog.config `<connectionString>`
	3. Environment variable: `APPLICATIONINSIGHTS_CONNECTION_STRING`
	4. _(Obsolete)_: NLog.config `<instrumentationKey>`
	5. _(Obsolete)_: Environment variable `APPINSIGHTS_INSTRUMENTATIONKEY`

* Cloud Role Name
	1. Hardcoded parameter passed in Startup.cs _(does not apply to NLog logs)_
	2. NLog.config `<cloudRoleName>`
	3. Environment variable: `APPLICATIONINSIGHTS_CLOUD_ROLENAME`

* Cloud Role Instance
	1. Hardcoded parameter passed in Startup.cs _(does not apply to NLog logs)_
	2. NLog.config `<cloudRoleInstance>`
	3. Environment variable: `APPLICATIONINSIGHTS_CLOUD_ROLEINSTANCE`
