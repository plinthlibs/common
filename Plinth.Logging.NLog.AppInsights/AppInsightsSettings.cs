using Microsoft.ApplicationInsights.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plinth.Logging.NLog.AppInsights;

/// <summary>
/// Settings class for Plinth Application Insights
/// </summary>
public class AppInsightsSettings
{
    /// <summary>
    /// Connection String for the Application Insights instance from the Azure Portal
    /// </summary>
    public string? ConnectionString { get; set; }

    /// <summary>
    /// (Optional) list of URI prefixes for which to disable Request Telemetry (blacklist)
    /// </summary>
    /// <remarks>
    /// Defaults to ["/swagger", "/redoc", "/favicon.ico", "/_vs", "/_framework"]
    /// </remarks>
    public List<string> DisabledPrefixes { get; set; } = ["/swagger", "/redoc", "/favicon.ico", "/_vs", "/_framework"];

    /// <summary>
    /// (Optional) If filled out, only these URI prefixes will send Request Telemetry (whitelist)
    /// </summary>
    /// <remarks>
    /// This is overridden by DisabledPrefixes, so that if there is a conflict, the preference is to disable.
    /// This only filters requests with a URI.
    /// </remarks>
    public List<string>? EnabledPrefixes { get; set; }

    /// <summary>
    /// (Optional) custom filter for allowing/disallowing request telemetry.  Overrides Disabled/Enabled Prefixes
    /// </summary>
    /// <remarks>Return true to allow, false to disallow</remarks>
    public Func<RequestTelemetry, bool>? CustomFilter { get; set; }

    /// <summary>
    /// If enabled, dependency telemetry for SQL commands will include the command text
    /// </summary>
    public bool CaptureSqlCommandText { get; set; }

    /// <summary>
    /// Typically the application name, used for filtering in Azure Portal
    /// </summary>
    public string? CloudRoleName { get; set; }

    /// <summary>
    /// Typically the server or host name, used for filtering in Azure Portal
    /// </summary>
    public string? CloudRoleInstance { get; set; }
}
