using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.ApplicationInsights.Channel;
using Microsoft.ApplicationInsights.DependencyCollector;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.ApplicationInsights.Extensibility.Implementation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog;
using Plinth.Common.Logging;

namespace Plinth.Logging.NLog.AppInsights;

/// <summary>
/// Extensions for adding app insights telemetry
/// </summary>
public static class ServicesCollectionExt
{
    private static readonly Microsoft.Extensions.Logging.ILogger log = StaticLogManager.GetLogger();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configure"></param>
    /// <param name="sensitiveDataMasker"></param>
    /// <returns></returns>
    public static IServiceCollection AddPlinthAppInsightsTelemetry(this IServiceCollection services, Action<AppInsightsSettings>? configure = null, ISensitiveDataMasker? sensitiveDataMasker = null)
    {
        var settings = new AppInsightsSettings();
        configure?.Invoke(settings);

        var nlogTarget = LogManager.Configuration.ConfiguredNamedTargets
            .OfType<AppInsightsTarget>()
            .FirstOrDefault();

        if (nlogTarget != null
            && sensitiveDataMasker != null)
        {
            nlogTarget.SensitiveDataMasker = sensitiveDataMasker;
        }

        SettingsProcessor.LoadConnectionString(settings, nlogTarget);
        SettingsProcessor.LoadCloudRoleName(settings, nlogTarget);
        SettingsProcessor.LoadCloudRoleInstance(settings, nlogTarget);

        return AddAI(services, settings);
    }

    private static IServiceCollection AddAI(this IServiceCollection services, AppInsightsSettings s)
    {
        // this adds telemetry like timing and dependencies
        var aiOptions = new ApplicationInsightsServiceOptions
        {
            EnableAdaptiveSampling = false
        };

        if (string.IsNullOrWhiteSpace(s.ConnectionString))
        {
            log.Warn("Disabling Application Insights due to missing connection string environment var 'APPLICATIONINSIGHTS_CONNECTION_STRING'");
            return services;
        }

        // (obsolete) if still using a legacy instrumentation key (which is a guid), set it here
        if (Guid.TryParse(s.ConnectionString, out Guid _))
        {
            log.Warn("Using an InstrumentationKey with Application Insights is deprecated, please use a connection string");
#pragma warning disable CS0618 // Type or member is obsolete
            aiOptions.InstrumentationKey = s.ConnectionString;
#pragma warning restore CS0618 // Type or member is obsolete
        }
        else
        {
            aiOptions.ConnectionString = s.ConnectionString;
        }

        services.AddApplicationInsightsTelemetry(aiOptions);

        // turn on capture of sql text
        if (s.CaptureSqlCommandText)
        {
            services.ConfigureTelemetryModule<DependencyTrackingTelemetryModule>(
                (module, o) => { module.EnableSqlCommandTextInstrumentation = true; });
        }

        services.AddSingleton(s);
        services.AddApplicationInsightsTelemetryProcessor<RequestsTelemetryFilter>();
        services.AddSingleton<ITelemetryInitializer>(new CustomFieldsTelemetryInitializer(s.CloudRoleName, s.CloudRoleInstance));

        // if azure service bus client library is present, filter out a useless service bus telemetry item
        // that logs a dependency event every minute
        if (Type.GetType("Azure.Messaging.ServiceBus.ServiceBusClient, Azure.Messaging.ServiceBus") != null)
        {
            log.Trace("ServiceBus detected, activating telemetry filter");
            services.AddApplicationInsightsTelemetryProcessor<ServiceBusTelemetryFilter>();
        }

        return services;
    }

    private class ServiceBusTelemetryFilter(ITelemetryProcessor next) : ITelemetryProcessor
    {
        public void Process(ITelemetry item)
        {
            if (item is OperationTelemetry operationTelemetry
                && operationTelemetry.Name == "ServiceBusReceiver.Receive")
            {
                return;
            }

            next.Process(item);
        }
    }
}
