# README

### Plinth.HttpApiClient.Polly

**Extensions for using Polly with Plinth.HttpApiClient**

Example usage:
```c#
using Plinth.HttpApiClient;
using Plinth.HttpApiClient.Polly;
using Polly;
using Polly.Extensions.Http;
using Polly.Timeout;


    IAsyncPolicy<HttpResponseMessage> policy =
        HttpPolicyExtensions.HandleTransientHttpError()
            .Or<TimeoutRejectedException>()
            .FallbackAsync(new HttpResponseMessage { StatusCode = HttpStatusCode.NotImplemented })
            .CreateTimeoutPolicy(TimeSpan.FromMilliseconds(25));

    services.AddHttpApiClient<MyApiClient, MyApiClientSettings>(configuration, policy);

```
