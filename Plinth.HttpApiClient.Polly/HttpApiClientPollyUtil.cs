using Polly;
using Polly.Timeout;
using System.Net;

namespace Plinth.HttpApiClient.Polly;

/// <summary>
/// Utilities for setting up Polly Policies for Http Api Clients
/// </summary>
public static class HttpApiClientPollyUtil
{
    /// <summary>
    /// Utility for constructing a policy correctly which applies per request timeouts (vs per total operation)
    /// </summary>
    /// <remarks>Use with <see cref="HandleRequestTimeout"/></remarks>
    /// <example>
    ///   <code>HttpApiClientPollyUtil.HandleRequestTimeout().Or(...).RetryAsync(10).CreateTimeoutPolicy(TimeSpan.FromSeconds(5))</code>
    /// </example>
    /// <param name="policy">master policy which might apply retries</param>
    /// <param name="timeout">timeout for individual requests</param>
    /// <returns>A policy which will trigger a timeout at the given time span</returns>
    public static IAsyncPolicy<HttpResponseMessage> CreateTimeoutPolicy(this IAsyncPolicy<HttpResponseMessage> policy, TimeSpan timeout)
        => policy.WrapAsync(Policy.TimeoutAsync(timeout));

    /// <summary>
    /// Start a policy builder with handling a Polly Timeout
    /// </summary>
    /// <remarks>Use with <see cref="CreateTimeoutPolicy"/></remarks>
    /// <example>
    ///   <code>HttpApiClientPollyUtil.HandleRequestTimeout().Or(...).RetryAsync(10).CreateTimeoutPolicy(TimeSpan.FromSeconds(5))</code>
    /// </example>
    /// <returns>A PolicyBuilder ready for more settings</returns>
    public static PolicyBuilder<HttpResponseMessage> HandleRequestTimeout()
        => Policy<HttpResponseMessage>.Handle<TimeoutRejectedException>();

    /* the below was copied out of Polly.Extensions.Http which was deprecated in Nov 2023 */

    private static readonly Func<HttpResponseMessage, bool> TransientHttpStatusCodePredicate
        = (HttpResponseMessage response) => response.StatusCode >= HttpStatusCode.InternalServerError || response.StatusCode == HttpStatusCode.RequestTimeout;

    /// <summary>
    /// Builds a Polly.PolicyBuilder`1 to configure a Polly.Policy`1 which will handle System.Net.Http.HttpClient requests that fail with conditions indicating a transient failure.
    /// </summary>
    /// <remarks>
    /// * Network failures (as System.Net.Http.HttpRequestException)
    /// * HTTP 5XX status codes (server errors)
    /// * HTTP 408 status code (request timeout)
    /// </remarks>
    public static PolicyBuilder<HttpResponseMessage> HandleTransientHttpError()
    {
        return OrTransientHttpStatusCode(Policy<HttpResponseMessage>.Handle<HttpRequestException>());
    }

    /// <summary>
    /// Configures the Polly.PolicyBuilder`1 to handle System.Net.Http.HttpClient requests that fail with conditions indicating a transient failure.
    /// </summary>
    /// <remarks>
    /// * Network failures (as System.Net.Http.HttpRequestException)
    /// * HTTP 5XX status codes (server errors)
    /// * HTTP 408 status code (request timeout)
    /// </remarks>
    public static PolicyBuilder<HttpResponseMessage> OrTransientHttpStatusCode(this PolicyBuilder<HttpResponseMessage> policyBuilder)
    {
        ArgumentNullException.ThrowIfNull(policyBuilder);

        return policyBuilder.OrResult(TransientHttpStatusCodePredicate);
    }
}
