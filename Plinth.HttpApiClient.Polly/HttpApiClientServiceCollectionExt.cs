﻿using Microsoft.Extensions.Configuration;
using Plinth.HttpApiClient;
using Polly;

namespace Microsoft.Extensions.DependencyInjection; // putting this here makes it easier to find

/// <summary>
/// Extension methods for registering HttpApiClients
/// </summary>
public static class HttpApiClientServiceCollectionExt
{
    #region Add with Config Section
    /// <summary>
    /// (Polly) Register an HTTP API Client for DI using an IConfiguration section for settings
    /// </summary>
    /// <typeparam name="Tinterface">An interface containing all the client methods, for injecting into calling code</typeparam>
    /// <typeparam name="Tclass">A concrete implementation of <typeparamref name="Tinterface"/> which derives from <see cref="BaseHttpApiClient"/></typeparam>
    /// <typeparam name="Tsettings">A <see cref="BaseHttpApiClientSettings"/> or derived</typeparam>
    /// <param name="services"></param>
    /// <param name="section">IConfiguration section to get settings from</param>
    /// <param name="policy">Polly Policy for handling errors</param>
    /// <param name="configureClient">(optional) Action to configure the httpclient settings</param>
    /// <returns>IHttpClientBuilder for more customization</returns>
    public static IHttpClientBuilder AddHttpApiClient<Tinterface, Tclass, Tsettings>(this IServiceCollection services, IConfigurationSection section, IAsyncPolicy<HttpResponseMessage> policy, Action<HttpClient>? configureClient = null)
        where Tinterface : class
        where Tclass : BaseHttpApiClient, Tinterface
        where Tsettings : BaseHttpApiClientSettings
    {
        return services.AddHttpApiClient<Tinterface, Tclass, Tsettings>(section, configureClient).AddPolicyHandler(policy);
    }

    /// <inheritdoc cref="AddHttpApiClient{Tinterface, Tclass, Tsettings}(IServiceCollection, IConfigurationSection, IAsyncPolicy{HttpResponseMessage}, Action{HttpClient})"/>
    public static IHttpClientBuilder AddHttpApiClient<Tinterface, Tclass, Tsettings>(this IServiceCollection services, IConfigurationSection section, IAsyncPolicy<HttpResponseMessage> policy, Action<IServiceProvider, HttpClient> configureClient)
        where Tinterface : class
        where Tclass : BaseHttpApiClient, Tinterface
        where Tsettings : BaseHttpApiClientSettings
    {
        return services.AddHttpApiClient<Tinterface, Tclass, Tsettings>(section, configureClient).AddPolicyHandler(policy);
    }

    /// <summary>
    /// (Polly) Register an HTTP API Client for DI using an IConfiguration section for settings
    /// </summary>
    /// <typeparam name="Tclass">A class which derives from <see cref="BaseHttpApiClient"/></typeparam>
    /// <typeparam name="Tsettings">A <see cref="BaseHttpApiClientSettings"/> or derived</typeparam>
    /// <param name="services"></param>
    /// <param name="section">IConfiguration section to get settings from</param>
    /// <param name="policy">Polly Policy for handling errors</param>
    /// <param name="configureClient">(optional) Action to configure the httpclient settings</param>
    /// <returns>IHttpClientBuilder for more customization</returns>
    public static IHttpClientBuilder AddHttpApiClient<Tclass, Tsettings>(this IServiceCollection services, IConfigurationSection section, IAsyncPolicy<HttpResponseMessage> policy, Action<HttpClient>? configureClient = null)
        where Tclass : BaseHttpApiClient
        where Tsettings : BaseHttpApiClientSettings
    {
        return services.AddHttpApiClient<Tclass, Tsettings>(section, configureClient).AddPolicyHandler(policy);
    }

    /// <inheritdoc cref="AddHttpApiClient{Tclass, Tsettings}(IServiceCollection, IConfigurationSection, IAsyncPolicy{HttpResponseMessage}, Action{HttpClient})"/>
    public static IHttpClientBuilder AddHttpApiClient<Tclass, Tsettings>(this IServiceCollection services, IConfigurationSection section, IAsyncPolicy<HttpResponseMessage> policy, Action<IServiceProvider, HttpClient> configureClient)
        where Tclass : BaseHttpApiClient
        where Tsettings : BaseHttpApiClientSettings
    {
        return services.AddHttpApiClient<Tclass, Tsettings>(section, configureClient).AddPolicyHandler(policy);
    }
    #endregion

    #region Add with IConfiguration
    /// <summary>
    /// (Polly) Register an HTTP API Client for DI using an IConfiguration section for settings
    /// </summary>
    /// <typeparam name="Tinterface">An interface containing all the client methods, for injecting into calling code</typeparam>
    /// <typeparam name="Tclass">A concrete implementation of <typeparamref name="Tinterface"/> which derives from <see cref="BaseHttpApiClient"/></typeparam>
    /// <typeparam name="Tsettings">A <see cref="BaseHttpApiClientSettings"/> or derived</typeparam>
    /// <param name="services"></param>
    /// <param name="configuration">An IConfiguration instance, will load settings from a section named the same as <typeparamref name="Tclass"/></param>
    /// <param name="policy">Polly Policy for handling errors</param>
    /// <param name="configureClient">(optional) Action to configure the httpclient settings</param>
    /// <returns>IHttpClientBuilder for more customization</returns>
    public static IHttpClientBuilder AddHttpApiClient<Tinterface, Tclass, Tsettings>(this IServiceCollection services, IConfiguration configuration, IAsyncPolicy<HttpResponseMessage> policy, Action<HttpClient>? configureClient = null)
        where Tinterface : class
        where Tclass : BaseHttpApiClient, Tinterface
        where Tsettings : BaseHttpApiClientSettings
    {
        return services.AddHttpApiClient<Tinterface, Tclass, Tsettings>(configuration, configureClient).AddPolicyHandler(policy);
    }

    /// <inheritdoc cref="AddHttpApiClient{Tinterface, Tclass, Tsettings}(IServiceCollection, IConfiguration, IAsyncPolicy{HttpResponseMessage}, Action{HttpClient})"/>
    public static IHttpClientBuilder AddHttpApiClient<Tinterface, Tclass, Tsettings>(this IServiceCollection services, IConfiguration configuration, IAsyncPolicy<HttpResponseMessage> policy, Action<IServiceProvider, HttpClient> configureClient)
        where Tinterface : class
        where Tclass : BaseHttpApiClient, Tinterface
        where Tsettings : BaseHttpApiClientSettings
    {
        return services.AddHttpApiClient<Tinterface, Tclass, Tsettings>(configuration, configureClient).AddPolicyHandler(policy);
    }

    /// <summary>
    /// (Polly) Register an HTTP API Client for DI using an IConfiguration section for settings
    /// </summary>
    /// <typeparam name="Tclass">A class which derives from <see cref="BaseHttpApiClient"/></typeparam>
    /// <typeparam name="Tsettings">A <see cref="BaseHttpApiClientSettings"/> or derived</typeparam>
    /// <param name="services"></param>
    /// <param name="configuration">An IConfiguration instance, will load settings from a section named the same as <typeparamref name="Tclass"/></param>
    /// <param name="policy">Polly Policy for handling errors</param>
    /// <param name="configureClient">(optional) Action to configure the httpclient settings</param>
    /// <returns>IHttpClientBuilder for more customization</returns>
    public static IHttpClientBuilder AddHttpApiClient<Tclass, Tsettings>(this IServiceCollection services, IConfiguration configuration, IAsyncPolicy<HttpResponseMessage> policy, Action<HttpClient>? configureClient = null)
        where Tclass : BaseHttpApiClient
        where Tsettings : BaseHttpApiClientSettings
    {
        return services.AddHttpApiClient<Tclass, Tsettings>(configuration, configureClient).AddPolicyHandler(policy);
    }

    /// <inheritdoc cref="AddHttpApiClient{Tclass, Tsettings}(IServiceCollection, IConfiguration, IAsyncPolicy{HttpResponseMessage}, Action{HttpClient})"/>
    public static IHttpClientBuilder AddHttpApiClient<Tclass, Tsettings>(this IServiceCollection services, IConfiguration configuration, IAsyncPolicy<HttpResponseMessage> policy, Action<IServiceProvider, HttpClient> configureClient)
        where Tclass : BaseHttpApiClient
        where Tsettings : BaseHttpApiClientSettings
    {
        return services.AddHttpApiClient<Tclass, Tsettings>(configuration, configureClient).AddPolicyHandler(policy);
    }
    #endregion

    #region Add with settings action
    /// <summary>
    /// (Polly) Register an HTTP API Client for DI using an action to configure settings
    /// </summary>
    /// <typeparam name="Tinterface">An interface containing all the client methods, for injecting into calling code</typeparam>
    /// <typeparam name="Tclass">A concrete implementation of <typeparamref name="Tinterface"/> which derives from <see cref="BaseHttpApiClient"/></typeparam>
    /// <typeparam name="Tsettings">A <see cref="BaseHttpApiClientSettings"/> or derived</typeparam>
    /// <param name="services"></param>
    /// <param name="configure">an Action that receives an unconfigured <typeparamref name="Tsettings"/> instance</param>
    /// <param name="policy">Polly Policy for handling errors</param>
    /// <param name="configureClient">(optional) Action to configure the httpclient settings</param>
    /// <returns>IHttpClientBuilder for more customization</returns>
    public static IHttpClientBuilder AddHttpApiClient<Tinterface, Tclass, Tsettings>(this IServiceCollection services, Action<Tsettings> configure, IAsyncPolicy<HttpResponseMessage> policy, Action<HttpClient>? configureClient = null)
        where Tinterface : class
        where Tclass : BaseHttpApiClient, Tinterface
        where Tsettings : BaseHttpApiClientSettings
    {
        return services.AddHttpApiClient<Tinterface, Tclass, Tsettings>(configure, configureClient).AddPolicyHandler(policy);
    }

    /// <inheritdoc cref="AddHttpApiClient{Tinterface, Tclass, Tsettings}(IServiceCollection, Action{Tsettings}, IAsyncPolicy{HttpResponseMessage}, Action{HttpClient})"/>
    public static IHttpClientBuilder AddHttpApiClient<Tinterface, Tclass, Tsettings>(this IServiceCollection services, Action<Tsettings> configure, IAsyncPolicy<HttpResponseMessage> policy, Action<IServiceProvider, HttpClient> configureClient)
        where Tinterface : class
        where Tclass : BaseHttpApiClient, Tinterface
        where Tsettings : BaseHttpApiClientSettings
    {
        return services.AddHttpApiClient<Tinterface, Tclass, Tsettings>(configure, configureClient).AddPolicyHandler(policy);
    }

    /// <summary>
    /// (Polly) Register an HTTP API Client for DI using an action to configure settings
    /// </summary>
    /// <typeparam name="Tclass">A class which derives from <see cref="BaseHttpApiClient"/></typeparam>
    /// <typeparam name="Tsettings">A <see cref="BaseHttpApiClientSettings"/> or derived</typeparam>
    /// <param name="services"></param>
    /// <param name="configure">an Action that receives an unconfigured <typeparamref name="Tsettings"/> instance</param>
    /// <param name="policy">Polly Policy for handling errors</param>
    /// <param name="configureClient">(optional) Action to configure the httpclient settings</param>
    /// <returns>IHttpClientBuilder for more customization</returns>
    public static IHttpClientBuilder AddHttpApiClient<Tclass, Tsettings>(this IServiceCollection services, Action<Tsettings> configure, IAsyncPolicy<HttpResponseMessage> policy, Action<HttpClient>? configureClient = null)
        where Tclass : BaseHttpApiClient
        where Tsettings : BaseHttpApiClientSettings
    {
        return services.AddHttpApiClient<Tclass, Tsettings>(configure, configureClient).AddPolicyHandler(policy);
    }

    /// <inheritdoc cref="AddHttpApiClient{Tclass, Tsettings}(IServiceCollection, Action{Tsettings}, IAsyncPolicy{HttpResponseMessage}, Action{HttpClient})"/>
    public static IHttpClientBuilder AddHttpApiClient<Tclass, Tsettings>(this IServiceCollection services, Action<Tsettings> configure, IAsyncPolicy<HttpResponseMessage> policy, Action<IServiceProvider, HttpClient> configureClient)
        where Tclass : BaseHttpApiClient
        where Tsettings : BaseHttpApiClientSettings
    {
        return services.AddHttpApiClient<Tclass, Tsettings>(configure, configureClient).AddPolicyHandler(policy);
    }
    #endregion

    #region Add without settings
    /// <summary>
    /// (Polly) Register an HTTP API Client for DI using an action to configure settings
    /// </summary>
    /// <typeparam name="Tinterface">An interface containing all the client methods, for injecting into calling code</typeparam>
    /// <typeparam name="Tclass">A concrete implementation of <typeparamref name="Tinterface"/> which derives from <see cref="BaseHttpApiClient"/></typeparam>
    /// <param name="services"></param>
    /// <param name="policy">Polly Policy for handling errors</param>
    /// <param name="configureClient">(optional) Action to configure the httpclient settings</param>
    /// <returns>IHttpClientBuilder for more customization</returns>
    public static IHttpClientBuilder AddHttpApiClient<Tinterface, Tclass>(this IServiceCollection services, IAsyncPolicy<HttpResponseMessage> policy, Action<HttpClient>? configureClient = null)
        where Tinterface : class
        where Tclass : BaseHttpApiClient, Tinterface
    {
        return services.AddHttpApiClient<Tinterface, Tclass>(configureClient).AddPolicyHandler(policy);
    }

    /// <inheritdoc cref="AddHttpApiClient{Tinterface, Tclass}(IServiceCollection, IAsyncPolicy{HttpResponseMessage}, Action{HttpClient})"/>
    public static IHttpClientBuilder AddHttpApiClient<Tinterface, Tclass>(this IServiceCollection services, IAsyncPolicy<HttpResponseMessage> policy, Action<IServiceProvider, HttpClient> configureClient)
        where Tinterface : class
        where Tclass : BaseHttpApiClient, Tinterface
    {
        return services.AddHttpApiClient<Tinterface, Tclass>(configureClient).AddPolicyHandler(policy);
    }

    /// <summary>
    /// (Polly) Register an HTTP API Client for DI using an action to configure settings
    /// </summary>
    /// <typeparam name="Tclass">A class which derives from <see cref="BaseHttpApiClient"/></typeparam>
    /// <param name="services"></param>
    /// <param name="policy">Polly Policy for handling errors</param>
    /// <param name="configureClient">(optional) Action to configure the httpclient settings</param>
    /// <returns>IHttpClientBuilder for more customization</returns>
    public static IHttpClientBuilder AddHttpApiClient<Tclass>(this IServiceCollection services, IAsyncPolicy<HttpResponseMessage> policy, Action<HttpClient>? configureClient = null)
        where Tclass : BaseHttpApiClient
    {
        return services.AddHttpApiClient<Tclass>(configureClient).AddPolicyHandler(policy);
    }

    /// <inheritdoc cref="AddHttpApiClient{Tclass}(IServiceCollection, IAsyncPolicy{HttpResponseMessage}, Action{HttpClient})"/>
    public static IHttpClientBuilder AddHttpApiClient<Tclass>(this IServiceCollection services, IAsyncPolicy<HttpResponseMessage> policy, Action<IServiceProvider, HttpClient> configureClient)
        where Tclass : BaseHttpApiClient
    {
        return services.AddHttpApiClient<Tclass>(configureClient).AddPolicyHandler(policy);
    }
    #endregion
}
