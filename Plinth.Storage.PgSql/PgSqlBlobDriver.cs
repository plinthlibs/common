namespace Plinth.Storage.PgSql;

/// <summary>
/// PostgreSQL Blob Driver constants and utilities
/// </summary>
public static class PgSqlBlobDriver
{
    /// <summary>
    /// Specify this parameter to set the schema to a custom schema
    /// </summary>
    public const string DriverSchemaParam = "schema";
}
