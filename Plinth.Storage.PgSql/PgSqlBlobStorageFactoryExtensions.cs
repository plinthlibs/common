﻿using Plinth.Storage.PgSql;
using Plinth.Database.PgSql;

namespace Plinth.Storage;

/// <summary>
/// Extensions for S3
/// </summary>
public static class PgSqlBlobStorageFactoryExtensions
{
    /// <summary>
    /// Get an IBlobStorage
    /// </summary>
    /// <returns>IBlobStorage</returns>
    public static IStorage Get(this StorageFactory fac, ISqlConnection conn, string callingUser)
        => fac.CreateFromIndexDriver(
            new BlobIndexDAO(conn, callingUser, fac.GetIndexDriverParameter(PgSqlBlobDriver.DriverSchemaParam, "public")));
}
