using Npgsql;
using Plinth.Storage.Models;
using Plinth.Database.PgSql;
using Plinth.Common.Extensions;

namespace Plinth.Storage.PgSql;

/// <summary>
/// Internal DAO for managing blobs
/// When using database storage, the data is stored in the blob record as well
/// </summary>
internal class BlobIndexDAO : IStorageIndexDriver
{
    private readonly ISqlConnection _connection;
    private readonly string _callingUser;
    private readonly string _schema;

    public BlobIndexDAO(ISqlConnection connection, string callingUser, string? schema = "public")
    {
        _connection = connection;
        _callingUser = callingUser;
        _schema = schema ?? "public";
    }

    /// <summary>
    /// Npgsql does not support streaming from BYTEA colums
    /// </summary>
    public bool SupportsStreamRead => false;

    /// <summary>
    /// Npgsql does support streaming into BYTEA colums
    /// </summary>
    public bool SupportsStreamWrite => true;

    /// <summary>
    /// Retrieves the blob with the specified guid
    /// </summary>
    /// <param name="guid"></param>
    /// <param name="loadData">if using database storage, true will also load the data (left as encrypted)</param>
    /// <param name="cancellationToken"></param>
    /// <returns>a blob model object or null if the blob with the specified guid does not exist</returns>
    public async Task<Blob?> GetBlobByGUIDAsync(Guid guid, bool loadData = false, CancellationToken cancellationToken = default)
    {
        var b = await _connection.ExecuteQueryProcOneAsync(
            $"{_schema}.fn_get_blob_by_guid",
            (rs) => Task.FromResult(LoadBlob(rs, loadData)),
            cancellationToken,
            new NpgsqlParameter<Guid>("i_guid", guid)
        );
        return b.Value;
    }

    /// <summary>
    /// Retrieves the blob with the specified guid and a stream to the data (if non null)
    /// </summary>
    /// <param name="guid"></param>
    /// <param name="loadData">if using database storage, true will also load the data (left as encrypted)</param>
    /// <param name="cancellationToken"></param>
    /// <returns>a blob model object or null if the blob with the specified guid does not exist</returns>
    public async Task<(Blob? blob, Stream? stream)> GetBlobStreamByGUIDAsync(Guid guid, bool loadData = false, CancellationToken cancellationToken = default)
    {
        var b = await _connection.ExecuteQueryProcOneAsync(
            $"{_schema}.fn_get_blob_by_guid",
            (rs) =>
            {
                var blob = LoadBlob(rs, false);

                if (loadData && !rs.IsNull("o_data"))
                    throw new NotSupportedException("postgresql does not support reading blob streams from the DB");

                return Task.FromResult((blob, (Stream?)null));
            },
            cancellationToken,
            new NpgsqlParameter<Guid>("i_guid", guid)
        );
        return b.Value;
    }

    private static Blob LoadBlob(IResult rs, bool loadData)
        => new()
        {
            Guid = rs.GetGuid("o_guid"),
            Name = rs.GetString("o_name")!,
            Provider = rs.GetString("o_provider")!,
            MimeType = rs.GetString("o_mime_type")!,
            BlobSize = rs.GetLong("o_orig_size"),
            StoredSize = rs.GetLong("o_stored_size"),
            Index = rs.GetString("o_blob_index"),
            Data = loadData ? rs.GetBytes("o_data") : null,
            Features = 
                (rs.GetBool("o_is_encrypted") ? BlobFeatures.Encrypted : BlobFeatures.None)
              | (rs.GetBool("o_is_compressed") ? BlobFeatures.Compressed : BlobFeatures.None)
        };

    /// <summary>
    /// Async Creates a new blob
    /// </summary>
    /// <param name="provider">provider id</param>
    /// <param name="blob">blob metadata</param>
    /// <param name="blobData">encrypted data for Database Storage, null otherwise</param>
    /// <param name="origSize">size of the original data in bytes</param>
    /// <param name="storedSize">size of the data written to storage</param>
    /// <param name="cancellationToken"></param>
    public async Task CreateBlobAsync(string provider, Blob blob, byte[]? blobData, long origSize, long storedSize, CancellationToken cancellationToken)
    {
        await _connection.ExecuteProcAsync(
            $"{_schema}.fn_insert_blob",
            cancellationToken,
            UpsertParams(provider, blob.Guid!.Value, blob, blobData, origSize, storedSize)
        );
    }

    /// <summary>
    /// Async Creates a new blob from a stream
    /// </summary>
    /// <param name="provider">provider id</param>
    /// <param name="blob">blob metadata</param>
    /// <param name="stream">data for Database Storage, null otherwise</param>
    /// <param name="origSize">size of the original data in bytes</param>
    /// <param name="storedSize">size of the data written to storage</param>
    /// <param name="cancellationToken"></param>
    public async Task CreateBlobStreamAsync(string provider, Blob blob, Stream? stream, long origSize, long storedSize, CancellationToken cancellationToken)
    {
        await _connection.ExecuteProcAsync(
            $"{_schema}.fn_insert_blob",
            cancellationToken,
            UpsertParams(provider, blob.Guid!.Value, blob, stream?.ToBytes((int)storedSize), origSize, storedSize)
        );
    }

    public async Task UpdateBlobAsync(string? provider, Guid guid, Blob blob, byte[]? blobData, bool setDataToNull, long? origSize, long? storedSize, CancellationToken cancellationToken)
    {
        blob.Guid = guid;
        await _connection.ExecuteProcAsync(
            $"{_schema}.fn_update_blob",
            cancellationToken,
            UpsertParams(provider, blob.Guid.Value, blob, blobData, origSize, storedSize)
        );

        if (blobData == null && setDataToNull)
        {
            await _connection.ExecuteProcAsync(
                $"{_schema}.fn_update_blob_data",
                cancellationToken,
                new NpgsqlParameter<Guid>("i_guid", guid),
                new NpgsqlParameter("i_data", blobData),
                new NpgsqlParameter("i_calling_user", _callingUser)
            );
        }
    }

    /// <summary>
    /// Async Updates a blob with stream data
    /// </summary>
    /// <param name="provider">provider id</param>
    /// <param name="guid">blob id</param>
    /// <param name="blob">blob metadata</param>
    /// <param name="stream">data for blob</param>
    /// <param name="origSize">size of the original data in bytes</param>
    /// <param name="storedSize">size of the data written to storage</param>
    /// <param name="cancellationToken"></param>
    public async Task UpdateBlobAsync(string? provider, Guid guid, Blob blob, Stream? stream, long? origSize, long? storedSize, CancellationToken cancellationToken)
    {
        blob.Guid = guid;
        await _connection.ExecuteProcAsync(
            $"{_schema}.fn_update_blob",
            cancellationToken,
            UpsertParams(provider, blob.Guid.Value, blob, stream?.ToBytes((int)(storedSize ?? 0)), origSize, storedSize)
        );
    }

    /// <summary>
    /// Async Updates a blob index sizes, used for streaming
    /// </summary>
    /// <param name="guid">blob id</param>
    /// <param name="origSize">size of the original data in bytes</param>
    /// <param name="storedSize">size of the data written to storage</param>
    /// <param name="cancellationToken"></param>
    public async Task UpdateBlobSizesAsync(Guid guid, long origSize, long storedSize, CancellationToken cancellationToken)
    {
        await _connection.ExecuteProcAsync(
            $"{_schema}.fn_update_blob_sizes",
            cancellationToken,
            new NpgsqlParameter<Guid>("i_guid", guid),
            new NpgsqlParameter<long>("i_orig_size", origSize),
            new NpgsqlParameter<long>("i_stored_size", storedSize),
            new NpgsqlParameter("i_calling_user", _callingUser)
        );
    }

    private NpgsqlParameter?[] UpsertParams(string? provider, Guid guid, Blob blob, byte[]? blobData, long? origSize, long? storedSize)
        =>
        [
            new NpgsqlParameter<Guid>("i_guid", guid),
            new NpgsqlParameter("i_name", blob.Name),
            new NpgsqlParameter("i_provider", provider),
            new NpgsqlParameter("i_mime_type", blob.MimeType),
            origSize == null ? null : new NpgsqlParameter<long>("i_orig_size", origSize.Value),
            storedSize == null ? null : new NpgsqlParameter<long>("i_stored_size", storedSize.Value),
            new NpgsqlParameter("i_calling_user", _callingUser),
            new NpgsqlParameter("i_blob_index", blob.Index),
            new NpgsqlParameter("i_data", blobData),
            new NpgsqlParameter<bool>("i_is_encrypted", blob.Features.HasFlag(BlobFeatures.Encrypted)),
            new NpgsqlParameter<bool>("i_is_compressed", blob.Features.HasFlag(BlobFeatures.Compressed))
        ];

    public async Task DeleteBlobAsync(Guid origGuid, CancellationToken cancellationToken)
    {
        await _connection.ExecuteProcAsync(
            $"{_schema}.fn_delete_blob_by_guid",
            cancellationToken,
            new NpgsqlParameter<Guid>("i_guid", origGuid),
            new NpgsqlParameter("i_calling_user", _callingUser)
        );
    }

    public void AddPostCommitAction(string desc, Action postCommit)
        => _connection.AddPostCommitAction(desc, postCommit);

    public void AddAsyncPostCommitAction(string desc, Func<Task> postCommitAsync)
        => _connection.AddAsyncPostCommitAction(desc, postCommitAsync);

    public void AddRollbackAction(string desc, Action onRollback)
        => _connection.AddRollbackAction(desc, onRollback);

    public void AddAsyncRollbackAction(string desc, Func<Task> onRollbackAsync)
        => _connection.AddAsyncRollbackAction(desc, onRollbackAsync);
}
