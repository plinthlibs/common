
-- Plinth BLOB Storage Procedures

CREATE OR REPLACE FUNCTION public.fn_insert_blob (
IN i_guid UUID,
IN i_name VARCHAR(255),
IN i_provider VARCHAR(10),
IN i_mime_type VARCHAR(255),
IN i_orig_size BIGINT,
IN i_stored_size BIGINT,
IN i_calling_user VARCHAR(255),
IN i_blob_index VARCHAR(1000) = NULL,
IN i_data BYTEA = NULL,
IN i_is_encrypted BOOLEAN = false,
IN i_is_compressed BOOLEAN = false
)
RETURNS INT
LANGUAGE plpgsql    
AS $$
DECLARE v_rc bigint;
BEGIN
    /*
        SELECT * FROM public.fn_insert_blob(
            i_guid := '7c393bb9-60de-4185-b9b4-0240c4ad0e15',
            i_name := 'My File',
            i_provider := 'DB',
            i_data := 0xFF00,
            i_orig_size := 4,
            i_stored_size := 4,
            i_calling_user := 'SampleUser',
            i_blob_index := '123\45\678',
            i_mime_type := 'application/pdf');
    */

    INSERT INTO public.blob (
        guid,
        name,
        provider,
        blob_index,
        mime_type,
        data,
        orig_size,
        stored_size,
        is_encrypted,
        is_compressed,
        date_inserted,
        inserted_by,
        date_updated,
        updated_by
    )
    SELECT
        i_guid,
        i_name,
        i_provider,
        i_blob_index,
        i_mime_type,
        i_data,
        i_orig_size,
        i_stored_size,
        i_is_encrypted,
        i_is_compressed,
        statement_timestamp(),
        i_calling_user,
        statement_timestamp(),
        i_calling_user
    WHERE NOT EXISTS(
        SELECT * FROM public.blob
        WHERE guid = i_guid
    );
    
    GET DIAGNOSTICS v_rc = ROW_COUNT;
    RETURN v_rc;
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_update_blob (
IN i_guid UUID,
IN i_name VARCHAR(255) = NULL,
IN i_provider VARCHAR(10) = NULL,
IN i_mime_type VARCHAR(255) = NULL,
IN i_orig_size BIGINT = NULL,
IN i_stored_size BIGINT = NULL,
IN i_calling_user VARCHAR(255) = NULL,
IN i_blob_index VARCHAR(1000) = NULL,
IN i_data BYTEA = NULL,
IN i_is_encrypted BOOLEAN = NULL,
IN i_is_compressed BOOLEAN = NULL
)
RETURNS INT
LANGUAGE plpgsql    
AS $$
DECLARE v_rc bigint;
BEGIN
    /*
        SELECT * FROM public.fn_update_blob(
            i_guid := '7c393bb9-60de-4185-b9b4-0240c4ad0e15',
            i_provider := 'DB',
            i_name := 'My Document',
            i_data := 0xFF01,
            i_calling_user := 'SampleUser');
    */

    UPDATE public.blob SET
        name = COALESCE(i_name, b.Name),
        provider = COALESCE(i_provider, b.provider),
        blob_index = COALESCE(i_blob_index, b.blob_index),
        mime_type = COALESCE(i_mime_type, b.mime_type),
        data = COALESCE(i_data, b.data),
        orig_size = COALESCE(i_orig_size, b.orig_size),
        stored_size = COALESCE(i_stored_size, b.stored_size),
        is_encrypted = COALESCE(i_is_encrypted, b.is_encrypted),
        is_compressed = COALESCE(i_is_compressed, b.is_compressed),
        date_updated = statement_timestamp(),
        updated_by = i_calling_user
        FROM public.blob AS b
    WHERE b.guid = i_guid;
    
    GET DIAGNOSTICS v_rc = ROW_COUNT;
    RETURN v_rc;
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_update_blob_data (
IN i_guid UUID,
IN i_data BYTEA = NULL,
IN i_calling_user VARCHAR(255) = NULL
)
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE v_rc bigint;
BEGIN
    /*
        SELECT * FROM public.fn_update_blob_data(
            i_guid := '7c393bb9-60de-4185-b9b4-0240c4ad0e15',
            i_data := 0xFF01,
            i_calling_user := 'SampleUser');
    */

    UPDATE public.blob SET
        data = i_data,
        date_updated = statement_timestamp(),
        updated_by = i_calling_user
        FROM public.blob AS b
    WHERE b.guid = i_guid;
    
    GET DIAGNOSTICS v_rc = ROW_COUNT;
    RETURN v_rc;
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_update_blob_sizes (
IN i_guid UUID,
IN i_orig_size BIGINT,
IN i_stored_size BIGINT,
IN i_calling_user VARCHAR(255)
)
RETURNS INT
LANGUAGE plpgsql    
AS $$
DECLARE v_rc bigint;
BEGIN
    /*
        SELECT * FROM public.fn_update_blob_sizes(
            i_guid := '7c393bb9-60de-4185-b9b4-0240c4ad0e15',
            i_orig_size := 1764,
            i_stored_size := 1732,
            i_calling_user := 'SampleUser');
    */

    UPDATE public.blob SET
        orig_size = i_orig_size,
        stored_size = i_stored_size,
        date_updated = statement_timestamp(),
        updated_by = i_calling_user
        FROM public.blob AS b
    WHERE b.guid = i_guid;
    
    GET DIAGNOSTICS v_rc = ROW_COUNT;
    RETURN v_rc;
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_delete_blob_by_guid (
IN i_guid UUID,
IN i_calling_user VARCHAR(255)
)
RETURNS INT
LANGUAGE plpgsql    
AS $$
DECLARE v_rc bigint;
BEGIN
    /*
        SELECT * FROM public.fn_delete_blob_by_guid(
            i_guid := '7c393bb9-60de-4185-b9b4-0240c4ad0e15',
            i_calling_user := 'SampleUser');
    */

    DELETE
    FROM public.blob
    WHERE guid = i_guid;
    
    GET DIAGNOSTICS v_rc = ROW_COUNT;
    RETURN v_rc;
END;
$$;

CREATE OR REPLACE FUNCTION public.fn_get_blob_by_guid (
IN i_guid UUID,
OUT o_guid UUID,
OUT o_name VARCHAR(255),
OUT o_provider VARCHAR(10),
OUT o_blob_index VARCHAR(1000),
OUT o_mime_type VARCHAR(255),
OUT o_data BYTEA,
OUT o_orig_size BIGINT,
OUT o_stored_size BIGINT,
OUT o_is_encrypted BOOLEAN,
OUT o_is_compressed BOOLEAN,
OUT o_date_inserted TIMESTAMP,
OUT o_inserted_by VARCHAR(255),
OUT o_date_updated TIMESTAMP,
OUT o_updated_by VARCHAR(255)
)
RETURNS SETOF RECORD
LANGUAGE plpgsql    
AS $$
BEGIN
    /*
        SELECT * FROM public.fn_get_blob_by_guid(
            i_guid := '7c393bb9-60de-4185-b9b4-0240c4ad0e15')
    */

    RETURN QUERY
    SELECT
        b.guid,
        b.name,
        b.provider,
        b.blob_index,
        b.mime_type,
        b.data,
        b.orig_size,
        b.stored_size,
        b.is_encrypted,
        b.is_compressed,
        b.date_inserted,
        b.inserted_by,
        b.date_updated,
        b.updated_by
        FROM public.blob AS b
    WHERE b.guid = i_guid;
END;
$$;
-- Plinth BLOB Storage Procedures
