
-- Plinth BLOBs Schema
CREATE TABLE public.blob (
    blob_id BIGINT GENERATED ALWAYS AS IDENTITY NOT NULL,
    guid UUID NOT NULL,
    name VARCHAR(255) NOT NULL,
    provider VARCHAR(10) NOT NULL,
    blob_index VARCHAR(1000) NULL,
    mime_type VARCHAR(255) NOT NULL,
    data BYTEA NULL,
    orig_size BIGINT NOT NULL,
    stored_size BIGINT NOT NULL,
    is_encrypted BOOLEAN NOT NULL,
    is_compressed BOOLEAN NOT NULL,
    -- Audit
    date_inserted TIMESTAMP NOT NULL,
    inserted_by VARCHAR(255) NOT NULL,
    date_updated TIMESTAMP NOT NULL,
    updated_by VARCHAR(255) NOT NULL,
    CONSTRAINT pk_blob PRIMARY KEY (blob_id),
    CONSTRAINT uk_blob UNIQUE (guid)
);

-- Plinth BLOBs Schema
