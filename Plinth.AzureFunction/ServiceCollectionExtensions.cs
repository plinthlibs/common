using Microsoft.ApplicationInsights.Channel;
using Microsoft.ApplicationInsights.DependencyCollector;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.ApplicationInsights.WorkerService;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Plinth.AzureFunction;

/// <summary>
/// ServiceCollection Extensions
/// </summary>
public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Enable Application Insights telemetry integration for azure functions
    /// </summary>
    /// <param name="services"></param>
    /// <param name="config"></param>
    /// <param name="captureSqlCommandText">(optional) enable capturing of sql command text</param>
    /// <param name="configureAppInsights">(optional) custom lower level configuration</param>
    /// <remarks>
    /// <list type="bullet">
    /// <item>The Application Insights connection string comes from "APPLICATIONINSIGHTS_CONNECTION_STRING"</item>
    /// <item>The Application Insights cloud role name comes from "WEBSITE_CLOUD_ROLENAME"</item>
    /// </list>
    /// </remarks>
    public static IServiceCollection AddPlinthAppInsightsTelemetry(this IServiceCollection services,
        IConfiguration config,
        bool? captureSqlCommandText = false,
        Action<ApplicationInsightsServiceOptions>? configureAppInsights = null)
    {
        var connectionString = config.GetValue<string>("APPLICATIONINSIGHTS_CONNECTION_STRING");
        var cloudRoleName = config.GetValue<string>("WEBSITE_CLOUD_ROLENAME");

        services
            .AddApplicationInsightsTelemetryWorkerService(options =>
            {
                options.ConnectionString = connectionString;
                configureAppInsights?.Invoke(options);
            })
            .ConfigureFunctionsApplicationInsights()
            .AddSingleton<ITelemetryInitializer>(new CustomFieldsTelemetryInitializer(cloudRoleName));

        // turn on capture of sql text
        if (captureSqlCommandText == true)
        {
            services.ConfigureTelemetryModule<DependencyTrackingTelemetryModule>(
                (module, o) => { module.EnableSqlCommandTextInstrumentation = true; });
        }

        return services;
    }

    internal class CustomFieldsTelemetryInitializer(string? roleName) : ITelemetryInitializer
    {
        public void Initialize(ITelemetry telemetry)
        {
            if (roleName is not null)
                telemetry.Context.Cloud.RoleName = roleName;
        }
    }
}
