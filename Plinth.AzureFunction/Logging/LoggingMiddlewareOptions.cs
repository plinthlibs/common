namespace Plinth.AzureFunction.Logging;

/// <summary>
/// Options class for function logging middleware
/// </summary>
public class LoggingMiddlewareOptions
{
    /// <summary>
    /// Default maximum amount of bytes to log of the request
    /// </summary>
    public const int DefaultMaxRequestLogging = 25000;

    /// <summary>
    /// Default maximum amount of bytes to log of the response
    /// </summary>
    public const int DefaultMaxResponseLogging = 25000;

    /// <summary>max bytes to log from request</summary>
    public int MaxRequestBody { get; set; } = DefaultMaxRequestLogging;
    /// <summary>max bytes to log from response</summary>
    public int MaxResponseBody { get; set; } = DefaultMaxResponseLogging;

    /// <summary>Preprocess the request content before logging it for a given api path</summary>
    public List<(string, Func<string, string>)>? RequestProcessors { get; set; }
    /// <summary>Preprocess the response content before logging it for a given api path</summary>
    public List<(string, Func<string, string>)>? ResponseProcessor { get; set; }
}
