using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Middleware;
using Microsoft.Extensions.Logging;
using Plinth.Common.Exceptions;
using System.Security;

namespace Plinth.AzureFunction.Logging;

internal class UnhandledExceptionMiddleware : IFunctionsWorkerMiddleware
{
    private static readonly ILogger logger = StaticLogManager.GetLogger();

    public async Task Invoke(FunctionContext context, FunctionExecutionDelegate next)
    {
        try
        {
            await next.Invoke(context);
        }
        catch (Exception ex)
        {
            if (context.GetHttpContext() is null)
                throw;

            if (ex is AggregateException aggregateEx)
            {
                ex = aggregateEx.InnerException ?? ex;
            }

            context.GetInvocationResult().Value = DefaultExceptionHandler(ex);
        }
    }

    private static ActionResult? DefaultExceptionHandler(Exception e)
    {
        object content = e.Message;

        switch (e)
        {
            case SecurityException _:
                logger.Warn(e, $"Security Exception: {e.Message}");
                return new ObjectResult(new { message = e.Message }) { StatusCode = StatusCodes.Status403Forbidden };

            case LogicalForbiddenException _:
                logger.Warn(e, $"Forbidden Exception: {e.Message}");
                content = GetContentFromLogicalException(e, content);
                return new ObjectResult(content) { StatusCode = StatusCodes.Status403Forbidden };

            case LogicalNotFoundException _:
                logger.Warn(e, $"Not Found Exception: {e.Message}");
                content = GetContentFromLogicalException(e, content);
                return new NotFoundObjectResult(content);

            case LogicalConflictException _:
                logger.Warn(e, $"Conflict Exception: {e.Message}");
                content = GetContentFromLogicalException(e, content);
                return new ConflictObjectResult(content);

            case LogicalPreconditionFailedException _:
                logger.Warn(e, $"Precondition Failed Exception: {e.Message}");
                content = GetContentFromLogicalException(e, content);
                return new ObjectResult(content) { StatusCode = StatusCodes.Status412PreconditionFailed };

            case LogicalBadRequestException _:
                logger.Warn(e, $"Bad Request Exception: {e.Message}");
                content = GetContentFromLogicalException(e, content);
                return new BadRequestObjectResult(content);

            case HttpApiClient.Common.RestException restEx:
                logger.Warn(e, $"Rest Exception: {e.Message}");
                return new ObjectResult(new { message = restEx.ResponseContent ?? restEx.Message }) { StatusCode = (int)restEx.StatusCode };

            default:
                logger.Error(e, $"Unhandled Exception: {e.Message}");
                return new ObjectResult(new { message = e.Message }) { StatusCode = StatusCodes.Status500InternalServerError };
        }
    }

    private static object GetContentFromLogicalException(Exception e, object defaultContent)
    {
        return (e as LogicalException)?.CustomData ?? defaultContent;
    }
}
