using Microsoft.Azure.Functions.Worker;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Plinth.AzureFunction.Logging;

/// <summary>
/// IFunctionsWorkerApplicationBuilder Extensions
/// </summary>
public static class IFunctionsWorkerApplicationBuilderExtensions
{
    /// <summary>
    /// Enable plinth service logging for HTTP triggered azure functions
    /// </summary>
    /// <example>
    /// .ConfigureFunctionsWorkerDefaults((config, builder) =>
    /// {
    ///     builder.UsePlinthServiceLogging();
    /// })
    /// </example>
    public static IFunctionsWorkerApplicationBuilder UsePlinthServiceLogging(this IFunctionsWorkerApplicationBuilder builder, Action<LoggingMiddlewareOptions>? configure = null)
    {
        if (configure != null)
            builder.Services.Configure(configure);

        builder.UseMiddleware<LoggingMiddleware>(); // it is vital that this middleware is installed before the unhandled exception middleware
        builder.UseMiddleware<UnhandledExceptionMiddleware>();

        return builder;
    }
}
