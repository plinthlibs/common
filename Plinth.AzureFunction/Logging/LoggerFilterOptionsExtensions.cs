using Microsoft.Extensions.Logging;

namespace Plinth.AzureFunction.Logging;

/// <summary>
/// LoggerFilterOptions Extensions
/// </summary>
public static class LoggerFilterOptionsExtensions
{
    /// <summary>
    /// The Application Insights SDK adds a default logging filter that instructs ILogger to capture only Warning and more severe logs. Application Insights requires an explicit override.
    /// Log levels can also be configured using funcsettings.json. For more information, see https://learn.microsoft.com/en-us/azure/azure-monitor/app/worker-service#ilogger-logs
    /// </summary>
    /// <param name="options"></param>
    /// <returns></returns>
    public static LoggerFilterOptions FixAppInsightsLogging(this LoggerFilterOptions options)
    {
        var toRemove = options.Rules.FirstOrDefault(rule => rule.ProviderName
            == "Microsoft.Extensions.Logging.ApplicationInsights.ApplicationInsightsLoggerProvider");

        if (toRemove is not null)
        {
            options.Rules.Remove(toRemove);
        }

        return options;
    }
}
