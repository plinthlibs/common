using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Middleware;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Plinth.AspNetCore.Logging; // some internal classes linked to the files in the aspnetcore package
using Plinth.Common.Constants;
using Plinth.Common.Extensions;
using Plinth.Common.Logging;
using Plinth.Serialization;
using System.Diagnostics;
using System.Net;
using System.Text;

using static Plinth.Common.Logging.Scopes;

namespace Plinth.AzureFunction.Logging;

/// <summary>
/// Middleware for plinth service logging for azure http functions
/// </summary>
internal partial class LoggingMiddleware : IFunctionsWorkerMiddleware
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private static readonly List<string> _loggableContentTypes =
    [
        ContentTypes.ApplicationJson,
        ContentTypes.ApplicationJavascript,
        ContentTypes.ApplicationEcmascript
    ];

    // start the request ids off at a random int32, so it doesn't reset to 0 on app reset
    private static long _requestId = new Random().Next();

    /// <summary>
    /// Set the context ID (logged as CTX-ABCDE)
    /// You only need to call this directly if spawning a thread outside the request flow
    /// For example, if using HostingEnvironment.QueueBackgroundWorkItem() or Task.Run()
    /// </summary>
    public static void SetContextId()
    {
        // do a thread safe increment on the request id and use that for async context
        // only the bottom 20 bits are used, so it will reset every ~1M requests
        StaticLogManager.SetContextId(Interlocked.Increment(ref _requestId));
    }

    private readonly IOptions<LoggingMiddlewareOptions> _opts;
    private readonly ISensitiveDataMasker? _sensitiveDataMasker;
    private readonly ContentPreProcessor _contentPreProcessor = new();

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="options"></param>
    /// <param name="sensitiveDataMasker"></param>
    public LoggingMiddleware(IOptions<LoggingMiddlewareOptions> options, ISensitiveDataMasker sensitiveDataMasker) : this(options)
    {
        _sensitiveDataMasker = sensitiveDataMasker;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="options"></param>
    public LoggingMiddleware(IOptions<LoggingMiddlewareOptions> options)
    {
        _opts = options;

        if (_opts.Value.RequestProcessors != null)
            foreach (var p in _opts.Value.RequestProcessors)
                _contentPreProcessor.AddRequestProcessor(p.Item1, p.Item2);

        if (_opts.Value.ResponseProcessor != null)
            foreach (var p in _opts.Value.ResponseProcessor)
                _contentPreProcessor.AddResponseProcessor(p.Item1, p.Item2);
    }

    /// <summary>
    /// Invocation Function
    /// </summary>
    public async Task Invoke(FunctionContext context, FunctionExecutionDelegate next)
    {
        var opName = $"[Function]-{context.FunctionDefinition.Name}";
        var sw = Stopwatch.StartNew();

        var httpContext = context.GetHttpContext();
        var httpRequest = httpContext?.Request;

        if (httpContext == null || httpRequest == null) // not an http trigger function
        {
            await next.Invoke(context);
            return;
        }

        if (IsOpenApiFunction(context)) // part of the Microsoft.Azure.Functions.Worker.Extensions.OpenApi package
        {
            log.Info("This function is an OpenApi extension function, disabling request logging");
            await next.Invoke(context);
            return;
        }

        RequestTraceProperties.ClearRequestTraceProperties();

        SetContextId();
        bool newTraceId = SetTraceId(httpRequest, out var reqTraceId);
        bool newTraceChain = SetTraceChain(httpRequest);
        SetTraceProperties(httpRequest);

        // Set request trace originating IP address if one is not already set
        string rIpAddress = GetIPAddress(httpRequest);
        RequestTraceProperties.SetRequestTraceOriginatingIpAddress(rIpAddress, false);

        LogTraceInfo(newTraceId, newTraceChain);

        var logMetadata = new List<KeyValuePair<string, object?>>()
        {
            Kvp("RequestTraceId", RequestTraceId.Instance.Get()),
            Kvp("RequestTraceChain", RequestTraceChain.Instance.Get()),
            Kvp("ContextId", StaticLogManager.GetContextId())
        };
        var rtps = RequestTraceProperties.GetRequestTraceProperties();
        if (rtps != null)
        {
            foreach (var rtp in rtps)
                logMetadata.Add(Kvp(rtp.Key, rtp.Value));
        }
        using var _ = log.BeginScope(logMetadata);

        string? requestContent = null;
        string reqUrl = httpRequest.GetDisplayUrl();
        string reqUrlPath = httpRequest.Path;
        string? requestContentType = httpRequest.ContentType ?? TryGetHeader(httpRequest.Headers, "Content-Type");

        bool shouldLogRequestContent = ShouldLogContent(requestContentType);
        bool isFormPost = IsForm(httpRequest, requestContentType);
        long requestContentLength = httpRequest!.ContentLength ?? 0;

        if (!isFormPost && shouldLogRequestContent && httpRequest.Body != null && requestContentLength > 0 && _opts.Value.MaxRequestBody > 0)
        {
            var forkedRequest = new ForkingReadStream(httpRequest.Body, _opts.Value.MaxRequestBody, requestContentLength);
            httpRequest.Body = forkedRequest;
            requestContent = await forkedRequest.ReadMemoryAsString(httpContext.RequestAborted);
        }

        _contentPreProcessor.PreProcessRequest(reqUrlPath, ref requestContent);

        LogRequest(httpRequest.Method, requestContent, requestContentLength, isFormPost, shouldLogRequestContent, requestContentType, opName, reqUrl, rIpAddress);

        HttpResponse response = httpContext.Response;
        var forkedResponse = new ForkingWriteStream(response.Body, _opts.Value.MaxResponseBody);

        response.OnStarting(() =>
        {
            // note: when errors are returned, the async context is not resumed in here, so no access to AsyncLocals (only local captures)
            if (newTraceId)
                response.Headers.Append(RequestTraceId.RequestTraceIdHeader, new StringValues(reqTraceId));
            return Task.CompletedTask;
        });

        response.Body = forkedResponse;

        try
        {
            await next.Invoke(context);
        }
        catch (Exception e)
        {
            log.Error(e, $"Unexpected Unhandled Exception: {e.Message}");
            context.GetInvocationResult().Value = new ObjectResult(new { message = e.Message }) { StatusCode = StatusCodes.Status500InternalServerError };
        }

        var invocationResult = context.GetInvocationResult();
        StaticLogManager.TraceInfo? traceInfo = null;

        // we support two modes.
        // 1. function returns an IActionResult => wrap it and return.  We will log after action result is executed
        // 2. function writes to the response manually and does not return a response => log directly as response body is populated
        // NOTE: to capture exceptions, the unhandled exception handler must execute before this middleware to turn exceptions into IActionResult
        if (invocationResult?.Value is IActionResult actionResult)
        {
            traceInfo = StaticLogManager.GetTraceInfo();
            invocationResult.Value = new ActionResultWrapper(actionResult, logResponse);
        }
        else
        {
            logResponse();
        }

        void logResponse()
        {
            // if we came in from the ActionResultWrapper, the async context is not returned,
            // so we restore our trace context
            if (traceInfo != null)
                StaticLogManager.LoadTraceInfo(traceInfo);

            var responseContentType = response.ContentType;
            var shouldLogResponseContent = ShouldLogContent(responseContentType);

            var responseContent = shouldLogResponseContent ? forkedResponse!.GetMemoryAsString() : null;

            _contentPreProcessor.PreProcessResponse(httpRequest!.Path, ref responseContent);

            sw!.Stop();
            LogResponse((HttpStatusCode)response.StatusCode, responseContent, response.Body.Length, shouldLogResponseContent, responseContentType, sw.Elapsed, opName!, reqUrl);
        }
    }

    /// <summary>
    /// The OpenApi extension package is not compatible with our logging because it uses the old HttpResponseData format
    /// and it creates a new Response instead of using the one already available.  We will not handle any functions that it defines
    /// </summary>
    private static bool IsOpenApiFunction(FunctionContext context)
        => context.FunctionDefinition.Name switch
        {
            "RenderSwaggerDocument" or
            "RenderSwaggerUI" or
            "RenderOpenApiDocument" or
            "RenderOAuth2Redirect"
                => true,

            _ => false
        };

    private class ActionResultWrapper(IActionResult inner, Action callback) : IActionResult
    {
        public async Task ExecuteResultAsync(ActionContext context)
        {
            await inner.ExecuteResultAsync(context);
            callback();
        }
    }

    private static bool IsForm(HttpRequest req, string? contentType)
        => req.Body != null && contentType != null && contentType.StartsWith(ContentTypes.MultipartFormData);

    private static void LogTraceInfo(bool newTraceId, bool newTraceChain)
    {
        var sb = new StringBuilder(128);

        if (newTraceId)
            sb.Append("New trace id: ").Append(RequestTraceId.Instance.Get());

        if (newTraceChain)
        {
            if (newTraceId)
                sb.Append(", ");
            sb.Append("New trace chain: ").Append(RequestTraceChain.Instance.Get());
        }

        if (newTraceId || newTraceChain)
            log.Debug(sb.ToString());
    }

    private void LogRequest(string? method, string? requestContent, long requestContentLength, bool isFormPost, bool shouldLog, string? contentType, string operationName, string reqUrl, string rIpAddress)
    {
        var rId = RequestTraceId.Instance.Get();
        var rChain = RequestTraceChain.Instance.Get();

        var logMetadata = CreateScope(
            Kvp("IpAddress", rIpAddress),
            Kvp(LoggingConstants.Field_MessageType, "RequestReceived")
        );

        using (log.BeginScope(logMetadata))
        {
            if (isFormPost)
                LogDefines.LogRequestFormPost(log, $"{rId},{rChain}", rIpAddress, operationName, method, reqUrl, requestContentLength);
            else if (string.IsNullOrEmpty(requestContent))
                LogDefines.LogRequestNoContent(log, $"{rId},{rChain}", rIpAddress, operationName, method, reqUrl, requestContentLength);
            else if (!shouldLog)
                LogDefines.LogRequestNoLog(log, $"{rId},{rChain}", rIpAddress, operationName, method, reqUrl, contentType, requestContentLength);
            else
            {
                var content = MaskSensitiveFields(requestContentLength > _opts.Value.MaxRequestBody ? string.Concat(requestContent, "<truncated>") : requestContent);
                LogDefines.LogRequestWithContent(log, $"{rId},{rChain}", rIpAddress, operationName, method, reqUrl, contentType, requestContentLength, content);
            }
        }
    }

    private void LogResponse(HttpStatusCode statusCode, string? responseContent, long contentLength, bool shouldLogContent, string? contentType, TimeSpan duration, string operationName, string reqUrl)
    {
        var rId = RequestTraceId.Instance.Get();
        var rChain = RequestTraceChain.Instance.Get();
        var statusCodeInt = (int)statusCode;
        var statusCodeStr = statusCode.ToString();

        var logMetadata = CreateScope(
            Kvp("RequestUri", reqUrl),
            Kvp(LoggingConstants.Field_TimeOperation, operationName),
            Kvp(LoggingConstants.Field_TimeDurationMicros, (long)(duration.TotalMilliseconds * 1000)),
            Kvp(LoggingConstants.Field_MessageType, "ResponseSent")
        );

        using (log.BeginScope(logMetadata))
        {
            if (shouldLogContent)
            {
                var content = MaskSensitiveFields(contentLength > _opts.Value.MaxResponseBody ? string.Concat(responseContent, "<truncated>") : responseContent);
                LogDefines.LogResponseWithContent(log, $"{rId},{rChain}", duration.ToString(), statusCodeInt, statusCodeStr, contentType, contentLength, content);
            }
            else
                LogDefines.LogResponseNoContent(log, $"{rId},{rChain}", duration.ToString(), statusCodeInt, statusCodeStr, contentType, contentLength);
        }
    }

    private string? MaskSensitiveFields(string? s)
    {
        var result = s.MaskSensitiveFields();

        if (_sensitiveDataMasker != null)
            result = _sensitiveDataMasker.MaskSensitiveFields(result);

        return result;
    }

    /// <summary>
    /// Utility function which determines if content is suitable for logging
    /// </summary>
    private static bool ShouldLogContent(string? contentType)
    {
        if (contentType == null)
            return false;

        // log any text/ or anything that is xml
        if (contentType.StartsWith("text/") || contentType.Contains("xml"))
            return true;

        // check if content type starts with any of the loggable types
        // starts with is used because some content types come back like "application/json; charset=utf-8"
        foreach (var loggable in _loggableContentTypes)
        {
            if (contentType.StartsWith(loggable))
                return true;
        }

        return false;
    }

    /// <summary>
    /// Tries to get client IP address for logging purposes
    /// </summary>
    private static string GetIPAddress(HttpRequest r)
    {
        // this only works when deployed to azure
        if (r.Headers.TryGetValue("X-Forwarded-For", out var vals))
        {
            var ipn = vals.FirstOrDefault()?.Split(',').FirstOrDefault()?.Split(':').FirstOrDefault();
            if (IPAddress.TryParse(ipn, out var ipAddress))
                return ipAddress.ToString();
        }

        var ip = r.HttpContext.Connection?.RemoteIpAddress;
        if (ip != null)
            return ip.ToString();

        // some value representing local connections
        return "::0";
    }

    private static string? TryGetHeader(IHeaderDictionary? headers, string header)
    {
        if (headers == null)
            return null;
        if (headers.TryGetValue(header, out var vals))
            return vals.First();
        return null;
    }

    private static bool SetTraceId(HttpRequest request, out string traceId)
    {
        if (!request.Headers.TryGetValue(RequestTraceId.RequestTraceIdHeader, out var vals) ||
            vals.Count == 0 ||
            string.IsNullOrWhiteSpace(vals.First()))
        {
            traceId = RequestTraceId.Instance.Create();
            return true;
        }
        else
        {
            traceId = vals.First()!;
            RequestTraceId.Instance.Set(traceId);
            return false;
        }
    }

    private static bool SetTraceChain(HttpRequest request)
    {
        if (!request.Headers.TryGetValue(RequestTraceChain.RequestTraceChainHeader, out var vals) ||
            vals.Count == 0 ||
            string.IsNullOrWhiteSpace(vals.First()))
        {
            RequestTraceChain.Instance.Create();
            return true;
        }
        else
        {
            RequestTraceChain.Instance.Set(vals.First()!);
            return false;
        }
    }

    private static void SetTraceProperties(HttpRequest request)
    {
        if (!request.Headers.TryGetValue(RequestTraceProperties.RequestTracePropertiesHeader, out var vals) ||
            vals.Count == 0 ||
            string.IsNullOrWhiteSpace(vals.First()))
        {
            // no trace props or empty
        }
        else
        {
            try
            {
                var rProps = JsonUtil.DeserializeObject<Dictionary<string, string>>(vals.First()!);
                RequestTraceProperties.SetRequestTraceProperties(rProps);
            }
            catch (Exception e)
            {
                log.Error(e, "Error deserializing request trace properties");
            }
        }
    }

    private static partial class LogDefines
    {
        [LoggerMessage(0, LogLevel.Information,
            "<== [{TraceString}] Received form request from {IpAddress}: {" + LoggingConstants.Field_TimeOperation + "}, {HttpMethod} {RequestUri}, Length: {ContentLength}",
            EventName = "RequestFormPost")]
        public static partial void LogRequestFormPost(ILogger logger, string? TraceString, string? ipAddress, string timeOperation, string? httpMethod, string? requestUri, long contentLength);

        [LoggerMessage(1, LogLevel.Information,
            "<== [{TraceString}] Received request from {IpAddress}: {" + LoggingConstants.Field_TimeOperation + "}, {HttpMethod} {RequestUri}, Type: {ContentType}, Length: {ContentLength}",
            EventName = "RequestNoLog")]
        public static partial void LogRequestNoLog(ILogger logger, string traceString, string? ipAddress, string? timeOperation, string? httpMethod, string? requestUri, string? contentType, long contentLength);

        [LoggerMessage(2, LogLevel.Information,
            "<== [{TraceString}] Received request from {IpAddress}: {" + LoggingConstants.Field_TimeOperation + "}, {HttpMethod} {RequestUri}, Length: {ContentLength}, No Content",
            EventName = "RequestNoContent")]
        public static partial void LogRequestNoContent(ILogger logger, string traceString, string? ipAddress, string? timeOperation, string? httpMethod, string? requestUri, long contentLength);

        [LoggerMessage(3, LogLevel.Information,
            "<== [{TraceString}] Received request from {IpAddress}: {" + LoggingConstants.Field_TimeOperation + "}, {HttpMethod} {RequestUri}, Type: {ContentType}, Length: {ContentLength}, Content {Content}",
            EventName = "RequestWithContent")]
        public static partial void LogRequestWithContent(ILogger logger, string traceString, string? ipAddress, string? timeOperation, string? httpMethod, string? requestUri, string? contentType, long contentLEngth, string? content);

        [LoggerMessage(4, LogLevel.Information,
            "==> [{TraceString}] Sending response: Took: {" + LoggingConstants.Field_TimeDuration + "}, Status Code: {HttpStatusCode}/{HttpStatus}, Type: {ContentType}, Length: {ContentLength}",
            EventName = "ResponseNoContent")]
        public static partial void LogResponseNoContent(ILogger logger, string traceString, string timeDuration, int httpStatusCode, string httpStatus, string? contentType, long contentLength);

        [LoggerMessage(5, LogLevel.Information,
            "==> [{TraceString}] Sending response: Took: {" + LoggingConstants.Field_TimeDuration + "}, Status Code: {HttpStatusCode}/{HttpStatus}, Type: {ContentType}, Length: {ContentLength}, Content: {Content}",
            EventName = "ResponseWithContent")]
        public static partial void LogResponseWithContent(ILogger logger, string? TraceString, string? timeDuration, int httpStatusCode, string httpStatus, string? contentType, long contentLength, string? content);
    }
}
