using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Plinth.Logging.Host;

namespace Plinth.AzureFunction.Logging;

/// <summary>
/// Extensions for IHost
/// </summary>
public static class IHostExtensions
{
    /// <summary>
    /// Attaches the azure function logger factory to plinth's StaticLogManager
    /// </summary>
    /// <param name="host">the built IHost</param>
    /// <example>
    /// host = hostBuilder.....Build();
    /// host.AddPlinthLogging();
    /// await host.RunAsync();
    /// </example>
    public static IHost AddPlinthLogging(this IHost host)
    {
        var loggerFac = host.Services.GetRequiredService<ILoggerFactory>();
        StaticLogManagerSetup.ConfigureForSingleThread(loggerFac);
        return host;
    }
}
