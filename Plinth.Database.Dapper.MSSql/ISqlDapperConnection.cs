﻿using Plinth.Database.MSSql;

namespace Plinth.Database.Dapper.MSSql;

/// <summary>
/// Dapper specific sql connection
/// </summary>
public interface ISqlDapperConnection
{
    #region Proc Async
    /// <summary>
    /// Execute a stored procedure that returns no results, async
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="param"></param>
    /// <param name="cancellationToken"></param>
    /// <exception cref="DatabaseException">thrown with ExpectedRowsMismatch if proc returns 0 rows modified</exception>
    Task<int> ExecuteProcAsync(string procName, object? param = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Execute a stored procedure that returns no results, check for a specific # of rows returned, async
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="expectedRows"></param>
    /// <param name="param"></param>
    /// <param name="cancellationToken"></param>
    /// <exception cref="DatabaseException">thrown with ExpectedRowsMismatch if proc returns 0 rows modified</exception>
    Task ExecuteProcAsync(string procName, int expectedRows, object? param = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Execute a stored procedure that returns no results, does not check expected rows, async
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="param"></param>
    /// <param name="cancellationToken"></param>
    /// <returns>number of rows affected</returns>
    Task<int> ExecuteProcUncheckedAsync(string procName, object? param = null, CancellationToken cancellationToken = default);
    #endregion

    #region Proc
    /// <summary>
    /// Execute a stored procedure that returns no results
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="param"></param>
    /// <exception cref="DatabaseException">thrown with ExpectedRowsMismatch if proc returns 0 rows modified</exception>
    int ExecuteProc(string procName, object? param = null);

    /// <summary>
    /// Execute a stored procedure that returns no results, check for a specific # of rows returned
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="expectedRows"></param>
    /// <param name="param"></param>
    /// <exception cref="DatabaseException">thrown with ExpectedRowsMismatch if proc returns 0 rows modified</exception>
    void ExecuteProc(string procName, int expectedRows, object? param = null);

    /// <summary>
    /// Execute a stored procedure that returns no results, does not check expected rows
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="param"></param>
    /// <returns>number of rows affected</returns>
    int ExecuteProcUnchecked(string procName, object? param = null);
    #endregion

    #region Query Proc Async
    /// <summary>
    /// Execute a stored procedure that returns a list of results, async
    /// </summary>
    /// <typeparam name="T">type of the objects in the list</typeparam>
    /// <param name="procName"></param>
    /// <param name="param"></param>
    /// <param name="cancellationToken"></param>
    /// <returns>An IEnumerable of objects, always non-null</returns>
    Task<IEnumerable<T>> ExecuteQueryProcListAsync<T>(string procName, object? param = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Execute a stored procedure that returns a single row, async
    /// </summary>
    /// <typeparam name="T">type of the single value to return</typeparam>
    /// <param name="procName"></param>
    /// <param name="param"></param>
    /// <param name="cancellationToken"></param>
    /// <returns>a single value or default(T) if no row returned</returns>
    Task<T?> ExecuteQueryProcOneAsync<T>(string procName, object? param = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Execute a stored procedure that can return multiple result sets
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="readerAction">An action that receives a multi-result-set which provides all the result sets</param>
    /// <param name="param"></param>
    /// <param name="cancellationToken"></param>
    Task ExecuteQueryProcMultiResultSetAsync(string procName, Func<IDapperMultiResultSetAsync, Task> readerAction, object? param = null, CancellationToken cancellationToken = default);
    #endregion

    #region Query Proc
    /// <summary>
    /// Execute a stored procedure that returns a list of results
    /// </summary>
    /// <typeparam name="T">type of the objects in the list</typeparam>
    /// <param name="procName"></param>
    /// <param name="param"></param>
    /// <returns>An IEnumerable of objects, always non-null</returns>
    IEnumerable<T> ExecuteQueryProcList<T>(string procName, object? param = null);

    /// <summary>
    /// Execute a stored procedure that returns a single row
    /// </summary>
    /// <typeparam name="T">type of the single value to return</typeparam>
    /// <param name="procName"></param>
    /// <param name="param"></param>
    /// <returns>a single value or default(T) if no row returned</returns>
    T? ExecuteQueryProcOne<T>(string procName, object? param = null);

    /// <summary>
    /// Execute a stored procedure that can return multiple result sets
    /// </summary>
    /// <param name="procName"></param>
    /// <param name="readerAction">An action that receives a multi-result-set which provides all the result sets</param>
    /// <param name="param"></param>
    void ExecuteQueryProcMultiResultSet(string procName, Action<IDapperMultiResultSet> readerAction, object? param = null);
    #endregion
}
