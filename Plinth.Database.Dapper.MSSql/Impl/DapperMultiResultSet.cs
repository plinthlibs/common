namespace Plinth.Database.Dapper.MSSql.Impl;

class DapperMultiResultSet(global::Dapper.SqlMapper.GridReader gridReader) : IDapperMultiResultSetAsync, IDapperMultiResultSet
{
    public async Task<IEnumerable<T>> GetListAsync<T>(CancellationToken cancellationToken)
        => DapperConnection.LogRows(await gridReader.ReadAsync<T>(), affected: false);

    public async Task<T?> GetOneAsync<T>(CancellationToken cancellationToken)
    {
        using var r = new DapperConnection.LogRowsSingle<T>(await gridReader.ReadFirstOrDefaultAsync<T>(), affected: false);
        return r.Value;
    }

    public IEnumerable<T> GetList<T>()
        => DapperConnection.LogRows(gridReader.Read<T>(), affected: false);

    public T? GetOne<T>()
    {
        using var r = new DapperConnection.LogRowsSingle<T>(gridReader.ReadFirstOrDefault<T>(), affected: false);
        return r.Value;
    }
}
