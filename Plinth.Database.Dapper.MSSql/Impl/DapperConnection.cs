using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Plinth.Common.Logging;
using Plinth.Common.Utils;
using Plinth.Database.MSSql;
using Plinth.Database.MSSql.Impl;
using Plinth.Serialization;
using System.Data;

using static Plinth.Common.Logging.Scopes;

namespace Plinth.Database.Dapper.MSSql.Impl;

internal partial class DapperConnection : IRawSqlDapperConnection
{
    private static readonly ILogger log = StaticLogManager.GetLogger();

    private readonly SqlConnection _conn;
    private readonly SqlTransaction? _txn;
    private readonly PlinthNoTxnSqlConnection? _noTxnConn;

    public DapperConnection(PlinthSqlTransaction plinthTxn)
    {
        _conn = plinthTxn.GetSqlConnection();
        _txn = plinthTxn.GetSqlTransaction();
        _noTxnConn = null;
    }

    public DapperConnection(PlinthNoTxnSqlConnection conn)
    {
        _conn = conn.GetSqlConnection();
        _noTxnConn = conn;
        _txn = null;
    }

    #region rowcount logging
    private static int LogRows(int rows, bool affected)
    {
        if (affected)
            LogDefines.LogRowsAffected(log, rows);
        else
            LogDefines.LogRowsReturned(log, rows);
        return rows;
    }

    internal static IEnumerable<T> LogRows<T>(IEnumerable<T> rows, bool affected)
    {
        int rowCount = 0;
        foreach (var r in rows)
        {
            rowCount++;
            yield return r;
        }
        LogRows(rowCount, affected);
    }

    internal class LogRowsSingle<T>(T? t, bool affected) : IDisposable
    {
        private readonly bool _rowReturned = EqualityComparer<T>.Default.Equals(t, default);

        public T? Value => t;

        public void Dispose()
        {
            LogRows(_rowReturned ? 1 : 0, affected);
        }
    }
    #endregion

    #region query proc async
    public async Task<IEnumerable<T>> ExecuteQueryProcListAsync<T>(string procName, object? param, CancellationToken cancellationToken)
    {
        using (log.BeginScope(ProcMeta(procName, param)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProcList", log))
        {
            var command = new CommandDefinition(procName, param, _txn, 
                commandType: CommandType.StoredProcedure, cancellationToken: cancellationToken);
            return LogRows(await _conn.QueryAsync<T>(command), affected: false);
        }
    }

    public async Task<T?> ExecuteQueryProcOneAsync<T>(string procName, object? param, CancellationToken cancellationToken)
    {
        using (log.BeginScope(ProcMeta(procName, param)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProcOne", log))
        {
            var command = new CommandDefinition(procName, param, _txn, 
                commandType: CommandType.StoredProcedure, cancellationToken: cancellationToken);
            using var r = new LogRowsSingle<T>(
                await _conn.QueryFirstOrDefaultAsync<T>(command), affected: false);
            return r.Value;
        }
    }
    #endregion query proc async

    #region query proc
    public IEnumerable<T> ExecuteQueryProcList<T>(string procName, object? param)
    {
        using (log.BeginScope(ProcMeta(procName, param)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProcList", log))
        {
            return LogRows(_conn.Query<T>(
                procName, param,
                commandType: CommandType.StoredProcedure,
                transaction: _txn), affected: false);
        }
    }

    public T? ExecuteQueryProcOne<T>(string procName, object? param)
    {
        using (log.BeginScope(ProcMeta(procName, param)))
        using (new TimeUtil.TimeLogger("ExecuteQueryProcOne", log))
        {
            using var r = new LogRowsSingle<T>(_conn.QueryFirstOrDefault<T>(
                procName, param,
                commandType: CommandType.StoredProcedure,
                transaction: _txn), affected: false);
            return r.Value;
        }
    }
    #endregion query proc

    #region proc async
    public async Task<int> ExecuteProcUncheckedAsync(string procName, object? param, CancellationToken cancellationToken)
    {
        _noTxnConn?.SetWritten();
        using (log.BeginScope(ProcMeta(procName, param)))
        using (new TimeUtil.TimeLogger("ExecuteProc", log))
        {
            var command = new CommandDefinition(procName, param, _txn, 
                commandType: CommandType.StoredProcedure, cancellationToken: cancellationToken);
            return LogRows(await _conn.ExecuteAsync(command), affected: true);
        }
    }

    public async Task<int> ExecuteProcAsync(string procName, object? param, CancellationToken cancellationToken)
    {
        int rows = await ExecuteProcUncheckedAsync(procName, param, cancellationToken);
        if (rows <= 0)
            throw new DatabaseException("expected #rows > 0", DatabaseExceptionType.ExpectedRowsMismatch);
        return rows;
    }

    public async Task ExecuteProcAsync(string procName, int expectedRows, object? param, CancellationToken cancellationToken)
    {
        int rows = await ExecuteProcUncheckedAsync(procName, param, cancellationToken);
        if (rows != expectedRows)
            throw new DatabaseException($"expected {expectedRows} got {rows}", DatabaseExceptionType.ExpectedRowsMismatch);
    }

    public async Task ExecuteQueryProcMultiResultSetAsync(string procName, Func<IDapperMultiResultSetAsync, Task> readerAction, object? param, CancellationToken cancellationToken)
    {
        using (log.BeginScope(ProcMeta(procName, param)))
        using (new TimeUtil.TimeLogger("ExecuteProcMultiResultSet", log))
        {
            var command = new CommandDefinition(procName, param, _txn, 
                commandType: CommandType.StoredProcedure, cancellationToken: cancellationToken);
            using var gr = await _conn.QueryMultipleAsync(command);
            await readerAction(new DapperMultiResultSet(gr));
        }
    }
    #endregion proc async

    #region proc
    public int ExecuteProcUnchecked(string procName, object? param)
    {
        _noTxnConn?.SetWritten();
        using (log.BeginScope(ProcMeta(procName, param)))
        using (new TimeUtil.TimeLogger("ExecuteProc", log))
        {          
            return LogRows(_conn.Execute(
                procName, param,
                commandType: CommandType.StoredProcedure,
                transaction: _txn), affected: true);
        }
    }

    public int ExecuteProc(string procName, object? param)
    {
        int rows = ExecuteProcUnchecked(procName, param);
        if (rows <= 0)
            throw new DatabaseException("expected #rows > 0", DatabaseExceptionType.ExpectedRowsMismatch);
        return rows;
    }

    public void ExecuteProc(string procName, int expectedRows, object? param)
    {
        int rows = ExecuteProcUnchecked(procName, param);
        if (rows != expectedRows)
            throw new DatabaseException($"expected {expectedRows} got {rows}", DatabaseExceptionType.ExpectedRowsMismatch);
    }

    public void ExecuteQueryProcMultiResultSet(string procName, Action<IDapperMultiResultSet> readerAction, object? param = null)
    {
        using (log.BeginScope(ProcMeta(procName, param)))
        using (new TimeUtil.TimeLogger("ExecuteProcMultiResultSet", log))
        {
            using var gr = _conn.QueryMultiple(
                procName, param,
                commandType: CommandType.StoredProcedure,
                transaction: _txn);
            readerAction(new DapperMultiResultSet(gr));
        }
    }
    #endregion proc

    private static KeyValuePair<string, object?>[] ProcMeta(string procName, object? param)
    {
        var parameters = JsonUtil.SerializeObject(param);

        LogDefines.LogExecute(log, procName, parameters);

        return CreateScope(
            Kvp( "StoredProc", procName ),
            Kvp( "StoredProcParams", parameters ),
            Kvp( LoggingConstants.Field_TimeOperation, procName )
        );
    }

    private static KeyValuePair<string, object?>[] RawMeta(string sql, object? param)
    {
        var parameters = JsonUtil.SerializeObject(param);

        LogDefines.LogRawExecute(log, sql, parameters);

        return CreateScope(
            Kvp( "SQLText", sql ),
            Kvp( "SQLParams", parameters ),
            Kvp( LoggingConstants.Field_TimeOperation, "RawSQL" )
        );
    }

    #region raw
    public int ExecuteRaw(string sql, object? param)
    {
        _noTxnConn?.SetWritten();
        using (log.BeginScope(RawMeta(sql, param)))
        using (new TimeUtil.TimeLogger("ExecuteRaw", log))
        {
            return LogRows(_conn.Execute(
                sql, param,
                commandType: CommandType.Text,
                transaction: _txn), affected: true);
        }
    }

    public IEnumerable<T> ExecuteRawQueryList<T>(string sql, object? param)
    {
        using (log.BeginScope(RawMeta(sql, param)))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryList", log))
        {
            return LogRows(_conn.Query<T>(
                sql, param,
                commandType: CommandType.Text,
                transaction: _txn), affected: false);
        }
    }

    public T? ExecuteRawQueryOne<T>(string sql, object? param)
    {
        using (log.BeginScope(RawMeta(sql, param)))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryOne", log))
        {
            using var r = new LogRowsSingle<T>(_conn.QueryFirstOrDefault<T>(
                sql, param,
                commandType: CommandType.Text,
                transaction: _txn), affected: false);
            return r.Value;
        }
    }

    public void ExecuteRawQueryMultiResultSet(string sql, Action<IDapperMultiResultSet> readerAction, object? param)
    {
        using (log.BeginScope(RawMeta(sql, param)))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryMultiResultSet", log))
        {
            using var gr = _conn.QueryMultiple(
                sql, param,
                commandType: CommandType.Text,
                transaction: _txn);
            readerAction(new DapperMultiResultSet(gr));
        }
    }

    public async Task<int> ExecuteRawAsync(string sql, object? param, CancellationToken cancellationToken)
    {
        _noTxnConn?.SetWritten();
        using (log.BeginScope(RawMeta(sql, param)))
        using (new TimeUtil.TimeLogger("ExecuteRaw", log))
        {
            var command = new CommandDefinition(sql, param, _txn, 
                commandType: CommandType.Text, cancellationToken: cancellationToken);
            return LogRows(await _conn.ExecuteAsync(command), affected: true);
        }
    }

    public async Task<IEnumerable<T>> ExecuteRawQueryListAsync<T>(string sql, object? param, CancellationToken cancellationToken)
    {
        using (log.BeginScope(RawMeta(sql, param)))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryList", log))
        {
            var command = new CommandDefinition(sql, param, _txn, 
                commandType: CommandType.Text, cancellationToken: cancellationToken);
            return LogRows(await _conn.QueryAsync<T>(command), affected: false);
        }
    }

    public async Task<T?> ExecuteRawQueryOneAsync<T>(string sql, object? param, CancellationToken cancellationToken)
    {
        using (log.BeginScope(RawMeta(sql, param)))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryOne", log))
        {
            var command = new CommandDefinition(sql, param, _txn, 
                commandType: CommandType.Text, cancellationToken: cancellationToken);
            using var r = new LogRowsSingle<T>(await _conn.QueryFirstOrDefaultAsync<T>(command), affected: false);
            return r.Value;
        }
    }

    public async Task ExecuteRawQueryMultiResultSetAsync(string sql, Func<IDapperMultiResultSetAsync, Task> readerAction, object? param, CancellationToken cancellationToken)
    {
        using (log.BeginScope(RawMeta(sql, param)))
        using (new TimeUtil.TimeLogger("ExecuteRawQueryMultiResultSet", log))
        {
            var command = new CommandDefinition(sql, param, _txn, 
                commandType: CommandType.Text, cancellationToken: cancellationToken);
            using var gr = await _conn.QueryMultipleAsync(command);
            await readerAction(new DapperMultiResultSet(gr));
        }
    }
    #endregion

    private static partial class LogDefines
    {
        [LoggerMessage(0, LogLevel.Debug, "{RowCount} row(s) affected", EventName = "RowsAffected")]
        public static partial void LogRowsAffected(ILogger logger, int rowCount);

        [LoggerMessage(1, LogLevel.Debug, "{RowCount} row(s) returned", EventName = "RowsReturned")]
        public static partial void LogRowsReturned(ILogger logger, int rowCount);

        [LoggerMessage(2, LogLevel.Debug, "EXECUTE {StoredProc} input={StoredProcParams}", EventName = "ExecuteProc")]
        public static partial void LogExecute(ILogger logger, string storedProc, string storedProcParams);

        [LoggerMessage(3, LogLevel.Debug, "{SQLText} input={SQLParams}", EventName = "ExecuteRaw")]
        public static partial void LogRawExecute(ILogger logger, string sqlText, string sqlParams);
    }
}
